/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when upgrading.
*/

Ext.application({
    name: 'singer',

    extend: 'singer.Application',
    
    autoCreateViewport: true
});


// overide all default timeouts

//Ext.define('TimeoutCls', {
//    override: 'Ext.data.proxy.Ajax',
//    timeout: 40
//});
