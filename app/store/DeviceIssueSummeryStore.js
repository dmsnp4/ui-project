
Ext.define('singer.store.DeviceIssueSummeryStore', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.DeviceIssueSummeryM',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET'
        },
        api: {
            read: singer.AppParams.JBOSS_PATH + 'DeviceIssues/getModelWiseDSRDeviceIssue'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});
