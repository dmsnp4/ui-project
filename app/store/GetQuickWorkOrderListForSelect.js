Ext.define('singer.store.GetQuickWorkOrderListForSelect', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrder',
    autoLoad: true,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST'
//            destroy: 'POST'
        },
        api: {
            read: singer.AppParams.JBOSS_PATH +  'WorkOrders/getWorkOrderListForSelectWO'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            var proxyUrl = store.getProxy().api.read;
            console.log(proxyUrl);
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId+ "&status=1";

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId+"&status=1&workOrderStatus=2";

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId+"&status=1&workOrderStatus=2";
                }
                
                
            }
            store.getProxy().api.read = proxyUrl;
            
            console.log("ewww", store.getProxy().api.read = proxyUrl);
        },
          load: function (callback) {
            console.log("CCC3 ", callback.totalCount);
            var totalRow = callback.totalCount;
            if (totalRow === 1000000) {
                Ext.Msg.alert("Operation failed", "Cannot connect to the database. Please contact the system Admin");

            }
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});
