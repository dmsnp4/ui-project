
Ext.define('singer.store.WOList', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrderPayment',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET'
        },
        api: {
//            read: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/getWorkOrderPaymentList'
            read: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/getWorkOrderPaymentLisToGrid'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        },
        timeout:60000
    },
    listeners: {
        beforeload: function (store, operation, options) {
            var proxyUrl = store.getProxy().api.read;
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');

            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId;

            } else {
                if (proxyUrl.indexOf("bisId") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});