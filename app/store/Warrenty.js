Ext.define('singer.store.Warrenty', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.Warrenty',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'WarrantyAdjustment/addWarrantyAdjustment',
            read: singer.AppParams.JBOSS_PATH + 'WarrantyAdjustment/getWarrantyAdjustmentDetails',
            update: singer.AppParams.JBOSS_PATH + 'WarrantyAdjustment/EditWarrantyAdjustment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});

