
Ext.define('singer.store.CreditNoteIssuIME', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.CreditNoteIssueDetails',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET'
        },
        api: {
            read: singer.AppParams.JBOSS_PATH + 'CreditNoteIssues/getCreditNoteDetail',
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});


