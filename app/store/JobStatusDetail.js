


Ext.define('singer.store.JobStatusDetail', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrderOpen',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET'
        },
        api: {
//            read: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getWorkOrderListToAddMaintain'
            read: singer.AppParams.JBOSS_PATH + 'WorkOrderInquries/getWorkOrderStatusInquireLoad'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        listeners:
                {
                    beforeload: function (store, operation, options)
                    {

                        var nic = localStorage.getItem('customerNIC');
                        var uri = store.proxy.api.read;

                        uri = uri.split('getGeneralFeedBacks')[0];
                        uri += 'getGeneralFeedBacks?nic=' + nic;

                        store.proxy.api.read = uri;
                    },
                    resetProxy: function (proxyURI)
                    {
                        return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
                    }
                }
                
//        beforeload: function (store, operation, options) {
////            //console.log(store, operation, options)
//            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
//            var proxyUrl = store.getProxy().api.read;
//
//            
////            options.resetProxy(proxyUrl);
//            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
//            
//            var id = '1';
//            if (proxyUrl.indexOf("?") === -1) {
//                proxyUrl += "?" + "bisId=" + bisId;
//
//            } else {
//                if (proxyUrl.indexOf("bisId") === -1) {
//                    proxyUrl += "&" + "bisId=" + bisId;
//
//                } else {
//                    proxyUrl = proxyUrl.split("?")[0];
//                    proxyUrl += "?" + "bisId=" + bisId;
//                }
//            }
//            store.getProxy().api.read = proxyUrl;
//        },
//        resetProxy: function (proxyURI) {
//            //console.log
//            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
//        }
    }
});

