Ext.define('singer.store.RepairLevels', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.RepairLevel',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'addLevel/addLevel',
            read: singer.AppParams.JBOSS_PATH + 'addLevel/getLevels',
            update: singer.AppParams.JBOSS_PATH + 'addLevel/editLevel'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

//            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;
            var id = singer.AppParams.TEMP;
            //console.log(proxyUrl);
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "levelCatId=" + id;

            } else {
                if (proxyUrl.indexOf("levelCatId") === -1) {
                    proxyUrl += "&" + "levelCatId=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "levelCatId=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
            ////console.log(proxyUrl);
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});

