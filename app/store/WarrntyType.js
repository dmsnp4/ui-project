
Ext.define('singer.store.WarrantyType', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    id: 'WarrntyType',
    fields: [
        'code',
        'description'
    ],
    pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getWarrantyType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
    autoLoad: true
});



