


Ext.define('singer.store.UserBusinessMappi', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.UserBusinessMapping',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET',
        },
        api: {
            read: singer.AppParams.JBOSS_PATH + 'Distributors/getDistributors'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});




