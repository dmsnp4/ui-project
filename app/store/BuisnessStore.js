Ext.define('singer.store.BuisnessStore', {
    extend: 'Ext.data.Store',
    model: 'singer.model.BuisnessM',
    pageSize: singer.AppParams.INFINITE_PAGE,
    autoLoad: false,
    treeNode: '',
    loadingSeq: 0,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'BussinessStructures/addBussinesStructure',
            read: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructure',
            update: singer.AppParams.JBOSS_PATH + 'BussinessStructures/editBussinessStructure',
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    folderSort: true,
    sorters: [{
            property: 'text',
            direction: 'ASC'
        }],
    listeners: {
        load: function (store, records) {
            //console.log(records);
            store.loadingSeq = 1 + store.loadingSeq;

            var mxtcat = singer.ParamController.MAXCAT_TYPES;
            var maxChildLevel = mxtcat;

            var map = {};
            var mapTmp = {};
            var tmpCnt = 0;
            for (var i = 0; i < records.length; i++) {
                var obj = records[i].data;
                obj.id = records[i].get('bisId');
                obj.bisId = records[i].get('bisId');
                obj.text = records[i].get('bisName');
                obj.children = [];
                map[records[i].get('bisId')] = obj;

               // console.log(records[i].get('bisId'), " - ParentBisId>>>>>>>>>>>>>>>: ", records[i].get('bisPrtnId'));
                var parent = records[i].get('bisPrtnId') || '-';
                  //console.log("00000000000000 ", parseInt(records[i].get('bisId')), '<' ,parseInt(records[i].get('bisPrtnId')));

//                if(obj.asset_category_type === maxChildLevel){
//                    obj.leaf = true;
//                }
                if (parseInt(records[i].get('bisId')) < parseInt(records[i].get('bisPrtnId'))) {
                    tmpCnt++;
                    mapTmp[tmpCnt] = obj;
                    
                } else {
                    if (!map[parent]) {
                        map[parent] = {
                            children: []
                        };
                        map[parent].children.push(obj);
                    } else {
                        map[parent].children.push(obj);
                    }
                }

            }
            
            console.log(tmpCnt, ' - 11111111111111111: ', mapTmp);
            for(var j=tmpCnt ; j>0 ; j--){
                if(Ext.isDefined(mapTmp[j])){
                    console.log("22222222222222: ", mapTmp[j]);
                    console.log("33333333333333: ", mapTmp[j].bisPrtnId);
                    
                    var parentTmp = mapTmp[j].bisPrtnId || '-';
                    
                   // map[mapTmp[j].bisPrtnId]
                    if (!map[parentTmp]) {
                        map[parentTmp] = {
                            children: []
                        };
                        map[parentTmp].children.push(mapTmp[j]);
                    } else {
                        map[parentTmp].children.push(mapTmp[j]);
                    }

                    

                }
                
            }

            //console.log("000000000000000000000: ", map['-']);
            if (Ext.isDefined(map['-'])) {
                store.treeNode = {text: "All Categories", bisName: 'root', expanded: true, children: map['-'].children, isRoot: true};
            } else {
            }
            ////console.log("category node creating- ------------- ", store.treeNode);
            var treeStore = Ext.getStore('BuisnessTree');
            treeStore.setRootNode(store.treeNode);
            ////console.log(treeStore.getRootNode());
        }




    }
});