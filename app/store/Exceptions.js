Ext.define('singer.store.Exceptions', {
    extend: 'Ext.data.Store',
    model: 'singer.model.Exception',
    pageSize: singer.AppParams.PAGE_SIZE + 5,
    autoLoad: false,
    proxy: {
        enablePaging: true,
        type: 'memory',
        reader: {
            type: 'json',
            totalProperty: 'total',
            root: 'exceptions',
            successProperty: 'success'
        }
    }
});

