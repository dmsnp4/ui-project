

Ext.define('singer.store.BuisnessCategories', {
    extend: 'Ext.data.Store',
    model: 'singer.model.BusinessCategoriesM',
    pageSize: singer.AppParams.INFINITE_PAGE,
    autoLoad: true,
    treeNode: '',
    loadingSeq : 0,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
//            destroy: 'POST'
        },
        api: {
         //   create: singer.AppParams.JBOSS_PATH + 'BussinessStructures/addBussinesStructure',
            read: singer.AppParams.JBOSS_PATH + 'BussinessCategory/getBussinessCategorys',
        //    update: singer.AppParams.JBOSS_PATH + 'putcategory',
         //   destroy: singer.AppParams.JBOSS_PATH + 'deletecategory'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});