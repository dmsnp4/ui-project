Ext.define('singer.store.ServiceUserType', {
    extend: 'Ext.data.Store',
    model: 'singer.model.UserTypeListsM',
    pageSize: singer.AppParams.INFINITE_PAGE,
    autoLoad: false,
    treeNode: '',
    loadingSeq: 0,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST'
        },
        api: {
            read: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructureParticulerBisId'
//            read: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownListWithoutBussCategory',
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    folderSort: true,
    sorters: [{
            property: 'text',
            direction: 'ASC'
        }],
    listeners: {
        load: function (store, records) {
            store.loadingSeq = 1 + store.loadingSeq;
            var mxtcat = singer.ParamController.MAXCAT_TYPES;
            var maxChildLevel = mxtcat;
            var map = {};
            var childMap = [];
            var loggedData = Ext.getStore("LoggedInUserData");
            
//            console.log("logged data ",loggedData);
            
            var curBisId = loggedData.getAt(0).get('extraParams');
            
            var m = 0;
            for (var i = 0; i < records.length; i++) {
                var obj = records[i].data;
                obj.id = records[i].get('bisId');
                obj.bisId = records[i].get('bisId');
                obj.text = records[i].get('bisName');
                obj.children = [];
                map[records[i].get('bisId')] = obj;
                var parent = records[i].get('bisPrtnId') || '-';
                var bisId = records[i].get('bisId');

//                console.log(m++);

                if (parent == 861) {
                    console.log("861 parent detected", records[i].get('bisId'));
                    console.log("861 parent ", parent);
                }

                if (records[i].get('bisId') == 248) {
                    console.log("BIS ID 248 DETECT");
                    console.log("parent ", parent);
                    console.log("parent bug", map[parent]);
                }
                if (records[i].get('bisId') == 10608) {
                    console.log("BIS ID 10608 DETECT");
                    console.log("parent bug", map[parent]);
                }

                if (parent <= bisId || bisId == curBisId) {
                    

                    if (!map[parent]) {

                        map[parent] = {
                            children: []
                        };
                        map[parent].children.push(obj);
                    } else {
                        if (records[i].get('bisId') == 248) {
                            console.log("BIS ID 248 DETECT");
                        }
                        if (records[i].get('bisId') == 10608) {
                            console.log("BIS ID 10608 DETECT");
                        }
                        if (records[i].get('bisId') == 248) {
                            console.log("BIS ID 248 DETECT");
                        }



                        map[parent].children.push(obj);
                    }

                } else {
                    childMap.push(obj);
                }
            }
//            console.log("map test", map[861]);
//            console.log("child map test", childMap);
            
            for(var m =0; m < childMap.length; m++){
                
                var parent2 = childMap[m]['bisPrtnId'];
                
                    if (!map[parent2]) {

                        map[parent2] = {
                            children: []
                        };
                        map[parent2].children.push(childMap[m]);
                    } else {
                        
                        map[parent2].children.push(childMap[m]);
                    }
            }

            if (Ext.isDefined(map['-'])) {
                store.treeNode = {text: "All Categories", bisName: 'root', expanded: true, children: map['-'].children, isRoot: true};
            } else {
            }
            var treeStore = Ext.getStore('UserTypeListTree');
            treeStore.setRootNode(store.treeNode);



        },
        beforeload: function (store, operation, options) {

            var proxyUrl = store.getProxy().api.read;
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
//            var bisId = '222';
//            
//            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructureParticulerBisId?bisId=' + bisId;


            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId;

            } else {
                if (proxyUrl.indexOf("bisId") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});