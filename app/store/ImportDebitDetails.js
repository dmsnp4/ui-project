



Ext.define('singer.store.ImportDebitDetails', {
    extend: 'Ext.data.Store',
    model: 'singer.model.ImportDebitNoteDetailM',
    autoLoad: true,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
           // update: 'POST',
          //  destroy: 'POST'
        },
        api: {
//              read: singer.AppParams.JBOSS_PATH + 'ImportDebitNotes/getImportDebitNoteDetial?debitNoteNo=',
              read: singer.AppParams.JBOSS_PATH + 'ImportDebitNotes/getImportDebitNoteDetial',
            //  update: singer.AppParams.JBOSS_PATH + 'ClearTransit/ClearTransitEdit',
             // create: singer.AppParams.JBOSS_PATH + 'ClearTransit'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    } ,
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;


            var id = singer.AppParams.TEMP;
            ////console.log(id);
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "debitNoteNo=" + id;

            } else {
                if (proxyUrl.indexOf("debitNoteNo") === -1) {
                    proxyUrl += "&" + "debitNoteNo=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "debitNoteNo=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
            ////console.log(proxyUrl);
        },
          load: function (callback) {
            console.log("CCC6 ", callback.totalCount);
            var totalRow = callback.totalCount;
            if (totalRow === 1000000) {
                Ext.Msg.alert("Operation failed", "Cannot connect to the database. Please contact the system Admin");

            }
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});