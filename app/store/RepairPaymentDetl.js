Ext.define('singer.store.RepairPaymentDetl', {
    extend: 'Ext.data.Store',
    model: 'singer.model.WorkOrderPaymentDetail',
//    WorkOrderPaymentDetail
//    autoLoad: true,
    pageSize: 10,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET',
        },
        api: {
//              read: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/getWorkOrderPaymentListNew',
//              read: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/getWorkOrderPaymentLisToGrid',
            read: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/getWorkOrderPaymentGrid'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
//            totalProperty: '1'
        },
        writer: {
            type: 'json',
            allowSingle: true
        },
    },
    listeners: {
        beforeload: function (store, operation, options) {
            var proxyUrl = store.getProxy().api.read;
            var id = singer.AppParams.TEMP;
            console.log("id", id);
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "paymentId=" + id;

            } else {
                if (proxyUrl.indexOf("paymentId") === -1) {
                    proxyUrl += "&" + "paymentId=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "paymentId=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
            console.log("proxyUrl", proxyUrl);
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});