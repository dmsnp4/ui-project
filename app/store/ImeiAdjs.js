Ext.define('singer.store.ImeiAdjs', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.ImeiAdj',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST',
//            destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
//            read: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/getImeiAdjusmentDetails',
            update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});