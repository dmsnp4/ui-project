/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.CreditNote', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.CreditNote',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
//            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'CreditNoteIssues/addCreditNoteIssue',
            read: singer.AppParams.JBOSS_PATH + 'CreditNoteIssues/getCreditNoteIssueDetails',
            update: singer.AppParams.JBOSS_PATH + 'CreditNoteIssues/editCreditNoteIssue'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
// CreditNoteIssues/getCreditNoteDetail?creditNoteIssueNo=1 for array list
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;

            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "serviceBisId=" + bisId;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "serviceBisId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "serviceBisId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


