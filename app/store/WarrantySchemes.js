Ext.define('singer.store.WarrantySchemes', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WarrantyScheme',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/addWarrantySchema',
            read: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantySchemeList',
            update: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/editWarrantySchema'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});

