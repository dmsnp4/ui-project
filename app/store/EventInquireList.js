/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.EventInquireList', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.EventInqiriList',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST'
//            destroy: 'POST'
        },
        api: {
//            create: singer.AppParams.JBOSS_PATH + 'Categories/addCaterory',
            read: singer.AppParams.JBOSS_PATH + 'EventInquires/getEventInqurieList',
//            update: singer.AppParams.JBOSS_PATH + 'Categories/editCategory'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});



