
Ext.define('singer.store.serviceComplain', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrderFeedBack',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET',
        },
        api: {
            read: singer.AppParams.JBOSS_PATH + 'WorkOrderFeedBacks/getWorkOrderFeedbackList',
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});
