


Ext.define('singer.store.WorkOrderHistory', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrderOpen',
    autoLoad: false,
    proxy: {
        type: 'rest',
         actionMethods: {
          //  create: 'PUT',
            read: 'GET',
//            update: 'POST',
          //  destroy: 'POST'
        },
        api: {
             // read: singer.AppParams.JBOSS_PATH + 'ClearTransit/getClearTransitDetails?Status=1',
//              read: singer.AppParams.JBOSS_PATH + 'ClearTransit/getClearTransitList',
              read: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList'
//              update: singer.AppParams.JBOSS_PATH + 'ClearTransit/ClearTransitEdit',
             // create: singer.AppParams.JBOSS_PATH + 'ClearTransit'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
          load: function (callback) {
            console.log("CCC3 ", callback.totalCount);
            var totalRow = callback.totalCount;
            if (totalRow === 1000000) {
                Ext.Msg.alert("Operation failed", "Cannot connect to the database. Please contact the system Admin");

            }
        },
         load: function (callback) {
            console.log("CCC8 ", callback.totalCount);
            var totalRow = callback.totalCount;
            if (totalRow === 1000000) {
                Ext.Msg.alert("Operation failed", "Cannot connect to the database. Please contact the system Admin");

            }
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});






