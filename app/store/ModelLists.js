Ext.define('singer.store.ModelLists', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.ModelList',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
//            create: singer.AppParams.JBOSS_PATH + 'RepairCategories/addRepairCategory',
            read: singer.AppParams.JBOSS_PATH + 'Models/getModelDetails',
            update: singer.AppParams.JBOSS_PATH + 'Models/editModel'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});