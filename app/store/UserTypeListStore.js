Ext.define('singer.store.UserTypeListStore', {
    extend: 'Ext.data.Store',
    model: 'singer.model.UserTypeListsM',
    pageSize: singer.AppParams.INFINITE_PAGE,
    autoLoad: false,
    treeNode: '',
    loadingSeq: 0,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
        },
        api: {
//            read: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructureParticulerBisId?bisId=222',
            read: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructure',
//            read: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownListWithoutBussCategory?bisId=222',

//            create: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBisIdUserList?bisId=94',
//            update: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBisIdUserList?bisId=94',
            //http://192.0.0.59:8080/SingerPortalWebService-1.0/Services/UserTypeLists/getBisIdUserList?bisId=94
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    folderSort: true,
    sorters: [{
            property: 'text',
            direction: 'ASC'
        }],
    listeners: {
        load: function (store, records) {
//            //console.log(records);
            //console.log(store);
            store.loadingSeq = 1 + store.loadingSeq;

            var mxtcat = singer.ParamController.MAXCAT_TYPES;
            var maxChildLevel = mxtcat;

            var map = {};
            for (var i = 0; i < records.length; i++) {
                var obj = records[i].data;

                obj.id = records[i].get('bisId');
                obj.bisId = records[i].get('bisId');
                obj.text = records[i].get('bisName');
                obj.children = [];
                map[records[i].get('bisId')] = obj;
                var parent = records[i].get('bisPrtnId') || '-';

//                if(obj.asset_category_type === maxChildLevel){
//                    obj.leaf = true;
//                }
                if (!map[parent]) {
                    map[parent] = {
                        children: []
                    };
                    map[parent].children.push(obj);
                } else {
                    map[parent].children.push(obj);
                }
            }
            if (Ext.isDefined(map['-'])) {
                store.treeNode = {text: "All Categories", bisName: 'root', expanded: true, children: map['-'].children, isRoot: true};
            } else {
            }
            ////console.log("category node creating- ------------- ", store.treeNode);
            var treeStore = Ext.getStore('UserTypeListTree');
            treeStore.setRootNode(store.treeNode);
//            //console.log(treeStore.nextNode());

//            while(treeStore.nextNode())

            //tree search


        }


    }
});