


Ext.define('singer.store.ImportDebitNotesStore', {
    extend: 'Ext.data.Store',
    model: 'singer.model.ImportDebitNotesModel',
    autoLoad: true,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
           // update: 'POST',
          //  destroy: 'POST'
        },
        api: {
              read: singer.AppParams.JBOSS_PATH + 'ImportDebitNotes/getImportDebitNote',
            //  update: singer.AppParams.JBOSS_PATH + 'ClearTransit/ClearTransitEdit',
             // create: singer.AppParams.JBOSS_PATH + 'ClearTransit'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
      listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;


            var id = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            ////console.log(id);
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + id;

            } else {
                if (proxyUrl.indexOf("bisId") === -1) {
                    proxyUrl += "&" + "bisId=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
            ////console.log(proxyUrl);
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});