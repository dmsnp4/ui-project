Ext.define('singer.store.ShopVisits', {
    extend: 'Ext.data.Store',
    //    model: 'singer.model.LoggedInUser'
    pageSize: singer.AppParams.INFINITE_PAGE,
    fields: [
                'bisId',
                'userId',
                'bisName'
            ],
});