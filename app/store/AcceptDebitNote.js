
Ext.define('singer.store.AcceptDebitNote', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.AcceptDebitM',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
          //  update: 'POST',
          //  destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNote',
       //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
     listeners: {
        beforeload: function (store, operation, options) {
            var proxyUrl = store.getProxy().api.read;
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
         load: function (callback) {
            console.log("CCC3 ", callback.totalCount);
            var totalRow = callback.totalCount;
            if (totalRow === 1000000) {
                Ext.Msg.alert("Operation failed", "Cannot connect to the database. Please contact the system Admin");

            }
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});