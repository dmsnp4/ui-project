Ext.define('singer.store.DSRListStore', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    fields: [
                'bisId',
                'firstName',
                'userId',
                'bisName',
                'teleNumber'
            ],
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST',
//            destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownList',
//            update: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownList'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
     listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;

            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId+ "&bisType=1";

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId+'&bisType=1';

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId+'&bisType=1';
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});