/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.DeviceReturnHeadofficeIMEI', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.DeviceReturnDetailHO',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
                    //            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'DeviceReturns/addDeviceReturnHeadOfficeDtl',
            read: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnListHeadOffice',
            
            update: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturnHeadOfficeDtl'
                    //            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
                    // DeviceReturns/getDeviceReturnDetail?returnNo=1 array list
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;

            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "dsrId=" + bisId;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "dsrId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "dsrId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});