
Ext.define('singer.store.EXGwrkOrderNoStore', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrder',
    autoLoad: true,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST'
//            destroy: 'POST'
        },
        api: {
//            create: singer.AppParams.JBOSS_PATH + 'DeviceIssues/addDeviceIssue',
            read: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/getWorkOrdersToExchange',
//            update: singer.AppParams.JBOSS_PATH + 'DeviceIssues/editDeviceIssue'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
//DeviceIssues/getDeviceIssueDetail?issueNo=1
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;
            console.log(proxyUrl);
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
            
            console.log("ewww", store.getProxy().api.read = proxyUrl);
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});
