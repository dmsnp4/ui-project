/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.MinorDefects', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.MinorDefects',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
//            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'MinorDefects/addMinorDefect',
            read: singer.AppParams.JBOSS_PATH + 'MinorDefects/getMinorDefects',
            update: singer.AppParams.JBOSS_PATH + 'MinorDefects/editMinorDefect'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;


            var id = singer.AppParams.TEMP;
            ////console.log(id);
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "mrjDefectCode=" + id;

            } else {
                if (proxyUrl.indexOf("mrjDefectCode") === -1) {
                    proxyUrl += "&" + "mrjDefectCode=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "mrjDefectCode=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
            ////console.log(proxyUrl);
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});
