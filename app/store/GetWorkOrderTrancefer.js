


Ext.define('singer.store.GetWorkOrderTrancefer', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrderTrancefer',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
          //  update: 'POST',
          //  destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/getWorkOrderTransfer',
       //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {
            
            var proxyUrl = store.getProxy().api.read;
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bis_id=" + bisId;

            } else {
                if (proxyUrl.indexOf("bisId") === -1) {
                    proxyUrl += "&" + "bis_id=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bis_id=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});

