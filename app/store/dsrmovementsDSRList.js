
Ext.define('singer.store.dsrmovementsDSRList', {
    extend: 'Ext.data.Store',
    model: 'singer.model.dsrmovementsMod',
    autoLoad: true,

//     data : [
//        {seqNo: '1', latitite: 7.292547, logtitute: 80.637783},
//        {seqNo: '2', latitite: 7.269390, logtitute: 80.596241},
//        {seqNo: '3', latitite: 7.256448, logtitute: 80.512126},
//        {seqNo: '4', latitite: 7.251339, logtitute: 80.343212},
//        {seqNo: '5', latitite: 7.236694, logtitute: 80.314716},
//        {seqNo: '6', latitite: 7.230564, logtitute: 80.256694},
//        {seqNo: '7', latitite: 7.166868, logtitute: 80.121082},
//        {seqNo: '8', latitite: 7.142342, logtitute: 80.099796},
//        {seqNo: '9', latitite: 7.011851, logtitute: 79.956630},
//        {seqNo: '10', latitite: 6.960735, logtitute: 79.889682},
//        {seqNo: '11', latitite: 6.950255, logtitute: 79.878353},
//        {seqNo: '12', latitite: 6.914896, logtitute: 79.877580}
//     ]
    
    
    
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getTrackingHistoryList',
            //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
            //            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});