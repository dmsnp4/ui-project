/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.store.DeviceIssue', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.DeviceIssue',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
//            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'DeviceIssues/addDeviceIssue',
            read: singer.AppParams.JBOSS_PATH + 'DeviceIssues/getDeviceIssueList',
            update: singer.AppParams.JBOSS_PATH + 'DeviceIssues/editDeviceIssue'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
//DeviceIssues/getDeviceIssueDetail?issueNo=1
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;

            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "distributerID=" + bisId;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "distributerID=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "distributerID=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        load : function(callback){
            console.log("CCC2 ",callback.totalCount)
            var totalRow = callback.totalCount;
            if( totalRow=== 1000000){
                Ext.Msg.alert("Records load fail", "Cannot connect to the database. Please contact the system Admin");

            }
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});
