

Ext.define('singer.store.GetAcceptDebitNoteDetail', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.AcceptDebitM',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            //  create: 'PUT',
            read: 'GET',
            //  update: 'POST',
            //  destroy: 'POST'
        },
        api: {
            //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            //read: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNote',
            read: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNoteDetial',
            //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;


            var id = singer.AppParams.TEMP;
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "debitNoteNo=" + id;

            } else {
                if (proxyUrl.indexOf("debitNoteNo") === -1) {
                    proxyUrl += "&" + "debitNoteNo=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "debitNoteNo=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        load: function (callback) {
            console.log("CCC3 ", callback.totalCount);
            var totalRow = callback.totalCount;
            if (totalRow === 1000000) {
                Ext.Msg.alert("Records load failed", "Cannot connect to the database. Please contact the system Admin");

            }
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});