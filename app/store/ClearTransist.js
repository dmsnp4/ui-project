

Ext.define('singer.store.ClearTransist', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.ClearTransModel',
    autoLoad: false,
    proxy: {
        type: 'rest',
         actionMethods: {
          //  create: 'PUT',
            read: 'GET',
            update: 'POST',
          //  destroy: 'POST'
        },
        api: {
             // read: singer.AppParams.JBOSS_PATH + 'ClearTransit/getClearTransitDetails?Status=1',
              read: singer.AppParams.JBOSS_PATH + 'ClearTransit/getClearTransitList',
              update: singer.AppParams.JBOSS_PATH + 'ClearTransit/ClearTransitEdit',
             // create: singer.AppParams.JBOSS_PATH + 'ClearTransit'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});






