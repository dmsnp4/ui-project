
Ext.define('singer.store.ReplacementReturnStore',{
     extend: 'Ext.data.Store',
     model: 'singer.model.Replacement&ReturnModel',
     autoLoad: true,

//      data:[
//          {rrNo:'123',erNo:'747',IMEIno:'d45',date:'2014/01/01'},
//          {rrNo:'123',erNo:'747',IMEIno:'d45',date:'2014/01/01'},
//          {rrNo:'123',erNo:'747',IMEIno:'d45',date:'2014/01/01'}
//      ]
    proxy: {
        type: 'rest',
         actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
          //  destroy: 'POST'
        },
        api: {
              read: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/getReturnReplacement',
//              update: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/editReplasementReturn',
//              create: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/addReplasementReturn',
             // destroy: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/deleteReplasementReturn'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
//    listeners: {
//        beforeload: function (store, operation, options) {
//
//            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
//            var proxyUrl = store.getProxy().api.read;
//
//
////            var id = singer.AppParams.TEMP;
//            ////console.log(id);
//            if (proxyUrl.indexOf("?") === -1) {
//                proxyUrl += "?" + "status=1" ;
//
//            } else {
//                if (proxyUrl.indexOf("status") === -1) {
//                    proxyUrl += "&" + "status=1" ;
//
//                } else {
//                    proxyUrl = proxyUrl.split("?")[0];
//                    proxyUrl += "?" + "status=1" ;
//                }
//            }
//            store.getProxy().api.read = proxyUrl;
//            ////console.log(proxyUrl);
//        },
//        resetProxy: function (proxyURI) {
//            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
//        }
//    }
});


 