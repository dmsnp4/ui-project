
Ext.define('singer.store.UserTypeListTree', {
    extend: 'Ext.data.TreeStore',
    pageSize: 3,
    realStore: 'UserTypeListStore',
    fields: [
        //added for expand node
        {name: 'id', type: 'int',
//        convert:function(v,rec){
//           return rec.getData().bisId;
//       }
        },
        {name: 'bisId', type: 'int'},
        {name: 'bisDesc', type: 'String'},
        {name: 'bisPrtnId', type: 'int'},
        {name: 'bisUserCount', type: 'int'},
        {name: 'structType', type: 'int'},
        {name: 'category1', type: 'int'},
        {name: 'category2', type: 'int'},
        {name: 'teleNumber', type: 'String'},
        {name: 'address', type: 'String'},
        {name: 'logitude', type: 'String'},
        {name: 'latitude', type: 'String'},
        {name: 'structType', type: 'int'},
        {name: 'mapFlag', type: 'int'},
        {name: 'status', type: 'int'},
        {name: 'user', type: 'String'},
        {name: 'bisName', type: 'String'},
        {name: 'invoiceNo', type: 'string'},
        {name: 'eddsPct', type: 'string'},
        {name: 'essdAcntNo', type: 'string'},
        {name: 'cheqTitle', type: 'string'},
        {name: 'poTitle', type: 'string'},
        {name: 'erpCode', type: 'string'}

    ]
});

//{"address":null,"status":0,"user":null,"structType":0,"bisDesc":"Electric",
//    "teleNumber":null,"category2":0,"latitude":null,"mapFlag":0,"logitude":null,
//    "bisPrtnId":0,"category1":0,"bisId":1,"bisName":"Iron","bisUserCount":8}