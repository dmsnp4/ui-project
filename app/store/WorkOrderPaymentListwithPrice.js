

Ext.define('singer.store.WorkOrderPaymentListwithPrice', {
    extend: 'Ext.data.Store',
    model: 'singer.model.RepairPayment',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
           // update: 'POST',
          //  destroy: 'POST'
        },
        api: {
//              read: singer.AppParams.JBOSS_PATH + 'ImportDebitNotes/getImportDebitNoteDetial?debitNoteNo=',
              read: singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails',
            //  update: singer.AppParams.JBOSS_PATH + 'ClearTransit/ClearTransitEdit',
             // create: singer.AppParams.JBOSS_PATH + 'ClearTransit'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            var proxyUrl = store.getProxy().api.read;
            
            var id = localStorage.getItem('PayApprovePayId');

           // var id = singer.AppParams.TEMP;
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "paymentId=" + id;

            } else {
                if (proxyUrl.indexOf("paymentId") === -1) {
                    proxyUrl += "&" + "paymentId=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "paymentId=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});