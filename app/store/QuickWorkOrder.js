
Ext.define('singer.store.QuickWorkOrder', {
    extend: 'Ext.data.Store',
    model: 'singer.model.QuickWorkOrderM',
    pageSize: singer.AppParams.PAGE_SIZE,
    autoLoad: true,
    loadingSeq : 0,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
//            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'WorkOrders/addQuickWorkOrder',
            read: singer.AppParams.JBOSS_PATH + 'WorkOrders/getQuickWorkOrder',
            update: singer.AppParams.JBOSS_PATH + 'WorkOrders/editQuickWorkOrder'
         //   destroy: singer.AppParams.JBOSS_PATH + 'deletecategory'editQuickWorkOrder
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
     listeners: {
        beforeload: function (store, operation, options) {

            //            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;

            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});


