Ext.define('singer.store.HeadOffliceReturnApproval', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.ImportDebitNoteDetailMHO',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET'
          //  update: 'POST',
          //  destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetailHeadOfficeList'
       //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {
            
            var proxyUrl = store.getProxy().api.read;
            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var returnNumberApproveHo = localStorage.getItem("returnNumberApproveHo");
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "returnNo=" + returnNumberApproveHo;

            } else {
                if (proxyUrl.indexOf("returnNo") === -1) {
                    proxyUrl += "&" + "returnNo=" + returnNumberApproveHo;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "returnNo=" + returnNumberApproveHo;
                }
            }
            store.getProxy().api.read = proxyUrl;
        },
        load : function(){
            
            

        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});

