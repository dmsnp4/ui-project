/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.store.DeviceIssueIme', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.DeviceIssueIme',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
//            create: 'PUT',
            read: 'GET',
//            update: 'POST'
//            destroy: 'POST'
        },
        api: {
//            create: singer.AppParams.JBOSS_PATH + 'DeviceIssues/addDeviceIssue',
            read: singer.AppParams.JBOSS_PATH + 'DeviceIssues/getDeviceIssueDetail',
//            update: singer.AppParams.JBOSS_PATH + 'DeviceIssues/editDeviceIssue'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
//DeviceIssues/getDeviceIssueDetail?issueNo=1
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});
