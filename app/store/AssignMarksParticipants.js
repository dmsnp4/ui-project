/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.AssignMarksParticipants', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.INFINITE_PAGE,
    model: 'singer.model.AssignMarksParticipant',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
          //  update: 'POST',
          //  destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'EventAssignMarks/getEventParticipant'
       //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});

