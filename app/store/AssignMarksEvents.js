/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.AssignMarksEvents', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.AssignMarksEvent',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
          //  create: 'PUT',
            read: 'GET',
          //  update: 'POST',
          //  destroy: 'POST'
        },
        api: {
          //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'EventMasters/getEventMasterList',
       //     update: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

//            var proxyUrl = options.resetProxy(store.getProxy().api.read);
            var proxyUrl = store.getProxy().api.read;
            var id = '3,9';//singer.AppParams.TEMP;
            //console.log(proxyUrl);
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "statusString=" + id;

            } else {
                if (proxyUrl.indexOf("status") === -1) {
                    proxyUrl += "&" + "statusString=" + id;

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "statusString=" + id;
                }
            }
            store.getProxy().api.read = proxyUrl;
            ////console.log(proxyUrl);
        },
        resetProxy: function (proxyURI) {
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});
