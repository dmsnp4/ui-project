
Ext.define('singer.store.Spares', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.Spare',
    autoLoad: false,

    proxy: {
        type: 'rest',
        api: {
//            create: singer.AppParams.JBOSS_PATH + 'Users/addUser',
            read: singer.AppParams.JBOSS_PATH + 'Parts/getPartsDetails',
//            update: singer.AppParams.JBOSS_PATH + 'putuser',
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});





