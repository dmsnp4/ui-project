/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('singer.store.RepairCategories', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.RepairCategory',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'RepairCategories/addRepairCategory',
            read: singer.AppParams.JBOSS_PATH + 'RepairCategories/getRapairCategories',
            update: singer.AppParams.JBOSS_PATH + 'RepairCategories/editRepairCategory'
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});

