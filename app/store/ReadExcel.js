Ext.define('singer.store.ReadExcel',
        {
            extend: 'Ext.data.Store',
            pageSize: singer.AppParams.PAGE_SIZE,
            model: 'singer.model.ReadExcelM',
            autoLoad: false,
            proxy:
                    {
                        type: 'rest',
                        actionMethods:
                                {
                                    read: 'GET',
                                },
                        api:
                                {
                                    read: singer.AppParams.JBOSS_PATH + 'reportService/readReport'
                                },
                        reader:
                                {
                                    type: 'json',
                                    root: 'data',
                                    totalProperty: 'totalRecords'
                                },
                        writer:
                                {
                                    type: 'json',
                                    allowSingle: true
                                }
                    }
        }
);