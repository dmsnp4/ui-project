/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.store.BusinessStructreUsers', {
    extend: 'Ext.data.Store',
//    pageSize: singer.AppParams.PAGE_SIZE,
    pageSize: 999999,
    model: 'singer.model.LoggedInUser',
    autoLoad: false,
    proxy: {
        type: 'rest',
        timeout: 60000,
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: singer.AppParams.JBOSS_PATH + 'Users/addUser'
//            read: singer.AppParams.JBOSS_PATH + 'Users/getUsers',
//            update: singer.AppParams.JBOSS_PATH + 'Users/editUserWithoutGroup',
//            update: singer.AppParams.JBOSS_PATH + 'Users/editUser',
//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {
            Ext.Msg.wait('Please wait', 'Getting user information...', {
                interval: 300
            });
        },
        load: function (callback) {
            Ext.Msg.hide();

        }
    }
//    proxy: {
//        type: 'ajax',
//        url: singer.AppParams.JBOSS_PATH + 'Users/getUsers?userid=all&fname=all&lname=all&comnname=all&degination=all&nic=all&telnum=all&mail=all&regdate=all&inactdate=all&bisname=all&status=0&slimit=1&elimi=999999',
//        reader: {
//            type: 'json',
//            root: 'data',
////            idProperty: 'id',
//            totalProperty: 'totalRecords'
//        }
//    }
});

