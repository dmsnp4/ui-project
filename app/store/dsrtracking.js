
Ext.define('singer.store.dsrtracking', {
    extend: 'Ext.data.Store',
    model: 'singer.model.DSRTrack',
    autoLoad: true,
   
    
    
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            //  create: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            read: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getTrackingEnableList',
            update: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/addEnableTracking'
            //            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});