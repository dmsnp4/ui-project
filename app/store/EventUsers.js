Ext.define('singer.store.EventUsers', {
	extend: 'Ext.data.Store',
	pageSize: singer.AppParams.INFINITE_PAGE,
	model: 'singer.model.EventBussinessList',
	autoLoad: false,
	proxy: {
		type: 'rest',
		actionMethods: {
			create: 'PUT',
			read: 'GET',
			update: 'POST',
			destroy: 'POST'
		},
		api: {
			//            create: singer.AppParams.JBOSS_PATH + 'RepairCategories/addRepairCategory',
			read: singer.AppParams.JBOSS_PATH + 'EventMasters/getBussinessList',
			//            update: singer.AppParams.JBOSS_PATH + 'RepairCategories/editRepairCategory'
			//            destroy: singer.AppParams.JBOSS_PATH + 'deleteuser'
		},
		reader: {
			type: 'json',
			root: 'data',
			totalProperty: 'totalRecords'
		},
		writer: {
			type: 'json',
			allowSingle: true
		}
	}
});