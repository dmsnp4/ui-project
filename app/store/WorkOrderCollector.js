
Ext.define('singer.store.WorkOrderCollector', {
    extend: 'Ext.data.Store',
    model: 'singer.model.QuickWorkOrderM',
    pageSize: singer.AppParams.PAGE_SIZE,
    autoLoad: false,
    loadingSeq : 0,
    proxy: {
        type: 'rest',
        actionMethods: {
            create: 'PUT',
            read: 'GET',
            update: 'POST'
//            destroy: 'POST'
        },
        api: {
           // create: singer.AppParams.JBOSS_PATH + 'WorkOrders/addQuickWorkOrder',
           // read: singer.AppParams.JBOSS_PATH + 'WorkOrders/getQuickWorkOrder',
            read: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderCollectorDetail',
            update: singer.AppParams.JBOSS_PATH + 'WorkOrders/editWorkOrderCollectorDetail'
         //   destroy: singer.AppParams.JBOSS_PATH + 'deletecategory'editQuickWorkOrder
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    }
});





