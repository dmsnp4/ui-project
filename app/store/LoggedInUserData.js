Ext.define('singer.store.LoggedInUserData', {
    extend: 'Ext.data.Store',
    model: 'singer.model.LoggedInUser',
    autoLoad: true
});