Ext.define('singer.store.StockClearanceRepair', {
    extend: 'Ext.data.Store',
    pageSize: singer.AppParams.PAGE_SIZE,
    model: 'singer.model.WorkOrderOpen',
    autoLoad: false,
    proxy: {
        type: 'rest',
        actionMethods: {
            read: 'GET'
        },
        api: {
            read: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderListForClearance'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRecords'
        },
        writer: {
            type: 'json',
            allowSingle: true
        }
    },
    listeners: {
        beforeload: function (store, operation, options) {

            var proxyUrl = store.getProxy().api.read;

            var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
            
            var id = '1';
            if (proxyUrl.indexOf("?") === -1) {
                proxyUrl += "?" + "bisId=" + bisId +'&paymentType=4&workOrderStatus=1';

            } else {
                if (proxyUrl.indexOf("bisId") === -1) {
                    proxyUrl += "&" + "bisId=" + bisId+"&workOrderStatus=1";

                } else {
                    proxyUrl = proxyUrl.split("?")[0];
                    proxyUrl += "?" + "bisId=" + bisId+"&workOrderStatus=1";
                }
                
                if (proxyUrl.indexOf("paymentType") === -1) {
                    proxyUrl += "&" + "paymentType=4";

                } 
            }
            store.getProxy().api.read = proxyUrl;
        },
        resetProxy: function (proxyURI) {
            //console.log
            return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
        }
    }
});

