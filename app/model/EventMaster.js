Ext.define('singer.model.EventMaster', {
	extend: 'Ext.data.Model',
	fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
		{
			name: 'eventId',
			type: 'int'
		},
		{
			name: 'eventName',
			type: 'string'
		},
		{
			name: 'eventDesc',
			type: 'string'
		},
		{
			name: 'startDate',
			type: 'string'
		},
		{
			name: 'endDate',
			type: 'string'
		},
		{
			name: 'threshold',
			type: 'string'
		},
		{
			name: 'status',
			type: 'int'
		},
		{
			name: 'statusMsg',
			type: 'string'
		},
		{
			name: 'eventMrgComment',
			type: 'string'
		},
		{
			name: 'mktMrgComment',
			type: 'string'
		},
		{
			name: 'evenAwardList',
			defaultValue: []
		},
		{
			name: 'eventNonSalesRuleList',
			defaultValue: []
		},
		{
			name: 'eventSalesRuleList',
			defaultValue: []
		},
		{
			name: 'eventBisTypeList',
			defaultValue: []
		}
    ]
});