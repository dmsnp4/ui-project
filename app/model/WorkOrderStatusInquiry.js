


Ext.define('singer.model.WorkOrderStatusInquiry', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'workOrderNo', type: 'string'},
        {name: 'customerName', type: 'string'},
        {name: 'customerAddress', type: 'string'},
        {name: 'customerNic', type: 'string'},
        {name: 'customerTelephoneNo', type: 'string'},
        {name: 'email', type: 'string'},
        {name: 'imeiNo', type: 'string'},
        {name: 'product', type: 'string'},
        {name: 'brand', type: 'string'},
        {name: 'dateOfSale', type: 'string'},
        {name: 'warrantyType', type: 'string'},
        {name: 'technician', type: 'string'},
        {name: 'deliveryType', type: 'string'},
        {name: 'transferLocation', type: 'string'},
        {name: 'deliveryDate', type: 'string'},
        {name: 'estClosingDate', type: 'string'},
        {name: 'repairAmount'},
        {name: 'repairLevelId'},
        {name: 'repairStatus'},
        
        {name: 'Part', type: 'auto', defaultValue : []},
        {name: 'WorkOrderDefect', type: 'auto', defaultValue : []},
        {name: 'WorkOrderEstimate', type: 'auto', defaultValue : []}
    ]
});

//      workOrderNo;
//      customerName;
//      customerAddress;
//      customerNic;
//      customerTelephoneNo;
//      email;
//      imeiNo;
//      product;
//      brand;
//      dateOfSale;
//      warrantyType;
//      technician;
//      deliveryType;
//      transferLocation;
//      deliveryDate;
//      estClosingDate;
//      ArrayList<Part> partList;
//      ArrayList<WorkOrderDefect> woDefect;