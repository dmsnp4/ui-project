Ext.define('singer.model.ShopVistCheckMDef', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'shopVistId',
            type: 'int'
        },
        {
            name: 'dSR',
            type: 'int'
        },
        {
            name: 'longitude',
            type: 'string'
        },
        {
            name: 'latitude',
            type: 'string'
        },
        {
            name: 'visitDate',
            type: 'string'
        },
        {
            name: 'subDealer',
            type: 'int'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'userId',
            type: 'string'
        },
        {
            name: 'firstName',
            type: 'string'
        },
        {
            name: 'lastName',
            type: 'string'
        },
        {
            name:'bisName',
            type:'string'
        },
         {
            name:'contactNo',
            type:'string'
        },
        {
            name:'shopVisitItemList'
        },{
            name:'bisDesc',
            type:'string'
        },{
            name:'address',
            type:'string'
        },{
            name:'teleNumber',
            type:'string'
        },
    ]
});