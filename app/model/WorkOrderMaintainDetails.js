
Ext.define('singer.model.WorkOrderMaintainDetails', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'wrokOrderNo', type: 'String'},
        {name: 'seqNo', type: 'int'},
        {name: 'partNo', type: 'int'},
        {name: 'partDesc', type: 'String'},
        {name: 'selliingPrice', type: 'double'},
        {name: 'invoiceNo', type: 'String'},
        {name: 'thirdPartyFlag', type: 'int'},
        {name: 'status', type: 'int'}
    ]
});

//class WorkOrderMaintainDetail {
//private String wrokOrderNo;
//private int seqNo;
//private int partNo;
//private String partDesc;
//private double selliingPrice;
//private String invoiceNo;
//private int thirdPartyFlag;
//private int status;
//}