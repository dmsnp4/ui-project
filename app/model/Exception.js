Ext.define('singer.model.Exception', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'store', type :'string'},
        {name: 'detail', type :'string'}
    ]
});



