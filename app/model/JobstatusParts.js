
Ext.define('singer.model.JobstatusParts', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'partNo'},
        {name: 'partDesc'},
        {name: 'partExist'},
        {name: 'partPrdCode'},
        {name: 'partPrdDesc'},
        {name: 'partPrdFamily'},
        {name: 'partPrdFamilyDesc'},
        {name: 'partGroup1'},
        {name: 'partGroup2'},
        {name: 'partSellPrice'},
        {name: 'partCost'}
    ]
});


//      partNo;
//      partDesc;
//      partExist;
//      partPrdCode;
//      partPrdDesc;
//      partPrdFamily;
//      partPrdFamilyDesc;
//      partGroup1;
//      partGroup2;
//      partSellPrice;
//      partCost;