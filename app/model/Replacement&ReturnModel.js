
Ext.define('singer.model.Replacement&ReturnModel', {
    extend: 'Ext.data.Model',
      fields:[
          {name: 'replRetutnNo', type: 'int'},
          {name: 'exchangeReferenceNo', type: 'String'},
          {name: 'imeiNo', type: 'String'},
          {name: 'returnDate', type: 'String'},
          {name: 'distributerId', type: 'int'},
          {name: 'status', type: 'int'},
          {name: 'user', type: 'String'},
          {name: 'modleDesc', type: 'String'},
      ]

    
});

//    private int replRetutnNo;
//    private String exchangeReferenceNo;
//    private int distributerId;
//    private String imeiNo;
//    private String returnDate;
//    private int status;
//    private String user;
//    private String modleDesc;

