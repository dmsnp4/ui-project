
Ext.define('singer.model.IMEImaintenanceM', {
    extend: 'Ext.data.Model',
    fields: [
    	 {name: 'imeiNo', type: 'String'},
         {name: 'modleNo', type: 'int'},
         {name: 'bisId', type: 'int'},
         {name: 'debitnoteNo', type: 'String'},
         {name: 'purchaseDate', type: 'String'},
         {name: 'brand', type: 'String'},
         {name: 'product', type: 'String'},
         {name: 'location', type: 'String'},
         {name: 'salesPrice', type: 'double'},
         {name: 'status', type: 'int'},
         {name: 'user', type: 'String'},
         {name: 'bisName', type: 'String'},
         {name: 'modleDesc', type: 'String'},
         {name: 'erpCode', type: 'String'}
    ]
});

////class ImeiMaster {
//            String imeiNo;
//            int modleNo;
//            int bisId;
//            String debitnoteNo;
//            String purchaseDate;
//            String brand;
//            String product;
//            String location;
//            double salesPrice;
//            int status;
//            String user;
//}
