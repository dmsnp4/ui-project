
Ext.define('singer.model.UserFunctionModel', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'functionId', type: 'String'},
       {name: 'functionName', type: 'String'},
       {name: 'menuHtmlId', type: 'String'}, 
       {name: 'menuAction', type: 'String'},
       {name: 'menuUi', type: 'String'},
       {name: 'menuIcon', type: 'String'}, 
       {name: 'parentDN', type: 'String'},
       
       'actions',
       'functions'
    ]
});



