/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.model.EventMark', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'txnId', type: 'int'},
        {name: 'eventId', type: 'int'},
        {name: 'userId', type: 'string'},
        {name: 'assignTo', type: 'string'},
        {
            name: 'pointAddDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
                if (Ext.isEmpty(v) || v === '0000-00-00') {
                    return '';
                } else {
                    return Ext.Date.format(new Date(v), 'Y-m-d');
                }
            }
        },
        {name: 'ruleId', type: 'int'},
        {name: 'salePoint', type: 'double'},
        {name: 'nonSaleId', type: 'int'},
        {name: 'nonSalePoint', type: 'double'},
        {name: 'assignId', type: 'int'},
        {name: 'assignBy', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'statusMsg', type: 'string'},
        {name: 'remarks', type: 'string'},
        {name: 'salesType', type: 'int'}
    ]
});
