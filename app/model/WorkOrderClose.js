
Ext.define('singer.model.WorkOrderClose', {
    extend: 'Ext.data.Model',
    fields: [
    	 {name: 'workOrderNo', type: 'String'},
         {name: 'repairid', type: 'int'},
         {name: 'repairSatus', type: 'String'},
         {name: 'deleveryId', type: 'int'},
         {name: 'deleveryType', type: 'String'},
         {name: 'locationId', type: 'int'},
         {name: 'transferLocation', type: 'String'},
         {name: 'courierNo', type: 'String'},
         {name: 'deleverDate', type: 'String'},
         {name: 'status', type: 'int'},
         {name: 'gatePass', type: 'String'},
         {name: 'remarks', type: 'String'},
    ]
});

//class WorkOrderClose {
//    
//    private String workOrderNo;
//    private int repairid;
//    private String repairSatus;
//    private int deleveryId;
//    private String deleveryType;
//    private int locationId;
//    private String transferLocation;
//    private String courierNo;
//    private String deleverDate;
//    private int status;
//    private String gatePass;
//}
