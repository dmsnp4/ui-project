

Ext.define('singer.model.QuickWorkOrderM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'imeiNo', type: 'string'},
        {name: 'product', type: 'string'},
        {name: 'modleNo', type: 'int'},
        {name: 'modleDesc', type: 'string'},
        
        {name: 'customerNic', type: 'string'},
        {name: 'customerName', type: 'string'},
        {name: 'workTelephoneNo', type: 'string'},
        ///
//        {name: 'poDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
//                if (Ext.isEmpty(v) || v === '0000-00-00') {
//                    return '';
//                } else {
//                    return Ext.Date.format(new Date(v), 'Y-m-d');
//                }
//            }},
        {name: 'email', type: 'string'},
        {name: 'defectNo', type: 'string'},
        {name: 'assessoriesRetained', type: 'string'},
        {name: 'workOrderNo', type: 'string'},
        {name: 'transferLocation', type: 'int'},
        {name: 'bisId', type: 'int'},
        {name: 'remarks', type: 'string'},
        {name: 'serviceCenter', type: 'string'},
        {name: 'serviceCenterAddress', type: 'string'},
        {name: 'bisAddress', type: 'string'},
        {name: 'bisName', type: 'string'},
        {name: 'serviceCenterId', type: 'int'},
        
        {name: 'contactNo', type: 'string'},
        {name: 'collectorNo', type: 'string'},
        {name: 'podNo', type: 'string'},
        {name: 'collectorName', type: 'string'},
        {name: 'collectorNic', type: 'string'}
    ]
});





