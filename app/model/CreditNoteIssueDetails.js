
Ext.define('singer.model.CreditNoteIssueDetails', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'crNoteissueNo', type: 'int'},
        {name: 'imeiNo', type: 'string'},
        {name: 'modleNo', type: 'int'},
        {name: 'seqNo', type: 'int'},
        {name: 'status', type: 'int'},
        {name: 'user', type: 'string'},
        {name: 'salesPrice', type: 'number'},
        {name: 'modleDesc', type: 'string'},
    ]
});