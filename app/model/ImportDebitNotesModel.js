
Ext.define('singer.model.ImportDebitNotesModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'debitNoteNo', type: 'string'},
        {name: 'orderNo', type: 'string'},
        {name: 'invoiceNo', type: 'string'},
        {name: 'contactNo', type: 'string'},
        {name: 'partNo', type: 'string'},
        {name: 'orderQty', type: 'int'},
        {name: 'deleveryQty', type: 'int'},
        {name: 'deleveryDate', type: 'string'},
        {name: 'headStatus', type: 'int'},
        {name: 'customerNo', type: 'string'},
        {name: 'siteDescription', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'user', type: 'string'},
//        {name: 'ImportDebitNoteDetails', defaultValue: []},
        {name: 'importDebitList', defaultValue: []}
    ]
});

//getImportDebitNote(
//              String debitNoteNo,
//             String orderNo,
//             String invoiceNo, 
//            String contactNo,
//            String partNo,
//            int orderQty,
//            int deleveryQty,
//            String deleveryDate,
//            int headStatus,
//            String customerNo,
//            String order,
//            String type,
//            int start,
//            int limt)
//
