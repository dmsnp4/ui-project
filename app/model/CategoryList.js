Ext.define('singer.model.CategoryList', {
    extend: 'Ext.data.Model',
//    fields: [
//        {name: 'UserId', type: 'string'},
//        {name: 'Password', type: 'string'},
//        {name: 'FName', type: 'string'},
//        {name: 'LName', type: 'string'},
//        {name: 'Designation', type: 'string'},
//        {name: 'NIC', type: 'string'},
//        {name: 'TelePhone', type: 'string'},
//        {name: 'EMail', type: 'string'},
//        {name: 'Status', type: 'int'},
//        {name: 'RegDate', type: 'string'},
//        {name: 'InctiveDate', type: 'string'},
//        {name: 'LastLogDate', type: 'string'},
//        {name: 'ReturnMsg', type: 'string'},
//        {name: 'RetunFlag', type: 'string'},
//        {name: 'BisName', type: 'string'},
//        {name: 'User', type: 'string'}
//    ]

    fields: [
        {name: 'cateId', type: 'int'},
        {name: 'cateName', type: 'string'},
        {name: 'cateDesc', type: 'string'},
        {name: 'cateStatus', type: 'int'},
        {name: 'remarks', type: 'string'}
        
    ]
});