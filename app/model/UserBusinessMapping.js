
Ext.define('singer.model.UserBusinessMapping', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'customerNo', type: 'String'},
       {name: 'bisId', type: 'int'},
       {name: 'bisName', type: 'String'},
       {name: 'status', type: 'int'},
       {name: 'seqNo', type: 'int'}
    ]
});


//private String customerNo;
//    private int bisId;
//    private String bisName;
//    private int status;
