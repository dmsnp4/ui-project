Ext.define('singer.model.ImportDebitNoteDetailReturn', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'debitNoteNo',
            type: 'String'
        },
        {
            name: 'imeiNo',
            type: 'String'
        },
        {
            name: 'customerCode',
            type: 'String'
        },
        {
            name: 'shopCode',
            type: 'String'
        },
        {
            name: 'modleNo',
            type: 'int'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'orderNo',
            type: 'String'
        },
        {
            name: 'salesPrice',
            type: 'double'
        },
        {
            name: 'dateAccepted',
            type: 'String'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'remarks',
            type: 'String'
        },
        {
            name: 'site',
            type: 'String'
        },
        {
            name: 'modleDesc',
            type: 'string'
        }
 //         {name: 'start', type: 'int'},
 //         {name: 'limt', type: 'int'},
    ]
});


//            String debitNoteNo,
//            String imeiNo,
//            String customerCode, 
//            String shopCode,
//            int modleNo,
//            int bisId,
//            String orderNo,
//            double salerPrice,
//            String dateAccepted,
//            int status,
//            String remarks, 
//            String site,
//            String order,
//            String type,