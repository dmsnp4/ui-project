Ext.define('singer.model.WinGrid', {
    extend: 'Ext.data.Model',
    fields: [

        {
            name: 'imeiNo',
            type: 'string'
        },
        {
            name: 'modleNo',
            type: 'int'
        },
        {
            name: 'salesPrice',
            type: 'number'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'margin',
            type: 'number'
        },
        {
            name: 'modleDesc',
            type: 'string'
        }
    ]
});