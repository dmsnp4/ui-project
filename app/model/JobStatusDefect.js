

Ext.define('singer.model.JobStatusDefect', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'woDefectId'},
        {name: 'woMaintainId'},
        {name: 'majorCode'},
        {name: 'minorCode'},
        {name: 'remarks'},
        {name: 'status'},
        {name: 'majDesc'},
        {name: 'minDesc'}
    ]
});

//woDefectId;
//     woMaintainId;
//     majorCode;
//     minorCode;
//     remarks;
//     status;
//     majDesc;
//     minDesc;