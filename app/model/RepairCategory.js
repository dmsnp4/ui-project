
Ext.define('singer.model.RepairCategory', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'reCateId', type: 'int'},
        {name: 'reCateDesc', type: 'string'},
        {name: 'reCateStatus', type: 'int'}
    ]
});
