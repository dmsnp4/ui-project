
Ext.define('singer.model.ModelList', {
    extend: 'Ext.data.Model',
    fields: [
//        {name: 'reCateId', type: 'int'},
//        {name: 'reCateDesc', type: 'string'},
//        {name: 'reCateStatus', type: 'int'}
        'status',
        {name: 'cost_Price', type: 'number'},
        'wrn_Scheme',
        'model_No',
        'wrn_Scheme_id',
        'rep_Category_id',
        'model_Description',
        'part_Prd_Code',
        'art_Prd_Code_Desc',
        'part_Prd_Family',
        'commodity_Group_1',
        'commodity_Group_2',
        'rep_Category',
        'erp_part',
        {name: 'selling_Price', type: 'number'},
        {name: 'dealer_Margin', type: 'number'},
        {name: 'distributor_Margin', type: 'number'},
        'part_Prd_Family_Desc',
        'art_Prd_Code_Desc',
        {name:'altPartsExist', type:'int'},
        {name:'altPartsExistDesc', type:'string'}
    ]
});
