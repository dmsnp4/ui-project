
Ext.define('singer.model.evenAwardListMod', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'award', type: 'auto'},
       {name: 'awardId', type: 'int'},
       {name: 'eventid', type: 'int'},
       {name: 'palce', type: 'string'},
       {name: 'status', type: 'int'}
    ]
});

