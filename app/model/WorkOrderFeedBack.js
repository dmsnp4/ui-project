


Ext.define('singer.model.WorkOrderFeedBack', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'workOrderNo', type: 'String'},
        {name: 'feedBackId', type: 'int'},
        {name: 'feedBackType', type: 'int'},
        {name: 'feedBack', type: 'String'},
        {name: 'prduct', type: 'String'},
        {name: 'modelName', type: 'String'},
        {name: 'feedbackTypeDesc', type: 'String'},
        {name: 'user', type: 'String'},
        {name: 'complainDate', type: 'String'},
        {name: 'purchaseDate', type: 'String'},
        {name: 'purchaseShop', type: 'String'},
        {name: 'serviceCenter', type: 'String'},
        {name: 'woOpenDate', type: 'String'},
        {name: 'deleveryDate', type: 'String'},
        {name: 'status', type: 'int'},
        {name: 'customerName', type: 'String'},
        {name: 'telephoneNo', type: 'String'},
        {name: 'address', type: 'String'},
        {name: 'imeiNo', type: 'String'},
        {name: 'email', type: 'String'},
        {name: 'customerNic', type: 'String'},
        {name: 'warrenty', type: 'String'},
        {name: 'rccRefe', type: 'String'},
        {name: 'replyStatus', type: 'String'},
    ]
});
//private String workOrderNo;
//    private int feedBackId;
//    private int feedBackType;
//    private String feedBack;
//    private String prduct;
//    private String modelName;
//    private String feedbackTypeDesc;
//    private String user;
//    private String complainDate;
//    private String purchaseDate;
//    private String purchaseShop;
//    private String serviceCenter;
//    private String woOpenDate;
//    private String deleveryDate;
//    private int status;
//    private String customerName;
//    private String telephoneNo;
//    private String address;
//    private String imeiNo;
//    private String email;
//    private String customerNic;
//    private String warrenty;
//    private String rccRefe;
