
Ext.define('singer.model.EventInqiriList', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'eventId', type: 'int'},
       {name: 'eventName', type: 'String'},
       {name: 'eventDesc', type: 'String'},
       {name: 'startDate', type: 'String'},
       {name: 'endDate', type: 'String'},
       {name: 'threshold', type: 'String'},
       {name: 'status', type: 'int'},
       {name: 'statusMsg', type: 'String'},
       {name: 'eventMrgComment', type: 'String'},
       {name: 'mktMrgComment', type: 'String'},
       {name: 'eventAchievement', type: 'String'},
       {name: 'evenAwardList'},
       {name: 'eventNonSalesRuleList',type: 'auto'},
       {name: 'eventSalesRuleList',type: 'auto'},
       {name: 'eventBisTypeList'},
    ]
});


//{"eventId":1,
//"eventName":"Sample Event",
//"eventDesc":"This is Event",
//"startDate":"2014/11/01"
//,"endDate":"2014/11/30",
//"threshold":"20",
//"status":1,
//"statusMsg":"Planning",
//"eventMrgComment":null,
//"mktMrgComment":null,
//"eventAchievement":"0",
//"evenAwardList":null,
//"eventNonSalesRuleList":null
//,"eventSalesRuleList":null,
//"eventBisTypeList":nul
//l}
//
//
//
//getEventInqurieList(
//              int eventId,
//             String eventName,
//             String eventDesc,
//             String startDate,
//             String endDate,
//             String threshold,
//             String eventAchievement,
//             String order,
//             String type,
//             int start,
//             int limt)