

Ext.define('singer.model.RepairPaymentSendMod', {
    extend: 'Ext.data.Model',
    fields: [
         {name: 'paymentId', type: 'int'},
         {name: 'amount', type: 'float'},
         {name: 'nbtValue', type: 'float'},
         {name: 'vatValue', type: 'float'},
         {name: 'essdAccountNo', type: 'String'},
         {name: 'paymentDate', type: 'String'},
         {name: 'status', type: 'int'},
//         {name: 'payBisId', type: 'int'},
    ]
});


//            String workOrderNo,
//            String imeiNo,
//            String warrantyVerifType,
//            String defectDesc,
//            int status,
//            String order,
//            String type,

