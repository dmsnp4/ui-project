/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.model.Spare', {
    extend: 'Ext.data.Model',
    fields: [
        'erpPartNo',
        'partNo',
        'partDesc',
        'partExist',
        'partPrdCode',
        'partPrdDesc',
        'partPrdFamily',
        'partPrdFamilyDesc',
        'partGroup1',
        'partGroup2',
        'partSellPrice',
        'partCost',
        'status'
    ]
});


//    private int partNo;
//    private String partDesc;
//    private int partExist;
//    private String partPrdCode;
//    private String partPrdDesc;
//    private String partPrdFamily;
//    private String partPrdFamilyDesc;
//    private String partGroup1;
//    private String partGroup2;
//    private double partSellPrice;
//    private double partCost;
//    private String erpPartNo;
//    private int status;