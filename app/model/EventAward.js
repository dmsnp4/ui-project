Ext.define('singer.model.EventAward', {
	extend: 'Ext.data.Model',
	fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
		{
			name: 'awardId',
			type: 'int'
		},
		{
			name: 'eventid',
			type: 'int'
		},
		{
			name: 'palce',
			type: 'string'
		},
		{
			name: 'award',
			type: 'string'
		},
		{
			name: 'status',
			type: 'int'
		}
    ]
});