
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



Ext.define('singer.model.DeviceIssueIme', {
    extend: 'Ext.data.Model',
    
    fields:[
        {
                name: 'bisId',
                type: 'int'
        },
        {
                name: 'bisName',
                type: 'string'
        },
        {
                name: 'brand',
                type: 'string'
        },
        {
                name: 'debitnoteNo',
                type: 'string'
        },
        {
                name: 'imeiNo',
                type: 'string'
        },
        {
                name: 'modleNo',
                type: 'int'
        },
        {
                name: 'salesPrice',
                type: 'number'
        },
        {
                name: 'status',
                type: 'int'
        },
        {
                name: 'margin',
                type: 'number'
        },
        {
            name:'modleDesc',
            type:'string'
        }
    ]
});