
Ext.define('singer.model.eventNonSalesRuleListMod', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'description', type: 'String'},
       {name: 'eventId', type: 'int'},
       {name: 'nonSalesRule', type: 'int'},
       {name: 'point', type: 'int'},
       {name: 'salesType', type: 'int'},
       {name: 'status', type: 'int'},
       {name: 'taget', type: 'int'}
    ]
});

