Ext.define('singer.model.DeviceReturnDetail', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'seqNo',
            type: 'int'
        },
        {
            name: 'returnNo',
            type: 'int'
        },
        {
            name: 'imeiNo',
            type: 'string'
        },
        {
            name: 'modleNo',
            type: 'int'
        },
        {
            name: 'modleDesc',
            type: 'string'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'user',
            type: 'string'
        },
        {
            name: 'salesPrice',
            type: 'int'
        },
        {
            name: 'reason',
            type: 'string'
        }
    ]
});