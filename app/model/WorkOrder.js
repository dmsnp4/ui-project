
Ext.define('singer.model.WorkOrder', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'workOrderNo', type: 'string'},
        {name: 'customerName', type: 'string'},
        {name: 'customerAddress', type: 'string'},
        {name: 'workTelephoneNo', type: 'string'},
        {name: 'email', type: 'string'},
        {name: 'customerNic', type: 'string'},
        {name: 'imeiNo', type: 'string'},
        {name: 'product', type: 'string'},
        {name: 'brand', type: 'string'},
        {name: 'modleNo', type: 'int'},
        {name: 'assessoriesRetained', type: 'string'},
        {name: 'defectNo', type: 'string'},
        {name: 'rccReference', type: 'string'},
        {name: 'dateOfSale', type: 'string'},
        {name: 'proofOfPurches', type: 'string'},
        {name: 'bisId', type: 'int'},
        {name: 'schemeId', type: 'int'},
        {name: 'workOrderStatus', type: 'int'},
        {name: 'warrentyVrifType', type: 'int'},
        {name: 'nonWarrentyVrifType', type: 'int'},
        {name: 'tecnician', type: 'string'},
        {name: 'actionTaken', type: 'string'},
        {name: 'levelId', type: 'int'},
        {name: 'remarks', type: 'string'},
        {name: 'repairStatus', type: 'int'},
        {name: 'deleveryType', type: 'int'},
        {name: 'transferLocation', type: 'int'},
        {name: 'courierNo', type: 'string'},
        {name: 'deleveryDate', type: 'string'},
        {name: 'gatePass', type: ''},
        {name: 'status', type: 'int'},
        {name: 'statusMsg', type: 'string'},
        {name: 'createDate', type: 'string'},
        {name: 'delayDate', type: 'string'},
        {name: 'repairDate', type: 'string'},
        {name: 'defectDesc', type: 'string'},
        {name: 'minorDefectNo', type: 'int'},
        {name: 'minorDefectDesc', type: 'string'},
        {name: 'repairLevelDesc', type: 'string'},
        {name: 'saleDate', type: 'string'},
        {name: 'warrantyStatus', type: 'int'},
        {name: 'warrantyStatusMsg', type: 'string'},
        {name: 'paymentStatus', type: 'int'},
        {name: 'mjrDefectDesc', type: 'string'},
        {name: 'minDefecDesc', type: 'string'},
        {name: 'modleDesc', type: 'string'},
        {name: 'levelName', type: 'string'},
        {name: 'binName', type: 'string'},
        {name: 'estimateRefNo', type: 'string'},
        {name: 'estimateStatus', type: 'int'},
        {name: 'estimateStatusMsg', type: 'string'},
        {name: 'distShop', type: 'string'},
        {name: 'remark2', type: 'string'},
        ///=--------------------
        {name: 'repairPayment', type: 'int'},
        {name: 'repairPaymentStatusMsg', type: 'string'},
        {name: 'nonWarrantyRemarks', type: 'string'},
        {name: 'customerComplain', type: 'string'},
        {name: 'serviceBisId', type: 'int'},
        {name: 'serviceBisDesc', type: 'string'},
//        {name:'registerDate', type:'string'},

        {name: 'woDetials', type: 'auto'},
        {name: 'woMaintainDetials', defaultValue: []},
        {name: 'woDefect', defaultValue: []},
        {name: 'paymentType', type: 'int'},
        {name: 'expireStatus', type: 'int'}

    ]
});

//class WorkOrder {
//





//
