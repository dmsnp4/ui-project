
Ext.define('singer.model.EventInquirePart', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'eventId', type: 'int'},
       {name: 'paticipantName', type: 'String'},
       {name: 'location', type: 'String'},
       {name: 'paticipantType', type: 'String'},
       {name: 'achivePoint', type: 'String'},
       {name: 'posotion', type: 'String'},
       {name: 'order', type: 'String'},
       {name: 'type', type: 'String'},
    ]
});


//{"eventId":1,
//"eventName":"Sample Event",
//"eventDesc":"This is Event",
//"startDate":"2014/11/01"
//,"endDate":"2014/11/30",
//"threshold":"20",
//"status":1,
//"statusMsg":"Planning",
//"eventMrgComment":null,
//"mktMrgComment":null,
//"eventAchievement":"0",
//"evenAwardList":null,
//"eventNonSalesRuleList":null
//,"eventSalesRuleList":null,
//"eventBisTypeList":nul
//l}
//
//
//
//getEventInqurieList(
//              int eventId,
//             String eventName,
//             String eventDesc,
//             String startDate,
//             String endDate,
//             String threshold,
//             String eventAchievement,
//             String order,
//             String type,
//             int start,
//             int limt)