Ext.define('singer.model.WorkOrderDefect', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'woDefectId',
            type: 'int'
        },
        {
            name: 'woMaintainId',
            type: 'int'
        },
        {
            name: 'majorCode',
            type: 'int'
        },
        {
            name: 'minorCode',
            type: 'int'
        },
        {
            name: 'remarks',
            type: 'string'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name:'majDesc',
            type:'string'
        },
        {
            name:'minDesc',
            type:'string'
        }
    ]
});