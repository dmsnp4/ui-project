Ext.define('singer.model.WarrentyRegistration', {
    extend: 'Ext.data.Model',
    fields: [
        //        {name: 'extraParams', defaultValue: null, type: 'int'},
        {
            name: 'warrntyRegRefNo',
            type: 'int'
        },
        {
            name: 'imeiNo',
            type: 'string'
        },
        {
            name: 'customerName',
            type: 'string'
        },
        {
            name: 'customerNIC',
            type: 'string'
        },
        {
            name: 'contactNo',
            type: 'string'
        },
        {
            name: 'dateOfBirth',
            type: 'string'
        },
        {
            name: 'sellingPrice',
            type: 'number'
        },
        {
            name: 'modleNo',
            type: 'int;'
        },
        {
            name: 'modleDescription',
            type: 'string'
        },
        {
            name: 'product',
            type: 'string'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'registerDate',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'customerCode',
            type: 'string'
        },
        {
            name: 'shopCode',
            type: 'string'
        },
        {
            name: 'siteDesc',
            type: 'string'
        },
        {
            name: 'orderNo',
            type: 'string'
        },
        {
            name: 'proofPurchas',
            type: 'string'
        },
        {
            name: 'warantyType',
            type: 'int'
        },
        {
            name: 'customerAddress',
            type: 'String'
        }

    ]
});