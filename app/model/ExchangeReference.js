Ext.define('singer.model.ExchangeReference', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'exchangeReferenceNo', type: 'string'},
        {name: 'workOrderNo', type: 'string'},
        {name: 'imeiNo', type: 'string'},
        {name: 'reason', type: 'int'},
        {name: 'typeOfExchange', type: 'int'},
        {name: 'bisId', type: 'int'},
        {name: 'status', type: 'int'},
        {name: 'remarks', type: 'string'},
        {name: 'customerNIC', type: 'string'},
        {name: 'customerName', type: 'string'},
        {name: 'warrantyRegDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
                if (Ext.isEmpty(v) || v === '0000-00-00') {
                    return '';
                } else {
                    return Ext.Date.format(new Date(v), 'Y-m-d');
                }
            }
        },
        {name: 'warrantyStatus', type: 'string'},
        {name: 'product', type: 'string'},
        {name: 'brand', type: 'string'},
        {name: 'erpCode', type: 'string'},
        {name: 'modelDesc', type: 'string'},
        {name: 'wrtPeriodFlag', type: 'int'},
        {name: 'wrtImeiFlag', type: 'int'},
        {name: 'wrtDamageFlag', type: 'int'},
        {name: 'unAuthRepairFlag', type: 'int'},
        {name: 'waterDamage', type: 'int'},
        {name: 'reprodicbleFlag', type: 'int'},
        {name: 'reasonDesc', type: 'string'},
        {name: 'exchangeDesc', type: 'string'},
        {name: 'warrantyDesc', type: 'string'},
        {name: 'exchangeDate', type: 'string'}
    ]
});
