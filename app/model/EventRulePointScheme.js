Ext.define('singer.model.EventRulePointScheme', {
	extend: 'Ext.data.Model',
	fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
		{
			name: 'pointSchemaId',
			type: 'int'
		},
		{
			name: 'schmeruleId',
			type: 'int'
		},
		{
			name: 'schemeSaleType',
			type: 'int'
		},
		{
			name: 'schmeNoOfUnit',
			type: 'int'
		},
		{
			name: 'scheamfixedPoint',
			type: 'number'
		},
		{
			name: 'schmefloatFlag',
			type: 'int'
		},
		{
			name: 'schmeAddnSalesType',
			type: 'number'
		},
		{
			name: 'schmeAddnSalesPoint',
			type: 'number'
		},
		{
			name: 'schmestatus',
			type: 'int'
		},
        {
            name:'schmeAssignPoint',
            type:'number'
        }
    
    ]
});