Ext.define('singer.model.ReadExcelM',
        {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'reportID'},
                {name: 'reportName'},
                {name: 'reportQuery'},
                {name: 'status'},
                {name: 'cond1', type: 'string'},
                {name: 'cond2', type: 'string'},
                {name: 'cond3', type: 'string'},
                {name: 'cond4', type: 'string'},
                {name: 'cond5', type: 'string'},
                {name: 'cond6', type: 'string'},
                {name: 'cond1a', type: 'string'},
                {name: 'cond2a', type: 'string'},
                {name: 'cond3a', type: 'string'},
                {name: 'cond4a', type: 'string'},
                {name: 'cond5a', type: 'string'},
                {name: 'cond6a', type: 'string'}
            ]
        }
);