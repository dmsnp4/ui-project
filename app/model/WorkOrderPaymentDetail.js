


Ext.define('singer.model.WorkOrderPaymentDetail', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'seqNo', type: 'int'},
        {name: 'paymentId', type: 'int'},
        {name: 'workOrderNo', type: 'string'},
        {name: 'imeiNo', type: 'string'},
        {name: 'warrantyVerifType', type: 'string'},
        {name: 'defectType', type: 'int'},
        {name: 'amount', type: 'double'},
        {name: 'referenceNo', type: 'string'},
        {name: 'paymentDate', type: 'string'},
        {name: 'laborCost', type: 'double'},
        {name: 'spareCost', type: 'double'},
        {name: 'remarks', type: 'string'},
        {name: 'tecnician', type: 'string'},
        {name: 'actionTake', type: 'string'},
        {name: 'nonWarrantyVerifType', type: 'int'},
        {name: 'invoice', type: 'string'},
        {name: 'statusMsg', type: 'string'},
        {name: 'defectDesc', type: 'string'},
        
        {name: 'woDetials', type: 'auto', defaultValue : []},
    ]
});

//    private int seqNo;
//    private int paymentId;
//    private String workOrderNo;
//    private String imeiNo;
//    private int warrantyVerifType;
//    private int defectType;
//    private double amount;
//    private String referenceNo;
//    private String paymentDate;
//    private int status;
//    private String defectDesc;
//    private String warrentyTypeDesc;
//    private double laborCost;
//    private double spareCost;
//    private String remarks;
//    private String tecnician;
//    private String actionTake;
//    private int nonWarrantyVerifType;
//    private String invoice;
//    private String statusMsg;
//    
//    
//    private ArrayList<WorkOrderMaintainDetail> woDetials;