
Ext.define('singer.model.ActionModel', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'actionId', type: 'String'},
       {name: 'btnAction', type: 'String'},
       {name: 'btnHtmlId', type: 'String'}, 
       {name: 'btnIcon', type: 'String'},
       {name: 'btnUi', type: 'String'},
       {name: 'parentDN', type: 'String'}, 
       {name: 'dn', type: 'String'},
       
       'actions'
    ]
});

//class Action {
//    String actionId;
//    String btnAction;
//    String btnHtmlId;
//    String btnIcon;
//    String btnUi;
//    String parentDN;
//    String dn;
//    ArrayList<Action> actions;
//}
////