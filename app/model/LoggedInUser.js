Ext.define('singer.model.LoggedInUser', {
    extend: 'Ext.data.Model',
    fields: [
//        'userId',
//        'firstName',
//        'lastName',
//        'commonName',
//        'designation',
//        'extraParams',
//        'nic',
//        'telephoneNumber',
//        'email',
//        'status',
//        'password',
//        'newPassword',
//        'createdOn',
//        'createdBy',
//        'modifiedOn',
//        'modifiedBy',
//        'inactivedOn',
//        'inactivedBy',
//        'remarks',
//        'address',
//        'loginStatus',
//        'loginFailAttempts',
//        'lastLoginAccessedOn',
//        'lastLoggedoutOn',
//        'lastLoggedIPAddress',
//        'lastLoggedInSystem',
//        'lastLoggedInOn',
//        'lastAccessedUri',
//        'activateBy',
//        'activateOn',
//        'lastGeneratedKey',
//        'pwdChangedBy',
//        'pwdChangedOn',
//        'pwdFailureAttemptsAll',
//        'pwdResetBy',
//        'pwdResetOn',
//        'securityAnswer',
//        'securityQuestion',
//        'accountUnlockedBy',
//        'accountUnlockedOn',
//        'accountUnlockTime',
//        'dn',
//        'organization',
//        'division',
//        'countryCode',
//        'branch',
//        'department',
//        'room',
//        'groups',
//        'functions',
//        {name: 'groups', convert: function (y, record) {
//                if (Ext.isEmpty(y)) {
//                    return null;
//                }
//                console.log(y);
//            }
//        },
//        {name: 'functions', convert: function (v, record) {
//                if (Ext.isEmpty(v)) {
//                    return null;
//                }
////                console.log(v);
//            }
//
//        }
//       


        {name: 'userId', defaultValue: null},
        {name: 'firstName', defaultValue: null},
        {name: 'lastName', defaultValue: null},
        {name: 'commonName', defaultValue: null},
        {name: 'designation', defaultValue: null},
        {name: 'extraParams', defaultValue: null, type: 'int'},
        {name: 'nic', defaultValue: null},
        {name: 'telephoneNumber', defaultValue: null},
        {name: 'email', defaultValue: null},
        {name: 'status', defaultValue: null},
        {name: 'password', defaultValue: null},
        {name: 'newPassword', defaultValue: null},
        {name: 'createdOn', defaultValue: null},
        {name: 'createdBy', defaultValue: null},
        {name: 'modifiedOn', defaultValue: null},
        {name: 'modifiedBy', defaultValue: null},
        {name: 'inactivedOn', defaultValue: null},
        {name: 'inactivedBy', defaultValue: null},
        {name: 'remarks', defaultValue: null, convert: function(v, record) {
                if (v==='') {
                    return null;
                }
            }},
        {name: 'address', defaultValue: null},
        {name: 'loginStatus', defaultValue: null},
        {name: 'loginFailAttempts', defaultValue: null},
        {name: 'lastLoginAccessedOn', defaultValue: null},
        {name: 'lastLoggedoutOn', defaultValue: null},
        {name: 'lastLoggedIPAddress', defaultValue: null},
        {name: 'lastLoggedInSystem', defaultValue: null},
        {name: 'lastLoggedInOn', defaultValue: null},
        {name: 'lastAccessedUri', defaultValue: null},
        {name: 'activateBy', defaultValue: null},
        {name: 'activateOn', defaultValue: null},
        {name: 'lastGeneratedKey', defaultValue: null},
        {name: 'pwdChangedBy', defaultValue: null},
        {name: 'pwdChangedOn', defaultValue: null},
        {name: 'pwdFailureAttemptsAll', defaultValue: null},
        {name: 'pwdResetBy', defaultValue: null},
        {name: 'pwdResetOn', defaultValue: null},
        {name: 'securityAnswer', defaultValue: null},
        {name: 'securityQuestion', defaultValue: null},
        {name: 'accountUnlockedBy', defaultValue: null},
        {name: 'accountUnlockedOn', defaultValue: null},
        {name: 'accountUnlockTime', defaultValue: null},
        {name: 'dn', defaultValue: null},
        {name: 'organization', defaultValue: null},
        {name: 'division', defaultValue: null},
        {name: 'countryCode', defaultValue: null},
        {name: 'branch', defaultValue: null},
        {name: 'department', defaultValue: null},
        {name: 'room', defaultValue: null},
        {name: 'groups', defaultValue: []},
        {name: 'functions', defaultValue: []}


    ]
});

//    ArrayList<Group> groups;
//
//    ArrayList<Function> functions;

//    String userId;
//    String firstName;
//    String lastName;
//    String commonName;
//    String designation;
//    String extraParams;
//    String nic;
//    String telephoneNumber;
//    String email;
//    String status;
//    String password;
//    String newPassword;
//    String createdOn;
//    String createdBy;
//    String modifiedOn;
//    String modifiedBy;
//    String inactivedOn;
//    String inactivedBy;
//    String remarks;
//    String address;
//    String loginStatus;
//    String loginFailAttempts;
//    String lastLoginAccessedOn;
//    String lastLoggedoutOn;
//    String lastLoggedIPAddress;
//    String lastLoggedInSystem;
//    String lastLoggedInOn;
//    String lastAccessedUri;
//    String activateBy;
//    String activateOn;
//    String lastGeneratedKey;
//    String pwdChangedBy;
//    String pwdChangedOn;
//    String pwdFailureAttemptsAll;
//    String pwdResetBy;
//    String pwdResetOn;
//    String securityAnswer;
//    String securityQuestion;
//    String accountUnlockedBy;
//    String accountUnlockedOn;
//    String accountUnlockTime;
//
//    String dn;
//    
//    String organization;
//    String division;
//    String countryCode;
//    String branch;
//    String department;
//    String room;

// {name: 'userId', type: 'string'},
//        {name: 'firstName', type: 'string'},
//        {name: 'lastName', type: 'string'},
//        {name: 'commonName', type: 'string'},
//        {name: 'designation', type: 'string'},
//        {name: 'extraParams', type: 'string'},
//        {name: 'nic', type: 'string'},
//        {name: 'telephoneNumber', type: 'string'},
//        {name: 'email', type: 'string'},
//        {name: 'status', type: 'string'},
//        {name: 'password', type: 'string'},
//        {name: 'newPassword', type: 'string'},
//        {name: 'createdOn', type: 'string'},
//        {name: 'createdBy', type: 'string'},
//        {name: 'modifiedOn', type: 'string'},
//        {name: 'modifiedBy', type: 'string'},
//        {name: 'inactivedOn', type: 'string'},
//        {name: 'inactivedBy', type: 'string'},
//        {name: 'remarks', type: 'string'},
//        {name: 'address', type: 'string'},
//        {name: 'loginStatus', type: 'string'},
//        {name: 'loginFailAttempts', type: 'string'},
//        {name: 'lastLoginAccessedOn', type: 'string'},
//        {name: 'lastLoggedoutOn', type: 'string'},
//        {name: 'lastLoggedIPAddress', type: 'string'},
//        {name: 'lastLoggedInSystem', type: 'string'},
//        {name: 'lastLoggedInOn', type: 'string'},
//        {name: 'lastAccessedUri', type: 'string'},
//        {name: 'activateBy', type: 'string'},
//        {name: 'activateOn', type: 'string'},
//        {name: 'lastGeneratedKey', type: 'string'},
//        {name: 'pwdChangedBy', type: 'string'},
//        {name: 'pwdChangedOn', type: 'string'},
//        {name: 'pwdFailureAttemptsAll', type: 'string'},
//        {name: 'pwdResetBy', type: 'string'},
//        {name: 'pwdResetOn', type: 'string'},
//        {name: 'securityAnswer', type: 'string'},
//        {name: 'securityQuestion', type: 'string'},
//        {name: 'accountUnlockedBy', type: 'string'},
//        {name: 'accountUnlockedOn', type: 'string'},
//        {name: 'accountUnlockTime', type: 'string'},
//        {name: 'dn', type: 'string'},
//        {name: 'organization', type: 'string'},
//        {name: 'division', type: 'string'},
//        {name: 'countryCode', type: 'string'},
//        {name: 'branch', type: 'string'},
//        {name: 'department', type: 'string'},
//        {name: 'room', type: 'string'},
//        {name: 'groups', type: 'string'},
//        {name: 'functions', type: 'string'}