
Ext.define('singer.model.RepairLevel', {
    extend: 'Ext.data.Model',
    fields: [
        'levelId',
        'levelName',
        'levelDesc',
        {name:'levelCatId', type:'number'},
        {name:'levelPrice', type:'number'},
        'levelCode',
        'levelStatus'
//        {name: 'levelName'},
//        {name: 'levelId'},
//        {name: 'reCateStatus', type: 'int'}
    ]
});
