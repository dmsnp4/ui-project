Ext.define('singer.model.EventBisType', {
	extend: 'Ext.data.Model',
	fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
		{
			name: 'eventPartId',
			type: 'int'
		},
		{
			name: 'eventId',
			type: 'int'
		},
		{
			name: 'status',
			type: 'int'
		},
		{
			name: 'bisStruTypeId',
			type: 'int'
		},
		{
			name: 'bisStruTypeName'
		},
		{
			name: 'bisStruTypeCount',
			type: 'int'
		},
		{
			name: 'eventBussinessList',
			defaultValue: []
		},
        {
            name:'chCol'
        }
    ]
});