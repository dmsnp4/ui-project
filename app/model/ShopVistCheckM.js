Ext.define('singer.model.ShopVistCheckM', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'shopVistId',
            type: 'int'
        },
        {
            name: 'dSR',
            type: 'int'
        },
        {
            name: 'longtitude',
            type: 'string'
        },
        {
            name: 'latitude',
            type: 'string'
        },
        {
            name: 'visitDate',
            type: 'string'
        },
        {
            name: 'subDealer',
            type: 'int'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'userId',
            type: 'string'
        },
        {
            name: 'firstName',
            type: 'string'
        },
        {
            name: 'lastName',
            type: 'string'
        },
        {
            name:'bisName',
            type:'string'
        },
         {
            name:'contactNo',
            type:'string'
        },
        {
            name:'parentName',
            type:'string'
        },
        {
            name:'subDelerName',
            type:'string'
        },
        {
            name:'shopVisitItemList'
        }
    ]
});