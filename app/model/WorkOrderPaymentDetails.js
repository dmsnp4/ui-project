Ext.define('singer.model.WorkOrderPaymentDetails', {
    extend: 'Ext.data.Model',
    fields: [
         {name: 'seqNo', type: 'int'},
         {name: 'paymentId', type: 'int'},
         {name: 'workOrderNo', type: 'String'},
         {name: 'imeiNo', type: 'String'},
         {name: 'warrantyVerifType', type: 'int'},
         {name: 'defectType', type: 'int'},
         {name: 'amount', type: 'double'},
         {name: 'referenceNo', type: 'String'},
         {name: 'paymentDate', type: 'String'},
         {name: 'status', type: 'int'},
         {name: 'defectDesc', type: 'String'},
         {name: 'warrentyTypeDesc', type: 'String'},
         {name: 'laborCost', type: 'double'},
         {name: 'spareCost', type: 'double'},
         {name: 'WorkOrderMaintainDetail', defaultValue: []}
    ]
});

//class WorkOrderPaymentDetail {
//private int seqNo;
//private int paymentId;
//private String workOrderNo;
//private String imeiNo;
//private int warrantyVerifType;
//private int defectType;
//private double amount;
//private String referenceNo;
//private String paymentDate;
//private int status;
//private String defectDesc;
//private String warrentyTypeDesc;
//private double laborCost;
//private double spareCost;
//private ArrayList<WorkOrderMaintainDetail> woDetials;
//}