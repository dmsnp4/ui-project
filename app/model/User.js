Ext.define('singer.model.User', {
    extend: 'Ext.data.Model',
    fields: [
        'userId',
        'firstName',
        'lastName',
        'commonName',
        'designation',
        {name: 'extraParams', defaultValue: 0},
        'nic',
        'telephoneNumber',
        'email',
        'status',
        {name: 'password', defaultValue: null},
        {name: 'newPassword', defaultValue: null},
        {name: 'createdOn', defaultValue: null},
        {name: 'createdBy', defaultValue: null},
        {name: 'modifiedOn', defaultValue: null},
        {name: 'modifiedBy', defaultValue: null},
        {name: 'inactivedOn', defaultValue: null},
        {name: 'inactivedBy', defaultValue: null},
        {name: 'remarks', defaultValue: null},
        {name: 'address', defaultValue: null},
        {name: 'loginStatus', defaultValue: null},
        {name: 'loginFailAttempts', defaultValue: null},
        {name: 'lastLoginAccessedOn', defaultValue: null},
        {name: 'lastLoggedoutOn', defaultValue: null},
        {name: 'lastLoggedIPAddress', defaultValue: null},
        {name: 'lastLoggedInSystem', defaultValue: null},
        {name: 'lastLoggedInOn', defaultValue: null},
        {name: 'lastAccessedUri', defaultValue: null},
        {name: 'activateBy', defaultValue: null},
        {name: 'activateOn', defaultValue: null},
        {name: 'lastGeneratedKey', defaultValue: null},
        {name: 'pwdChangedBy', defaultValue: null},
        {name: 'pwdChangedOn', defaultValue: null},
        {name: 'pwdFailureAttemptsAll', defaultValue: null},
        {name: 'pwdResetBy', defaultValue: null},
        {name: 'pwdResetOn', defaultValue: null},
        {name: 'securityAnswer', defaultValue: null},
        {name: 'securityQuestion', defaultValue: null},
        {name: 'accountUnlockedBy', defaultValue: null},
        {name: 'accountUnlockedOn', defaultValue: null},
        {name: 'accountUnlockTime', defaultValue: null},
        {name: 'dn', defaultValue: null},
        {name: 'organization', defaultValue: null},
        {name: 'division', defaultValue: null},
        {name: 'countryCode', defaultValue: null},
        {name: 'branch', defaultValue: null},
        {name: 'department', defaultValue: null},
        {name: 'room', defaultValue: null},
        {name: 'groups', defaultValue: []},
        {name: 'functions', defaultValue: []}
        // 'functions',
        // {name: 'groups', convert: function (y, record) {
        //         if (Ext.isEmpty(y)) {
        //             return [];
        //         }
        //         console.log(y);
        //     }
        // },
//         {name: 'functions', convert: function (v, record) {
//                 if (Ext.isEmpty(v)) {
//                     return [];
//                 }
// //                console.log(v);
//             }

//         }
//       
    ]
});