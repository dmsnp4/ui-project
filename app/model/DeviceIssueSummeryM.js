Ext.define('singer.model.DeviceIssueSummeryM', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'distributorName', type: 'string'},
        {name: 'dsrName', type: 'string'},
        {name: 'issueNo', type: 'string'},
        {name: 'modelNo', type: 'string'},
        {name: 'modelDescription', type: 'string'},
        {name: 'modelCnt', type: 'string'},
        {name: 'status', type: 'string'},
        {name: 'price', type: 'string'},
        {name: 'totValue', type: 'string'}
    ]
});
