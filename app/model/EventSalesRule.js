Ext.define('singer.model.EventSalesRule', {
    extend: 'Ext.data.Model',
    fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
        {
            name: 'ruleId',
            type: 'int'
  },
        {
            name: 'eventId',
            type: 'int'
  },
        {
            name: 'productFamily',
            type: 'string'
  },
        {
            name: 'barnd',
            type: 'string'
  },
        {
            name: 'modleDesc',
            type: 'string'
  },
        {
            name: 'modleNo',
            type: 'int'
  },
        {
            name: 'period',
            type: 'int'
  },
        {
            name: 'intervals',
            type: 'int'
  },
        {
            name: 'acceptedInteval',
            type: 'int'
  },
        {
            name: 'salesType',
            type: 'int'
  },
        {
            name: 'point',
            type: 'number'
  },
        {
            name: 'minReq',
            type: 'number'
  },
        {
            name: 'pointSchemeFlag',
            type: 'int'
  },
        {
            name: 'totalAchivement',
            type: 'number'
  },
        {
            name: 'status',
            type: 'int'
  },
        {
            name: 'schmefloatPoint',
            type: 'number'
        },
        {
            name: 'schmefloagFlagString',
            type: 'string'
        },
        {
            name: 'schmefloatFlag',
            type: 'int'
        },
        {
            name: 'scheamfixedPoint',
            type: 'number'
        },
        {
            name: 'scheamfixedPoint',
            type: 'number'
        },
        {
            name:'schemeSaleType',
            type:'int'
        },
        {
            name:'schmeNoOfUnit',
            type:'int'
        },
        {
            name:'schmeAddnSalesType',
            type:'int'
        },
        {
            name:'schmeAddnSalesPoint',
            type:'number'
        },
        {
            name:'schmeAssignPoint',
            type:'number'
        },
        {
            name: 'eventRulePointSchemaList',
            defaultValue: []
        }
        
    ]
});