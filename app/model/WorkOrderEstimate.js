Ext.define('singer.model.WorkOrderEstimate', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'esRefNo',
            type: 'string'
        },
        {
            name: 'workOrderNo',
            type: 'string'
        },
        {
            name: 'imeiNo',
            type: 'string'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'costSparePart',
            type: 'number'
        },
        {
            name: 'costLabor',
            type: 'number'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'customerRef',
            type: 'string'
        },
        {
            name: 'paymentOption',
            type: 'int'
        }
    ]
});