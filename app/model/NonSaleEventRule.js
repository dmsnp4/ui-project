/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('singer.model.NonSaleEventRule', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'nonSalesRule', type: 'int'},
        {name: 'eventId', type: 'int'},
        {name: 'description', type: 'string'},
        {name: 'taget', type: 'int'},
        {name: 'point', type: 'int'},
        {name: 'salesType', type: 'int'},
        {name: 'status', type: 'int',
            convert: function(v, r) {
                return (v === 1 || v === true) ? 1 : 0;
            }
        }
    ]
});
