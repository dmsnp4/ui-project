Ext.define('singer.model.WorkOrderTrancefer', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'woTransNo',
            type: 'int'
        },
        {
            name: 'workOrderNo',
            type: 'String'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'transferDate',
            type: 'String'
        },
        {
            name: 'transferReason',
            type: 'String'
        },
        {
            name: 'courierEmail',
            type: 'String'
        },
        {
            name: 'deleverDetils',
            type: 'String'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'bisName',
            type: 'String'
        },
        {
            name: 'deleveryType',
            type: 'int'
        },
        {
            name: 'deleveryTypeDesc',
            type: 'String'
        },
        {
            name: 'statusMsg',
            type: 'string'
        },
        {
            name: 'tansferLocationId',
            type: 'int'
        },
        {
            name: 'transferLocationDesc',
            type: 'String'
        },
        {
            name: 'retunDeleveryType',
            type: 'int'
        },
        {
            name: 'returnDeleveryDesc',
            type: 'String'
        },
        {
            name: 'returnDate',
            type: 'String'
        },
        {
            name: 'returnReason',
            type: 'String'
        },
        {
            name: 'returnDeleveryDetails',
            type: 'String'
        },
    ]
});




//{"woTransNo":3,"workOrderNo":"WO000009","bisId":0,"transferDate":"31-12-14 00:00:00.0",
//"transferReason":"Sample Test","courierEmail":"test@mail.com","deleverDetils":"test",
//"status":1,"bisName":"Iron","deleveryType":0,"deleveryTypeDesc":null}


//    private int tansferLocationId;
//    private String transferLocationDesc;
//    private int retunDeleveryType;
//    private String returnDeleveryDesc;
//    private String returnDate;
//    private String returnReason;
//    private String returnDeleveryDetails