Ext.define('singer.model.Warrenty', {
    extend: 'Ext.data.Model',
    fields: [
//        {name : 'id', mapping : 'warrantyId'} ,
        'warrantyId',
        'extendedDays',
        'iMEI',
        'status'
    ]
});
