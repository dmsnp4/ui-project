Ext.define('singer.model.EventNonSalesRule', {
	extend: 'Ext.data.Model',
	fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
		{
			name: 'nonSalesRule',
			type: 'int'
		},
		{
			name: 'eventId',
			type: 'int'
		},
		{
			name: 'description',
			type: 'string'
		},
		{
			name: 'taget',
			type: 'int'
		},
		{
			name: 'point',
			type: 'int'
		},
		{
			name: 'salesType',
			type: 'int'
		},
		{
			name: 'status',
			type: 'int'
		}
    ]
});