Ext.define('singer.model.EventBussinessList', {
	extend: 'Ext.data.Model',
	fields: [
 //        {name: 'extraParams', defaultValue: null, type: 'int'},
		{
			name: 'bisId',
			type: 'int'
		},
		{
			name: 'bisName'
		},
		{
			name: 'bisDesc'
		},
		{
			name: 'bisTypeId',
			type: 'int'
		},
		{
			name: 'bisTypeDesc'
		},
		{
			name: 'eventPartId',
			type: 'int'
		},
		{
			name: 'status',
			type: 'int'
		},
		{
			name: 'seqNo',
			type: 'int'
		},
        {
            name:'userId',
            type:'string'
        }
    ]
});