Ext.define('singer.model.BuisnessAssignedUsersM', {
    extend: 'Ext.data.Model',
    fields: [
           {name: 'bisId', type: 'int'},
           {name: 'userId', type: 'String'},
           {name: 'firstName', type: 'String'},
           {name: 'lastName', type: 'String'},
           {name: 'status', type: 'int'},
           {name: 'user', type: 'String'}
    ]
});