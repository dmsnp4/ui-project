Ext.define('singer.model.DeviceIssue', {
    extend: 'Ext.data.Model',
//    fields: [
//        {name: 'UserId', type: 'string'},
//        {name: 'Password', type: 'string'},
//        {name: 'FName', type: 'string'},
//        {name: 'LName', type: 'string'},
//        {name: 'Designation', type: 'string'},
//        {name: 'NIC', type: 'string'},
//        {name: 'TelePhone', type: 'string'},
//        {name: 'EMail', type: 'string'},
//        {name: 'Status', type: 'int'},
//        {name: 'RegDate', type: 'string'},
//        {name: 'InctiveDate', type: 'string'},
//        {name: 'LastLogDate', type: 'string'},
//        {name: 'ReturnMsg', type: 'string'},
//        {name: 'RetunFlag', type: 'string'},
//        {name: 'BisName', type: 'string'},
//        {name: 'User', type: 'string'}
//    ]

    fields: [
        {name: 'issueNo', type: 'int'},
        {name: 'distributerID', type: 'int'},
        {name: 'dSRId', type: 'int'},
        {name: 'issueDate', type: 'string'},
        {name: 'price', type: 'number'},
        {name: 'distributorMargin', type: 'number'},
        {name: 'delerMargin', type: 'number'},
        {name: 'discount', type: 'number'},
        {name: 'status', type: 'int'},
        {name: 'deviceImei', defaultValue: []},
        {name:'fromName', type:'string'},
        {name:'toName', type:'string'},
        {name: 'dtlCnt', type: 'int'},
         {name: 'remarks', type: 'string'},
         {name: 'type', type: 'string'}
    ]
});
