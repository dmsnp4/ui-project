
Ext.define('singer.model.ClearTransModel', {
    extend: 'Ext.data.Model',
    fields: [
        'debitNoteNo',
        'imeiNo', 
        'customerCode',
        'shopCode',
        'user',
        'remarks',
        'modelNo',
        'bisId',
        'bisName',
        'orderNo',
        'salesPrice',
        'dateAccepted',
        'status',
        
        'issueNo',
        'distributerID',
        'fromName',
        'dSRId',
        'toName',
        'issueDate',
        'price',
        'distributorMargin',
        'delerMargin',
        'discount',
        'status',
        'user',
        'statusMsg',
        'remarks',
        'deviceImei'
        
    ]
});

//{"debitNoteNo":null,"issueNo":38,"distributerID":1,"fromName":"Iron",
//"dSRId":45,"toName":"LG","issueDate":"2014/11/07","price":0.0,
//"distributorMargin":0.0,"delerMargin":0.0,"discount":0.0,"status":1,
//"user":null,"statusMsg":"Pending","remarks":null,"deviceImei":null}
//
//
//    private String debitNoteNo;
//    private int issueNo;
//    private int distributerID;
//    private String fromName;
//    private int dSRId;
//    private String toName;
//    private String issueDate;
//    private double price;
//    private double distributorMargin;
//    private double delerMargin;
//    private double discount;
//    private int status;
//    private String user;
//    private String statusMsg;
//    private String remarks;
