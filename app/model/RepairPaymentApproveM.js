
Ext.define('singer.model.RepairPaymentApproveM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'setviceName', type: 'string'},
        {name: 'paymentPrice', type: 'float'},
        {name: 'paymentId', type: 'int'},
        {name: 'refDate', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'poNumber', type: 'string'},
        {name: 'poDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
                if (Ext.isEmpty(v) || v === '0000-00-00') {
                    return '';
                } else {
                    return Ext.Date.format(new Date(v), 'Y-m-d');
                }
            }},
        {name: 'settleAmount', type: 'float'},
        {name: 'remarks', type: 'string'},
        {name: 'chequeNo', type: 'string'},
        {name: 'chequeDetails', type: 'string'},
        {name: 'chequeDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
                if (Ext.isEmpty(v) || v === '0000-00-00') {
                    return '';
                } else {
                    return Ext.Date.format(new Date(v), 'Y-m-d');
                }
            }},
        {name: 'vatValue', type: 'float'},
        {name: 'nbtValue', type: 'float'},
        {name: 'totalAmountWithDeduct', type: 'float'},
        {name: 'deductionAmount', type: 'float'},
        {name: 'essdAccountNo', type: 'string'},
        {name: 'paymentDate', type: 'string'},
        {name: 'approveBisId', type: 'int'}
        
    ]
});

//    private String setviceName;
//    private double paymentPrice;
//    private int paymentId;
//    private String refDate;
//    private int status;
//    private String poNumber;
//    private String poDate;
//    private double settleAmount;
//    private String remarks;
//    private String chequeNo;
//    private String chequeDetails;
//    private String chequeDate;
//    private double tax;
//    private String essdAccountNo;
//    private String paymentDate;
