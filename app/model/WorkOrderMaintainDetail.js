

Ext.define('singer.model.WorkOrderMaintainDetail', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'workOrderNo', type: 'String'},
        {name: 'workOrderMainId', type: 'int'},
        {name: 'seqNo', type: 'int'},
        {name: 'partNo', type: 'int'},
        {name: 'partDesc', type: 'String'},
        {name: 'partSellPrice', type: 'double'},
        {name: 'invoiceNo', type: 'String'},
        {name: 'thirdPartyFlag', type: 'int'},
        {name: 'status', type: 'int'},
    ]
});

//    private String wrokOrderNo;
//    private int workOrderMainId;
//    private int seqNo;
//    private int partNo;
//    private String partDesc;
//    private double partSellPrice;            
//    private String invoiceNo;
//    private int thirdPartyFlag;
//    private int status;