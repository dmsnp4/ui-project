Ext.define('singer.model.WarrantyScheme', {
    extend: 'Ext.data.Model',
    fields: [
//        {name : 'id', mapping : 'warrantyId'} ,
        {name:'schemeId', type:'int'},
        {name:'schemeName', type:'string'},
        {name:'period', type:'int'},
        {name:'status', type:'int'},
        {name:'statusMsg', type:'string'}
    ]
});
