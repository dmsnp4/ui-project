/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.model.AssignMarksParticipant', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'bisId', type: 'int'},
        {name: 'bisName', type: 'string'},
        {name: 'userId', type: 'string'},
        {name: 'bisDesc', type: 'string'},
        {name: 'bisTypeId', type: 'int'},
        {name: 'bisTypeDesc', type: 'string'},
        {name: 'eventPartId', type: 'int'},
        {name: 'status', type: 'int'},
        {name: 'seqNo', type: 'int'},
        {name: 'usedStatus', type: 'int'}
    ]
});

