
Ext.define('singer.model.ImeiAdj', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'seqNo', type: 'int'},
        {name: 'debitNoteNo', type: 'String'},
        {name: 'newImeiNo', type: 'String'},
        {name: 'imeiNo', type: 'String'},
        {name: 'customerCode', type: 'String'},
        {name: 'shopCode', type: 'String'},
        {name: 'modleNo', type: 'int'},
        {name: 'bisId', type: 'int'},
        {name: 'orderNo', type: 'String'},
        {name: 'salerPrice', type: 'number'},
        {name: 'status', type: 'int'},
        {name: 'user', type: 'String'},
        {name: 'location', type: 'String'},
        {name: 'site', type: 'String'},
        {name:'modleDesc', type:'String'},
        {name:'oldImei', type:'string'}
    ]
});


//    int seqNo;
//    String debitNoteNo;
//    String newImeiNo;
//    String imeiNo;
//    String customerCode;
//    String shopCode;
//    int modleNo;
//    pint bisId;
//    String orderNo;
//    double salesPrice;
//    int status;
//    String user;

