
Ext.define('singer.model.EventInquireDetailsMod', {
    extend: 'Ext.data.Model',
    fields: [
       {name: 'acceptedInteval', type: 'int'},
       {name: 'addnSalesPoint', type: 'int'},
       {name: 'addnSalesType', type: 'int'},
       {name: 'assignPoint', type: 'int'},
       {name: 'barnd', type: 'String'},
       {name: 'eventId', type: 'int'},
       {name: 'fixedPoint', type: 'int'},
       {name: 'floatFlag', type: 'int'},
       {name: 'intervals', type: 'int'},
       {name: 'minReq', type: 'int'},
       {name: 'modleDesc', type: 'String'},
       {name: 'modleNo', type:'int'},
       {name: 'noOfUnit', type: 'int'},
       {name: 'period', type: 'int'},
       {name: 'point', type: 'int'},
       {name: 'pointSchemaId', type: 'int'},
       {name: 'pointSchemeFlag', type: 'int'},
       {name: 'productFamily', type: 'string'},
       {name: 'ruleId', type: 'int'},
       {name: 'salesType', type: 'int'},
       {name: 'schemaSalesType', type: 'int'},
       {name: 'schemaStatus', type: 'int'},
       {name: 'status', type: 'int'},
       {name: 'totalAchivement', type: 'int'}  ,
        {name: 'eventMrgComment', type: 'String'},
       {name: 'mktMrgComment', type: 'String'},
    ]
});

