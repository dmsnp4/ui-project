/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.model.AssignMarksEvent', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'eventId', type: 'int'},
        {name: 'eventName', type: 'string'},
        {name: 'eventDesc', type: 'string'},
        {
            name: 'startDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
                if (Ext.isEmpty(v) || v === '0000-00-00') {
                    return '';
                } else {
                    return Ext.Date.format(new Date(v), 'Y-m-d');
                }
            }
        },
        {
            name: 'endDate', type: 'date', dateFormat: 'Y-m-d', convert: function(v, record) {
                if (Ext.isEmpty(v) || v === '0000-00-00') {
                    return '';
                } else {
                    return Ext.Date.format(new Date(v), 'Y-m-d');
                }
            }
        },
        {name: 'threshold', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'statusMsg', type: 'string'},
        {name: 'eventMrgComment', type: 'string'},
        {name: 'mktMrgComment', type: 'string'},
        {name: 'eventAchievement', type: 'string'},
        {name: 'evenAwardList', type: 'string'},
        {name: 'eventNonSalesRuleList', type: 'string'},
        {name: 'eventSalesRuleList', type: 'string'},
        {name: 'eventBisTypeList', type: 'string'}
        
    ]
});
