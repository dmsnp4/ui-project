
Ext.define('singer.model.RepairPaymentWithPrice', {
    extend: 'Ext.data.Model',
    fields: [
    	 {name: 'status', type: 'int'},
         {name: 'workOrderNo', type: 'String'},
         {name: 'imeiNo', type: 'String'},
         {name: 'warrantyVerifType', type: 'String'},
         {name: 'defectDesc', type: 'String'},
         //{name: 'order', type: 'String'},
         //{name: 'type', type: 'String'},
         {name: 'laborCost', type: 'double'},
         {name: 'seqNo', type: 'int'},
         {name: 'paymentId', type: 'int'},
         {name: 'defectType', type: 'int'},
         {name: 'amount', type: 'double'},
         {name: 'referenceNo', type: 'String'},
         {name: 'paymentDate', type: 'String'},
         {name: 'warrentyTypeDesc', type: 'String'},
         {name: 'spareCost', type: 'double'},
    ]
});

//{"seqNo":0,"paymentId":0,"workOrderNo":"WO000021","imeiNo":"66666",
//"warrantyVerifType":0,"defectType":0,"amount":0.0,"referenceNo":null,
//"paymentDate":null,"status":0,"defectDesc":"Data","warrentyTypeDesc":"2",
//"laborCost":0.0,"spareCost":0.0,"remarks":null,"tecnician":null,
//"actionTake":null,"nonWarrantyVerifType":0,"woDetials":null},

//    private int seqNo;
//    private int paymentId;
/////    private String workOrderNo;
/////    private String imeiNo;
/////    private int warrantyVerifType;
//    private int defectType;
//    private double amount;
//    private String referenceNo;
//    private String paymentDate;
/////    private int status;
/////    private String defectDesc;
//    private String warrentyTypeDesc;
/////    private double laborCost;
//    private double spareCost;
