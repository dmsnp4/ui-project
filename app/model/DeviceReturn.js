
Ext.define('singer.model.DeviceReturn', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'returnNo', type: 'int'},
        {name: 'distributorId', type: 'int'},
        {name: 'dsrId', type: 'int'},
        {name: 'returnDate', type: 'string'},
        {name: 'price', type: 'number'},
        {name: 'distributorMargin', type: 'number'},
        {name: 'dealerMargin', type: 'number'},
        {name: 'discount', type: 'number'},
        {name: 'status', type: 'int'},
        {name: 'reason', type: 'string'},
		{
			name:'distributorName', type:'string'
		},
		{
			name:'dsrName', type:'string'
		},
		{
			name:'user', type:'string'	
		},
        {name: 'deviceReturnList', defaultValue: []}
    ]
});