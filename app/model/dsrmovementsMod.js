

Ext.define('singer.model.dsrmovementsMod', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'bisId', type: 'int'},
        {name: 'bisName', type: 'string'},
        {name: 'bisDesc', type: 'string'},
        {name: 'logtitute', type: 'string'},
        {name: 'latitite', type: 'string'},
        {name: 'bisTypeDesc', type: 'string'},
        {name: 'bisTypeId', type: 'int'},
        {name: 'enableFlag', type: 'int'},
        {name: 'vistiorBisId', type: 'int'},
        {name:'visitDate', type:'string'},
        {name:'seqNo', type:'int'},
        {name:'userId', type:'string'},
        {name:'firstName', type:'string'},
        {name:'lastName', type:'string'}

    ]
});
