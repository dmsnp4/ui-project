

Ext.define('singer.model.RepairPayment', {
    extend: 'Ext.data.Model',
    fields: [
//        {name: 'active', type: 'int', convert: function(v, record) {
//                if (record.get("status") === 1) {
//                    return true;
//                } else {
//                    return false;
//                }
//            }},
        
        {name:'status',type:'int',
            convert: function (v,r) {
                return (v === 1 || v === true) ? 1 : 0;
            }},
        
        {name: 'workOrderNo', type: 'string'},
        {name: 'imeiNo', type: 'string'},
        {name: 'warrantyVerifType', type: 'string'},
        {name: 'defectDesc', type: 'string'},
        {name: 'delayDays', type: 'string'},
        {name: 'deductionAmount', type: 'string'},
        {name: 'amount', type: 'double'},
        {name: 'warrentyTypeDesc', type: 'string'},
        {name: 'referenceNo', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'initDeduct', type: 'float'},
        {name: 'vatApplicable', type: 'int'},
        {name: 'nbtApplicable', type: 'int'},
        {name: 'lineDeductSum', type: 'float'}
    ]
});

