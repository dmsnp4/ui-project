Ext.define('singer.model.DSRTrack', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'bisName',
            type: 'string'
        },
        {
            name: 'bisDesc',
            type: 'string'
        },
        {
            name: 'logtitute',
            type: 'string'
        },
        {
            name: 'latitite',
            type: 'string'
        },
        {
            name: 'bisTypeDesc',
            type: 'string'
        },
        {
            name: 'bisTypeId',
            type: 'int'
        },
        {
            name: 'enableFlag',
            type: 'int',
            convert: function (v) {
                return (v === 1 || v === true) ? 1 : 0;
            }
        },
        {
            name: 'vistiorBisId',
            type: 'int'
        },
        {
            name: 'visitDate',
            type: 'string'
        },
        {
            name: 'seqNo',
            type: 'int'
        },
        {
            name: 'bisId',
            type: 'int'
        },
        {
            name: 'firstName',
            type: 'string'
        },
        {
            name: 'userId',
            type: 'string'
        },{
            name:'parantName',
            type:'string'
        },
        {
            name: 'address',
            type: 'string'
        },{
            name:'telePhoneNum',
            type:'string'
        }
    ]
});