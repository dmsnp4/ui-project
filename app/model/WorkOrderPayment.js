


Ext.define('singer.model.WorkOrderPayment', {
    extend: 'Ext.data.Model',
    fields: [
    	 {name: 'paymentId', type: 'int'},
         {name: 'amount', type: 'float'},
         {name: 'nbtValue', type: 'number'},
         {name: 'vatValue', type: 'number'},
         {name: 'essdAccountNo', type: 'String'},
         {name: 'paymentDate', type: 'String'},
         {name: 'status', type: 'int'},
         {name: 'poNo', type: 'String'},
         {name: 'poDate', type: 'String'},
         {name: 'chequeNo', type: 'String'},
         {name: 'chequeDate', type: 'String'},
         {name: 'woPaymentDetials', type: 'auto', defaultValue : []},
         {name: 'payBisId', type: 'int'},
         {name: 'vatApplicable', type: 'int'},
         {name: 'nbtApplicable', type: 'int'}
         
    ]
});

// private int paymentId;
//    private double amount;
//    private double nbtValue;
//    private double vatValue;
//    private String essdAccountNo;
//    private String paymentDate;
//    private int status;
//    private String poNo;
//    private String poDate;
//    private String chequeNo;
//    private String chequeDate;
//    
//    private ArrayList<WorkOrderPaymentDetail> woPaymentDetials;

