

Ext.define('singer.model.AcceptDebitM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'debitNoteNo', type: 'String'},
        {name: 'orderNo', type: 'String'},
        {name: 'invoiceNo', type: 'String'},
        {name: 'contactNo', type: 'String'},
        {name: 'partNo', type: 'int'},
        {name: 'orderQty', type: 'int'},
        {name: 'deleveryQty', type: 'int'},
        {name: 'deleveryDate', type: 'String'},
        {name: 'headStatus', type: 'int'},
        {name: 'customerNo', type: 'String'},
        {name: 'siteDescription', type: 'String'},
        {name: 'status', type: 'int'},
        {name: 'user', type: 'String'},
        {name: 'importDebitList'},
        {name:'statusMsg', type:'string'}

    ]
});

//String debitNoteNo;  
//    String orderNo; 
//    String invoiceNo; 
//    String contactNo; 
//    String partNo; 
//    int orderQty; 
//    int deleveryQty; 
//    String deleveryDate; 
//    int headStatus; 
//   String customerNo; 
//    String siteDescription; 
//    int status; 
//    String user;


