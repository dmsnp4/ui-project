

Ext.define('singer.model.ImportDebitList', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'bisId', type: 'int'},
        {name: 'customerCode', type: 'String'},
        {name: 'dateAccepted', type: 'String'},
        {name: 'debitNoteNo', type: 'String'},
        {name: 'imeiNo', type: 'String'},
        {name: 'modleNo', type: 'int'},
        {name: 'orderNo', type: 'String'},
        {name: 'remarks', type: 'String'},
        {name: 'shopCode', type: 'String'},
        {name: 'site', type: 'String'},
        {name: 'salerPrice', type: 'String'},
    ]
});
//bisId: 1
//customerCode: "14cus"
//dateAccepted: 
//nulldebitNoteNo: "1"
//imeiNo: "111111111111111111"
//modleNo: 1
//orderNo: "324"
//remarks: "transit cleared"
//salerPrice: 0
//shopCode: "412"
//site: null
//status: 1