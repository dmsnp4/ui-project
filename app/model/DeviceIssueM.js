
Ext.define('singer.model.DeviceIssueM', {
    extend: 'Ext.data.Model',
    fields: [
//        'debitNoteNo',
//        'imeiNo', 
//        'customerCode',
//        'shopCode',
//        'user',
//        'remarks',
//        'modelNo',
//        'bisId',
//        'bisName',
//        'orderNo',
//        'salesPrice',
//        'dateAccepted',
//        'status',
        
        'issueNo',
        'distributerID',
        'fromName',
        'dSRId',
        'toName',
        'issueDate',
        'price',
        'distributorMargin',
        'delerMargin',
        'discount',
        'status',
        'user',
        'statusMsg',
        'remarks',
        'deviceImei'
        
    ]
});