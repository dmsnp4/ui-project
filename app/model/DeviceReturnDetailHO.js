Ext.define('singer.model.DeviceReturnDetailHO', {
	extend: 'Ext.data.Model',

	fields: [
                {
                    name : 'distributor',
                    type : 'string'
                },
                {
                    name : 'erp_model',
                    type : 'string'
                },
                {
                    name : 'imeiNo',
                    type : 'string'
                },
                {
                    name : 'recvDate',
                    type : 'string'
                },
                {
                    name : 'price',
                    type : 'string'
                }
               
    ]
});