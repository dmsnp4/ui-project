

Ext.define('singer.controller.ImportDebitNotesController', {
    extend: 'Ext.app.Controller',
    views: [
        'DeviceIssueModule.ImportDebitNotes.DebitGrid',
        'DeviceIssueModule.ImportDebitNotes.DebitView',
        'DeviceIssueModule.ImportDebitNotes.DebitDetailsGridView',
        'DeviceIssueModule.ImportDebitNotes.DetailsDebitGrid',
        'DeviceIssueModule.ImportDebitNotes.DebitDetailView'
    ],
    stores: [
        'ImportDebitNotesStore', 
        'ImportDebitDetails'
    ],
    models: [
        'ImportDebitNotesModel',
        'ImportDebitNoteDetailM'
    ],
    refs: [
        {
            ref: 'debit_grid',
            selector: 'importDebitgrid'
        },
        {
            ref: 'Debit_V',
            selector: 'debitVieww'
        },
        {
            ref: 'Debit_Details_V',
            selector: 'debitDetailGridView'
        },
        {
            ref: 'DetailsGrid',
            selector: 'importdebitdetailsGrid'
        },
        {
            ref: 'DetailsGridView',
            selector: 'debitDetailView'
        }
    ],
    init: function() {
        var me = this;
        me.control({
            'importDebitgrid': {
                added: this.initializeGrid
            },
            ' importDebitgrid': {
                debitView: this.openDebitNoteViewWindow
            },
            'debitVieww button[action=cancel]': {
                click: this.mainDebitGridViewCancel
            },
            'debitDetailView button[action=done]': {
                click: this.debitDetailGridViewCancel
            },
            'importDebitgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'importdebitdetailsGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'importDebitgrid   ': {
                debitDetailsgrid: this.openImportDebitDetailsGrid
            },
            'detailsGrid   ': {
                detailsgridVieww: this.openDetailsGridView
            },
            'importdebitdetailsGrid ': {
                detailVieww: this.openDebitNoteDetailsViewWindow
            },
        });
    },
    
    openDetailsGridView: function(view, record) {
        //console.log('openDetailsGridView');
        this.getController('CoreController').onOpenViewMoreWindow(record, 'debitDetailView', 'View');
    },
    
    openImportDebitDetailsGrid: function(view, record) {
        singer.AppParams.TEMP = record.get('debitNoteNo');
        var store = this.getImportDebitDetailsStore();
        this.getController('CoreController').loadBindedStore(store);
        var grid = Ext.widget('importdebitdetailsGrid');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Details',
            id: 'tempWindow',
            items: [grid]
        });
        win.show();
        win.setTitle("Details ["+record.get('debitNoteNo')+"]");
    },
    
    onClearSearch: function(btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    
    mainDebitGridViewCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    
    debitDetailGridViewCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    
    openDebitNoteViewWindow: function(view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'debitVieww','View');
    },
    
    openDebitNoteDetailsViewWindow: function(view, record) {
//        //console.log("comming to openDebitNoteDetailsViewWindow: function");
        this.getController('CoreController').onOpenViewMoreWindow(record, 'debitDetailView', 'View');
    },
    
    initializeGrid: function(grid) {
        var store = this.getImportDebitNotesStoreStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    
    moreViewClose: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    }
   

});