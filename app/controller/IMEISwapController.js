Ext.define('singer.controller.IMEISwapController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.IMEISwapController',
    controllers: ['CoreController'],
    views: [
        'Administrator.IMEISwap.IMEISwapView'
    ],
    refs: [
        {
            ref: 'IMEISwapView',
            selector: 'IMEISwapView'
        }
    ],
    init: function() {
        var me = this;
        me.control({
            'IMEISwapView': {
                added: this.initialize
            },
            'radiogroup':{
                change: this.selectOption
            },
            'IMEISwapView button[action=cancel]': {
                click: this.closeWin
            },
            'IMEISwapView button[action=validate]': {
                click: this.validateIMEI
            },
            'IMEISwapView button[action=save]': {
                click: this.saveData
            }
        });
    },
    
    initialize: function(grid) {
        console.log('Initializing IMEI Swap ....');
    },
    selectOption: function(me, newValue, oldValue, eOpts){
        var chekedVal = newValue.type;
        console.log('Radio Buuton select..........', chekedVal);
        if(chekedVal == 1){
            Ext.getCmp('enter-swap-imei').setVisible(true);
            Ext.getCmp('select-swap-bis-id').setVisible(false);
        }else{
            Ext.getCmp('enter-swap-imei').setVisible(false);
            Ext.getCmp('select-swap-bis-id').setVisible(true);
        } 
    },
    closeWin: function(btn){
        var frm = btn.up('window');
        frm.close();
    },
    validateIMEI: function(btn){
        var imeiNo = Ext.getCmp('swap-imei').getValue();
        if(imeiNo != null && imeiNo.length>0){
            Ext.getBody().mask('Please wait...', 'loading');
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'SwapIMEIService/validateIMEI?imei='+imeiNo,
                method: 'GET',
                success: function (response, btn) {
                    Ext.getBody().unmask();
                    var responseData = Ext.decode(response.responseText);
                    var status = responseData.statusMsg;
                    if(status == 'Successfull'){
                        var bisId = responseData.bisId;
                        var bisName = responseData.bisName;
                        if(bisId == 0){
                            Ext.Msg.alert("IMEI", 'Invalid IMEI.');
                        }else{
                            Ext.getCmp('current-bis-loc').setText(bisId+' - '+bisName);
                            Ext.getCmp('hdn-bis-id').setValue(bisId);
                        }
                        
                    }else{
                        Ext.Msg.alert("Error", status);
                    }
                },
                failure: function () {
                    Ext.getBody().unmask();
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        }else{
            Ext.Msg.alert("Enter", "Enter the IMEI first.");
        }
        
    },
    saveData: function(btnU){
        var type = parseInt(Ext.getCmp('radio-group-select').getValue().type);
        console.log('saving Data.......', type);
        var oldImei = Ext.getCmp('swap-imei').getValue();
        var newImei = Ext.getCmp('enter-swap-imei').getValue();
        var bisId = Ext.getCmp('hdn-bis-id').getValue();
        
        var qryParam = '';
        if(type==1){
            qryParam = 'type=1&oldimei='+oldImei+'&newimei='+newImei+'&bisid='+bisId;
        }
        else{
            var changeBisId = Ext.getCmp('select-swap-bis-id').getValue();
            qryParam = 'type=2&oldimei='+oldImei+'&newimei='+newImei+'&bisid='+changeBisId; 
        }
        
        Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'SwapIMEIService/swapIMEI?'+qryParam,
                method: 'GET',
                success: function (response, btn) {
                    var responseData = Ext.decode(response.responseText);
                   
                     if(responseData==1){
                        Ext.Msg.alert("Success", "IMEI is Successfully changed.");
                        var frm = btnU.up('window');
                        frm.close();
                    }
                    if(responseData==2){
                        Ext.Msg.alert("Error", "IMEI is already existing.");
                    }
                    if(responseData==3){
                        Ext.Msg.alert("Error", "IMEI is invalid to change the shop.");
                    }
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
    }
    
});

