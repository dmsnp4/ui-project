

Ext.define('singer.controller.RepairPaymentsApproving', {
    extend: 'Ext.app.Controller',
    alias: 'widget.wrkOrdercontroller',
    controllers: ['CoreController'],
    stores: [
        'RepairPayment',
        'WorkOrderPaymentListwithPrice',
        'LoggedInUserData'
    ],
    views: [
        'ServiceModule.RepairPaymentsApprove.MainGrid',
        'ServiceModule.RepairPaymentsApprove.ApprovalForm',
        'ServiceModule.RepairPaymentsApprove.DeductAmountInput',
        'ServiceModule.RepairPaymentsApprove.SelectOrUnselectWindow',
    ],
    temp: null,
    rowIndex: null,
    refs: [
        {
            ref: 'MainGrid',
            selector: 'RepairPaymentsApproveGrid'
        },
        {
            ref: 'ApprovalForm',
            selector: 'appove'
        },
        {
            ref: 'DeductAmountInput',
            selector: 'deductAdd'
        },
        {
            ref: 'SelectOrUnselectWindow',
            selector: 'SlectionW'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'RepairPaymentsApproveGrid': {
                added: this.initializeGrid
            },
            'RepairPaymentsApproveGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'wrkOrderOpeningGrid  ': {
                onView: this.onView
            },
            'RepairPaymentsApproveGrid': {
                aprove: this.openViewWindow
            },
            'RepairPaymentsApproveGrid ': {
                authorize: this.openApproveWindow
            },
            ' RepairPaymentsApproveGrid ': {
                PrintReceiptForWOSummary: this.PrintReceiptForWOSummary
            },
            'appove button[action=exit_form]': {
                click: this.cancelAproveWindow
            },
            'deductAdd button[action=submit]': {
                click: this.onAddDeduct
            },
            'appove ': {
                unselectt: this.openSelectOrUnselectWindow
            },
            'appove  ': {
                viewmore: this.openViewMoreWindow
            },
            ' appove ': {
                deductAmt: this.onDeductAdd
            },
            '  appove ': {
                onWorkorderChange: this.onChangeWorkOrder
            },
            '   appove ': {
                onWorkorderDeduct: this.onDeductWorkOrder
            },
            'SlectionW button[action=submit_selection]': {
                click: this.onSaveBtnWorkOrderViewMore
            },
            'SlectionW button[action=submit_cancel]': {
                click: this.onCancelBtnWorkOrderViewMore
            },
            'appove button[action=accept]': {
                click: this.onApproveBtnClicked
            },
            'appove button[action=tempsave]': {
                click: this.onTempSaveBtnClicked
            },
            'appove button[action=settle]': {
                click: this.onSettleBtnClicked
            },
            'appove button[action=finish]': {
                click: this.onChequeInsertBtnClicked
            },
//            'appove button[action=reject]': {
//                click: this.rejectPaymentBtnClicked
//            },
            'appove button[action=close]': {
                click: this.onCancelBtnWorkOrderViewMore
            }
        });
    },
    onCancelBtnWorkOrderViewMore: function (btn) {
        var window = btn.up('window');
        window.close();
    },
    PrintReceiptForWOSummary: function (payid) {

        var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Excel_RepairPaymentSummaryWithGrandTotal.rptdesign&RepPay=" + payid;
        localStorage.setItem('myReptExcelURI', urii);
        this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=RepairPaymentSummaryWithGrandTotal.rptdesign&RepPay=" + payid, 'Payment Summary');


//        var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=RepairPaymentSummaryWithGrandTotal.rptdesign&PayID=" + payid;
//        console.log('URI', URI);
//
//        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepairPaymentSummaryWithGrandTotal.rptdesign&RepPay=" + payid, 'Payment Summary');

    },
    onSaveBtnWorkOrderViewMore: function (btn) {
        var form = btn.up('form').getForm();
        var record = form.getRecord();
        this.onChangeWorkOrder(form.findField("status").getValue(), record, 1);
        btn.up('window').close();
    },
//    rejectPaymentBtnClicked: function(btn) {
//        var form = this.getApprovalForm().down("form").getForm();
//        if (Ext.isEmpty(form.findField("remarks").getValue())) {
//            Ext.MessageBox.show({
//                title: "Remarks Required.",
//                msg: "Please Insert remarks before reject Repair Payment Approve",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.ERROR
//            });
//        } else {
//            var me = this;
//            var form = btn.up('form').getForm();
//            var grid = btn.up('form').down('grid');
//            var record = form.getRecord();
//
//            Ext.Msg.confirm('Reject Repair Payment Record', 'Are you sure you want to reject current repair payment?', function(btn) {
//                if (btn === 'yes') {
//                    record.set("status", 3);
//                    record.set("paymentPrice", parseFloat(form.findField("paymentPrice").getValue()));
//                    var detailsStore = grid.getStore();
//                    ////console.log(record, grid.getStore());
//                    var dataObj = record.data;
//                    var detailRecords = [];
//                    detailsStore.each(function(rec, index) {
//                        detailRecords.push(rec.data);
//                    });
//                    dataObj.woPaymentDetials = detailRecords;
//                    //console.log(dataObj, form.findField("paymentPrice").getValue());
//                    me.authorizeWithRemoteServer(dataObj, "Rejecting Repair Payment", "Repair Payment Rejected", "", "PaymentApproves/addApprovePayment");
//                }
//            });
//        }
//    },
    openViewMoreWindow: function (view, record, rowIndex) {
        var win = Ext.widget('SlectionW');
        win.down("form").getForm().loadRecord(record);
        if (this.getApprovalForm().down("form").getForm().findField("status").getValue() == '1') {
            Ext.getCmp("select_or_unselect_radio_grp").setVisible(true);
        }
        win.setTitle('Add or Remove Work Order');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            modal: true,
//            constrain: true,
//            title: '',
//            items: [form]
//        });
        win.show();
    },
    onChangeWorkOrder: function (checked, record, rowIndex) {
        var form = this.getApprovalForm().down("form").getForm();
        var grandTotal = form.findField("paymentPrice").getValue();
        var serviceAmount = record.get("amount");
        var lineDeduction = record.get("deductionAmount");

//        console.log(record);
//        console.log(this);
        console.log("grandTotal", grandTotal);
        console.log("serviceAmount", serviceAmount);
        console.log("lineDeduction", lineDeduction);

        console.log(Ext.getStore("WorkOrderPaymentListwithPrice"));
        var rowTotal = parseFloat(serviceAmount) - parseFloat(lineDeduction);
        console.log("total", rowTotal);

        var nbtValueOfRow = 0;
        var vatValueOfRow = 0;
        var isVatApplicable = (localStorage.getItem("isVatApplicable") == 1) ? true : false;
        var isNbtApplicable = (localStorage.getItem("isNbtApplicable") == 1) ? true : false;

        var deductAmount = localStorage.getItem("deductAmount");
        var deductAmountApply = localStorage.getItem("deductAmountApply");


        var totalWoSumWithInitDeduct = localStorage.getItem("totalWoSumWithInitDeduct");

        console.log("totalWoSumWithInitDeduct", totalWoSumWithInitDeduct);
        console.log("rowTotal", rowTotal);

//
//        if (isNbtApplicable) {
//            nbtValueOfRow = singer.AppParams.NBT * parseFloat(rowTotal);
//        }
//
//        if (isVatApplicable) {
//            vatValueOfRow = singer.AppParams.TAX * parseFloat(rowTotal);
//        }
//
//        console.log("NBT" + nbtValueOfRow + "VAT" + vatValueOfRow);
//
//        var totalAlterValue = rowTotal + nbtValueOfRow + vatValueOfRow;
//

        if (checked) {
            record.set("status", 1);
            console.log("checked");
            //  var newTotal = parseFloat(grandTotal) + parseFloat(totalAlterValue);
            var newTotal = parseFloat(totalWoSumWithInitDeduct) + parseFloat(rowTotal);
//            var totTax = 0;

            if (isNbtApplicable) {
                newTotal = (newTotal * parseFloat(1 + singer.AppParams.NBT));
            }

            if (isVatApplicable) {
                newTotal = (newTotal * parseFloat(1 + singer.AppParams.TAX));
            }

            console.log("TTTOOTAL  " + newTotal);

//            newTotal = newTotal + parseFloat(newTotal * singer.AppParams.NBT) + parseFloat(newTotal * singer.AppParams.TAX);
            form.findField("paymentPrice").setValue(newTotal);
            console.log("TTTTT", newTotal);

            localStorage.setItem("totalWoSumWithInitDeduct", parseFloat(totalWoSumWithInitDeduct) + parseFloat(rowTotal));

//            form.findField("paymentPrice").setValue(parseFloat(form.findField("paymentPrice").getValue()) + parseFloat(record.get("deductionAmount")));
        } else {
            console.log("unchecked")
            record.set("status", 0);
            var dedVat = "";
            var dedNbt = "";
            // var newTotal = parseFloat(grandTotal) - parseFloat(totalAlterValue);
            //    var newTotal = parseFloat(totalWoSumWithInitDeduct) - parseFloat(totalAlterValue);
//            var totTax = 0;
            var newTotal = parseFloat(totalWoSumWithInitDeduct) - parseFloat(rowTotal);

            console.log("totalWoSumWithInitDeduct", totalWoSumWithInitDeduct);
            console.log("rowTotal", rowTotal);

            if (isNbtApplicable) {
                newTotal = (newTotal * parseFloat(1 + singer.AppParams.NBT));
            }

            if (isVatApplicable) {
                newTotal = (newTotal * parseFloat(1 + singer.AppParams.TAX));
            }

//            newTotal = newTotal + parseFloat(newTotal * singer.AppParams.NBT) + parseFloat(newTotal * singer.AppParams.TAX);

            console.log("TTTTT2222 ", newTotal);

            localStorage.setItem("totalWoSumWithInitDeduct", (totalWoSumWithInitDeduct) - parseFloat(rowTotal));

            if (newTotal <= 0) {
                newTotal = 0;
            }
            console.log("newTotalnewTotal", newTotal);
            form.findField("paymentPrice").setValue(newTotal);

//            form.findField("paymentPrice").setValue(parseFloat(form.findField("paymentPrice").getValue()) - parseFloat(record.get("deductionAmount")));
        }
    },
//    onDeductWorkOrder: function (checked, record, rowIndex) {
//        var form = this.getApprovalForm().down("form").getForm();
//        console.log(record);
//        
//        if (checked) {
//            record.set("status", 1);
//            form.findField("paymentPrice").setValue(parseFloat(form.findField("paymentPrice").getValue()) - parseFloat(record.get("deductionAmount")));
//            
//        } else {
//            record.set("status", 0);
//            form.findField("paymentPrice").setValue(parseFloat(form.findField("paymentPrice").getValue()) + parseFloat(record.get("deductionAmount")));
//        }
//        
//    },
    onDeductWorkOrder: function (me, record) {
        var me1 = this;
        var nbt = singer.AppParams.NBT;
        var vat = singer.AppParams.TAX;

        Ext.Msg.confirm('Cancel Amount Deduction.', 'Are you sure you want to Cancel the <br> Amount deduction?', function (btn) {
            if (btn === 'yes') {

//                var store = me1.getWorkOrderPaymentListwithPriceStore();
//                var dedRec = store.findRecord('workOrderNo', workOrderNo);

                var status = record.get("status");

                var curTotalValueWithDed = localStorage.getItem("totalWoSumWithInitDeduct");

                curPayPrice = parseFloat(curTotalValueWithDed) + parseFloat(record.get('deductionAmount'));

                localStorage.setItem("totalWoSumWithInitDeduct", curPayPrice);

                var isNbtApplicable = localStorage.getItem("isNbtApplicable");
                var isVatApplicable = localStorage.getItem("isVatApplicable");



                var me = this;
                var form = me1.getApprovalForm().down("form").getForm();
                var store = me1.getWorkOrderPaymentListwithPriceStore();
                var workOrderNo = record.data.workOrderNo;

                var dedAmt = parseFloat(record.data.deductionAmount);

//                var curAmount = parseFloat(form.findField("paymentPrice").getValue()) + dedAmt;

                var dedAmount = 0;
                var paymentId = me1.record.data.paymentId;

                var dedRec = store.findRecord('workOrderNo', workOrderNo);
                var chkBoxStatus = dedRec['data']['status'];

                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'PaymentApproves/addWorkOrderGridUpdate?workOrderNo=' + workOrderNo + '&paymentId=' + paymentId + '&deductionAmount=' + dedAmount + "&nbt=" + nbt + "&vat=" + vat + "&status=" + status + "&currentDeduction=" + curPayPrice,
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    success: function (response) {
                        console.log('success');

                        store['data']['items'][dedRec.index].set('deductionAmount', dedAmount);

                        //  record.set("deductionAmount", 0);
                        //     store.load();

                        if (chkBoxStatus == 1) {

                            if (isNbtApplicable == 1) {
                                curPayPrice = curPayPrice * parseFloat(1 + singer.AppParams.NBT);
                            }

                            if (isVatApplicable == 1) {
                                curPayPrice = curPayPrice * parseFloat(1 + singer.AppParams.TAX);
                            }

                            console.log(curPayPrice);


                            form.findField("paymentPrice").setValue(curPayPrice);

                        }

                    },
                    failure: function () {
                        console.log('failure');
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });


            }
        });


    },
    onDeductAdd: function (record, rowIndex) {
        var me = this;
        console.log('onDeductAdd');
        me.rowIndex = rowIndex.data;
        console.log('rowIndex', me.rowIndex.deductionAmount);

        localStorage.setItem("deductAmount", me.rowIndex.deductionAmount);

        var win = Ext.widget('deductAdd');
        win.show();
    },
    authorizeWithRemoteServer: function (dataObj, loadingMsg, successMsg, onErrorMsg, uri) {
        Ext.getBody().mask(loadingMsg, 'loading');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var me = this;
        var store = this.getRepairPaymentStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + uri,
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    singer.LayoutController.notify("Operation Success", successMsg);

                } else {
                    singer.LayoutController.createErrorPopup("Error Occured", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
                me.getApprovalForm().close();
                me.getController('CoreController').loadBindedStore(store);
            },
            failure: function () {
                Ext.getBody().unmask();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onChequeInsertBtnClicked: function (btn) {
        console.log("sdsfdfedfdf");
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();

        Ext.Msg.confirm('Apply Cheque Collection to Repair Payment Record', 'Are you sure you want to Apply Cheque to current repair payment?', function (btn) {
            if (btn === 'yes') {
                record.set("status", 5);
                record.set("chequeNo", form.findField("chequeNo").getValue());
                record.set("chequeDetails", form.findField("chequeDetails").getValue());
                record.set("chequeDate", Ext.Date.format(form.findField("chequeDate").getValue(), "Y-m-d"));
                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;
                me.authorizeWithRemoteServer(dataObj, "Appling Cheque to Repair Payment", "Cheque Aplied to Repair Payment", "", "PaymentApproves/addChequeDetails");
            }
        });
    },
    onSettleBtnClicked: function (btn) {
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();

        Ext.Msg.confirm('Settle Repair Payment Record', 'Are you sure you want to settle current repair payment?', function (btn) {
            if (btn === 'yes') {
                record.set("status", 4);
                record.set("settleAmount", parseFloat(form.findField("settleAmount").getValue()));
                record.set("poNumber", form.findField("poNumber").getValue());
                record.set("poDate", Ext.Date.format(form.findField("poDate").getValue(), "Y-m-d"));
                record.set("remarks", form.findField("remarks").getValue());
                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;
                me.authorizeWithRemoteServer(dataObj, "Settle Repair Payment", "Repair Payment Settled", "", "PaymentApproves/addSettleAmount");
            }
        });
    },
    onApproveBtnClicked: function (btn) {
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();
        console.log('record', record);


        Ext.Msg.confirm('Accept Repair Payment Record', 'Are you sure you want to approve current repair payment?', function (btn) {
            if (btn === 'yes') {
                var totalAmountWithDeduct = localStorage.getItem("totalWoSumWithInitDeduct");
                record.set("nbtValue", singer.AppParams.NBT);
                record.set("vatValue", singer.AppParams.TAX);
                record.set("status", 2);
                record.set("totalAmountWithDeduct", totalAmountWithDeduct);
                record.set("paymentPrice", parseFloat(form.findField("paymentPrice").getValue()));
                
                record.set("remarks", form.findField("remarks").getValue());
                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    delete rec.data[''];
                    console.log(rec.data[''], ' - ZZZZZZZZZZ: ', rec.data)
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;
                //console.log(dataObj, form.findField("paymentPrice").getValue());
                me.authorizeWithRemoteServer(dataObj, "Approving Repair Payment", "Repair Payment Approved", "", "PaymentApproves/addApprovePayment");
            }
        });


//        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var win = btn.up('window');
//        win.destroy();
//        Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'PaymentApproves/addApprovePayment',
//                method: 'POST',
//                headers: {
//                    userId: loginData.data.getAt(0).get('userId'),
//                    room: loginData.data.getAt(0).get('room'),
//                    department: loginData.data.getAt(0).get('department'),
//                    branch: loginData.data.getAt(0).get('branch'),
//                    countryCode: loginData.data.getAt(0).get('countryCode'),
//                    division: loginData.data.getAt(0).get('division'),
//                    organization: loginData.data.getAt(0).get('organization'),
//                    system: sys,
//                    'Content-Type': 'application/json'
//                },
//                params: JSON.stringify(win.getValues()),
//                success: function(response, btn) {
//                    var responseData = Ext.decode(response.responseText);
//                    if (responseData.returnFlag === '1') {
//                        win.close();
//                    } else {
//                        singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                    }
//
//                },
//                failure: function() {
//                    Ext.Msg.hide();
//                    Ext.Msg.alert("Error in connection", "Please try again later");
//                }
//            });
    },
//    submitSelection: function(btn, view, record, rowIndex) {
//        var chkk = Ext.getCmp('chkbx').getValue();
//        //console.log(chkk);
//        var win = btn.up('form').up('form');
////        //console.log(Ext.getStore('WorkOrderPaymentListwithPrice').data.items[0].data);
//        if (chkk === true) {
//            //console.log('true');
//            this.str.removeAt(this.rwIndx);
//            win.destroy();
//        } else {
//            //console.log('false');
//        }
//
//    },
    openSelectOrUnselectWindow: function (view, record, rowIndex) {
        var me = this;
//        this.str = view.getStore();
//        this.rwIndx = rowIndex;
//        //console.log(view.getStore().removeAt(rowIndex));
//        var actionTake = record.data.actionTake;
//        var amount = record.data.amount;
//        var defectDesc = record.data.defectDesc;
//        var defectType = record.data.defectType;
//        var imeiNo = record.data.imeiNo;
//        var laborCost = record.data.laborCost;
//        var nonWarrantyVerifType = record.data.nonWarrantyVerifType;
//        var paymentDate = record.data.paymentDate;
//        var paymentId = record.data.paymentId;
//        var referenceNo = record.data.referenceNo;
//        var remarks = record.data.remarks;
//        var seqNo = record.data.seqNo;
//        var spareCost = record.data.spareCost;
//        var order = record.data.order;
//        var status = record.data.status;
//        var tecnician = record.data.tecnician;
//        var warrantyVerifType = record.data.warrantyVerifType;
//        var workOrderNo = record.data.workOrderNo;
        ////console.log(imeiNo);

//        var form = Ext.widget('SlectionW');
//        form.getForm().loadRecord(record);
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            modal: true,
//            constrain: true,
//            title: 'Add Or Remove Work Order',
//            items: [form]
//        });
//        win.show();
//        Ext.getCmp('WOno').setValue(workOrderNo);
//        Ext.getCmp('Wverification').setValue(warrantyVerifType);
//        Ext.getCmp('wrnty').setValue("NO DATA");
//        Ext.getCmp('nonWrnty').setValue(nonWarrantyVerifType);
//        Ext.getCmp('techn').setValue(tecnician);
//        Ext.getCmp('dfct').setValue(defectType);
//        Ext.getCmp('action').setValue(actionTake);
//        Ext.getCmp('imeei').setValue(imeiNo);
//        Ext.getCmp('inviceNo').setValue("NO DATA");
//        Ext.getCmp('sprPrts').setValue(spareCost);
//        Ext.getCmp('ThrdInvNo').setValue("NO DATA");
//        Ext.getCmp('ThrdPrice').setValue("NO DATA");
    },
    cancelAproveWindow: function (btn) {
        var window = btn.up('window');
        window.close();
    },
    onAddDeduct: function (btn) {
        var me = this;
        var store = me.getWorkOrderPaymentListwithPriceStore();
        console.log('onAddDeduct', me.rowIndex.workOrderNo);
        var workOrderNo = me.rowIndex.workOrderNo;
        var dedAmount = Ext.getCmp('deductionAmount').getValue();
        console.log("dedAmount ", dedAmount);

        var nbt = singer.AppParams.NBT;
        var vat = singer.AppParams.TAX;

        var oldDeductAmount = parseFloat(localStorage.getItem("deductAmount"));

        var dedRec = store.findRecord('workOrderNo', workOrderNo);

        console.log("oldDeductAmount ", oldDeductAmount);
        console.log("ActiveChangeColumn ", dedRec.data['status']);
        dedAmount = parseFloat(dedAmount);

        var status = dedRec.data['status'];
        var paymentId = me.record.data.paymentId;
        var window = btn.up('window');
        var status = me.rowIndex.status;

        console.log("SSSSS", status);

        var totalWoSumWithInitDeduct = localStorage.getItem("totalWoSumWithInitDeduct");
        var isNbtApplicable = localStorage.getItem("isNbtApplicable");
        var isVatApplicable = localStorage.getItem("isVatApplicable");


//        ##############################


        var storeApprovePayment = Ext.getStore("WorkOrderPaymentListwithPrice");
        //set debit number for spesific row and col
        storeApprovePayment['data']['items'][dedRec.index].set('deductionAmount', dedAmount);

        var curPayPrice = 0;
        var curPayPrice2 = 0;
        if (status == 1) {
            console.log(me.rowIndex);
//                    me.rowIndex.deductionAmount.setValue(dedAmount);

            var form = me.getApprovalForm().down("form").getForm();

//                    var grandTotalBeforeDeduct = parseFloat(form.findField("paymentPrice").getValue());

            console.log("OLDDDD", oldDeductAmount);
            console.log("NEWWWW", dedAmount);

            if (parseFloat(oldDeductAmount) > dedAmount)
            {
                var finalDeductAmont = parseFloat(oldDeductAmount) - parseFloat(dedAmount);

                curPayPrice = parseFloat(totalWoSumWithInitDeduct) + parseFloat(finalDeductAmont);

                localStorage.setItem("totalWoSumWithInitDeduct", curPayPrice);

                console.log("paymentPrice", form.findField("paymentPrice").getValue());

                if (isNbtApplicable == 1) {
                    curPayPrice = curPayPrice * parseFloat(1 + singer.AppParams.NBT);
                }


                if (isVatApplicable == 1) {
                    curPayPrice = curPayPrice * parseFloat(1 + singer.AppParams.TAX);
                }

                if (curPayPrice < 0) {
                    form.findField("paymentPrice").setValue(0);
                } else {
                    form.findField("paymentPrice").setValue(curPayPrice);
                }

            } else {


                var finalDeductAmont = parseFloat(dedAmount) - parseFloat(oldDeductAmount);

                curPayPrice = parseFloat(totalWoSumWithInitDeduct) - parseFloat(finalDeductAmont);
                curPayPrice2 = curPayPrice;

                localStorage.setItem("totalWoSumWithInitDeduct", curPayPrice);

                console.log("paymentPrice", form.findField("paymentPrice").getValue());

                if (isNbtApplicable ==1) {
                    curPayPrice = curPayPrice * parseFloat(1 + singer.AppParams.NBT);
                }


                if (isVatApplicable ==1) {
                    curPayPrice = curPayPrice * parseFloat(1 + singer.AppParams.TAX);
                }

                if (curPayPrice < 0) {
                    form.findField("paymentPrice").setValue(0);
                } else {
                    form.findField("paymentPrice").setValue(curPayPrice);
                }

//                        var curPayPrice = grandTotalBeforeDeduct - dedAmount;
//
//                        console.log("paymentPrice", form.findField("paymentPrice").getValue());
//
//                        if (curPayPrice < 0) {
//                            form.findField("paymentPrice").setValue(0);
//                        } else {
//                            form.findField("paymentPrice").setValue(grandTotalBeforeDeduct - finalDeductAmont);
//                        }

            }



        }



//        ################################


        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'PaymentApproves/addWorkOrderGridUpdate?workOrderNo=' + workOrderNo + '&paymentId=' + paymentId + '&deductionAmount=' + dedAmount + "&nbt=" + nbt + "&vat=" + vat + "&status=" + status + "&currentDeduction=" + curPayPrice2,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            success: function (response) {
                console.log('success');

                window.close();


            },
            failure: function () {
                console.log('failure');
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    openApproveWindow: function (view, record) {
        var me = this;
        me.record = record;
        console.log('openApproveWindow11111', record.data['deductionAmount']);

        var deductAmount = record.data['deductionAmount'];

        localStorage.setItem("deductAmount", deductAmount);
        localStorage.setItem("deductAmountApply", 1);

//        var frnName = record.data.setviceName;
//        //console.log(record.data.setviceName);
//        singer.AppParams.TEMP = record.get('paymentId');
        var store = this.getWorkOrderPaymentListwithPriceStore();
        ////console.log(store.getProxy().api.read);
        console.log("sttttt", store);

        localStorage.setItem('PayApprovePayId', record.get("paymentId"));

        if (record.get("status") === 1) {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails?paymentId=' + record.get("paymentId");
        } else {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails?paymentId=' + record.get("paymentId") + "&status=1";
        }

//        this.getController('CoreController').loadBindedStore(store);
//        store.load();
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                // the operation object
                // contains all of the details of the load operation
                console.log("ffg", records[0].data['totalAmountWithDeduct']);

                var vatApplicable = records[0].data['vatApplicable'];
                var nbtApplicable = records[0].data['nbtApplicable'];
                var initDeduct = records[0].data['initDeduct'];
                var lineDeductSum = records[0].data['lineDeductSum'];

                console.log("lineDeductSum", lineDeductSum);

                localStorage.setItem("isVatApplicable", vatApplicable);
                localStorage.setItem("isNbtApplicable", nbtApplicable);
                //localStorage.setItem("isNbtApplicable", initDeduct);

                var totalSum = parseFloat(store.sum("amount")) - parseFloat(store.sum("deductionAmount")) - parseFloat(initDeduct);

                localStorage.setItem("totalWoSumWithInitDeduct", "");

                if (lineDeductSum == 100000000) {
                    localStorage.setItem("totalWoSumWithInitDeduct", totalSum);
                } else {
                    localStorage.setItem("totalWoSumWithInitDeduct", lineDeductSum);

                }
                console.log("store...", totalSum);

                //  Ext.getCmp("paymentPricePaymentApprove").setValue(totalWithDeduction);
            }
        });

        //  var newTotalWithDeduct = store.data.items[0].data;

        //   console.log("totalAmountWithDeduct",newTotalWithDeduct);


        var win = Ext.widget('appove');
        var form = win.down("form");
        var formBasic = form.getForm();
        formBasic.loadRecord(record);
        //console.log(record.get("poDate"), record.get("chequeDate"));
        formBasic.findField("chequeDate").setValue(record.get("chequeDate"));
        //    Ext.getCmp("paymentPricePaymentApprove").setValue("");
        formBasic.findField("poDate").setValue(record.get("poDate"));

        var windowTitle = "Repair Payments Approving";
        if (record.get("status") === 1) {
            Ext.getCmp('Save-Without-Approve').show();
            Ext.getCmp("ActiveChangeColumn").hidden = false;
            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
            Ext.getCmp("repair_payment_approve_form_approve").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_reject").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", false, false],
                ["paymentPrice", true, true],
                ["remarks", false, true]
            ], true);
        } else if (record.get("status") === 2) {
            Ext.getCmp('Save-Without-Approve').hide();
            windowTitle = "Repair Payments Settlement";
            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
            Ext.getCmp("repair_payment_approve_form_settle").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", false, false],
                ["poNumber", false, false],
                ["poDate", false, false],
                ["remarks", false, true]
            ], false);
        } else if (record.get("status") === 4) {
            Ext.getCmp('Save-Without-Approve').hide();
            windowTitle = "Repair Payments Cheque Payment Approving";
            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
            Ext.getCmp("repair_payment_approve_form_save").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", false, false],
                ["chequeDate", false, false],
                ["chequeDetails", false, true]
            ], false);
        } else if (record.get("status") === 5) {
            Ext.getCmp('Save-Without-Approve').hide();
            windowTitle = "Completed Repair Payments Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", true, true],
                ["chequeDate", true, true],
                ["chequeDetails", true, true]
            ], true);

            Ext.getCmp("ActiveChangeColumn").setVisible(false);
            Ext.getCmp("cancel-deduct-Amount-action").hidden = true;
            Ext.getCmp("deduct-Amount-action").hidden = true;

        } else if (record.get("status") === 3) {
            Ext.getCmp('Save-Without-Approve').hide();
            windowTitle = "Rejected Repair Payments Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", true, true],
                ["chequeDate", true, true],
                ["chequeDetails", true, true]
            ], true);
        }
        //1st stage form
        function showAndEnableFormFields(fields, showGrid) {
            form.down("grid").setVisible(showGrid);
            form.down("label").setVisible(showGrid);
            for (var i = 0; i < fields.length; i++) {
                formBasic.findField(fields[i][0]).allowBlank = fields[i][2];
                formBasic.findField(fields[i][0]).setVisible(true);
                formBasic.findField(fields[i][0]).setReadOnly(fields[i][1]);
            }
        }
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            //iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: windowTitle,
//            items: [form]
//        });
        win.setTitle(windowTitle);
        win.show();


        console.log('nbt', singer.AppParams.NBT);
        console.log('vat', singer.AppParams.TAX);
        console.log('cmp', Ext.getCmp("nbtValue"));

//        Ext.getCmp("nbtValue").setValue(parseFloat(singer.AppParams.NBT));
//        Ext.getCmp("vatValue").setValue(parseFloat(singer.AppParams.TAX));

//        Ext.getCmp('Franchise').setValue(frnName);
    },
    openViewWindow: function (view, record) {
        console.log('openViewWindow',record.get("paymentId"));
//        var frnName = record.data.setviceName;
//        //console.log(record.data.setviceName);
//        singer.AppParams.TEMP = record.get('paymentId');
        var store = this.getWorkOrderPaymentListwithPriceStore();
        ////console.log(store.getProxy().api.read);
        if (record.get("status") == 1) {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails?paymentId=' + record.get("paymentId");
        } else {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails?paymentId=' + record.get("paymentId") + "&status=1";
        }

//        this.getController('CoreController').loadBindedStore(store);
//        store.load();
        store.load();
        var win = Ext.widget('appove');
        var form = win.down("form");
        var formBasic = form.getForm();
        formBasic.loadRecord(record);
        //console.log(record.get("poDate"), record.get("chequeDate"));
        formBasic.findField("chequeDate").setValue(record.get("chequeDate"));
        formBasic.findField("poDate").setValue(record.get("poDate"));

        var windowTitle = "Repair Payments Approving";
        if (record.get("status") === 1) {
            Ext.getCmp("ActiveChangeColumn").hidden = true;
            Ext.getCmp("deduct-Amount-action").hidden = true;
            Ext.getCmp("cancel-deduct-Amount-action").hidden = true;
            Ext.getCmp("Save-Without-Approve").hidden = true;


            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_approve").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_reject").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", false, false],
                ["paymentPrice", true, true],
                ["remarks", false, true]
            ], true);


//        Ext.getCmp("nbtValue").setValue(parseFloat(singer.AppParams.NBT));
//        Ext.getCmp("vatValue").setValue(parseFloat(singer.AppParams.TAX));

        } else if (record.get("status") === 2) {
            Ext.getCmp("Save-Without-Approve").hidden = true;


            windowTitle = "Repair Payments Settlement";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_settle").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", false, false],
                ["poNumber", false, false],
                ["poDate", false, false],
                ["remarks", false, true]
            ], false);
        } else if (record.get("status") === 4) {
            Ext.getCmp("Save-Without-Approve").hidden = true;
            windowTitle = "Repair Payments Cheque Payment Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);

            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", false, false],
                ["chequeDate", false, false],
                ["chequeDetails", false, true]
            ], false);
        } else if (record.get("status") === 5) {
            console.log("5555555555555555555")
            windowTitle = "Completed Repair Payments Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
            console.log("dfdfed esf df ");

            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", true, true],
                ["chequeDate", true, true],
                ["chequeDetails", true, true]
            ], true);


            Ext.getCmp("Save-Without-Approve").setVisible(false);
            Ext.getCmp("ActiveChangeColumn").setVisible(false);
            Ext.getCmp("cancel-deduct-Amount-action").hidden = true;
            Ext.getCmp("deduct-Amount-action").hidden = true;


        } else if (record.get("status") === 3) {
            windowTitle = "Rejected Repair Payments Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", true, true],
                ["chequeDate", true, true],
                ["chequeDetails", true, true]
            ], true);
        }
        //1st stage form
        function showAndEnableFormFields(fields, showGrid) {
            form.down("grid").setVisible(showGrid);
            form.down("label").setVisible(showGrid);
            for (var i = 0; i < fields.length; i++) {
                formBasic.findField(fields[i][0]).allowBlank = fields[i][2];
                formBasic.findField(fields[i][0]).setVisible(true);
                formBasic.findField(fields[i][0]).setReadOnly(fields[i][1]);
            }
        }
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            //iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: windowTitle,
//            items: [form]
//        });
        win.setTitle(windowTitle);
        win.show();
//        Ext.getCmp('Franchise').setValue(frnName);
    },
    initializeGrid: function (grid) {
        var store = this.getRepairPaymentStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'wrkOrderview', 'Work Order Opening details of WORK ORDER NO :' + record.get('workOrderNo'));
    },
    onTempSaveBtnClicked: function (btn) {
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();


        record.set("nbtValue", singer.AppParams.NBT);
        record.set("vatValue", singer.AppParams.TAX);
        
        var lastAmountWithDeduction = localStorage.getItem("totalWoSumWithInitDeduct");
        
        record.set("totalAmountWithDeduct", lastAmountWithDeduction);
        console.log('record', record);

        Ext.Msg.confirm('Save Payment Record', 'Are you sure you want to save current repair payment <br> without Approving?', function (btn) {
            if (btn === 'yes') {

                var totalWoSumWithInitDeduct = localStorage.getItem("totalWoSumWithInitDeduct");

                record.set("status", 1);
                record.set("paymentPrice", parseFloat(form.findField("paymentPrice").getValue()));
                record.set("remarks", form.findField("remarks").getValue());
                record.set("lineDeductSum", totalWoSumWithInitDeduct);


                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    delete rec.data[''];
                    console.log(rec.data[''], ' - ZZZZZZZZZZ: ', rec.data)
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;

                me.authorizeWithRemoteServer(dataObj, "Approving Repair Payment", "Repair Payment Saved without Approved", "", "PaymentApproves/addApprovePayment");
            }
        });


    }

});

