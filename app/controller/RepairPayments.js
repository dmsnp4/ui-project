Ext.define('singer.controller.RepairPayments', {
    extend: 'Ext.app.Controller',
    alias: 'widget.rpirPaymentsController',
    controllers: ['CoreController'],
    stores: [
        'RepairPayments',
        'RepairPaymentWithPrice',
        'LoggedInUserData',
        'WOList',
        'RepairPaymentSend',
        'RepairPaymentDetl',
        'WorkOrderMaintainAdd',
        'RepairPaymentWithPriceSelected'
    ],
    views: [
        'ServiceModule.RepairPaymentss.MainGrid',
        'ServiceModule.RepairPaymentss.View',
        'ServiceModule.RepairPaymentss.AddRepairPayments',
        'ServiceModule.RepairPaymentss.AddNewRep'
    ],
    refs: [
        {
            ref: 'MainGrid',
            selector: 'repairPayGrid'
        },
        {
            ref: 'View',
            selector: 'viewRepairPayments'
        },
        {
            ref: 'AddRepairPayments',
            selector: 'addRpairPayment'
        },
        {
            ref: 'AddNewRep',
            selector: 'addnewrep'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'repairPayGrid': {
                added: this.initializeGrid
            },
            'repairPayGrid  ': {
                view: this.onView
            },
            ' repairPayGrid  ': {
                print: this.onPrint
            },
            'repairPayGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'viewRepairPayments button[action=view_Repair_payments_cancel]': {
                click: this.repairPaymentsViewCancel
            },
            'repairPayGrid button[action=repair_payment]': {
                click: this.OpenrepairPaymentsWindow
            },
            'addRpairPayment button[action=cancel_repair_payment_Form]': {
                click: this.addrepairPaymentsCancel
            },
            'addRpairPayment button[action=submit_repair_payment_form]': {
                click: this.addNewRepairSubmit
            },
            'viewRepairPayments button[action=print_payment_memo]': {
                click: this.printPaymentMemo
            },
            'addRpairPayment button[action=select_Work_Order]': {
                click: this.openSelectWorkOrderWindow
            },
            'addRpairPayment grid': {//new to catch delete action column
                on_open_delete: this.clickOnSelectedWorkOrder
            },
            'addnewrep': {//new to catch action column
                selectWrkOrder: this.clickOnSelectWorkOrder
            },
        });
    },
    printPaymentMemo: function (btn, record) {
//        var form = Ext.widget('repairPayGrid');
//        console.log('record', record);
        var store = this.getRepairPaymentDetlStore();
//        console.log('STORE', store);
        var payID = store.data.items[0].data.paymentId;
//        console.log('payID', payID);

        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepairPaymentMemo.rptdesign&RepPay=" + payID, 'Repair Payment Memo');
    },
    onPrint: function (view, record) {
        var payment_id = record.data.paymentId;
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepairPaymentSummary.rptdesign&PayID=" + payment_id, "Repair Payment History");
    },
    openSelectWorkOrderWindow: function () {
        console.log('openSelectWorkOrderWindow');
        //var store = this.getWorkOrderOpeningStore();
        var store = this.getRepairPaymentWithPriceStore();
        console.log(store);
        this.getController('CoreController').loadBindedStore(store);

        var temp = Ext.getStore('RepairPaymentWithPrice');

        console.log('store', temp);
//
        var form = Ext.widget('addnewrep');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        win.show();
    },
    addNewRepairSubmit: function (btn) {
        console.log('addNewRepairSubmit');
        var me = this;
        var windw = btn.up('window');
        var grid = Ext.getCmp('RepairPayGrid');
        var getData = grid.getStore();
        var tempData = [];
        var store = this.getWOListStore();
        var storeSelected = Ext.getStore('RepairPaymentWithPriceSelected');//  me.getRepairPaymentWithPriceSelectedStore();
        var storePrice = me.getRepairPaymentWithPriceStore();
        var form = btn.up('form');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var vatVal = Ext.getCmp('addVat').getValue();
        var nbtVal = Ext.getCmp('addNbt').getValue();

        console.log('storePrice', storePrice);
        console.log('storeSelected', storeSelected);

        getData.data.each(function (item) {
            var ch = item.data.chk;
            delete item.data.chk;
            if (ch === true) {
                //console.log(item.data)
                item.data.amount = item.data.laborCost + item.data.spareCost;
                tempData.push(item.data);
            }
        });

        console.log('addVat val' + Ext.getCmp('addVat').getValue());
        console.log('addNbt val' + Ext.getCmp('addNbt').getValue());
//        'RepairPaymentSendMod' mdle class of previous

        console.log('VAT ', vatVal);
        console.log('NBT ', nbtVal);

        var getModel = Ext.create('singer.model.WorkOrderPayment',
                form.getValues()
                );

        //set nbt and vat applicable
        var nbtApplicable = (Ext.getCmp("addNbt").getValue()) ? 1 : 0;
        var vatApplicable = (Ext.getCmp("addVat").getValue()) ? 1 : 0;
        getModel.set("vatApplicable", vatApplicable);
        getModel.set("nbtApplicable", nbtApplicable);

            console.log("5rfgfffhfh",getModel.get("amount"));

        if (Ext.getCmp("chkDeduct").getValue()) {
            getModel.set("deductionAmount", Ext.getCmp("deduct_val").getValue());
        }


        if (vatVal === false && nbtVal === false) {//VAT NBT FALSE
            console.log('11111');
            getModel.set("vatValue", '0');
            getModel.set("nbtValue", '0');


            getModel.set("woPaymentDetials", tempData);
            getModel.set("payBisId", loginData.data.getAt(0).get('extraParams'));

            
            if (tempData.length !== 0) {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/addWorkOrderPayment',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(getModel.data),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        var payid = responseData.returnRefNo;
                        storeSelected.removeAll();
                        storePrice.removeAll();

                        if (responseData.returnFlag === '1') {
                            console.log('PRINT CODE GOES HERE');
                            me.PrintRepairPaymentReport(windw, store, payid);
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else {
                Ext.Msg.alert("Error in Repair Paymentx", "Please select at least one Work order");
                console.log("test");
            }
        } else if (vatVal === true && nbtVal === false) {//VAT TRUE NBT FALSE
            getModel.set("vatValue", '0');
            getModel.set("nbtValue", '0');
            var vatValue = Ext.getCmp("vat").getValue();
            console.log(vatValue);
            console.log('22222');
            getModel.set("vatValue", vatValue);

            getModel.set("woPaymentDetials", tempData);
            getModel.set("payBisId", loginData.data.getAt(0).get('extraParams'));

            if (tempData.length !== 0) {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/addWorkOrderPayment',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(getModel.data),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        var payid = responseData.returnRefNo;
                        storeSelected.removeAll();
                        storePrice.removeAll();

                        if (responseData.returnFlag === '1') {
                            console.log('PRINT CODE GOES HERE');
                            me.PrintRepairPaymentReport(windw, store, payid);
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else {
                Ext.Msg.alert("Error in Repair Paymentx", "Please select at least one Work order");
                console.log("test");
            }
        } else if (vatVal === false && nbtVal === true) {//VAT FALSE NBT TRUE
            getModel.set("vatValue", '0');
            getModel.set("nbtValue", '0');

            var nbtValue = Ext.getCmp("nbt").getValue();
            console.log(nbtValue);

            console.log('33333');
            getModel.set("nbtValue", nbtValue);

            getModel.set("woPaymentDetials", tempData);
            getModel.set("payBisId", loginData.data.getAt(0).get('extraParams'));

            if (tempData.length !== 0) {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/addWorkOrderPayment',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(getModel.data),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        var payid = responseData.returnRefNo;
                        storeSelected.removeAll();
                        storePrice.removeAll();

                        if (responseData.returnFlag === '1') {
                            console.log('PRINT CODE GOES HERE');
                            me.PrintRepairPaymentReport(windw, store, payid);
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else {
                Ext.Msg.alert("Error in Repair Paymentx", "Please select at least one Work order");
                console.log("test");
            }
        } else if (vatVal === true && nbtVal === true) {//BOTH TRUE
            console.log('44444');

            getModel.set("vatValue", '0');
            getModel.set("nbtValue", '0');
            
            var vatValue = Ext.getCmp("vat").getValue();
            var nbtValue = Ext.getCmp("nbt").getValue();

            getModel.set("vatValue", vatValue);
            getModel.set("nbtValue", nbtValue);

            getModel.set("woPaymentDetials", tempData);
            getModel.set("payBisId", loginData.data.getAt(0).get('extraParams'));

            if (tempData.length !== 0) {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/addWorkOrderPayment',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(getModel.data),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        var payid = responseData.returnRefNo;
                        storeSelected.removeAll();
                        storePrice.removeAll();

                        if (responseData.returnFlag === '1') {
                            console.log('PRINT CODE GOES HERE');
                            me.PrintRepairPaymentReport(windw, store, payid);
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else {
                Ext.Msg.alert("Error in Repair Paymentx", "Please select at least one Work order");
                console.log("test");
            }
        }
    }, //submit function ends


    PrintRepairPaymentReport: function (windw, store, payid) {

        Ext.Msg.confirm("Want to Print?", "Do You Want to Print the repair payment memo?", function (btnn) {
            if (btnn === "yes") {
                windw.destroy();
//                var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=RepairPaymentSummary.rptdesign&PayID=" + payid;
                var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=RepairPaymentMemo.rptdesign&RepPay=" + payid;
                console.log('URI', URI);

                var ajaxreq;
                function makeHttpObject() {
                    try {
                        return new XMLHttpRequest();
                    } catch (error) {
                    }
                    try {
                        return new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (error) {
                    }
                    try {
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (error) {
                    }

                    throw new Error("Could not create HTTP request object.");
                }
                var request = makeHttpObject();
                request.open("GET", URI, true);
                request.send(null);
                request.onreadystatechange = function () {
                    if (request.readyState == 4) {
                        ajaxreq = request.responseText;
                        var myWindow = window.open('', '', 'width=500,height=500');
                        myWindow.document.write('<html><head>');
                        myWindow.document.write('<title>' + '' + '</title>');
                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                        myWindow.document.write('</head><body>');
                        myWindow.document.write('<div>' + ajaxreq + '</div>');
                        myWindow.document.write('</body></html>');
                        setInterval(function () {
                            myWindow.print();
                        }, 3000);
                    }
                };
                store.load();
            } else {
                windw.destroy();
                store.load();
            }
        });
    },
    initializeGrid: function () {
        var store = this.getWOListStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    OpenrepairPaymentsWindow: function (btn) {
        console.log('xxxx');
        var bottomStore = Ext.getStore('RepairPaymentWithPriceSelected');
        bottomStore.removeAll();//thi store is the store of clickOnSelectWorkOrder's store
        var form = Ext.widget('addRpairPayment');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Repair Payment',
            items: [form]
        });
        var store = this.getRepairPaymentWithPriceStore();
        this.getController('CoreController').loadBindedStore(store);
        //=========
        var loginData = this.getLoggedInUserDataStore();
        var buisnesID = loginData.data.items[0].data.extraParams;
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructure?bisId=' + buisnesID,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
//                params: JSON.stringify(getModel.data),
            success: function (response, btn) {
                var me = this;
                var responseData = Ext.decode(response.responseText);
                var ESSDpecentage = responseData.data[0].eddsPct;
                Ext.getCmp('addRpairPayment-essd').setValue(ESSDpecentage);

                var ESSDAccountNo = responseData.data[0].essdAcntNo;
                Ext.getCmp('acountNo').setValue(ESSDAccountNo);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
        win.show();
    },
    repairPaymentsViewCancel: function (btn) {
        var viewCncel = btn.up('window');
        viewCncel.destroy();
    },
    addrepairPaymentsCancel: function (btn) {
        var viewCncel = btn.up('window');
        viewCncel.destroy();
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        //console.log(record.data.paymentId);
        console.log('click view');
        var payID = record.data.paymentId;
        singer.AppParams.TEMP = record.get('paymentId');
        var store = this.getRepairPaymentDetlStore();

//        var dtl = record.data.WorkOrderPaymentDetail;
//        //console.log(dtl);
//        Ext.getCmp('spareN').getStore().add(spair);
        this.getController('CoreController').loadBindedStore(store);
        var grid = Ext.widget('viewRepairPayments');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Details',
            items: [grid]
        });

//        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var form = btn.up('form').getValues();
//        delete form.courierEmail2;
//        var store = this.getGetWorkOrderTranceferStore();
        var store = this.getRepairPaymentDetlStore();
//        //console.log(store);
//        
        Ext.getBody().mask('Loading...');
        Ext.getCmp('printpaybtn').setDisabled(true);

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderPayments/getWorkOrderPaymentGrid?paymentId=' + payID,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
//                system: sys,
                'Content-Type': 'application/json'
            },
//            params: JSON.stringify(form),
            success: function (response, btn) {
                win.show();
                Ext.getCmp('printpaybtn').setDisabled(false);
                Ext.getBody().unmask();

                var responseData = Ext.decode(response.responseText);
//                store.removeAll();
                var paymentDetails = responseData.data[0].woPaymentDetials;
                store.add(paymentDetails);

//                this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepairPaymentMemo.rptdesign&RepPay=" + payID, 'Repair Payment Memo');
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
                Ext.getBody().unmask();

            }
        });

    },
    clickOnSelectWorkOrder: function (grid, record, rowIndex) {
        console.log('record.data', record.data);
//        console.log('record', record);
        console.log("rowIndex ", rowIndex);
        var me = this;

        Ext.Msg.confirm('Need to select this Work Order?', "Selected Work Order : <b>" + record.get('workOrderNo') + "</b>", function (btn) {
            if (btn === 'yes') {

                var rowIndexVar = record.data;
                var store = Ext.getStore('RepairPaymentWithPriceSelected');//down
                var selectorStore = Ext.getStore('RepairPaymentWithPrice');//selector

                var valueCheck = store.findExact('workOrderNo', record.data.workOrderNo);
                console.log('test', valueCheck);

                if (valueCheck === -1) {
                    console.log('no');
                    selectorStore.removeAt(rowIndex);
                    store.add(rowIndexVar);
                    store.removeAt(rowIndexVar);
                } else if (valueCheck > -1) {
                    Ext.Msg.alert("Error", record.data.workOrderNo + " already added to Repair Payment!");
                } else {
                    Ext.Msg.alert("Error", "Internal error!");
                }
            }
        });

    },
    clickOnSelectedWorkOrder: function (grid, record, rowIndex) {
        //console.log("rowIndex ",rowIndex);
        var me = this;
        Ext.Msg.confirm('Need to remove this Work Order?', "Remove Work Order : <b>" + record.get('workOrderNo') + "</b>", function (btn) {
            if (btn === 'yes') {
                var rowIndexVar = record.data;

                var store = me.getRepairPaymentWithPriceStore();

//        console.log("RepairPaymentWithPriceSelected",store);

                //var ExtTmp=Ext.getStore('RepairPaymentWithPriceSelected');
//        console.log("ExtTmp",ExtTmp);

                store.add(rowIndexVar);
                //console.log(store);
                grid.getStore().removeAt(rowIndex);
                //console.log(ExtTmp);

            }
        });
    }
});