Ext.define('singer.controller.Overrides', {
    extend: 'Ext.app.Controller',
    init: function() {
        ////////console.log("init fired override........");
        Ext.override(Ext.ux.grid.FiltersFeature, {
            buildQuery: function(filters) {
                ////////console.log("Overrrrrrrrrrrrrrrrrrrrrrr");
                var p = {}, i, f, root, dataPrefix, key, tmp,
                        len = filters.length;

                if (!this.encode) {
                    for (i = 0; i < len; i++) {
                        f = filters[i];
                        root = [this.paramPrefix, '[', i, ']'].join('');
                        p[root + '[field]'] = f.field;

                        dataPrefix = root + '[data]';
                        for (key in f.data) {
                            p[[dataPrefix, '[', key, ']'].join('')] = f.data[key];
                        }
                    }
                } else {
                    tmp = [];
                    for (i = 0; i < len; i++) {
                        f = filters[i];
                        tmp.push(Ext.apply(
                                {},
                                {field: f.field},
                        f.data
                                ));
                    }
                    // only build if there is active filter
                    if (tmp.length > 0) {
                        p[this.paramPrefix] = Ext.JSON.encode(tmp);
                    }
                }

                return p;
            },
//            bindStore: function (store) {
//                var me = this;
//
//                // Unbind from the old Store
//                if (me.store && me.storeListeners) {
//                    me.store.un(me.storeListeners);
//                }
//
//                // Set up correct listeners
//                if (store) {
//                    me.storeListeners = {
//                        scope: me
//                    };
//                    if (me.local) {
//                        me.storeListeners.load = me.onLoad;
//                    } else {
//                        
//                        me.storeListeners['before' + (store.buffered ? 'prefetch' : 'load')] = me.onBeforeLoad;
//                    }
//                    store.on(me.storeListeners);
//                } else {
//                    delete me.storeListeners;
//                }
//                me.store = store;
//                ////////console.log('overrrrrrrrrrrrrrrr');
//            },
        });

        Ext.override(Ext.form.field.ComboBox, {
            assertValue: function() {
                var me = this,
                        value = me.getRawValue(),
                        rec;
                if (me.multiSelect) {
                    if (value !== me.getDisplayValue()) {
                        me.setValue(me.lastSelection);
                    }
                } else {
                    rec = me.findRecordByDisplay(value);
                    if (rec) {
                        me.select(rec);
                    } else {
                        if (!value) {
                            me.setValue('');
                        } else {
                            me.setValue(me.lastSelection);
                        }
                    }
                }
                me.collapse();
            }
        });
        Ext.override(Ext.form.field.Text, {
            postBlur: function() {
                var me = this;
                var val = me.getValue();
                if (!Ext.isEmpty(val)) {
                    try {
                        if (val.length > 0) {
                            me.setValue(val.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
                        }
                    } catch (ex) {
                        //console.log(ex);
                    }
//                    try {
//                        //console.log(val.replace(/^\s\s*/, ''));
//                        me.setValue(val.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
//                    } catch (ex) {
//                        //console.log(ex);
//                    }
                }
            }
        });
    }
});



