Ext.define('singer.controller.CreditNoteController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.creditnotecontroller',
    //    requirs:['NewUserController'],
    controllers: ['CoreController'],
    stores: [
        'CreditNote',
        'LoggedInUserData',
        'WinGrid',
        'CreditNoteIssuIME'
    ],
    views: [
        'DeviceIssueModule.credit_note.CreditNoteGrid',
        'DeviceIssueModule.credit_note.NewCreditNote',
        'DeviceIssueModule.credit_note.CreditNoteView',
        'singer.view.DeviceIssueModule.credit_note.AddIMEIs'
    ],
    refs: [{
            ref: 'CreditNoteGrid',
            selector: 'creditnotegrid'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        },
        {
            ref: 'NewCreditNote',
            selector: 'newcreditnote'
        },
        {
            ref: 'CreditNoteView',
            selector: 'creditnoteview'
        },
        {
            ref: 'AddIMEIs',
            selector: 'AddIMEIs'
        }
    ],
    init: function () {
        //        //////console.log(this);
        var me = this;
        me.control({
            'creditnotegrid': {
                added: this.initializeGrid
            },
            'creditnotegrid ': {
                creditEdit: this.openUpdateForm
            },
            'creditnotegrid button[action=open_window]': {
                click: this.openAddNew
            },
            //            ' newcreditnote  ': {
            //                creditRemove: this.removeRecord
            //            },
            'creditnotegrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            '  creditnotegrid ': {
                creditView: this.moreViewOpen
            },
            ' creditnotegrid ': {
                printCreditNote: this.OnPrintCreditNote
            },
            'newcreditnote button[action=cancel]': {
                click: this.insertFormCancelnew
            },
            'newcreditnote button[action=save]': {
                click: this.updataRecord
            },
            'newcreditnote button[action=PrintCreditNote]': {
                click: this.OnPrintCreditNote
            },
            'newcreditnote button[action=create]': {
                click: this.onCreate
            },
            'newcreditnote button[action=gridadd]': {
                click: this.addtoGrid
            },
            ' creditnotegrid': {
                itemdblclick: this.moreViewOpen
            },
            'creditnoteview button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newcreditnote button[action=scanimei]': {
                click: this.openScanIMEI
            },
            'AddIMEIs button[action=add]': {
                click: this.addIMEIToGrid
            },
            'AddIMEIs button[action=okay]': {
                click: this.saveIMEIGrid
            },
            'AddIMEIs button[action=cancel]': {
                click: this.exitIMEIGrid
            },
            'AddIMEIs panel field': {
                specialkey: this.isEnterKey
            },
            'newcreditnote grid items[action=removeItem]': {
                click: this.removeItem
            }
        });
        
    },
    isEnterKey: function (field, e) {
        var me = this;
        var temp=Ext.getCmp('AddIMEIs-verify')
        if (e.getKey() === e.ENTER) {
            //console.log('console')
            me.addIMEIToGrid(temp);
        }
    },
    initializeGrid: function (grid) {
        //////console.log('loading cat list');
//        var userStore = this.getCreditNoteStore();
//        this.getController('CoreController').loadBindedStore(userStore);
//        this.getController('Main').createModelStore();
        //        var test = this.getController('Main').getStore('dsrStore');
        //        //console.log(Ext.data.StoreManager.lookup('dsrStore'));
    },
    //    removeRecord: function(){
    //        
    //    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    updataRecord: function (btn) {
        var form;
        form = btn.up().up().down('form');
        var me=this;
        var loggedUser = this.getLoggedInUserDataStore();
        var uid = loggedUser.data.getAt(0).get('extraParams');
        ////console.log(loggedUser);
        //console.log("User Id.............: ", uid);
        form.getForm().findField('distributerId').setValue(uid);

        //        //console.log(form);
        //        var assignStore = Ext.getCmp('cmgrid').getStore();


        //var assignStore = btn.up().up().down('grid').getStore();
        ////console.log("AAAAAAAAAAAAAAAA: ", btn.up().up().down('grid'), " => ", assignStore);

        var assignStore = Ext.getStore('CreditNoteIssuIME');

        var temp = [];
        assignStore.data.each(function (item) {
            var intAray = Ext.create('singer.model.CreditNoteIssueDetails',
                item.data
            );
            //delete intAray.data.salesPrice;
            delete intAray.data.modleDesc;
            temp.push(intAray.data);
            //             temp.push(item.data);
            var record = form.getRecord();
            ////console.log(record);
            record.set('cerditNoteList', temp);
        });
        
        if(assignStore.data.length!==0){
            Ext.Msg.confirm("Update Data", "Are you sure you want to change data?", function(button) {
                if (button === 'yes') {
                    me.getController('CoreController').onCreditNoteRecordUpdate(btn, me.getCreditNoteStore(), ' Data Updated.', 'Updating Data', true, true, me.getCreditNoteGrid());
                } else {
                    return false;
                }
            });
        }
        else{
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }
        
        //        var str = this.getNewDeviceIssue().down('grid').getStore();
        //        ////console.log(str);
        //        str.removeAll();
        //        str.sync();

        //         var assignGrid = Ext.getCmp('UserGroupsGrid');
        ////        //console.log('test');
        //        var groups = assignGrid.getStore();
        //        var form;
        //        form = btn.up().up('form').up().up().down('form');
        //        
        //        var assignStore = Ext.getCmp('UserGroupsGrid').getStore();
        //        var temp = [];
        //        assignStore.data.each(function (item) {
        //
        //            temp.push(item.data);
        //            var record = form.getRecord();
        //            ////console.log(record);
        //            record.set('groups', temp);
        //        });
        //
        //
        //        this.getController('CoreController').onUserUpdate(form, this.getUsersStore(), 'User Data Updated.', 'Updating User', true, true, this.getUserGrid());
        //        
        //        
        //        
        ////        var form = btn.up('form');
        //////        //console.log(form);
        ////        var assignStore = Ext.getCmp('cmgrid').getStore();
        //////        //console.log(assignStore);
        ////        var temp = [];
        ////        assignStore.data.each(function(item) {
        ////            temp.push(item.data);
        ////            var record = form.getRecord();
        ////            //console.log(record);
        ////            record.set('cerditNoteList', temp);
        ////        });
        ////        this.getController('CoreController').onRecordUpdate(btn, this.getCreditNoteStore(), 'Credit Note Data Updated.', 'Updating Credit Note', true, true, this.getCreditNoteGrid());
        ////        var str = this.getNewCreditNote().down('grid').getStore();
        ////        //console.log(str);
        ////        str.removeAll();
        ////        str.sync();
    },
    onCreate: function (btn) {
        var form;
        form = btn.up().up().down('form');

        var loggedUser = this.getLoggedInUserDataStore();
        var uid = loggedUser.data.getAt(0).get('extraParams');
        ////console.log(loggedUser);
        //console.log("User Id.............: ", uid);
        form.getForm().findField('distributerId').setValue(uid);
        //        //console.log(temp);

        var assignStore = Ext.getCmp('cmgrid').getStore();
        var answer = [];
        //var ans='CreditNoteIssueDetails'
        assignStore.data.each(function (item) {
            //            answer.push(item.data);
            var intAray = Ext.create('singer.model.CreditNoteIssueDetails',
                item.data
            );
            //delete intAray.data.salesPrice;
            delete intAray.data.modleDesc;
            answer.push(intAray.data);
        });

        var user = Ext.create('singer.model.CreditNote', form.getValues());
        user.set("cerditNoteList", answer);

        //        var loginData = this.getLoggedInUserDataStore();
        //        Ext.Ajax.request({
        //            url: singer.AppParams.JBOSS_PATH + 'CreditNoteIssues/addCreditNoteIssue',
        //            method: 'PUT',
        //            headers: {
        //                userId: loginData.data.getAt(0).get('userId'),
        //                room: loginData.data.getAt(0).get('room'),
        //                department: loginData.data.getAt(0).get('department'),
        //                branch: loginData.data.getAt(0).get('branch'),
        //                countryCode: loginData.data.getAt(0).get('countryCode'),
        //                division: loginData.data.getAt(0).get('division'),
        //                organization: loginData.data.getAt(0).get('organization'),
        //                'Content-Type': 'application/json'
        //            },
        //            params: JSON.stringify(user.data),
        //            success: function(response) {
        //                var responseData = Ext.decode(response.responseText);
        //
        ////                 Ext.Msg.hide();
        ////                    var responseData = Ext.decode(response.responseText);
        //                ////console.log(responseData);
        //                if (responseData.returnFlag === '1') {
        ////                    btn.up().up('form').up('window').destroy();
        //                    btn.up('window').close();
        //                }
        ////                    
        //                form.destroy();
        //              //  parentStore.load();
        //                //=============
        //            },
        //            failure: function() {
        //                Ext.Msg.hide();
        //                Ext.Msg.alert("Error in connection", "Please try again later");
        //            }
        //        });
        
        //console.log(assignStore);
        if(assignStore.data.length!==0){
            this.getController('CoreController').onCreateCreditNote(btn, this.getCreditNoteStore(), 'New credit note issue created.', 'Creating New Credit Note Issue.', true, true, this.getCreditNoteGrid(), user);
        }
        else{
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }
        

        
        //        this.getController('Main').initializeAppReq();
        //        var store = this.getCreditNoteStore();
        ////        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Issue Created.', 'Creating New Issue', true, true, this.getDeviceIssueGrid(), user);
        ////       // this.getController('Main').initializeAppReq();
        //        var grid = this.getCreditNoteGrid();
        //        store.load();
        //        grid.bindStore(store);


    },
    moreViewOpen: function (view, record) {
        var me = this;

        this.getController('CoreController').onOpenViewMoreWindow(record, 'creditnoteview', "View");

        var detailStore = Ext.getStore('CreditNoteIssuIME');
        detailStore.load({
            scope: this,
            params: {
                creditNoteIssueNo: record.get('creditNoteIssueNo')
            },
            callback: function (records, operation, success) {
                // the operation object
                // contains all of the details of the load operation
                //console.log(Ext.getCmp('cvgrid'), " => ", records);
                Ext.getCmp('cvgrid').bindStore(this);
            }
        });

    },
    openUpdateForm: function (view, record) {
        var me = this;
        //console.log("Credit Note Issue: ", record.get('creditNoteIssueNo'));
        this.getController('CoreController').openUpdateWindow(record, 'newcreditnote', "Edit");
        var form = this.getNewCreditNote().down('form').getForm();
        // this.focusToFirstFieldinForm(form);
        //        form.findField('userId').setDisabled(true);
        form.findField('creditNoteIssueNo').disable(true);
        //        Ext.getCmp('sibtn').setVisible(false);
        var ino = record.get('creditNoteIssueNo');
        var devIme = record.get('cerditNoteList');

        var dt = new Date(record.get('issueDate'));

        me.getNewCreditNote().down('datefield').setValue(dt);

        //var detailStore = me.getNewCreditNote().down('grid').getStore();
        //this.getController('CoreController').loadBindedStore(detailStore);

        //var detailStore = Ext.getStore('CreditNoteIssuIME');
        var detailStore = me.getNewCreditNote().down('grid').getStore();
        detailStore.load({
            scope: this,
            params: {
                creditNoteIssueNo: record.get('creditNoteIssueNo')
            },
            callback: function (detailsrecords, operation, success) {
                me.getNewCreditNote().down('grid').bindStore(detailStore);
                ////console.log(me.getNewCreditNote().down('datefield'), " => eeeeeeeeeeee: ", dt);
            }
        });

    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);

    },
    insertFormCancelnew: function (btn) {
        //        var str = this.getNewCreditNote().down('grid').getStore();
        //        ////console.log('str');
        //        str.removeAll();
        //        str.sync();
        //        this.getController('CoreController').onInsertFormCancel(btn);

        btn.up('window').destroy();
    },
    openAddNew: function () {

        Ext.getStore('CreditNoteIssuIME').removeAll();

        //        var str = this.getNewCreditNote().down('grid').getStore();
        //        //console.log('str');
        //        str.removeAll();
        //        str.sync();
        this.getController('CoreController').openInsertWindow('newcreditnote');
        //        var form;
        //        form = btn.up().up().down('form');


        var form = this.getNewCreditNote().down('form').getForm();
        //        form.findField('iMEINo').hide();
        form.findField('creditNoteIssueNo').hide();
        //        form.findField('modleNo').hide();
        //        Ext.getCmp('agbtn').setVisible(false);
        //        form.findField('iMEINo').setValue("empty");
        //        Ext.get("rgrid").enableDisplayMode().hide();
        //        Ext.get("rgrid").setBorder(0);
    },
    addtoGrid: function (btn) {



        var imei = btn.up('form').getForm().findField("imeiNo").getValue();
        var model = btn.up('form').getForm().findField("modleNo").getValue();
        var store = Ext.getCmp('cmgrid').getStore();
        store.add({
            imeiNo: imei,
            modleNo: model
        });


    },
    openScanIMEI: function (btn) {

        //   Ext.getBody().mask();
        //        var win = Ext.widget('AddIMEIs');
        //        win.show();

        var grid = Ext.widget('AddIMEIs');

        var win = Ext.create("Ext.window.Window", {
            width: 450,
            height: 300,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            resizable: false,
            //  id: 'defCrfeditWindow',
            items: [grid]
        });
        win.setTitle("IMEIs");
        win.show();
        Ext.getCmp('crimtextf').focus('', 10);

        //        var grid = Ext.widget('AddIMEIs');
        //        grid.show();
    },
    addIMEIToGrid: function (btn) {

        var form = btn.up('window').down('form').getForm().findField('enterimei').getValue();
        var str = btn.up('window').down('grid').getStore();

        var loginData = this.getWinGridStore();

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/getImeiDetails?imeiNo=' + form + '',
            method: 'GET',
            success: function (response) {
                var responseData = Ext.decode(response.responseText);

                if (responseData.totalRecords === 1) {
                    var temp = responseData.data[0].modleNo;

                    var chk = str.findExact('imeiNo', form);
                    ////console.log(chk);

                    if (chk > -1) {
                        Ext.Msg.alert("Error.", "Duplicate IMEI number",function() {
                            Ext.getCmp('crimtextf').focus('', 10);
                        });
                        
                    } else {
                        str.add(responseData.data);

                        var cnt = str.getCount() - 1;

                        var record = str.getAt(cnt);
                        var salP = record.data.salesPrice;

                        salP = Ext.util.Format.number(salP, '0000000.00');

                        record.set('salesPrice', salP);
                        Ext.getCmp('crimtextf').focus('', 10);
                    }
                } else {
                    Ext.Msg.alert("Invalid IMEI", 'Invalid IMEI no... ',function() {
                        Ext.getCmp('crimtextf').focus('', 10);
                    });
                    
                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert('Error... ', 'Please try agin later...',function() {
                    Ext.getCmp('crimtextf').focus('', 10);
                });
                
            }
        });
        btn.up('window').down('form').getForm().findField('enterimei').setValue("");
        ////console.log(loginData);


        //        Ext.getStore('CreditNoteIssuIME').add({imeiNo: btn.up('window').down('form').getForm().findField('enterimei').getValue()});
        //        btn.up('window').down('form').getForm().findField('enterimei').setValue("");
    },
    saveIMEIGrid: function (btn) {
        var myStore = btn.up('window').down('grid').getStore();
        ////console.log(myStore);
        btn.up('window').destroy();

        // var win = Ext.widget('newcreditnote');


        // this.getNewCreditNote().down('grid').getStore().add({imeiNo: 100});

        //this.getNewCreditNote().down('grid').getStore().removeAll();
        //this.getNewCreditNote().down('grid').bindStore(myStore);
    },
    exitIMEIGrid: function (btn) {
        btn.up('window').destroy();
    },
    removeItem: function (grid) {
        ////console.log("Remove: ", grid);
    },
    
    OnPrintCreditNote:function(grid, record){
        
        //console.log(record);
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=creditNote.rptdesign&IssueNo=" + record.get('creditNoteIssueNo'), 'Report of ' + record.get('creditNoteIssueNo') + " - Credit Note");
        
        
//        var CreditNo = b.data.creditNoteIssueNo;
//        //console.log(a, b, c);
//        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=creditNote.rptdesign&IssueNo=" + CreditNo;
//        window.location.href = reporturl;
//        }
        
        
        
        
        
//     OnPrintCreditNote:function(grid, record){
////         //console.log(record);
////        var CreditNo = b.data.creditNoteIssueNo;
////        //console.log(a, b, c);
////        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=creditNote.rptdesign&IssueNo=" + CreditNo;
////        window.location.href = reporturl
////          this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=creditNote.rptdesign&IssueNo=" + record.get('workOrderNo'), 'Report of ' + record.get('workOrderNo') + " - Work Order"); 
//        }
        
//        onPrintWorkOrderReport: function(grid, record) {
//        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&IssueNo=" + record.get('workOrderNo'), 'Report of ' + record.get('workOrderNo') + " - Work Order");
    },
});