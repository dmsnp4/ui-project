/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Ext.define('RepairVar', {
//    singleton: true,
//    CatId: ''
//});
Ext.define('singer.controller.EventEnquiryController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.eventenquirycontroller',
    controllers: ['CoreController'],
    stores: [
        'EventInquireList',
        'LoggedInUserData',
        'EventInquirePart',
        'EventInquireDetails',
        'eventNonSalesRuleList',
        'evenAwardList'
    ],
    models:[
        'EventInqiriList'
    ],
    
    views: [
        'Events.EventEnquiry.EventEnquiryGrid',
        'Events.EventEnquiry.EventEnquiryView',
        'Events.EventEnquiry.EvnEnqParticipants',
        'Events.EventEnquiry.EventEnquiryPartiView'
    ],
    refs: [
        {
            ref: 'EventView',
            selector: 'eventview'
        },
        {
            ref: 'EventView',
            selector: 'eventview'
        },
        {
            ref: 'CreateEventForm',
            selector: 'neweventform'
        }
    ],
    init: function () {
        ////console.log(this);
        var me = this;
        me.control({
            'eventenquirygrid': {
                eventView: this.eventViewOpen
            },
            'eventenquirygrid ':{
                ParticipantsView:this.ParticipantsViewOpen
            },
             ' eventenquirygrid ':{
                added: this.initializeGrid
            },
            'eventenquirygrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'eventenquiryview button[action=cancel]': {
                click: this.onviewDone
            },
            'evnenqparticipants': {
                eventView: this.eventPartiViewOpen
            },
            'eventenquirypartiview button[action=cancel]': {
                click: this.onviewDone
            },
        });
    },
    
    initializeGrid: function (grid) {
       var store = this.getEventInquireListStore();
        this.getController('CoreController').loadBindedStore(store);        
    },
    
    onClearSearch: function(btn) {
        //console.log("CLR");
        this.getController('CoreController').onClearSearch(btn);
    },
    
    eventViewOpen:function(view, record){
        var win = Ext.widget('eventenquiryview');
        var form = win.down('form');
        var EvntID=record.data.eventId;
//        var EventEnqDetail = Ext.create('Ext.data.Store', {
//            model: 'singer.model.EventInquireDetailsMod'
//        });
        var EvntEnqDetailsStore=Ext.getStore('EventInquireDetails');
        var EvntEnqNonSaleStore =Ext.getStore('eventNonSalesRuleList');
        var EvntEnqAwardStore =Ext.getStore('evenAwardList');
        
        EvntEnqDetailsStore.removeAll();
        EvntEnqNonSaleStore.removeAll();
        EvntEnqAwardStore.removeAll();
        
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'EventInquires/getEventMasterDetails?eventId='+EvntID,
            method: "GET",
            timeout: 10000,
            success: function (response, options) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                var resData=responseData.data[0].eventSalesRuleList;
                //var resData=responseData.data;
                EvntEnqDetailsStore.add(resData);
                EvntEnqNonSaleStore.add(responseData.data[0].eventNonSalesRuleList);
                EvntEnqAwardStore.add(responseData.data[0].evenAwardList);
                
                //console.log(responseData.data);
                var title=responseData.data[0].eventName;
                
                win.setTitle(title);
                
                form.getForm().loadRecord(record);
                win.show();
            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
            }
        });
        
        
//        //console.log("record>",EvntID);

        //this.getController('CoreController').onOpenViewMoreWindow(record, 'eventenquiryview','View');
    },
    
    
    eventPartiViewOpen:function(view, record){
        //console.log("record>",record);
        var win = Ext.widget('eventenquirypartiview');
        var title='Place: '+record.data.posotion;
        win.setTitle(title);
        win.show();
    },
    
    ParticipantsViewOpen:function(view, record){
        //alert("ParticipantsView");
        var Evnid=record.get('eventId');
        //console.log("eventId>",Evnid);
        
        
        singer.AppParams.TEMP = record.get('eventId');
        var store = this.getEventInquirePartStore();
        this.getController('CoreController').loadBindedStore(store);
        var grid = Ext.widget('evnenqparticipants');
        var winPop = Ext.create("Ext.window.Window", {

            height: 500,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            id: 'tempWindow',
            items: [grid]
        });
        winPop.setTitle(record.get('eventName') + "'s Participant");
        winPop.show();
        
        //this.getController('CoreController').onOpenViewMoreWindow(record, 'evnenqparticipants','View');
    }
    ,
    onviewDone: function (btn) {
        btn.up('window').close();
    },

});