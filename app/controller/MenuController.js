Ext.define('singer.controller.MenuController', {
    extend: 'Ext.app.Controller',
    views: ['MenuContainer', 'AppTabTitleContainer', 'AppTabPanel', 'ContentContainer', 'Administrator.AllowDsrOffline.AddOfflineDsrForm','Administrator.UploadImeis.UploadImeisForm','DeviceIssueModule.HeadOfficeReturn.HeadofficeDeviceReturnGrid'],
    stores: [
        'LoggedInUserData', 'ShopVisits', 'dsrmovements', 'UserTypeListStore', 'UserTypeListTree','DSRListStore'
    ],
    models: ['UserTypeListsM'],
    refs: [{
            ref: 'mainMenu',
            selector: 'appmainmenu'
        },
        {
            ref: 'TabTitleContainer',
            selector: 'appTabTitleContainer'
        },
        {
            ref: 'homeView',
            selector: 'appcontentContainer'
        },
        {
            ref: 'appTabPanel',
            selector: 'appTabPanel'
        }
    ],
    init: function() {
        this.control({
            'appmainmenu menuitem[action=user]': {
                click: this.openUserMaintenanceWindow
            },
            'appmainmenu menuitem[action=warrentyschema]': {
                click: this.openWarrentySchema
            },
            'appmainmenu menuitem[action=userGroup]': {
                click: this.openUserGroupWindow
            },
            'appmainmenu menuitem[action=buisness]': {
                click: this.openBuisnessStructureWindow
            },
            'appmainmenu menuitem[action=offDsr]': {
                click: this.openOffDsrWindow
            },
            'appmainmenu menuitem[action=spare]': {
                click: this.openSpareMaintenaceGrid
            },
            'appmainmenu menuitem[action=repCat]': {
                click: this.onRepCat
            },
            'appmainmenu menuitem[action=clearTransist]': {
                click: this.openClearTransistWindow
            },
            'appmainmenu menuitem[action=modelList]': {
                click: this.onModelList
            },
            'appmainmenu menuitem[action=category]': {
                click: this.openCategoryListWindow
            },
            'appmainmenu menuitem[action=defectCodes]': {
                click: this.openDefectCodesWindow
            },
            'appmainmenu menuitem[action=warrentyAdj]': {
                click: this.openWarrentyAdjWindow
            },
            'appmainmenu menuitem[action=device_issue]': {
                click: this.openDeviceIssueWindow
            },
            'appmainmenu menuitem[action=device_return]': {
                click: this.openDeviceReturnWindow
            },
//            'appmainmenu menuitem[action=ho_device_return]': {
//                click: this.openDeviceReturnWindow
//            },
            'appmainmenu menuitem[action=ho_device_return]': {
                
                click: this.openHeadOfficeDeviceReturnWindow
               
            },
            'appmainmenu menuitem[action=ho_device_return_approval]': {
                click: this.openHeadOfficeDeviceApprovalWindow
               
            },
            'appmainmenu menuitem[action=credit_note]': {
                click: this.openCreditNoteWindow
            },
            'appmainmenu menuitem[action=reports]': {
                click: this.openReportsWindow
            },
            'appmainmenu menuitem[action=deviceIssueReports]': {
                click: this.openDeviceIssueReportsWindow
            },
            'appmainmenu [action=huReports]': {
                click: this.openHuReports
            },
            'appmainmenu menuitem[action=debitNotes]': {
                click: this.onDebitNotes
            },
            'appmainmenu menuitem[action=adjustImei]': {
                click: this.onAdjustImei
            },
            'appmainmenu menuitem[action=import_debit_notes]': {
                click: this.openImportDebitNotesWindow
            },
            'appmainmenu menuitem[action=repacement_return]': {
                click: this.openReplacementReturnWindow
            },
            'appmainmenu menuitem[action=imeiMaintenace]': {
                click: this.openIMEImaintenaceWindow
            },
            'appmainmenu menuitem[action=wrkOrderOpening]': {
                click: this.openWorkOrderOpeningWindow
            },
            'appmainmenu menuitem[action=brandmgr]': {
                click: this.openBrandManagerDashBoard
            },
            'appmainmenu menuitem[action=distributor]': {
                click: this.distributor
            },
            'appmainmenu menuitem[action=territorymgr]': {
                click: this.openTeritoryManagerDashBoard
            },
            'appmainmenu menuitem[action=bisdevmgr]': {
                click: this.openBusinessDevelopmentManagerDashBoard
            },
            'appmainmenu menuitem[action=operationmgr]': {
                click: this.openOperationManagerDashBoard
            },
            'appmainmenu menuitem[action=marketingmgr]': {
                click: this.openMarketingManagerDashBoard
            },
            'appmainmenu menuitem[action=servicemgr]': {
                click: this.openServiceManagerDashBoard
            },
            'appmainmenu menuitem[action=exchngeFunction]': {
                click: this.openExchangeRefference
            },
            'appmainmenu menuitem[action=wrkOrderMaintain]': {
                click: this.openWorkOrderMaintence
            },
            'appmainmenu menuitem[action=repairPayments]': {
                click: this.openrepairPaymentsWindow
            },
            'appmainmenu menuitem[action=woTransfer]': {
                click: this.openWorkOrderTranceferWindow
            },
            'appmainmenu menuitem[action=woClosing]': {
                click: this.openWorkOrderClosingWindow
            },
            'appmainmenu menuitem[action=rpirAprove]': {
                click: this.openRepairPaymentsApprovingWindow
            },
            'appmainmenu menuitem[action=enableusertracking]': {
                click: this.gotoenableusertracking
            },
            'appmainmenu menuitem[action=showcurrentdsrlocation]': {
                click: this.gotoshowcurrentdsrlocation
            },
            'appmainmenu menuitem[action=dsrmovementhistory]': {
                click: this.gotodsrmovementhistory
            },
            'appmainmenu menuitem[action=onCreateEvent]': {
                click: this.openCreateEvent
            },
            'appmainmenu menuitem[action=openShowShopVisit]': {
                click: this.openShowShopVisit
            },
            'appmainmenu menuitem[action=onEventInq]': {
                click: this.openEventInq
            },
            'appmainmenu menuitem[action=onAssignMarks]': {
                click: this.openAssignMarks
            },
            'appmainmenu menuitem[action=ServiceRpt]': {
                click: this.ServiceModuleReports
            },
            'appmainmenu menuitem[action=eventReportss]': {
                click: this.EventModuleReports
            },
            'appmainmenu menuitem[action=servicecomplain]': {
                click: this.serviceCmplain
            },
            'appmainmenu menuitem[action= jobStatus]': {
                click: this.jobStatus
            },
            'appmainmenu menuitem[action= adderpcode]': {
                click: this.AddERPCodes
            },
            ' appmainmenu menuitem[action=servicefch]': {
                click: this.openServiceFranchiseDashBoard
            },
            ' appmainmenu [action=dashboardinquiry]': {
                click: this.opendashboardinquiry
            },
            'appmainmenu menuitem[action=acceptDebitNote]': {
                click: this.openAdminacceptDebitNoteWindow
            },
            'appmainmenu menuitem[action=quickWrkOrderOpening]': {
                click: this.openQuickWorkOrderClosingWindow
            },
            'appmainmenu menuitem[action=approveExchngeFunction]': {
                click: this.openApproveExchangeFunctionWindow
            },
            'appmainmenu menuitem[action=quickWrkOrderCollector]': {
                click: this.openQuickWorkOrderCollectorWindow

            },
            'appmainmenu menuitem[action=uploadimeis]': {
                click: this.openUploadImeisWindow

            },
            'appmainmenu menuitem[action=stockandclearancerepair]': {
                click: this.openStockAndClearanceRepair

            },
            'appmainmenu menuitem[action=inventoryUpload]': {
                click: this.inventoryUpload

            },
            'appmainmenu menuitem[action=imeiSwap]': {
                click: this.openIMEISwapWindow
            }
//            'appmainmenu menuitem[action=adminWorkOrderClose]': {
//                click: this.openAdminWorkOrderCloseWindow
//            }
        })
    },
//     openAdminWorkOrderCloseWindow: function (menuItem) {
//         console.log("Admin WO close");
//        var homeView = this.getHomeView();
//        this.updateClickedTabTitle(homeView, menuItem.text);
//        this.createMenuTab(menuItem, homeView,'admin_wo_close');
//    },
    openStockAndClearanceRepair: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'WorkOrderOpenGridForStockRepair');
    },
    openIMEISwapWindow: function(menuItem) {
        var win = Ext.widget('IMEISwapView');
        win.show();
        Ext.getCmp('enter-swap-imei').setVisible(true);
    },
    openAdminacceptDebitNoteWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'debitnotegridd');
    },
    jobStatus: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'statusMainview');
    },
    serviceCmplain: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'complainGrid');
    },
    EventModuleReports: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'eventMReports');
    },
    ServiceModuleReports: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'serviceMReports');
    },
    openWarrentySchema: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'warschgrid');
    },
    openRepairPaymentsApprovingWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'RepairPaymentsApproveGrid');
    },
    openQuickWorkOrderClosingWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'QuickWorkOrdersGrid');
    },
    openWorkOrderClosingWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'wrkOrderClosingGrid');
        Ext.getStore('openWOnum').load();
        Ext.getStore('distributorstr').load();
        var Repstore = Ext.getStore('RepStatus');
        if (Repstore.getCount() === 0) {
            Repstore.load();
        }
        var deliverstore = Ext.getStore('DeliverType');
        if (deliverstore.getCount() === 0) {
            deliverstore.load();
        }

        //        var Distributstore = Ext.getStore('distributorstr');
        //        if (Distributstore.getCount() === 0) {
        //            Distributstore.load();
        //        }
    },
    openWorkOrderTranceferWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'wrkOrdrTranceferGrid');
        //        var franchiseStr = 
        Ext.getStore('bisAssignList').load();
        //        if (franchiseStr.getCount() === 0) {
        //            franchiseStr.load();
        //        }
        var deliverStr = Ext.getStore('DeliverType');
        if (deliverStr.getCount() === 0) {
            deliverStr.load();
        }

    },
    openrepairPaymentsWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'repairPayGrid');
    },
    openWorkOrderMaintence: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'wrkOrdrMaintneGrid');
        Ext.getStore('mjrdefectStr').load();
        Ext.getStore('minordefectStr').load();
        //        Ext.getStore('ReplevelSTR').load();
        //        Ext.getStore('RepStatus').load();
        //        Ext.getStore('EstStat').laod();

        var WrrntyType = Ext.getStore('WrrntyType');

        if (WrrntyType.getCount() === 0) {
            WrrntyType.load();
        }
        var NonWrrntyType = Ext.getStore('NonWrrntyType');
        if (NonWrrntyType.getCount() === 0) {
            NonWrrntyType.load();
        }
        var ReplevelSTR = Ext.getStore('ReplevelSTR');
        if (ReplevelSTR.getCount() === 0) {
            ReplevelSTR.load();
        }
        var RepStatus = Ext.getStore('RepStatus');
        if (RepStatus.getCount() === 0) {
            RepStatus.load();
        }
        var EstStat = Ext.getStore('EstStat');
        if (EstStat.getCount() === 0) {
            EstStat.load();
        }
        var PayOpt = Ext.getStore('PayOpt');
        if (PayOpt.getCount() === 0) {
            EstStat.load();
        }
    },
    openExchangeRefference: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'exchangeReferenceGrid');
        Ext.getStore('wrkOrderNo').load();
        Ext.getStore('distributorstr').load();

    },
    opendashboardinquiry: function(menuItem) {
        console.log('opendashboardinquiry');
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'serviceFranchise');
    },
    openServiceFranchiseDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'serviceFranchise');
//         console.log('comming to service franchise dashboard');
//        var loginStore = this.getLoggedInUserDataStore();
//        var user = loginStore.data.getAt(0).get('extraParams');
////        console.log(user);
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=service_franchise.rptdesign&BIS_ID=" + user, 'Service Franchise  Dashboard');

    },
    openServiceManagerDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'serviceM');
//        var loginStore = this.getLoggedInUserDataStore();
//        var user = loginStore.data.getAt(0).get('extraParams');
////        console.log(user);
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=service_manager.rptdesign&BIS_ID=" + user, 'Service Manager Dashboard');

    },
    openMarketingManagerDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'marketingM');
//        var loginStore = this.getLoggedInUserDataStore();
////        console.log(loginStore);
//        var user = loginStore.data.getAt(0).get('extraParams');
////        console.log(user);
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=marketing_manager.rptdesign&BIS_ID=" + user, 'Marketing Manager Dashboard');

    },
    openOperationManagerDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'marketingM');
//        var loginStore = this.getLoggedInUserDataStore();
//        var user = loginStore.data.getAt(0).get('extraParams');
////        console.log(user);
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=operation_manager.rptdesign&BIS_ID=" + user, 'Operation Manager Dashboard');
    },
    openBusinessDevelopmentManagerDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'buisnesDM');
//        var loginStore = this.getLoggedInUserDataStore();
//        var user = loginStore.data.getAt(0).get('extraParams');
////        console.log(user);
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=BuisnessDevelopmentManager.rptdesign&BIS_ID=" + user, 'Business Development Manager Dashboard');
    },
    openTeritoryManagerDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'teritoryManger');
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=TerritoryManager.rptdesign&BIS_ID=" + user, 'Teritory Manager Dashboard');
    },
    distributor: function(menuItem) {
        var loginStore = this.getLoggedInUserDataStore();
        var user = loginStore.data.getAt(0).get('extraParams');
//        console.log(user);
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'distributorView');

        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=distributor_dashboard.rptdesign&BIS_ID=" + user, 'Distributor Dashboard');

    },
    openBrandManagerDashBoard: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'brandManger');
//        this.getController('CoreController').dash_board_interface_view("BirtReportController?ReportFormat=html&ReportName=BrandManageRR.rptdesign&BIS_ID=" + user, 'Brand Manager Dashboard');
    },
    openWorkOrderOpeningWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'workOrderOpenGrid');
        Ext.getStore('modStore').load();
        Ext.getStore('mjrdefectStr').load();
        Ext.getStore('distributorstr').load();
        Ext.getStore('WrrntyType').load();
        Ext.getStore('NonWrrntyType').load();
        Ext.getStore('woDrp').load();
        Ext.getStore('WarrentySchStore').load();

        //        var modelstor = Ext.getStore('modStore');
        //
        //        if (modelstor.getCount() === 0) {
        //            modelstor.load();
        //        }
        //
        //        var defectstor = Ext.getStore('mjrdefectStr');
        //        if (defectstor.getCount() === 0) {
        //            defectstor.load();
        //        }
        //
        //        var distributorstor = Ext.getStore('distributorstr');
        //        if (distributorstor.getCount() === 0) {
        //            distributorstor.load();
        //        }
        //
        //        var warrntyType = Ext.getStore('WrrntyType');
        //        if (warrntyType.getCount() === 0) {
        //            warrntyType.load();
        //        }
        //
        //        var nonwarrntyType = Ext.getStore('NonWrrntyType');
        //        if (nonwarrntyType.getCount() === 0) {
        //            nonwarrntyType.load();
        //        }
    },
    openIMEImaintenaceWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'imeiMaintenaceGrid');
    },
    onAdjustImei: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        //        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'imeigrid');
    },
    openReplacementReturnWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'replacmentReturnGrid');
        Ext.getStore('exchangeRfnStore').load();
        //        var rrstor = Ext.getStore('exchangeRfnStore');
        //        if (rrstor.getCount() === 0) {
        //            rrstor.load();
        //        }

    },
    openImportDebitNotesWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'importDebitgrid');
        //this.createMenuTab(menuItem, homeView, 'openImportDebitNotesWindow');
    },
    openReportsWindow: function(menuItem) {
//        console.log('openReportsWindow',openReportsWindow);
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'reportsContainer');
    },
    openDeviceIssueReportsWindow: function(menuItem) { //here
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        //Ext.getStore('UserTypeListStore').load();
//        //console.log(Ext.getStore('UserTypeListStore'));
        this.createMenuTab(menuItem, homeView, 'deviceIssueReports');
    },
    openHuReports: function(menuItem) { //here
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        //Ext.getStore('UserTypeListStore').load();
//        //console.log(Ext.getStore('UserTypeListStore'));
        this.createMenuTab(menuItem, homeView, 'DeviceIssueReportsExternal');
    },
//    openDeviceIssueTreeDropDown: function (menuItem) { //here2
//        var homeView = this.getHomeView();
//        this.updateClickedTabTitle(homeView, menuItem.text);
//        Ext.getStore('UserTypeListStore').load();
//        //console.log(Ext.getStore('UserTypeListStore'));
//        //this.createMenuTab(menuItem, homeView, 'deviceIssueReports');
//    },

    onDebitNotes: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'debitnotegrid');
    },
    openCreditNoteWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        var me = this;
        me.createMenuTab(menuItem, homeView, 'creditnotegrid');

        var store = Ext.getStore('CreditNote');
        store.removeAll();

        var loginStore = this.getLoggedInUserDataStore();
        var distributerId = loginStore.data.getAt(0).get('extraParams');
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'CreditNoteIssues/getCreditNoteIssueDetails?serviceBisId=' + distributerId,
            method: "GET",
            timeout: 10000,
            success: function(response, options) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                store.add(responseData.data);

            },
            failure: function(response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
            }
        });

    },
    openDeviceIssueWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'deviceissuegrid');

        Ext.getStore('dsrStore').load();
        //        Ext.getStore('disStore').load();
        Ext.getStore('subStore').load();
    },
    openDeviceReturnWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'devicereturngrid');
    },
    openHeadOfficeDeviceReturnWindow: function(menuItem) {
        console.log('openHeadOfficeDeviceReturnWindow');
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'headofficedevicereturngrid');
    },
    openHeadOfficeDeviceApprovalWindow: function(menuItem) {
        console.log('openHeadOfficeDeviceReturnWindow');
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'headofficedeviceapprovalgrid');
    },
    openWarrentyAdjWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'warrentyadjgrid');
    },
    onModelList: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'modellistgrid');
        Ext.getStore('RepCats').load();
        Ext.getStore('WarrentySchStore').load();
    },
    openUserGroupWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'usergroup');
    },
    openCategoryListWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'categorygrid');
    },
    openUserMaintenanceWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'usergrid');
//        var store = Ext.getStore('userIds');
//        if (store.getCount() === 0) {
//            store.load();
//        }

    },
    openBuisnessStructureWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'buisnessStructure');
        Ext.getStore('bsnsTypeStr').load();
        Ext.getStore('BisIdsSto').load();
        //load relevent stores if not loaded
        //        this.getController('CoreController').loadBindedStore(Ext.getStore('bsnsTypeStr'));
        //        this.getController('CoreController').loadBindedStore(Ext.getStore('BisIdsSto'));
        //        this.getController('CoreController').loadBindedStore(Ext.getStore('BisIdsSto'));
    },
    openDefectCodesWindow: function(menutItem) {
        var homeView = this.getHomeView();
        ////////console.log(homeView);
        this.updateClickedTabTitle(homeView, menutItem.text);
        this.createMenuTab(menutItem, homeView, 'majordefectgrid');

    },
//    openOffDsrWindow: function (menutItem) {
//        var homeView = this.getHomeView();
//        ////////console.log(homeView);
//        this.updateClickedTabTitle(homeView, menutItem.text);
//        this.createMenuTab(menutItem, homeView, 'addOfflinedsrForm');
//
//    },
    openOffDsrWindow: function(menuItem) {
     
        var win = Ext.widget('addOfflinedsrForm');
           console.log("menu dsr");
        win.show();

    },
    openUploadImeisWindow: function(menuItem) {
     
        var win = Ext.widget('uploadImeisForm');
           console.log("menu upload imeis");
        win.show();

    },
    AddERPCodes: function(menuItem) {
        //console.log('comming to add erp');
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'mapping');
    },
    openSpareMaintenaceGrid: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'sparemaintenancegrid');
    },
    onRepCat: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'repairlistgrid');
    },
    openClearTransistWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'cleartransist');
    },
    openCreateEvent: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'createeventgrid');
        this.getController('CoreController').loadBindedStore(Ext.getStore('CreateEvent'));
        var stores = ['Brands', 'modStore', 'EventPeriods', 'SalesTypes', 'ProductFamily'];
        for (i = 0; i < stores.length; i++) {
            var store = Ext.getStore(stores[i]);
            if (store.getCount() === 0) {
                store.load();
            }
        }
    },
    openShowShopVisit: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'shopvisitshoplocation');

        var loginStore = this.getLoggedInUserDataStore();
        var bisid = loginStore.data.getAt(0).get('extraParams');
        //        var store = Ext.getStore('ShopVisits');

        var cmp = Ext.getCmp('shopvisitshoplocation-distributor');
        var store = cmp.getStore();
        store.removeAll();
        Ext.getBody().mask('Loading Data', 'large-loading');
        var me = this;
        //console.log(cmp);
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownTypeList?bisId=' + bisid + '&bisType=3',
            method: "GET",
            timeout: 10000,
            success: function(response, options) {

                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                //                //console.log(responseData.data);
                //                store.add(responseData.data);
                //                cmp.bindStore(store);
                store.add(responseData.data);
            },
            failure: function(response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                //                form.getForm().reset();

            }
        });
        //        if (store.getCount() === 0) {
        //            Ext.Ajax.request({
        //                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBussinessAvilableTypeList?bisId=' + bisid + '&bisType=3',
        //                method: "GET",
        //                timeout: 10000,
        //                success: function (response, options) {
        //
        //                    Ext.getBody().unmask();
        //                    var responseData = Ext.decode(response.responseText);
        //                    //                //console.log(responseData.data);
        //                    //                store.add(responseData.data);
        //                    //                cmp.bindStore(store);
        //                    cmp.getStore().add(responseData.data);
        //                },
        //                failure: function (response, options) {
        //                    Ext.getBody().unmask();
        //                    singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
        //                    //                form.getForm().reset();
        //
        //                }
        //            });
        //        }else{
        //             Ext.getBody().unmask();
        //        }
        //        //console.log('store',store);
    },
    openEventInq: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'eventenquirygrid');
    },
    openAssignMarks: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'assignMarksEventsGrid');
    },
    createMenuTab: function(menutItem, homeView, panelItem) {
        var tabPanel = this.createAppTabPanel(homeView);
        var controller = this;
        var tab = {
            id: menutItem.text.replace(/\s/g, ""),
            title: menutItem.text + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
            xtype: panelItem,
            closable: true,
            listeners: {
                close: function(crntTab) {
                    controller.checkTabPaneIsEmpty(tabPanel, crntTab);
                }
            }
        };
        if (this.checkCurrentTabIsExisting(tabPanel, tab)) {
            tabPanel.setActiveTab(tab.id);
        } else {
            tabPanel.insert(tab); // 0
            tabPanel.setActiveTab(tab.id);
        }
        //tabPanel.setActiveTab(tabID);
    },
    checkTabPaneIsEmpty: function(tabPanel, crntTab) {
        if (tabPanel.items.length - 1 === 0) {
            this.getHomeView().remove(tabPanel);
            this.getHomeView().remove(this.getTabTitleContainer());
            document.location.hash = "";
        }
        //////////console.log(crntTab);
    },
    checkCurrentTabIsExisting: function(tabPanel, tab) {
        var tabItems = tabPanel.items.items;
        var existingBool = false;
        if (tabItems.length === 0) {
            existingBool = false;
        } else {
            for (var i = 0; i < tabItems.length; i++) {
                if (tabItems[i].id === tab.id) {
                    existingBool = true;
                }
            }
        }
        return existingBool;
    },
    createAppTabPanel: function(homeView) {
        var tabPanel = this.getAppTabPanel();
        if (tabPanel === undefined) {
            tabPanel = Ext.widget('appTabPanel');
            homeView.insert(2, tabPanel);
        } else {
            tabPanel = homeView.down('tabpanel');
        }
        return tabPanel;
    },
    updateClickedTabTitle: function(homeView, title) {
        //        var titleContainer = this.getTabTitleContainer();
        //        if (titleContainer === undefined) {
        //            titleContainer = Ext.widget('appTabTitleContainer');
        //            homeView.insert(1, titleContainer);
        //        }
        //        titleContainer.update(Ext.util.Format.capitalize(title));
    },
    openApproveExchangeFunctionWindow: function(menuItem) {
        var homeView = this.getHomeView();
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'ApproveDeviceExchange');
    },
    gotoenableusertracking: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'enhomeview');
    },
    gotoshowcurrentdsrlocation: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'dsrshowlocation');
    },
    openQuickWorkOrderCollectorWindow: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'workordercollectorgrid');
    },
    inventoryUpload: function(menuItem) {
        var homeView = this.getHomeView(menuItem);
        this.updateClickedTabTitle(homeView, menuItem.text);
        this.createMenuTab(menuItem, homeView, 'addimeis');
    },
    
    gotodsrmovementhistory: function(menuItem) {
        me = this;
        var loginStore = this.getLoggedInUserDataStore();
        var bisid = loginStore.data.getAt(0).get('extraParams');
        //this.getDsrmovementsStore();
        var arg = 'bisType=3&bisId=' + bisid;

        var homeView = me.getHomeView(menuItem);
        me.updateClickedTabTitle(homeView, menuItem.text);
        me.createMenuTab(menuItem, homeView, 'dsrovement');
        var store = this.getDsrmovementsStore();

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownTypeList',
            params: arg,
            method: "GET",
            timeout: 10000,
            success: function(response, options) {

                store.removeAll();
                var responseData = Ext.decode(response.responseText);
                store.add(responseData.data);
                //console.log(store);

            },
            failure: function(response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
            }
        });




        //        Ext.create('Ext.data.Store', {
        //            id: 'dsrmovements',
        //            model: 'singer.model.dsrmovementsMod',
        //            pageSize: singer.AppParams.INFINITE_PAGE,
        //            proxy: {
        //                type: 'ajax',
        //                url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getTrackingEnableList?bisTypeId=3&bisId='+bisid,
        //                reader: {
        //                    type: 'json',
        //                    root: 'data'
        //                }
        //            },
        //            autoLoad: true
        //        });
    }
});