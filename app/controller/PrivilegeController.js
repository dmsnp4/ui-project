
Ext.define('singer.controller.PrivilegeController', {
    extend: 'Ext.app.Controller',
    requires : ['Ext.menu.Menu'],
    models: ['LoggedInUser', 'Privilege'],
    stores: ['LoggedInUserData', 'Privileges'],
    views: ['MenuContainer'],
    refs: [{
            ref: 'MainMenu',
            selector: 'appmainmenu'
        }],
    init: function() {
        this.control({
            'appmainmenu': {
                added: this.checkCurrentUserPrivileges
            }
        });
    },
    checkCurrentUserPrivileges: function(contanr, parent, index) {
        var me = this;
        var userData = this.getLoggedInUserDataStore();
        //////console.log(userData);
        var parentMenu = userData.first().get("functions");
        //////console.log(parentMenu);
        
        for (var i = 0; i < parentMenu.length; i++) {
            var menuItem = parentMenu[i];
            var menuItemGen = {};
            menuItemGen.action = menuItem.menuAction;
            menuItemGen.text = menuItem.functionName;
            menuItemGen.arrowAlign = 'right';
            var subMenuItems = menuItem.functions;
            if (subMenuItems.length > 0) {
                var submenuArray = [];
                for (var x = 0; x < subMenuItems.length; x++) {
                    var submenuItem = {
                        text : subMenuItems[x].functionName,
                        action : subMenuItems[x].menuAction
                    };
                    submenuArray.push(submenuItem);
                }
//                ////console.log(submenuArray);
                menuItemGen.menu = submenuArray;
            }
            me.getMainMenu().insert(i, Ext.create('Ext.button.Button',menuItemGen));
        }
    },
    getChildrenMenu: function(parentFunc) {

    }
});



