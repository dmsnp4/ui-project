Ext.define('singer.controller.WorkOrderMaintenance', {
    extend: 'Ext.app.Controller',
    alias: 'widget.wrkOrderMaintncecontroller',
    rec: [],
    temp: null,
    mainId: null,
    expireStat: null,
    controllers: ['CoreController'],
    stores: [
        'GetWorkOrderList', 'WorkOrdeMaintainceDeatails', 'LoggedInUserData', 'WorkOrderOpening', 'WorkOrderMaintainAdd',
        'DefectStore',
    ],
    models: [
        'WorkOrderMaintain'
    ],
    views: [
        'ServiceModule.WorkOrderMaintenace.MainGrid',
        'ServiceModule.WorkOrderMaintenace.AddNew',
        'ServiceModule.WorkOrderMaintenace.SelectWorkOrder',
        'ServiceModule.WorkOrderMaintenace.AddDefects',
        'ServiceModule.WorkOrderMaintenace.AddSpareParts',
        'ServiceModule.WorkOrderMaintenace.MaintenanceView'
    ],
    refs: [
        {
            ref: 'MainGrid',
            selector: 'wrkOrdrMaintneGrid'
        },
        {
            ref: 'AddNew',
            selector: 'addnewform'
        },
        {
            ref: 'SelectWorkOrder',
            selector: 'selectWrkOrderGrid'
        },
        {
            ref: 'AddDefects',
            selector: 'addDeffectWindow'
        },
        {
            ref: 'AddSpareParts',
            selector: 'addSparePartsWindow'
        },
        {
            ref: 'MaintenanceView',
            selector: 'wrkOmaintenaceView'
        },
        {
            ref: 'EditWorkMaintaince',
            selector: 'editForm'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'wrkOrdrMaintneGrid': {
                added: this.initializeGrid
            },
            '  wrkOrdrMaintneGrid': {
                editt: this.openEditWindow
            },
            '   wrkOrdrMaintneGrid': {
                del: this.onDel
            },
            '    wrkOrdrMaintneGrid': {
                estimate: this.onEstimate
            },
            'addnewform button[action=getEst]': {
                click: this.onGetEst
            },
            'wrkOrdrMaintneGrid button[action=addNew]': {
                click: this.openAddNewWindow
            },
            'addnewform button[action=select_Work_Order]': {
                click: this.openSelectWorkOrderWindow
            },
            'addnewform button[action=add_defect]': {
                click: this.openAddDefectWindow
            },
            'addnewform button[action=spare_parts]': {
                click: this.openSparePartsWindowGrid
            },
            'addnewform button[action=cancel_wrkOrdr_Maintence_Form]': {
                click: this.cancelAddNewWorkOrderMaintenanceForm
            },
            'wrkOmaintenaceView button[action=View_cancel]': {
                click: this.cancelAddNewWorkOrderMaintenanceForm
            },
            'wrkOmaintenaceView button[action=done]': {
                click: this.viewDone
            },
            'wrkOrdrMaintneGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'selectWrkOrderGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'selectWrkOrderGrid ': {
                selectWrkOrder: this.clickOnSelectWorkOrder
            },
            'addDeffectWindow button[action=submit_Defect_Codes_form]': {
                click: this.SubmitDefectCodes
            },
            'addSparePartsWindow button[action=search_spare_partsDetails]': {
                click: this.addSparePartsDetails
            },
            //            'addSparePartsWindow checkcolumn[action=test]': {
            //                checkchange: this.AddingSparePartsDetails
            //            },
            'addSparePartsWindow button[action=submit_Add_SapreParts_form]': {
                click: this.onDoneEvent
            },
            'addnewform button[action=submit_wrkOrdr_Maintence_form]': {
                click: this.DoneAddNew
            },
            ' wrkOrdrMaintneGrid': {
                vieww: this.onView
            },
            //            ' addSparePartsWindow checkcolumn[action=test]': {
            //                checkchange: this.onCheckChange
            //            },
            'addnewform button[action=save]': {
                click: this.onSave
            },
            'combobox[action=rprLevel]': {
                change: this.onSelect
            },
            ' addnewform button[action=printEst]': {
                click: this.printestimate
            },
            '  addnewform button[action=mailCus]': {
                click: this.mailCus
            },
        });
    },
    viewDone: function (cmp) {
        cmp.up('window').close();
    },
    printestimate: function (cmp) {
        var estimaterfrf = Ext.getCmp('addnewform-estNo').getValue();
        var loginData = this.getLoggedInUserDataStore();
        var loggeduser = loginData.data.items[0].data.firstName;
//        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=EstimateWo.rptdesign&Refference=" + estimaterfrf+ '&User='+loggeduser, 'Estimate');

        //=======================================
        var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=EstimateWo.rptdesign&Refference=" + estimaterfrf + '&User=' + loggeduser;
        console.log('URI', URI);

        var ajaxreq;
        function makeHttpObject() {
            try {
                return new XMLHttpRequest();
            }
            catch (error) {
            }
            try {
                return new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (error) {
            }
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (error) {
            }

            throw new Error("Could not create HTTP request object.");
        }
        var request = makeHttpObject();
        request.open("GET", URI, true);
        request.send(null);
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                ajaxreq = request.responseText;

                var myWindow = window.open('', '', 'width=500,height=500');
                myWindow.document.write('<html><head>');
                myWindow.document.write('<title>' + '' + '</title>');
                myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                myWindow.document.write('</head><body>');
                myWindow.document.write('<div>' + ajaxreq + '</div>');
                myWindow.document.write('</body></html>');

                setInterval(function () {
                    myWindow.print();
                }, 3000);
            }
        };
    },
    mailCus: function (cmp) {
        var me = this;
        //console.log('expireStat');
        //console.log(me.expireStat);
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = cmp.up('form').getValues();
        var x = Ext.getCmp('addnewform-totCost').getValue();
        var tot = Ext.getCmp('addnewform-totCost').getValue();

        if (me.expireStat === 0) {
            Ext.Msg.confirm('This is UNDER WARRANTY work order', 'Are you sure you want to send the estimate to the customer?', function (btn) {
                if (btn === 'yes') {
                    Ext.Msg.wait('Please wait', 'Generating Estimate No...', {
                        interval: 300
                    });
                    Ext.Ajax.request({
                        url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/sendEstimateCustomerMail?workOrderNo=' + form.workOrderNo + '&woReply=' + x + '&esRefNo=' + form.esRefNo + '&customerRef=' + form.customerRef + '&woReply=' + tot + '&workorderMaintainId=' + me.mainId,
                        method: 'GET',
                        headers: {
                            userId: loginData.data.getAt(0).get('userId'),
                            room: loginData.data.getAt(0).get('room'),
                            department: loginData.data.getAt(0).get('department'),
                            branch: loginData.data.getAt(0).get('branch'),
                            countryCode: loginData.data.getAt(0).get('countryCode'),
                            division: loginData.data.getAt(0).get('division'),
                            organization: loginData.data.getAt(0).get('organization'),
                            system: sys,
                            'Content-Type': 'application/json'
                        },
                        success: function (response) {
                            Ext.Msg.hide();
                            var responseData = Ext.decode(response.responseText);
                            var error = responseData.returnFlag;
                            if (error === '1') {
                                Ext.Msg.alert("Mail Sent!", "Mail Sent Successfuly...");
                            } else {
                                Ext.Msg.alert("Mail Send fail...", "Error Sending Mail...");
                            }
                        },
                        failure: function () {
                            Ext.Msg.hide();
                            Ext.Msg.alert("Error in connection", "Please try again later");
                        }
                    });
                }
            });
        } else {
            Ext.Msg.wait('Please wait', 'Generating Estimate No...', {
                interval: 300
            });
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/sendEstimateCustomerMail?workOrderNo=' + form.workOrderNo + '&woReply=' + x + '&esRefNo=' + form.esRefNo + '&customerRef=' + form.customerRef + '&woReply=' + tot + '&workorderMaintainId=' + me.mainId,
                method: 'GET',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                success: function (response) {
                    Ext.Msg.hide();
                    var responseData = Ext.decode(response.responseText);
                    var error = responseData.returnFlag;
                    if (error === '1') {
                        Ext.Msg.alert("Mail Sent!", "Mail Sent Successfuly...");
                    } else {
                        Ext.Msg.alert("Mail Send fail...", "Error Sending Mail...");
                    }
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        }


    },
    onEstimate: function (grid, record, index) {
        console.log('Esti');

        var repLevelStr = Ext.getStore('ReplevelSTR');
        repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + record.data.iemiNo;
        repLevelStr.load();
        var me = this;
        var dat;
        var loginData = this.getLoggedInUserDataStore();
        var form = Ext.widget('addnewform');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Generate Estimate for ' + record.get('workOrderNo'),
            items: [form]
        });

        var editBtn = Ext.getCmp('selectWoBtn');
        editBtn.setDisabled(true);

        Ext.getCmp('testGrid').getStore().removeAll();
        Ext.getCmp('spareN').getStore().removeAll();
        Ext.getCmp('addnewform-save').show();
        Ext.getCmp('addnewform-submit').hide();

        this.mainId = record.getData().workorderMaintainId;
        this.temp = record.data.workorderMaintainId;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getWorkOrderMaintainaceDetails?workorderMaintainId=' + record.data.workorderMaintainId + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            //                     params: JSON.stringify(form.getValues()),
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                dat = responseData;
                //console.log('responseData expireStatus');
                //console.log(responseData.data[0].expireStatus);
                me.expireStat = responseData.data[0].expireStatus;

                if (dat.length !== 0) {
                    var def = dat.data[0].woDefect;
                    var est = dat.data[0].woEstimate;
                    if (est.length !== 0) {
                        Ext.getCmp('addnewform-genEst').hide();
                        Ext.getCmp('addnewform-estTitle').show();
                        Ext.getCmp('addnewform-estNo').show();
                        Ext.getCmp('addnewform-estStat').show();
                        Ext.getCmp('addnewform-cusRef').show();
                        Ext.getCmp('addnewform-payOpt').show();
                        Ext.getCmp('addnewform-printEst').show();
                        Ext.getCmp('addnewform-mailCus').show();
                        if (est[0].status === 3) {
                            Ext.getCmp('addnewform-estNo').setReadOnly(true);
                            Ext.getCmp('addnewform-estStat').setReadOnly(true);
                            Ext.getCmp('addnewform-cusRef').setReadOnly(true);
                            Ext.getCmp('addnewform-payOpt').setReadOnly(true);
                        }
                    } else {
                        Ext.getCmp('addnewform-genEst').show();
                    }
                    Ext.getCmp('testGrid').getStore().add(def);
                    var spair = dat.data[0].woDetials;

                    var tot = 0;
                    for (i = 0; i < spair.length; i++) {
                        tot += spair[i].partSellPrice;
                    }
                    Ext.getCmp('spareN').getStore().add(spair);
                    form.getForm().setValues(dat.data[0]);
                    form.getForm().setValues(est[0]);
                    var dt = new Date(dat.data[0].estimateClosingDate);
                    form.getForm().findField('estimateClosingDate').setValue(dt);

                    var txt = Ext.getCmp('addnewform-temp');
                    txt.setValue(0);
                    //                    //console.log(Ext.getCmp('levelPrice').getValue())
                    txt.setValue(parseFloat(Ext.getCmp('levelPrice').getValue()));
                    //                    //console.log('before add tot ', txt.getValue())
                    tot += parseFloat(txt.getValue());
                    txt.setValue(tot);
                    Ext.getCmp('addnewform-totCost').setValue(Ext.util.Format.number(tot, '0,000.00'));

                }

                win.show();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    onGetEst: function (cmp, record) {
        var form = cmp.up('form');
        //console.log(form.getForm());
        //         //console.log(form.getForm().findField('esRefNo'))
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Generating Estimate No...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/GenarateEstimateNo',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            //            params: JSON.stringify(x),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                //                 var error = responseData.returnMsg;
                //                 //console.log(responseData.returnRefNo);
                form.getForm().findField('esRefNo').setValue(responseData.returnRefNo);
                Ext.getCmp('addnewform-genEst').hide();
                Ext.getCmp('addnewform-estTitle').show();
                Ext.getCmp('addnewform-estNo').show();
                Ext.getCmp('addnewform-estStat').show();
                Ext.getCmp('addnewform-cusRef').show();
                Ext.getCmp('addnewform-payOpt').show();
                Ext.getCmp('addnewform-printEst').show();
                Ext.getCmp('addnewform-mailCus').show();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    onSelect: function (cmp, newValue, oldValue, eOpts) {
        try {
            //console.log(newValue, oldValue);
            var rplvel = Ext.getStore('ReplevelSTR');
            //console.log(newValue, oldValue);
            var ind = rplvel.findRecord('levelId', newValue);
            var indOld = rplvel.findRecord('levelId', oldValue);
            //console.log(ind);
            Ext.getCmp('levelPrice').setValue(Ext.util.Format.number(parseFloat(ind.getData().levelPrice), '0,000.00'));
            //        cmp.setValue(Ext.util.Format.number(parseFloat(newValue), '0,000.00'));
            var txt = Ext.getCmp('addnewform-temp');


            var prev = parseFloat(txt.getValue());


            var val1 = 0;
            var val2 = 0;
            var tot = 0;
            if (oldValue > -1) {
                val1 = parseFloat(indOld.getData().levelPrice);
                val2 = parseFloat(ind.getData().levelPrice);
                //            txt.setValue(Ext.util.Format.number(
                //                parseFloat(txt.getValue()) - parseFloat(indOld.getData().levelPrice) + parseFloat(ind.getData().levelPrice),'0,000.00'));
                //                //console.log(prev, val1, val2);
                //                //console.log(prev - val1 + val2);
                tot = prev - val1 + val2;
                //            txt.setValue(Ext.util.Format.number(tot, '0,000.00'));
            } else {
                val2 = parseFloat(ind.getData().levelPrice);
                //                //console.log(prev + val2);
                tot = prev + val2
                //            txt.setValue(Ext.util.Format.number(tot, '0,000.00'));
                //            txt.setValue(Ext.util.Format.number(
                //                parseFloat(txt.getValue()) + parseFloat(ind.getData().levelPrice),'0,000.00'));
            }
            Ext.getCmp('addnewform-totCost').setValue(Ext.util.Format.number(tot, '0,000.00'));
            txt.setValue(tot);
        } catch (ex) {
            //            //console.log(ex)
        }
        //    
    },
    onDel: function (grid, record, index) {
        //        WorkOrderMaintainances/deleteWorkOrderMaintainces.
        var woMod = Ext.create('singer.model.WorkOrderMaintain',
                record.data
                );
        delete woMod.data.id;
        //        //console.log(woMod);
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.MessageBox.confirm('Delete?', 'Do you Want to delete?', function (btn) {
            if (btn === 'yes') {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/deleteWorkOrderMaintainces',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(woMod.data),
                    success: function (response) {
                        var responseData = Ext.decode(response.responseText);
                        if (responseData.returnFlag === '1') {
                            grid.getStore().removeAt(index);
                        } else {
                            Ext.Msg.alert("Error...", responseData.returnMsg);
                        }

                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
        });


    },
    onSave: function (btn) {
        //        WorkOrderMaintainances/ editWorkOrderMaintainces
        var form = btn.up('form');
        var store = this.getGetWorkOrderListStore();

        var defect = [];
        var dat = Ext.getCmp('testGrid').getStore();
        dat.each(function (item) {
            defect.push(item.data);
        });

        var spars = [];
        var sdat = Ext.getCmp('spareN').getStore();
        //        //console.log(form.getValues());
        sdat.each(function (item) {
            item.data.wrokOrderNo = form.getValues().workOrderNo;
            spars.push(item.data);
        });
        //        //console.log(spars)
        var woMod = Ext.create('singer.model.WorkOrderMaintain',
                form.getValues()
                );
        var est = Ext.create('singer.model.WorkOrderEstimate',
                form.getValues()
                );
        //        //console.log(est.data.esRefNo);
        var estAr = [];
        var loginData = this.getLoggedInUserDataStore();
        //console.log(loginData.data.items[0].data.extraParams);
        var loggeduser = loginData.data.items[0].data.extraParams;
        if (est.data.esRefNo !== '') {
            est.set('bisId', loginData.data.getAt(0).get('extraParams'));
            //            //console.log(est.data);

            estAr.push(est.data);
            woMod.set('woEstimate', estAr);
        }
        woMod.set('woDefect', defect);
        woMod.set('woDetials', spars);
        woMod.set('workorderMaintainId', this.temp);
        woMod.set('bisId', loggeduser);

        //console.log(woMod.data.repairStatus);
        var repairStatus = woMod.data.repairStatus;

        if (repairStatus === 2 || repairStatus === 3) {
            Ext.MessageBox.confirm('Warning.!!', 'Are you sure you want to STOP updating this record ?', function (btn) {
                if (btn === 'yes') {
                    var sys = singer.AppParams.SYSTEM_CODE;
                    Ext.Ajax.request({
                        url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/editWorkOrderMaintainces',
                        method: 'POST',
                        headers: {
                            userId: loginData.data.getAt(0).get('userId'),
                            room: loginData.data.getAt(0).get('room'),
                            department: loginData.data.getAt(0).get('department'),
                            branch: loginData.data.getAt(0).get('branch'),
                            countryCode: loginData.data.getAt(0).get('countryCode'),
                            division: loginData.data.getAt(0).get('division'),
                            organization: loginData.data.getAt(0).get('organization'),
                            system: sys,
                            'Content-Type': 'application/json'
                        },
                        params: JSON.stringify(woMod.data),
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.returnFlag === '1') {
                                //                    btn.up('window').destroy();
                                Ext.getCmp('addnewform-printEst').setDisabled(false);
                                Ext.getCmp('addnewform-mailCus').setDisabled(false);
                                store.load();
                                singer.LayoutController.notify('Record Updated...', 'Record saved to database...');
                            } else {
                                Ext.Msg.alert("Error...", responseData.returnMsg);
                            }

                        },
                        failure: function () {
                            //                Ext.Msg.hide();
                            Ext.Msg.alert("Error in connection", "Please try again later");
                        }
                    });
                }
            });
        } else {
            var sys = singer.AppParams.SYSTEM_CODE;
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/editWorkOrderMaintainces',
                method: 'POST',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(woMod.data),
                success: function (response) {
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
                        //                    btn.up('window').destroy();
                        Ext.getCmp('addnewform-printEst').setDisabled(false);
                        Ext.getCmp('addnewform-mailCus').setDisabled(false);
                        store.load();
                        singer.LayoutController.notify('Record Updated...', 'Record saved to database...');
                    } else {
                        Ext.Msg.alert("Error...", responseData.returnMsg);
                    }

                },
                failure: function () {
                    //                Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        }

    },
    onView: function (view, record) {
        //        this.getController('CoreController').onOpenViewMoreWindow(record, 'wrkOmaintenaceView', 'View');

        var dat;
        var me = this;
        var loginData = this.getLoggedInUserDataStore();
        var form = Ext.widget('wrkOmaintenaceView');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Details of ' + record.get('workOrderNo'),
            items: [form]
        });
        Ext.getCmp('testGrid').getStore().removeAll();
        Ext.getCmp('spareN').getStore().removeAll();
        //        Ext.getCmp('addnewform-save').show();
        //        Ext.getCmp('addnewform-submit').hide();
        //        form.getForm().loadRecord(record);
        this.temp = record.data.workorderMaintainId;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getWorkOrderMaintainaceDetails?workorderMaintainId=' + record.data.workorderMaintainId + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            //                     params: JSON.stringify(form.getValues()),
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                dat = responseData;
                //                //console.log(dat);
                var def = dat.data[0].woDefect;
                //                //console.log(def)
                //                //console.log();
                Ext.getCmp('testGrid').getStore().add(def);
                var spair = dat.data[0].woDetials;
                Ext.getCmp('spareN').getStore().add(spair);
                //                var frm = form.down('form');
                //                //console.log(form.getForm());
                //        var form = win.down('form');
                //                frm.getForm().loadRecord(dat.data[0])
                form.getForm().setValues(dat.data[0]);
                var dt = new Date(dat.data[0].estimateClosingDate);
                form.getForm().findField('estimateClosingDate').setValue(dt);
                //                //console.log(spair)
                var tot = 0;
                for (i = 0; i < spair.length; i++) {
                    tot += spair[i].partSellPrice;
                }
                //                 //console.log();
                tot += parseFloat(Ext.getCmp('addnewform-totCost').getValue())
                Ext.getCmp('addnewform-totCost').setValue(tot)
                win.show();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    onCheckChange: function (cmp, rowIndex, checked, eOpts) {
        //        //console.log(rowIndex, checked)
        //
        //        var txt = Ext.getCmp('addSparePartsWindow-tot');
        //        var store = cmp.up('grid').getStore();
        //        var val = store.getAt(rowIndex).data.partSellPrice;
        //
        //        if (checked) {
        //            txt.setValue(parseFloat(txt.getValue()) + val);
        //        } else {
        //            txt.setValue(parseFloat(txt.getValue()) - val);
        //        }
        //
        //
        //        //console.log();

    },
    DoneAddNew: function (btn) {

        var me = this;
        var form = btn.up('form');

        var defect = [];
        var dat = Ext.getCmp('testGrid').getStore();
        dat.each(function (item) {
            defect.push(item.data);
        });

        var spars = [];
        var sdat = Ext.getCmp('spareN').getStore();
        //        //console.log(form.getValues());
        sdat.each(function (item) {
            item.data.wrokOrderNo = form.getValues().workOrderNo;
            spars.push(item.data);
        });
        //        //console.log(spars)
        var loginData = this.getLoggedInUserDataStore();
        //console.log(loginData.data.items[0].data.extraParams);
        var loggeduser = loginData.data.items[0].data.extraParams;

        var sys = singer.AppParams.SYSTEM_CODE;
        var woMod = Ext.create('singer.model.WorkOrderMaintain',
                form.getValues()
                );
        woMod.set('woDefect', defect);
        woMod.set('woDetials', spars);
        woMod.set('bisId', loggeduser);

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/addWorkOrderMaintainces',
            method: 'PUT',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(woMod.data),
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    btn.up('window').destroy();
                    me.getGetWorkOrderListStore().load();
                } else {
                    Ext.Msg.alert("Error...", responseData.returnMsg);
                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    AddingSparePartsDetails: function (cmp, rowIndex, checked, eOpts) {
        ////console.log('check changed');

        //        if (checked === true) {
        //            var temp = Ext.getStore('sparePrtStr').data.items[0].data;
        //            ////console.log(temp);
        //            this.rec.push(temp)
        //            //                var rec = [{part:partCde, desc:partDesc,price:partPrice}];
        //            //                var grid = Ext.getCmp('spare');
        //            //                grid.getStore().add(rec);
        //        } else {
        //
        //            //                Ext.getCmp('selctedJob').setValue(dedutVal);
        //        }
    },
    onDoneEvent: function (btn) {
        var form = btn.up('form');
        var grid = form.down('grid');
        var third = 0;
        var newPrice = 0;
        if (form.getForm().findField('chk').getValue()) {
            third = 1;
            newPrice = Ext.getCmp('addSparePartsWindow-tot').getValue();
        }

        var dat = [];
        var temp = 0;
        grid.getStore().data.each(function (item) {

            if (item.data.select) {
                //                //console.log(item.data);
                if (third === 1) {
                    item.data.partSellPrice = newPrice;
                }
                item.data.thirdPartyFlag = third;
                item.data.invoiceNo = form.getForm().findField('invoiceNo').getValue();
                //                //console.log(item.data)
                dat.push(item.data);
                temp++;
            }
        });
        if (temp > 1) {
            Ext.Msg.alert("Error....", "Select only one Sparepart");
        } else if (temp === 0) {
            Ext.Msg.alert("Error....", "Please select a spare part");
        } else {
            //            Ext.getCmp('spareN').getStore().removeAll();
            Ext.getCmp('spareN').getStore().add(dat);
            //            //console.log(dat);
            var nwVal = dat[0].partSellPrice;
            var txt = Ext.getCmp('addnewform-temp');
            var dis = Ext.getCmp('addnewform-totCost');
            txt.setValue(parseFloat(txt.getValue()) + parseFloat(nwVal));
            dis.setValue(Ext.util.Format.number(txt.getValue(), '0,000.00'));
            btn.up('window').destroy();
        }

        //        //console.log(dat);


        //        var str = Ext.getStore('sparePrtStr');
        //        var part_num = str.data.items[0].data;
        //        //console.log(str.data.items[0].data);
        //        //        var part_price = str.data.items[0].data.price;
        //        //        var rec = [{
        //        //            partNo: part_num,
        //        //            selliingPrice: part_price
        //        //        }];
        //        var grid = Ext.getStore('sparePartN');
        //        grid.add(part_num);
        //        //        if (grid.findExact('partNo', part_num) === -1) {
        //        //            grid.add(str.data);
        //        //        }
        //        var cancelFrm = btn.up('window');
        //        cancelFrm.close();
    },
    SubmitDefectCodes: function (btn, rowIndex) {
        var form = btn.up('form');
        var mod = Ext.create('singer.model.WorkOrderDefect',
                form.getValues()
                );
        mod.set('majDesc', Ext.getCmp('mjrD').getRawValue());
        mod.set('minDesc', Ext.getCmp('minD').getRawValue());
        var grid = Ext.getCmp('testGrid');
        //        //console.log(grid.getStore())
        grid.getStore().add(mod.data);
        var cancelFrm = btn.up('window');
        cancelFrm.close();
    },
    addSparePartsDetails: function () {
        var part = Ext.getCmp('partNo').getValue();
        var desc = Ext.getCmp('descrip').getValue();
        if (part !== '' || desc !== '') {
            if (part === '')
                part = 0;

            var loginData = this.getLoggedInUserDataStore();
            Ext.Msg.wait('Please wait', 'Gathering Information...', {
                interval: 300
            });
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'Parts/getPartsDetails?erpPartNo=' + part + '&partDesc=' + desc + '',
                method: 'GET',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    'Content-Type': 'application/json'
                },
                //                     params: JSON.stringify(form.getValues()),
                success: function (response) {
                    Ext.Msg.hide();
                    var responseData = Ext.decode(response.responseText);
                    var dat = responseData.data;
                    //console.log(dat);
                    if (dat.length !== 0) {
                        //                        //console.log(dat);
                        var erpPartNo = responseData.data[0].erpPartNo;
                        var grid = Ext.getCmp('spare');
                        //console.log(grid.getStore());
                        var resul = grid.getStore().findExact('erpPartNo', erpPartNo);
                        if (resul === -1) {
                            grid.getStore().removeAll();
                            grid.getStore().add(dat);
                        }
                    } else {
                        Ext.Msg.alert("Try Again", " Search Term Unavailable");
                    }
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        }



        //        if (part !== '') {
        //            var loginData = this.getLoggedInUserDataStore();
        //            Ext.Ajax.request({
        //                url: singer.AppParams.JBOSS_PATH + 'Parts/getPartsDetails?partNo=' + part,
        //                method: 'GET',
        //                headers: {
        //                    userId: loginData.data.getAt(0).get('userId'),
        //                    room: loginData.data.getAt(0).get('room'),
        //                    department: loginData.data.getAt(0).get('department'),
        //                    branch: loginData.data.getAt(0).get('branch'),
        //                    countryCode: loginData.data.getAt(0).get('countryCode'),
        //                    division: loginData.data.getAt(0).get('division'),
        //                    organization: loginData.data.getAt(0).get('organization'),
        //                    'Content-Type': 'application/json'
        //                },
        //                //                     params: JSON.stringify(form.getValues()),
        //                success: function (response) {
        //                    var responseData = Ext.decode(response.responseText);
        //                    //                        ////console.log(responseData.data[0]);
        //                    //console.log(responseData);
        //
        //                    var dat = responseData.data;
        //                    if (dat.length !== 0) {
        //                        //console.log(dat);
        //                        var parNo = responseData.data[0].partNo;
        //                        var grid = Ext.getCmp('spare');
        //                        var resul = grid.getStore().findExact('partNo', parNo);
        //                        if (resul === -1) {
        //                            grid.getStore().add(dat);
        //                        }
        //                    } else {
        //                        Ext.Msg.alert("Oops...", "No results");
        //                    }
        //                },
        //                failure: function () {
        //                    Ext.Msg.hide();
        //                    Ext.Msg.alert("Error in connection", "Please try again later");
        //                }
        //            });
        //
        //        } else {
        //            //console.log('else part');
        //            var loginData = this.getLoggedInUserDataStore();
        //            Ext.Ajax.request({
        //                url: singer.AppParams.JBOSS_PATH + 'Parts/getPartsDetails?partDesc=' + desc,
        //                method: 'GET',
        //                headers: {
        //                    userId: loginData.data.getAt(0).get('userId'),
        //                    room: loginData.data.getAt(0).get('room'),
        //                    department: loginData.data.getAt(0).get('department'),
        //                    branch: loginData.data.getAt(0).get('branch'),
        //                    countryCode: loginData.data.getAt(0).get('countryCode'),
        //                    division: loginData.data.getAt(0).get('division'),
        //                    organization: loginData.data.getAt(0).get('organization'),
        //                    'Content-Type': 'application/json'
        //                },
        //                success: function (response) {
        //                    var responseData = Ext.decode(response.responseText);
        //                    var partCde = responseData.data[0].partPrdCode;
        //                    var partDesc = responseData.data[0].partDesc;
        //                    var partPrice = responseData.data[0].partSellPrice;
        //                    var rec = [{
        //                        part: partCde,
        //                        desc: partDesc,
        //                        price: partPrice
        //                        }];
        //                    var grid = Ext.getCmp('spare');
        //                    grid.getStore().add(rec);
        //                },
        //                failure: function () {
        //                    Ext.Msg.hide();
        //                    Ext.Msg.alert("Error in connection", "Please try again later");
        //                }
        //            });
        //        }

    },
    clickOnSelectWorkOrder: function (btn, rowIndex) {
        var wrkNO = rowIndex.data.workOrderNo;
        var immi = rowIndex.data.imeiNo;
        Ext.getCmp('wrkN').setValue(wrkNO);
        Ext.getCmp('addnewform-imeiNo').setValue(immi);

        var repLevelStr = Ext.getStore('ReplevelSTR');
        repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + immi;
        repLevelStr.load();

        var cancelFrm = btn.up('window');
        cancelFrm.close();
        var under = Ext.getCmp('addnewform-underWar');
        var non = Ext.getCmp('addnewform-nonWar');
        if (rowIndex.get('warrentyVrifType') === 1) {
            under.show();
            under.allowBlank = false;
            non.hide();
            non.allowBlank = true;
        } else {
            under.hide();
            under.allowBlank = true;
            non.show();
            non.allowBlank = false;
        }
    },
    cancelAddNewWorkOrderMaintenanceForm: function (btn) {
        var cancelFrm = btn.up('window');
        cancelFrm.close();
    },
    openAddDefectWindow: function () {
        var form = Ext.widget('addDeffectWindow');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add Defect',
            items: [form]
        });
        var mjrdefectStr = Ext.getStore('mjrdefectStr');
        if (mjrdefectStr.getCount() === 0) {
            mjrdefectStr.load();
        }
        var minordefectStr = Ext.getStore('minordefectStr');
        if (minordefectStr.getCount() === 0) {
            minordefectStr.load();
        }
        win.show();
    },
    openSparePartsWindowGrid: function () {
        var form = Ext.widget('addSparePartsWindow');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add Spares',
            items: [form]
        });
        Ext.getCmp('spare').getStore().removeAll();
        win.show();
    },
    openSelectWorkOrderWindow: function () {
        console.log('test');
        //var store = this.getWorkOrderOpeningStore();
        var store = this.getWorkOrderMaintainAddStore();

        this.getController('CoreController').loadBindedStore(store);
        //=====================
//            var repLevelStr = this.getReplevelSTRStore();
//            repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + record.get("eventId");
//        
        //=====================
        var form = Ext.widget('selectWrkOrderGrid');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        win.show();
    },
    //    
    openAddNewWindow: function () {
        var form = Ext.widget('addnewform');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        var loginData = this.getLoggedInUserDataStore();
        //console.log(loginData.data.getAt(0).get('commonName'));
        Ext.getCmp('technicianName').setValue(loginData.data.getAt(0).get('commonName'));
        Ext.getCmp('testGrid').getStore().removeAll();
        Ext.getCmp('spareN').getStore().removeAll();
        Ext.getCmp('addnewform-save').hide();
        Ext.getCmp('addnewform-submit').show();
        win.show();
    },
    openEditWindow: function (grid, record) {//sh
        console.log('record', record.data.iemiNo);
        Ext.getStore('ReplevelSTR').load();

        var repLevelStr = Ext.getStore('ReplevelSTR');
        repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + record.data.iemiNo;
        repLevelStr.load();

        var dat;
        var me = this;
        var loginData = this.getLoggedInUserDataStore();
        var form = Ext.widget('addnewform');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Edit Workorder ' + record.get('workOrderNo'),
            items: [form]
        });

        var editBtn = Ext.getCmp('selectWoBtn');
        editBtn.setDisabled(true);

        Ext.getCmp('testGrid').getStore().removeAll();
        Ext.getCmp('spareN').getStore().removeAll();
        Ext.getCmp('addnewform-save').show();
        Ext.getCmp('addnewform-submit').hide();
        //        //console.log(record.getData().repairStatus)
        form.getForm().findField('technician').setReadOnly(true);
        //        form.getForm().loadRecord(record);
        this.temp = record.data.workorderMaintainId;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getWorkOrderMaintainaceDetails?workorderMaintainId=' + record.data.workorderMaintainId + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            //                     params: JSON.stringify(form.getValues()),
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                dat = responseData;
                //                //console.log(dat);
                var def = dat.data[0].woDefect;
                //console.log(def)
                //                //console.log();
                Ext.getCmp('testGrid').getStore().add(def);
                var spair = dat.data[0].woDetials;
                Ext.getCmp('spareN').getStore().add(spair);
                //                var frm = form.down('form');
                //                //console.log(form.getForm());
                //        var form = win.down('form');
                //                frm.getForm().loadRecord(dat.data[0])
                form.getForm().setValues(dat.data[0]);
                //console.log(dat.data[0]);
                var dt = new Date(dat.data[0].estimateClosingDate);
                form.getForm().findField('estimateClosingDate').setValue(dt);
                //                //console.log(spair)
                var tot = 0;
                for (i = 0; i < spair.length; i++) {
                    tot += spair[i].partSellPrice;
                }
                //                 //console.log();
                var txt = Ext.getCmp('addnewform-temp');
                txt.setValue(parseFloat(Ext.getCmp('levelPrice').getValue()));
                //                //console.log('before add tot ', txt.getValue())
                tot += parseFloat(txt.getValue());
                txt.setValue(tot);
                Ext.getCmp('addnewform-totCost').setValue(Ext.util.Format.number(tot, '0,000.00'));

                win.show();
//                var under = Ext.getCmp('addnewform-underWar');
//                var non = Ext.getCmp('addnewform-nonWar');
                //                //console.log(record)
//                if (responseData.data[0].warrantyVrifType === 1) {
////                    under.show();
////                    under.allowBlank = false;
////                    non.hide();
////                    non.allowBlank = true;
//                } else {
////                    under.hide();
////                    under.allowBlank = true;
////                    non.show();
////                    non.allowBlank = false;
//                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });







        //        var form = Ext.widget('editForm');
        //        var win = Ext.create("Ext.window.Window", {
        //            layout: 'fit',
        //            iconCls: 'icon-verification',
        //            modal: true,
        //            constrain: true,
        //            title: 'Edit',
        //            items: [form]
        //        });
        //        win.show();
    },
    initializeGrid: function (grid) {
        var store = this.getGetWorkOrderListStore();
        store.load();
        //        this.getController('CoreController').loadBindedStore(store);
    },
    //    addNewWorkOrderFormCancel: function (btn) {
    //        
    //        var form = btn.up('form').up().up();
    //        
    //        Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function(btnn) {
    //                    if (btnn === "yes") {
    //                          form.close();
    //                    } else {
    //                    }
    //                });
    //    },

    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    //    onView: function (view, record) {
    //        this.getController('CoreController').onOpenViewMoreWindow(record, 'wrkOrderview', 'Work Order Opening details of WORK ORDER NO :'+ record.get('workOrderNo'));
    //    },

    //    insertFormCanceladdIMEI: function (btn) {
    //        this.getController('CoreController').onInsertFormCancel(btn);
    //    },
    //   
    //    
    //    insertRecord: function (btn) {
    //        //////console.log('comming to insert record');
    ////        var store = this.getImeiAdjsStore();
    //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Record Created.', 'Creating New record', true, true, this.getGrid(), false);
    ////        var grid = this.getGrid();
    //
    //    },
    //    
    //    editFormCancel: function(btn) {
    //        this.getController('CoreController').onInsertFormCancel(btn);
    //    },
    //    insertFormCancel: function(btn) {
    //        this.getController('CoreController').onInsertFormCancel(btn);
    //    },
    //    onAdjustImei: function () {
    //        this.getController('CoreController').openInsertWindow('debitnoteform');
    //
    //    },
    //    onEdit:function(view, record){
    //        this.getController('CoreController').openUpdateWindow(record, 'imeiform', 'Edit details of Debit Note No:'+ record.get('debitNoteNo'));
    //    },
    //    updataRecord: function (btn) {
    //        this.getController('CoreController').onRecordUpdate(btn, this.getImeiAdjsStore(), 'IMEI Data Updated.', 'Updating IMEI Data', true, true, this.getGrid());
    //    },

});