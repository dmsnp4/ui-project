
Ext.define('singer.controller.SapreMaintnencaeController',{
    extend: 'Ext.app.Controller',
    
    views: [
        'Administrator.sparesMaintenance.SpareMaintenanceGrid',
        'Administrator.sparesMaintenance.SpareMaintenanceView',
        'Administrator.sparesMaintenance.Edit'
    ],
    stores:['Spares','LoggedInUserData'],
    refs: [
        {
            ref:'spare_grid',
            selector:'sparemaintenancegrid'
        },
        {
            ref:'SpareMaintenanceView',
            selector:'spareView'
        },
        {
            ref:'SpareMaintenanceEdit',
            selector:'editspares'
        }
    ],
    
        init: function() {
        var me = this;
        me.control({
            'sparemaintenancegrid': {
                added: this.initializeGrid
            },            
            'sparemaintenancegrid  ':{
                sparesView:this.onSparesView
            },
            ' sparemaintenancegrid  ':{
                editSpares:this.onSparesEdit
            },
            'spareView button[action=cancel]': {
                click: this.insertFormCancel
            },
            'sparemaintenancegrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'editspares button[action=save]': {
                click: this.save_edit
            },
        });
    },
    
    save_edit:function(btn){
        var store=Ext.getStore('Spares');
        console.log("save spare parts");
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
        var fomval = form.getValues();
        fomval.status='3';
//        var store = this.getGetWorkOrderCloseStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'Parts/editParts',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(fomval),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
                    window.destroy();
                    store.load();
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    
    onSparesEdit:function(grid, record){
        console.log("comming to edit");
        var win = Ext.widget('editspares');
        
        var formWindow = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Edit ',
            items: [win]
        });
        ////console.log(record.data);
        formWindow.show();
        var form = formWindow.down('form');
        form.getForm().loadRecord(record);
        win.show();
    },
    //**
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    //**
    initializeGrid: function(grid) {
        var me = this;
        var tempStore = me.getSparesStore();
        this.getController('CoreController').loadBindedStore(tempStore);
    },
    //**
    insertFormCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },

//**
    onSparesView:function(view, record){
        this.getController('CoreController').onOpenViewMoreWindow(record, 'spareView','View');
    }
    
});