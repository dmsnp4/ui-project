
var plainExcelUrl = "";



Ext.define('singer.controller.CoreController', {
    extend: 'Ext.app.Controller',
    //    controllers: [
    //        'UserMaintenence'
    //    ],
    stores: [
        'LoggedInUserData'
    ],
    init: function () {

    },
    appendLDAPHeadersToRequest: function () {
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        return {
            userId: loginData.first().get('userId'),
            room: loginData.first().get('room'),
            department: loginData.first().get('department'),
            branch: loginData.first().get('branch'),
            countryCode: loginData.first().get('countryCode'),
            division: loginData.first().get('division'),
            organization: loginData.first().get('organization'),
            system: sys,
            'Content-Type': 'application/json'
        };
    },
    syncStoreAfterInsertWithLoading: function (store, form, successTitle, successMsg) {
        var me = this;
        form.up("window").setDisabled(true);
        store.on('beforesync', function () {
            Ext.getBody().mask(store.insertingMsg);
        });
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                form.up("window").setDisabled(false);
                var response = batch.proxy.getReader().jsonData;
                var isSuccess = false;
                if (parseInt(response) === 1) {
                    isSuccess = true;
                    FXApp.LayoutController.notify(successTitle, successMsg);
                } else {
                    store.rejectChanges();
                    FXApp.LayoutController.createErrorPopup("Error Occured while processing", response, Ext.MessageBox.WARNING);
                }
                me.insertFormPrepareAfterSyncStore(form, isSuccess);
                Ext.getBody().mask(store.loadingMsg);
                me.loadBindedStore(store);
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                form.up("window").setDisabled(false);
                FXApp.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    insertFormPrepareAfterSyncStore: function (insertFormComp, isSuccess) {
        var form = insertFormComp.getForm();
        if (isSuccess) {
            form.reset();
        }
        var firstPanel = insertFormComp.items.items[0];
        firstPanel.expand();
        firstPanel.items.items[0].focus(false, 300);
    },
    syncStoreAfterModifyWithLoading: function (store, form, successTitle, successMsg) {
        var me = this;
        form.up("window").setDisabled(true);
        store.on('beforesync', function () {
            Ext.getBody().mask(store.updatingMsg);
        });
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                form.up("window").setDisabled(false);
                var response = batch.proxy.getReader().jsonData;
                var isSuccess = false;
                if (parseInt(response) === 1) {
                    isSuccess = true;
                    FXApp.LayoutController.notify(successTitle, successMsg);
                } else {
                    store.rejectChanges();
                    FXApp.LayoutController.createErrorPopup("Error Occured while processing", response, Ext.MessageBox.WARNING);
                }
                me.updatingFormPrepareAfterSyncStore(form, isSuccess);
                Ext.getBody().mask(store.loadingMsg);
                me.loadBindedStore(store);
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                form.up("window").setDisabled(false);
                FXApp.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    updatingFormPrepareAfterSyncStore: function (updateFormComp, isSuccess) {
        //////console.log(isSuccess);
        updateFormComp.up('window').destroy();
        /*if (isSuccess) {
         updateFormComp.up('window').destroy();
         } else {
         var firstPanel = updateFormComp.items.items[0];
         firstPanel.expand();
         firstPanel.items.items[0].focus(false, 300);
         }*/
    },
    syncStoreAfterRemoveWithLoading: function (store, successTitle, successMsg) {
        var me = this;
        store.on('beforesync', function () {
            Ext.getBody().mask(store.removingMsg);
        });
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                var response = batch.proxy.getReader().jsonData;
                if (parseInt(response) === 1) {
                    FXApp.LayoutController.notify(successTitle, successMsg);
                } else {
                    store.rejectChanges();
                    FXApp.LayoutController.createErrorPopup("Error Occured while processing", response, Ext.MessageBox.WARNING);
                }
                Ext.getBody().mask(store.loadingMsg);
                me.loadBindedStore(store);
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                FXApp.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    accordionInsertFormReset: function (btn) {
        var me = this;
        var changedField = me.checkChanagesforInsertForm(btn.up('form'));
        if (changedField !== false) {
            Ext.Msg.confirm("Reset Form?", "Form data is modified.</br> You Want to Reset Form?", function (btnn) {
                if (btnn === "yes") {
                    btn.up('form').getForm().reset();
                    var panels = btn.up('form').items.items;
                    me.removeInvalidStylesFromAccordionPanels(btn.up('window'));
                    if (panels[0].items.items[0].isHidden() && panels[0].items.items[0].getXType() !== 'formheader') {
                        panels[0].items.items[1].focus(false, 300);
                    } else {
                        panels[0].items.items[0].focus(false, 300);
                    }
                } else {
                    changedField.up('panel');
                    changedField.up('panel').expand(true);
                    changedField.focus(false, 300);
                }
            });
        } else {
            me.removeInvalidStylesFromAccordionPanels(btn.up('window'));
            btn.up('form').getForm().reset();
        }
    },
    removeInvalidStylesFromAccordionPanels: function (window) {
        var panels = window.down('form').items.items;
        for (var i = 0; i < panels.length; i++) {
            var header = Ext.get(panels[i].getEl().dom.children[0].id);
            var headerText = Ext.get(((((header.dom.children[0]).children[0]).children[0]).children[0]).children[0].id);
            if (header.hasCls('alert-danger')) {
                header.removeCls('alert-danger');
            }
            if (headerText.hasCls('alert-danger-text')) {
                headerText.removeCls('alert-danger-text');
            }
        }
    },
    applyErrorStyleForInvalidAccordionPanels: function (window) {
        var invalidPanels = [];
        var panels = window.down('form').items.items;
        for (var i = 0; i < panels.length; i++) {
            var fields = panels[i].items.items;
            for (var x = 0; x < fields.length; x++) {
                if (fields[x].getXType() !== 'formheader') {
                    if (!fields[x].isValid()) {
                        var header = Ext.get(panels[i].getEl().dom.children[0].id);
                        header.addCls('alert-danger');
                        var headerText = Ext.get(((((header.dom.children[0]).children[0]).children[0]).children[0]).children[0].id);
                        headerText.addCls('alert-danger-text');
                        invalidPanels.push(panels[i]);
                    }
                }
            }
        }
        if (invalidPanels.length !== 0) {
            invalidPanels[0].expand();
            if (invalidPanels[0].items.items[0].isHidden() && invalidPanels[0].items.items[0].isDisabled() && invalidPanels[0].items.items[0].getXType() !== 'formheader') {
                invalidPanels[0].items.items[1].focus(false, 300);
            } else {
                invalidPanels[0].items.items[0].focus(false, 300);
            }
        }
    },
    accordionFormSpecialKeyAction: function (field, e, parentController) {
        //////console.log(e.getKey());
        var currentlyOpenedForm = Ext.ClassManager.getAliasesByName(Ext.ClassManager.getName(Ext.ClassManager.getClass(field.up('window'))))[0].split('widget.')[1];
        if (e.getKey() === e.ENTER && (field.getXType() !== 'combobox')) {
            if (!Ext.getCmp(currentlyOpenedForm + '-create').isHidden()) {
                parentController.insertFormSubmit(field);
            } else if (!Ext.getCmp(currentlyOpenedForm + '-save').isHidden()) {
                parentController.updateFormSubmit(field);
            }
        } else if (e.getKey() === e.TAB) {
            var panelItems = field.up('panel').items.keys;
            var panelItemsCount = field.up('panel').items.keys.length;
            var lastElementID = panelItems[panelItemsCount - 1];
            if (field.id === lastElementID) {
                if (field.up('panel').nextSibling('panel') !== null) {
                    field.up('panel').nextSibling('panel').expand(false);
                    field.up('panel').nextSibling('panel').items.items[0].focus(false, 300);
                } else {
                    field.up('panel').up('form').items.items[0].expand(false);
                    var firstCollapsiblePanel = field.up('panel').up('form').items.items[0];
                    firstCollapsiblePanel.items.items[0].focus(false, 300);
                }
            }
        }
    },
    focusToFirstAccessibleFormElement: function (formItems) {
        for (var i = 0; i < formItems.length; i++) {

        }
    },
    accordionFormBeforeClose: function (window) {
        var me = this;
        var currentlyOpenedForm = Ext.ClassManager.getAliasesByName(Ext.ClassManager.getName(Ext.ClassManager.getClass(window)))[0].split('widget.')[1];
        var cancelBool = false;
        if (!Ext.getCmp(currentlyOpenedForm + '-create').isHidden()) {
            var changedField = me.checkChanagesforInsertForm(window.down('form'));
            if (changedField !== false) {
                Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function (btnn) {
                    if (btnn === "yes") {
                        window.destroy();
                        cancelBool = true;
                    } else {
                        changedField.up('panel');
                        changedField.up('panel').expand(true);
                        changedField.focus(true, 300);
                        cancelBool = false;
                    }
                });
                cancelBool = false;
            } else {
                cancelBool = true;
            }
        } else if (!Ext.getCmp(currentlyOpenedForm + '-save').isHidden()) {
            var changedField = me.checkRecordDataisUpdated(window.down('form'));
            if (changedField !== false) {
                Ext.Msg.confirm("Save Changes?", "Record data is modified.</br> You want to close form without saving modifications?", function (btnn) {
                    if (btnn === "yes") {
                        window.destroy();
                        cancelBool = true;
                    } else {
                        changedField.up('panel');
                        changedField.up('panel').expand(true);
                        changedField.focus(false, 300);
                        cancelBool = false;
                    }
                });
                cancelBool = false;
            } else {
                cancelBool = true;
            }
        }
        return cancelBool;
    },
    assetSearchByCombos: function (catArray, locArray) {
        var assetStore = Ext.getStore('allAssetsStore');
        var catStore = Ext.getStore('allCategoriesStore');
        catStore.clearFilter();
        var locStore = Ext.getStore('allLocationsStore');
        locStore.clearFilter();
        var selectedCat = null;
        var selectedLoc = null;
        for (var i = 0; i < catArray.length; i++) {
            if (!Ext.isEmpty(catArray[i])) {
                selectedCat = {
                    cat_id: catArray[i],
                    cat_rec: catStore.findRecord('asset_cat_id', catArray[i], 0, false, true, true)
                };
                break;
            }
        }
        for (var i = 0; i < locArray.length; i++) {
            if (!Ext.isEmpty(locArray[i])) {
                selectedLoc = {
                    loc_id: locArray[i],
                    loc_rec: locStore.findRecord('location_id', locArray[i], 0, false, true, true)
                };
                break;
            }
        }
        if (Ext.isEmpty(selectedCat) && Ext.isEmpty(selectedLoc)) {
            assetStore.clearFilter();
        } else {
            var selectedCatLevel = (selectedCat === null ? 100 : selectedCat.cat_rec.get('asset_category_type'));
            var selectedLocLevel = (selectedLoc === null ? 100 : selectedLoc.loc_rec.get('location_level'));
            var microCatIDs = [];
            var catID = (selectedCat === null ? null : selectedCat.cat_rec.get('asset_cat_id'));
            if (selectedCatLevel === 1) {
                catStore.each(function (minRecord) {
                    if (minRecord.get('asset_parent_category_id') === catID) {
                        catStore.each(function (micRecord) {
                            if (micRecord.get('asset_parent_category_id') === minRecord.get('asset_cat_id')) {
                                microCatIDs.push(micRecord.get('asset_cat_id'));
                            }
                        });
                    }
                });
            } else if (selectedCatLevel === 2) {
                catStore.each(function (micRecord) {
                    if (micRecord.get('asset_parent_category_id') === catID) {
                        microCatIDs.push(micRecord.get('asset_cat_id'));
                    }
                });
            } else if (selectedCatLevel === 3) {
                microCatIDs.push(catID);
            }
            //////console.log(microCatIDs);
            var roomLocIDs = [];
            var locID = (selectedLoc === null ? null : selectedLoc.loc_rec.get('location_id'));
            if (selectedLocLevel === 1) {
                locStore.each(function (divRecord) {
                    if (divRecord.get('parent_location_id') === locID) {
                        locStore.each(function (deptRecord) {
                            if (deptRecord.get('parent_location_id') === divRecord.get('location_id')) {
                                locStore.each(function (roomRecord) {
                                    if (roomRecord.get('parent_location_id') === deptRecord.get('location_id')) {
                                        roomLocIDs.push(roomRecord.get('location_id'));
                                    }
                                });
                            }
                        });
                    }
                });
            } else if (selectedLocLevel === 2) {
                locStore.each(function (deptRecord) {
                    if (deptRecord.get('parent_location_id') === locID) {
                        locStore.each(function (roomRecord) {
                            if (roomRecord.get('parent_location_id') === deptRecord.get('location_id')) {
                                roomLocIDs.push(roomRecord.get('location_id'));
                            }
                        });
                    }
                });
            } else if (selectedLocLevel === 3) {
                locStore.each(function (roomRecord) {
                    if (roomRecord.get('parent_location_id') === locID) {
                        roomLocIDs.push(roomRecord.get('location_id'));
                    }
                });
            } else if (selectedLocLevel === 4) {
                roomLocIDs.push(locID);
            }
            //////console.log(roomLocIDs);
            var assetFilterOperator = "";
            if (roomLocIDs.length !== 0 && microCatIDs.length === 0) {
                for (var i = 0; i < roomLocIDs.length; i++) {
                    if (i === (roomLocIDs.length - 1)) {
                        assetFilterOperator += " assetData.data.location_id === " + roomLocIDs[i] + "";
                    } else {
                        assetFilterOperator += " assetData.data.location_id === " + roomLocIDs[i] + " ||";
                    }
                }
            } else if (roomLocIDs.length === 0 && microCatIDs.length !== 0) {
                for (var i = 0; i < microCatIDs.length; i++) {
                    if (i === (microCatIDs.length - 1)) {
                        assetFilterOperator += " assetData.data.asset_cat_id === " + microCatIDs[i] + "";
                    } else {
                        assetFilterOperator += " assetData.data.asset_cat_id === " + microCatIDs[i] + " ||";
                    }
                }
            } else if (roomLocIDs.length !== 0 && microCatIDs.length !== 0) {
                assetFilterOperator = "(";
                for (var i = 0; i < microCatIDs.length; i++) {
                    if (i === (microCatIDs.length - 1)) {
                        assetFilterOperator += " assetData.data.asset_cat_id === " + microCatIDs[i] + "";
                    } else {
                        assetFilterOperator += " assetData.data.asset_cat_id === " + microCatIDs[i] + " ||";
                    }
                }
                assetFilterOperator += ") && (";
                for (var i = 0; i < roomLocIDs.length; i++) {
                    if (i === (roomLocIDs.length - 1)) {
                        assetFilterOperator += " assetData.data.location_id === " + roomLocIDs[i] + ")";
                    } else {
                        assetFilterOperator += " assetData.data.location_id === " + roomLocIDs[i] + " ||";
                    }
                }
            }
            //////console.log(assetFilterOperator);
            var assetFilter = new Ext.util.Filter({
                id: 'assetFilterLoc',
                filterFn: function (assetData) {
                    return (eval(assetFilterOperator));
                }
            });
            assetStore.filter(assetFilter);
        }
    },
    loadBindedStore: function (store, start, limit, page) {
        var me = this;
        this.getController('Main').setHeaders(store);

        store.getProxy().api.read = this.resetProxy(this.getProxyReadURI(store)); //thisStore.getProxy().api.read;
        store.load();
        //        if (Ext.isDefined(start) && Ext.isDefined(limit) && Ext.isDefined(page)) {
        //            store.loadPage(page, {
        //                callback: function (records, operation, success) {
        //                    if (Ext.getBody().isMasked()) {
        //                        Ext.getBody().unmask();
        //                    }
        //                    if (operation.success === false) {
        //                        me.getController('ExceptionsController').openStoreReloaderWindow(store);
        //                    }
        //                }
        //            });
        //        } else if (Ext.isDefined(start) && Ext.isDefined(limit)) {
        //            store.load({
        //                start: start,
        //                limit: limit,
        //                callback: function (records, operation, success) {
        //                    if (Ext.getBody().isMasked()) {
        //                        Ext.getBody().unmask();
        //                    }
        //                    if (operation.success === false) {
        //                        me.getController('ExceptionsController').openStoreReloaderWindow(store);
        //                    }
        //                }
        //            });
        //        } else {
        //            store.load({
        //                callback: function (records, operation, success) {
        //                    if (Ext.getBody().isMasked()) {
        //                        Ext.getBody().unmask();
        //                    }
        //                    if (operation.success === false) {
        //                        me.getController('ExceptionsController').openStoreReloaderWindow(store);
        //                    }
        //                }
        //            });
        //        }
    },
    openInsertWindow: function (alias) {

        var win = Ext.widget(alias);

        win.show();
        var form = win.down('form');
        this.focusToFirstFieldinForm(form);
        form.items.items[0].focus(false, 200);

        //        Ext.getCmp(alias + '-reset-insert').setVisible(true);
        //        Ext.getCmp(alias + '-reset-update').setVisible(false);
        Ext.getCmp(alias + '-create').setVisible(true);
        Ext.getCmp(alias + '-save').setVisible(false);
    },
    openUpdateWindow: function (record, alias, title) {
        var win = Ext.widget(alias);
        //        //////console.log(record);

        var form = win.down('form');
        form.getForm().loadRecord(record);
        win.show();
        win.setTitle(title);
        this.focusToFirstFieldinForm(win.down('form'));
        //        Ext.getCmp(alias + '-reset-insert').setVisible(false);
        //        Ext.getCmp(alias + '-reset-update').setVisible(true);
        Ext.getCmp(alias + '-create').setVisible(false);
        Ext.getCmp(alias + '-save').setVisible(true);
    },
    // generic validations before reseting and closing starts
    //focus to first field in any form
    focusToFirstFieldinForm: function (form) {
        var formm = form.getForm();
        var fields = formm.getFields().items;

        for (var i = 0; i < fields.length; i++) {
            //            //////console.log(fields[i]);
            //            fields[i].focus(false, 300);
            //            break;
            //            if ((fields[i].xtype !== 'hiddenfield')) {
            if ((fields[i].xtype !== 'displayfield')) {
                if ((!fields[i].isDisabled())) {
                    fields[i].focus(false, 300);
                    break;
                }
            }
            //            }
        }
    },
    //focus to first invalid field in any form
    focusToFirstDirtyFieldinForm: function (form) {
        var formm = form.getForm();
        var fields = formm.getFields().items;
        for (var i = 0; i < fields.length; i++) {
            //////console.log(fields[i]);
            fields[i].focus(true, 300);
            break;
            //  if ((fields[i].xtype !== 'hiddenfield')) {
            //     if ((fields[i].xtype !== 'displayfield')) {
            //            if (!fields[i].isValid()) {
            //                //////console.log(fields[i]);
            //                fields[i].focus(true, 300);
            //                break;
            //            }
            //   }
            //  }
        }
    },
    //check update form record details changed before save
    checkRecordDataisUpdated: function (form) {
        var formm = form.getForm();
        var record = formm.getRecord();
        var fields = formm.getFields().items;
        var isModified = false;
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].getXType() === 'datefield') {
                if (record.get(fields[i].name) !== undefined && (Ext.Date.format(new Date(fields[i].getValue()), 'Y-m-d') != Ext.Date.format(new Date(record.get(fields[i].name)), 'Y-m-d'))) {
                    //////console.log(record.get(fields[i].name));
                    //////console.log(fields[i].name, fields[i].getValue());
                    isModified = fields[i];
                    break;
                }
            } else {
                if (record.get(fields[i].name) !== undefined && (fields[i].getValue() != record.get(fields[i].name))) {
                    //////console.log(record.get(fields[i].name));
                    //////console.log(fields[i].name, fields[i].getValue());
                    isModified = fields[i];
                    break;
                }
            }
        }
        return isModified;
    },
    //check changes happened before save in insert form
    checkChanagesforInsertForm: function (form) {
        var hasChanged = false;
        var formm = form.getForm();
        var fields = formm.getFields().items;
        for (var i = 0; i < fields.length; i++) {
            if ((fields[i].xtype !== 'hiddenfield') && (fields[i].xtype !== 'hidden')) {
                if ((fields[i].xtype !== 'displayfield')) {
                    if (fields[i].isValid() && (fields[i].getValue() !== '' && fields[i].getValue() !== null)) {
                        //fields[i].focus(true, 300);
                        hasChanged = fields[i];
                        break;
                    }
                }
            }
        }
        return hasChanged;
    },
    checkChangesBeforeClose: function (window, alias) {
        var me = this;
        var cancelBool = false;
        if (!Ext.getCmp(alias + '-create').isHidden()) {
            var changedField = me.checkChanagesforInsertForm(window.down('form'));
            if (changedField !== false) {
                //ask message nicely
                Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function (btnn) {
                    if (btnn === "yes") {
                        window.destroy();
                        cancelBool = true;
                    } else {
                        changedField.focus(true, 300);
                        cancelBool = false;
                    }
                });
                cancelBool = false;
            } else {
                cancelBool = true;
            }
        } else if (!Ext.getCmp(alias + '-save').isHidden()) {
            var changedField = this.checkRecordDataisUpdated(window.down('form'));
            //          me.focusToFirstDirtyFieldinForm(window.down('form'));
            //            //////console.log(changedField);
            if (changedField !== false) {
                Ext.Msg.confirm("Save Changes?", "Record data is modified.</br> You want to close form without saving modifications?", function (btnn) {
                    if (btnn === "yes") {
                        window.destroy();
                        cancelBool = true;
                    } else {
                        changedField.focus(false, 300);
                        cancelBool = false;
                    }
                });
                cancelBool = false;
            } else {
                cancelBool = true;
            }
        }
        ////////console.log('cancelboo', cancelBool);
        return cancelBool;
    },
    //insert form start
    onInsertFormCancel: function (btn) {
        var window = btn.up('window');
        var form = btn.up('form');
        var changedField = this.checkChanagesforInsertForm(form);
        if (changedField !== false) {
            //ask message nicely
            Ext.Msg.confirm("Exit Form?", "Unsaved changes found.</br>You want to exit form before save it?", function (btnn) {
                if (btnn === "yes") {
                    //                    form.getForm().reset();
                    //                    me.focusToFirstFieldinForm(form);
                    window.close();
                } else {
                    changedField.focus(true, 300);
                }
            });
        } else {
            //            form.getForm().reset();
            //            me.focusToFirstFieldinForm(form);
            window.close();
        }

    },
    onInsertFormReset: function (btn) {
        var me = this;
        var form = btn.up('form');
        var changedField = me.checkChanagesforInsertForm(form);
        if (changedField !== false) {
            //ask message nicely
            Ext.Msg.confirm("Reset Form?", "Unsaved changes found.</br>You want to reset form before save it?", function (btnn) {
                if (btnn === "yes") {
                    form.getForm().reset();
                    me.focusToFirstFieldinForm(form);
                } else {
                    changedField.focus(true, 300);
                }
            });
        } else {
            form.getForm().reset();
            me.focusToFirstFieldinForm(form);
        }
    },
    onNewRecordCreate: function (cmp, store, successMsg, processMsg, isWindow, hasGrid, grid, tree) {
        //        var form = cmp.down('form');
        var form = cmp.up('form');

        //////console.log(form);
        form = form.getForm();
        if (form.isValid()) {
            store.insert(0, form.getValues());
            this.insertRecordtoDatabase(store, cmp.up('form'), successMsg, processMsg, isWindow, hasGrid, grid, tree);
        } else {
            this.focusToFirstDirtyFieldinForm(cmp.up('form'));
        }
    },
    //single record insertion
    insertRecordtoDatabase: function (store, form, successMsg, processMsg, isWindow, hasGrid, grid, tree) {
        var me = this;
        var executingRecord = store.getModifiedRecords().length > 0 ? store.getModifiedRecords() : store.getRemovedRecords();
        var parentExcuteRecord = store.findRecord('bisId', executingRecord[0].get('asset_parent_category_id'), 0, false, true, true);
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        if (isWindow) {
            form.up("window").setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData['returnFlag'];
                var resMsg = batch.proxy.getReader().jsonData['returnMsg'];
                //////console.log(parseInt(resMsg));
                if (response === '1' || response === '100') {
                    //                    //////console.log();
                    singer.LayoutController.notify('Record Inserted.', successMsg);
                    if (isWindow) {
                        ////////console.log("reset Form ..............................");
                        form.getForm().reset();
                        //                        me.getController('CoreController').focusToFirstFieldinForm(form);
                        form.up("window").destroy();
                        store.load({
                            callback: function (records, operation, success) {
                                if (tree) {
                                    //me.getController('BuisnessStructureController').expandChangedNode(response,parentExcuteRecord)                                  
                                    //}

                                }
                            }
                        });
                    }
                    //                    me.setFakeProxytoStore(store, 'add');
                    //                    if (hasGrid) {
                    //                        me.animateGivenRow(grid, 0);
                    //                    }
                } else {
                    //////console.log('Error :', resMsg);
                    //                    Ext.getBody().unmask();
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", resMsg, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    animateGivenRow: function (grid, index) {
        Ext.fly(grid.getView().getNode(index)).highlight("#33cc00", {
            attr: 'backgroundColor',
            duration: 2000
        });
    },
    //insert form end
    //form update start
    onUpdateFormReset: function (btn) {
        var form = btn.up('form');
        var me = this;
        var changedField = this.checkRecordDataisUpdated(form);
        if (changedField !== false) {
            Ext.Msg.confirm("Reset Form?", "Record is modified.</br> You want to reset modified data before save it?", function (btnn) {
                if (btnn === "yes") {
                    form.loadRecord(form.getRecord());
                    me.focusToFirstFieldinForm(btn.up('form'));
                } else {
                    changedField.focus(false, 300);
                }
            });
        } else {
            form.loadRecord(form.getRecord());
            me.focusToFirstFieldinForm(btn.up('form'));
        }

    },
    onUpdateFormCancel: function (btn) {
        var win = btn.up('window');
        win.close();
    },
    onRecordUpdate: function (btn, store, successMsg, processMsg, isWindow, hasGrid, grid) {
        var form = btn.up('form').getForm();
        var record = form.getRecord();
        //                //console.log(record);
        ////console.log(form);
        //		//console.log(this.checkRecordDataisUpdated(btn.up('form')));
        if (form.isValid() && this.checkRecordDataisUpdated(btn.up('form'))) {
            record.set(form.getValues());
            this.updateRecordinDatabase(store, btn.up('form'), successMsg, processMsg, isWindow, hasGrid, grid, store.indexOf(record));
        }
        //        record.set(form.getValues());
        //        //console.log(store.indexOf(record));
        //        this.updateRecordinDatabase(store, btn.up('form'), successMsg, processMsg, isWindow, hasGrid, grid, grid, store.indexOf(record));
    },
    onCreditNoteRecordUpdate: function (btn, store, successMsg, processMsg, isWindow, hasGrid, grid) {
        //        var form = btn.up('form').getForm();
        form = btn.up().up().down('form');
        var record = form.getRecord();
        //console.log(record);
        if (form.isValid()) { // && this.checkRecordDataisUpdated(btn.up().up().down('form')) !== false) {
            record.set(form.getValues());
            this.updateRecordinDatabase(store, btn.up().up().down('form'), successMsg, processMsg, isWindow, hasGrid, grid, store.indexOf(record));
        }
    },
    updateRecordinDatabaseLdap: function (store, form, successMsg, processMsg, isWindow, hasGrid, grid, recordIndex) {
        //////console.log('it did', store);

        var me = this;
        store.on('beforesync', function () {
            //////console.log('before');
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        if (isWindow) {
            form.up("window").setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData['flag'];
                var resMsg = batch.proxy.getReader().jsonData['returnMsg'];
                var error = batch.proxy.getReader().jsonData['exceptionMessages'];
                ////console.log('Response data : ', response);
                if (response === '1' || response === '100') {
                    singer.LayoutController.notify('Record Updated.', 'User Data Saved Successfully...');
                    if (isWindow) {
                        form.up("window").destroy();
                        store.load();
                    }
                    if (hasGrid) {
                        me.animateGivenRow(grid, recordIndex);
                    }
                } else {
                    singer.LayoutController.createErrorPopup("Record Updating Fail.", resMsg, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    //single record update
    updateRecordinDatabase: function (store, form, successMsg, processMsg, isWindow, hasGrid, grid, recordIndex) {
        var me = this;
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        if (isWindow) {
            form.up("window").setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData['returnFlag'];
                var resMsg = batch.proxy.getReader().jsonData['returnMsg'];
                //                //////console.log('Response data : ', response);
                if (response === '1' || response === '100') {
                    singer.LayoutController.notify('Record Updated.', successMsg);
                    if (isWindow) {
                        form.up("window").destroy();
                        store.load();
                    }
                    //                    if (hasGrid) {
                    //                        me.animateGivenRow(grid, recordIndex);
                    //                    }
                } else {
                    singer.LayoutController.createErrorPopup("Record Updating Fail.", resMsg, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            //fix for Update
                            form.getForm().loadRecord(store.getAt(recordIndex));
                            //                            if (operation.success === false) {
                            //                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            //                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });

    },
    onRowUpdate: function (grid, rowIndex, successMsg, processMsg) {
        this.updateGridRow(grid.getStore(), grid, rowIndex, successMsg, processMsg);
    },
    updateGridRow: function (store, grid, rowIndex, successMsg, processMsg) {
        var me = this;
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                var response = batch.proxy.getReader().jsonData;
                if (parseInt(response) === 1) {
                    FXApp.LayoutController.notify('Record Updated.', successMsg);
                    me.animateGivenRow(grid, rowIndex);
                } else {
                    FXApp.LayoutController.createErrorPopup("Record Updating Fail.", response, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                FXApp.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    //update form ends
    //single record delete start
    onRowDelete: function (grid, rowIndex, successMsg, confirmMsg, processMsg) {
        var me = this;
        Ext.Msg.confirm("Remove From System?", confirmMsg, function (btn) {
            //            //console.log(btn);
            if (btn === "yes") {
                var record = grid.getStore().getAt(rowIndex.index);
                grid.getStore().remove(record);
                me.deleteRecordinDatabase(grid.getStore(), grid, rowIndex, successMsg, processMsg);
            }
        });
    },
    deleteRecordinDatabase: function (store, grid, rowIndex, successMsg, processMsg) {
        var me = this;
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                var response = batch.proxy.getReader().jsonData;
                ////console.log(response);
                if (response.returnFlag === "1") {
                    //                    singer.LayoutController.notify('Record Removed.', successMsg);
                    me.setFakeProxytoStore(store, 'delete');
                } else {
                    singer.LayoutController.createErrorPopup("Record Removing Fail.", response, Ext.MessageBox.WARNING);
                    ////console.log(response);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    //on row delete ends
    setFakeProxytoStore: function (store, crudState) {
        var me = this;
        var storeOrgProxy = store.getProxy();
        var storePageSize = store.pageSize;
        //after user insert record to database successfully
        if (crudState === 'add') {
            var fakeArray = this.generateStoreFakeArray(store);
            var jsonObj = {
                "success": true,
                "totalRecords": store.getTotalCount() + 1,
                "data": fakeArray
            };
            store.removeAll(false);
            store.setProxy({
                enablePaging: true,
                type: 'memory',
                data: jsonObj,
                reader: {
                    type: 'json',
                    totalProperty: 'totalRecords',
                    root: 'data',
                    successProperty: 'success'
                }
            });
            store.load({
                start: 0,
                limit: storePageSize
            });
            store.setProxy(storeOrgProxy);
            // after user delete record from database
        } else if (crudState === 'delete') {
            var limit = storePageSize - store.getCount();
            var start = ((store.currentPage - 1) * storePageSize) + store.getCount();
            var totalCount = store.getTotalCount() - 1;
            //////console.log(start, limit, totalCount);
            //////console.log((start % storePageSize));
            var fakeArray = this.generateStoreFakeArray(store);
            if (start !== totalCount) {
                store.load({
                    start: start,
                    limit: limit,
                    callback: function (records, operation, success) {
                        if (Ext.getBody().isMasked()) {
                            Ext.getBody().unmask();
                        }
                        if (operation.success === false) {
                            me.getController('ExceptionsController').openStoreReloaderWindow(store);
                        } else {
                            //////console.log(records);
                            //////console.log(fakeArray.concat(records));
                            var jsonObj = {
                                "success": true,
                                "totalRecords": totalCount,
                                "data": fakeArray.concat(records)
                            };
                            store.removeAll(false);
                            store.setProxy({
                                enablePaging: true,
                                type: 'memory',
                                data: jsonObj,
                                reader: {
                                    type: 'json',
                                    totalProperty: 'totalRecords',
                                    root: 'data',
                                    successProperty: 'success'
                                }
                            });
                            store.load({
                                start: 0,
                                limit: jsonObj.data.length
                            });
                            store.setProxy(storeOrgProxy);
                        }
                    }
                });
            } else if ((start % storePageSize) === 0 && start !== 0) {
                store.previousPage({
                    callback: function (records, operation, success) {
                        if (Ext.getBody().isMasked()) {
                            Ext.getBody().unmask();
                        }
                        if (operation.success === false) {
                            me.getController('ExceptionsController').openStoreReloaderWindow(store);
                        } else {
                            //////console.log(records);
                            //////console.log(fakeArray.concat(records));
                            var jsonObj = {
                                "success": true,
                                "totalRecords": totalCount,
                                "data": fakeArray.concat(records)
                            };
                            store.removeAll(false);
                            store.setProxy({
                                enablePaging: true,
                                type: 'memory',
                                data: jsonObj,
                                reader: {
                                    type: 'json',
                                    totalProperty: 'totalRecords',
                                    root: 'data',
                                    successProperty: 'success'
                                }
                            });
                            store.load({
                                start: 0,
                                limit: jsonObj.data.length
                            });
                            store.setProxy(storeOrgProxy);
                        }
                    }
                });
            } else {
                var jsonObj = {
                    "success": true,
                    "totalRecords": totalCount,
                    "data": fakeArray
                };
                store.removeAll(false);
                store.setProxy({
                    enablePaging: true,
                    type: 'memory',
                    data: jsonObj,
                    reader: {
                        type: 'json',
                        totalProperty: 'totalRecords',
                        root: 'data',
                        successProperty: 'success'
                    }
                });
                store.load({
                    start: 0,
                    limit: storePageSize
                });
                store.setProxy(storeOrgProxy);
            }
        }
    },
    generateStoreFakeArray: function (store) {
        var fakeArray = [];
        var i = store.getCount();
        store.each(function (record) {
            --i;
            if (i >= 0) {
                fakeArray.push(record);
            }
        });
        return fakeArray;
    },
    onDataSearch: function (btn, store, proxyPath) {
        var form = btn.up('form');
        form = form.getForm();
        var formVals = form.getValues();
        for (var key in formVals) {
            if (formVals.hasOwnProperty(key)) {
                if (formVals[key].length === 0) {
                    formVals[key] = "all";
                }
            }
        }
        var queryString = Ext.Object.toQueryString(formVals);
        store.getProxy().api.read = FXApp.AppParams.JBOSS_PATH + proxyPath + queryString;
        this.loadBindedStore(store, 0, store.pageSize, 1);
    },
    onResetReadProxy: function (store, proxyPath, comp) {
        if (comp !== false) {
            var form = comp.up('form');
            form.getForm().reset();
        }
        store.getProxy().api.read = FXApp.AppParams.JBOSS_PATH + proxyPath;
        this.loadBindedStore(store, 0, store.pageSize, 1);
    },
    onRemoveAllFilters: function (store) {
        if (store.isFiltered()) {
            store.clearFilter();
        }
    },
    onOpenViewMoreWindow: function (record, alias, title) {

        var win = Ext.widget(alias);

        win.setTitle(title);
        var form = win.down('form');
        form.getForm().loadRecord(record);
        win.show();
    },
    onCloseViewMoreWindow: function (btn) {
        var win = btn.up('window');
        win.close();
    },
    syncStore: function (store, form, successMsg, isWindow) {
        var start = 0;
        var end = 10;
        var me = this;
        store.on('beforesync', function () {
            Ext.getBody().mask('Wait Process In Progress', 'large-loading');
        });
        if (isWindow) {
            form.up("window").setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData;
                if (parseInt(response) === 1) {
                    FXApp.LayoutController.notify('Success!', successMsg);
                    if (isWindow) {
                        form.getForm().reset();
                        form.up("window").close();
                    }
                    store.setProxy({
                        enablePaging: true,
                        type: 'memory'
                    });
                    store.load({
                        start: 0,
                        limit: 10
                    });
                } else {
                    FXApp.LayoutController.createErrorPopup("Transaction Failure", response, Ext.MessageBox.WARNING);
                    store.rejectChanges();
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                FXApp.LayoutController.createErrorPopup("Transaction Fail", "Unexpected Error Occured", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    onGenerateExcelSheet: function (store, headerColumns, title, subTitles, columns, minClmnWidth, maximumSpace) {
        if (store.getTotalCount() === 0) {
            FXApp.LayoutController.createErrorPopup("Grid is Empty", "Data is not in the grid. Cannot create empty excel sheet.", Ext.MessageBox.WARNING);
        } else {
            var excelTable = Ext.get("excelGeneratorTable");
            if (excelTable !== null) {
                excelTable.remove();
            }
            excelTable = Ext.DomHelper.createDom({
                tag: "table",
                id: 'excelGeneratorTable'
            });
            Ext.getBody().appendChild(excelTable);
            excelTable = Ext.get("excelGeneratorTable");

            //assign column group for min width
            var colgroup = "<colgroup>";
            for (var x = 0; x < columns.length; x++) {
                colgroup += "<col width='" + minClmnWidth + "'>";
            }
            colgroup += "</colgroup>";

            //assigned spacing generate
            var spacing = "";
            for (var i = 0; i < maximumSpace; i++) {
                spacing += "<tr>";
                for (var x = 0; x < columns.length; x++) {
                    spacing += "<td>&nbsp;</td>";
                }
                spacing += "</tr>";
            }
            //build extra header details with current date
            var extraHeader = "";
            extraHeader += "<tr><td align='CENTER' colspan=" + columns.length + "><h1 style='text-align:center;'>" + title + "</h1></td></tr>";
            extraHeader += "<tr><td align='LEFT' colspan=" + columns.length + "><b>" + Ext.Date.format(new Date(), 'Y-m-d') + "</b></td></tr>";
            extraHeader += spacing;
            for (var i = 0; i < subTitles.length; i++) {
                extraHeader += "<tr><td align='LEFT' colspan=" + columns.length + "><b>" + subTitles[i] + "</b></td></tr>";
            }
            //columns header generate
            var header = "<tr>";
            for (var i = 0; i < headerColumns.length; i++) {
                header += "<th align='LEFT' width: " + minClmnWidth + "px'><b>" + headerColumns[i] + "</b></th>";
            }
            header += "</tr>";

            //data cells generate
            var dataColumns = "";
            store.each(function (record) {
                dataColumns += "<tr>";
                for (var i = 0; i < columns.length; i++) {
                    dataColumns += "<td align='LEFT'>" + record.get(columns[i]) + "</td>";
                }
                dataColumns += "</tr>";
            });

            //table generate
            excelTable.insertHtml('afterBegin', colgroup + extraHeader + spacing + header + dataColumns);
            var htmltable = document.getElementById('excelGeneratorTable');
            var html = htmltable.outerHTML;
            window.location.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);

            //table remove after generation
            excelTable.remove();
        }
    },
    onClearSearch: function (btn) {
        var me = btn;
        var tablePanel = me.up('gridpanel'),
            allColumns = tablePanel.columns;
        var store = tablePanel.getStore();
        for (var i = 0; i < allColumns.length; i++) {
            if (!Ext.isEmpty(allColumns[i].searchableField)) {
                var field = allColumns[i].searchableField;

                //////console.log(field.getValue());
                if (field.getValue() !== "") {
                    field.setValue("");
                }


            }
        }
        store.getProxy().api.read = this.resetProxy(this.getProxyReadURI(store)); //thisStore.getProxy().api.read;
        store.load();
    },
    getProxyReadURI: function (store) {
        switch (store.getProxy().type) {
        case "rest":
            return store.getProxy().api.read;
        case "ajax":
            return store.getProxy().url;
        case "jsonp":
            return store.getProxy().url;
        default:
            return Ext.Error.raise("Searchable column only support with rest, ajax or jsonp proxies.");
        }
    },
    setProxyReadURI: function (store, query) {
        switch (store.getProxy().type) {
        case "rest":
            store.getProxy().api.read = this.resetProxy(this.getProxyReadURI(store)) + "?" + query;
            return store;
        case "ajax":
            store.getProxy().url = this.resetProxy(this.getProxyReadURI(store)) + "?" + query;
            return store;
        case "jsonp":
            store.getProxy().url = this.resetProxy(this.getProxyReadURI(store)) + "?" + query;
            return store;
        default:
            Ext.Error.raise("Searchable column only support with rest, ajax or jsonp proxies.");
            break;
        }
    },
    resetProxy: function (proxyURI) {
        return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
    },
    onUserCreate: function (form, store, successMsg, processMsg, isWindow, hasGrid, grid) {
        var record = form.getRecord();

        if (form.isValid()) {
            store.insert(0, form.getValues());
            this.insertRecordtoDatabase(store, form, successMsg, processMsg, isWindow, hasGrid, grid);
        } else {
            this.focusToFirstDirtyFieldinForm(form);
        }

        //        if (form.isValid() && this.checkRecordDataisUpdated(form) !== false) {
        //            record.set(form.getValues());
        //            //console.log(record);
        ////            this.updateRecordinDatabase(store, form, successMsg, processMsg, isWindow, hasGrid, grid, store.indexOf(record));
        //            this.insertRecordtoDatabase(store, form, successMsg, processMsg, isWindow, hasGrid, grid);
        //        }
    },
    onUserUpdate: function (form, store, successMsg, processMsg, isWindow, hasGrid, grid) {
        ////console.log(form.isValid());
        //        var form = formm.getForm();
        var record = form.getRecord();

        if (form.isValid()) {
            record.set(form.getValues());
            ////console.log(record);
            this.updateRecordinDatabaseLdap(store, form, successMsg, processMsg, isWindow, hasGrid, grid, store.indexOf(record));
        }
    },
    /////////////////////////
    onCreditNoteUpdate: function (form, store, successMsg, processMsg, isWindow, hasGrid, grid) {
        ////console.log(form.isValid());
        //        var form = formm.getForm();
        var record = form.getRecord();

        if (form.isValid()) {
            record.set(form.getValues());
            ////console.log(record);
            this.updateRecordinDatabaseLdap(store, form, successMsg, processMsg, isWindow, hasGrid, grid, store.indexOf(record));
        }
    },
    //////////////////////
    onNewActivityRecordCreate: function (btn, store, successMsg, processMsg, isWindow, hasGrid, grid, model) {

        //        var form = cmp.down('form');
        var form = btn.up().up('form').up().up().down('form');

        ////console.log(form);
        form = form.getForm();
        if (form.isValid()) {
            store.insert(0, model);
            this.insertUsertoDatabase(store, btn, successMsg, processMsg, isWindow, hasGrid, grid);
        } else {
            //            this.focusToFirstDirtyFieldinForm(form);
            Ext.Msg.alert("Error", "Please fill the form correctly");
        }
    },
    onCreateCreditNote: function (btn, store, successMsg, processMsg, isWindow, hasGrid, grid, model) {

        //        var form = cmp.down('form');
        var form = btn.up().up().down('form');

        ////console.log(form);
        form = form.getForm();
        //        //console.log(form.up('window'));
        if (form.isValid()) {
            store.insert(0, model);
            this.insertCreditNoteIssueToDatabase(store, btn, successMsg, processMsg, isWindow, hasGrid, grid);
        } else {
            //            this.focusToFirstDirtyFieldinForm(form);
            Ext.Msg.alert("Error", "Please fill the form correctly");
        }
    },
    insertUsertoDatabase: function (store, btn, successMsg, processMsg, isWindow, hasGrid, grid) {
        var me = this;
        //        var form =
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        if (isWindow) {
            btn.up().up('form').up().up('window').setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    btn.up().up('form').up().up('window').setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData['flag'];
                var resMsg = batch.proxy.getReader().jsonData['successMessages'];
                var error = batch.proxy.getReader().jsonData['exceptionMessages'];
                //////console.log(parseInt(resMsg));
                if (response === '1' || response === '100') {
                    //                    //////console.log();
                    singer.LayoutController.notify('Record Inserted.', successMsg);
                    if (isWindow) {
                        ////////console.log("reset Form ..............................");
                        //                        form.getForm().reset();
                        //                        btn.up().up('form');
                        //                        me.getController('CoreController').focusToFirstFieldinForm(form);
                        btn.up().up('form').up().up('window').destroy();
                        //                        me.getController('UserMaintenence').openAddNew();
                        store.load({
                            callback: function (records, operation, success) {
                                if (Ext.getBody().isMasked()) {
                                    Ext.getBody().unmask();
                                }
                                if (operation.success === true) {
                                    me.getController('UserMaintenence').openAddNew();
                                    Ext.getCmp("UserGroupsGrid").getStore().removeAll();

                                    //                                    Ext.getCmp('allUserGroups').getStore().load();
                                }
                            }
                        });

                        //                        Ext.getStore('userIds').load();
                    }
                    //                    me.setFakeProxytoStore(store, 'add');
                    //                    if (hasGrid) {
                    //                        me.animateGivenRow(grid, 0);
                    //                    }
                } else {
                    //////console.log('Error :', resMsg);
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", error, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    btn.up().up('form').up().up('window').setDisabled(false);
                }
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    BusinessStructureUserAdd: function (cmp, store, successMsg, processMsg, isWindow, hasGrid, grid) {
        //        var form = cmp.down('form');
        //        var form = cmp.up('form');
        //    //console.log(cmp.up().up('form').up('window'));
        this.insertBusinessUserToDB(store, cmp.up().up('form'), successMsg, processMsg, isWindow, hasGrid, grid);
        //////console.log(form);
        //        form = form.getForm();
        //        if (form.isValid()) {
        //            store.insert(0, form.getValues());
        //            
        //        } else {
        //            this.focusToFirstDirtyFieldinForm(cmp.up('form'));
        //        }
    },
    insertBusinessUserToDB: function (store, form, successMsg, processMsg, isWindow, hasGrid, grid) {
        ////console.log('comming here')
        var me = this;
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        if (isWindow) {
            form.up("window").setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData['returnFlag'];
                var resMsg = batch.proxy.getReader().jsonData['returnMsg'];
                //                //console.log(parseInt(resMsg));
                if (response === '1' || response === '100') {
                    //                    //////console.log();
                    singer.LayoutController.notify('Record Inserted.', successMsg);
                    if (isWindow) {
                        ////console.log("reset Form ..............................");
                        form.getForm().reset();
                        //                        me.getController('CoreController').focusToFirstFieldinForm(form);
                        form.up("window").destroy();
                        store.load();
                    }
                    //                    me.setFakeProxytoStore(store, 'add');
                    //                    if (hasGrid) {
                    //                        me.animateGivenRow(grid, 0);
                    //                    }
                } else {
                    //////console.log('Error :', resMsg);
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", resMsg, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    form.up("window").setDisabled(false);
                }
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    
    
    insertCreditNoteIssueToDatabase: function (store, btn, successMsg, processMsg, isWindow, hasGrid, grid) {
        var me = this;
        //        var form =
        store.on('beforesync', function () {
            Ext.getBody().mask(processMsg, 'large-loading');
        });
        if (isWindow) {
            btn.up().up('window').setDisabled(true);
        }
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    btn.up().up('window').setDisabled(false);
                }
                var response = batch.proxy.getReader().jsonData['returnFlag'];
                var resMsg = batch.proxy.getReader().jsonData['returnMsg'];
                //                var error = batch.proxy.getReader().jsonData['exceptionMessages'];
                //////console.log(parseInt(resMsg));
                if (response === '1' || response === '100') {
                    //                    //////console.log();
                    singer.LayoutController.notify('Record Inserted.', successMsg);
                    if (isWindow) {
                        ////////console.log("reset Form ..............................");
                        //                        form.getForm().reset();
                        //                        btn.up().up('form');
                        //                        me.getController('CoreController').focusToFirstFieldinForm(form);
                        btn.up().up('window').destroy();

                        store.load();
                    }
                    //                    me.setFakeProxytoStore(store, 'add');
                    //                    if (hasGrid) {
                    //                        me.animateGivenRow(grid, 0);
                    //                    }
                } else {
                    //////console.log('Error :', resMsg);
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", resMsg, Ext.MessageBox.WARNING);
                    store.load({
                        callback: function (records, operation, success) {
                            if (Ext.getBody().isMasked()) {
                                Ext.getBody().unmask();
                            }
                            if (operation.success === false) {
                                me.getController('ExceptionsController').openStoreReloaderWindow(store);
                            }
                        }
                    });
                }
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                if (isWindow) {
                    btn.up().up('window').setDisabled(false);
                }
                singer.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
    
    viewPrintReadyReport: function (reportURI, title) {
        Ext.getBody().mask("Generating " + title, 'loading');
        var me = this;

        showReportWindow();
//        console.log('reportURI', reportURI);
//        var reportURIhtml = replaceWithNewFormat("html");
//        console.log('reportURIhtml', reportURIhtml);

        //var catchRes = me.wo_direct_ajax_print(reportURI,title);
        //console.log('catchRes', catchRes);

        function showReportWindow() {
            var win = Ext.create("Ext.window.Window", {
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                modal: true,
                constrain: true,
                loadMask: true,
                title: (Ext.isEmpty(title) ? '' : title),
                width: "90%",
                height: "90%",
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        maxHeight: 40,
                        id: 'reportWindowHeader',
                        layout: {
                            type: 'hbox',
                            pack: 'end'
                        },
                        defaults: {
                            margin: 10
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'PRINT',
                                width: 100,
                                handler: function (btn) {
                                    var ajaxreq;
                                    function makeHttpObject() {
                                        try {
                                            return new XMLHttpRequest();
                                        }
                                        catch (error) {
                                        }
                                        try {
                                            return new ActiveXObject("Msxml2.XMLHTTP");
                                        }
                                        catch (error) {
                                        }
                                        try {
                                            return new ActiveXObject("Microsoft.XMLHTTP");
                                        }
                                        catch (error) {
                                        }

                                        throw new Error("Could not create HTTP request object.");
                                    }
                                    var request = makeHttpObject();
                                    request.open("GET", singer.AppParams.REPO + reportURI, true);
                                    request.send(null);
                                    request.onreadystatechange = function () {
                                        if (request.readyState == 4){
                                        ajaxreq = request.responseText;
                                        var myWindow = window.open('', '', 'width=700,height=700');
                                        console.log('myWindow',myWindow);
                                        myWindow.document.write('<title>' + 'Singer' + '</title>');
                                        myWindow.document.write( ajaxreq );
                                        myWindow.print();
                                        

                                        
                               /*         myWindow.document.write('<html><head>');
                                        myWindow.document.write('<title>' + 'Title' + '</title>');
                                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                                        myWindow.document.write('</head><body>');
                                        myWindow.document.write('<div>' + ajaxreq + '</div>');
                                        myWindow.document.write('</body></html>');
                                      //  console.log("AJAXREQ >>>",ajaxreq)
                                        if(ajaxreq !== null){
                                            
                                        setInterval(function () {
                                            myWindow.print();
                                            }, 2000);
                                            
                                        } */
                                        
                                    }
                                    };
                                }
                            },
//                            , '->',
                            {
                                xtype: 'label',
                                text: 'Download Report as : ',
                                margin: '15px 10px 10px 0px'
                            },
                            {
                                xtype: 'button',
                                text: '.Doc',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("doc"), '_blank');
                                    docWindow.focus();
                                }
                            },
                            {
                                xtype: 'button',
                                text: '.PDF',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("pdf"), '_blank');
                                    docWindow.focus();
                                }
                            },
                            {
                                xtype: 'button',
                                text: '.XLS',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("xls"), '_blank');
                                    docWindow.focus();
                                }
                            },
//                            {
//                                xtype: 'button',
//                                text: '.Plain XLS',
//                                width: 100,
//                                id : 'btnPlainXls',
//                                handler: function () {
//                                    var docWindow = window.open(singer.AppParams.REPO + plainExcelUrl, '_blank');
//                                    console.log(singer.AppParams.REPO);
//                                    docWindow.focus();
//                            }
//                            }
                        ]
                    },
                    {
                        xtype: "form",
                        overflowY: 'auto',
                        bodyPadding: '0',
                        flex: 10,
                        html: '<div id="BirtReportsContainer" style="width: 100%; overflow: auto;"></div>',
                        buttons: [
                            '->',
                            {
                                xtype: 'button',
                                text: 'Close',
                                handler: function () {
                                    win.close();
                                }
                            }
                        ]
                    }
                ]
            });
            win.show();
            
         
        }

        function replaceWithNewFormat(newFormat) {
            reportURI = reportURI.replace("html", newFormat);
            reportURI = reportURI.replace("pdf", newFormat);
            reportURI = reportURI.replace("doc", newFormat);
            reportURI = reportURI.replace("xls", newFormat);
            return reportURI;
        }

        $('#BirtReportsContainer').load(singer.AppParams.REPO + reportURI, function (response, status, xhr) {
            Ext.getBody().unmask();
            if (status === "error") {
                var msg = "Sorry there was an error getting report ! ";
                $("#BirtReportsContainer").html(msg + xhr.status + " " + xhr.statusText);
            }
        });
    },
    
     viewPrintReadyReport_Excep: function (reportURI, title) {
        Ext.getBody().mask("Generating " + title, 'loading');
        var me = this;

        showReportWindow();

        function showReportWindow() {
            var win = Ext.create("Ext.window.Window", {
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                modal: true,
                constrain: true,
                loadMask: true,
                title: (Ext.isEmpty(title) ? '' : title),
                width: "90%",
                height: "90%",
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        maxHeight: 40,
                        id: 'reportWindowHeader',
                        layout: {
                            type: 'hbox',
                            pack: 'end'
                        },
                        defaults: {
                            margin: 10
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'PRINT',
                                width: 100,
                                handler: function (btn) {
                                    var ajaxreq;
                                    function makeHttpObject() {
                                        try {
                                            return new XMLHttpRequest();
                                        }
                                        catch (error) {
                                        }
                                        try {
                                            return new ActiveXObject("Msxml2.XMLHTTP");
                                        }
                                        catch (error) {
                                        }
                                        try {
                                            return new ActiveXObject("Microsoft.XMLHTTP");
                                        }
                                        catch (error) {
                                        }

                                        throw new Error("Could not create HTTP request object.");
                                    }
                                    
                                    var request = makeHttpObject();
                                    request.open("GET", singer.AppParams.REPO + reportURI, true);
                                    //request.open("GET", singer.AppParams.REPO2 + excelURI, true);
                                   
                                    
                                    request.send(null);
                                    request.onreadystatechange = function () {
                                        if (request.readyState == 4){
                                        ajaxreq = request.responseText;
                                        var myWindow = window.open('', '', 'width=700,height=700');
                                        console.log('myWindow',myWindow);
                                        myWindow.document.write('<html><head>');
                                        myWindow.document.write('<title>' + 'Title' + '</title>');
                                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                                        myWindow.document.write('</head><body>');
                                        myWindow.document.write('<div>' + ajaxreq + '</div>');
                                        myWindow.document.write('</body></html>');
                                        setInterval(function () {
                                            myWindow.print();
                                            }, 5000);
                                    }
                                    };
                                }
                            },
//                            , '->',
                            {
                                xtype: 'label',
                                text: 'Download Report as : ',
                                margin: '15px 10px 10px 0px'
                            },
                            {
                                xtype: 'button',
                                text: '.Doc',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("doc"), '_blank');
                                    docWindow.focus();
                                }
                            },
                            {
                                xtype: 'button',
                                text: '.PDF',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("pdf"), '_blank');
                                    docWindow.focus();
                                }
                            },
                            {
                                xtype: 'button',
                                text: '.XLS',
                                width: 100,
                                handler: function () {
                                    var url =  localStorage.getItem('myReptExcelURI');
                                    var docWindow = window.open(url, '_blank');
                                    docWindow.focus();
                                }
                            },
//                            {
//                                xtype: 'button',
//                                text: '.Plain XLS',
//                                width: 100,
//                                id : 'btnPlainXls',
//                                handler: function () {
//                                    var docWindow = window.open(singer.AppParams.REPO + plainExcelUrl, '_blank');
//                                    console.log(singer.AppParams.REPO);
//                                    docWindow.focus();
//                            }
//                            }
                        ]
                    },
                    {
                        xtype: "form",
                        overflowY: 'auto',
                        bodyPadding: '0',
                        flex: 10,
                        html: '<div id="BirtReportsContainer" style="width: 100%; overflow: auto;"></div>',
                        buttons: [
                            '->',
                            {
                                xtype: 'button',
                                text: 'Close',
                                handler: function () {
                                    win.close();
                                }
                            }
                        ]
                    }
                ]
            });
            win.show();
            
         
        }

        function replaceWithNewFormat(newFormat) {
            reportURI = reportURI.replace("html", newFormat);
            reportURI = reportURI.replace("pdf", newFormat);
            reportURI = reportURI.replace("doc", newFormat);
            reportURI = reportURI.replace("xls", newFormat);
            return reportURI;
        }

        $('#BirtReportsContainer').load(singer.AppParams.REPO + reportURI, function (response, status, xhr) {
            Ext.getBody().unmask();
            if (status === "error") {
                var msg = "Sorry there was an error getting report ! ";
                $("#BirtReportsContainer").html(msg + xhr.status + " " + xhr.statusText);
            }
        });
        
        
    },
    
    
    
    viewSalesReport: function (reportURI, title) {
        Ext.getBody().mask("Generating " + title, 'loading');
        var me = this;

        showReportWindow();

        function showReportWindow() {
            var win = Ext.create("Ext.window.Window", {
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                modal: true,
                constrain: true,
                loadMask: true,
                title: (Ext.isEmpty(title) ? '' : title),
                width: "90%",
                height: "90%",
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        maxHeight: 40,
                        id: 'reportWindowHeader',
                        layout: {
                            type: 'hbox',
                            pack: 'end'
                        },
                        defaults: {
                            margin: 10
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'PRINT',
                                width: 100,
                                handler: function (btn) {
                                    var ajaxreq;
                                    function makeHttpObject() {
                                        try {
                                            return new XMLHttpRequest();
                                        }
                                        catch (error) {
                                        }
                                        try {
                                            return new ActiveXObject("Msxml2.XMLHTTP");
                                        }
                                        catch (error) {
                                        }
                                        try {
                                            return new ActiveXObject("Microsoft.XMLHTTP");
                                        }
                                        catch (error) {
                                        }

                                        throw new Error("Could not create HTTP request object.");
                                    }
                                    
                                    var request = makeHttpObject();
                                    request.open("GET", singer.AppParams.REPO + reportURI, true);
                                    //request.open("GET", singer.AppParams.REPO2 + excelURI, true);
                                   request.timeout = 3600000;
                                    
                                    request.send(null);
                                    request.onreadystatechange = function () {
                                        if (request.readyState == 4){
                                        ajaxreq = request.responseText;
                                        var myWindow = window.open('', '', 'width=700,height=700');
                                        console.log('myWindow',myWindow);
                                        myWindow.document.write('<html><head>');
                                        myWindow.document.write('<title>' + 'Title' + '</title>');
                                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                                        myWindow.document.write('</head><body>');
                                        myWindow.document.write('<div>' + ajaxreq + '</div>');
                                        myWindow.document.write('</body></html>');
                                        setInterval(function () {
                                            myWindow.print();
                                            }, 5000);
                                    }
                                    };
                                }
                            },
//                            , '->',
                            {
                                xtype: 'label',
                                text: 'Download Report as : ',
                                margin: '15px 10px 10px 0px'
                            },
                            {
                                xtype: 'button',
                                text: '.Doc',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("doc"), '_blank');
                                    docWindow.focus();
                                }
                            },
                            {
                                xtype: 'button',
                                text: '.PDF',
                                width: 100,
                                handler: function () {
                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("pdf"), '_blank');
                                    docWindow.focus();
                                }
                            },
//                            {
//                                xtype: 'button',
//                                text: 'Distributor Stock Issues Report To DSR',
//                                width: 250,
//                                handler: function () {
//                                    var url =  localStorage.getItem('salseExcelRep1');
//                                    var docWindow = window.open(url, '_blank');
//                                    docWindow.focus();
//                                }
//                            },
                            {
                                xtype: 'button',
                                text: 'XLS',
                                width: 100,
                                handler: function () {
                                    var url =  localStorage.getItem('salseExcelRep2');
                                    var docWindow = window.open(url, '_blank');
                                    docWindow.focus();
                                }
                            },
//                            {
//                                xtype: 'button',
//                                text: '.Plain XLS',
//                                width: 100,
//                                id : 'btnPlainXls',
//                                handler: function () {
//                                    var docWindow = window.open(singer.AppParams.REPO + plainExcelUrl, '_blank');
//                                    console.log(singer.AppParams.REPO);
//                                    docWindow.focus();
//                            }
//                            }
                        ]
                    },
                    {
                        xtype: "form",
                        overflowY: 'auto',
                        bodyPadding: '0',
                        flex: 10,
                        html: '<div id="BirtReportsContainer" style="width: 100%; overflow: auto;"></div>',
                        buttons: [
                            '->',
                            {
                                xtype: 'button',
                                text: 'Close',
                                handler: function () {
                                    win.close();
                                }
                            }
                        ]
                    }
                ]
            });
            win.show();
            
         
        }

        function replaceWithNewFormat(newFormat) {
            reportURI = reportURI.replace("html", newFormat);
            reportURI = reportURI.replace("pdf", newFormat);
            reportURI = reportURI.replace("doc", newFormat);
            reportURI = reportURI.replace("xls", newFormat);
            return reportURI;
        }

        $('#BirtReportsContainer').load(singer.AppParams.REPO + reportURI, function (response, status, xhr) {
            Ext.getBody().unmask();
            if (status === "error") {
                var msg = "Sorry there was an error getting report ! ";
                $("#BirtReportsContainer").html(msg + xhr.status + " " + xhr.statusText);
            }
        });
        
        
    },
    
    
    
    setPlainExcelReportParam: function (reportURI) {
    
        plainExcelUrl = reportURI;


//        Ext.getBody().mask("Generating " + title, 'loading');
//        var me = this;

//        console.log('reportURI', reportURI);
//        var reportURIhtml = replaceWithNewFormat("html");
//        console.log('reportURIhtml', reportURIhtml);

        //var catchRes = me.wo_direct_ajax_print(reportURI,title);
        //console.log('catchRes', catchRes);

//        var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("xls"), '_blank');
//        docWindow.focus();
//
//        function replaceWithNewFormat(newFormat) {
//            reportURI = reportURI.replace("html", newFormat);
//            reportURI = reportURI.replace("pdf", newFormat);
//            reportURI = reportURI.replace("doc", newFormat);
//            reportURI = reportURI.replace("xls", newFormat);
//            return reportURI;
//        }
//
//
//
//        $('#BirtReportsContainer').load(singer.AppParams.REPO + reportURI, function (response, status, xhr) {
//            Ext.getBody().unmask();
//            if (status === "error") {
//                var msg = "Sorry there was an error getting report ! ";
//                $("#BirtReportsContainer").html(msg + xhr.status + " " + xhr.statusText);
//            }
//        });
    },
//    viewPrintReadyReport: function (reportURI, title) {
//        console.log(reportURI);
//        Ext.getBody().mask("Generating " + title, 'loading');
//        var me = this;
//
//        showReportWindow();
//
//        function showReportWindow() {
//            var win = Ext.create("Ext.window.Window", {
//                layout: {
//                    type: 'vbox',
//                    align: 'stretch'
//                },
//                modal: true,
//                constrain: true,
//                loadMask: true,
//                title: (Ext.isEmpty(title) ? '' : title),
//                width: "90%",
//                height: "90%",
//                items: [
//                    {
//                        xtype: 'container',
//                        flex: 1,
//                        maxHeight: 40,
//                        layout: {
//                            type: 'hbox',
//                            pack: 'end'
//                        },
//                        defaults: {
//                            margin: 10
//                        },
//                        items: [
//                            {
//                                xtype: 'button',
//                                text: 'Print',
//                                width: 100,
//                                handler: function (btn) {
//                                    var ajaxreq;
//                                    function makeHttpObject() {
//                                        try {
//                                            return new XMLHttpRequest();
//                                        }
//                                        catch (error) {
//                                        }
//                                        try {
//                                            return new ActiveXObject("Msxml2.XMLHTTP");
//                                        }
//                                        catch (error) {
//                                        }
//                                        try {
//                                            return new ActiveXObject("Microsoft.XMLHTTP");
//                                        }
//                                        catch (error) {
//                                        }
//
//                                        throw new Error("Could not create HTTP request object.");
//                                    }
//                                    var request = makeHttpObject();
//                                    request.open("GET", singer.AppParams.REPO + reportURI, true);
//                                    request.send(null);
//                                    request.onreadystatechange = function () {
//                                        if (request.readyState == 4){
//                                            ajaxreq = request.responseText;
//                                        console.log('catchRes');
//                                        console.log('Print button clicked', btn);
//                                        var targetElement = btn.up('window').down('panel');
//                                        var myWindow = window.open('', '', 'width=700,height=700');
//                                        myWindow.document.write('<html><head>');
//                                        myWindow.document.write('<title>' + 'Title' + '</title>');
//                                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
//                                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
//                                        myWindow.document.write('</head><body>');
//                                        myWindow.document.write('<div>' + ajaxreq + '</div>');
//                                        myWindow.document.write('</body></html>');
//                                        myWindow.print();
//                                        console.log(ajaxreq);
//                                    }
//                                    };
//                                }
//                            },
//                            {
//                                xtype: 'label',
//                                text: 'Download Report as : ',
//                                margin: '15px 10px 10px 0px'
//                            },
//                            {
//                                xtype: 'button',
//                                text: '.Doc',
//                                width: 100,
//                                handler: function () {
//                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("doc"), '_blank');
//                                    docWindow.focus();
//                                }
//                            },
//                            {
//                                xtype: 'button',
//                                text: '.PDF',
//                                width: 100,
//                                handler: function () {
//                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("pdf"), '_blank');
//                                    docWindow.focus();
//                                }
//                            },
//                            {
//                                xtype: 'button',
//                                text: '.XLS',
//                                width: 100,
//                                handler: function () {
//                                    var docWindow = window.open(singer.AppParams.REPO + replaceWithNewFormat("xls"), '_blank');
//                                    docWindow.focus();
//                                }
//                            }
//                        ]
//                    },
//                    {
//                        xtype: "form",
//                        overflowY: 'auto',
//                        bodyPadding: '0',
//                        flex: 10,
//                        html: '<iframe src="'+singer.AppParams.REPO + reportURI+'" id="BirtReportsContainer" style="width: 100%;  height: 100%; overflow: auto;"></iframe >',
//                        buttons: [
//                            '->',
//                            {
//                                xtype: 'button',
//                                text: 'Close',
//                                handler: function () {
//                                    win.close();
//                                }
//                            }
//                        ]
//                    }
//                ]
//            });
//            win.show();
//        }
//
//        function replaceWithNewFormat(newFormat) {
//            reportURI = reportURI.replace("html", newFormat);
//            reportURI = reportURI.replace("pdf", newFormat);
//            reportURI = reportURI.replace("doc", newFormat);
//            reportURI = reportURI.replace("xls", newFormat);
//            return reportURI;
//        }
//        Ext.getBody().unmask();
////        $('#BirtReportsContainer').load(singer.AppParams.REPO + reportURI, function (response, status, xhr) {
////            Ext.getBody().unmask();
////            if (status === "error") {
////                var msg = "Sorry there was an error getting report ! ";
////                $("#BirtReportsContainer").html(msg + xhr.status + " " + xhr.statusText);
////            }
////        });
//    },
    
    
    
    dash_board_interface_view: function (reportURI, title) {
        Ext.getBody().mask("Generating " + title, 'loading');
        var me = this;

        showReportWindow();

        function showReportWindow() {
            var win = Ext.create("Ext.window.Window", {
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                modal: true,
                constrain: true,
                loadMask: true,
                title: (Ext.isEmpty(title) ? '' : title),
                width: "90%",
                height: "90%",
                items: [
                    {
                        xtype: "form",
                        overflowY: 'auto',
                        bodyPadding: '0',
                        flex: 10,
                        html: '<div id="BirtReportsContainer" style="width: 100%; overflow: auto;"></div>',
                        buttons: [
                            '->',
                            {
                                xtype: 'button',
                                text: 'Close',
                                handler: function () {
                                    win.close();
                                }
                            }
                        ]
                    }
                ]
            });
            win.show();
        }

        $('#BirtReportsContainer').load(singer.AppParams.REPO + reportURI, function (response, status, xhr) {
            Ext.getBody().unmask();
            if (status === "error") {
                var msg = "Sorry there was an error getting report ! ";
                $("#BirtReportsContainer").html(msg + xhr.status + " " + xhr.statusText);
            }
        });
    },
    displayHistoryReport: function (reportURI) {
        var docWindow = window.open(singer.AppParams.REPO + reportURI, '_blank');
//        console.log(singer.AppParams.REPO);
        docWindow.focus();
    },
    displayPlainXlsReport: function (reportURI) {
        var docWindow = window.open(singer.AppParams.REPO + reportURI, '_blank');
//        console.log(singer.AppParams.REPO);
        docWindow.focus();
    }
});