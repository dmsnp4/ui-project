Ext.define('singer.controller.DeviceIssueController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.deviceissuecontroller',
    //    requirs:['NewUserController'],
    controllers: ['CoreController', 'Main'],
    stores: [
        'DeviceIssue',
        'LoggedInUserData',
        'WinGrid',
        'DeviceIssueIme',
        'DeviceIssueSummeryStore'
    ],
    views: [
        'DeviceIssueModule.device_issue.DeviceIssueGrid',
        'DeviceIssueModule.device_issue.NewDeviceIssue',
        'DeviceIssueModule.device_issue.DeviceIssueView',
        'DeviceIssueModule.device_issue.ImeiGridIssue',
    ],
    refs: [{
            ref: 'DeviceIssueGrid',
            selector: 'deviceissuegrid'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        },
        {
            ref: 'NewDeviceIssue',
            selector: 'newdeviceissue'
        },
        {
            ref: 'ImeiGridIssue',
            selector: 'imeigridissue'
        },
        {
            ref: 'DeviceIssueView',
            selector: 'deviceissueview'
        }
    ],
    init: function () {
//        console.log('DeviceIssueController');
        var me = this;
        me.control({
            'deviceissuegrid': {
                added: this.initializeGrid
            },
            'deviceissuegrid ': {
                issueEdit: this.openUpdateForm
            },
            'deviceissuegrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'deviceissuegrid button[action=open_window]': {
                click: this.openAddNew
            },
            '  deviceissuegrid ': {
                issueView: this.moreViewOpen
            },
            'newdeviceissue button[action=cancel]': {
                click: this.insertFormCancelnew
            },
            'newdeviceissue button[action=save]': {
                click: this.updataRecord
            },
            'newdeviceissue button[action=create]': {
                click: this.insertRecord
            },
            ' deviceissuegrid': {
                itemdblclick: this.moreViewOpen
            },
            'newdeviceissue button[action=scanimei]': {
                click: this.openImeiWindow
            },
            'newdeviceissue': {
                remDevIsuItem: this.onremDevIsuItem
            },
            'deviceissueview button[action=cancel]': {
                click: this.insertFormCancel
            },
            'deviceissueview button[action=report]': {
                click: this.DSRReport
            },
            'imeigridissue button[action=cancel]': {
                click: this.insertFormCancelImei
            },
            'imeigridissue button[action=okay]': {
                click: this.showGrid
            },
            'imeigridissue button[action=add]': {
                click: this.addtoGrid
            },
            'imeigridissue panel field': {
                specialkey: this.isEnterKey
            },
            'newdeviceissue radiogroup[action=clickRdio]': {
                change: this.changeRdioGrp2
            },
            '  deviceissuegrid  ': {
                dsrReport: this.dsrReportPrint
            },
        });
        //this.changeRdioGrp2();
    },
    dsrReportPrint: function (view, record) {
        console.log('DSR Repot Print .............', record);

        var me = this;
        var summStore = Ext.getStore('DeviceIssueSummeryStore');
        var nxtIssueNo = record.data.issueNo;
        var issueDate = record.data.issueDate;
        var price = record.data.price;
        var frmStatus = record.data.status;

        console.log(nxtIssueNo, ' - RRRRRRRRR: ', issueDate, ':', price, ':', frmStatus);

        summStore.load({
            // sync : false,
            params: {issueNo: nxtIssueNo},
            callback: function (records, operation, success) {
                //console.log('RRRRRRRRR: ', records);
                //me.WaitForFunction(summStore, issueDate, price, nxtIssueNo, frmStatus); 
                // me.generateDSRReport(summStore, issueDate, price, nxtIssueNo, frmStatus);
            },
        });

        var detailStore = Ext.getStore('DeviceIssueIme');
        detailStore.load({
            scope: this,
            params: {
                issueNo: nxtIssueNo
            },
            callback: function (records, operation, success) {

            }
        });


        Ext.Msg.confirm("Are you sure?", "Are you sure you want to print the report?", function (answer) {
            if (answer === "yes") {
                me.generateDSRReport(detailStore, issueDate, price.toString(), nxtIssueNo, frmStatus);
            } else {
                return;
            }
        }, this);

    },
    isEnterKey: function (field, e) {
        var me = this;
        if (e.getKey() === e.ENTER) {
            me.addtoGrid();
        }
    },
    changeRdioGrp2: function (btn, newValue, oldValue, eOpts) {

        var val = newValue.type;
        //console.log(newValue);
        var loginData = this.getLoggedInUserDataStore();
        var uid = loginData.data.getAt(0).get('extraParams');
        var store = /*Ext.getCmp('dsr').getStore();*/ Ext.getStore('radioStr');
        store.removeAll();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownList?bisId=' + uid + '&bisType=' + val,
            method: 'GET',
            timeout: 60000,
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            // params: JSON.stringify(user.data),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                console.log('responseData', responseData);
                console.log('loginData', loginData);
                store.add(responseData.data);
                //                //console.log(store);
                //                if (responseData.data[0].returnFlag === '1') {
                ////                    Ext.getCmp('prdct').setValue(responseData.data[0].product);
                ////                    Ext.getCmp('brnd').setValue(responseData.data[0].brand);
                ////                    Ext.getCmp('model').setValue(responseData.data[0].modleNo);
                //                } else {
                //                    singer.LayoutController.createErrorPopup("Not Registered", "This Customer is not registered Previously. Fill the other datails of the customer", responseData.returnMsg, Ext.MessageBox.WARNING);
                //                }
                //console.log('OK');

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    initializeGrid: function (grid) {
        ////console.log('loading device issue');
        var userStore = this.getDeviceIssueStore();

        this.getController('CoreController').loadBindedStore(userStore);
        var loginStore = this.getLoggedInUserDataStore();
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        console.log("grid loading...", userStore);
        ////console.log('aaaaaaaaaaaa',user);
        //        this.getController('Main').createDsrStore(user);
        //        this.getController('Main').createSubdealerStore(user);
        //        var test = this.getController('Main').getStore('dsrStore');
        ////console.log(Ext.data.StoreManager.lookup('dsrStore'));
        //        singer.controller.Main.createDsrStore(user);
        //        singer.controller.Main.createSubdealerStore(user);
    },
    updataRecord: function (btn) {
        //        var assignGrid = Ext.getCmp('igrid');
        ////        //console.log('test');
        //        var groups = assignGrid.getStore();
        //        //console.log("beep beep", groups);
        var form = btn.up('form');
        //        //        //console.log(form);
        var assignStore = Ext.getCmp('igrid').getStore();
        //        //        //console.log(assignStore);
        var answer = [];
        //        assignStore.data.each(function (item) {
        //temp.push({
        //                'imeiNo':item.data.imeiNo,
        //                'modleNo':item.data.modleNo,
        //                'margin':item.data.margin,
        //                'salesPrice':item.data.salesPrice
        //            });
        ////            temp.push(item.data);
        //            var record = form.getRecord();
        //            ////console.log(record);
        //            record.set('deviceImei', temp);
        //        });
        //        this.getController('CoreController').onRecordUpdate(btn, this.getDeviceIssueStore(), 'Issue Data Updated.', 'Updating Issue', true, true, this.getDeviceIssueGrid());
        //        var str = this.getNewDeviceIssue().down('grid').getStore();
        //        ////console.log(str);
        //        str.removeAll();
        //        str.sync();


        assignStore.data.each(function (item) {
            answer.push({
                'imeiNo': item.data.imeiNo,
                'modleNo': item.data.modleNo,
                'margin': item.data.margin,
                'salesPrice': item.data.salesPrice
            });
        });
        var me = this;
        var user = Ext.create('singer.model.DeviceIssue',
                form.getValues()
                );
        user.set('dSRId', '');
        var tm = form.getValues().dSRId;
        var ty = form.getValues().type;
        if (ty === '1') {
            user.set('dSRId', tm[0]);
        } else if (ty === '2') {
            user.set('dSRId', tm[1]);
        }
        user.set("deviceImei", answer);
        if (assignStore.data.length !== 0) {
            //        //console.log(user);

            Ext.Msg.confirm("Update Data", "Are you sure you want to change data?", function (button) {
                if (button == 'yes') {
                    var store = me.getDeviceIssueStore();
                    var loginData = me.getLoggedInUserDataStore();
                    var sys = singer.AppParams.SYSTEM_CODE;
                    Ext.Ajax.request({
                        url: singer.AppParams.JBOSS_PATH + 'DeviceIssues/editDeviceIssue',
                        method: 'POST',
                        timeout: 60000,
                        headers: {
                            userId: loginData.data.getAt(0).get('userId'),
                            room: loginData.data.getAt(0).get('room'),
                            department: loginData.data.getAt(0).get('department'),
                            branch: loginData.data.getAt(0).get('branch'),
                            countryCode: loginData.data.getAt(0).get('countryCode'),
                            division: loginData.data.getAt(0).get('division'),
                            organization: loginData.data.getAt(0).get('organization'),
                            system: sys,
                            'Content-Type': 'application/json'
                        },
                        params: JSON.stringify(user.data),
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            var error = responseData.returnMsg;
                            //                 Ext.Msg.hide();
                            //                    var responseData = Ext.decode(response.responseText);
                            ////console.log(responseData);
                            if (responseData.returnFlag === '1') {
                                //                    btn.up().up('form').up('window').destroy();
                                btn.up('window').close();
                                store.load();
                            } else if (responseData.returnFlag === '1000000') {
                                singer.LayoutController.createErrorPopup("Record Insertion Fail.", "Cannot connect to the database. Please contact the system Admin", Ext.MessageBox.WARNING);
                            } else {
                                singer.LayoutController.createErrorPopup("Record Insertion Fail.", error, Ext.MessageBox.WARNING);
                            }
                            //                    
                            //                form.destroy();
                            //assignStore.load();
                            //=============
                        },
                        failure: function () {
                            Ext.Msg.hide();
                            Ext.Msg.alert("Error in connection", "Please try again later");
                        }
                    });
                } else {
                    return false;
                }
            });
        } else {
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }



        //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Issue Created.', 'Creating New Issue', true, true, this.getDeviceIssueGrid(), user);
        //       // this.getController('Main').initializeAppReq();
        var grid = this.getDeviceIssueGrid();
        //grid.bindStore(store);

    },
    insertRecord: function (btn) {
        console.log('insertRecord');
        var me = this;
        var form;
        form = btn.up().up();
        ////console.log(form);
        Ext.getCmp('newdeviceissue-create').setDisabled(true);
        Ext.getBody().mask('Please wait...', 'loading');
        var assignStore = Ext.getCmp('igrid').getStore();
        var store = this.getDeviceIssueStore();
        var answer = [];
        assignStore.data.each(function (item) {

            var imei = item.data.imeiNo;
            var out = answer.indexOf(imei);

            console.log("imei ", imei);
            console.log("index ", out);

            if (out == -1) {
                answer.push({
                    'imeiNo': item.data.imeiNo,
                    'modleNo': item.data.modleNo,
                    'margin': item.data.margin,
                    'salesPrice': item.data.salesPrice
                });
            }


        });
        var user = Ext.create('singer.model.DeviceIssue',
                form.getValues()
                );
        user.set('dSRId', '');
        var tm = form.getValues().dSRId;
        var ty = form.getValues().type;
        if (ty === '1') {
            user.set('dSRId', tm[0]);
        } else if (ty === '2') {
            user.set('dSRId', tm[1]);
        }
        user.set("deviceImei", answer);
        //  console.log(assignStore.getAt(0));

        if (assignStore.data.length !== 0) {
            var loginData = this.getLoggedInUserDataStore();
            var sys = singer.AppParams.SYSTEM_CODE;

            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'DeviceIssues/addDeviceIssue',
                method: 'PUT',
                timeout: 60000,
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(user.data),
                // async: false,
                success: function (response) {

                    Ext.getCmp('newdeviceissue-create').setDisabled(false);
                    Ext.getBody().unmask();
                    var responseData = Ext.decode(response.responseText);
                    var error = responseData.returnMsg;
                    //                 Ext.Msg.hide();
                    //                    var responseData = Ext.decode(response.responseText);
                    console.log('Response..........', responseData.returnRefNo);
                    if (responseData.returnFlag == 1) {
                        //                    btn.up().up('form').up('window').destroy();

                        console.log("REFRESH..........: ", form.getValues().status);
                        var issueDate = form.getValues().issueDate;
                        var price = form.getValues().price;
                        var frmStatus = form.getValues().status;
                        var nxtIssueNo = responseData.returnRefNo;

//                        var summStore = Ext.getStore('DeviceIssueSummeryStore');
//                        summStore.load({
//                            params: {issueNo: nxtIssueNo},
//                            async: false
//                        });


//                        
//                     if(frmStatus != '1'){      
//                         
//                         
//                        Ext.defer(function () {
//                              console.log("Defer Function is calld ...........: ");
//                             me.generateDSRReport(assignStore, issueDate, price, nxtIssueNo, frmStatus);
//
//                          }, 2900, this);
//                         
//                      
//                   }


                        assignStore.load();
                        btn.up('window').close();
                        var grid = me.getDeviceIssueGrid();
                        store.load();
                        grid.bindStore(store);
                    } else if (responseData.returnFlag == 1000000) {
                        singer.LayoutController.createErrorPopup("Record Insertion Fail.", "Cannot connect to the database. Please contact the system Admin", Ext.MessageBox.WARNING);
                    } else {
                        singer.LayoutController.createErrorPopup("Record Insertion Fail.", error, Ext.MessageBox.WARNING);
                    }

                },
                failure: function () {
                    Ext.getCmp('newdeviceissue-create').setDisabled(false);
                    Ext.getBody().unmask();
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
            var store = this.getDeviceIssueStore();
            //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Issue Created.', 'Creating New Issue', true, true, this.getDeviceIssueGrid(), user);
            //       // this.getController('Main').initializeAppReq();

        } else {
            Ext.Msg.alert('Creating Error.', 'Please select at least one IMEI number.');
            Ext.getCmp('newdeviceissue-create').setDisabled(false);
            Ext.getBody().unmask();
        }
    },
    moreViewOpen: function (view, record) {

        var me = this;
        var deviceStore = Ext.create('Ext.data.Store', {
            model: 'singer.model.DeviceIssueIme'
        });
        this.getController('CoreController').onOpenViewMoreWindow(record, 'deviceissueview', "View");
        var detailStore = Ext.getStore('DeviceIssueIme');
        detailStore.load({
            scope: this,
            params: {
                issueNo: record.get('issueNo')
            },
            callback: function (records, operation, success) {

                // the operation object
                // contains all of the details of the load operation
                //console.log(Ext.getCmp('ivgrid'), " => ", records);
                Ext.getCmp('ivgrid').bindStore(this);
            }
        });
        //////////////////////Load Summery report Data///////////////////////
        localStorage.setItem('dsrReportStatus', record.get('status'));
        var summStore = Ext.getStore('DeviceIssueSummeryStore');
        summStore.load({
            params: {issueNo: record.get('issueNo')},
            async: false
        });
        //        var gird = Ext.getCmp('ivgrid');
        //        var issueNo = record.get('issueNo');
        //        var loginData = this.getLoggedInUserDataStore();
        //        Ext.Ajax.request({
        //                url: singer.AppParams.JBOSS_PATH + 'DeviceIssues/getDeviceIssueDetail?issueNo='+issueNo ,
        //                method: 'GET',
        //                headers: {
        //                    userId: loginData.data.getAt(0).get('userId'),
        //                    room: loginData.data.getAt(0).get('room'),
        //                    department: loginData.data.getAt(0).get('department'),
        //                    branch: loginData.data.getAt(0).get('branch'),
        //                    countryCode: loginData.data.getAt(0).get('countryCode'),
        //                    division: loginData.data.getAt(0).get('division'),
        //                    organization: loginData.data.getAt(0).get('organization'),
        //                    'Content-Type': 'application/json'
        //                },
        //                
        //                success: function (response) {
        //                    ////console.log('comming to here');
        //                    var responseData = Ext.decode(response.responseText);
        //                    ////console.log(responseData.data[0].deviceImei);
        //                    
        //                    var data = responseData.data[0].deviceImei;
        //                    deviceStore.add(data);
        //                    gird.bindStore(deviceStore);
        //                },
        //                failure: function () {
        //                    Ext.Msg.hide();
        //                    Ext.Msg.alert("Error in connection", "Please try again later");
        //                }
        //            });

        //         var data = store.data.getAt(0).get('deviceImei');
        //         //console.log(data); 
    },
    openUpdateForm: function (view, record) {
        var me = this;
        console.log(record);
        this.getController('CoreController').openUpdateWindow(record, 'newdeviceissue', "Edit");
        var form = this.getNewDeviceIssue().down('form').getForm();
        //this.focusToFirstFieldinForm(form);
        var ino = record.get('issueNo');
        //        var deviceImeiStore = this.getDeviceIssueStore();
        var devIme = record.get('deviceImei');
        var dt = new Date(record.get('issueDate'));
        me.getNewDeviceIssue().down('datefield').setValue(dt);
        ////console.log("ddddddd", devIme);
        var DeviceIssueImeStore = Ext.create('Ext.data.Store', {
            model: 'singer.model.DeviceIssueIme'
        });
        var abcd = Ext.getCmp('rad').getValue();
        this.changeRdioGrp2("", abcd);
        var detailStore = me.getNewDeviceIssue().down('grid').getStore();
        detailStore.load({
            scope: this,
            params: {
                issueNo: record.get('issueNo')
            },
            callback: function (detailsrecords, operation, success) {
                me.getNewDeviceIssue().down('grid').bindStore(detailStore);
                //console.log(me.getNewDeviceIssue().down('datefield'), " => eeeeeeeeeeee: ", dt);
            }
        });
        //        this.DeviceIssueImeStore.add(devIme);

        //        Ext.getCmp('deviceImei').bindStore(DeviceIssueImeStore.load());
        //        Ext.getCmp("DeviceIssueGrid").bindStore(DeviceIssueImeStore);

        //        form.findField('userId').setDisabled(true);
        //        form.findField('issueNo').hide();

    },
    insertFormCancelnew: function (btn) {
        var str = this.getNewDeviceIssue().down('grid').getStore();
        ////console.log('str');
        //		str.removeAll();
        //		str.sync();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancelImei: function (btn) {
        var str = this.getImeiGridIssue().down('grid').getStore();
        ////console.log('str');
        //str.removeAll();
        //str.sync();
        this.getController('CoreController').onInsertFormCancel(btn);
        //Ext.getCmp('priceBox').setValue('');
    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    openAddNew: function () {
        Ext.getStore('radioStr').load();
        this.getController('CoreController').openInsertWindow('newdeviceissue');
        var form = this.getNewDeviceIssue().down('form').getForm();
        var loggedUser = this.getLoggedInUserDataStore();
        var uid = loggedUser.data.getAt(0).get('extraParams');
        Ext.getCmp('igrid').getStore().removeAll();
        ////console.log(loggedUser);

        form.findField('distributerID').setValue(uid);
        var abcd = Ext.getCmp('rad').getValue();
        this.changeRdioGrp2("", abcd);
    },
    openImeiWindow: function (view, record) {
        //        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //        var store = this.getMinorDefectsStore();
        //        this.getController('CoreController').loadBindedStore(store);
        var str = this.getNewDeviceIssue().down('grid').getStore();



        //str.removeAll();
        var grid = Ext.widget('imeigridissue');
        var winGrid = Ext.getCmp('imgrid');
        winGrid.bindStore(str);
        ////console.log(winGrid.getStore());

        var win = Ext.create("Ext.window.Window", {
            width: 450,
            height: 300,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            resizable: false,
            id: 'defWindow',
            items: [grid]
        });
        win.setTitle("IMEIs");
        //        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //////console.log(singer.AppParams.TEMP);
        win.show();
        Ext.getCmp('imtextf').focus('', 10);

        Ext.getCmp('noimei').setText(str.getCount());
    },
    showGrid: function (btn) {
        //console.log('this');
        var mainGrid = Ext.getCmp('igrid');
        var winGrid = Ext.getCmp('imgrid');
        //        var mainData= mainGrid.getStore();
        var winData = winGrid.getStore();
        //        mainData.loadData(winData);
        mainGrid.bindStore(winData);
        var store = this.getDeviceIssueImeStore();
        var grid = this.getDeviceIssueGrid();
        //store.load();
        //        grid.bindStore(store);
        ////console.log(store);
        //        this.gridStore.addListener('load', function(records, operation, success) {
        //   if(success) {
        //      var obj = new Object();
        //      obj.success = true;
        //      obj.results = records;
        //      grid.store2.loadData(obj);
        //   }
        //});
        //        grid.store2.loadData(store1.data)

        this.getController('CoreController').onInsertFormCancel(btn);
//        Ext.getCmp('newdeviceissue-create').hide();
        var totalPrice = winData.sum('salesPrice');
        totalPrice = Ext.util.Format.number(totalPrice, '0000000.00');
        Ext.getCmp('priceBox').setValue(totalPrice);
    },
    addtoGrid: function () {
        console.log('addtoGrid');
        var form = this.getImeiGridIssue().down('form').getForm().findField("enterimei").getValue();
        var str = this.getImeiGridIssue().down('grid').getStore();
        //console.log(str);
        ////console.log('form'+form);
        //         str.add({imei: form});

        var loginData = this.getLoggedInUserDataStore();
        var uid = loginData.data.getAt(0).get('extraParams');
        console.log('loginData', loginData, uid);
        Ext.Msg.wait('Please wait', 'Finding Model No...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/getImeiDetailsForIssue?imeiNo=' + form + '',
            method: 'GET',
            success: function (response) {

                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                console.log(responseData);

                var totRow = responseData.totalRecords;

                if (totRow == 1000000) {
                    singer.LayoutController.createErrorPopup("Database connection failed", "Cannot connect to the database. Please contact the system Admin", Ext.MessageBox.WARNING);
                } else if (totRow == 50) {
                    Ext.Msg.alert('Issue Error.', 'The IMEI number already returned from your location.');
                } else {

                    if (uid === responseData.data[0].bisId) {

                        if (responseData.totalRecords === 1) {
                            var temp = responseData.data[0].modleNo;
                            ////console.log('temp'+temp);

                            var chk = str.findExact('imeiNo', form);
                            //console.log(chk);

                            if (chk > -1) {
                                Ext.Msg.alert("Record Insertion Fail", "Already Issued", function () {
                                    Ext.getCmp('imtextf').focus('', 10);
                                });
                            } else {
                                str.add(responseData.data);
                                var cnt = str.getCount() - 1;
                                var mrg = Ext.getCmp('marginBox').getValue();

                                Ext.getCmp('noimei').setText(str.getCount());

                                mrg = parseInt(mrg, 10);
                                var record = str.getAt(cnt);
                                var salP = record.data.salesPrice;
                                var netP = salP + mrg / 100 * salP;
                                netP = Ext.util.Format.number(netP, '0000000.00');
                                record.set('margin', mrg);
                                record.set('salesPrice', netP);
                                Ext.getCmp('imtextf').focus('', 10);
                                ////console.log(record.get('salesPrice'));
                            }


                        } else {
                            Ext.Msg.alert("Invalid IMEI Number", 'Invalid IMEI number.', function () {
                                Ext.getCmp('imtextf').focus('', 10);
                                console.log('1');

                            });
                        }

                    } else if (uid !== responseData.data[0].bisId) {
                        Ext.Msg.alert('Location Error.', 'The IMEI number not belongs to your location.');
                    }

                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Invalid IMEI Number", 'Invalid IMEI number.', function () {
                    Ext.getCmp('imtextf').focus('', 10);
                });
            }
        });
        var emb = this.getImeiGridIssue().down('form').getForm().findField("enterimei");
        emb.setValue("");
        Ext.getCmp('imtextf').focus('', 10);
        //        Ext.getCmp('imtextf').clearInvalid();
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onremDevIsuItem: function () {
        var mGrid = Ext.getCmp('igrid');
        var mData = mGrid.getStore();
        var totalPrice = mData.sum('salesPrice');
        totalPrice = Ext.util.Format.number(totalPrice, '0000000.00');
        Ext.getCmp('priceBox').setValue(totalPrice);
    },
    /////Generating report
    generateDSRReport: function (assetStore, issueDateStr, issuePrice, issueNo, status) {//issueNo
        var me = this;
        var sumStore = Ext.getStore('DeviceIssueSummeryStore');
        var sumData = sumStore.data;
        var dsrName = sumStore.data.items[0].data.dsrName;
        var distributorName = sumStore.data.items[0].data.distributorName;
        var loginUser = Ext.getStore('LoggedInUserData').getAt(0).data.userId;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var h = (today.getHours() < 10 ? '0' : '') + today.getHours();
        var m = (today.getMinutes() < 10 ? '0' : '') + today.getMinutes();
        var curTime = h + ':' + m;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        var today = yyyy + '-' + mm + '-' + dd + ' ' + curTime;
        var myWindow = window.open('', '', 'width=800,height=800');
        myWindow.document.write('<html><head>');
        var htmlHead = '<style>' +
                'table th{background-color: #EFEFEF;}' +
                'table, th, td {' +
                ' border: 1px solid #949494;' +
                'border-collapse: collapse;' +
                'font-size:0.9em;' +
                '}' +
                'th, td {' +
                ' padding: 8px;' +
                '}' +
                '</style>';
        myWindow.document.write(htmlHead);
        var header = '<div style="margin-right: 18px;" align="right"><table style="border:none;"><tr><td style="border:none; align:right;padding: 4px 0;"><b>Printed By:</td><td style="border:none; align:left;padding: 4px 0;"></b> ' + loginUser + ' </td></tr>' + '<tr><td style="border:none; padding: 4px 0; align:right;padding: 4px 0;"><b>Printed Date:</b></td><td style="border:none;align:left;padding: 4px 0;"> ' + today + '</td></tr></table></div>';
        myWindow.document.write(header);
        myWindow.document.write('<div style="font-weight: bold;" align="center"><u>DSR Inventory Acceptance Form</u></div>');

        myWindow.document.write('<div>');
        var issueNoLbl = '<div style="margin-top: 4%;margin-left: 14px;" align="left"><table style="border:none;"><tr><td style="border:none; padding: 4px 0; align:right;padding: 4px 0;"><b>Issue Note No : </b></td><td style="border:none;align:left;padding: 4px 0;"> ' + issueNo + '</td></tr></table></div>';
        myWindow.document.write(issueNoLbl);
        var dsrNameLbl = '<div style="margin-left: 14px;" align="left"><table style="border:none;"><tr><td style="border:none; padding: 4px 0; align:right;padding: 4px 0;"><b>DSR : </b></td><td style="border:none;align:left;padding: 4px 0;"> ' + dsrName + '</td></tr></table></div>';
        myWindow.document.write(dsrNameLbl);
        var distributorNameLbl = '<div style="margin-left: 14px;" align="left"><table style="border:none;"><tr><td style="border:none; padding: 4px 0; align:right;padding: 4px 0;"><b>Distributor : </b></td><td style="border:none;align:left;padding: 4px 0;"> ' + distributorName + '</td></tr></table></div>';
        myWindow.document.write(distributorNameLbl);
        var issueDate = '<div style="margin-left: 14px;" align="left"><table style="border:none;"><tr><td style="border:none; padding: 4px 0; align:right;padding: 4px 0;"><b>Issue Date : </b></td><td style="border:none;align:left;padding: 4px 0;"> ' + issueDateStr + '</td></tr></table></div>';
        myWindow.document.write(issueDate);
        myWindow.document.write('</div>');

        myWindow.document.write('</head><body style="font-size:0.85em;font-family: Helvetica Neue,Helvetica,Arial,sans-serif; ">');
        var htmlTable = '<table style="width:100%;">' +
                '<tr>' +
                '<th style="text-align: center;">Model No</th>	' +
                // '<th style="text-align: center;">Model</th>	' +
                '<th style="text-align: center;">Brand</th>	' +
                '<th style="text-align: center;">Serial No</th>' +
                // '<th style="text-align: center;">Margin</th>' +
                '<th style="text-align: center;">Sales Price(LKR)</th>' +
                '</tr>';
        for (var i = 0; i < assetStore.getCount(); i++) {

            var assetData = assetStore.getAt(i);
            htmlTable += '<tr>';
            if (assetData.data.modleNo == null || assetData.data.modleNo == 'null') {
                htmlTable += '<td style="text-align: center;"> - </td>';
            } else {
                if (htmlTable.indexOf('<td style="text-align: left;">' + assetData.data.modleNo + '</td>') == -1) {
                    htmlTable += '<td style="text-align: left;">' + assetData.data.modleDesc + '</td>';
                } else {
                    htmlTable += '<td style="text-align: center;"></td>';
                }
            }

//                if (assetData.data.modleDesc == null || assetData.data.modleDesc == 'null') {
//                    htmlTable += '<td style="text-align: center;"> - </td>';
//                } else {
//                    htmlTable += '<td style="text-align: left;">' + assetData.data.modleDesc + '</td>';
//                }

            if (assetData.data.brand == null || assetData.data.brand == 'null') {
                htmlTable += '<td style="text-align: center;"> - </td>';
            } else {
                //    if (htmlTable.indexOf('<td style="text-align: left;">' + assetData.data.brand + '</td>') == -1) {
                htmlTable += '<td style="text-align: left;">' + assetData.data.brand + '</td>';
//                } else {
//                    htmlTable += '<td style="text-align: left;"></td>';
//                }
            }



            if (assetData.data.imeiNo == null || assetData.data.imeiNo == 'null') {
                htmlTable += '<td style="text-align: center;"> - </td>';
            } else {
                htmlTable += '<td style="text-align: left;">' + assetData.data.imeiNo + '</td>';
            }




//            if (assetStore.getAt(i).data.margin == null || assetStore.getAt(i).data.margin == 'null') {
//                htmlTable += '<td style="text-align: center;"> - </td>';
//            } else {
//                htmlTable += '<td style="text-align: left;">' + assetStore.getAt(i).data.margin + '</td>';
//            }

            if (assetData.data.salesPrice == null || assetData.data.salesPrice == 'null') {
                htmlTable += '<td style="text-align: center;"> - </td>';
            } else {
                htmlTable += '<td style="text-align: right;">' + me.generateCurrencyCode((assetData.data.salesPrice).toString()) + '</td>';
            }

            htmlTable += '</tr>';
        }

        htmlTable += '</table>';
        myWindow.document.write('<div style="margin-top: 35px;">' + htmlTable + '</div>');
        var statusMsg = "Pending";
        if (status == '1') {
            statusMsg = 'Pending';
        } else if (status == '2') {
            statusMsg = 'Complete';
        } else if (status == '3') {
            statusMsg = 'Accepted';
        }

        var summeryData = '<table style="margin-top:2%; margin-left:4%; width:76%; border:none;">' +
                '<tr>' +
                '</tr>' +
                '<tr>' +
                '<td style="text-align: left;border:none;"><b>Model</b></td>' +
                '<td style="text-align: center;border:none;"><b>QTY</b></td>' +
                '<td style="text-align: right;border:none;"><b>Unit Price(LKR)</b></td>' +
                '<td style="text-align: right;border:none;"><b>Value(LKR)</b></td>' +
                '</tr>';
        //     '<tr>';

        var totalQTY = 0;
        for (var i = 0; i < sumStore.getCount(); i++) {
            totalQTY += parseInt(sumData.items[i].data.modelCnt);
            summeryData += '<tr>' +
                    '<td style="text-align: left;border:none;">' + sumData.items[i].data.modelDescription + '</td>' +
                    '<td style="text-align: center;border:none;">' + sumData.items[i].data.modelCnt + '</td>' +
                    '<td style="text-align: right;border:none;">' + me.generateCurrencyCode(sumData.items[i].data.price) + '</td>' +
                    '<td style="text-align: right;border:none;">' + me.generateCurrencyCode(sumData.items[i].data.totValue) + '</td>' +
                    '</tr>';
        }
        summeryData += '<tr><td style="padding: 10px;border:none;"></td> </tr> ' +
                '</table>';
        myWindow.document.write(summeryData);
        var footerData = '<table style="margin-top:2%; margin-left:34%; width:49%; border:none;">' +
                '<tr>' +
                '</tr>' +
                '<tr>' +
                '<tr>' +
                '<td style="text-align: right;border:none;"><b>Total QTY:</b></td>' +
                '<td style="text-align: left;border:none;"><b>' + totalQTY + '</b></td>' +
                '<td style="text-align: right;border:none;"><b>Net Price (LKR):</b></td>' +
                '<td style="text-align: left;border:none;"><b>' + me.generateCurrencyCode(issuePrice) + '</b></td>' +
                '</tr>' +
                '<tr>' +
                '<tr><td style="padding: 10px;border:none;"></td> </tr> ' +
                '</table>';
        myWindow.document.write(footerData);
        myWindow.document.write('<div style="margin-bottom: 65px;">I declare the above mentioned goods are received in good condition.</div>');
        var desig1 = '<table style="margin-top:2%; margin-left:2%; width:90%; border:none;">' +
                '<tr>' +
                '<td style="text-align: left;border:none;"><b>............................................</b></td>' +
                '<td style="text-align: center;border:none;"><b>............................................</b></td>' +
                '<td style="text-align: center;border:none;"><b>............................................</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td style="text-align: left;border:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp; Issueing Officer</b></td>' +
                '<td style="text-align: center;border:none;"><b>Signature</b></td>' +
                '<td style="text-align: center;border:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date</b></td>' +
                '</tr>' +
                '</table>';
        myWindow.document.write(desig1);
        var desig2 = '<table style="margin-top:10%; margin-left:2%; width:90%; border:none;">' +
                '<tr>' +
                '<td style="text-align: left;border:none;"><b>............................................</b></td>' +
                '<td style="text-align: left;border:none;"><b>............................................</b></td>' +
                '<td style="text-align: center;border:none;"><b>............................................</b></td>' +
                '<td style="text-align: center;border:none;"><b>............................................</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td style="text-align: left;border:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DSR ID No</b></td>' +
                '<td style="text-align: left;border:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name</b></td>' +
                '<td style="text-align: center;border:none;"><b>Signature</b></td>' +
                '<td style="text-align: center;border:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date</b></td>' +
                '</tr>' +
                '</table>';
        myWindow.document.write(desig2);
        myWindow.document.write('</body></html>');
        myWindow.print();
    },
    DSRReport: function (btn, record) {
        var me = this;
        var status = localStorage.getItem('dsrReportStatus');
        if (status == 3 || status == 2) {
            var detailStore = Ext.getStore('DeviceIssueIme');
            var issueDateStr = String(Ext.getCmp('deviseIssueDate').getValue());
            if (issueDateStr.indexOf('>') > 0) {
                issueDateStr = issueDateStr.substring(33, 43);
            }

            var issuePrice = Ext.getCmp('deviseIssuenetPrice').getValue();
            var issueNo = detailStore.data.items[0].store.data.items[0].raw.issueNo;

            me.generateDSRReport(detailStore, issueDateStr, issuePrice, issueNo, status);
        } else {
            Ext.Msg.alert("Cannot Produce Report", "Please Accept the Device Issue to Generate the Report.");
        }

    },
    generateCurrencyCode: function (price) {

        var fixedPrice;
        fixedPrice = price.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (price.indexOf('.') === -1) {
            fixedPrice = fixedPrice + '.00';
        } else {
            price = price.split('.')[1];
            if (price.length === 1) {
                fixedPrice = fixedPrice + '0';
            }
        }
        return fixedPrice;
    }


});