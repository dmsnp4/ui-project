

Ext.define('singer.controller.ExchangeReferenceController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.exchngeref',
    controllers: ['CoreController'],
    stores: [
        'ExchangeReferences',
        'ApproveDeviceExchangeStore', 'EXGwrkOrderNoStore'
    ],
    views: [
        'ServiceModule.ExchangeRefference.ApproveDeviceExchange',
        'ServiceModule.ExchangeRefference.ExchangeReferenceGrid',
        'ServiceModule.ExchangeRefference.ViewExchange',
        'ServiceModule.ExchangeRefference.ExchangeReferenceForm',
        'ServiceModule.WorkOrderMaintenace.SelectWorkOrderForm'
    ],
    refs: [
        {
            ref: 'ApproveDeviceExchange',
            selector: 'ApproveDeviceExchange'
        },
        {
            ref: 'ExchangeReferenceGrid',
            selector: 'exchangeReferenceGrid'
        },
        {
            ref: 'ExchangeReferenceForm',
            selector: 'exchangeReferenceForm'
        },
        {
            ref: 'SelectWorkOrderForm',
            selector: 'selectWorkOrderForm'
        },
        {
            ref: 'ViewExchange',
            selector: 'exchangeView'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'exchangeReferenceGrid': {
                added: this.initializeGrid
            },
            'ApproveDeviceExchange': {
                added: this.initializeApproveGrid
            },
            'ApproveDeviceExchange  ': {
                onApprove: this.onApproveExchange
            },
            'ApproveDeviceExchange   ': {
                onReject: this.onRejectExchange
            },
            'exchangeReferenceGrid  ': {
                onView: this.onView
            },
            ' exchangeReferenceGrid  ': {
                itemdblclick: this.onView
            },
            'exchangeReferenceGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'selectWorkOrderForm button[action=clearSearch]': {
                click: this.onClearSearch
            },
            ' exchangeReferenceGrid button[action=add_new]': {
                click: this.openAddNewForm
            },
            'exchangeReferenceGrid ': {
                onEdit: this.openEditForm
            },
            ' exchangeView button[action=cancel]': {
                click: this.closeWindow
            },
            ' exchangeReferenceGrid': {
                ondelete: this.openDeleteForm
            },
            'exchangeReferenceForm button[action=create]': {
                click: this.onCreate
            },
            'exchangeReferenceForm button[action=save]': {
                click: this.onSave
            },
            'exchangeReferenceForm button[action=cancel]': {
                click: this.closeWindow
            },
            'exchangeReferenceForm': {
                wo_change: this.onWOComboChange
            },
            '  exchangeReferenceGrid  ': {
                on_exchange_print: this.on_exchange_print
            },
            'exchangeReferenceForm button[action=select_Work_Order]': {
                click: this.openSelectWorkOrderWindow
            },
            'selectWorkOrderForm': {
                selectWrkOrderForExchangeAction: this.selectWrkOrderForExchangeAction
            }
        });
    },
    selectWrkOrderForExchangeAction: function (grid, record) {
        var wo = record.get('workOrderNo');
        var imeiNo = record.get('imeiNo');
        var customerName = record.get('customerName');
        var customerNic = record.get('customerNic');
        var product = record.get('product');
        var brand = record.get('brand');
        var modleDesc = record.get('modleDesc');
        var dateOfSale = record.get('dateOfSale');
        var warrantyStatusMsg = record.get('warrantyStatusMsg');

        Ext.getCmp("exchangeRefWo").setValue(wo);
        Ext.getCmp("exchangeRefImei").setValue(imeiNo);
        Ext.getCmp("exchangeRefCustomerName").setValue(customerName);
        Ext.getCmp("exchangeRefCustomerNic").setValue(customerNic);
        Ext.getCmp("exchangeRefProduct").setValue(product);
        Ext.getCmp("exchangeRefBrand").setValue(brand);
        Ext.getCmp("exchangeRefModel").setValue(modleDesc);
        Ext.getCmp("exchangeRefDateOfSale").setValue(dateOfSale);
        Ext.getCmp("exchangeRefWarrantyDesc").setValue(warrantyStatusMsg);

        var cancelFrm = grid.up('window');
        cancelFrm.close();
    },
    on_exchange_print: function (grid, record) {
        var reffrence_no = record.data.exchangeReferenceNo;
        console.log(record);
        //if(record.data.st)
        if(record.data.status === 3){
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DeviceExchange.rptdesign&ExchangeRffrn=" + reffrence_no, "Report of Exchange reffrence");
        }
        
        else
            Ext.Msg.alert("Error","Record Not Approved!");
    },
    onWOComboChange: function (combo, newValue) {

        var selectedRec = combo.findRecordByValue(newValue);
        var form = this.getExchangeReferenceForm().down("form").getForm();
        if (selectedRec !== false) {

            form.findField("imeiNo").setValue(selectedRec.get("imeiNo"));
            form.findField("customerName").setValue(selectedRec.get("customerName"));
            form.findField("customerNIC").setValue(selectedRec.get("customerNic"));
            form.findField("product").setValue(selectedRec.get("product"));
            form.findField("brand").setValue(selectedRec.get("brand"));
            form.findField("modelDesc").setValue(selectedRec.get("modleDesc"));
            form.findField("warrantyRegDate").setValue(selectedRec.get("dateOfSale"));
            form.findField("warrantyDesc").setValue(selectedRec.get("warrantyStatusMsg"));
        } else {

            form.findField("imeiNo").setValue("");
            form.findField("customerName").setValue("");
            form.findField("customerNIC").setValue("");
            form.findField("product").setValue("");
            form.findField("brand").setValue("");
            form.findField("modelDesc").setValue("");
            form.findField("warrantyRegDate").setValue("");
            form.findField("warrantyDesc").setValue("");
        }
    },
    openAddNewForm: function (btn) {
        this.getController('CoreController').openInsertWindow('exchangeReferenceForm');
        var form = this.getExchangeReferenceForm().down("form").getForm();
        form.findField("imeiNo").setReadOnly(true);
        form.findField("customerName").setReadOnly(true);
        form.findField("customerNIC").setReadOnly(true);
        form.findField("product").setReadOnly(true);
        form.findField("brand").setReadOnly(true);
        form.findField("modelDesc").setReadOnly(true);
        form.findField("warrantyRegDate").setReadOnly(true);
        form.findField("warrantyDesc").setReadOnly(true);
    },
    openEditForm: function (grid, record) {
        this.getController('CoreController').openUpdateWindow(record, 'exchangeReferenceForm', 'Modify Details of - ' + record.get('exchangeReferenceNo'));
        var form = this.getExchangeReferenceForm().down("form").getForm();
        form.findField("exchangeReferenceNo").setVisible(true);
        form.findField("bisId").setVisible(false);
        form.findField("bisId").allowBlank = true;
        form.findField("remarks").setVisible(false);
        form.findField("wrtPeriodFlag").setVisible(false);
        form.findField("wrtImeiFlag").setVisible(false);
        form.findField("wrtDamageFlag").setVisible(false);
        form.findField("unAuthRepairFlag").setVisible(false);
        form.findField("waterDamage").setVisible(false);
        form.findField("reprodicbleFlag").setVisible(false);
        form.findField("imeiNo").setReadOnly(true);
        form.findField("customerName").setReadOnly(true);
        form.findField("customerNIC").setReadOnly(true);
        form.findField("product").setReadOnly(true);
        form.findField("brand").setReadOnly(true);
        form.findField("modelDesc").setReadOnly(true);
        form.findField("warrantyRegDate").setReadOnly(true);
        form.findField("warrantyDesc").setReadOnly(true);

        Ext.getCmp('ext-bisId').setValue(bisId);

        Ext.getCmp("exchangeRefSelectWoBtn").hide();
    },
    openDeleteForm: function (grid, record) {
        var me = this;
        var win = Ext.create("Ext.window.Window", {
            title: "Cancel Exchange - " + record.get("exchangeReferenceNo") + " ?",
            constrain: true,
            modal: true,
            resizable: false,
            width: 300,
            height: 180,
            items: [
                {
                    xtype: "form",
                    items: [
                        {
                            anchor: '100%',
                            labelAlign: 'top',
                            msgTarget: 'bottom',
                            margin: 10,
                            width: 270,
                            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                            allowBlank: false,
                            xtype: 'textareafield',
                            grow: true,
                            fieldLabel: '<b>Enter Reason to Cancel the Exchange :</b>',
                            name: 'cancelRemarks'
                        }
                    ],
                    buttons: [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            formBind: true,
                            handler: function (btn) {
                                record.set("remarks", btn.up("form").getForm().findField("cancelRemarks").getValue());
                                me.onDelete(record, win);
                            }
                        },
                        '->',
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            handler: function () {
                                win.close();
                            }
                        }
                    ]
                }
            ]
        });
        win.show();
    },
    onCreate: function (btn) {
        var createBtn = Ext.getCmp('exchangeReferenceForm-create');
        var form = btn.up("form").getForm();
        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        Ext.getCmp('ext-bisId').setValue(bisId);
//        console.log(form.findField("wrtPeriodFlag").getValue());
//        console.log(form.findField("wrtImeiFlag").getValue());
//        console.log(form.findField("wrtDamageFlag").getValue());
//        console.log(form.findField("unAuthRepairFlag").getValue());
//        console.log(form.findField("waterDamage").getValue());
//        console.log(form.findField("reprodicbleFlag").getValue());
        if (
                form.findField("wrtPeriodFlag").getValue() &&
                form.findField("wrtImeiFlag").getValue() &&
                form.findField("wrtDamageFlag").getValue() &&
                form.findField("unAuthRepairFlag").getValue() &&
                form.findField("waterDamage").getValue() &&
                form.findField("reprodicbleFlag").getValue()
                ) {
            createBtn.setDisabled(true);
            this.submitDataToServer('DeviceExchanges/addDeviceExchange', 'PUT', form.getValues(), "Creating New Exchange Reference", "New Reference Created", btn.up("window"), true);
            createBtn.setDisabled(false);
        } else {
            singer.LayoutController.createErrorPopup("No options selected", "Please select all conditions to continue...", Ext.MessageBox.WARNING);
        }
    },
    onSave: function (btn) {
        var form = btn.up("form").getForm();
        var rec = form.getRecord();
        rec.set(form.getValues());
        this.submitDataToServer('DeviceExchanges/editDeviceReturn', 'POST', rec.getData(), "Modifyng Exchange Reference", "Exchange Reference Modified", btn.up("window"));
    },
    onDelete: function (record, win) {
        this.submitDataToServer('DeviceExchanges/deleteDeviceExchange', 'POST', record.getData(), "Cancelling Exchange Reference", "Exchange Reference Cancelled", win);
    },
    submitDataToServer: function (submitUri, httpType, dataObj, loadingMsg, successMsg, win, shwPrintWin) {
        Ext.getBody().mask(loadingMsg, 'loading');
        var me = this;
        var store = this.getExchangeReferencesStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + submitUri,
            method: httpType,
            headers: me.getController("CoreController").appendLDAPHeadersToRequest(),
            params: Ext.encode(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    win.close();
                    singer.LayoutController.notify("Operation Success", successMsg);
                    if (shwPrintWin) {
                       // me.showPrintWindow(dataObj, responseData.returnRefNo);
                    }
                } else {
                    singer.LayoutController.createErrorPopup("Error Occured", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
                store.load();
            },
            failure: function () {
                Ext.getBody().unmask();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    showPrintWindow: function (values, returnRefNo) {
        var me = this;
        var prompt = Ext.Msg.confirm('Print Exchange Reference Details', 'Exchange Reference No is ' + returnRefNo + ". Print Exchange Reference No?", function (btn) {
            if (btn === 'yes') {
                me.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DeviceExchange.rptdesign&ExchangeRffrn=" + returnRefNo, 'Report of ' + returnRefNo + " - Exchange Reference");
            }
        });
    },
    closeWindow: function (btn) {
        var window = btn.up('window');
        window.close();
    },
    initializeGrid: function (grid) {
        var store = this.getExchangeReferencesStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    initializeApproveGrid: function (grid) {
        var store = this.getApproveDeviceExchangeStoreStore();

        this.getController('CoreController').loadBindedStore(store);
    },
    onView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'exchangeView', record.get('workOrderNo') + " - Exchange Reference Details");
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    addNewExchangeRefferenceNo: function (record) {
        //this.getController('CoreController').onOpenViewMoreWindow(record, 'exchangeView', ' Details :'+ record.get('workOrderNo'));
        var win = Ext.widget('newExchangeRefference');
        win.show();
    },
    onApproveExchange: function (grid, record) {
        console.log('Approve Exchange:: ', record);

        var me = this;
        var store = this.getApproveDeviceExchangeStoreStore();

        var prompt = Ext.Msg.confirm('Confirm Approval', "Are you sure you want to approve? ", function (btn) {
            if (btn === 'yes') {
                Ext.getBody().mask('Loading', 'loading');
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/approveRejectExchange?refNo=' + record.data.exchangeReferenceNo + '&status=3', //approved
                    method: 'GET',
                    success: function (response) {
                        Ext.getBody().unmask();
                        var responseData = Ext.decode(response.responseText);
                        if (responseData === 1) {
                            singer.LayoutController.createErrorPopup("Approved", 'Exchange Approved', Ext.MessageBox.Success);
                        } else {
                            singer.LayoutController.createErrorPopup("Error Occured", 'System Error', Ext.MessageBox.WARNING);
                        }
                        store.load();
                    },
                    failure: function () {
                        Ext.getBody().unmask();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
        });



    },
    onRejectExchange: function (grid, record) {
        console.log('Reject Exchange:: ', record);

        var me = this;
        var store = this.getApproveDeviceExchangeStoreStore();

        var prompt = Ext.Msg.confirm('Confirm Reject', "Are you sure you want to Reject? ", function (btn) {
            if (btn === 'yes') {
                Ext.getBody().mask('Loading', 'loading');
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/approveRejectExchange?refNo=' + record.data.exchangeReferenceNo + '&status=4', //approved
                    method: 'GET',
                    success: function (response) {
                        Ext.getBody().unmask();
                        var responseData = Ext.decode(response.responseText);
                        if (responseData === 1) {
                            singer.LayoutController.createErrorPopup("Reject", 'Exchange Rejected', Ext.MessageBox.Success);
                        } else {
                            singer.LayoutController.createErrorPopup("Error Occured", 'System Error', Ext.MessageBox.WARNING);
                        }
                        store.load();
                    },
                    failure: function () {
                        Ext.getBody().unmask();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
        });



    },
    openSelectWorkOrderWindow: function (btn) {
        console.log(btn);
        console.log('test - Open Select Work Order Window');
        // var form = Ext.widget('addnewform');
        var form = Ext.widget('selectWorkOrderForm');
//        this.getController('CoreController').onClearSearch(form);
        var store = Ext.getStore("EXGwrkOrderNo");

        console.log(store);
        store.removeAll();
        store.load();
        
        this.getController('CoreController').loadBindedStore(store);


//        

        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Select Work Order',
            items: [form]
        });
        win.show();



        //  var form = Ext.widget('selectWrkOrderGrid');
//       var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Add New',
//            items: [form]
//        });
//        var loginData = this.getLoggedInUserDataStore();
//        //console.log(loginData.data.getAt(0).get('commonName'));
//        Ext.getCmp('technicianName').setValue(loginData.data.getAt(0).get('commonName'));
//        Ext.getCmp('testGrid').getStore().removeAll();
//        Ext.getCmp('spareN').getStore().removeAll();
//        Ext.getCmp('addnewform-save').hide();
//        Ext.getCmp('addnewform-submit').show();
//        win.show();
        // var MaintainWorkOrderform = Ext.widget('addnewform');
        //var store = this.getWorkOrderOpeningStore();
//        var store = this.getWorkOrderMaintainAddStore();
//
//        this.getController('CoreController').loadBindedStore(store);
//        //=====================
////            var repLevelStr = this.getReplevelSTRStore();
////            repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + record.get("eventId");
////        
//        //=====================
//        var form = Ext.widget('selectWrkOrderGrid');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Add New',
//            items: [form]
//        });
        win.show();
    }
});

