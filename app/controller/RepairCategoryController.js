/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Ext.define('RepairVar', {
//    singleton: true,
//    CatId: ''
//});
Ext.define('singer.controller.RepairCategoryController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.repaircategorycontroller',
    controllers: ['CoreController'],
    stores: [
        'RepairCategories',
        'RepairLevels'
    ],
    views: [
        'Administrator.Repair.RepairListGrid',
        'Administrator.Repair.NewRepairCategory',
        'Administrator.Repair.ViewRepairCategory',
        'Administrator.Repair.RepairLevelGrid',
        'Administrator.Repair.NewRepairLevel',
        'Administrator.Repair.ViewRepairLevel'
    ],
    refs: [
        {
            ref: 'RepairListGrid',
            selector: 'repairlistgrid'
        }, {
            ref: 'NewRepairCategory',
            selector: 'newrepaircategory'
        }, {
            ref: 'ViewRepairCategory',
            selector: 'viewrepaircategory'
        }, {
            ref: 'RepairLevelGrid',
            selector: 'repairlevelgrid'
        }, {
            ref: 'NewRepairLevel',
            selector: 'newrepairlevel'
        }, {
            ref: 'ViewRepairLevel',
            selector: 'viewrepairlevel'
        }
    ],
    init: function () {
        //        ////////console.log(this);
        var me = this;
        me.control({
            'repairlistgrid': {
                added: this.initializeGrid
            },
            'repairlistgrid button[action=open_window]': {
                click: this.openAddNew
            },
            'repairlistgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'repairlistgrid ': {
                viewRepairCat: this.onviewRepairCat
            },
            'repairlistgrid  ': {
                editRepairCat: this.onEditRepairCat
            },
            'repairlistgrid   ': {
                repairLevels: this.onRepairLevels
            },
            'newrepaircategory button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newrepaircategory button[action=create]': {
                click: this.insertRecord
            },
            'newrepaircategory button[action=save]': {
                click: this.updataRecord
            },
            'viewrepaircategory button[action=cancel]': {
                click: this.insertFormCancel
            },
            'repairlevelgrid button[action=open_window]': {
                click: this.openAddNewRepairLevel
            },
            'repairlevelgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'repairlevelgrid ': {
                viewLevel: this.onViewLevel
            },
            'repairlevelgrid  ': {
                editLevel: this.onEditLevel
            },
            'newrepairlevel button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newrepairlevel button[action=create]': {
                click: this.onNewRepairLevelCreate
            },
            'newrepairlevel button[action=save]': {
                click: this.onNewRepairLevelSave
            },
            'viewrepairlevel button[action=cancel]': {
                click: this.insertFormCancel
            }
        });
    },
    initializeGrid: function (grid) {
        var repStore = this.getRepairCategoriesStore();

        this.getController('CoreController').loadBindedStore(repStore);
    },
    insertRecord: function (btn) {
        var store = this.getRepairCategoriesStore();
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Category Created.', 'Creating New Category', true, true, this.getRepairListGrid(), false);
        var grid = this.getRepairListGrid();

    },
    onNewRepairLevelCreate: function (btn) {
        var form = this.getNewRepairLevel().down('form').getForm();
        var id = form.findField('levelCatId').getValue();

        var store = this.getRepairLevelsStore();
        //        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getLevels?levelCatId=' + id;

        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Category Level Created.', 'Creating New Category Level', true, true, this.getRepairLevelGrid(), false);

    },
    onNewRepairLevelSave: function (bt) {
        var me = this;
        Ext.MessageBox.confirm('Are you sure?', 'Do you want to save changes?', function (btn) {
            if (btn === 'yes') {
                var form = me.getNewRepairLevel().down('form').getForm();
                var id = form.findField('levelCatId').getValue();

                var store = me.getRepairLevelsStore();
                store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getLevels?levelcateid=' + id;

                me.getController('CoreController').onRecordUpdate(bt, store, 'Category Level Updated.', 'Updating Category Level', true, true, me.getRepairLevelGrid());
            }
        });
    },
    updataRecord: function (bt) {
        var me = this;
        Ext.MessageBox.confirm('Are you sure?', 'Do you want to save changes?', function (btn) {
            if (btn === 'yes') {
                me.getController('CoreController').onRecordUpdate(bt, me.getRepairCategoriesStore(), 'Category Data Updated.', 'Updating Category', true, true, me.getRepairListGrid());
            }
        });

    },
    openAddNew: function () {
        this.getController('CoreController').openInsertWindow('newrepaircategory');
        //        ////////console.log('came to add new');
    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    onviewRepairCat: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'viewrepaircategory', 'View');
    },
    onEditRepairCat: function (view, record) {
        this.getController('CoreController').openUpdateWindow(record, 'newrepaircategory', 'Edit');
    },
    onRepairLevels: function (view, record) {
        singer.AppParams.TEMP = record.get('reCateId');
        var store = this.getRepairLevelsStore();
        //        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getLevels?levelCatId=' + record.get('reCateId')

        this.getController('CoreController').loadBindedStore(store);
        var grid = Ext.widget('repairlevelgrid');

        var win = Ext.create("Ext.window.Window", {
            width: 700,
            height: 500,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            id: 'tempWindow',
            items: [grid]
        });
        win.setTitle(record.get('reCateDesc') + "'s Repair Levels");
        //        ////////console.log(singer.AppParams.TEMP);
        win.show();

    },
    openAddNewRepairLevel: function () {
        this.getController('CoreController').openInsertWindow('newrepairlevel');
        var grid = this.getRepairLevelGrid();
        var id = singer.AppParams.TEMP; //Ext.getCmp('tempWindow').title.substring(0, 1);
        var form = this.getNewRepairLevel().down('form').getForm();
        //        this.focusToFirstFieldinForm(form);
        form.findField('levelCatId').setValue(id);
        form.findField('levelCatId').setVisible(false);
    },
    onViewLevel: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'viewrepairlevel', 'View');
    },
    onEditLevel: function (view, record) {
        this.getController('CoreController').openUpdateWindow(record, 'newrepairlevel', 'Edit');
        var form = this.getNewRepairLevel().down('form').getForm();
        form.findField('levelCatId').setVisible(false);
        form.findField('levelName').setReadOnly(true);
        
        //this.focusToFirstFieldinForm(form);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    }
});