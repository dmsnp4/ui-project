



Ext.define('singer.controller.Main', {
    extend: 'Ext.app.Controller',
    views: [
        'AppContainer', 'AppHeaderContainer', 'Footer', 'Administrator.login.buttons.LoginButton', 'Administrator.login.buttons.LogoutButton',
        'Administrator.login.forms.LoginForm', 'HomeContainer', 'MenuContainer', 'ContentContainer'
    ],
    models: [
        'LoggedInUser',
        'WorkOrder'
    ],
    stores: [
        'LoggedInUserData'
    ],
    refs: [{
            ref: 'appView',
            selector: 'appcontainer'
        }, {
            ref: 'headView',
            selector: 'appheadercontainer'
        }, {
            ref: 'homeView',
            selector: 'apphomeContainer'
        }, {
            ref: 'loginView',
            selector: 'apploginform'
        }, {
            ref: 'logoutBtn',
            selector: 'logoutBtn'
        }, {
            ref: 'AppContentView',
            selector: 'appcontentContainer'
        }],
    init: function() {
        var me = this;
        //        me.isIdleStateStarted = true;
        me.removePreLoader();
        //        if (Ext.firefoxVersion !== 0) {
        //            //for development
        //            var cssText = '[id^="pagingtoolbar-"] .x-btn-icon-el { left: 0; top: 11px;} .x-btn-icon-el{top: 11px;} .x-form-text{height : 30px;}';
        //            //for production
        //            //var cssText = '[id^="pagingtoolbar-"] .x-btn-icon-el { left: 8px; top: 3px;} .x-btn-icon-el{top: 4px;} .x-form-text{height : 30px;}';
        //            Ext.util.CSS.createStyleSheet(cssText, 2);
        //        }
        me.beforeLoginCheck();
        me.control({
            /* 'appcontainer': {
             afterrender: this.openLoginView
             }, */
            'apploginform': {
                added: this.focusLoginForm
            }
        });
        //        me.checkFunctionIsAvailable();
        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));
        //        Ext.getBody().on('contextmenu', function(e) {
        //            me.isIdleStateStarted = false;
        //            // e.preventDefault();
        //        });
    },
    /***********************************************
     *
     * Drop down stores
     */
    creatRadioStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'radioStr',
            fields: [
                'bisId',
                'firstName',
                'userId',
                'bisName'
            ],
            //            autoLoad: true
        });
    },
    newTestStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'newTestStore',
            fields: [
                'mjrDefectDesc', 'minDefecDesc'
            ]
        });
    },
    createTestGrid: function() {
        Ext.create('Ext.data.Store', {
            id: 'testGrid',
            fields: [
                'issueNo', 'imeiNo'
            ]
        });
    },
    sprePartStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'sparePrtStr',
            fields: [
                {
                    name: 'workOrderMainId',
                    type: 'int'
                },
                {
                    name: 'wrokOrderNo',
                    type: 'string'
                },
                {
                    name: 'seqNo',
                    type: 'int'
                },
                {
                    name: 'partNo',
                    type: 'int'
                },
                {
                    name: 'partDesc',
                    type: 'string'
                },
                //                {name:'selliingPrice',type:'number'},
                {
                    name: 'invoiceNo',
                    type: 'string'
                },
                {
                    name: 'thirdPartyFlag',
                    type: 'int'
                },
                {
                    name: 'status',
                    type: 'int'
                },
                {
                    name: 'partSellPrice'
                }

            ]
        });
    },
    sprePartStore_temp: function() {
        Ext.create('Ext.data.Store', {
            id: 'sparePrtStr_temp',
            fields: [
                {
                    name: 'workOrderMainId',
                    type: 'int'
                },
                {
                    name: 'wrokOrderNo',
                    type: 'string'
                },
                {
                    name: 'seqNo',
                    type: 'int'
                },
                {
                    name: 'partNo',
                    type: 'int'
                },
                {
                    name: 'partDesc',
                    type: 'string'
                },
                //                {name:'selliingPrice',type:'number'},
                {
                    name: 'invoiceNo',
                    type: 'string'
                },
                {
                    name: 'thirdPartyFlag',
                    type: 'int'
                },
                {
                    name: 'status',
                    type: 'int'
                },
                {
                    name: 'partSellPrice'
                },
                {
                    name: 'erpPartNo',
//                    type: 'int'
                },
            ]
        });
    },
    sparePartsStoreOfAddNewForm: function() {
        Ext.create('Ext.data.Store', {
            id: 'sparePartN',
            fields: [
                //                'partNo', 'selliingPrice'
                {
                    name: 'workOrderMainId',
                    type: 'int'
                },
                {
                    name: 'wrokOrderNo',
                    type: 'string'
                },
                {
                    name: 'seqNo',
                    type: 'int'
                },
                {
                    name: 'partNo',
                    type: 'int'
                },
                {
                    name: 'partDesc',
                    type: 'string'
                },
                {
                    name: 'selliingPrice',
                    type: 'number'
                },
                {
                    name: 'invoiceNo',
                    type: 'string'
                },
                {
                    name: 'thirdPartyFlag',
                    type: 'int'
                },
                {
                    name: 'status',
                    type: 'int'
                }
            ]
        });
    },
    LoadDistributorList: function() {
        //        Ext.create('Ext.data.Store', {
        //            id: 'dsrmovements',
        //            model: 'singer.model.dsrmovementsMod',
        //            pageSize: singer.AppParams.INFINITE_PAGE,
        ////            proxy: {
        ////                type: 'ajax',
        ////                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBussinessTypeList?bisType=3&',
        ////                reader: {
        ////                    type: 'json',
        ////                    root: 'data'
        ////                }
        ////            },
        ////            autoLoad: true
        //        });
        //        ////console.log('LoadDistributorList');
    },
    LoadEnableUserList: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'EnableUsers',
            model: 'singer.model.DSRTrack',
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getTrackingEnableList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    loadhistorygrid: function() {
////        var IMEIvalue = Ext.getCmp('imeiVal').getValue();
//        var store = Ext.create('Ext.data.Store', {
//            id: 'histrgrid',
//            model: 'singer.model.WorkOrderOpeningM',
//            pageSize: singer.AppParams.INFINITE_PAGE,
//            proxy: {
//                type: 'ajax',
////                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?imeiNo=' + IMEIvalue,
//                  url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList',
//                reader: {
//                    type: 'json',
//                    root: 'data'
//                }
//            },
////            autoLoad: true
//        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    OpenWONumbers: function() {
        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        var store = Ext.create('Ext.data.Store', {
            id: 'openWOnum',
            fields: [
                'workOrderNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?status=1&bisId='+bisId+'',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    
    closeWOnumbers: function() {
        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        var store = Ext.create('Ext.data.Store', {
            id: 'clseWOnum',
            fields: [
                'workOrderNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getWorkOrderListToClose?bisId='+bisId+'',
//                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?status=1&bisId='+bisId+'',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    
    
    estimateStat: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'EstStat',
            fields: [
                'description', 'code'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getEstimateStatus',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    payOptions: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'PayOpt',
            fields: [
                'description', 'code'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getPaymentOption',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    createRepairCatStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'RepCats',
            model: 'singer.model.RepairCategory',
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'RepairCategories/getRapairCategories?reCateStatus=1',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    createWOopenDrpStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'woDrp',
//            model: 'singer.model.RepairCategory',
            fields: [
                'workOrderNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getQuickWorkOrderOpenList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    createMajorDefecStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'mjrdefectStr',
            model: 'singer.model.Defects',
//            fields: [
//                'mjrDefectDesc',
//                'mjrDefectCode'
//            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'MajorDefects/getMajorDefects?mjrDefectStatus=1',
                //                url: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantySchemeDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        });
        store.load();
        //this.getController('CoreController').loadBindedStore(store);
    },
    createDeliverTypeStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'DeliverType',
            fields: [
                'description', 'code'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getDeleveryType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createWarrentyTypeMaintainStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'WrrntyTypeMaintain',
            fields: [
                'code',
                'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getMaintainWarranty',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createWarrentyTypeStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'WrrntyType',
            fields: [
                'code',
                'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getWarrantyType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createNONwarrentyTypeStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'NonWrrntyType',
            fields: [
                'code', 'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getNonWarrantyType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createWoPaymentTypeStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'WoPayTypeStore',
            fields: [
                'code', 'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getWoPaymentType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createRepairStatusStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'RepStatus',
            fields: [
                'description', 'code'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getRepairStatus',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    
    WOcloseRepairStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'clseRprStore',
            fields: [
                'description', 'code'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/getWorkOrderCloseStatus',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    
    createRepairLevelStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'ReplevelSTR',
            fields: [
//                'levelDesc', {name:'repairLevelId',type:'int'}, 'levelPrice'
                 {name:'levelName',type:'String'},
                 {name:'levelId',type:'int'},
                 'levelPrice'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
//                url: singer.AppParams.JBOSS_PATH + 'addLevel/getLevels',
                url: singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei',
                
//                url: singer.AppParams.JBOSS_PATH + 'WorkOrderMaintainances/getWorkOrderMaintainList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: false
        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    
    createDsrShopStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'dsrShop',
            //  model: 'singer.model.Defects',
            fields: [
                'bisId',
                'userId',
                'bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            //            proxy: {
            //                type: 'ajax',
            //                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBussinessAvilableTypeList',
            //                //                url: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantySchemeDetails',
            //                reader: {
            //                    type: 'json',
            //                    root: 'data'
            //                }
            //            },
            //            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    createMinorDefecStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'minordefectStr',
            //  model: 'singer.model.Defects',
            //                        minDefectCode":67,"minDefectDesc"
            fields: [
                'minDefectCode', 'minDefectDesc'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'MinorDefects/getMinorDefects',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: false
        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    createDistributer_ShopStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'distributorstr',
            //  model: 'singer.model.Defects',
            fields: [
                'bisId',
                'bisName',
                'address'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructure',
                //                url: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantySchemeDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    
    createDistributer_ServiceCenterStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'servicestr',
            //  model: 'singer.model.Defects',
            fields: [
                'bisId',
                'bisName',
                'address'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructureServiceCenters',
                //                url: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantySchemeDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    
    createWrkOrderStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'wrkOrderNo',
            model: 'singer.model.WorkOrder',
//            fields: [
//                'workOrderNo', 'imeiNo', 'customerName',
//                'customerNic', 'product', 'brand', 'modleNo',
//                'dateOfSale', 'warrentyVrifType'
//            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?status=3',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    
    exchangeWONumstore: function() {
        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        console.log("bisId",bisId);
        Ext.create('Ext.data.Store', {
            id: 'EXGwrkOrderNo',
            model: 'singer.model.WorkOrder',
            fields: [
                'workOrderNo'
            ],
            
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/getWorkOrdersToExchange?bisId=' + bisId,
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    
    createdeviceExchangeReasonStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'exchngeReason',
            //  model: 'singer.model.Defects',
            fields: [
                'code', 'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/getDeviceExchangeReason',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createdeviceExchangeTypeStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'exchngeType',
            //  model: 'singer.model.Defects',
            fields: [
                'code', 'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/getDeviceExchangeType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createModelStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'modStore',
            fields: [
                {name: 'model_No', type: 'int'},
                'model_Description', 'wrn_Scheme'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'Models/getModelDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: false
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    createWarrentySchemeStore: function() {
        Ext.create('Ext.data.Store', {
            id: 'WarrentySchStore',
            fields: [
                'schemeId',
                'schemeName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantySchemeList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    businessTypeStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'bsnsTypeStr',
            fields: [
                'bisTypeId',
                'bisTypeDesc'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessTypes/getBussinessType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    createIMEIstore: function() {
        Ext.create('Ext.data.Store', {
            id: 'imeiSTR',
            fields: [
                'imeiNo',
                'modleNo',
                'brand',
                'product'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/getImeiMasterDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createBuisnessStructureStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'bisStore',
            model: 'singer.model.BuisnessM',
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessStructure',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    createDsrStore: function() {

        var store = Ext.create('Ext.data.Store', {
            id: 'dsrStore',
            fields: [
                'bisId',
                'bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBussinessTypeList?bisType=1',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    createDisStore: function(user) {
        //console.log('createDisStore');
        Ext.create('Ext.data.Store', {
            id: 'dissStore',
            fields: [
                'bisId',
                'bisDesc',
                'bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getUpperLevelList?bisType=3',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });

    },
    createSubdealerStore: function(user) {
        //console.log('createSubdealerStore');
        var store = Ext.create('Ext.data.Store', {
            id: 'subStore',
            fields: [
                'bisId',
                'bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBussinessTypeList?bisType=2',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    createDepTypesStore: function() {
        //console.log('createDepTypesStore');
        var store = Ext.create('Ext.data.Store', {
            id: 'depTypeStore',
            fields: ['depType', 'typeID'],
            data: [
                {
                    depType: "type1",
                    typeID: 1
                },
                {
                    depType: 'type2',
                    typeID: 2
                }
            ],
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    /*************************************Event************************/
    createEventStores: function() {
        this.createBrandStore();
        this.productFamilyStore();
        this.createEventPeriodStore();
        this.createSalesTypeStore();
    },
    createBrandStore: function() {
        //console.log('createDepTypesStore');
        var store = Ext.create('Ext.data.Store', {
            id: 'Brands',
            fields: [
                'code',
                'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'EventMasters/getBrandList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    productFamilyStore: function() {
        //console.log('createDepTypesStore');
        var store = Ext.create('Ext.data.Store', {
            id: 'ProductFamily',
            fields: [
                'code',
                'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'EventMasters/getProductFamily',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    createEventPeriodStore: function() {
        //console.log('createDepTypesStore');
        var store = Ext.create('Ext.data.Store', {
            id: 'EventPeriods',
            fields: [
                'code',
                'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'EventMasters/getEventPeriod',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    createSalesTypeStore: function() {
        //console.log('createDepTypesStore');
        var store = Ext.create('Ext.data.Store', {
            id: 'SalesTypes',
            fields: [
                'code',
                'description'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'EventMasters/getSalesType',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    },
    WOTrancferNum: function() {
        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        var store = Ext.create('Ext.data.Store', {
            id: 'WOtransfrNo',
            fields: [
                'workOrderNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/getWorkOrderToTransfer?bisId='+bisId+'',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
//            listeners: {
//                beforeload: function(store, operation, options) {
//
////            var proxyUrl = options.resetProxy(store.getProxy().api.read);
//                    var proxyUrl = store.getProxy().api.read;
//                    var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
//                    //console.log(proxyUrl);
//                    if (proxyUrl.indexOf("?") === -1) {
//                        proxyUrl += "?" + "bisId=" + bisId;
//
//                    } else {
//                        if (proxyUrl.indexOf("bisId") === -1) {
//                            proxyUrl += "&" + "bisId=" + bisId;
//
//                        } else {
//                            proxyUrl = proxyUrl.split("?")[0];
//                            proxyUrl += "?" + "bisId=" + bisId;
//                        }
//                    }
//                    store.getProxy().api.read = proxyUrl;
//                    ////console.log(proxyUrl);
//                },
//                resetProxy: function(proxyURI) {
//                    return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
//                }},
            autoLoad: true
        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    /***************************************************************************/
    //
    //
    addIdleStateListeners: function() {
        var me = this;
        Ext.getBody().on('mouseover', function(e) {
            me.isIdleStateStarted = false;
        });
        Ext.getBody().on('click', function(e) {
            me.isIdleStateStarted = false;
        });
        Ext.getBody().on('mousemove', function(e) {
            me.isIdleStateStarted = false;
        });
        window.onfocus = function endIdling() {
            me.isIdleStateStarted = false;
        };
    },
    checkAppIdleState: function() {
        var me = this;
        var logoutController = this.getController('LogoutController');
        var idleTime = singer.AppParams.IDLE_TIMEOUT;
        var runner = new Ext.util.TaskRunner();
        var task = runner.start({
            run: function() {
                if (me.isIdleStateStarted) {
                    idleTime = --idleTime;
                } else {
                    idleTime = singer.AppParams.IDLE_TIMEOUT;
                }
                if (idleTime === 0) {
                    //////console.log('stop idle counter');
                    runner.stop(task);
                    logoutController.performSilentLogOut();
                }
                me.isIdleStateStarted = true;
            },
            interval: 1000
        });
    },
    checkSession: function() {
        this.addIdleStateListeners();
        this.checkAppIdleState();
        ////console.log('session check');


        var runner = new Ext.util.TaskRunner();
        var task = runner.start({
            run: function() {
                Ext.Ajax.request({
                    url: singer.AppParams.BASE_PATH + '/sessionChecker.jsp',
                    success: function(response) {
                        //                        var responseData = Ext.decode(response.responseText);
                        ////console.log(response.responseText);
                        var data = response.responseText;
                        if (parseInt(data) === 0) {
                            Ext.Msg.show({
                                title: "Session is not Available",
                                msg: "Your session is not available.</br> Please login or Refresh to continue...",
                                buttonText: {
                                    yes: "OK"
                                },
                                icon: Ext.MessageBox.WARNING,
                                closable: false,
                                fn: function(btn) {
                                    if (btn === "yes") {
                                        window.location.href = singer.AppParams.BASE_PATH;
                                    }
                                }
                            });
                            runner.stop(task);
                        }
                    }
                });

            },
            interval: 30000
        });
        singer.ParamController.SESSION_CHECKER = task;
        singer.ParamController.TASKER = runner;
    },
    beforeLoginCheck: function() {
        var me = this;
        var loggedinstore = this.getLoggedInUserDataStore();
        loggedinstore.removeAll();
        Ext.Ajax.request({
            url: singer.AppParams.BASE_PATH + '/sessionHolder.jsp',
            params: "getSession=true",
            method: "GET",
            timeout: 10000,
            success: function(response, options) {
                var responseData = Ext.decode(response.responseText);
                if (parseInt(responseData) === 0) {
                    me.openLoginView();
                } else {
                    loggedinstore.add(responseData);
                    me.createApplicationView(responseData.UserType, true);
                }
            },
            failure: function(response, options) {
            }
        });
    },
    focusLoginForm: function(container) {
        container.down('form').getForm().findField('userId').focus(false, 300);
    },
    removePreLoader: function() {
        if (Ext.get('page-loader')) {
            Ext.get('page-loader').remove();
        }
    },
    openLoginView: function() {
        var loginView = Ext.widget("apploginform");
        var appView = this.getAppView();
        appView.insert(0, loginView);
    },
    createApplicationView: function(userType, hasSession) {

        ////console.log("coming to create application view ");

        hasSession = hasSession || false;
        var me = this;
        var appView = this.getAppView();
        var loginView = this.getLoginView();
        ////console.log(loginView);
        var headView = this.getHeadView();
        this.initializeAppReq();
        if (!hasSession) {
            ////console.log("no session and fresh login");
            loginView.animate({
                duration: 500,
                to: {
                    x: -loginView.getWidth()
                },
                listeners: {
                    afteranimate: function() {
                        ////console.log("coming to after animate function ");
                        appView.remove(loginView);
                        var homeView = Ext.widget('apphomeContainer');
                        var menuView = Ext.widget('appmainmenu');
                        var contentView = Ext.widget('appcontentContainer');
                        homeView.insert(0, menuView);
                        homeView.insert(1, contentView);
                        appView.insert(0, homeView);
                        homeView.setX(document.width);
                        homeView.animate({
                            duration: 300,
                            to: {
                                x: 10
                            },
                            listeners: {
                                afteranimate: function() {
                                    ////console.log("coming to afteranimate listner function ");
                                    var logoutBtn = Ext.widget('logoutBtn');
                                    headView.insert(1, logoutBtn);
                                    //                                    var userType = Ext.getStore('LoggedInUserData').first().get('UserType');
                                    //                                    userType === 0 ? logoutBtn.setIconCls('icon-dms-admin') : '';
                                    //                                    userType === 1 ? logoutBtn.setIconCls('icon-admin-user') : '';
                                    //                                    userType === 2 ? logoutBtn.setIconCls('icon-normal-user') : '';
                                    //                                    userType === 3 ? logoutBtn.setIconCls('icon-inquiry-user') : '';
                                    logoutBtn.setText("<b style='width: 90px; float:left; margin-left: 20px;'>Welcome</br><span style='width: 95px;'>" + Ext.getStore('LoggedInUserData').first().get('firstName') + "</span></b>");
                                    ////console.log('came near checksession');
                                    me.checkSession();
                                }
                            }
                        });
                    }
                }
            });
        } else {
            var homeView = Ext.widget('apphomeContainer');
            var menuView = Ext.widget('appmainmenu');
            var contentView = Ext.widget('appcontentContainer');
            homeView.insert(0, menuView);
            homeView.insert(1, contentView);
            appView.insert(0, homeView);
            var logoutBtn = Ext.widget('logoutBtn');
            headView.insert(1, logoutBtn);
            //            var userType = Ext.getStore('LoggedInUserData').first().get('UserType');
            //            userType === 1 ? logoutBtn.setIconCls('icon-admin-user') : '';
            //            userType === 2 ? logoutBtn.setIconCls('icon-normal-user') : '';
            //            userType === 3 ? logoutBtn.setIconCls('icon-inquiry-user') : '';
            logoutBtn.setText("<b style='width: 90px; float:left; margin-left: 20px;text-align: center;font-size: 14px;'>Welcome</br><span style='width: 95px;'>" + Ext.getStore('LoggedInUserData').first().get('firstName') + "</span></b>");
            me.checkSession();
        }
    },
    createLogoutView: function() {
        var appView = this.getAppView();
        var homeView = this.getHomeView();
        var headView = this.getHeadView();
        var logoutBtn = this.getLogoutBtn();
        homeView.animate({
            duration: 500,
            to: {
                x: -document.width
            },
            listeners: {
                afteranimate: function() {
                    appView.remove(homeView);
                    var loginView = Ext.widget("apploginform");
                    appView.insert(0, loginView);
                    loginView.setX(document.width);
                    loginView.animate({
                        duration: 300,
                        to: {
                            x: 10
                        },
                        listeners: {
                            afteranimate: function() {
                                headView.remove(logoutBtn);
                            }
                        }
                    });
                }
            }
        });
    },
    workordernum: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'wrkordern',
            fields: [
                'workOrderNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderInquries/getWorkOrderList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
//        this.getController('CoreController').loadBindedStore(store);
    },
    jobstatusDefects: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'jobDefect',
            model: 'singer.model.JobStatusDefect',
            pageSize: singer.AppParams.INFINITE_PAGE,
        });
    },
    jobstatusPartss: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'jobParts',
            model: 'singer.model.JobstatusParts',
            pageSize: singer.AppParams.INFINITE_PAGE
        });
    },
    jobestimate: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'jobestimate',
            model: 'singer.model.JobStatusestimate',
            pageSize: singer.AppParams.INFINITE_PAGE
        });
    },
    
    businessMappingStore: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'BisMapStr',
            fields: [
                'bisName','bisId'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getUserList',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    
    business_structure_device_issue: function() {
        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var store = Ext.create('Ext.data.Store', {
            id: 'structure_Device_issue',
            fields: [
                'bisId','bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBisIdUserList?bisId='+loginData,
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    
    initializeAppReq: function() {
        this.createBisCats();
        this.businessTypeStore();
        this.allExchangeRefferenceNoStore();
        this.createDepTypesStore();
        this.createModelStore();
        var loginStore = this.getLoggedInUserDataStore();
        var user = loginStore.data.getAt(0).get('userId');
        this.createDsrStore();
        this.createSubdealerStore();
        this.createBuisnessStructureStore();
        this.createMajorDefecStore();
        this.createDistributer_ShopStore();
        this.createDistributer_ServiceCenterStore();
//        this.createIMEIstore();
        this.createWrkOrderStore();
        this.createdeviceExchangeReasonStore();
        this.createdeviceExchangeTypeStore();
        this.createMinorDefecStore();
        this.newTestStore();
        this.sprePartStore();
        this.sparePartsStoreOfAddNewForm();
        this.createRepairLevelStore();
        this.createRepairStatusStore();
        this.createDeliverTypeStore();
        this.LoadEnableUserList();
        this.createWarrentyTypeStore();
        this.createNONwarrentyTypeStore();
        this.createWoPaymentTypeStore();
        this.LoadDistributorList();
        this.createTestGrid();
        this.createBussinessAsignList();
        this.createDsrShopStore();
        this.creatRadioStore();
        this.createEventStores();
        this.sprePartStore_temp();
        this.OpenWONumbers();
        this.createWarrentySchemeStore();
        this.estimateStat();
        this.payOptions();
        this.createWarrentyTypeMaintainStore();
        this.createWOopenDrpStore();
        this.WOTrancferNum();
        this.workordernum();
        this.jobstatusDefects();
        this.jobstatusPartss();
        this.jobestimate();
        this.closeWOnumbers();
        this.WOcloseRepairStore();
        this.businessMappingStore();
        this.exchangeWONumstore();
        this.business_structure_device_issue();
    },
    createUserIdsStore: function() {
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.create('Ext.data.Store', {
            id: 'userIds',
            model: 'singer.model.LoggedInUser',
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                //                url: singer.AppParams.JBOSS_PATH + 'RepairCategories/getRapairCategories',
                url: singer.AppParams.JBOSS_PATH + 'Users/getUsers',
                reader: {
                    type: 'json',
                    root: 'data'
                },
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                },
            },
            autoLoad: true
        });
        //        
    },
    createBisCats: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'BisIdsSto',
            model: 'singer.model.BusinessCategoriesM',
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                //                url: singer.AppParams.JBOSS_PATH + 'RepairCategories/getRapairCategories',
                url: singer.AppParams.JBOSS_PATH + 'BussinessCategory/getBussinessCategorys',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        this.getController('CoreController').loadBindedStore(store);
    },
    allExchangeRefferenceNoStore: function() {
        ////console.log('loading exchangeRfnStore store');
        Ext.create('Ext.data.Store', {
            //            model: 'singer.model.DeviceExchange',
            id: 'exchangeRfnStore',
            // model: 'singer.model.Replacement&ReturnModel',
            fields: [
                'exchangeReferenceNo',
                'imeiNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                // url: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/getReturnReplacement',
                url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/getDeviceExchangeDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
    },
    setHeaders: function(store) {
        var loginStore = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        ////console.log();
        store.getProxy().headers = {
            userId: loginStore.data.getAt(0).get('userId'),
            room: loginStore.data.getAt(0).get('room'),
            department: loginStore.data.getAt(0).get('department'),
            branch: loginStore.data.getAt(0).get('branch'),
            countryCode: loginStore.data.getAt(0).get('countryCode'),
            division: loginStore.data.getAt(0).get('division'),
            organization: loginStore.data.getAt(0).get('organization'),
            system: sys,
            // room: 'DefaultRoom',
            // department: 'DefaultDepartment',
            // branch: 'HeadOffice',
            // countryCode: 'LK',
            // division: 'SingerLK',
            // organization: 'Singer'
        };
    },
    createBussinessAsignList: function() {
        var store = Ext.create('Ext.data.Store', {
            id: 'bisAssignList',
            fields: [
                'bisName', 'bisId'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/getBussinessAsignList?structType=7,8',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        //        this.getController('CoreController').loadBindedStore(store);
    }
});