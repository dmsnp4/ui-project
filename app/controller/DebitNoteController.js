Ext.define('singer.controller.DebitNoteController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.debitnotecontroller',
    headDetail: null,
    imeiList: [],
    controllers: ['CoreController'],
    stores: [
        'AcceptDebitNote', 'LoggedInUserData', 'ImportDebitNotesStore'
    ],
    views: [
        'DeviceIssueModule.DebitNote.Grid',
        'DeviceIssueModule.DebitNote.Detail',
        'DeviceIssueModule.DebitNote.Form',
        'DeviceIssueModule.DebitNote.View'
    ],
    refs: [
        {
            ref: 'Grid',
            selector: 'debitnotegrid'
        }, {
            ref: 'View',
            selector: 'debitnoteview'
        }, {
            ref: 'Detail',
            selector: 'debitdetail'
        }, {
            ref: 'Form',
            selector: 'debitnoteform'
        }
    ],
    init: function () {
        //        //////console.log(this);

        var me = this;
        me.control({
            'debitnotegrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'debitnotegrid ': {
                onView: this.onView
            },
            'debitnotegrid  ': {
                onDetails: this.onDetails
            },
            'debitnotegrid': {
                added: this.initializeGrid
            },
            'debitnoteview button[action=done]': {
                click: this.insertFormCancel
            },
            'debitdetail button[action=scan]': {
                click: this.focusToIMEIfield
            },
            ' debitdetail button[action=accept]': {
                click: this.acceptIMEI
            },
            'debitdetail button[action=done]': {
                click: this.acceptDone
            },
            'debitdetail panel field': {
                specialkey: this.isEnterKey
            },
            ' debitdetail button[action=prevAll]': {
                click: this.removeIMEI
            },
        });


    },
    removeIMEI: function () {
        var debitNoteIMEI = Ext.getCmp('debitNoteImei');
//        //console.log(debitNoteIMEI);
        var acceptedIMEI = Ext.getCmp('acceptedImei');
//        //console.log(debitNoteIMEI);
        var selectionModel = acceptedIMEI.getSelectionModel();
//        //console.log(selectionModel);
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
//                //console.log(record);
                debitNoteIMEI.getStore().add(record);
                acceptedIMEI.getStore().remove(record);
            });

//          ---------------------------------------------------
            var store = Ext.getCmp('acceptedImei').getStore();

            Ext.getCmp('acceptedImei').setTitle('Accepted Count: ' + store.getCount());



            var store2 = Ext.getCmp('debitNoteImei').getStore();

            Ext.getCmp('debitNoteImei').setTitle('Debit Note Count: ' + store2.getCount());

        }
    },
    isEnterKey: function (field, e) {
        var me = this;
        if (e.getKey() === e.ENTER) {
            me.acceptIMEI();

            var storeOfDebitNote = Ext.getCmp('debitNoteImei').getStore();

            Ext.getCmp('debitNoteImei').setTitle('Debit Note Count: ' + storeOfDebitNote.getCount());

        }
    },
    acceptDone: function (btn) {
        console.log('acceptDone', btn);
        var frmWindow = btn.up('window');
        var me = this;
        var form = this.getDetail();
        var parentStore = this.getAcceptDebitNoteStore();
        //console.log(me.imeiList);
        var imeiArray = me.imeiList;


        var debNoIM = Ext.getCmp('debitNoteImei').getStore().data.items.length;
        //console.log(debNoIM);
        if ((debNoIM < 0) || (debNoIM === 0)) {
            me.headDetail.data.status = 3;
        } else {
            me.headDetail.data.status = 1;
        }
        var debit = Ext.create('singer.model.ImportDebitNotesModel',
                me.headDetail.data
                );
        //console.log(debit);
        //        debit.set('ImportDebitNoteDetails',answer);
        debit.set('importDebitList', imeiArray);
        ////console.log(debit);
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var doneBtn = Ext.getCmp('doneBtn');
        doneBtn.setDisabled(true);
        Ext.getBody().mask("Please wait...", 'loading');

        if (imeiArray.length > 0) {
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/addacceptDebitDetials',
                timeout: 300000,
                method: 'PUT',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(debit.data),
                success: function (response) {
                    var responseData = Ext.decode(response.responseText);
                    ////console.log(responseData);

                    var totalRec = responseData.returnFlag;
                    console.log("totalRec 55", totalRec);

                    if (totalRec == 1000000) {
                        Ext.Msg.alert("Records load fail", "Cannot connect to the database. Please contact the system Admin");
                    }

                    if (responseData.returnFlag === '1') {
                        Ext.getBody().unmask();
                        frmWindow.destroy();
                        parentStore.load();
                        doneBtn.setDisabled(false);

                    } else {
                        Ext.Msg.alert("Error", responseData.returnMsg);
                        doneBtn.setDisabled(false);
                        Ext.getBody().unmask();

                    }
                },
                failure: function () {
                    doneBtn.setDisabled(false);
                    Ext.getBody().unmask();
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later", function () {
                        Ext.getCmp('imeiID').focus('', 10);
                    });
                }
            });
        } else {
            doneBtn.setDisabled(false);
            Ext.getBody().unmask();
            Ext.Msg.alert("Error in Accept", "No selected new IMEI number(s).", function () {
                Ext.getCmp('imeiID').focus('', 10);
            });
        }

    },
    acceptIMEI: function (fieldName, value, start) {

        var audio = new Audio('resources/sounds/beep.mp3');
        
        var me = this;
        var scannedIMEI = Ext.getCmp('imeiID').getValue();
        var debitImeiStore = Ext.getCmp('debitNoteImei').getStore();
        console.log("debitImeiStore", debitImeiStore);
        var acceptImeiStore = Ext.getCmp('acceptedImei').getStore(); //.bindStore(acceptimei);
        var res = debitImeiStore.findExact('imeiNo', scannedIMEI);
        if (res !== -1) {
            var record = debitImeiStore.getAt(res);
            var loginData = this.getLoggedInUserDataStore();
            var uid = loginData.data.getAt(0).get('extraParams');
            record.data.bisId = uid;
            me.imeiList.push(record.data);
            acceptImeiStore.add(record);
            debitImeiStore.remove(record);
            console.log(Ext.getCmp('acceptedImei').getStore().getCount());
            var store = Ext.getCmp('acceptedImei').getStore();
            var store2 = Ext.getCmp('debitNoteImei').getStore();
            //   Ext.getCmp('acceptedImei').setTitle('Accepted Count: '+store.getCount()); //replace the count code
            Ext.getCmp('acceptedImei').setTitle('Accepted Count: ' + store.getCount());
            Ext.getCmp('debitNoteImei').setTitle('Debit Note Count: ' + store2.getCount());
        } else {
            Ext.MessageBox.alert('Accept Error', 'The selected IMEI is not in the Debit Note', function () {
                Ext.getCmp('imeiID').focus('', 10);
            });

            audio.play();
            //console.log('');
        }
        Ext.getCmp('imeiID').setValue('');
    },
    focusToIMEIfield: function (btn) {
        Ext.getCmp('imeiID').focus('', 10);

    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    initializeGrid: function (grid) {
        var store = this.getAcceptDebitNoteStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'debitnoteview', 'View');
        Ext.getCmp('imeiID').focus('', 10);
    },
    onDetails: function (view, record) {
        console.log('onDetails');


//        //console.log(record);
        this.imeiList = [];
        this.headDetail = record;
        var form = Ext.widget('debitdetail');
        var win = Ext.create("Ext.window.Window", {
            width: 500,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Accept Debit Note',
            items: [form]
        });
        win.show();
        var debitNoteDetail = Ext.create('Ext.data.Store', {
            model: 'singer.model.ImportDebitNoteDetailM'
        });
        //        //console.log('comming to debit note detail store');
        //        //console.log(debitNoteDetail);

        var acceptimei = Ext.create('Ext.data.Store', {
            model: 'singer.model.ImportDebitNoteDetailM'
        });

        // get the id of DEBIT NOTE IMEI grid
        var grid = Ext.getCmp('debitNoteImei');
        ////console.log(grid);
        var debitNoteNo = record.get('debitNoteNo');
        var loginData = this.getLoggedInUserDataStore();
        Ext.Msg.wait('Please wait', 'Gathering information...', {
            interval: 300
        });
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNoteDetial?debitNoteNo=' + debitNoteNo,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response) {
                var responseData = Ext.decode(response.responseText);

                console.log("CCC9 ", responseData.totalRecords);

                var totalRec = responseData.totalRecords;

                if (totalRec == 1000000) {
                    Ext.Msg.alert("Records load failed", "Cannot connect to the database. Please contact the system Admin");
                }

                var data = responseData.data[0].importDebitList;
                ////console.log(data);
                debitNoteDetail.add(data);
                debitNoteDetail.data.each(function (itemAll) {
                    if (itemAll.data['status'] === 3) {
                        debitNoteDetail.remove(itemAll);
                        acceptimei.add(itemAll);
                    }
                });
                grid.bindStore(debitNoteDetail);
                Ext.Msg.hide();
                Ext.getCmp('imeiID').focus('', 10);

                //load initial data


                var store2 = Ext.getCmp('debitNoteImei').getStore();

                Ext.getCmp('debitNoteImei').setTitle('Debit Note Count: ' + store2.getCount());

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later", function () {
                    Ext.getCmp('imeiID').focus('', 10);
                });
            }
        });

        var acceptimei = Ext.create('Ext.data.Store', {
            model: 'singer.model.ImportDebitNoteDetailM'
        });

        // get the id of the ACCEPTED IME grid and bind the store to it
        Ext.getCmp('acceptedImei').bindStore(acceptimei);
//        Ext.getCmp('DebitNtNo').setText('Debit Note No: '+debitNoteNo);



    },
});