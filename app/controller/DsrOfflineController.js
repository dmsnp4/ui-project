Ext.define('singer.controller.DsrOfflineController', {
    extend: 'Ext.app.Controller',
    views: [
        'Administrator.AllowDsrOffline.AddOfflineDsrForm'
    ],
    stores: [
        'DSRListStore'
    ],
    refs: [
        {
            ref: 'AddOfflineDsrForm',
            selector: 'addOfflinedsrForm'
        }
    ],
    init: function() {

        var me = this;
        me.control({
            'radiogroup': {
                change: this.selectOption
            },
            'addOfflinedsrForm button[action=cancel]': {
                click: this.closeWin
            },
//            'AddOfflineDsrForm button[action=validate]': {
//                click: this.validateIMEI
//            },
            'addOfflinedsrForm button[action=save]': {
                click: this.saveData
            }
        });
    },
    initialize: function(grid) {
        
        console.log("init dsr");
//        console.log('Initializing IMEI Swap ....');
//         Ext.getStore('radioStr').load();
//        var userStore = this.getDeviceIssueStore();
//        this.getController('CoreController').loadBindedStore(userStore);
//        var loginStore = this.getLoggedInUserDataStore();
//        var loggedUser = this.getLoggedInUserDataStore();
//        var user = loggedUser.data.getAt(0).get('extraParams');


    },
    selectOption: function(me, newValue, oldValue, eOpts) {
        var chekedVal = newValue.type;
        console.log('Radio Buuton select..........', chekedVal);
        if (chekedVal == 1) {
            Ext.getCmp('enter-swap-imei').setVisible(true);
            Ext.getCmp('select-swap-bis-id').setVisible(false);
        } else {
            Ext.getCmp('enter-swap-imei').setVisible(false);
            Ext.getCmp('select-swap-bis-id').setVisible(true);
        }
    },
    closeWin: function(btn) {
        
        var frm = btn.up('window');
        frm.close();
    },
    validateIMEI: function(btn) {
        var imeiNo = Ext.getCmp('swap-imei').getValue();
        if (imeiNo != null && imeiNo.length > 0) {
            Ext.getBody().mask('Please wait...', 'loading');
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'SwapIMEIService/validateIMEI?imei=' + imeiNo,
                method: 'GET',
                success: function(response, btn) {
                    Ext.getBody().unmask();
                    var responseData = Ext.decode(response.responseText);
                    var status = responseData.statusMsg;
                    if (status == 'Successfull') {
                        var bisId = responseData.bisId;
                        var bisName = responseData.bisName;
                        if (bisId == 0) {
                            Ext.Msg.alert("IMEI", 'Invalid IMEI.');
                        } else {
                            Ext.getCmp('current-bis-loc').setText(bisId + ' - ' + bisName);
                            Ext.getCmp('hdn-bis-id').setValue(bisId);
                        }

                    } else {
                        Ext.Msg.alert("Error", status);
                    }
                },
                failure: function() {
                    Ext.getBody().unmask();
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert("Enter", "Enter the IMEI first.");
        }

    },
    saveData: function(btn) {
        
          Ext.getBody().mask('Please wait...', '');

        console.log('Save button ....');
//        var bisId = Ext.getStore('DSRListStore').getAt(0).data.bisId;
        var bisId = Ext.getCmp("dsr").getValue();
//        var statusOfline = Ext.getStore('BuisnessStore').getAt(0).data.statusOfline;

        var allowOffline = Ext.getCmp("allowOffline").getValue();

        console.log("bisid", bisId);
        console.log("allowOffline", allowOffline);

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DsrAllowOffline/editDsrAllowOffline',
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            },
            params: {
                bisId: bisId, statusOfline : allowOffline
            },
            timeout: 6000,
            async: true,
            // isUpload: true,
            // headers: {'Content-Type': 'multipart/form-data'},
//            jsonData: {
//                //excelBase64: excel.getValue()
//            },
            success: function(response) {
                Ext.getBody().unmask();
                Ext.Msg.alert("Success", "Update  Successfully");

            },
            failure: function(response, options) {
                Ext.getBody().unmask();
                console.log("Fail to Update");
            }
        }

        );
    }
});



