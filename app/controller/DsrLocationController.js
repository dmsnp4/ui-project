Ext.define('singer.controller.DsrLocationController', {
    extend: 'Ext.app.Controller',
//    alias: 'widget.dsrlocationcontroller',
    map: null,
    markers: [],
    controllers: [
        'CoreController'
    ],
    stores: [
        'DsrCurrentLocations', 'LoggedInUserData'
    ],
    models: [
    ],
    views: [
        'DSRTrackingModule.ShowDsrLocation.ShowLocation'
    ],
    refs: [
        {
            ref: 'ShowLocation',
            selector: 'dsrshowlocation'
        }
    ],
    init: function() {
        //        ////////console.log(this);
        var me = this;
        me.control({
            'dsrshowlocation': {
                added: this.initializeLocation
            },
            'dsrshowlocation checkboxgroup[action=check]': {
                change: this.onCheckChange
            }
        });
    },
    initializeLocation: function(cmp) {
        var map = Ext.getCmp('dsrMap');
        var loginData = this.getLoggedInUserDataStore();
        var bisId = loginData.data.getAt(0).get('extraParams');
        var store = this.getDsrCurrentLocationsStore();
        
        //Ext.getCmp('countLbl').setText(responseData.data.length);
        this.map = map;
        Ext.getBody().mask('Establishing Connection', 'large-loading');
        var me = this;
        this.removeMarkers(store);
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getLastTrackingDSRList?bisId=' + bisId + '&bisTypeId=1',
            //            params: val,
            method: "GET",
            timeout: 10000,
            success: function(response, options) {

                Ext.getBody().unmask();
                
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData.totalRecords);
                store.add(responseData.data);
                me.addMarkers(store.data);
                Ext.getCmp('countLbl').setText('Total DSRs: '+responseData.totalRecords+'/'+responseData.data.length);
            },
            failure: function(response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                form.getForm().reset();

            }
        });

    },
    onCheckChange: function(cmp) {
        var loginData = this.getLoggedInUserDataStore();
        var bisId = loginData.data.getAt(0).get('extraParams');
        var val = 'bisId=' + bisId+'&';
        var ch = false;
        cmp.items.each(function(item) {
            if (item.checked) {
                val += item.name + '=' + item.inputValue + '&';
                ch = true;
            } else {
                val += item.name + '=' + '0' + '&';
            }
        });
        ////console.log(ch);
        if (!ch) {
            this.initializeLocation();
        } else {
            var store = this.getDsrCurrentLocationsStore();
            this.removeMarkers(store);
            Ext.getBody().mask('Establishing Connection', 'large-loading');
            var me = this;
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getBussinessTrackingList',
                params: val,
                method: "GET",
                timeout: 10000,
                success: function(response, options) {

                    Ext.getBody().unmask();
                    var responseData = Ext.decode(response.responseText);
                    ////console.log(responseData);
                    store.add(responseData.data);
                    me.addMarkers(store.data);
                },
                failure: function(response, options) {
                    Ext.getBody().unmask();
                    singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                    form.getForm().reset();

                }
            });
        }
    },
    addMarkers: function(data) {
        var map = this.map;
        var markerArray = this.markers;
        var infowindow = new google.maps.InfoWindow();
        var PointColor;
        data.each(function(item) {
            //            //console.log(item.data);
            var data = item.data;
            var getType = data.bisTypeId;
            ////console.log(getType);
            PointColor = "666666";

            switch (getType) {
                case 1:
                    PointColor = "1c53b7";
                    break;
                case 2:
                    PointColor = "af8162";
                    break;
                case 3:
                    PointColor = "00cf6b";
                    break;
                case 8:
                    PointColor = "ffc71c";
                    break;
                case 9:
                    PointColor = "e22b2b";
                    break;
            }

            var mark = new google.maps.Marker({
                map: map.gmap,
                draggable: false,
                animation: google.maps.Animation.DROP,
                icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=|' + PointColor + '|FFFFFF',
                //            icon: mapIcon,
                position: new google.maps.LatLng(data.latitite, data.logtitute)
            });

            
            ////console.log('data--',data);
            google.maps.event.addListener(mark, 'mousedown', function(e) {
                infoBubble.close();
                infoBubble.open(map.gmap, this);
                //                infoBubble.record = {places: this.data};
                infoBubble.setContent([
                    '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h1 id="firstHeading" class="firstHeading">' + data.bisName + '</h1>' +
                            '<div id="bodyContent">' +
                            '<p><b>' + data.bisId+'</b></br>'
                            + data.bisName+'</br>'
                            + data.bisDesc+'</br>'
                            + data.address+'</br>'
                            + data.telePhoneNum+'</br>'
                            + data.parantName+'</br>'
                            + data.bisTypeDesc+'</br>'
                            + '<br>' + data.visitDate + '</p>' +
                            '</div>' +
                            '</div>'
                ].join(''));
            });
            var infoBubble = new google.maps.InfoWindow();
//            //console.log(mark);
            markerArray.push(mark);
        });

    },
    removeMarkers: function(store) {
        var map = this.map;
        var mark = this.markers;
        store.removeAll();
        //        //console.log(map.gmap);
        for (var i = 0; i < mark.length; i++) {
            mark[i].setMap(null);
        }
    }

});