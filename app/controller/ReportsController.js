
Ext.define('singer.controller.ReportsController', {
    extend: 'Ext.app.Controller',
    views: ['Reports.ReportsContainer'],
    models: [],
    stores: ['ReadExcel'],
    refs: [{
            ref: 'ReportsContainer',
            selector: 'reportsContainer'
        }],
    init: function() {
        var me= this;
         me.control({
            
//            'reportsContainer button[action=showUserMaintenanceReport]': {
//                click: this.openUserMaintenanceReport
//            },
//             'reportsContainer button[action=showBusinessReport]': {
//                click: this.openBusinessReport
//            },
//             'reportsContainer button[action=showUserGroupsReport]': {
//                click: this.openUserGroupsReport
//            },
             'reportsContainer button[action=showTransitReport]': {
                click: this.openTransitReport
            },
             'reportsContainer button[action=showCategoryReport]': {
                click: this.openCategoryReport
            },
             'reportsContainer button[action=showRepairCategoriesReport]': {
                click: this.openRepairCategoriesReport
            },
            
            'reportsContainer button[action=showRepairLevelsReport]': {
                click: this.openRepairLevelsReport
            },
             'reportsContainer button[action=showMajorReport]': {
                click: this.openMajorReport
            },
             'reportsContainer button[action=showMinorReport]': {
                click: this.openMinorReport
            },
             'reportsContainer button[action=showSparesReport]': {
                click: this.openSparesReport
            },
             'reportsContainer button[action=showModelReport]': {
                click: this.openModelReport
            },
            'reportsContainer button[action=showWarrantyReport]': {
                click: this.openWarrantyReport
            },
            'reportsContainer button[action=showComplainReport]': {
                click: this.openCustomerComplainReport
            },
            'reportsContainer button[action=showExceptionReport]': {
                click: this.openExceptionReport
            },
            'reportsContainer button[action=showSQLReport]': {
                click: this.showSQLReport
            },
            'reportsContainer button[action=showUserList]': {
                click: this.showUserList
            }
        });

    },
    
   
    
    openCustomerComplainReport:function (){      
       this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=CustomerComplainsReport.rptdesign",'Customer Complain Report');
    },
    
    showUserList:function (){      
       this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=UserList_DM.rptdesign",'User List Report');
    },
    
    showSQLReport:function(btn){
        var readExcel = Ext.getStore('ReadExcel');
        readExcel.load();
        
        var myWin = new Ext.Window(
        {
            layout: 'fit',
            title: 'Account Receipts',
            width: '90%',
            height: '70%',
            closable: true,
            buttonAlign: 'center',
            items: Ext.create(
                    'singer.view.ReportsModule.ReportsMenu'),
            modal: true
        });
       myWin.show();
    },
    
    openTransitReport: function() {
       this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ClearTransitsReports.rptdesign",'Clear Transits Report');
    },
    
    openCategoryReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=CategoryListReport.rptdesign",'Category List Report');
    },
    
    openRepairCategoriesReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepairCategoriesReport.rptdesign",'Repai Categories Report');
    },
    
    openRepairLevelsReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepairLevelsReport.rptdesign",'Repair Level Report');
    },
    
    openMajorReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DefectCodesReport.rptdesign",'Major Defect Codes Report');
    },
    
    openMinorReport: function() {      
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=MinorDefectCodesReport.rptdesign",'Minor Defect Codes Report');
    },
    
    openSparesReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=SparesMaintenanceReport.rptdesign",'Spares maintenance report');
    },
    
    openModelReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ModelListReport.rptdesign",'Model Lists Report');
    },
    
    openWarrantyReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=WarrantyAdjustmentReport.rptdesign",'Warrenty Adjustment Report');
    },
    
    openExceptionReport: function() {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=BisIdMismatch.rptdesign",'IMEI Exception Report');
    }
    

});



