Ext.define('singer.controller.WarrentySchemeController', {
    extend: 'Ext.app.Controller',
    //    alias: 'widget.repaircategorycontroller',
    controllers: ['CoreController'],
    stores: [
        'WarrantySchemes'
    ],
    views: [
        'Administrator.warrentyScheme.Grid', 'Administrator.warrentyScheme.NewScheme',
        'Administrator.warrentyScheme.View'
    ],
    refs: [
        {
            ref: 'Grid',
            selector: 'warschgrid'
        },
        {
            ref: 'NewScheme',
            selector: 'newscheme'
        },
        {
            ref: 'View',
            selector: 'viewscheme'
        }
    ],
    init: function () {
        //        ////////console.log(this);
        var me = this;
        me.control({
            'warschgrid': {
                added: this.initializeGrid
            },
            'warschgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'warschgrid ': {
                ediWarScheme: this.onEdit
            },
            'warschgrid  ': {
                viewWarScheme: this.onView
            },
            'warschgrid button[action=open_window]': {
                click: this.onOpenAddNew
            },
            'newscheme button[action=cancel]': {
                click: this.onFormCancle
            },
            'newscheme button[action=create]': {
                click: this.onCreate
            },
            'newscheme button[action=save]': {
                click: this.onSave
            },
            'viewscheme button[action=done]': {
                click: this.onDone
            }

        });
    },
    initializeGrid: function (grid) {
        var store = this.getWarrantySchemesStore();
        //
        this.getController('CoreController').loadBindedStore(store);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onOpenAddNew: function (cmp) {
        this.getController('CoreController').openInsertWindow('newscheme');
    },
    onFormCancle: function (cmp) {
        this.getController('CoreController').onInsertFormCancel(cmp);
    },
    onCreate: function (cmp) {
        var store = this.getWarrantySchemesStore();
        this.getController('CoreController').onNewRecordCreate(cmp, store, 'New Schema Created.', 'Creating New Schema', true, true, this.getGrid(), false);
    },
    onEdit: function (grid, record) {
        this.getController('CoreController').openUpdateWindow(record, 'newscheme', 'Edit');
    },
        onView:function(grid, record){
            var win = Ext.widget('viewscheme');
            var form = win.down('form');
            form.getForm().loadRecord(record);
            win.show();
        },
    onSave: function (cmp) {
        var me = this;
        Ext.MessageBox.confirm('Are you sure?', 'Do you want to save changes?', function (btn) {
            if (btn === 'yes') {
                me.getController('CoreController').onRecordUpdate(cmp, me.getWarrantySchemesStore(), 'Schema Data Updated.', 'Updating Schema', true, true, me.getGrid());
            }
        });


    },
    onDone:function(cmp){
        cmp.up('window').close();
    }
});