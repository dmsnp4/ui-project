


Ext.define('singer.controller.IMEImaintenace', {
    extend: 'Ext.app.Controller',
    controllers: ['CoreController'],
    views: [
        'DeviceIssueModule.IMEImaintenace.IMEIgrid',
        'DeviceIssueModule.IMEImaintenace.IMEIview'
    ],
    stores: [
        'IMEImaintenace'
    ],
    models: [
        'IMEImaintenanceM'
    ],
    refs: [
        {
            ref: 'MainGrid',
            selector: 'imeiMaintenaceGrid'
        },
        {
            ref: 'IMEIview',
            selector: 'imeiGridview'
        }
    ],
    init: function() {
        var me = this;
        me.control({
            'imeiMaintenaceGrid': {
                added: this.initializeGrid
            },
            'imeiMaintenaceGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'imeiMaintenaceGrid   ': {
                IMEIView: this.openIMEIView
            },
            'imeiGridview button[action=done]': {
                click: this.IMEIviewCancel
            },
            'imeiMaintenaceGrid ': {
                deactivate1: this.onDeactivate
            },
            
            ////////////////////////////////////////////////
//            ' importDebitgrid': {
//                debitView: this.openDebitNoteViewWindow
//            },
            'debitView button[action=cancel]': {
                click: this.mainDebitGridViewCancel
            },
            
            
            'importdebitdetailsGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'importDebitgrid   ': {
                detailsgrid: this.onDetailsGrid
            },
            ' imeiMaintenaceGrid ': {
                activate1: this.onActivate
            },
//            'importdebitdetailsGrid   ': {
//                detailsgridView: this.openDebitNoteDetailsViewWindow
//            },
        });
    },
    
    onActivate: function(btn, record) {
//        //console.log(record);
        var store = this.getIMEImaintenaceStore();
        var serviceRecord = record.data;
        var loginData = this.getIMEImaintenaceStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.MessageBox.confirm('Deactivate.', 'Are you sure you want to Activate IMEI', confirmFunction);

        function confirmFunction (btn)
        {
            if (btn === 'yes')
            {
                Ext.Msg.wait('Please wait', 'Deactivating Data...', {interval: 300});
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/activateImeiMaster',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(serviceRecord),
                    success: function(response) {
                        Ext.Msg.hide();
                        var responseData = Ext.decode(response.responseText);
                        //console.log(responseData);
                        if (responseData.returnFlag === '1') {
                            
                        }
                        store.load();
                    },
                    failure: function() {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
        }   

    },
    
    onDeactivate: function(btn, record) {
//        //console.log(record);
        var store = this.getIMEImaintenaceStore();
        var serviceRecord = record.data;
        var loginData = this.getIMEImaintenaceStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.MessageBox.confirm('Deactivate.', 'Are you sure you want to Deactivate IMEI', confirmFunction);

        function confirmFunction (btn)
        {
            if (btn === 'yes')
            {
                Ext.Msg.wait('Please wait', 'Deactivating Data...', {interval: 300});
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/deactivateImeiMaster',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(serviceRecord),
                    success: function(response) {
                        Ext.Msg.hide();
                        var responseData = Ext.decode(response.responseText);
                        //console.log(responseData);
                        if (responseData.returnFlag === '1') {
                            
                        }
                        store.load();
                    },
                    failure: function() {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
        }   

    },
    openIMEIView: function(view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'imeiGridview', 'View');
    },
    IMEIviewCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    initializeGrid: function(grid) {
        var me = this;
        ////console.log("come to app initialize");
         var tempStore = me.getIMEImaintenaceStore();
        if (tempStore.getCount() === 0) {
            tempStore.load();
        }
//        this.getController('CoreController').loadBindedStore(tempStore);
    },
    
    moreViewClose: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    
    
    
    ////////////////////////////////////////////////////
    onDetailsGrid: function(view, record) {
        ////console.log('comming to here');
        var store = this.getImportDebitDetailsStore();
        ////console.log(store);
        this.getController('CoreController').loadBindedStore(store);
        var grid = Ext.widget('importdebitdetailsGrid');

        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
           modal: true,
            constrain: true,
            id: 'tempWindow',
            items: [grid]
        });
        win.show();

    },
    onClearSearch: function(btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    mainDebitGridViewCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    
//    openDebitNoteViewWindow: function(view, record) {
//        this.getController('CoreController').onOpenViewMoreWindow(record, 'debitView',"Details of the DEBIT NOTE NO:"+ record.get('debitNoteNo'));
//    },
    openDebitNoteDetailsViewWindow: function(view, record) {
        ////console.log("comming to openDebitNoteDetailsViewWindow: function");
        this.getController('CoreController').onOpenViewMoreWindow(record, 'debitDetailView', "Details of the IMEI NO:"+record.get('imeiNo'));
    },
    
   

});