
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.controller.CategoryList', {
    extend: 'Ext.app.Controller',
    alias: 'widget.categorylist',
//    requirs:['NewUserController'],
    controllers: ['CoreController'],
    stores: [
        'CategoryList'
    ],
    views: [
        'Administrator.categoryList.CategoryGrid', 'Administrator.categoryList.NewCategory', 'Administrator.categoryList.CategoryView'
    ],
    refs: [{
            ref: 'CategoryGrid',
            selector: 'categorygrid'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        }, {
            ref: 'NewCategory',
            selector: 'newcategory'
        }, {
            ref: 'CategoryView',
            selector: 'categoryview'
        }],
    init: function () {
//        //////console.log(this);
        var me = this;
        me.control({
            'categorygrid': {
                added: this.initializeGrid
            },
            'categorygrid ': {
                categoryEdit: this.openUpdateForm
            },
            'categorygrid button[action=open_window]': {
                click: this.openAddNew
            },
            'categorygrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            '  categorygrid ': {
                categoryView: this.moreViewOpen
            },
            'newcategory button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newcategory button[action=save]': {
                click: this.updataRecord
            },
            'newcategory button[action=create]': {
                click: this.insertRecord
            },
            ' categorygrid': {
                itemdblclick: this.moreViewOpen
            },
//            'edituser button[action=cancel]': {
//                click: this.insertFormCancel
//            },

            'categoryview button[action=cancel]': {
                click: this.insertFormCancel
            }

//            'usergrid': {
//                added: this.initializeGrid
//            },
//            'usergrid button[action=open_window]': {
//                click: this.openAddNew
//            }
        });
    },
    initializeGrid: function (grid) {
        //////console.log('loading cat list');
        var userStore = this.getCategoryListStore();
        this.getController('CoreController').loadBindedStore(userStore);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    updataRecord: function (btn) {
        var me=this;
        var bt=btn;
        Ext.MessageBox.confirm('Are you sure?', 'Do you want to save changes?', function (btn) {
            if (btn === 'yes') {
                me.getController('CoreController').onRecordUpdate(bt, me.getCategoryListStore(), 'Category Data Updated.', 'Updating Category', true, true, me.getCategoryGrid());
            }
        });
        
    },
    insertRecord: function (btn) {

        var store = this.getCategoryListStore();
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Category Created.', 'Creating New Category', true, true, this.getCategoryGrid(), false);
        var grid = this.getCategoryGrid();
        store.load();
        grid.bindStore(store);
    },
    moreViewOpen: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'categoryview','View');
    },
    openUpdateForm: function (view, record) {
        this.getController('CoreController').openUpdateWindow(record, 'newcategory', 'Edit');


        var form = this.getNewCategory().down('form').getForm();
         //  this.focusToFirstFieldinForm(form);
//        form.findField('userId').setDisabled(true);
        form.findField('cateId').hide();

    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    openAddNew: function () {

        this.getController('CoreController').openInsertWindow('newcategory');
        var form = this.getNewCategory().down('form').getForm();
//        form.findField('userId').setDisabled(true);
        form.findField('cateId').hide();
         //  this.focusToFirstFieldinForm(form);

//        var userGroupStore = this.getUserGroupsStore();
//        Ext.getCmp('allUserGroups').bindStore(userGroupStore.load());
//        this.getController('CoreController').loadBindedStore(userGroupStore);
    }
//    updateFormSubmit: function(btn) {
//        this.getController('CoreController').onRecordUpdate(btn, this.getCategoryListStore(), 'Category List Data Updated.', 'Updating Category List', true, true, this.getCategoryGrid());
//    }
//    assignOneToUser: function() {
//        var allGrid = Ext.getCmp('allUserGroups');
//        var assignGrid = Ext.getCmp('UserGroupsGrid');
//        var selectionModel = allGrid.getSelectionModel();
//        if (selectionModel.hasSelection()) {
//            selectionModel.getSelection().forEach(function(record, index, array) {
//                allGrid.getStore().remove(record);
//                assignGrid.getStore().add(record);
//            });
//        }
//
//    },
//    assignAllToUser: function() {
//        var allGrid = Ext.getCmp('allUserGroups');
//        var assignGrid = Ext.getCmp('UserGroupsGrid');
//        var allUserStore = allGrid.getStore();
//        var userGroupStore = assignGrid.getStore();
//
//        allUserStore.data.each(function(item) {
//            allUserStore.remove(item);
//            userGroupStore.add(item);
//        });
//
//    },
//    removeOneFromUser: function() {
//        var allGrid = Ext.getCmp('allUserGroups');
//        var assignGrid = Ext.getCmp('UserGroupsGrid');
//        var selectionModel = assignGrid.getSelectionModel();
//        if (selectionModel.hasSelection()) {
//            selectionModel.getSelection().forEach(function(record, index, array) {
//                allGrid.getStore().add(record);
//                assignGrid.getStore().remove(record);
//            });
//        }
//    },
//    removeAllFromUser: function() {
//        var allGrid = Ext.getCmp('allUserGroups');
//        var assignGrid = Ext.getCmp('UserGroupsGrid');
//        var allUserStore = allGrid.getStore();
//        var userGroupStore = assignGrid.getStore();
//
//        userGroupStore.data.each(function(item) {
//            allUserStore.add(item);
//            userGroupStore.remove(item);
//        });
//    }
//    initializeGrid: function() {
//        var userStore = this.getUsersStore();
//        this.getController('CoreController').loadBindedStore(userStore);
//        this.registerNewVTypes();
//    }

});

