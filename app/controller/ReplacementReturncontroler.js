Ext.define('singer.controller.ReplacementReturncontroler', {
    extend: 'Ext.app.Controller',
    views: [
        'DeviceIssueModule.ReplacementReturn.ReplacementReturnGrid',
        'DeviceIssueModule.ReplacementReturn.AddNewForm',
        'DeviceIssueModule.ReplacementReturn.EditForm'
    ],
    stores: [
        'ReplacementReturnStore', 'LoggedInUserData'
    ],
    models: [
        'Replacement&ReturnModel'
    ],
    refs: [
        {
            ref: 'RR_grid',
            selector: 'replacmentReturnGrid'
        },
        {
            ref: 'AddNewForm',
            selector: 'addNWform'
        },
        {
            ref: 'EditForm',
            selector: 'editform'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'replacmentReturnGrid': {
                added: this.initializeGrid
            },
            ' replacmentReturnGrid button[action=add_new]': {
                click: this.loadInToAddNewWindow
            },
            'addNWform button[action=cancel]': {
                click: this.insertFormCancel
            },
            'editform button[action=cancel_edit]': {
                click: this.insertFormCancel
            },
            'addNWform button[action=create]': {
                click: this.insertRecordNewRR
            },
            'replacmentReturnGrid   ': {
                editRR: this.onEdit
            },
            'combobox[action=refChange]': {
                change: this.onSelect
            },
            'combobox[action=editRefChange]': {
                change: this.onSelectRefEdit
            },
            'replacmentReturnGrid ': {
                del: this.onRowDelete
            },
            'replacmentReturnGrid button[action=clearsearch]': {
                click: this.onClearSearch
            },
            'editform button[action=save]': {
                click: this.updataRecord
            },
            ' addNWform button[action=printRR]': {
                click: this.print_repalcementReturn
            },
             ' replacmentReturnGrid   ': {
                aprove: this.on_approve
            },
        });
    },
    on_approve:function(btn,rec){
       //console.log(rec.data);
       //console.log("approve");
       var me = this;
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var store = this.getReplacementReturnStoreStore();
        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        //console.log(bisId);
        rec.data.distributerId =bisId;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/approveReplasementReturn',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(rec.data),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    //console.log(responseData);
                    store.load();
                    Ext.Msg.alert("Approved", responseData.returnMsg);
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                }

            }
           
        });
    },
    
    print_repalcementReturn: function () {
        var rrnumber = Ext.getCmp('exchangeRRnum').getValue();
        //console.log(rrnumber);
        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=repalcementReturn.rptdesign&RRnumber=" + rrnumber;
        window.location.href = reporturl;
    },

    onSelect: function (btn, value) {
//        //console.log("comming to onselect of ADD new");
        var rrValuesStore = btn.getStore(); //Ext.getStore('exchangeRfnStor');
//        //console.log(rrValuesStore);
        var temp = rrValuesStore.findExact('exchangeReferenceNo', value);
        if (temp !== -1) {
//            //console.log(rrValuesStore.getAt(temp).raw);
            var q = rrValuesStore.getAt(temp).raw.imeiNo;
            var WO_num = rrValuesStore.getAt(temp).raw.workOrderNo;
            Ext.getCmp('ime').setValue(q);
            Ext.getCmp('ReplacemetReturn_ADD_New_wonum').setValue(WO_num);
        }

    },

    onSelectRefEdit: function (btn, value) {
        var rrValuesStore = btn.getStore(); //Ext.getStore('exchangeRfnStor');
        var temp = rrValuesStore.findExact('exchangeReferenceNo', value);
        if (temp !== -1) {
            var imeVal = rrValuesStore.getAt(temp).data.imeiNo;
            Ext.getCmp('editIME').setValue(imeVal);
        }else{
            //console.log('error', value)
        }
    },

    onEdit: function (view, record) {
        ////console.log(record);
        var did = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');

        this.getController('CoreController').openUpdateWindow(record, 'editform', 'Edit details of Replacement & Return No:' + record.get('replRetutnNo'));

        var form = this.getEditForm().down('form').getForm();
        form.findField('replRetutnNo').setDisabled(true);
        form.findField('distributerId').setValue(did);
    },

    updataRecord: function (btn) {
        this.getController('CoreController').onRecordUpdate(btn, this.getReplacementReturnStoreStore(), ' Data Updated.', 'Updating  Data.....', true, true, this.getRR_grid());
    },

    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },

    onRowDelete: function (btn, record) {
        //        ////console.log(record);
        Ext.Msg.confirm("Confirmation", "Do you want to delete?", function (btnText) {
            if (btnText === "yes") {
                var store = this.getReplacementReturnStoreStore();
                var x = record.data;
                var loginData = this.getReplacementReturnStoreStore();
                var sys = singer.AppParams.SYSTEM_CODE;
                Ext.Msg.wait('Please wait', 'Deleting Data...', {
                    interval: 300
                });
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/deleteReplasementReturn',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(x),
                    success: function (response) {
                        Ext.Msg.hide();
                        var responseData = Ext.decode(response.responseText);
                        //////console.log(responseData);
                        if (responseData.returnFlag === '1') {
                            //                    btn.up().up('form').up('window').destroy();
                            //////console.log('record deleted');
                        }
                        //                    
                        //                form.destroy();
                        //                parentStore.load();
                        store.load();
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else if (btnText === "no") {}
        }, this);

    },

    initializeGrid: function (grid) {
        var me = this;
        var store = this.getReplacementReturnStoreStore();
        this.getController('CoreController').loadBindedStore(store);
    },

    loadInToAddNewWindow: function (btn) {
        
        //console.log("THIS");
        
        
        Ext.create('Ext.data.Store', {
            //            model: 'singer.model.DeviceExchange',
            id: 'exchangeRfnStore',
            // model: 'singer.model.Replacement&ReturnModel',
            fields: [
                'exchangeReferenceNo',
                'imeiNo'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                // url: singer.AppParams.JBOSS_PATH + 'ReplacementReturns/getReturnReplacement',
                url: singer.AppParams.JBOSS_PATH + 'DeviceExchanges/getDeviceExchangeDetails',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });
        

        var did = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');

        var win = Ext.widget('addNWform');
        var formWindow = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [win]
        });
        formWindow.show();

        var form = this.getAddNewForm().down('form').getForm();
        form.findField('distributerId').setValue(did);

    },


    insertFormCancel: function (btn) {
        var frm = btn.up('window');
        frm.close();
    },

    insertRecordNewRR: function (btn, record) {
        var store = this.getReplacementReturnStoreStore();
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Record Created.', 'Creating New Record', true, true, this.getRR_grid(), false);
        //        var grid = this.getReplacementReturnGrid();
    }
});