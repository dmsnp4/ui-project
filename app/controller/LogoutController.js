
Ext.define('singer.controller.LogoutController', {
    extend: 'Ext.app.Controller',
    views: ['Administrator.login.buttons.LogoutButton',
        'Administrator.login.ChangePassword'
    ],
    require: ['singer.model.User'],
    models: ['LoggedInUser'],
    stores: ['LoggedInUserData'],
    refs: [{
            ref: 'Change_Pw',
            selector: 'changepassword'
        }],
    init: function () {
        this.control({
            'appheadercontainer menuitem[action=logout]': {
                click: this.logoutCheck
            }, 'appheadercontainer menuitem[action=password_change]': {
                click: this.opnChangePasswordWindow
            },
            'changepassword button[action=submit]': {
                click: this.onSubmitChange
            }
        });
    },
    onSubmitChange: function (btn) {

//        //console.log(btn);
        //////console.log("coming to onSubmitChange");
//        Ext.Msg.confirm("Change the password?", "Are you sure you want to change the password ?", function(btn) {
//            if (btn === "yes") {
//                Ext.Ajax.request({
//                    url: singer.AppParams.BASE_PATH + 'Users/changePassword',
//                  //  params: "destroySession=true",
//                    method: "POST",
//                    timeout: 10000,
//                    success: function(response, options) {
//                        
//                    },
//                    failure: function(response, options) {
//                        
//                    }
//                });
//
//            }
//        }); 

        ///////////////////////////
        var form = btn.up('form').getForm();
//        //console.log();
        var newPass = form.getValues().newPassword;
        var pass = form.getValues().password;
        var store = this.getLoggedInUserDataStore();
        var newPass2 = form.findField('txt_newPIN').getValue();
        if (newPass === newPass2) {
            var user = Ext.create('singer.model.LoggedInUser',
                    store.data.items[0]
                    );
            user = store;
            user.data.items[0].set('newPassword', newPass);
            user.data.items[0].set('password', pass);
            var temp;
            store.data.each(function (item) {
//             allUserStore.add(item);
//             userGroupStore.remove(item);
//             item.data.extraParams = 0;
//             temp.push(item.data);
                temp = item.data;
            });

//        //console.log(user.data.items);
//        //console.log(temp);
            var loginData = this.getLoggedInUserDataStore();
            var sys = singer.AppParams.SYSTEM_CODE;
            Ext.Msg.wait('Please wait', 'changing Users Password...', {interval: 300});

            var chngePasswrdDetails = user.data.items;
//        var previousPassword = btn.up('form').getForm().findField('password').getValue();
//        var newPasswrd = btn.up('form').getForm().findField('newPassword').getValue();
			form.reset();
            Ext.Ajax.request({
                // url: singer.AppParams.JBOSS_PATH + 'Users/resetUserPassword?userId=' + userID + '&email=' + email + '',
                url: singer.AppParams.JBOSS_PATH + 'Users/changePassword',
                method: 'POST',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(temp),
                success: function (response) {
                    Ext.Msg.hide();
                    var responseData = Ext.decode(response.responseText);
//                //console.log(responseData);
                    if (responseData.flag === '100') {
//                    btn.up('form').up('window').destroy();
                        Ext.Msg.alert('Password Updated', 'Your Password Updated Successfully');
                    } else {
                        Ext.Msg.alert("Error in changing password", 'Please Use A New Password');
                    }
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in changing  password", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert("Password Update Failed", "Incorrect Current Password");
        }
//        //console.log(store.data.items[0]);
//        //console.log();

    },
//    onResetPass: function (btn) {
//        var userID = btn.up('form').getForm().findField('userId').getValue();
//        var email = btn.up('form').getForm().findField('email').getValue();
//
//        var loginData = this.getLoggedInUserDataStore();
//        Ext.Msg.wait('Please wait', 'Resetting Users Password...', {interval: 300});
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'Users/resetUserPassword?userId=' + userID + '&email=' + email + '',
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
////            params: JSON.stringify(x),
//            success: function (response) {
//                Ext.Msg.hide();
//                var responseData = Ext.decode(response.responseText);
//                //console.log(responseData);
//                if (responseData.flag === '100') {
////                    btn.up('form').up('window').destroy();
//                    Ext.Msg.alert('Success',responseData.successMessages[0]);
//                }
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in resetting password", "Please try again later");
//            }
//        });
//    },

    logoutCheck: function (btn) {
        var lastloggedstore = this.getLoggedInUserDataStore();
        var mainController = this.getController('Main');
        Ext.Msg.confirm("Sign Out from System?", "Are you sure you want to sign out from the system ?", function (btn) {
            if (btn === "yes") {
                document.location.hash = "";
//                //console.log(singer.ParamController.SESSION_CHECKER);
                var session = singer.ParamController.SESSION_CHECKER;
                var task = singer.ParamController.TASKER;
                task.stop(session);
//                session.close();
//                Ext.util.TaskRunner().stop(session);
                Ext.Ajax.request({
                    url: singer.AppParams.BASE_PATH + '/sessionHolder.jsp',
                    params: "destroySession=true",
                    method: "GET",
                    timeout: 10000,
                    success: function (response, options) {
                        lastloggedstore.removeAll();
                        mainController.createLogoutView();
                    },
                    failure: function (response, options) {
                        ////////console.log('cancel');
                    }
                });

            }
        });
    },
    opnChangePasswordWindow: function () {
        var grid = Ext.widget('changepassword');
        var win = Ext.create("Ext.window.Window", {
            width: 500,
//            height: 200,
            layout: 'fit',
            modal: true,
            constrain: true,
            title: 'Change Password',
            items: [grid]
        });
        win.show();
    },
    performSilentLogOut: function () {
        Ext.Ajax.request({
            url: singer.AppParams.BASE_PATH + '/sessionHolder.jsp',
            params: "destroySession=true",
            method: "GET",
            timeout: 10000,
            success: function (response, options) {
                ////////console.log('auto logged out');
            },
            failure: function (response, options) {
                ////////console.log('cancel');
            }
        });
    }

});
