


Ext.define('singer.controller.WorkOrderTrancefer', {
    extend: 'Ext.app.Controller',
    alias: 'widget.wrkOrderTranscontroller',
    controllers: ['CoreController'],
    stores: [
        'GetWorkOrderTrancefer', 'LoggedInUserData'
    ],
    views: [
        'ServiceModule.WorkOrderTrancefer.MainGrid',
        'ServiceModule.WorkOrderTrancefer.AddNew',
        'ServiceModule.WorkOrderTrancefer.ViewTrancefer'
    ],
    temp: null,
    printWOnum: null,
    refs: [
        {
            ref: 'MainGrid',
            selector: 'wrkOrdrTranceferGrid'
        },
        {
            ref: 'AddNew',
            selector: 'addNewWorkOrderTrancefer'
        },
        {
            ref: 'ViewTrancefer',
            selector: 'tranceferView'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'wrkOrdrTranceferGrid': {
                added: this.initializeGrid
            },
            'wrkOrdrTranceferGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'wrkOrdrTranceferGrid  ': {
                onView: this.onView
            },
            'wrkOrdrTranceferGrid button[action=addNew]': {
                click: this.openAddNew
            },
            'tranceferView button[action=view_wrk_order_cancel]': {
                click: this.closeTrancefer
            },
            'addNewWorkOrderTrancefer button[action=addNewSubmit]': {
                click: this.SubmitNewWorkOrderTrancefer
            },
            'addNewWorkOrderTrancefer button[action=work_order_transfer_cancel]': {
                click: this.closeTrancefer
            },
            'editWorkOrderTrancefer button[action=work_order_transfer_cancel]': {
                click: this.closeTrancefer
            },
            'tranceferView button[action=print_trnacfer]': {
                click: this.PrintWorkOrderTransfer
            },
            ' wrkOrdrTranceferGrid ': {
                accept: this.AcceptWOtransfer
            },
            '  wrkOrdrTranceferGrid ': {
                returnn: this.ReturnWOtransfer
            },
            ' addNewWorkOrderTrancefer button[action=returnTransfer]': {
                click: this.ReturnWOrkOrder
            }
        });
    },
    ReturnWOrkOrder: function (btn) {
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form').getValues();
        delete form.courierEmail2;
        var store = this.getGetWorkOrderTranceferStore();
        var editBtn=Ext.getCmp('addNewWorkOrderTrancefer-Return');
        editBtn.setDisabled(true);


        var mod = Ext.create('singer.model.WorkOrderTrancefer',
                form);
        mod.set('tansferLocationId', loginData.data.getAt(0).get('extraParams'));
        //console.log(mod);

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/returnWorkOrderTransfer',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(mod.data),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
                    editBtn.setDisabled(false);
                    window.destroy();
                    store.load();
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                    editBtn.setDisabled(false);
                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    AcceptWOtransfer: function (btn, record) {
        console.log(record);
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var store = this.getGetWorkOrderTranceferStore();
        var me = this;

//        if (record.data.status === 1 || record.data.status === 4) {

        if (record.data.status === 3) {
            Ext.Msg.confirm('Accept ', 'Are you sure you want to accept the Work Order?', function (btn) {
                if (btn === 'yes') {
                    var dataobj = record.data;
                    record.set("status", 4);
                    Ext.Ajax.request({
                        url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/editWorkOrderTransfer',
                        method: 'POST',
                        headers: {
                            userId: loginData.data.getAt(0).get('userId'),
                            room: loginData.data.getAt(0).get('room'),
                            department: loginData.data.getAt(0).get('department'),
                            branch: loginData.data.getAt(0).get('branch'),
                            countryCode: loginData.data.getAt(0).get('countryCode'),
                            division: loginData.data.getAt(0).get('division'),
                            organization: loginData.data.getAt(0).get('organization'),
                            system: sys,
                            'Content-Type': 'application/json'
                        },
                        params: JSON.stringify(dataobj),
                        success: function (response, btn) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.returnFlag === '1') {
                                singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
                                store.load();
                            } else {
                                singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                            }
                        },
                        failure: function () {
                            Ext.Msg.hide();
                            Ext.Msg.alert("Error in connection", "Please try again later");
                        }
                    });
                }
            });
        } else if (record.data.status === 1) {
            Ext.Msg.confirm('Accept ', 'Are you sure you want to accept the Work Order?', function (btn) {
                if (btn === 'yes') {
                    record.set("status", 2);
                    var dataobj = record.data;
                    Ext.Ajax.request({
                        url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/editWorkOrderTransfer',
                        method: 'POST',
                        headers: {
                            userId: loginData.data.getAt(0).get('userId'),
                            room: loginData.data.getAt(0).get('room'),
                            department: loginData.data.getAt(0).get('department'),
                            branch: loginData.data.getAt(0).get('branch'),
                            countryCode: loginData.data.getAt(0).get('countryCode'),
                            division: loginData.data.getAt(0).get('division'),
                            organization: loginData.data.getAt(0).get('organization'),
                            system: sys,
                            'Content-Type': 'application/json'
                        },
                        params: JSON.stringify(dataobj),
                        success: function (response, btn) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.returnFlag === '1') {
                                singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
                                store.load();
                            } else {
                                singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                            }
                        },
                        failure: function () {
                            Ext.Msg.hide();
                            Ext.Msg.alert("Error in connection", "Please try again later");
                        }
                    });
                }
            });
        }

//        }
    },
    ReturnWOtransfer: function (grid, record) {
        console.log(record.data);



        var win = Ext.widget('addNewWorkOrderTrancefer');
        var formWindow = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Return ',
            items: [win]
        });

        formWindow.show();
        var form = formWindow.down('form');
        form.getForm().loadRecord(record);
        //console.log(form.getForm().loadRecord(record));

        var transferDate = new Date(record.data.transferDate);
        form.getForm().findField('transferDate').setValue(transferDate);
        form.getForm().findField('transferDate').setFieldLabel('Return Date');
        ///////////
        form.getForm().findField('bisId').setValue(record.get('tansferLocationId'));
        //        form.getForm().findField('bisId').readOnly = true;
        form.getForm().findField('bisId').hide();
        form.getForm().findField('workOrderNo').readOnly = true;
        form.getForm().findField('deleveryType').setFieldLabel('Return Deliver Type');

        form.getForm().findField('transferReason').setValue('');
        form.getForm().findField('deleveryType').setValue('');
        form.getForm().findField('deleverDetils').setValue('');
        form.getForm().findField('transStatus').setValue(3).readOnly = true;
        //////////////////////////////////
        form.getForm().findField('transferDate').hide();
        form.getForm().findField('transferDate').disable();

        form.getForm().findField('transferReason').hide();
        form.getForm().findField('transferReason').disable();

        form.getForm().findField('deleverDetils').hide();
        form.getForm().findField('deleverDetils').disable();

//        form.getForm().findField('status').setValue(3);
        if (record.data.status === 2) {
            console.log('coming to if status =2...');
            form.getForm().findField('status').setValue(3);
        }else if (record.data.status === 4) {
            console.log('coming to if status =4...');
            form.getForm().findField('status').setValue(1);
            form.getForm().findField('transferLocationDesc').hide();
            form.getForm().findField('transferLocationDesc').disable();
            
            form.getForm().findField('bisId').show();
            form.getForm().findField('returnReason').setValue('');
            form.getForm().findField('returnDeleveryDetails').setValue('');
        }
        ////////////////////////////////////
        Ext.getCmp('addNewWorkOrderTrancefer-transfer').hide();
        this.temp = 2;
        win.show();


//          else if(){
//              
//          }

//        if(record.data.status===2){
//            console.log('coming to IF......');
//            var win = Ext.widget('addNewWorkOrderTrancefer');
//            var formWindow = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Transfer to new service center ',
//            items: [win]
//        });
//        
//        formWindow.show();
//        var form = formWindow.down('form');
//        form.getForm().loadRecord(record);
//        //console.log(form.getForm().loadRecord(record));
//        
//        var transferDate = new Date(record.data.transferDate);
////        form.getForm().findField('transferDate').setValue(transferDate);
////        form.getForm().findField('transferDate').setFieldLabel('Transfer Date');
//        ///////////
//        form.getForm().findField('transferLocationDesc').hide();
//        form.getForm().findField('bisId').setValue('');
////        form.getForm().findField('bisId').readOnly = true;
//        form.getForm().findField('bisId').show();
//        form.getForm().findField('workOrderNo').readOnly = true;
//        form.getForm().findField('deleveryType').setFieldLabel('Transfer Deliver Type');
//
//        form.getForm().findField('transferReason').setValue('');
//        form.getForm().findField('deleveryType').setValue('');
//        form.getForm().findField('deleverDetils').setValue('');
//        form.getForm().findField('transStatus').setValue(3).readOnly = true;
//        //////////////////////////////////
//        form.getForm().findField('transferDate').show();
//        form.getForm().findField('returnDate').hide();
//        form.getForm().findField('returnDate').disable();
//        
//        form.getForm().findField('transferReason').show();
//        form.getForm().findField('returnReason').hide();
//        form.getForm().findField('returnReason').disable();
//        
//        form.getForm().findField('deleverDetils').show();
//        form.getForm().findField('returnDeleveryDetails').hide();
//        form.getForm().findField('returnDeleveryDetails').disable();
//       
//        ////////////////////////////////////
//        Ext.getCmp('addNewWorkOrderTrancefer-transfer').show();
//        Ext.getCmp('addNewWorkOrderTrancefer-Return').hide();
//        this.temp = 2;
//        win.show();
//        
//        
//        
//        }else{
//            
//            console.log('coming to .........ELSE');
//            var win = Ext.widget('addNewWorkOrderTrancefer');
//            var formWindow = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Return ',
//            items: [win]
//        });
//        
//        formWindow.show();
//        var form = formWindow.down('form');
//        form.getForm().loadRecord(record);
//        //console.log(form.getForm().loadRecord(record));
//        
//        var transferDate = new Date(record.data.transferDate);
//        form.getForm().findField('transferDate').setValue(transferDate);
//        form.getForm().findField('transferDate').setFieldLabel('Return Date');
//        ///////////
//        form.getForm().findField('bisId').setValue(record.get('tansferLocationId'));
////        form.getForm().findField('bisId').readOnly = true;
//        form.getForm().findField('bisId').hide();
//        form.getForm().findField('workOrderNo').readOnly = true;
//        form.getForm().findField('deleveryType').setFieldLabel('Return Deliver Type');
//
//        form.getForm().findField('transferReason').setValue('');
//        form.getForm().findField('deleveryType').setValue('');
//        form.getForm().findField('deleverDetils').setValue('');
//        form.getForm().findField('transStatus').setValue(3).readOnly = true;
//        //////////////////////////////////
//        form.getForm().findField('transferDate').hide();
//        form.getForm().findField('transferDate').disable();
//        
//        form.getForm().findField('transferReason').hide();
//        form.getForm().findField('transferReason').disable();
//        
//        form.getForm().findField('deleverDetils').hide();
//        form.getForm().findField('deleverDetils').disable();
//        
//       
//        ////////////////////////////////////
//        Ext.getCmp('addNewWorkOrderTrancefer-transfer').hide();
//        this.temp = 2;
//        win.show();
//        }

    },
    PrintWorkOrderTransfer: function (btn, record) {
        //console.log(this.printWOnum);
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=WOTRANSFERFINAL.rptdesign&WOnum=" + this.printWOnum, 'Transfer ');
    },
    SubmitNewWorkOrderTrancefer: function (btn) {
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form').getValues();
        delete form.courierEmail2;
        var store = this.getGetWorkOrderTranceferStore();
        
        var submitBtn=Ext.getCmp('addNewWorkOrderTrancefer-transfer');
        submitBtn.setDisabled(true);

        var mail = Ext.getCmp('curiermail1').getValue();
        var RetypeMail = Ext.getCmp('curiermail2').getValue();
        //        //console.log(form);

        var mod = Ext.create('singer.model.WorkOrderTrancefer',
                form);
        mod.set('tansferLocationId', loginData.data.getAt(0).get('extraParams'));
        //console.log(mod)
        var deliverTypeVal = form.deleveryType;
        if (deliverTypeVal === 2) {
            if (mail === RetypeMail) {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/addWorkOrderTransfer',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(mod.getData()),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        if (responseData.returnFlag === '1') {
                            ////console.log(responseData)
                            singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                            window.close();
                            store.load();
                            submitBtn.setDisabled(false);
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                            submitBtn.setDisabled(true);
                        }

                    },
                    failure: function () {
                        Ext.Msg.hide();
                        submitBtn.setDisabled(true);
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else {
                Ext.Msg.alert("Error ", "Email values not match");
            }
        } else {
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/addWorkOrderTransfer',
                method: 'PUT',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(mod.getData()),
                success: function (response, btn) {
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
                        ////console.log(responseData)
                        singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                        window.close();
                        store.load();
                    } else {
                        singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                    }

                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        }
    },
    closeTrancefer: function (btn) {
        var frm = btn.up('window');
        frm.close();
    },
    openAddNew: function () {
        var form = Ext.widget('addNewWorkOrderTrancefer');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        //console.log('open');

        Ext.getCmp('addNewWorkOrderTrancefer-Return').hide();
        form.getForm().findField('transStatus').setValue(1).readOnly = true;
        form.getForm().findField('transferLocationDesc').hide();
        form.getForm().findField('returnDate').disable();
        form.getForm().findField('returnReason').disable();
        form.getForm().findField('returnDeleveryDetails').disable();
        form.getForm().findField('returnDate').hide();
        form.getForm().findField('returnReason').hide();
        form.getForm().findField('returnDeleveryDetails').hide();
        this.temp = 1;
        win.show();
    },
    initializeGrid: function (grid) {
        var store = this.getGetWorkOrderTranceferStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        //console.log(record);
        this.temp = 2;
        this.printWOnum = record.data.workOrderNo;
        //console.log(this.printWOnum);
        var curierNum = record.data.courierEmail;
        this.getController('CoreController').onOpenViewMoreWindow(record, 'tranceferView', 'View');
        if (curierNum !== '') {
            Ext.getCmp('trancferEmail').show();
        }
    },
    //    onDeletee: function (btn, record) {
    //        //        ////console.log(record);
    //        Ext.Msg.confirm("Confirmation", "Do you want to delete?", function (btnText) {
    //            if (btnText === "yes") {
    //                var store = this.getGetWorkOrderTranceferStore();
    //                var x = record.data;
    //                var loginData = this.getLoggedInUserDataStore();
    //                var sys = singer.AppParams.SYSTEM_CODE;
    //                Ext.Msg.wait('Please wait', 'Deleting Data...', {
    //                    interval: 300
    //                });
    //                Ext.Ajax.request({
    //                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/deleteWorkOrderTransfer',
    //                    method: 'POST',
    //                    headers: {
    //                        userId: loginData.data.getAt(0).get('userId'),
    //                        room: loginData.data.getAt(0).get('room'),
    //                        department: loginData.data.getAt(0).get('department'),
    //                        branch: loginData.data.getAt(0).get('branch'),
    //                        countryCode: loginData.data.getAt(0).get('countryCode'),
    //                        division: loginData.data.getAt(0).get('division'),
    //                        organization: loginData.data.getAt(0).get('organization'),
    //                        system: sys,
    //                        'Content-Type': 'application/json'
    //                    },
    //                    params: JSON.stringify(x),
    //                    success: function (response) {
    //                        Ext.Msg.hide();
    //                        var responseData = Ext.decode(response.responseText);
    //                        //////console.log(responseData);
    //                        if (responseData.returnFlag === '1') {}
    //                        store.load();
    //                    },
    //                    failure: function () {
    //                        Ext.Msg.hide();
    //                        Ext.Msg.alert("Error in connection", "Please try again later");
    //                    }
    //                });
    //            } else if (btnText === "no") {}
    //        }, this);
    //    },

    //    SubmitEdit: function (btn) {
    //        //console.log('SubmitEdit');
    //        var window = btn.up('window');
    //        var loginData = this.getLoggedInUserDataStore();
    //        var sys = singer.AppParams.SYSTEM_CODE;
    //        var form = btn.up('form').getValues();
    //        delete form.courierEmail2;
    //        var store = this.getGetWorkOrderTranceferStore();
    //        //        var formData = btn.up('form').getValues();
    //        //        var user = Ext.create('singer.model.ImeiAdj',
    //        //                form.getValues()
    //        //                );
    //
    //        Ext.Ajax.request({
    //            url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/editWorkOrderTransfer',
    //            method: 'POST',
    //            headers: {
    //                userId: loginData.data.getAt(0).get('userId'),
    //                room: loginData.data.getAt(0).get('room'),
    //                department: loginData.data.getAt(0).get('department'),
    //                branch: loginData.data.getAt(0).get('branch'),
    //                countryCode: loginData.data.getAt(0).get('countryCode'),
    //                division: loginData.data.getAt(0).get('division'),
    //                organization: loginData.data.getAt(0).get('organization'),
    //                system: sys,
    //                'Content-Type': 'application/json'
    //            },
    //            params: JSON.stringify(form),
    //            success: function (response, btn) {
    //                var responseData = Ext.decode(response.responseText);
    //                if (responseData.returnFlag === '1') {
    //                    singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
    //                    window.destroy();
    //                    store.load();
    //                } else {
    //                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
    //                }
    //
    //            },
    //            failure: function () {
    //                Ext.Msg.hide();
    //                Ext.Msg.alert("Error in connection", "Please try again later");
    //            }
    //        });
    //    },

    //    EditWorkOrderTrancefer: function (grid, record) {
    //
    //        //console.log(record.data.transferDate);
    //
    //        //        //console.log(loadRecord(record));
    //
    //        var win = Ext.widget('addNewWorkOrderTrancefer');
    //        var formWindow = Ext.create("Ext.window.Window", {
    //            layout: 'fit',
    //            iconCls: 'icon-verification',
    //            modal: true,
    //            constrain: true,
    //            title: 'Edit ',
    //            items: [win]
    //        });
    //        ////console.log(record.data);
    //        formWindow.show();
    //        var form = formWindow.down('form');
    //        form.getForm().loadRecord(record);
    //
    //        var transferDate = new Date(record.data.transferDate);
    //        form.getForm().findField('transferDate').setValue(transferDate);
    //
    //        Ext.getCmp('addNewWorkOrderTrancefer-create').hide();
    //        this.temp = 2;
    //        win.show();
    //    },



    //    searchWorkOrderNo: function (btn, record) {
    //        var wrkNo = Ext.getCmp('wrkONm').getValue();
    //        ////console.log(wrkNo);
    //
    //        var window = btn.up('window');
    //        var loginData = this.getLoggedInUserDataStore();
    //        var form = btn.up('form');
    //        Ext.Ajax.request({
    //            url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/verifyWorkOrder?workOrderNo=' + wrkNo,
    //            method: 'GET',
    //            headers: {
    //                userId: loginData.data.getAt(0).get('userId'),
    //                room: loginData.data.getAt(0).get('room'),
    //                department: loginData.data.getAt(0).get('department'),
    //                branch: loginData.data.getAt(0).get('branch'),
    //                countryCode: loginData.data.getAt(0).get('countryCode'),
    //                division: loginData.data.getAt(0).get('division'),
    //                organization: loginData.data.getAt(0).get('organization'),
    //                'Content-Type': 'application/json'
    //            },
    //            success: function (response, btn) {
    //                var responseData = Ext.decode(response.responseText);
    //                if (responseData.returnFlag === '1') {
    //                    Ext.getCmp('veryfy').show();
    //                } else {
    //                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
    //                    Ext.getCmp('wrkONm').reset();
    //                    Ext.getCmp('veryfy').hide();
    //                }
    //            },
    //            failure: function () {
    //                Ext.Msg.hide();
    //                Ext.Msg.alert("Error in connection", "Please try again later");
    //            }
    //        });
    //    },
});