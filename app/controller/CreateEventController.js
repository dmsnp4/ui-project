Ext.define('singer.controller.CreateEventController', {
    extend: 'Ext.app.Controller',
    //	alias: 'widget.repaircategorycontroller',
    controllers: ['CoreController'],
    stores: [
        'CreateEvent', 'CreateEventBisTypes', 'EventUsers', 'LoggedInUserData',
        //stores loading grids without web services
        'EventRules', 'EventNonSales', 'EventAwards'
    ],
    views: [
        'Events.CreateEvent.CreateEventGrid',
        'Events.CreateEvent.CreateEventForm',
        'Events.CreateEvent.ChangeParticipants',
        'Events.CreateEvent.AddEventRules',
        'Events.CreateEvent.PointScheme',
        'Events.CreateEvent.NonSales',
        'Events.CreateEvent.ViewEvent',
        'Events.CreateEvent.NonSalesView',
        'Events.CreateEvent.AwardDetail',
        'Events.CreateEvent.AwardDetailView',
        'Events.CreateEvent.ViewSalesRules'
    ],
    models: [
        'EventRulePointScheme'
    ],
    EventMaster: [],
    EventAward: [],
    EventNonSalesRule: [],
    EventSalesRule: [],
    EventRulePointScheme: [],
    EventBisType: [],
    EventBussinessList: [],
    EventUsers: [],
    temp: null,
    schemeClick: false,
    refs: [
        {
            ref: 'CreateEventGrid',
            selector: 'createeventgrid'
        },
        {
            ref: 'CreateEventForm',
            selector: 'neweventform'
        },
        {
            ref: 'ChangeParticipants',
            selector: 'changeparticipants'
        },
        {
            ref: 'AddEventRules',
            selector: 'addeventrules'
        },
        {
            ref: 'PointScheme',
            selector: 'pointscheme'
        },
        {
            ref: 'NonSales',
            selector: 'nonsales'
        },
        {
            ref: 'ViewEvent',
            selector: 'viewevent'
        },
        {
            ref: 'NonSalesView',
            selector: 'nonsalesview'
        },
        {
            ref: 'AwardDetail',
            selector: 'awarddetail'
        },
        {
            ref: 'AwardDetailView',
            selector: 'awarddetailview'
        },
        {
            ref: 'ViewSalesRules',
            selector: 'viewsalesrules'
        }
    ],
    init: function () {
        //        ////////console.log(this);
        var me = this;
        me.control({
            'createeventgrid': {
                added: this.initializeGrid
            },
            ' createeventgrid': {
                onEventEdit: this.onEventEdit
            },
            '  createeventgrid': {
                onEventStart: this.onEventStart
            },
            '   createeventgrid': {
                onEventView: this.onEventView
            },
            'createeventgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'createeventgrid button[action=open_window]': {
                click: this.onNewEvent
            },
            'neweventform button[action=onChangeParticipants]': {
                click: this.onChangeParticipants
            },
            'neweventform button[action=cancel]': {
                click: this.insertFormCancel
            },
            'viewsalesrules button[action=cancel]': {
                click: this.onviewDone
            },
            'nonsalesview button[action=cancel]': {
                click: this.insertFormCancel
            },
            'neweventform button[action=onAddEventRules]': {
                click: this.onAddEventRules
            },
            'neweventform button[action=onAddNonSalesRules]': {
                click: this.onAddNonSalesRules
            },
            'neweventform button[action=onAwardDetail]': {
                click: this.onAwardDetail
            },
            'neweventform button[action=create]': {
                click: this.onNewEventCreate
            },
            'neweventform button[action=save]': {
                click: this.onNewEventSave
            },
            'neweventform  ': {
                createEventEdit: this.onCreateEventEdit
            },
            ' neweventform ': {
                createEventView: this.onCreateEventView
            },
            'neweventform ': {
                createEventDelete: this.onCreateEventDelete
            },
            'neweventform': {
                nonSalesView: this.onNonSalesView
            },
            ' neweventform': {
                nonSalesEdit: this.onNonSalesEdit
            },
            '  neweventform': {
                nonSalesDelete: this.onNonSalesDelete
            },
            '   neweventform': {
                awardView: this.onAwardView
            },
            '    neweventform': {
                awardDelete: this.onAwardDelete
            },
            '     neweventform': {
                awardEdit: this.onAwardEdit
            },
            '     neweventform checkcolumn[action=chChange]': {
                checkchange: this.onChekcChangeParticipants
            },
            'addeventrules button[action=onPointScheme]': {
                click: this.onPointScheme
            },
            'addeventrules button[action=cancel]': {
                click: this.insertFormCancel
            },
            'addeventrules button[action=onSubmit]': {
                click: this.onEventRulesSubmit
            },
            'addeventrules button[action=Save]': {
                click: this.onEventRulesSave
            },
            'changeparticipants button[action=nextOne]': {
                click: this.onNextOne
            },
            'changeparticipants button[action=cancel]': {
                click: this.insertFormCancel
            },
            'changeparticipants button[action=nextAll]': {
                click: this.onNextAll
            },
            'changeparticipants button[action=prevAll]': {
                click: this.onPrevAll
            },
            'changeparticipants button[action=prevOne]': {
                click: this.onPrevOne
            },
            'changeparticipants button[action=create]': {
                click: this.onChangeParticipantsSubmit
            },
            'pointscheme button[action=onEnter]': {
                click: this.onPointSchemeEnter
            },
            'pointscheme button[action=cancel]': {
                click: this.insertFormCancel
            },
            'nonsales button[action=onSubmit]': {
                click: this.onNonSalesOnSubmit
            },
            'nonsales button[action=cancel]': {
                click: this.insertFormCancel
            },
            'nonsales button[action=save]': {
                click: this.onNonSalesOnSave
            },
            'awarddetail button[action=onSubmit]': {
                click: this.onAwardDetailOnSubmit
            },
            'awarddetail button[action=cancel]': {
                click: this.insertFormCancel
            },
            'awarddetail button[action=save]': {
                click: this.onAwardDetailOnSave
            },
            'viewevent button[action=cancel]': {
                click: this.onviewDone
            }
        });
    },
    initializeGrid: function (grid) {
        var store = this.getCreateEventStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onNewEvent: function (cmp) {
        this.getController('CoreController').openInsertWindow('neweventform');
        this.getController('CoreController').loadBindedStore(this.getCreateEventBisTypesStore());
        this.getCreateEventBisTypesStore().removeAll();
        this.getEventRulesStore().removeAll();
        this.getEventNonSalesStore().removeAll();
        this.getEventAwardsStore().removeAll();
        this.getEventUsersStore().removeAll();
    },
    onEventView: function (grid, rec) {
        //        this.getController('CoreController').onOpenViewMoreWindow(record, 'viewevent', 'View');
        //        this.getController('CoreController').loadBindedStore(this.getCreateEventBisTypesStore());
        //        this.getCreateEventBisTypesStore().removeAll();
        //        this.getEventRulesStore().removeAll();
        //        this.getEventNonSalesStore().removeAll();
        //        this.getEventAwardsStore().removeAll();
        //        this.getEventUsersStore().removeAll();



        this.getCreateEventBisTypesStore().removeAll();
        this.getEventRulesStore().removeAll();
        this.getEventNonSalesStore().removeAll();
        this.getEventAwardsStore().removeAll();
        this.getEventUsersStore().removeAll();
        var evntId = rec.data.eventId;
        //form stores
        var eventRules = Ext.getStore('EventRules');
        eventRules.removeAll();
        var eventNonSales = Ext.getStore('EventNonSales');
        eventNonSales.removeAll();
        var eventAwards = Ext.getStore('EventAwards');
        eventAwards.removeAll();
        var asign = this.getEventUsersStore();
        asign.removeAll();

        var endDate = new Date(rec.data.endDate);
        var startDate = new Date(rec.data.startDate);



        var win = Ext.widget('viewevent');
        //        //////console.log(record);
        var bistypeGrid = Ext.getCmp('neweventform-EventBisTypes');

        var form = win.down('form');
        form.getForm().loadRecord(rec);
        form.getForm().findField('startDate').setValue(startDate);
        form.getForm().findField('endDate').setValue(endDate);

        this.getController('CoreController').loadBindedStore(this.getCreateEventBisTypesStore());

        //load data from service 
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Loading Data...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'EventMasters/getEventMasterDetails?eventId=' + evntId + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            //            params: JSON.stringify(mod.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData);

                //set check for bistype


                for (i = 0; i < responseData.data[0].eventSalesRuleList.length; i++) {
                    eventRules.add(responseData.data[0].eventSalesRuleList[i]);
                }
                for (i = 0; i < responseData.data[0].eventNonSalesRuleList.length; i++) {
                    eventNonSales.add(responseData.data[0].eventNonSalesRuleList[i]);
                }
                for (i = 0; i < responseData.data[0].evenAwardList.length; i++) {
                    eventAwards.add(responseData.data[0].evenAwardList[i]);
                }
                bistypeGrid.getStore().removeAll();
                for (i = 0; i < responseData.data[0].eventBisTypeList.length; i++) {
                    //                    eventAwards.add(responseData.data[0].eventBisTypeList[i]);
                    //                    var bis = responseData.data[0].eventBisTypeList[i].bisStruTypeId;
                    //                    var res = bistypeGrid.getStore().findExact('bisStruTypeId', bis);
                    //                    if (res !== -1) {
                    //                        bistypeGrid.getStore().removeAt(res);
                    //                        responseData.data[0].eventBisTypeList[i].chCol = true;
                    //                        bistypeGrid.getStore().add(responseData.data[0].eventBisTypeList[i]);
                    //                    }
                    bistypeGrid.getStore().add(responseData.data[0].eventBisTypeList[i]);
                }
                for (i = 0; i < responseData.data[0].eventBisTypeList.length; i++) {
                    asign.add(responseData.data[0].eventBisTypeList[i].eventBussinessList);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });



        win.show();
    },
    onChekcChangeParticipants: function (cmp, rowIndex, checked, eOpts) {
        //        var bisTypeGrid = Ext.getCmp('neweventform-EventBisTypes').getStore();



        //        //console.log(bisTypeGrid);
        var bisTypeGrid = Ext.getCmp('neweventform-EventBisTypes').getStore();
        //get ids of selected col from the grid
        var ids = '';
        var temp = [];
        bisTypeGrid.each(function (item) {
            // 			//console.log(item.data);

            if (item.data.chCol) {
                //				//console.log(item.data);
                ids += item.data.bisStruTypeId + ',';
                temp.push(item.data);
            }

        });
        ids = ids.substring(0, ids.length - 1);
        //		//console.log(ids);
        this.EventBisType = temp;

        var asign = this.getEventUsersStore();
        asign.proxy.api.read = this.resetProxy(asign.proxy.api.read);
        asign.proxy.api.read += '?bisTypeList=' + ids + '';
        asign.load();
        //                //console.log(asign.data);
    },
    onChangeParticipants: function (cmp) {
        var win = Ext.widget('changeparticipants');

        //        var bisTypeGrid = Ext.getCmp('neweventform-EventBisTypes').getStore();
        //        //get ids of selected col from the grid
        //        var ids = '';
        //        var temp = [];
        //        bisTypeGrid.each(function (item) {
        //            // 			//console.log(item.data);
        //
        //            if (item.data.chCol) {
        //                //				//console.log(item.data);
        //                ids += item.data.bisStruTypeId + ',';
        //                temp.push(item.data);
        //            }
        //
        //        });
        //        ids = ids.substring(0, ids.length - 1);
        //        //		//console.log(ids);
        //        this.EventBisType = temp;

        //load data to grid
        var asign = this.getEventUsersStore();
        //        asign.proxy.api.read = this.resetProxy(asign.proxy.api.read);
        //        asign.proxy.api.read += '?bisTypeList=' + ids + '';
        var assignGrid = Ext.getCmp('AsssignedUsers');
        assignGrid.bindStore(asign);
        //        asign.load();
        var unAssign = Ext.create('Ext.data.Store', {
            model: 'singer.model.EventBussinessList',
        });
        Ext.getCmp('UnAssinedUsers').bindStore(unAssign);
        //show window
        win.show();
    },
    resetProxy: function (proxyURI) {
        return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
    },
    onAddEventRules: function (cmp) {
        //        var win = Ext.widget('addeventrules');
        //
        //        win.show();
        this.EventRulePointScheme = [];
        this.getController('CoreController').openInsertWindow('addeventrules');
        //console.log(this.EventMaster);
    },
    onPointScheme: function (cmp) {
        var me = this;
        this.schemeClick = true;
        var win = Ext.widget('pointscheme');
        var rec = cmp.up('form').getForm().getRecord();
        //console.log("rec", rec);
        //console.log("array", me.EventRulePointScheme);
        if (me.EventRulePointScheme.length ===0) {
            try {
                win.down('form').getForm().loadRecord(rec)
            } catch (e) {
                //console.log('error:', e);
            }
//            //console.log('rec loaded', win.down('form').getForm());
        }else{
            win.down('form').getForm().setValues(me.EventRulePointScheme);
//            win.down('form').getForm().loadRecord(me.EventRulePointScheme);
        }


        win.show();
    },
    onAddNonSalesRules: function (cmp) {
        //        var win = Ext.widget('nonsales');
        //
        //        win.show();
        this.getController('CoreController').openInsertWindow('nonsales');
    },
    onCreateEventEdit: function (view, record, index) {
        this.getController('CoreController').openUpdateWindow(record, 'addeventrules', 'Edit Event Rules');
        this.temp = index;
    },
    onCreateEventView: function (view, record) {
        //console.log(record)
        this.getController('CoreController').onOpenViewMoreWindow(record, 'viewsalesrules', 'Event Details');
    },
    onCreateEventDelete: function (view, record) {
        Ext.MessageBox.confirm('Delete', 'Are you sure you want to delete this rule?', function (btn) {
            if (btn === 'yes') {
                view.getStore().remove(record);
            }
        });
    },
    onNonSalesView: function (view, record) {
        //        //console.log('view')
        this.getController('CoreController').onOpenViewMoreWindow(record, 'nonsalesview', 'Non Sales Details');
    },
    onNonSalesEdit: function (view, record, index) {
        this.getController('CoreController').openUpdateWindow(record, 'nonsales', 'Non Sales');
        Ext.getCmp('nonsales-salesCombo').setVisible(false);
        this.temp = index;
    },
    onNonSalesDelete: function (view, record) {
        Ext.MessageBox.confirm('Delete', 'Are you sure you want to delete this non-sale rule?', function (btn) {
            if (btn === 'yes') {
                view.getStore().remove(record);
            }
        });

    },
    onAwardDetail: function (cmp) {
        this.getController('CoreController').openInsertWindow('awarddetail');
    },
    onAwardView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'awarddetailview', 'Award Details');
    },
    onAwardDelete: function (view, record) {
        Ext.MessageBox.confirm('Delete', 'Are you sure you want to delete this award data?', function (btn) {
            if (btn === 'yes') {
                view.getStore().remove(record);
            }
        });
    },
    onAwardEdit: function (view, record) {
        this.getController('CoreController').openUpdateWindow(record, 'awarddetail', 'Award Details');
    },
    onNextOne: function (cmp) {
        var left = Ext.getCmp('UnAssinedUsers');
        var right = Ext.getCmp('AsssignedUsers');

        var selectionModel = left.getSelectionModel();
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                left.getStore().remove(record);
                right.getStore().insert(0, record);
            });
        }

    },
    onNextAll: function (cmp) {
        var left = Ext.getCmp('UnAssinedUsers').getStore();
        var right = Ext.getCmp('AsssignedUsers').getStore();

        left.data.each(function (item) {
            left.remove(item);
            right.add(item);
        });
    },
    onPrevOne: function (cmp) {
        var left = Ext.getCmp('UnAssinedUsers');
        var right = Ext.getCmp('AsssignedUsers');

        var selectionModel = right.getSelectionModel();
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                left.getStore().add(record);
                right.getStore().remove(record);
                //                record.data.extraParams = me.localVar;
                //                singer.ParamController.UN_USR.push(record.data);
            });
        }
    },
    onPrevAll: function (cmp) {
        var left = Ext.getCmp('UnAssinedUsers').getStore();
        var right = Ext.getCmp('AsssignedUsers').getStore();

        right.data.each(function (item) {
            left.add(item);
            right.remove(item);
            //            item.data.extraParams = this.localVar;
        });
    },
    onChangeParticipantsSubmit: function (cmp) {
        var store = this.getEventUsersStore();
        var temp = [];
        store.data.each(function (item) {
            // 			//console.log(item)
            temp.push(item.data)
        });
        this.EventBussinessList = temp;
        //        //console.log(this.EventBussinessList);
        cmp.up('window').destroy();

    },
    onPointSchemeEnter: function (cmp) {
        //        		this.EventRulePointScheme
        //console.log(cmp.up('form').getValues());
        var mod = Ext.create('singer.model.EventRulePointScheme',
                cmp.up('form').getValues()
                );
        var flag;
        if (Ext.getCmp('pointscheme-floatFlag').getValue()) {
            flag = 1;
        } else {
            flag = 0;
        }
        mod.set('schmefloatFlag', flag);
        this.EventRulePointScheme = mod.data;
        cmp.up('window').destroy();
        //console.log(this.EventRulePointScheme);
        // this.EventRulePointScheme = cmp.up('form').getValues();
        // 		//console.log(this.EventRulePointScheme);
    },
    onEventRulesSubmit: function (cmp) {
        var form = cmp.up('form');
        var me = this;
        var flag;
        if (Ext.getCmp(form.getForm().findField('pointSchemeFlag').getValue())) {
            flag = 1;
        } else {
            flag = 0;
        }
        var mod = Ext.create('singer.model.EventSalesRule',
                form.getValues()
                );
        var temp = [];
        //console.log(this.EventRulePointScheme);

        temp.push(this.EventRulePointScheme);
        mod.set('eventRulePointSchemaList', temp);
        mod.set('pointSchemeFlag', flag);
        this.EventSalesRule = mod.data;

        //load form data to grid
        var store = this.getEventRulesStore();
        //console.log(mod.data);
        if (!me.schemeClick) {
            mod.data.eventRulePointSchemaList = [];
        }
        store.add(mod.data);
        cmp.up('window').destroy();

    },
    onEventRulesSave: function (cmp) {
        //        var form = cmp.up('form');
        //        //        this.getEventRulesStore().commitChanges();
        //        var store = this.getEventRulesStore();
        //        store.removeAt(this.temp);
        //        store.add(form.getValues());
        //        cmp.up('window').destroy();
        //        //console.log('did');


        var form = cmp.up('form');
        var me = this;
        var flag;
        if (Ext.getCmp(form.getForm().findField('pointSchemeFlag').getValue())) {
            flag = 1;
        } else {
            flag = 0;
        }
        var mod = Ext.create('singer.model.EventSalesRule',
                form.getValues()
                );
        var temp = [];
        //console.log(this.EventRulePointScheme);

        temp.push(this.EventRulePointScheme);
        mod.set('eventRulePointSchemaList', temp);
        mod.set('pointSchemeFlag', flag);
        this.EventSalesRule = mod.data;

        //load form data to grid
        var store = this.getEventRulesStore();
        //console.log(mod.data);
        if (!me.schemeClick) {
            mod.data.eventRulePointSchemaList = [];
        }
        store.removeAt(this.temp);
        store.add(mod.data);
        cmp.up('window').destroy();
    },
    onNonSalesOnSubmit: function (cmp) {
        var form = cmp.up('form');
        var mod = Ext.create('singer.model.EventNonSalesRule',
                form.getValues()
                );
        this.EventNonSalesRule = mod.data;
        //console.log(form.getValues().description);
        //load data to the grid
        var store = this.getEventNonSalesStore();
        var stat = store.findExact('description', form.getValues().description);
        if (stat === -1) {
            store.add(this.EventNonSalesRule);
            cmp.up('window').destroy();
        } else {
            Ext.Msg.alert("Error...", "Already have a rule with that description...");
        }
        //        //console.log(this.EventNonSalesRule);
    },
    onNonSalesOnSave: function (cmp) {
        var form = cmp.up('form');
        //        this.getEventRulesStore().commitChanges();
        var store = this.getEventNonSalesStore();
        store.removeAt(this.temp);
        store.add(form.getValues());
        cmp.up('window').destroy();
    },
    onAwardDetailOnSubmit: function (cmp) {
        var form = cmp.up('form');
        var mod = Ext.create('singer.model.EventAward',
                form.getValues()
                );
        this.EventAward = mod.data;
        //load data to the grid
        var store = this.getEventAwardsStore();
        store.add(this.EventAward);

        cmp.up('window').destroy();
    },
    onAwardDetailOnSave: function (cmp) {
        var form = cmp.up('form');
        //        this.getEventRulesStore().commitChanges();
        var store = this.getEventAwardsStore();
        store.removeAt(this.temp);
        store.add(form.getValues());
        cmp.up('window').destroy();
    },
    onNewEventCreate: function (cmp) {
        var form = cmp.up('form');
        var mod = Ext.create('singer.model.EventMaster',
                form.getValues()
                );
        var awards = [];
        this.getEventAwardsStore().data.each(function (item) {
            awards.push(item.data);
        });
        mod.set('evenAwardList', awards);
        var bisTypeGrid = Ext.getCmp('neweventform-EventBisTypes').getStore();
        var bis = [];
        var asign = this.getEventUsersStore();
        bisTypeGrid.each(function (item) {
            // 			//console.log(item.data);

            if (item.data.chCol) {
                delete item.data.chCol;
                var usrs = [];
                asign.each(function (itm) {
                    if (itm.data.bisTypeId === item.data.bisStruTypeId) {
                        usrs.push(itm.data);
                    }
                    //                    //console.log(itm.data.bisTypeId);
                });
                //                eventBussinessList
                item.data.eventBussinessList = usrs;
                bis.push(item.data);

            }
        });
        mod.set('eventBisTypeList', bis);
        var nonSales = [];
        this.getEventNonSalesStore().data.each(function (item) {
            nonSales.push(item.data);
        });
        mod.set('eventNonSalesRuleList', nonSales);
        var salesRules = [];
        //console.log(this.getEventRulesStore())
        this.getEventRulesStore().data.each(function (item) {
            salesRules.push(item.data);
        });
        mod.set('eventSalesRuleList', salesRules);

        //console.log(mod.data);

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Creating Event ...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'EventMasters/addEventMaster',
            method: 'PUT',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(mod.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                if (responseData.returnFlag === '1') {
                    form.up('window').destroy();
                    Ext.getStore('CreateEvent').load();
                } else {
                    Ext.Msg.alert("Error...", responseData.returnMsg, function (cmp) {
                        //                        form.up('window').destroy();
                    });

                }
                //                    
                // form.destroy();
                // parentStore.load();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onEventEdit: function (grid, rec) {
        //console.log(rec);
        this.getCreateEventBisTypesStore().removeAll();
        this.getEventRulesStore().removeAll();
        this.getEventNonSalesStore().removeAll();
        this.getEventAwardsStore().removeAll();
        this.getEventUsersStore().removeAll();
        var evntId = rec.data.eventId;
        //form stores
        var eventRules = Ext.getStore('EventRules');
        eventRules.removeAll();
        var eventNonSales = Ext.getStore('EventNonSales');
        eventNonSales.removeAll();
        var eventAwards = Ext.getStore('EventAwards');
        eventAwards.removeAll();
        var asign = this.getEventUsersStore();
        asign.removeAll();

        var endDate = new Date(rec.data.endDate);
        var startDate = new Date(rec.data.startDate);



        var win = Ext.widget('neweventform');
        //        //////console.log(record);
        win.setTitle('Edit Event');
        var bistypeGrid = Ext.getCmp('neweventform-EventBisTypes');

        var form = win.down('form');
        form.getForm().loadRecord(rec);
        form.getForm().findField('startDate').setValue(startDate);
        form.getForm().findField('endDate').setValue(endDate);

        this.getController('CoreController').loadBindedStore(this.getCreateEventBisTypesStore());

        //load data from service 
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Loading Data...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'EventMasters/getEventMasterDetails?eventId=' + evntId + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            //            params: JSON.stringify(mod.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData);

                //set check for bistype


                for (i = 0; i < responseData.data[0].eventSalesRuleList.length; i++) {
                    eventRules.add(responseData.data[0].eventSalesRuleList[i]);
                }
                for (i = 0; i < responseData.data[0].eventNonSalesRuleList.length; i++) {
                    eventNonSales.add(responseData.data[0].eventNonSalesRuleList[i]);
                }
                for (i = 0; i < responseData.data[0].evenAwardList.length; i++) {
                    eventAwards.add(responseData.data[0].evenAwardList[i]);
                }
                for (i = 0; i < responseData.data[0].eventBisTypeList.length; i++) {
                    //                    eventAwards.add(responseData.data[0].eventBisTypeList[i]);
                    var bis = responseData.data[0].eventBisTypeList[i].bisStruTypeId;
                    var res = bistypeGrid.getStore().findExact('bisStruTypeId', bis);
                    if (res !== -1) {
                        bistypeGrid.getStore().removeAt(res);
                        responseData.data[0].eventBisTypeList[i].chCol = true;
                        bistypeGrid.getStore().add(responseData.data[0].eventBisTypeList[i]);
                    }
                }
                for (i = 0; i < responseData.data[0].eventBisTypeList.length; i++) {
                    asign.add(responseData.data[0].eventBisTypeList[i].eventBussinessList);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });



        win.show();
        //        win.setTitle(title);
        //        this.focusToFirstFieldinForm(win.down('form'));
        //        Ext.getCmp(alias + '-reset-insert').setVisible(false);
        //        Ext.getCmp(alias + '-reset-update').setVisible(true);
        Ext.getCmp('neweventform' + '-create').setVisible(false);
        Ext.getCmp('neweventform' + '-save').setVisible(true);
    },
    onNewEventSave: function (cmp) {
        var form = cmp.up('form');
        var mod = Ext.create('singer.model.EventMaster',
                form.getValues()
                );
        var awards = [];
        this.getEventAwardsStore().data.each(function (item) {
            awards.push(item.data);
        });
        mod.set('evenAwardList', awards);
        var bisTypeGrid = Ext.getCmp('neweventform-EventBisTypes').getStore();
        var bis = [];
        var asign = this.getEventUsersStore();
        bisTypeGrid.each(function (item) {
            // 			//console.log(item.data);

            if (item.data.chCol) {
                delete item.data.chCol;
                var usrs = [];
                asign.each(function (itm) {
                    if (itm.data.bisTypeId === item.data.bisStruTypeId) {
                        usrs.push(itm.data);
                    }
                    //                    //console.log(itm.data.bisTypeId);
                });
                //                eventBussinessList
                item.data.eventBussinessList = usrs;
                bis.push(item.data);

            }
        });
        mod.set('eventBisTypeList', bis);
        var nonSales = [];
        this.getEventNonSalesStore().data.each(function (item) {
            nonSales.push(item.data);
        });
        mod.set('eventNonSalesRuleList', nonSales);
        var salesRules = [];
        this.getEventRulesStore().data.each(function (item) {
            salesRules.push(item.data);
        });
        mod.set('eventSalesRuleList', salesRules);

        //console.log(mod.data);

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Creating Event ...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'EventMasters/editEventMaster',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(mod.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                if (responseData.returnFlag === '1') {
                    form.up('window').destroy();
                    Ext.getStore('CreateEvent').load();
                } else {
                    Ext.Msg.alert("Error...", responseData.returnMsg, function (cmp) {
                        //                        form.up('window').destroy();
                    });

                }
                //                    
                // form.destroy();
                // parentStore.load();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onEventStart: function (grid, rec) {
        //console.log(rec)

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Creating Event ...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'EventMasters/startEvent',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(rec.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                if (responseData.returnFlag === '1') {
                    Ext.getStore('CreateEvent').load();
                } else {
                    Ext.Msg.alert("Error...", responseData.returnMsg, function (cmp) {
                        //                        form.up('window').destroy();
                    });

                }
                //                    
                // form.destroy();
                // parentStore.load();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onviewDone: function (btn) {
        btn.up('window').close();
    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);

    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    }
});