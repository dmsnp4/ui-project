///**********
//    NOT USING
//******/
//
//Ext.define('singer.controller.WorkOrderOpeningController', {
//    extend: 'Ext.app.Controller',
//    alias: 'widget.wrkOrderOpencontroller',
//    controllers: ['CoreController'],
//    stores: [
//        'WorkOrderOpening',
//        'LoggedInUserData',
//        'Histrystr'
////        'histrgrid'
//    ],
//    views: [
//        'ServiceModule.WorkOrderOpening.mainGrid',
//        'ServiceModule.WorkOrderOpening.ViewWrkOrder',
//        'ServiceModule.WorkOrderOpening.AddNew',
//        'ServiceModule.WorkOrderOpening.RegisterNewIMEI',
//        'ServiceModule.WorkOrderOpening.RepairHistoryGrid',
//        'ServiceModule.WorkOrderOpening.EditWorkOrder',
//        'ServiceModule.WorkOrderOpening.quickWorkOrderOpening',
//        'ServiceModule.WorkOrderOpening.RepairHistoryGridView'
//    ],
//    refs: [
//        {
//            ref: 'mainGrid',
//            selector: 'wrkOrderOpeningGrid'
//        },
//        {
//            ref: 'AddNew',
//            selector: 'addNewWorkOpening'
//        },
//        {
//            ref: 'RegisterNewIMEI',
//            selector: 'newIMEIregister'
//        },
//        {
//            ref: 'RepairHistoryGrid',
//            selector: 'repairHistoryGrid'
//        },
//        {
//            ref: 'EditWorkOrder',
//            selector: 'editWorkOrderOpening'
//        },
//        {
//            ref: 'quickWorkOrderOpening',
//            selector: 'quickWorkOrder'
//        },
//        {
//            ref: 'ViewWrkOrder',
//            selector: 'wrkOrderviewww'
//        },
//        {
//            ref: 'RepairHistoryGridView',
//            selector: 'histryView'
//        }
//
//    ],
//    init: function () {
//        var me = this;
//        me.control({
//            'wrkOrderOpeningGrid': {
//                added: this.initializeGrid
//            },
//            
//            '  wrkOrderOpeningGrid': {
//                onEdit: this.editNewWorkOrderOpeningWindow
//            },
//            ' wrkOrderOpeningGrid': {
//                onPrint: this.PrintWOdetails
//            },
//            'wrkOrderOpeningGrid button[action=clearSearch]': {
//                click: this.onClearSearch
//            },
//            'wrkOrderOpeningGrid button[action=qickWork]': {
//                click: this.openQuickWorkOrderwindow
//            },
//            'addNewWorkOpening button[action=addNewCancel]': {
//                click: this.addNewWorkOrderFormCancel
//            },
//            'addNewWorkOpening button[action=repairHistory]': {
//                click: this.openRepairHistoryGrid
//            },
//            'addNewWorkOpening button[action=registerNewIMEI]': {
//                click: this.openRegisterNewIMEIForm
//            },
//            'wrkOrderviewww button[action=view_wrk_order_cancel]': {
//                click: this.closeViewWorkOrder
//            },
//            'wrkOrderOpeningGrid  ': {
//                onView: this.onViewworkorder
//            },
//            'editWorkOrderOpening button[action=editCancel]': {
//                click: this.cancelEditWorkOrderForm
//            },
//            'wrkOrderOpeningGrid button[action=add_new]': {
//                click: this.openAddNew
//            },
//            'wrkOrderOpeningGrid ': {
//                onDelete: this.onDelete
//            },
//            'addNewWorkOpening button[action=addNewSubmit]': {
//                click: this.submitNewWorkOrder
//            },
//            'addNewWorkOpening button[action=verifyNIC]': {
//                click: this.NICverification
//            },
//            'combobox[action=imeiChange]': {
//                change: this.onSelect
//            },
//            'addNewWorkOpening button[action=editNewSubmit]': {
//                click: this.submitEdit
//            },
//            'newIMEIregister button[action=submitNewIMEIregistration]': {
//                click: this.onImeiReg
//            },
//            'addNewWorkOpening button[action=imeiVerification]': {
//                click: this.IMEIverification
//            },
//            'addNewWorkOpening button[action=chekWrnty]': {
//                click: this.warrentyVerification 
//            },
//            'quickWorkOrder button[action=SearchQuick]': {
//                click: this.quickWorkOrderSearch 
//            },
//            'quickWorkOrder button[action=Submit_quickWrk_order]': {
//                click: this.submitQuickWorkOrder 
//            },
//            'newIMEIregister button[action=cancelNewIMEIregistration]': {
//                click: this.cancelIMEIRegistration 
//            },
//            'combobox[action=selectAddress]': {
//                change: this.onSelection
//            },
//            'repairHistoryGrid': {
//                viewRepairHistory: this.openhistoryView
//            },
//            'histryView button[action=view_wrk_order_cancel]': {
//                click: this.closeViewWorkOrder
//            },
//            ' addNewWorkOpening button[action=repairHistory]': {
//                click: this.openrepairHistoryGrid
//            },
//            ' combobox[action=mdleNochnge]': {
//                change: this.onSelectt
//            },
//        });
//    },
//    onSelectt: function(btn, value) {
//       var modStore = btn.getStore().data.items[0].data.wrn_Scheme;
//        Ext.getCmp('wrntyTypeee').setValue(modStore);
//    },
//    
//    openrepairHistoryGrid:function(){
//        var IMEIvalue = Ext.getCmp('imeiVal').getValue();
//        if(IMEIvalue !== ''){
//          var store = Ext.getStore('Histrystr');
//          store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?imeiNo=' + IMEIvalue+'';
//          store.load();
//        }else{
//           Ext.Msg.alert("Error ", "Please enter valid IMEI No");
//        }
//       
//    },
//    
//    openhistoryView:function(view, record){
////        //console.log('openhistoryView');
////        this.getController('CoreController').onOpenViewMoreWindow(record, 'histryView', 'View');
//          var wrkNo=record.data.workOrderNo;
//        //console.log(record.data.workOrderNo);
//        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=WOmaintain.rptdesign&woNUM="+wrkNo;
//        window.location.href = reporturl;
//    },
//    
//    onViewworkorder: function (view, record) {
//        
//        var win = Ext.widget('wrkOrderviewww');
//        var formWindow = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'View',
//            items: [win]
//        });
//        ////console.log(record.data);
//        formWindow.show();
//        var form = formWindow.down('form');
//        form.getForm().loadRecord(record);
//        win.show();
//    },
//    onSelection: function(btn, value) {
//        var WOvaluesStore = btn.getStore().data.items[0].data.address;
////        //console.log(WOvaluesStore);
////        Ext.getCmp('distributorAddress').setValue(WOvaluesStore);
//    },
//    
//    cancelIMEIRegistration:function(btn){
//        this.getController('CoreController').onInsertFormCancel(btn);
//    },
//    
//    submitQuickWorkOrder:function(btn){
//        //console.log('submitQuickWorkOrder');
//        var window = btn.up('window');
//        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var form = btn.up('form').getValues();
//        var store = this.getWorkOrderOpeningStore();
//        
//        if(form!== ''){
//            Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'WorkOrders/editWorkOrder',
//            method: 'POST',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                system: sys,
//                'Content-Type': 'application/json'
//            },
//            params: JSON.stringify(form),
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                if (responseData.returnFlag === '1') {
//                    ////console.log(responseData)
//                    singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
//                    window.destroy();
//                    store.load();
//                } else {
//                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                }
//
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
//        }else{
//                Ext.Msg.alert("Can not update", "Please fill the form before submit");
//        }
//        
//    },
//    
//    quickWorkOrderSearch:function(){
//        //console.log('quickWorkOrderSearch');
//        var workOrderno = Ext.getCmp('WoN').getValue();
//        var NICno = Ext.getCmp('nicc').getValue();
//        var imeino = Ext.getCmp('imeii').getValue();
//        var loginData = this.getLoggedInUserDataStore();
//        
//        if(workOrderno !== ''){
//                Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?workOrderNo='+ workOrderno,
//                method: 'GET',
//                headers: {
//                    userId: loginData.data.getAt(0).get('userId'),
//                    room: loginData.data.getAt(0).get('room'),
//                    department: loginData.data.getAt(0).get('department'),
//                    branch: loginData.data.getAt(0).get('branch'),
//                    countryCode: loginData.data.getAt(0).get('countryCode'),
//                    division: loginData.data.getAt(0).get('division'),
//                    organization: loginData.data.getAt(0).get('organization'),
//                    'Content-Type': 'application/json'
//                },
//
//                success: function (response, btn) {
//                    var responseData = Ext.decode(response.responseText);
//                    //console.log(responseData);
//                    Ext.getCmp('WoN').setValue(responseData.data[0].workOrderNo);
//                    Ext.getCmp('nme').setValue(responseData.data[0].customerName);
//                    Ext.getCmp('adres').setValue(responseData.data[0].customerAddress);
//                    Ext.getCmp('4hone').setValue(responseData.data[0].workTelephoneNo);
//                    Ext.getCmp('email').setValue(responseData.data[0].email);
//                    Ext.getCmp('nicc').setValue(responseData.data[0].customerNic);
//                    Ext.getCmp('imeii').setValue(responseData.data[0].imeiNo);
//                    Ext.getCmp('prdctt').setValue(responseData.data[0].product);
//                    Ext.getCmp('brndd').setValue(responseData.data[0].brand);
//                    Ext.getCmp('mdle').setValue(responseData.data[0].modleNo);
//                    Ext.getCmp('retain').setValue(responseData.data[0].assessoriesRetained);
//                    Ext.getCmp('dfct').setValue(responseData.data[0].defectNo);
//                    Ext.getCmp('rccrference').setValue(responseData.data[0].rccReference);
//                    Ext.getCmp('saleD').setValue(responseData.data[0].dateOfSale);
//                    Ext.getCmp('deliverDte').setValue(responseData.data[0].deleveryDate);
//                    Ext.getCmp('proofPurchase').setValue(responseData.data[0].proofOfPurches);
//                    Ext.getCmp('disShp').setValue(responseData.data[0].bisId);
//                    Ext.getCmp('wtype').setValue(responseData.data[0].warrentyVrifType);
//                },
//                failure: function () {
//                    Ext.Msg.hide();
//                    Ext.Msg.alert("Error in connection", "Please try again later");
//                }
//            });
//        }else if(NICno !== ''){
//                Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?customerNic='+ NICno,
//                method: 'GET',
//                headers: {
//                    userId: loginData.data.getAt(0).get('userId'),
//                    room: loginData.data.getAt(0).get('room'),
//                    department: loginData.data.getAt(0).get('department'),
//                    branch: loginData.data.getAt(0).get('branch'),
//                    countryCode: loginData.data.getAt(0).get('countryCode'),
//                    division: loginData.data.getAt(0).get('division'),
//                    organization: loginData.data.getAt(0).get('organization'),
//                    'Content-Type': 'application/json'
//                },
//
//                success: function (response, btn) {
//                    var responseData = Ext.decode(response.responseText);
//                    //console.log(responseData);
//                    Ext.getCmp('WoN').setValue(responseData.data[0].workOrderNo);
//                    Ext.getCmp('nme').setValue(responseData.data[0].customerName);
//                    Ext.getCmp('adres').setValue(responseData.data[0].customerAddress);
//                    Ext.getCmp('4hone').setValue(responseData.data[0].workTelephoneNo);
//                    Ext.getCmp('email').setValue(responseData.data[0].email);
//                    Ext.getCmp('nicc').setValue(responseData.data[0].customerNic);
//                    Ext.getCmp('imeii').setValue(responseData.data[0].imeiNo);
//                    Ext.getCmp('prdctt').setValue(responseData.data[0].product);
//                    Ext.getCmp('brndd').setValue(responseData.data[0].brand);
//                    Ext.getCmp('mdle').setValue(responseData.data[0].modleNo);
//                    Ext.getCmp('retain').setValue(responseData.data[0].assessoriesRetained);
//                    Ext.getCmp('dfct').setValue(responseData.data[0].defectNo);
//                    Ext.getCmp('rccrference').setValue(responseData.data[0].rccReference);
//                    Ext.getCmp('saleD').setValue(responseData.data[0].dateOfSale);
//                    Ext.getCmp('deliverDte').setValue(responseData.data[0].deleveryDate);
//                    Ext.getCmp('proofPurchase').setValue(responseData.data[0].proofOfPurches);
//                    Ext.getCmp('disShp').setValue(responseData.data[0].bisId);
//                    Ext.getCmp('wtype').setValue(responseData.data[0].warrentyVrifType);
//                },
//                failure: function () {
//                    Ext.Msg.hide();
//                    Ext.Msg.alert("Error in connection", "Please try again later");
//                }
//            });
//        }else{
//             Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderList?imeiNo='+ imeino,
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
//            
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                //console.log(responseData);
//                Ext.getCmp('WoN').setValue(responseData.data[0].workOrderNo);
//                    Ext.getCmp('nme').setValue(responseData.data[0].customerName);
//                    Ext.getCmp('adres').setValue(responseData.data[0].customerAddress);
//                    Ext.getCmp('4hone').setValue(responseData.data[0].workTelephoneNo);
//                    Ext.getCmp('email').setValue(responseData.data[0].email);
//                    Ext.getCmp('nicc').setValue(responseData.data[0].customerNic);
//                    Ext.getCmp('imeii').setValue(responseData.data[0].imeiNo);
//                    Ext.getCmp('prdctt').setValue(responseData.data[0].product);
//                    Ext.getCmp('brndd').setValue(responseData.data[0].brand);
//                    Ext.getCmp('mdle').setValue(responseData.data[0].modleNo);
//                    Ext.getCmp('retain').setValue(responseData.data[0].assessoriesRetained);
//                    Ext.getCmp('dfct').setValue(responseData.data[0].defectNo);
//                    Ext.getCmp('rccrference').setValue(responseData.data[0].rccReference);
//                    Ext.getCmp('saleD').setValue(responseData.data[0].dateOfSale);
//                    Ext.getCmp('deliverDte').setValue(responseData.data[0].deleveryDate);
//                    Ext.getCmp('proofPurchase').setValue(responseData.data[0].proofOfPurches);
//                    Ext.getCmp('disShp').setValue(responseData.data[0].bisId);
//                    Ext.getCmp('wtype').setValue(responseData.data[0].warrentyVrifType);
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
//        }
//    },
//    
//    warrentyVerification:function(){
//        //console.log('warrentyVerification');
//        var IMEIvalue = Ext.getCmp('imeiVal').getValue();
//        var datevalue = Ext.getCmp('datte').getValue();
//        
//        var loginData = this.getLoggedInUserDataStore();
//
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'WarrantyScheme/getWarrantyDays?imeiNo=' + IMEIvalue+'&dateOfSale='+datevalue,
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
//            // params: JSON.stringify(user.data),
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                //console.log(responseData);
//
//                if (responseData.returnFlag === '1') {
//                    Ext.MessageBox.alert('Warrenty Verification', 'This item is with in the warrenty period', showResult);
//                } else {
//                    singer.LayoutController.createErrorPopup("Not Registered", "This Customer is not registered Previously. Fill the other datails of the customer", responseData.returnMsg, Ext.MessageBox.WARNING);
//                }
//
//
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });     
//    },
//    
//    IMEIverification:function(){
//        //console.log('IMEIverification');
//        var IMEIvalue = Ext.getCmp('imeiVal').getValue();
//        //console.log(IMEIvalue);
//
//        var loginData = this.getLoggedInUserDataStore();
//        if (IMEIvalue === ''){
//            Ext.Msg.alert("Error ", "Please enter valid IMEI number ");
//        }else{
//            Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/getImeiDetails?imeiNo=' + IMEIvalue,
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
//            
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                    Ext.getCmp('prdct').setValue(responseData.data[0].product);
//                    Ext.getCmp('brnd').setValue(responseData.data[0].brand);
//                    Ext.getCmp('model').setValue(responseData.data[0].modleNo);
//                    Ext.getCmp('prchaseDte').setValue(responseData.data[0].purchaseDate); 
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        }); 
//        }
//       
//    },
//    PrintWOdetails: function (veiw,rec) {
//        
//        var wrkNo=rec.data.workOrderNo;
//        //console.log(rec.data.workOrderNo);
//        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=wo_OPENING.rptdesign&workOrderNo="+wrkNo;
//        window.location.href = reporturl;
//    },
//    submitEdit: function (btn) {
//        ////console.log('comming to edit');
//        var window = btn.up('window');
//        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var form = btn.up('form').getValues();
//        delete form.email2;
//        var store = this.getWorkOrderOpeningStore();
////        var mod = Ext.create('singer.model.WorkOrderOpeningM',
////                form.getValues()
////                );
////        //console.log(mod.data);
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'WorkOrders/editWorkOrder',
//            method: 'POST',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                system: sys,
//                'Content-Type': 'application/json'
//            },
//            params: JSON.stringify(form),
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                if (responseData.returnFlag === '1') {
//                    ////console.log(responseData)
//                    singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
//                    window.destroy();
//                    store.load();
//                } else {
//                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                }
//
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
//    },
//    submitNewWorkOrder: function (btn,rec) {
//        
//        var windoww1 = btn.up('window');
//        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var form = btn.up('form');
//        ////console.log(form.getValues());
//        var store = this.getWorkOrderOpeningStore();
//       var mod = Ext.create('singer.model.WorkOrderOpeningM',
//               form.getValues()
//               );
//       //console.log(mod.data);
//
//       var mail = Ext.getCmp('cusmail1').getValue();
//       var RetypeMail = Ext.getCmp('cusmail2').getValue();
////       var wrkorderNum = Ext.getCmp('cusmail2').getValue();
//       
//       if(mail===RetypeMail){
//            Ext.Msg.confirm("Print?", "Do You want to Print this?", function (btnn) {
//                if (btnn === "yes") {        
//                        Ext.Ajax.request({
//                        url: singer.AppParams.JBOSS_PATH + 'WorkOrders/addWorkOrder',
//                        method: 'PUT',
//                        headers: {
//                            userId: loginData.data.getAt(0).get('userId'),
//                            room: loginData.data.getAt(0).get('room'),
//                            department: loginData.data.getAt(0).get('department'),
//                            branch: loginData.data.getAt(0).get('branch'),
//                            countryCode: loginData.data.getAt(0).get('countryCode'),
//                            division: loginData.data.getAt(0).get('division'),
//                            organization: loginData.data.getAt(0).get('organization'),
//                            system: sys,
//                            'Content-Type': 'application/json'
//                        },
//                        params: JSON.stringify(mod.data),
//                        success: function (response, btn) {
//                            var responseData = Ext.decode(response.responseText);
//                            if (responseData.returnFlag === '1') {
//                                ////console.log(responseData)
//                                windoww1.close();
//                                singer.LayoutController.notify('Record Inserted.', responseData.returnMsg);
//                                store.load({
//                                    callback: function (records, operation, success) {
//                                        var wonm = records[0].data.workOrderNo;
//                                       var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=wo_OPENING.rptdesign&workOrderNo="+wonm;
//                                        window.location.href = reporturl;
//                                    }
//                                });
//                            } else {
//                                singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                            }
//
//                        },
//                        failure: function () {
//                            Ext.Msg.hide();
//                            Ext.Msg.alert("Error in connection", "Please try again later");
//                        }
//                    });
//                } else {
//                    //console.log('comming to else');
//                    Ext.Ajax.request({
//                    url: singer.AppParams.JBOSS_PATH + 'WorkOrders/addWorkOrder',
//                    method: 'PUT',
//                    headers: {
//                        userId: loginData.data.getAt(0).get('userId'),
//                        room: loginData.data.getAt(0).get('room'),
//                        department: loginData.data.getAt(0).get('department'),
//                        branch: loginData.data.getAt(0).get('branch'),
//                        countryCode: loginData.data.getAt(0).get('countryCode'),
//                        division: loginData.data.getAt(0).get('division'),
//                        organization: loginData.data.getAt(0).get('organization'),
//                        system: sys,
//                        'Content-Type': 'application/json'
//                    },
//                    params: JSON.stringify(mod.data),
//                    success: function (response, btn) {
//                        var responseData = Ext.decode(response.responseText);
//                        if (responseData.returnFlag === '1') {
//                            ////console.log(responseData)
//                            windoww1.close();
//                            singer.LayoutController.notify('Record Inserted.', responseData.returnMsg);
//                            store.load();
//                        } else {
//                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                        }
//
//                    },
//                    failure: function () {
//                        Ext.Msg.hide();
//                        Ext.Msg.alert("Error in connection", "Please try again later");
//                    }
//                });
//                    
//                }
//            });
//           //===============================
//           
//       }else{
//           Ext.Msg.alert("Error ", "Email values not match");
//       }
//    },
//    
//    
//    onSelect: function (btn, value) {
//        var imeiValuesStore = btn.getStore(); //Ext.getStore('exchangeRfnStor');
//        var temp = imeiValuesStore.findExact('imeiNo', value);
//        if (temp !== -1) {
//            var imeVal = imeiValuesStore.getAt(temp).data;
//            Ext.getCmp('prdct').setValue(imeVal.product);
//            Ext.getCmp('brnd').setValue(imeVal.brand);
//            Ext.getCmp('model').setValue(imeVal.modleNo);
//        }
//    },
//    NICverification: function (record) {
//        ////console.log('comming to NIC verification');
//        var valueOfNIC = Ext.getCmp('cusNIC').getValue();
//        ////console.log(valueOfNIC);
//
//        var loginData = this.getLoggedInUserDataStore();
//        if(valueOfNIC !==''){
//            Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'Customers/checkCustomerDetal?customerNIC=' + valueOfNIC,
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
//            // params: JSON.stringify(user.data),
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                ////console.log(("res data : ",responseData));
//
//                if (responseData.data[0].returnFlag === '1') {
//                    Ext.getCmp('cusname').show();
//                    Ext.getCmp('cusname').setValue(responseData.data[0].customerName);
//                    
//                    Ext.getCmp('cusAddrs').show();
//                    Ext.getCmp('cusAddrs').setValue(responseData.data[0].address);
//                    
//                    Ext.getCmp('contactNo').show();
//                    Ext.getCmp('contactNo').setValue(responseData.data[0].contactNo);
//                    
//                    Ext.getCmp('cusmail1').show();
//                    Ext.getCmp('cusmail1').setValue(responseData.data[0].email);
//                    
//                    Ext.getCmp('cusmail2').show();
//                    Ext.getCmp('cusmail2').setValue(responseData.data[0].email);
//                } else {
//                    singer.LayoutController.createErrorPopup("Not Registered", "This Customer is not registered Previously. Fill the other datails of the customer", responseData.returnMsg, Ext.MessageBox.WARNING);
//                }
//
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
//        }else{
//            Ext.Msg.alert("Error ", "Please enter valid NIC");
//        }
//        
//    },
//    onDelete: function (btn, record) {
//
//        //         'WorkOrderOpening',
//        //        'LoggedInUserData'
//        Ext.Msg.confirm("Confirmation", "Do you want to delete?", function (btnText) {
//            if (btnText === "yes") {
//                var store = this.getWorkOrderOpeningStore();
//                //console.log(record.data);
//                var x = record.data;
//
//                var loginData = this.getLoggedInUserDataStore();
//                var sys = singer.AppParams.SYSTEM_CODE;
//                Ext.Msg.wait('Please wait', 'Deleting Data...', {
//                    interval: 300
//                });
//                Ext.Ajax.request({
//                    url: singer.AppParams.JBOSS_PATH + 'WorkOrders/deleteWorkOrder',
//                    method: 'POST',
//                    headers: {
//                        userId: loginData.data.getAt(0).get('userId'),
//                        room: loginData.data.getAt(0).get('room'),
//                        department: loginData.data.getAt(0).get('department'),
//                        branch: loginData.data.getAt(0).get('branch'),
//                        countryCode: loginData.data.getAt(0).get('countryCode'),
//                        division: loginData.data.getAt(0).get('division'),
//                        organization: loginData.data.getAt(0).get('organization'),
//                        system: sys,
//                        'Content-Type': 'application/json'
//                    },
//                    params: JSON.stringify(x),
//                    success: function (response) {
//                        Ext.Msg.hide();
//                        var responseData = Ext.decode(response.responseText);
//                        //////console.log(responseData);
//                        if (responseData.returnFlag === '1') {
//                            singer.LayoutController.notify('Record Deleted.', responseData.returnMsg);
//                        }
//                        //parentStore.load();
//                        store.load();
//                    },
//                    failure: function () {
//                        Ext.Msg.hide();
//                        Ext.Msg.alert("Error in connection", "Please try again later");
//                    }
//                });
//            } else if (btnText === "no") {
//            }
//        }, this);
//
//    },
//    cancelEditWorkOrderForm: function (btn) {
//        // ////console.log('comming to cancel');
//        var frm = btn.up('window');
//        //////console.log(frm);
//        //frm.destroy();
//        Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function (btnn) {
//            if (btnn === "yes") {
//                frm.destroy();
//            } else {
//            }
//        });
//    },
//    closeViewWorkOrder: function (btn) {
//        ////console.log('comming to cancel');
//        var frm = btn.up('window');
//        ////console.log(frm);
//        frm.destroy();
//    },
//    openQuickWorkOrderwindow: function () {
//        ////console.log('open quick work order');
//        var form = Ext.widget('quickWorkOrder');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Quick Work Order',
//            items: [form]
//        });
//        win.show();
//    },
//    openRepairHistoryGrid: function () {
//        var form = Ext.widget('repairHistoryGrid');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Repair History',
//            items: [form]
//        });
//        win.show();
//    },
//    openRegisterNewIMEIForm: function (cmp) {
//        var form = Ext.widget('newIMEIregister');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Register New IMEI',
//            items: [form]
//        });
//        win.show();
//        //				//console.log(cmp.up('form'));
//    },
//    
//    openAddNew: function () {
//        var form = Ext.widget('addNewWorkOpening');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Add New Work Order',
//            items: [form]
//        });
//        Ext.getCmp('addNewWorkOpening-save').hide();
//        win.show();
//        //  ////console.log(Ext.getCmp('cusInfo'));
//    },
//    
//    editNewWorkOrderOpeningWindow: function (grid, record) {
////        this.getController('CoreController').openUpdateWindow(record, 'addNewWorkOpening', 'Edit');
//        var win = Ext.widget('addNewWorkOpening');
//        //console.log('editNewWorkOrderOpeningWindow');
//        //console.log(record);
//        var formWindow = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Edit Work Order',
//            items: [win]
//        });
//        ////console.log(record.data);
//        formWindow.show();
//        var form = formWindow.down('form');
//        form.getForm().loadRecord(record);
//        Ext.getCmp('addNewWorkOpening-create').hide();
//        win.show();
//
//    },
//    initializeGrid: function (grid) {
//        var store = this.getWorkOrderOpeningStore();
//        this.getController('CoreController').loadBindedStore(store);
//    },
//    addNewWorkOrderFormCancel: function (btn) {
//
//        var form = btn.up('form').up().up();
//
//        Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function (btnn) {
//            if (btnn === "yes") {
////                form.close();
//                  btn.up('window').destroy();
//            } else {
//            }
//        });
//    },
//    
//    onClearSearch: function (btn) {
//        this.getController('CoreController').onClearSearch(btn);
//    },
//    
//    
//    
//    onImeiReg: function (cmp) {
//        var form = cmp.up('form').up('form');
//        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var mod = Ext.create('singer.model.WarrentyRegistration',
//                form.getValues()
//                );
//        mod.set('bisId', loginData.data.getAt(0).get('extraParams'));
//        //console.log(mod.data);
//        Ext.Msg.wait('Please wait', 'Registering IMEI...', {
//            interval: 300
//        });
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'WarrentyRegistrations/addWorkOrderWarrentyRegistration',
//            method: 'PUT',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                system: sys,
//                'Content-Type': 'application/json'
//            },
//            params: JSON.stringify(mod.data),
//            success: function (response) {
//                Ext.Msg.hide();
//                var responseData = Ext.decode(response.responseText);
//                //////console.log(responseData);
//                if (responseData.returnFlag === '1') {
//                    singer.LayoutController.notify('Registered.', responseData.returnMsg);
//                    form.destroy();
//                } else {
//                    Ext.Msg.alert("Error Registering...", responseData.returnMsg);
//                }
//                //parentStore.load();
//                //				store.load();
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
//
//    }
//
//
//    //    insertFormCanceladdIMEI: function (btn) {
//    //        this.getController('CoreController').onInsertFormCancel(btn);
//    //    },
//    //   
//    //    
//    //    insertRecord: function (btn) {
//    //        //////console.log('comming to insert record');
//    ////        var store = this.getImeiAdjsStore();
//    //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Record Created.', 'Creating New record', true, true, this.getGrid(), false);
//    ////        var grid = this.getGrid();
//    //
//    //    },
//    //    
//    //    editFormCancel: function(btn) {
//    //        this.getController('CoreController').onInsertFormCancel(btn);
//    //    },
//    //    insertFormCancel: function(btn) {
//    //        this.getController('CoreController').onInsertFormCancel(btn);
//    //    },
//    //    onAdjustImei: function () {
//    //        this.getController('CoreController').openInsertWindow('debitnoteform');
//    //
//    //    },
//    //    onEdit:function(view, record){
//    //        this.getController('CoreController').openUpdateWindow(record, 'imeiform', 'Edit details of Debit Note No:'+ record.get('debitNoteNo'));
//    //    },
//    //    updataRecord: function (btn) {
//    //        this.getController('CoreController').onRecordUpdate(btn, this.getImeiAdjsStore(), 'IMEI Data Updated.', 'Updating IMEI Data', true, true, this.getGrid());
//    //    },
//
//});