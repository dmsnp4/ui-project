Ext.define('singer.controller.ServiceReports', {
    extend: 'Ext.app.Controller',
    views: ['ServiceModule.ServiceModuleReports'],
    models: [],
    stores: ['LoggedInUserData', 'ServiceUserType'],
    refs: [{
            ref: 'ServiceModuleReports',
            selector: 'serviceMReports'
        }],
    init: function () {
        var me = this;
        me.control({
            'serviceMReports button[action=W_Nrpt]': {
                click: this.Defect_Codes_Repair_Report_InWarranty
            },
            'serviceMReports button[action=sparePrtDelay]': {
                click: this.spareParts_UsageReport_inWaranty
            },
            'serviceMReports button[action=sparePrtusageOutWaranty]': {
                click: this.spareParts_UsageReport_Out_of_Waranty
            },
            'serviceMReports button[action=defectrpr]': {
                click: this.defectCodesRepairReport
            },
            'serviceMReports button[action=quickWrkOrder]': {
                click: this.quickWorkOrderReport
            },
            'serviceMReports button[action=repairRpt]': {
                click: this.repairReports
            },
            'serviceMReports button[action=franchiseRpt]': {
                click: this.franchisePaymentReport
            },
            'serviceMReports button[action=exchngereport]': {
                click: this.exchangeRpt
            },
            'serviceMReports button[action=ffrReport]': {
                click: this.ffr_report
            },
            'serviceMReports button[action=frnchise_payment_not_summary]': {
                click: this.frnchise_payment_not_send_summary
            },
            'serviceMReports button[action=frnchise_payment_summary]': {
                click: this.frnchise_payment_send_summary
            },
            'serviceMReports button[action=delay_date]': {
                click: this.delay_date_report
            },
            'serviceMReports button[action=rp_report]': {
                click: this.repair_report
            },
            'serviceMReports button[action=exchange_report]': {
                click: this.exchange_report
            },
        });
    },
//    repair_report:function(){
//        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=REPAIRREPORT.rptdesign"  ,'Delay Report');
//     
//    },
    repair_report: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
        //var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');

        var bis_id = Ext.getCmp('location_struct_id_rep').getValue();

        var rep_type = Ext.getCmp('rep_type').getValue();
        console.log('rep_type', rep_type);

        if (rep_type === null) {
            Ext.Msg.alert('Error', 'Please select the report type.');
        }
        else if (rep_type === '1') {
            if (bis_id === "" || bis_id === null) {
                Ext.Msg.alert('Error', 'Please select the location.');
            }
            else {
                this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=REPAIRREPORT2.rptdesign&BID_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Repair Report');
            }
        }
        else if (rep_type === '2') {
            if (bis_id === "" || bis_id === null) {
                Ext.Msg.alert('Error', 'Please select the location.');
            }
            else{
                this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=REPAIRREPORTSINGLELINE.rptdesign&BID_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Repair Report');
                Ext.getCmp("btnPlainXls").destroy(); 
            }
        }
    },
    delay_date_report: function () {
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DELAYDAYS.rptdesign&BIS_ID=" + bis_id, 'Delay Date Report');
        Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    exchange_report: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ExchangeReportNew.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Exchange Report');
        Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    ffr_report: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();
        var modelNo = Ext.getCmp('FFR_value').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=FFRrateReport.rptdesign&DATE_ONE=" + sDate + "&DATE_TWO=" + eDate + "&BIS_ID=" + bis_id + "&modelno=" + modelNo, 'FFR Rate Report');
      Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    Defect_Codes_Repair_Report_InWarranty: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');

        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DEFECTCODEREPAIRREPORT.rptdesign&BID_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, "Defect Codes Repair Report ");
        Ext.getCmp("btnPlainXls").destroy();    
        }
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        //console.log(loginData);
    },
    Defect_Codes_Repair_Reports_Out_Of_Warranty: function () {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DefectsCodeRepairReportUnderWaranty.rptdesign", 'Defect Codes Repair Reports Out Of Warranty');
    },
    spareParts_UsageReport_inWaranty: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');

        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=SPAREPARTSUSAGEREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE" + sDate + "&DATETWO=" + eDate, 'Spare Parts Usage Report ');
      Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    spareParts_UsageReport_Out_of_Waranty: function () {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=SPAREPARTSUSAGEREPORTOutWaranty.rptdesign", 'Spare Parts Usage Report Out Of Warranty');
     Ext.getCmp("btnPlainXls").destroy(); 
        
    },
    defectCodesRepairReport: function () {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=new_report_4.rptdesign", 'Defect Codes Repair Report');
     Ext.getCmp("btnPlainXls").destroy(); 
        
    },
    quickWorkOrderReport: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=UNATTENDEDQUICKWORKREPORT.rptdesign&BIS_ID=" + bis_id + "&DATE_ONE=" + sDate + "&DATE_TWO=" + eDate, 'Unattendent Quick Work Order Report');
        Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    repairReports: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=RepeatRepairReport.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Repair Reports');
   Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    franchisePaymentReport: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();


        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=FranchisePayment.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&PAY_BIS_ID=" + bis_id, 'Franchise Payment Report');
        Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    frnchise_payment_not_send_summary: function () {

        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=WOPAYMENTNOTSENT.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BIS_ID=" + bis_id, 'Franchise Payment WO Not Send Report');
        Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    frnchise_payment_send_summary: function () {
        var start = Ext.getCmp('startDate_service_reports');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endDate_service_reports');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var loginData = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        var bis_id = Ext.getCmp('service_location_struct_id').getValue();

        //console.log('ffr rate');
        //console.log(sDate);
        //console.log(eDate);
        //console.log(loginData);

        if (bis_id === '') {
            console.log('bis_id', bis_id);
            Ext.Msg.alert('Error', 'Please select the location.');
        }
        else{
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=WOPAYMENTSENT.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BIS_ID=" + bis_id, 'Franchise Payment Send Report');
        Ext.getCmp("btnPlainXls").destroy(); 
        }
        },
    exchangeRpt: function () {
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ExchangeReport.rptdesign", 'Exchange Report');
    Ext.getCmp("btnPlainXls").destroy(); 
    },
});



