


Ext.define('singer.controller.EventRpt', {
    extend: 'Ext.app.Controller',
    views: ['Events.EventsReports'],
    models: [],
    stores: [],
    refs: [{
            ref: 'EventsReports',
            selector: 'eventMReports'
        }],
    init: function() {
        var me = this;
        me.control({
            'eventMReports button[action=Ongoing]': {
                click: this.openOngoingReports
            },
            'eventMReports button[action=cmpleted]': {
                click: this.openCompletedReports
            },
        });
    },
    openOngoingReports: function() {
//        //console.log('openOngoingReports');
//        var reporturl = singer.AppParams.REPO +"BirtReportController?ReportFormat="+singer.AppParams.REPOTYPE+"&ReportName=ongoingEventRpt.rptdesign";
//        window.location.href = reporturl;
          this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ongoingEventRpt.rptdesign",'On Going Event Report' );
    },
    openCompletedReports: function() {
//        //console.log('openCompletedReports');
//        var reporturl = singer.AppParams.REPO +"BirtReportController?ReportFormat="+singer.AppParams.REPOTYPE+"&ReportName=completedEventsrpt.rptdesign";
//        window.location.href = reporturl;
          this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=completedEventsrpt.rptdesign",'Complete Event Report' );
    },

});



