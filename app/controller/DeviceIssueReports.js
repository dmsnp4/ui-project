

Ext.define('singer.controller.DeviceIssueReports', {
    extend: 'Ext.app.Controller',
    views: ['DeviceIssueModule.DeviceIssueReports', 'DeviceIssueModule.DeviceIssueReportsExternal'],
    models: [],
    stores: [],
    refs: [{
            ref: 'DeviceIssueReports',
            selector: 'deviceIssueReports'
        }],
    init: function () {
        var me = this;
        me.control({
            'deviceIssueReports button[action=showBalanceStockReport]': {
                click: this.openBalanceStockReport, //DONE
            },
            'deviceIssueReports button[action=showBalanceStockReportDtl]': {
                click: this.openBalanceStockReportDtl, //DONE
            },
            'deviceIssueReports button[action=ShowInventoryReport]': {
                click: this.openInventoryReport
            },
            'deviceIssueReports button[action=showNonMovingReports]': {
                click: this.openNonMovingInventoryReport
            },
            'deviceIssueReports button[action=showPurchaseReport]': {
                click: this.openPurchaseReport
            },
            'DeviceIssueReportsExternal button[action=showSalesReport]': {
                click: this.openSalesReport
            },
            'deviceIssueReports button[action=showSalesReport]': {
                click: this.openSalesReport
            },
            'deviceIssueReports button[action=showSalesReportSumry]': {
                click: this.openSalesSummary
            },
            'deviceIssueReports button[action=showDistIssueToDsr]': {
                click: this.openshowDistIssueToDsr
            },
            ' deviceIssueReports button[action=showOutletReport]': {
                click: this.outletrpt
            },
            ' deviceIssueReports button[action=show_adjustment_Report]': {
                click: this.adjustmentReport
            },
            ' deviceIssueReports button[action=show_credit_note_Report]': {
                click: this.credit_note_Report
            },
            ' deviceIssueReports button[action=show_Device_return_Report]': {
                click: this.Device_return_Report
            },
            ' deviceIssueReports button[action=show_warr_reg_Report]': {
                click: this.warranty_reg_report
            },
            ' deviceIssueReports button[action=show_inserted_reg_Report]': {
                click: this.openWrRegReport
            },
            ' deviceIssueReports button[action=showDistrubutorStockReport]': {
                click: this.openDisStockReport
            },
            ' deviceIssueReports button[action=showDistrubutorStockReportDtl]': {
                click: this.openDisStockReportDtl
            },
            ' deviceIssueReports button[action=showDealerStockReport]': {
                click: this.showDealerStockReport
            },
            ' deviceIssueReports button[action=showDealerStockReportDtl]': {
                click: this.showDealerStockReportDtl
            },
            ' deviceIssueReports button[action=salesAreaDisReport]': {
                click: this.salesAreaDisReport
            },
            ' deviceIssueReports button[action=salesAreaDisReportDtl]': {
                click: this.salesAreaDisReportDtl
            },
            ' deviceIssueReports button[action=salesAreaDisDealerReport]': {
                click: this.salesAreaDisDealerReport
            },
            ' deviceIssueReports button[action=salesAreaDisDealerReport]': {
                click: this.salesAreaDisDealerReport
            },
            ' deviceIssueReports button[action=salesAreaDisDealerReportDtl]': {
                click: this.salesAreaDisDealerReportDtl
            },
            ' deviceIssueReports button[action=show_imei_movement]': {
                click: this.openImeiMovement
            }
        });
    },
    showDealerStockReport: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Dealer_Stock_Excel.rptdesign&BisId=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
            localStorage.setItem('myReptExcelURI', urii);
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=DealerStock_Report.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BisId=" + bis_id, 'Dealer Stock Report');


        }

    },
    showDealerStockReportDtl: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
//            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Dealer_Stock_Excel.rptdesign&BisId=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
//            localStorage.setItem('myReptExcelURI', urii);
//            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=DealerStock_Report.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BisId=" + bis_id, 'Dealer Stock Report');
            window.open(singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=DealerStock_Report_IMEI.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, '_blank');

        }

    },
    salesAreaDisDealerReport: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=AreaDistributorDSRDealer_SalesReport.rptdesign&BisId=" + bis_id + "&fromDate=" + sDate + "&toDate=" + eDate;
            localStorage.setItem('myReptExcelURI', urii);
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=AreaDistributorDSRDealer_SalesReport.rptdesign&fromDate=" + sDate + "&toDate=" + eDate + "&BisId=" + bis_id, 'Sales Report  - Area, Distributor, DSR wise and dealer wise');


        }

    },
    salesAreaDisDealerReportDtl: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
            window.open(singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=AreaDistributorDSRDealer_SalesReport_IMEI.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, '_blank');

        }

    },
    salesAreaDisReport: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=AreaDistributorDSR_SalesReport.rptdesign&BisId=" + bis_id + "&fromDate=" + sDate + "&toDate=" + eDate;
            localStorage.setItem('myReptExcelURI', urii);
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=AreaDistributorDSR_SalesReport.rptdesign&fromDate=" + sDate + "&toDate=" + eDate + "&BisId=" + bis_id, 'Sales Report - Area, Distributor, DSR wise');


        }

    },
    salesAreaDisReportDtl: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
//            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=AreaDistributorDSR_SalesReport_IMEI.rptdesign&BisId=" + bis_id + "&fromDate=" + sDate + "&toDate=" + eDate;
//            localStorage.setItem('myReptExcelURI', urii);
//            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=AreaDistributorDSR_SalesReport.rptdesign&fromDate=" + sDate + "&toDate=" + eDate + "&BisId=" + bis_id, 'Sales Report - Area, Distributor, DSR wise');
window.open(singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=AreaDistributorDSR_SalesReport_IMEI.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, '_blank');

        }

    },
    openDisStockReport: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Distributor_Stock_Excel.rptdesign&BisId=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
            localStorage.setItem('myReptExcelURI', urii);
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=DistributorStock_Report.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BisId=" + bis_id, 'Distributor Stock Report');


        }

    },
    openDisStockReportDtl: function () {

        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {

            Ext.Msg.alert('Error', 'Please select the location.');

        } else {
//            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Distributor_Stock_Excel.rptdesign&BisId=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
//            localStorage.setItem('myReptExcelURI', urii);
//            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=DistributorStock_Report.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BisId=" + bis_id, 'Distributor Stock Report');
            window.open(singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=DistributorStock_Report_IMEI.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, '_blank');

        }

    },
    openImeiMovement: function () {

        var me = this;

        Ext.create("Ext.window.Window", {
            alias: 'widget.GenerateImeiMovement',
            modal: true,
            resizable: false,
            title: 'IMEI Movement Report',
            items: [
                {
                    xtype: 'textfield',
                    margin: "10 10 10 10",
                    fieldLabel: 'IMEI No:* ',
                    id: 'imeiNumberForMovement'
                },
                {
                    xtype: 'button',
                    margin: "10 10 10 220",
                    text: 'Generate',
                    allowblank: false,
                    buttonAlign: 'center',
                    handler: function (e) {

                        var imei = Ext.getCmp("imeiNumberForMovement").getValue();

                        if (imei != "") {
                            me.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=Imei_Movement.rptdesign&imeiNo=" + imei, 'IMEI Movement Report');
                        } else {
                            Ext.Msg.alert("Information", "Please enter IMEI number");
                        }

                    }
                }
            ]

        }).show();

//        var start = Ext.getCmp('startD').getValue();
//        start = Ext.Date.format(start, 'Y-m-d');
//        var end = Ext.getCmp('endD').getValue();
//        end = Ext.Date.format(end, 'Y-m-d');
//
//
//        var bis_id = Ext.getCmp('location_struct_id').getValue();
//        console.log(start);//to get this value, used the id at view.
//
//        if (bis_id === "" || bis_id === null) {
//            Ext.Msg.alert('Error', 'Please select the location.');
//        } else {
//            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=SDM_ImeiModelNoForPeriod.rptdesign&BIS_ID=" + bis_id + "&FROM_DATE=" + start + "&TO_DATE=" + end, 'Inserted IMEI / Model NO Report');
//        }
    },
    openWrRegReport: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y-m-d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y-m-d');


        var bis_id = Ext.getCmp('location_struct_id').getValue();
        console.log(start);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=SDM_ImeiModelNoForPeriod.rptdesign&BIS_ID=" + bis_id + "&FROM_DATE=" + start + "&TO_DATE=" + end, 'Inserted IMEI / Model NO Report');
        }
    },
    openSalesSummary: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y/m/d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y/m/d');


        var bis_id = Ext.getCmp('location_struct_id').getValue();
        console.log(start);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=SalesReportNew_xls.rptdesign&bisId=" + bis_id + "&fromDate=" + start + "&toDate=" + end;
            localStorage.setItem('myReptExcelURI', urii);
//            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=SalesReportNew.rptdesign&bisId=" + bis_id + "&fromDate=" + start + "&toDate=" + end, 'Sales - Report Summary');
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=SalesReportNew.rptdesign&bisId=" + bis_id + "&fromDate=" + start + "&toDate=" + end, 'Sales - Report Summary');
    
        }
    },
    openshowDistIssueToDsr: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y-m-d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y-m-d');


        var bis_id = Ext.getCmp('location_struct_id').getValue();
        console.log(start);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=Distributor_stock_issues_report_to_DSR_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end, 'Distributor Stock Issues Report To DSR');
        }
    },
    adjustmentReport: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y-m-d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y-m-d');


        var bis_id = Ext.getCmp('location_struct_id').getValue();
        console.log(start);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ADJUSTIMEIREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end, 'Adjustment Report');
        }
    },
    credit_note_Report: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y/m/d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=CREDITNOTEREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end, 'Credit Note Report');
        }
    },
    Device_return_Report: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y/m/d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
//            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DEVICERETURNREPORT.rptdesign&BIS_ID=" + bis_id + "&fromdate=" + start + "&todate=" + end, 'Device Return Report');
//            
//            Ext.getCmp("reportWindowHeader").add({
//                xtype: 'button',
//                text: ".Plain XLS",
//                width: 100,
//                handler: function () {
//                    singer.app.getController("CoreController").displayPlainXlsReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLDeviceReturn.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end);
//                }
//            });

            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=RAWXLDeviceReturn.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
            localStorage.setItem('myReptExcelURI', urii);

            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=DEVICERETURNREPORT.rptdesign&BIS_ID=" + bis_id + "&fromdate=" + start + "&todate=" + end, 'Device Return Report');

        }
    },
    outletrpt: function () {
        var start = Ext.getCmp('startD').getValue();
        start = Ext.Date.format(start, 'Y/m/d');
        var end = Ext.getCmp('endD').getValue();
        end = Ext.Date.format(end, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {

            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=RAWXLOutlet.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
            localStorage.setItem('myReptExcelURI', urii);

            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=RAWXLOutlet.rptdesign&BIS_ID=" + bis_id + "&fromdate=" + start + "&todate=" + end, 'Outlet Report');


        }
//        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=outlets.rptdesign&fromdate=" + start + "&todate=" + end;
//        window.location.href = reporturl;
    },
    openBalanceStockReport: function () { //DONE SH
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Balance_Stock_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
            localStorage.setItem('myReptExcelURI', urii);
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=BALANCESTOCKREPORT.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BIS_ID=" + bis_id, 'Balance Stock Report');

            //plain excel report url set method
//            this.getController("CoreController").setPlainExcelReportParam("BirtReportController?ReportFormat=xls&ReportName=RAWXLBalanceStock.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Sales Report');

            //load plain xls report button and its functionality
//            Ext.getCmp("reportWindowHeader").add({
//                xtype: 'button',
//                text: ".Plain XLS",
//                width: 100,
//                handler: function () {
//                    singer.app.getController("CoreController").displayPlainXlsReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLBalanceStock.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
//                }
//            });


            //load history report button and its functionality
//            Ext.getCmp("reportWindowHeader").add({
//                xtype: 'button',
//                text: "History",
//                width: 100,
//                handler: function () {
//                    singer.app.getController("CoreController").displayHistoryReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLSalesHistory.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
//                }
//            });

        }

    },
    openBalanceStockReportDtl: function () { //DONE SH
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        var bis_id = Ext.getCmp('location_struct_id').getValue();

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
//            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Balance_Stock_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + start + "&DATETWO=" + end;
//            localStorage.setItem('myReptExcelURI', urii);
//            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=BALANCESTOCKREPORT.rptdesign&DATEONE=" + sDate + "&DATETWO=" + eDate + "&BIS_ID=" + bis_id, 'Balance Stock Report');
            window.open(singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=RAWXLBalanceStock.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, '_blank');

        }

    },
    openPurchaseReport: function () {
        console.log('openPurchaseReport');
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
//        sDate = Ext.Date.format(sDate, 'd/m/y');
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
//        eDate = Ext.Date.format(eDate, 'd/m/y');
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        console.log('sDate', sDate);
        console.log('eDate', eDate);

//        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=purchase.rptdesign&startdate=" + sDate + "&enddate=" + eDate;
//        window.location.href = reporturl;

        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            console.log(start);
            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=RAWXLPurchasereport.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate;
            localStorage.setItem('myReptExcelURI', urii);
            this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=PURCHASESTOCKREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Purchase Report');

            //plain excel report url set method
//            this.getController("CoreController").setPlainExcelReportParam("BirtReportController?ReportFormat=xls&ReportName=RAWXLPurchasereport.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Sales Report');

            //load plain xls report button and its functionality
//            Ext.getCmp("reportWindowHeader").add({
//                xtype: 'button',
//                text: ".Plain XLS",
//                width: 100,
//                handler: function () {
//                    
//                  
//                    
//                    singer.app.getController("CoreController").displayPlainXlsReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLPurchasereport.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
//                }
//            });

            //load histor report button and its functionality
            Ext.getCmp("reportWindowHeader").add({
                xtype: 'button',
                text: "History",
                width: 100,
                handler: function () {
                    singer.app.getController("CoreController").displayHistoryReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLPurchaseHistory.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
                }
            });


        }
    },
    openSalesReport: function () {
        console.log('openSalesReport');
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();

//        sDate = Ext.Date.format(sDate, 'd/m/y');
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
//        eDate = Ext.Date.format(eDate, 'd/m/y');
        eDate = Ext.Date.format(eDate, 'Y/m/d');
//        var reporturl = singer.AppParams.REPO + "BirtReportController?ReportFormat=" + singer.AppParams.REPOTYPE + "&ReportName=sales.rptdesign&startdate=" + sDate + "&enddate=" + eDate;
//        window.location.href = reporturl;

        console.log('sDate', sDate);
        console.log('eDate', eDate);

        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            //          var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=RAWXLSalesSummaryNew.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate;                                   

            var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=EXCEL_SALESSTOCKREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate;
            localStorage.setItem('myReptExcelURI', urii);
            // this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=SALESSTOCKREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Sales Report');


            localStorage.setItem('salseExcelRep1', singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=Distributor_stock_issues_report_to_DSR_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
            localStorage.setItem('salseExcelRep2', singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=SalesReport_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
//            this.getController('CoreController').viewSalesReport("BirtReportController?ReportFormat=html&ReportName=SALESSTOCKREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Sales Report');
//            this.getController('CoreController').viewSalesReport("BirtReportController?ReportFormat=html&ReportName=SalesReport_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Sales Report');

            window.open(singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=SalesReport_Excel.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, '_blank');


            //plain excel report url set method
//            this.getController("CoreController").setPlainExcelReportParam("BirtReportController?ReportFormat=xls&ReportName=RAWXLSalesSummaryNew.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Sales Report');

            //load plain xls report button and its functionality
//            Ext.getCmp("reportWindowHeader").add({
//                xtype: 'button',
//                text: ".Plain XLS",
//                width: 100,
//                handler: function () {
//                    singer.app.getController("CoreController").displayPlainXlsReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLSalesSummaryNew.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
//                }
//            });

            //load history report button and its functionality
            Ext.getCmp("reportWindowHeader").add({
                xtype: 'button',
                text: "History",
                width: 100,
                handler: function () {
                    singer.app.getController("CoreController").displayHistoryReport("BirtReportController?ReportFormat=xls&ReportName=RAWXLSalesHistory.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate);
                }
            });



        }

    },
    openInventoryReport: function () {
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'Y/m/d');


        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else {
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=INVENTORYATTRANSITSTOCKREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Inventory At Transits Report');
        }
    },
    openNonMovingInventoryReport: function () {
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        sDate = Ext.Date.format(sDate, 'd/m/y');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        eDate = Ext.Date.format(eDate, 'd/m/y');

        var bis_id = Ext.getCmp('location_struct_id').getValue();
        //console.log(bis_id);//to get this value, used the id at view.

        if (bis_id === "" || bis_id === null) {
            Ext.Msg.alert('Error', 'Please select the location.');
        } else
            this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=NONMOVINGREPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Non Moving Inventory Report');
    },
    warranty_reg_report: function () {
        var start = Ext.getCmp('startD');
        var sDate = start.getValue();
        // sDate = Ext.Date.format(sDate, 'd/m/y');
        sDate = Ext.Date.format(sDate, 'Y/m/d');
        var end = Ext.getCmp('endD');
        var eDate = end.getValue();
        // eDate = Ext.Date.format(eDate, 'd/m/y');
        eDate = Ext.Date.format(eDate, 'Y/m/d');

        console.log('Warranty Dates>>>>> ', sDate, ' : ', eDate);

        var bis_id = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
        if (Ext.getCmp('location_struct_id').getValue() != null && Ext.getCmp('location_struct_id').getValue().length > 0) {
            bis_id = Ext.getCmp('location_struct_id').getValue();
        }


        //this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=WARRANTY_REG_REPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Warrenty Registration Report');

//this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=EXCEL_WARRANTY_REG_REPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Warrenty Registration Report');

        var urii = singer.AppParams.REPO2 + "BirtReportController?ReportFormat=xls&ReportName=EXCEL_WARRANTY_REG_REPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate;
        localStorage.setItem('myReptExcelURI', urii);
        this.getController('CoreController').viewPrintReadyReport_Excep("BirtReportController?ReportFormat=html&ReportName=WARRANTY_REG_REPORT.rptdesign&BIS_ID=" + bis_id + "&DATEONE=" + sDate + "&DATETWO=" + eDate, 'Warrenty Registration Report');


    }

});



