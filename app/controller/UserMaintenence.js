/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.controller.UserMaintenence', {
    extend: 'Ext.app.Controller',
    alias: 'widget.usermaintenence',
    //    requirs:['NewUserController'],
    controllers: ['CoreController', 'Main'],
    stores: [
        'Users', 'UserGroups', 'LoggedInUserData'
    ],
    views: [
        'Administrator.users.UserGrid', 'Administrator.users.NewUser',
        'Administrator.users.UserView', 'Administrator.userGroups.UserGroup'
    ],
    refs: [{
        ref: 'UserGrid',
        selector: 'usergrid'
        }, {
        ref: 'CoreController',
        selector: 'corecontroller'
        }, {
        ref: 'NewUser',
        selector: 'newuser'
        }, {
        ref: 'UserView',
        selector: 'userview'
        }],
    init: function () {
        //        //////console.log(this);
        var me = this;
        me.control({
            'usergrid': {
                added: this.initializeGrid
            },
            'usergrid ': {
                userEdit: this.openUpdateForm
            },
            'usergrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'usergrid button[action=open_window]': {
                click: this.openAddNew
            },
            'usergrid combo[action=search]': {
                change: this.userSearch
            },
            'newuser button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newuser button[action=save]': {
                click: this.updateFormSubmit
            },
            'newuser button[action=nextOne]': {
                click: this.assignOneToUser
            },
            'newuser button[action=nextAll]': {
                click: this.assignAllToUser
            },
            'newuser button[action=prevOne]': {
                click: this.removeOneFromUser
            },
            'newuser button[action=prevAll]': {
                click: this.removeAllFromUser
            },
            'edituser button[action=cancel]': {
                click: this.insertFormCancel
            },
            '  usergrid ': {
                userView: this.moreViewOpen
            },
            'userview button[action=cancel]': {
                click: this.formClose
            },
            'newuser button[action=create]': {
                click: this.onCreate
            },
            'newuser button[action=resetPass]': {
                click: this.onResetPass
            }
            //            'usergrid': {
            //                added: this.initializeGrid
            //            },
            //            'usergrid button[action=open_window]': {
            //                click: this.openAddNew
            //            }
        });
    },
    initializeGrid: function (grid) {



        var userStore = this.getUsersStore();

        // //console.log('setting headers');
        // userStore.getProxy().headers = {
        //     userId:'admin',
        //     room:'DefaultRoom',
        //     department:'DefaultDepartment',
        //     branch:'HeadOffice',
        //     countryCode:'LK',
        //     division:'SingerLK',
        //     organization:'Singer'
        // };
        //        this.getController('Main').setHeaders(userStore);
        //        this.createUserIdsStore();
        this.getController('CoreController').loadBindedStore(userStore);
    },
    insertFormSubmit: function (btn) {
        this.getController('CoreController').onNewRecordCreate(btn, this.getUsersStore(), 'New System User Created.', 'Creating New user', true, true, this.getUserGrid(), false);
    },
    moreViewOpen: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'userview', 'View');
        var grid = Ext.getCmp('UserGroupsGrid');
        var userID = record.get('userId');

        var GroupsStore = Ext.create('Ext.data.Store', {
            model: 'singer.model.UserGroups'
        });
        grid.bindStore(GroupsStore);

        var sys = singer.AppParams.SYSTEM_CODE;
        var loginData = this.getLoggedInUserDataStore();

        Ext.getBody().mask('Loading...', 'large-loading');


        Ext.Ajax.request({
            // url: singer.AppParams.JBOSS_PATH + 'Users/resetUserPassword?userId=' + userID + '&email=' + email + '',
            url: singer.AppParams.JBOSS_PATH + 'Users/getUserGroups',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: "userId=" + userID + "",
            success: function (response) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData);
                GroupsStore.add(responseData.data);

            },
            failure: function () {
                Ext.getBody().unmask();
                Ext.Msg.alert("Error Loading...", "Please try again later");
            }
        });
    },
    openUpdateForm: function (view, record) {
        this.getController('CoreController').openUpdateWindow(record, 'newuser', 'Edit');
        var form = this.getNewUser().down('form').getForm();
        //console.log(form);
        form.findField('email2').setValue(record.get('email'));
        var allUserGroupsStore = this.getUserGroupsStore();
        var userID = record.get('userId');
        var sys = singer.AppParams.SYSTEM_CODE;
        var GroupsStore = Ext.create('Ext.data.Store', {
            model: 'singer.model.UserGroups'
        });

        var loginData = this.getLoggedInUserDataStore();

        this.getController('Main').setHeaders(allUserGroupsStore);
        allUserGroupsStore.load({
            callback: function (records, operation, success) {
                if (operation.success === true) {
                    Ext.Ajax.request({
                        url: singer.AppParams.JBOSS_PATH + 'Users/getUserGroups',
                        headers: {
                            userId: loginData.data.getAt(0).get('userId'),
                            room: loginData.data.getAt(0).get('room'),
                            department: loginData.data.getAt(0).get('department'),
                            branch: loginData.data.getAt(0).get('branch'),
                            countryCode: loginData.data.getAt(0).get('countryCode'),
                            division: loginData.data.getAt(0).get('division'),
                            organization: loginData.data.getAt(0).get('organization'),
                            system: sys,
                            'Content-Type': 'application/json'
                        },
                        params: "userId=" + userID + "",
                        method: "GET",
                        timeout: 10000,
                        success: function (response, options) {
                            var responseData = Ext.decode(response.responseText);
                            var data = responseData.data;
                            ////console.log(data);
                            // form.findField('groups').setValue(data);
                            GroupsStore.add(data);
                            allUserGroupsStore.data.each(function (itemAll) {
                                var allGroupId = itemAll.data['groupId'];
                                GroupsStore.data.each(function (item) {
                                    var groupId = item.data['groupId'];
                                    //                        //console.log(groupId);
                                    if (allGroupId === groupId) {

                                        allUserGroupsStore.remove(itemAll);
                                    }
                                });
                            });
                        },
                        failure: function () {
                            ////console.log('fail');
                        }
                    });
                }
            }
        });
        Ext.getCmp('allUserGroups').bindStore(allUserGroupsStore.load());
        Ext.getCmp("UserGroupsGrid").bindStore(GroupsStore);


        form.findField('userId').setDisabled(true);
        form.findField('nic').hide();

    },
    formClose: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancel: function (btn) {
        Ext.getCmp("UserGroupsGrid").getStore().removeAll();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    openAddNew: function () {
        this.getController('CoreController').openInsertWindow('newuser');
        var userGroupStore = this.getUserGroupsStore();
        this.getController('Main').setHeaders(userGroupStore);
        Ext.getCmp('allUserGroups').bindStore(userGroupStore.load());
        //        this.getController('CoreController').loadBindedStore(userGroupStore);
    },
    updateFormSubmit: function (btn) {
        var assignGrid = Ext.getCmp('UserGroupsGrid');
        //        //console.log('test');
        var groups = assignGrid.getStore();
        var form;
        form = btn.up().up('form').up().up().down('form');
        //console.log(form.getValues());
        if (form.getValues().email !== form.getValues().email2) {
            Ext.Msg.alert("Error", "Email doesn't mach...");
        } else {

            var assignStore = Ext.getCmp('UserGroupsGrid').getStore();
            var temp = [];
            assignStore.data.each(function (item) {

                temp.push(item.data);
                var record = form.getRecord();
                ////console.log(record);
                record.set('groups', temp);
            });
            if (assignStore.getCount() <= 0) {
                Ext.Msg.alert("Error", "No gorups assined to the user");
                //            Ext.getCmp('exportDirtyBtn').setDisabled(true);
            } else {

                this.getController('CoreController').onUserUpdate(form, this.getUsersStore(), 'User Data Updated.', 'Updating User', true, true, this.getUserGrid());
            }
        }

    },
    onCreate: function (btn) {
        ////console.log(btn.up().up('form').up().up('window'));
        var form;
        form = btn.up().up('form').up().up().down('form');
        var assignStore = Ext.getCmp('UserGroupsGrid').getStore();
        if (assignStore.getCount() <= 0) {
            Ext.Msg.alert("Error", "No gorups assined to the user");
            //            Ext.getCmp('exportDirtyBtn').setDisabled(true);
        } else {
            if (form.getValues().email !== form.getValues().email2) {
                Ext.Msg.alert("Error", "Email doesn't mach...");
            } else {
                var answer = [];
                assignStore.data.each(function (item) {
                    answer.push(item.data);
                });
                ////console.log(answer);
                //        var form = btn.up('form').getForm();
                var user = Ext.create('singer.model.LoggedInUser',
                    form.getValues()
                );
                user.set("groups", answer);
                user.set('extraParams', 0);

                this.getController('CoreController').onNewActivityRecordCreate(btn, this.getUsersStore(), 'New User Created.', 'Creating New User', true, true, this.getUserGrid(), user);
                
            }
            //        this.getController('CoreController').onUserCreate(form, this.getUsersStore(), "successMsg", "processMsg", true, true, this.getUserGrid());
        }

    },
    assignOneToUser: function () {
        var allGrid = Ext.getCmp('allUserGroups');
        var assignGrid = Ext.getCmp('UserGroupsGrid');
        var selectionModel = allGrid.getSelectionModel();
        //        //console.log(selectionModel);
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                allGrid.getStore().remove(record);
                assignGrid.getStore().add(record);
            });
        }

    },
    assignAllToUser: function () {
        var allGrid = Ext.getCmp('allUserGroups');
        var assignGrid = Ext.getCmp('UserGroupsGrid');
        var allUserStore = allGrid.getStore();
        var userGroupStore = assignGrid.getStore();

        allUserStore.data.each(function (item) {
            allUserStore.remove(item);
            userGroupStore.add(item);
        });

    },
    removeOneFromUser: function () {
        var allGrid = Ext.getCmp('allUserGroups');
        var assignGrid = Ext.getCmp('UserGroupsGrid');
        var selectionModel = assignGrid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                allGrid.getStore().add(record);
                assignGrid.getStore().remove(record);
            });
        }
    },
    removeAllFromUser: function () {
        var allGrid = Ext.getCmp('allUserGroups');
        var assignGrid = Ext.getCmp('UserGroupsGrid');
        var allUserStore = allGrid.getStore();
        var userGroupStore = assignGrid.getStore();

        userGroupStore.data.each(function (item) {
            allUserStore.add(item);
            userGroupStore.remove(item);
        });
    },
    onClearSearch: function (btn) {
        //        btn.up().down('combobox').setValue('');
        this.getController('CoreController').onClearSearch(btn);
Ext.getCmp('usergrid-searchDrp').reset();
        //        //console.log();
    },
    userSearch: function (btn, value) {
        //        //console.log('searching on ',value );
        var store = this.getUsersStore();
        if (value === '') {
            //            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + "Users/getUsers";
            //console.log('empty');
            this.onClearSearch(btn);
        } else {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + "Users/getUsers?key=userId&value=" + value + "";
        }
        store.load();
    },
    onResetPass: function (btn) {
        var userID = btn.up('form').getForm().findField('userId').getValue();
        var email = btn.up('form').getForm().findField('email').getValue();

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Resetting Users Password...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'Users/resetUserPassword?userId=' + userID + '&email=' + email + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            //            params: JSON.stringify(x),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                if (responseData.flag === '100') {
                    //                    btn.up('form').up('window').destroy();
                    Ext.Msg.alert('Success', responseData.successMessages[0]);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in resetting password", "Please try again later");
            }
        });
    }
    //    initializeGrid: function() {
    //        var userStore = this.getUsersStore();
    //        this.getController('CoreController').loadBindedStore(userStore);
    //        this.registerNewVTypes();
    //    }

});