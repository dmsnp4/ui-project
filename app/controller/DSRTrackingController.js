Ext.define('singer.controller.DSRTrackingController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.dsrtrackingcontroller',
    controllers: [],
    stores: [
        'dsrtracking', 'dsrmovements','LoggedInUserData','dsrmovementsDSRpoints'
    ],
    views: [
        'DSRTrackingModule.EnableUserTracking.EnHomeView', 'DSRTrackingModule.DSRMovementHistory.DSRMovement'
    ],
    refs: [
        {
            ref: 'EnHomeView',
            selector: 'enhomeview'
        }
    ],
    init: function() {
        //Ext.getCmp('anyusername').hide();
        var me = this;
        me.control({
            'enhomeview button[action=getusers]': {
                click: this.onloadDSR
            },
            'enhomeview combo[action=load]': {
                change: this.userLoad
            },
            'enhomeview button[action=submitbtn]':{
                click: this.onSubmitbtn
            }
        });
    },
    onloadDSR: function() {
        
        

        ////console.log(value);
    },
    userLoad: function() {
        var store = Ext.getStore('dsrtracking');
        var value = Ext.getCmp('selectedusers').getValue();
        var grid = Ext.getCmp('userListGrid');
        var me = this;
        store.removeAll();
        var logUser = this.getLoggedInUserDataStore();
        var uid = logUser.data.getAt(0).get('extraParams');
//        //console.log("valuevaluevalue",value);
        
        if(value!==null){
           
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getTrackingEnableList',
                params: "bisTypeId=" + value+'&bisId='+uid,
                method: "GET",
                timeout: 10000,
                success: function(response, options) {
                    grid.show();
                    var responseData = Ext.decode(response.responseText);
                    store.add(responseData.data);
                    ////console.log(store);

                },
                failure: function(response, options) {
                     grid.hide();
                    ////console.log("response> " + response);
                }
            });
        }
        else{
            Ext.Msg.alert('Error !','Please Select the user.');
        }

    },
    
    onSubmitbtn:function(){
        var store = Ext.getStore('dsrtracking');
        var value = Ext.getCmp('selectedusers').getValue();
        var grid = Ext.getCmp('userListGrid');
        var me = this;
        
        if(value!==null){
            var getData=grid.getStore();
            var tempData=[];
            getData.data.each(function(item){
                  tempData.push(item.data);            
            });

            var loginData = this.getLoggedInUserDataStore();
            var sys = singer.AppParams.SYSTEM_CODE;
            Ext.Ajax.request({
            url:singer.AppParams.JBOSS_PATH + 'BussinessTrackings/addEnableTracking',
            method: 'POST',
             headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(tempData),
            success: function (response) {

                var UpdateResponse = Ext.JSON.decode(response.responseText);
//                //console.log("OK> ",UpdateResponse);
                Ext.getBody().mask('Updating', 'Updating Enable/Disable users.');
                Ext.Msg.alert('Done !','Enable Tracking updated.');
                Ext.getBody().unmask();
            },
            failed: function (response) {
                Ext.Msg.alert('Error !','Update error.');
                
            }

            }); 
            
            
            
            
            
        }
        else{
            Ext.Msg.alert('Error !','Please Select the user.');
        }   
    }
});

