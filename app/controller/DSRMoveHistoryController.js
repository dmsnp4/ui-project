Ext.define('singer.controller.DSRMoveHistoryController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.dsrovehistorycontroller',
    map: null,
    markers: [],
    controllers: [],
    stores: [
        'dsrmovementsDSRList', 'dsrmovementsDSRpoints', 'dsrmovements'
    ],
    views: [
        'DSRTrackingModule.DSRMovementHistory.DSRMovement'
    ],
    refs: [
        {
            ref: 'DSRMovement',
            selector: 'dsrovement'
        }
    ],
    init: function () {
        //Ext.getCmp('anyusername').hide();  
        var me = this;
        me.control({
            'dsrovement': {
                added: this.initializeLocation
            },
            ' dsrovement treecombo': {
                change: this.onChangeTreeCombo
            },
            'dsrovement button[action=loadlocations]': {
                click: this.onloadlocations
            },
            'dsrovement combo[action=DistributorloadDSR]': {
                change: this.onDistributorloadDSR
            },
            'dsrovement combo[action=DSRloadLocations]': {
                change: this.onDSRloadLocations
            }
        });
    },
    onChangeTreeCombo: function (comp, rec) {
        console.log(comp, rec);
        this.onDistributorloadDSR();
    },
    initializeLocation: function (cmp) {
        var map = Ext.getCmp('dsrmove');
        this.map = map;
    },
    onDistributorloadDSR: function () {
        var DropDwn = Ext.getCmp('DistributorDSRset');
        DropDwn.show();
        Ext.getCmp('DistributorDSRset').setValue('');
        var store = Ext.getStore('dsrmovementsDSRList');
        var value = Ext.getCmp('selctdistributor').getValue();
        //console.log("store > ", store);
        ////console.log("value > ", value);
        var me = this;
        store.removeAll();

        if (value !== null) {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownList?bisId=' + value;
            ////console.log(store);
            //store.load();
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownListWithoutBussCategory',
                params: "bisId=" + value,
                method: "GET",
                timeout: 10000,
                success: function (response, options) {
                    var responseData = Ext.decode(response.responseText);
                    store.add(responseData.data);
                    ////console.log(store);

                },
                failure: function (response, options) {
                    ////console.log("response> " + response);
                }
            });
        }
        else {
            Ext.Msg.alert('Error !', 'Please Select the user.');
        }

    },
    onloadlocations: function () {

        var me = this;
        var map = this.map;
        var mark = this.markers;
        var store = this.getDsrmovementsDSRpointsStore();//Ext.getStore('dsrmovementsDSRpoints');
        var dsr = Ext.getCmp('DistributorDSRset').getValue();
        console.log('dsr', dsr);
        var date = Ext.getCmp('DSRmvDate').getSubmitValue();
        var fromtime = Ext.Date.format(Ext.getCmp('DSRmvFromTime').getValue(), 'H:i');
        var totime = Ext.Date.format(Ext.getCmp('DSRmvToTime').getValue(), 'H:i');
        //store.removeAll();
        var fromDate = date + ' ' + fromtime;
        var toDate = date + ' ' + totime;
        var arg = 'fromDate=' + fromDate + '&toDate=' + toDate + "&userId=" + dsr;

        me.clearMap(store);
        Ext.getBody().mask('Establishing Connection', 'large-loading');
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getTrackingHistoryList',
            params: arg,
            method: "GET",
            timeout: 10000,
            success: function (response, options) {

                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                if (responseData.totalRecords === 0) {
                    Ext.Msg.alert('Error !', 'No history records.');
                    store.removeAll();
                    for (var i = 0; i < mark.length; i++) {
                        ////console.log(mark[i]);
                        mark[i].setMap(null);
                    }

                }
                else {
                    store.add(responseData.data);
                    me.BuildPath(store.data);
                }

            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                form.getForm().reset();

            }
    });
    },
    BuildPath: function (StoreData) {

        var a;
        var me = this;
        var map = this.map;
        var bounds = new google.maps.LatLngBounds();
        var points = StoreData.items;
        var sqNumber = 0;
        var stroke = '#000000';
        var pointCol = '#252525';
        var scale = 4;
        var PathPoints = [];
        var path = this.markers;
        var infowindow = new google.maps.InfoWindow();
        for (a = 0; a < points.length; a++) {
            sqNumber = a + 1;
            //console.log("sqNumber");
            if (sqNumber === 1) {
                pointCol = '#b40303';
                scale = 6;
            }
            else if (sqNumber === points.length) {
                pointCol = '#00a427';
                scale = 6;
            }
            else {
                pointCol = '#252525';
                scale = 4;
            }
            ////console.log(points[a][0],points[a][1]);
            var pos = new google.maps.LatLng(points[a].data.latitite, points[a].data.logtitute);
            PathPoints.push(pos);
            bounds.extend(pos);
            marker = new google.maps.Marker({
                position: pos,
                //animation: google.maps.Animation.DROP,
//                icon: singer.AppParams.BASE_PATH+'/resources/images/'+pointIcon+'.png',

                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: scale,
                    strokeWeight: 1,
                    fillOpacity: 0.8,
                    fillColor: pointCol,
                    strokeColor: stroke
                },
                map: map.gmap});
            path.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, a) {
                return function () {
                    var data = '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h1 id="firstHeading" class="firstHeading">DSR ID: ' + points[a].data.bisId + '</h1>' +
                            '<div id="bodyContent">' +
                            '<p><b>Name of the DSR:</b> ' + points[a].data.bisName + '</p>' +
                            '<p><b>Business Discription:</b> ' + points[a].data.bisDesc + '</p>' +
                            '<p><b>Telephone number:</b> ' + points[a].data.teleNumber + '</p>' +
                            '<p><b>Address:</b> ' + points[a].data.address + '</p>' +
                            '(<b>last visited:</b> ' + points[a].data.visitDate + ').</p>' +
                            '</div>' +
                            '</div>';
                    infowindow.setContent(data);
                    infowindow.close();
                    infowindow.open(map.gmap, marker);

                }

            })(marker, a));
        }












//------------------------------------------------------------------------------        

//        var a;
//        var me = this;
//        var map = this.map;
//        var bounds = new google.maps.LatLngBounds();
//        var points=StoreData.items;
//        var sqNumber=0;
//        var pointCol='252525';
//        var pointNum='FFFFFF';
//        var PathPoints=[];
//        var path =this.markers;
//        var infowindow = new google.maps.InfoWindow();
//        for (a = 0; a < points.length; a++) {
//            sqNumber = a + 1;
//            ////console.log(sqNumber);
//            if(sqNumber===1){
//                pointCol='b40303';
//                pointNum='FFFFFF';
//            }
//            else if(sqNumber===points.length){
//                pointCol='00a427';
//                pointNum='FFFFFF';
//            }
//            else{
//                pointCol='252525';
//                pointNum='ffcc00';
//            }
//            ////console.log(points[a][0],points[a][1]);
//            var pos = new google.maps.LatLng(points[a].data.latitite, points[a].data.logtitute);
//            PathPoints.push(pos);
//            bounds.extend(pos);
//             marker = new google.maps.Marker({
//                    position: pos,
//                    animation: google.maps.Animation.DROP,
//                    icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + sqNumber + '|'+pointCol+'|'+pointNum,
//                    shadow: 'https://chart.googleapis.com/chart?chst=d_map_pin_shadow',
//                    map: map.gmap});
//                    path.push(marker);
//                google.maps.event.addListener(marker, 'click', (function(marker, a) {
//                    return function() {
//                        var data = '<div id="content">' +
//                                '<div id="siteNotice">' +
//                                '</div>' +
//                                '<h1 id="firstHeading" class="firstHeading">DSR ID: ' + points[a].data.bisId + '</h1>' +
//                                '<div id="bodyContent">' +
//                                '<p>Name of the DSR: ' + points[a].data.bisName + '</p>' +
//                                '<p>Business Discription: ' + points[a].data.bisDesc + '</p>' +
//                                '<p>Telephone number: ' + points[a].data.teleNumber + '</p>' +
//                                '<p>Address: ' + points[a].data.address + '</p>' +
//                                '(last visited ' + points[a].data.visitDate + ').</p>' +
//                                '</div>' +
//                                '</div>';
//                        infowindow.setContent(data);
//                        infowindow.close();
//                        infowindow.open(map.gmap, marker);
//
//                    }
//
//                })(marker, a));
//        }
//        //console.log(PathPoints);


//------------------------------------------------------------------------        

//        var flightPath = new google.maps.Polyline({
//            path: path,
//            geodesic: false,
//            strokeColor: '#d20000',
//            strokeOpacity: 0.7,
//            strokeWeight: 2,
//            
//        });

//-------------------------------------------------------
//        var rendererOptions = {
//            polylineOptions:{strokeColor:'#d20000'},
//            strokeOpacity: 0.7,
//            strokeWeight: 2,
//            //suppressMarkers: true
//        };
//        var directionsService = new google.maps.DirectionsService();
//        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
//        directionsDisplay.setMap(map.gmap);
//        
//        
//        
//
////        var j;
////        var waypts = [];
////        for(j=1;j<PathPoints.length-1;j++){            
////              waypts.PathPoints({location: PathPoints[j],
////                           stopover: true});
////        }
//
//        ////console.log(waypts);
//
//        var request = {
//            origin:PathPoints[0],
//            destination:PathPoints[PathPoints.length - 1],
//           // waypoints: waypts,
//            travelMode: google.maps.DirectionsTravelMode.DRIVING,
//        };
//        
//        directionsService.route(request, function(response, status) {
//            if (status === google.maps.DirectionsStatus.OK) {
//                directionsDisplay.setDirections(response);
//            }
//            else{
//                directionsDisplay.setDirections({ routes: [] });
//            }
//        });

//-----------------------------------------------------------

        //flightPath.setMap(map.gmap);
    },
    clearMap: function (store) {

        var map = this.map;
        var mark = this.markers;
        store.removeAll();
        //        //console.log(map.gmap);
        for (var i = 0; i < mark.length; i++) {
            mark[i].setMap(null);
        }

//        var map = this.map;
//        var mark = this.markers;
//        store.removeAll();
//        //console.log(map.gmap);
//        for (var i = 0; i < mark.length; i++) {
//            mark[i].setMap(null);
//        }
    },
    dsrInfo: function (map, mkr, infowindow, dsrid, time) {
        //Ext.Msg.alert('DSR ID- '+dsrid, 'Time- '+time);
        ////console.log(mkr);

    },
    onDSRloadLocations: function () {
        var btn = Ext.getCmp('hisLoadBtn');
        btn.show();
        Ext.getCmp('DSRmvDate').show();
        Ext.getCmp('DSRmvFromTime').show();
        Ext.getCmp('DSRmvToTime').show();
    }
});

