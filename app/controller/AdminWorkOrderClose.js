



Ext.define('singer.controller.AdminWorkOrderClose', {
    extend: 'Ext.app.Controller',
    controllers: ['CoreController'],
    stores: [
        'GetWorkOrderClose', 'LoggedInUserData'
    ],
    views: [
        'Administrator.AdminWorkOrderClose.MainGrid',
        'Administrator.AdminWorkOrderClose.AddNew',
        'Administrator.AdminWorkOrderClose.View'
    ],
    refs: [
        
    ],
    init: function () {
        var me = this;
        me.control({
            'admin_wo_close': {
                added: this.initializeGrid
            },
            'admin_wo_close button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'admin_wo_close button[action=add_new]': {
                click: this.addNewWorkOrderCancelForm
            },
            'admin_add_wo_close button[action=work_order_closing_cancel]': {
                click: this.cancelAddnewForm
            },
             'admin_wo_close  ': {
                onView: this.onView
            },
             'admin_wo_close_view button[action=View_cancel]': {
                click: this.cancelViewWorkOpeningForm
            },
             ' admin_add_wo_close button[action=addNewSubmit]': {
                click: this.AddNewWorkOrderClosing
            },

        });
    },

    AddNewWorkOrderClosing: function (btn, record) {
        ////console.log('AddNewWorkOrderClosing');
        var me = this;
        var windw = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
        var store = this.getGetWorkOrderCloseStore();
        var fomval = form.getValues();
        fomval.status='3';
        
        console.log('Form Val.............');
        
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/addWorkOrderClose',
            method: 'PUT',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(fomval),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData);
                if (responseData.returnFlag === '1') {
                    Ext.Msg.confirm('Print the receipt', 'Are you sure you want to Print the receipt?', function(btn) {
                    if (btn === 'yes') {
                            singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                            windw.destroy();
                            singer.LayoutController.notify("Success", "Closed work order.<br> Email has been sent");     
                            store.load({
                                    callback: function (records, operation, success) {
                                        var wonm = records[0].data.workOrderNo;
                                        var loggeduser = loginData.data.items[0].data.firstName;
                                        var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=woclose.rptdesign&WOnumber=" + wonm +'&User'+loggeduser;
                                        var ajaxreq;
                                        function makeHttpObject() {
                                            try {
                                                return new XMLHttpRequest();
                                            }
                                            catch (error) {
                                            }
                                            try {
                                                return new ActiveXObject("Msxml2.XMLHTTP");
                                            }
                                            catch (error) {
                                            }
                                            try {
                                                return new ActiveXObject("Microsoft.XMLHTTP");
                                            }
                                            catch (error) {
                                            }

                                            throw new Error("Could not create HTTP request object.");
                                        }
                                        var request = makeHttpObject();
                                        request.open("GET", URI, true);
                                        request.send(null);
                                        request.onreadystatechange = function () {
                                            if (request.readyState == 4) {
                                                ajaxreq = request.responseText;
                                                var myWindow = window.open('', '', 'width=500,height=500');
                                                myWindow.document.write('<html><head>');
                                                myWindow.document.write('<title>' + '' + '</title>');
                                                myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                                                myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                                                myWindow.document.write('</head><body>');
                                                myWindow.document.write('<div>' + ajaxreq + '</div>');
                                                myWindow.document.write('</body></html>');

                                                setInterval(function () {
                                                myWindow.print();
                                                }, 3000);
                                            }
                                        };
                                                    }
                                                });
                                        }else{
                                            singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                                            windw.destroy();
                                            singer.LayoutController.notify("Success", "Closed work order.<br> Email has been sent");     
                                            store.load();
                                        }
                });
                    
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },

    cancelViewWorkOpeningForm: function (btn) {
        var formcancel = btn.up('window');
        formcancel.destroy();
    },
    cancelAddnewForm: function (btn) {
        var formcancel = btn.up('window');
        formcancel.destroy();
    },
    
    addNewWorkOrderCancelForm: function () {
//        ////console.log('open quick work order');
        var form = Ext.widget('admin_add_wo_close');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        Ext.getCmp('RstsREMARK').hide();
        Ext.getCmp('addNewWorkOrderclosing-save').hide();
        win.show();
    },
    
    
    
    initializeGrid: function (grid) {
        var store = this.getGetWorkOrderCloseStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    
    addNewWorkOrderFormCancel: function (btn) {
        var form = btn.up('form').up().up();
        form.close();

    },
    
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    
    onView: function (view, record) {
        var curierNum = record.data.courierNo;
        this.getController('CoreController').onOpenViewMoreWindow(record, 'admin_wo_close_view', 'View');
        if(curierNum !== ''){
            Ext.getCmp('currierrNo').show();
        }
    }
});

