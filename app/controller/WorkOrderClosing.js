


Ext.define('singer.controller.WorkOrderClosing', {
    extend: 'Ext.app.Controller',
    controllers: ['CoreController'],
    stores: [
        'GetWorkOrderClose', 'LoggedInUserData'
    ],
    views: [
        'ServiceModule.WorkOrderClosing.MainGrid',
        'ServiceModule.WorkOrderClosing.AddNew',
        'ServiceModule.WorkOrderClosing.View',
        'ServiceModule.WorkOrderClosing.Edit'
    ],
    refs: [
        {
            ref: 'MainGrid',
            selector: 'wrkOrderClosingGrid'
        },
        {
            ref: 'AddNew',
            selector: 'addNewWorkOrderclosing'
        },
        {
            ref: 'View',
            selector: 'WOclosingView'
        },
        {
            ref: 'Edit',
            selector: 'editWorkOrderclosing'
        },
    ],
    init: function () {
        var me = this;
        me.control({
            'wrkOrderClosingGrid': {
                added: this.initializeGrid
            },
            ' wrkOrderClosingGrid': {
                prntRecipt: this.PrintReceipt
            },
            'wrkOrderClosingGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'wrkOrderClosingGrid button[action=add_new]': {
                click: this.addNewWorkOrderCancelForm
            },
            'addNewWorkOrderclosing button[action=work_order_closing_cancel]': {
                click: this.cancelAddnewForm
            },
            'editWorkOrderclosing button[action=Edit_work_order_closing_cancel]': {
                click: this.cancelEditForm
            },
            'wrkOrderview button[action=view_wrk_order_cancel]': {
                click: this.closeViewWorkOrder
            },
            'wrkOrderClosingGrid  ': {
                onView: this.onView
            },
            ' wrkOrderClosingGrid  ': {
                onedit: this.EditWorkOrderClose
            },
            'WOclosingView button[action=View_cancel]': {
                click: this.cancelViewWorkOpeningForm
            },
//            'addNewWorkOrderclosing button[action=search_Work_Order]': {
//                click: this.searchWorkOrderNo
//            },
            ' addNewWorkOrderclosing button[action=addNewSubmit]': {
                click: this.AddNewWorkOrderClosing
            },
            ' addNewWorkOrderclosing button[action=editNewSubmit]': {
                click: this.SubmitEdit
            },
            'wrkOrderClosingGrid ': {
                ondelete: this.onDeletee
            },
        });
    },
    
    PrintReceipt: function (veiw, record) {
        var wrkNo = record.data.workOrderNo;
        //console.log(wrkNo);
        var loginData = this.getLoggedInUserDataStore();
        //console.log(loginData.data.items[0].data);
        var loggeduser = loginData.data.items[0].data.firstName;
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=woclose.rptdesign&WOnumber=" + wrkNo +'&User'+loggeduser , 'Work Order Close');
    },
    
    onDeletee: function (btn, record) {
//        ////console.log(record);
        Ext.Msg.confirm("Confirmation", "Do you want to delete?", function (btnText) {
            if (btnText === "yes") {
                var store = this.getGetWorkOrderCloseStore();
                var x = record.data;
                var loginData = this.getLoggedInUserDataStore();
                var sys = singer.AppParams.SYSTEM_CODE;
                Ext.Msg.wait('Please wait', 'Deleting Data...', {interval: 300});
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/deleteWorkOrderClose',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(x),
                    success: function (response) {
                        Ext.Msg.hide();
                        var responseData = Ext.decode(response.responseText);
                        //////console.log(responseData);
                        if (responseData.returnFlag === '1') {
                        }
                        store.load();
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
            else if (btnText === "no") {
            }
        }, this);
    },
    
    SubmitEdit: function (btn) {
        ////console.log('SubmitEdit');
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
        var fomval = form.getValues();
        fomval.status='3';
        var store = this.getGetWorkOrderCloseStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/editWorkOrderClose',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(fomval),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    singer.LayoutController.notify('Record Updated.', responseData.returnMsg);
                    window.destroy();
                    store.load();
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    
    EditWorkOrderClose: function (grid, record) {
        //console.log(record.data.deleverDate);
        var win = Ext.widget('addNewWorkOrderclosing');
        
        var formWindow = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Edit ',
            items: [win]
        });
        ////console.log(record.data);
        formWindow.show();
        var form = formWindow.down('form');
        form.getForm().loadRecord(record);
        
        var deleverDate = new Date(record.data.deleverDate);
       form.getForm().findField('deleverDate').setValue(deleverDate);
       form.getForm().findField('repairid').setReadOnly(true);
       
//       Ext.getCmp('repairNAME').disable();
       
        Ext.getCmp('addNewWorkOrderclosing-create').hide();
        win.show();
    },
    AddNewWorkOrderClosing: function (btn, record) {
        var me = this;
        var windw = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
        var store = this.getGetWorkOrderCloseStore();
        var fomval = form.getValues();
        fomval.status='3';
        
        
       // console.log(fomval);
        
        
        if(Ext.getCmp('resolveDefect').getValue() === true &&
                Ext.getCmp('visualInspectionTest').getValue() === true &&
                Ext.getCmp('engineeringTest').getValue() === true &&
                Ext.getCmp('callSms').getValue() === true &&
                Ext.getCmp('accessories_delivered').getValue() === true){
            
                    Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WorkOrderCloses/addWorkOrderClose',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(fomval),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        //console.log(responseData);
                        if (responseData.returnFlag === '1') {
                            Ext.Msg.confirm('Print the receipt', 'Are you sure you want to Print the receipt?', function(btn) {
                            if (btn === 'yes') {
                                    singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                                    windw.destroy();
                                    singer.LayoutController.notify("Success", "Closed work order.<br> Email has been sent");     
                                    store.load({
                                            callback: function (records, operation, success) {
                                                var wonm = records[0].data.workOrderNo;
                                                var loggeduser = loginData.data.items[0].data.firstName;
                                                var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=woclose.rptdesign&WOnumber=" + wonm +'&User'+loggeduser;
                                                var ajaxreq;
                                                function makeHttpObject() {
                                                    try {
                                                        return new XMLHttpRequest();
                                                    }
                                                    catch (error) {
                                                    }
                                                    try {
                                                        return new ActiveXObject("Msxml2.XMLHTTP");
                                                    }
                                                    catch (error) {
                                                    }
                                                    try {
                                                        return new ActiveXObject("Microsoft.XMLHTTP");
                                                    }
                                                    catch (error) {
                                                    }

                                                    throw new Error("Could not create HTTP request object.");
                                                }
                                                var request = makeHttpObject();
                                                request.open("GET", URI, true);
                                                request.send(null);
                                                request.onreadystatechange = function () {
                                                    if (request.readyState == 4) {
                                                        ajaxreq = request.responseText;
                                                        var myWindow = window.open('', '', 'width=500,height=500');
                                                        myWindow.document.write('<html><head>');
                                                        myWindow.document.write('<title>' + '' + '</title>');
                                                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                                                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                                                        myWindow.document.write('</head><body>');
                                                        myWindow.document.write('<div>' + ajaxreq + '</div>');
                                                        myWindow.document.write('</body></html>');

                                                        setInterval(function () {
                                                        myWindow.print();
                                                        }, 3000);
                                                    }
                                                };
                                                            }
                                                        });
                                                }else{
                                                    singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                                                    windw.destroy();
                                                    singer.LayoutController.notify("Success", "Closed work order.<br> Email has been sent");     
                                                    store.load();
                                                }
                        });

                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }

                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            
                }else{
                     Ext.Msg.alert("Warning", "Please Check the Quality Check List.");
                }
        
        
    },
//    searchWorkOrderNo: function (btn, record) {
//        var wrkNo = Ext.getCmp('wrkOrdNo').getValue();
//        ////console.log(wrkNo);
//
//        var window = btn.up('window');
//        var loginData = this.getLoggedInUserDataStore();
//        var form = btn.up('form');
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'WorkOrderTransfers/verifyWorkOrder?workOrderNo=' + wrkNo,
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
//            success: function (response, btn) {
//                var responseData = Ext.decode(response.responseText);
//                if (responseData.returnFlag === '1') {
//                    Ext.getCmp('veryfy').show();
//                } else {
//                    singer.LayoutController.createErrorPopup("Insert Existing Work Order.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                    Ext.getCmp('wrkOrdNo').reset();
//                    Ext.getCmp('veryfy').hide();
//                }
//            },
//            failure: function () {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
//    },
    cancelEditForm: function (btn) {
        var formcancel = btn.up('window');
        formcancel.destroy();
    },
    cancelViewWorkOpeningForm: function (btn) {
        var formcancel = btn.up('window');
        formcancel.destroy();
    },
    cancelAddnewForm: function (btn) {
        var formcancel = btn.up('window');
        formcancel.destroy();
    },
    cancelEditWorkOrderForm: function (btn) {
        var frm = btn.up('window');
        frm.destroy();
//        Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function(btnn) {
//                    if (btnn === "yes") {
//                          frm.destroy();
//                    } else {
//                    }
//                });
    },
    closeViewWorkOrder: function (btn) {
        var frm = btn.up('window');
        frm.destroy();
    },
    addNewWorkOrderCancelForm: function () {
//        ////console.log('open quick work order');
        var form = Ext.widget('addNewWorkOrderclosing');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        Ext.getCmp('RstsREMARK').hide();
        Ext.getCmp('addNewWorkOrderclosing-save').hide();
        win.show();
    },
    openRepairHistoryGrid: function () {
        var form = Ext.widget('repairHistoryGrid');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Repair History',
            items: [form]
        });
        win.show();
    },
    openRegisterNewIMEIForm: function () {
        var form = Ext.widget('newIMEIregister');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Register New IMEI',
            items: [form]
        });
        win.show();
    },
    editNewWorkOrderOpeningWindow: function () {
        var form = Ext.widget('addNewWorkOrderclosing');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Edit Work Order',
            items: [form]
        });
        win.show();
    },
    initializeGrid: function (grid) {
        var store = this.getGetWorkOrderCloseStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    addNewWorkOrderFormCancel: function (btn) {

        var form = btn.up('form').up().up();
        form.close();

//        Ext.Msg.confirm("Save Changes?", "You want to close form without saving?", function(btnn) {
//                    if (btnn === "yes") {
//                          form.close();
//                    } else {
//                    }
//                });
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        //console.log('comming to view');
        //console.log(record.data.courierNo);
        var curierNum = record.data.courierNo;
        this.getController('CoreController').onOpenViewMoreWindow(record, 'WOclosingView', 'View');
        if(curierNum !== ''){
            Ext.getCmp('currierrNo').show();
        }
    },
});

