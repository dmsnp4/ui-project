Ext.define('singer.controller.ReportsExcelController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.reportsmenucontroller',
    controllers: ['CoreController', 'Main'],
    stores: [
        'LoggedInUserData', 'ReadExcel'
    ],
    views: [
        'ReportsModule.ReportsMenu', 'ReportsModule.addReportBook', 'ReportsModule.GenerateReport'
    ],
    refs: [
        {
            ref: 'ReportsMenu',
            selector: 'reportsmenu'
        },
        {
            ref: 'addReportBook',
            selector: 'addreportbook'
        },
        {
            ref: 'CoreController',
            selector: 'corecontroller'
        },
        {
            ref: 'GenerateReport',
            selector: 'GenerateReport'
        },
    ],
    init: function () {
        var me = this;
        me.control({
            'reportsmenu': {
                added: this.initializeGrid
            },
            'reportsmenu ': {
                genExcel: this.openUpdateFormNew
            },
            'reportsmenu  ': {
                EditRep: this.openEditReportWindow
            },
            'reportsmenu button[action=addNewReportBookWindow]': {
                click: this.openAddNewReportBookWindow
            },
            'addReportBook button[action=saveExcelData]': {
                click: this.saveExcelData
            },
            'addReportBook button[action=create]': {
                click: this.onCreate
            },
            'addReportBook button[action=save]': {
                click: this.onSave
            },
            'GenerateReport button[action=generateReport]': {
                click: this.generateReport
            }

        });
    },
    initializeGrid: function (grid) {
        var readExcel = this.getReadExcelStore();
        readExcel.load();
    },
    openAddNewReportBookWindow: function (btn) {
        this.getController('CoreController').openInsertWindow('addReportBook');
    },
    openEditReportWindow: function (view, record) {//reportService/updateexcelreport
        this.getController('CoreController').openUpdateWindow(record, 'addReportBook', 'Edit Report');

    },
    openUpdateForm: function (view, record) {
        var qryVar = record.data.reportQuery;
        var qry = [];
        qry.push({'reportQuery': qryVar});

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'reportService/getData',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(qry),
            success: function (response) {
                var results = response;
                var json = eval(results);

                var results = atob(response.responseText);

                window.open("data:application/csv;base64," + response.responseText, "_blank");


                if (response.status === 200) {

                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", "Problem with report data", Ext.MessageBox.WARNING);
                }
            },
            failure: function () {
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    openUpdateFormNew: function (view, record) {
        var qryVar = record.data.reportQuery;
        var reptName = record.data.reportName;
        var cond1 = record.data.cond1;
        var cond2 = record.data.cond2;
        var cond3 = record.data.cond3;
        var cond4 = record.data.cond4;
        var cond5 = record.data.cond5;
        var cond6 = record.data.cond6;
        
        var cond1a = record.data.cond1a;
        var cond2a = record.data.cond2a;
        var cond3a = record.data.cond3a;
        var cond4a = record.data.cond4a;
        var cond5a = record.data.cond5a;
        var cond6a = record.data.cond6a;

        localStorage.setItem("qryVar", qryVar);
        localStorage.setItem("reptName", reptName);
        localStorage.setItem("cond1", cond1);
        localStorage.setItem("cond2", cond2);
        localStorage.setItem("cond3", cond3);
        localStorage.setItem("cond4", cond4);
        localStorage.setItem("cond5", cond5);
        localStorage.setItem("cond6", cond6);
        
        localStorage.setItem("cond1a", cond1a);
        localStorage.setItem("cond2a", cond2a);
        localStorage.setItem("cond3a", cond3a);
        localStorage.setItem("cond4a", cond4a);
        localStorage.setItem("cond5a", cond5a);
        localStorage.setItem("cond6a", cond6a);


        //  console.log(cond6.length,' - SSSSSSSS: ', cond6);

        this.getController('CoreController').openInsertWindow('GenerateReport');
        
        if(localStorage.getItem("cond1") === ""){
            Ext.getCmp("sql-cond1").setVisible(false);
            Ext.getCmp("sql-cond1").allowBlank = true;
        }
        
        if(localStorage.getItem("cond2") === ""){
            Ext.getCmp("sql-cond2").setVisible(false);
            Ext.getCmp("sql-cond2").allowBlank = true;
        }
        
        if(localStorage.getItem("cond3") === ""){
            Ext.getCmp("sql-cond3").setVisible(false);
            Ext.getCmp("sql-cond3").allowBlank = true;
        }
        
        if(localStorage.getItem("cond4") === ""){
            Ext.getCmp("sql-cond4").setVisible(false);
            Ext.getCmp("sql-cond4").allowBlank = true;
        }
        
        if(localStorage.getItem("cond5") === ""){
            Ext.getCmp("sql-cond5").setVisible(false);
            Ext.getCmp("sql-cond5").allowBlank = true;
        }
        
        if(localStorage.getItem("cond6") === ""){
            Ext.getCmp("sql-cond6").setVisible(false);
            Ext.getCmp("sql-cond6").allowBlank = true;
        }
   
        Ext.getCmp("GenerateReport-create").setVisible(false);

        //   window.open(singer.AppParams.JBOSS_PATH + 'reportService/usertasksexcel?query=' + qryVar + '&reportName=' + reptName + ' &condition1=' +cond1 + ' &condition2=' +cond2+ ' &condition2=' +cond2+ ' &condition3=' +cond3+ ' &condition4=' +cond4+ ' &condition5=' +cond5 + ' &condition6=' +cond6, "_blank");

    },
    generateReport: function (view, record) {
        console.log("Generate report action call....");
        
        var form = view.up('form').getForm();

        var qryVar = localStorage.getItem("qryVar");
        var reptName = localStorage.getItem("reptName");

        var cond1 = localStorage.getItem("cond1") + "='" + Ext.getCmp("sql-cond1").getValue() + "'";
        var cond2 = localStorage.getItem("cond2") + "='" + Ext.getCmp("sql-cond2").getValue() + "'";
        var cond3 = localStorage.getItem("cond3") + "='" + Ext.getCmp("sql-cond3").getValue() + "'";
        var cond4 = localStorage.getItem("cond4") + "='" + Ext.getCmp("sql-cond4").getValue() + "'";
        var cond5 = localStorage.getItem("cond5") + "='" + Ext.getCmp("sql-cond5").getValue() + "'";
        var cond6 = localStorage.getItem("cond6") + "='" + Ext.getCmp("sql-cond6").getValue() + "'";

        console.log("cond1",cond1);
        console.log("cond2",cond2);
        console.log("cond3",cond3);

        window.open(singer.AppParams.JBOSS_PATH + 'reportService/usertasksexcel?query=' + qryVar + '&reportName=' + reptName + ' &condition1=' + cond1 + ' &condition2=' + cond2 + ' &condition3=' + cond3 + ' &condition4=' + cond4 + ' &condition5=' + cond5 + ' &condition6=' + cond6, "_blank");

    },
    saveExcelData: function (btn) {
        var excel = Ext.getCmp('image_path_srv');
        var me = this;

        var siteId = Ext.getStore('LoggedInUserData').getAt(0).data.extraParams;
        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;

    },
    insertFormSubmit: function (btn) {
        this.getController('CoreController').onNewRecordCreate(btn, this.getOpenBatchStore(), 'New System User Created.', 'Creating New user', true, true, this.getUserGrid(), false);
    },
    formClose: function (btn) {
        var form;
        form = btn.up().up('form').up();
        form.close();
    },
    onCreate: function (btn) {
        var me = this;

        var Report = Ext.create('singer.model.ReadExcelM');
        var qry = Ext.getCmp('repQuery').getValue().toUpperCase();
        ;
        //SQL injection validations
        if ((qry.indexOf("INSERT") > -1) || (qry.indexOf("DELETE") > -1) || (qry.indexOf("UPDATE") > -1)
                || (qry.indexOf("DROP") > -1) || (qry.indexOf("TRUNCATE") > -1)) {
            Ext.Msg.alert("Error", "Please insert correct sql query.");
        } else if ((qry.indexOf("SELECT") > -1) && (qry.indexOf("FROM") > -1)) {

            var repName = Ext.getCmp('repName').getValue();
            var repQuery = Ext.getCmp('repQuery').getValue();
            var status = Ext.getCmp('status').getValue();
            var cond1 = Ext.getCmp('sql-cond1').getValue();
            var cond2 = Ext.getCmp('sql-cond2').getValue();
            var cond3 = Ext.getCmp('sql-cond3').getValue();
            var cond4 = Ext.getCmp('sql-cond4').getValue();
            var cond5 = Ext.getCmp('sql-cond5').getValue();
            var cond6 = Ext.getCmp('sql-cond6').getValue();

            var cond1a = Ext.getCmp('sql-cond1a').getValue();
            var cond2a = Ext.getCmp('sql-cond2a').getValue();
            var cond3a = Ext.getCmp('sql-cond3a').getValue();
            var cond4a = Ext.getCmp('sql-cond4a').getValue();
            var cond5a = Ext.getCmp('sql-cond5a').getValue();
            var cond6a = Ext.getCmp('sql-cond6a').getValue();

            Report.set("reportName", repName);
            Report.set("reportQuery", repQuery);
            Report.set("status", status);
            Report.set("cond1", cond1);
            Report.set("cond2", cond2);
            Report.set("cond3", cond3);
            Report.set("cond4", cond4);
            Report.set("cond5", cond5);
            Report.set("cond6", cond6);

            Report.set("cond1a", cond1a);
            Report.set("cond2a", cond2a);
            Report.set("cond3a", cond3a);
            Report.set("cond4a", cond4a);
            Report.set("cond5a", cond5a);
            Report.set("cond6a", cond6a);
            
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'reportService/addexcelreport',
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(Report.data),
                success: function (response) {
                    var Response = Ext.JSON.decode(response.responseText);

                    if (Response === 1) {
                        btn.up('form').up('window').destroy();
                        Ext.getBody().mask('Adding...', 'Adding Report');
                        Ext.Msg.alert('Done !', 'New report Added.');
                        Ext.getBody().unmask();

                        var readExcel = me.getReadExcelStore();
                        readExcel.load();

                    } else
                        Ext.Msg.alert('Error !', 'Error occoured while adding report.');

                },
                failed: function (response) {
                    Ext.Msg.alert('Error !', 'Update error.');
                }

            });
        } else {
            Ext.Msg.alert("Error", "Please insert correct sql query.");
        }
    },
    onSave: function (btn) {
        var me = this;

        var repID = btn.up('form').getForm()._record.data.reportID;

        var Report = Ext.create('singer.model.ReadExcelM');
        var qry = Ext.getCmp('repQuery').getValue().toUpperCase();
        ;
        //SQL injection validations
        if ((qry.indexOf("INSERT") > -1) || (qry.indexOf("DELETE") > -1) || (qry.indexOf("UPDATE") > -1)
                || (qry.indexOf("DROP") > -1) || (qry.indexOf("TRUNCATE") > -1)) {
            Ext.Msg.alert("Error", "Please insert correct sql query.");
        } else if ((qry.indexOf("SELECT") > -1) && (qry.indexOf("FROM") > -1)) {
//           
            var repName = Ext.getCmp('repName').getValue();
            var repQuery = Ext.getCmp('repQuery').getValue();
            var status = Ext.getCmp('status').getValue();
            var cond1 = Ext.getCmp('sql-cond1').getValue();
            var cond2 = Ext.getCmp('sql-cond2').getValue();
            var cond3 = Ext.getCmp('sql-cond3').getValue();
            var cond4 = Ext.getCmp('sql-cond4').getValue();
            var cond5 = Ext.getCmp('sql-cond5').getValue();
            var cond6 = Ext.getCmp('sql-cond6').getValue();
            
            var cond1a = Ext.getCmp('sql-cond1a').getValue();
            var cond2a = Ext.getCmp('sql-cond2a').getValue();
            var cond3a = Ext.getCmp('sql-cond3a').getValue();
            var cond4a = Ext.getCmp('sql-cond4a').getValue();
            var cond5a = Ext.getCmp('sql-cond5a').getValue();
            var cond6a = Ext.getCmp('sql-cond6a').getValue();

            console.log("con1", cond1);

            Report.set("reportID", repID);
            Report.set("reportName", repName);
            Report.set("reportQuery", repQuery);
            Report.set("status", status);
            Report.set("cond1", cond1);
            Report.set("cond2", cond2);
            Report.set("cond3", cond3);
            Report.set("cond4", cond4);
            Report.set("cond5", cond5);
            Report.set("cond6", cond6);
            
            Report.set("cond1a", cond1a);
            Report.set("cond2a", cond2a);
            Report.set("cond3a", cond3a);
            Report.set("cond4a", cond4a);
            Report.set("cond5a", cond5a);
            Report.set("cond6a", cond6a);


            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'reportService/updateexcelreport',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(Report.data),
                success: function (response) {

                    var Response = Ext.JSON.decode(response.responseText);

                    if (Response === 1) {
                        Ext.getBody().mask('Saving...', 'Saving Report');
                        Ext.Msg.alert('Done !', 'Saved Report.');
                        Ext.getBody().unmask();

                        var readExcel = me.getReadExcelStore();
                        readExcel.load();

                    } else
                        Ext.Msg.alert('Error !', 'Error occoured while adding report.');

                },
                failed: function (response) {
                    Ext.Msg.alert('Error !', 'Update error.');
                }

            });
        } else {
            Ext.Msg.alert("Error", "Please insert correct sql query.");
        }
    }
});