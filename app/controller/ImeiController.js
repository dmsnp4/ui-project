

Ext.define('singer.controller.ImeiController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.imeicontroller',
    temp:null,
    controllers: ['CoreController'],
    stores: [
        'ImeiAdjs',
        'LoggedInUserData',
        'AcceptDebitNote',
        'GetAcceptDebitNoteDetail',
        'DebitNoteDetails'
    ],
    views: [
        'DeviceIssueModule.Imei.Grid',
        'DeviceIssueModule.Imei.View',
        'DeviceIssueModule.Imei.Form',
        'DeviceIssueModule.Imei.AddIMEI',
        'DeviceIssueModule.Imei.EditImei'
    ],
    refs: [
        {
            ref: 'Grid',
            selector: 'imeigrid'
        },
        {
            ref: 'Form',
            selector: 'imeiform'
        },
        {
            ref: 'View',
            selector: 'imeiview'
        },
        {
            ref: 'AddNew',
            selector: 'addImei'
        },
        {
            ref: 'EditImei',
            selector: 'editImei'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'imeigrid': {
                added: this.initializeGrid
            },
            'imeigrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'imeidetailgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'imeiform grid': {
                itemdblclick: this.editDocument
            },
            'imeigrid  ': {
                onView: this.onView
            },
            'imeigrid   ': {
                onDtail: this.onDetail
            },
            'imeiview button[action=cancel]': {
                click: this.insertFormCancel
            },
            ' imeiview button[action=cancel_edit]': {
                click: this.editFormCancel
            },
            'imeiform button[action=save]': {
                click: this.updataRecord
            },
            'addImei button[action=create_add]': {
                click: this.insertRecord
            },
            'editImei button[action=submit_edit]': {
                click: this.insertEditedRecord
            }, 'editImei button[action=onVerify]': {
                click: this.onVerify
            },
            'imeiform button[action=open_window]': {
                click: this.openAddNew
            },
            'addImei button[action=cancel]': {
                click: this.onAddNewCancel
            },
            'imeiform button[action=cancel_detail]': {
                click: this.closeDetailwindow
            },
        });
    },
    closeDetailwindow: function (btn) {
        var frm = btn.up('window');
        frm.destroy();
    },
    insertEditedRecord: function (btn) {
        ////console.log('comming to insert edited record function');
        var me = this;
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
        var store = this.getGetAcceptDebitNoteDetailStore();
        
        var user = Ext.create('singer.model.ImeiAdj',
                form.getValues()
                );
        user.set('oldImei',me.temp);
        var tem = Ext.getStore('DebitNoteDetails');
        delete user.data.modleDesc;
        //console.log(user.data);
        Ext.Msg.confirm("Update Data", "Are you sure you want to change data?", function (button) {
            if (button == 'yes') {
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/editImeiAdjusment',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(user.data),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        if (responseData.returnFlag === '1') {
//                            Ext.getStore('DebitNoteDetails').load();
                            window.destroy();
                            store.load({
                                callback: function () {
                                    //                            //console.log(store);
                                    tem.removeAll();
                                    var data = store.getAt(0).data.importDebitList;
                                    data.forEach(function (entry) {
                                        if (entry.status != 3) {
                                            ////console.log(entry);
                                            tem.add(entry);
                                        }
                                        

                                    });
                                    
                                }
                            });
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            } else {
                return false;
            }
        });




    },
    onAddNewCancel: function (btn) {
        var window = btn.up('window');
        var form = btn.up('form');
        var changedField = this.checkChanagesforInsertForm(form);
        if (changedField !== false) {
            //ask message nicely
            Ext.Msg.confirm("Exit Form?", "Unsaved changes found.</br>You want to exit form before save it?", function (btnn) {
                if (btnn === "yes") {
//                    form.getForm().reset();
//                    me.focusToFirstFieldinForm(form);
                    window.close();
                } else {
                    changedField.focus(true, 300);
                }
            });
        } else {
//            form.getForm().reset();
//            me.focusToFirstFieldinForm(form);
            window.close();
        }
    },
    editDocument: function (grid, record) {
        var view = Ext.widget('editImei');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Edit',
            items: [view]
        });

        view.down('form').loadRecord(record);
        //console.log(record);
        this.temp = record.get('imeiNo');
        win.show();
    },
    insertFormCanceladdIMEI: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    openAddNew: function (btn) {
        var form = Ext.widget('addImei');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Add New',
            items: [form]
        });
        var form = btn.up('form');
        var str = Ext.getCmp('debitNoteImeii').getStore();
        var businessID = parseInt(str.data.items[0].data.bisId);
        Ext.getCmp('businessId').setValue(businessID);
        var debitNote = str.data.items[0].data.debitNoteNo;
        Ext.getCmp('debitNo').setValue(debitNote);
        win.show();
    },
    insertRecord: function (btn) {
        var me = this;
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
//        var imeistrr =Ext.getCmp('debitNoteImeii').getStore();
        var store = this.getAcceptDebitNoteStore();
        var user = Ext.create('singer.model.ImeiAdj',
                form.getValues()
                );
//        ImportDebitList.data.imeiNo=ImeiAdj.data.newImeiNo;
        //console.log(user);
        delete user.data.modleDesc;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/addImeiAdjusment',
            method: 'PUT',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(user.data),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData);
                if (responseData.returnFlag === '1') {
                    window.destroy();
                    store.load();
                    //console.log(user.data.imeiNo = user.data.newImeiNo);
                    //console.log(me.getDebitNoteDetailsStore());
                    me.getDebitNoteDetailsStore().add(user.data);
                    me.getDebitNoteDetailsStore().data.imeiNo = user.data.newImeiNo;
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    editFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    initializeGrid: function (grid) {
        var store = this.getAcceptDebitNoteStore();
        this.getController('CoreController').loadBindedStore(store);

    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'imeiview', 'View');
    },
    onAdjustImei: function () {
        this.getController('CoreController').openInsertWindow('debitnoteform');
    },
//    openApproveWindow: function (view, record) {
//        //console.log(record.get('paymentId'));
//        singer.AppParams.TEMP = record.get('paymentId');
//        var store = this.getWorkOrderPaymentListwithPriceStore();
//        this.getController('CoreController').loadBindedStore(store);
//        store.load();
//        //console.log(store);
//        
//        var form = Ext.widget('appove');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Repair Payments Approving',
//            items: [form]
//        });
//        win.show();
//    },

    onDetail: function (view, record) {
//        
        var form = Ext.widget('imeiform');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Details',
            items: [form]
        });
        this.headDetail = record;
        var me = this;

        var grid = Ext.getCmp('debitNoteImeii');
        singer.AppParams.TEMP = record.get('debitNoteNo');
        var debitNoteNo = record.get('debitNoteNo');
        var loginData = this.getLoggedInUserDataStore();
        var store = this.getGetAcceptDebitNoteDetailStore();
        var tem = Ext.getStore('DebitNoteDetails');
//        var debitNoteDetail = Ext.create('Ext.data.Store', {
//            model: 'singer.model.ImportDebitList'
//        });
//        var GroupsStore = Ext.create('Ext.data.Store', {
//            model: 'singer.model.ImportDebitList'
//        });
        tem.removeAll();
        store.load({
            callback: function () {
                ////console.log(store);
//                var data = store.getAt(0).data.importDebitList;
//                //console.log(data);
//                tem.add(data);

                var data = store.getAt(0).data.importDebitList;
                data.forEach(function (entry) {
                    if (entry.status != 3) {
                        ////console.log(entry);
                        tem.add(entry);
                    }

                });

            }
        });

//                var data = store.getAt(0).importDebitList;
//        debitNoteDetail.add(data);

//        //console.log(store);
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNoteDetial?debitNoteNo=' + debitNoteNo,
//            method: 'GET',
//            headers: {
//                userId: loginData.data.getAt(0).get('userId'),
//                room: loginData.data.getAt(0).get('room'),
//                department: loginData.data.getAt(0).get('department'),
//                branch: loginData.data.getAt(0).get('branch'),
//                countryCode: loginData.data.getAt(0).get('countryCode'),
//                division: loginData.data.getAt(0).get('division'),
//                organization: loginData.data.getAt(0).get('organization'),
//                'Content-Type': 'application/json'
//            },
//            success: function(response) {
//                var responseData = Ext.decode(response.responseText);
//                ////console.log(responseData);
//                var data = responseData.data[0].importDebitList;
//                debitNoteDetail.add(data);
//                grid.bindStore(debitNoteDetail);
//            },
//            failure: function() {
//                Ext.Msg.hide();
//                Ext.Msg.alert("Error in connection", "Please try again later");
//            }
//        });
        win.show();
        win.setTitle('Details-Debit Note No:[ ' + debitNoteNo + ' ]');
        //console.log('okok');
    },
    updataRecord: function (btn) {
        this.getController('CoreController').onRecordUpdate(btn, this.getImeiAdjsStore(), 'IMEI Data Updated.', 'Updating IMEI Data', true, true, this.getGrid());
    },
    onVerify: function (btn) {
        //console.log(btn.up('form').getForm());
        var form = btn.up('form').getForm();
        var imeiNo = form.getValues().imeiNo;
        var submitBtn = Ext.getCmp('editImei_submit_edit');

        Ext.Msg.wait('Please wait', 'Loading Data...', {
            interval: 300
        });
        var loginData = this.getLoggedInUserDataStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ImeiAdjusments/verfiedImeiDetails?imeiNo=' + imeiNo + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData);
                var data = responseData.data[0];
                if (responseData.data.length !== 0) {
                    form.setValues(data);
                    submitBtn.setDisabled(false);
                }
                else{
                    Ext.Msg.alert("Error", "Invalid IMEI No");
                    submitBtn.setDisabled(true);
                }
//                debitNoteDetail.add(data);
//                grid.bindStore(debitNoteDetail);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    }
});

