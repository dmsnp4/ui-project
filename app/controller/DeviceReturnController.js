/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.controller.DeviceReturnController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.devicereturncontroller',
    headDetail: null,
    rec: null,
    //    requirs:['NewUserController'],
    imeiList: [],
    controllers: ['CoreController', 'Main'],
    stores: [
        'DeviceReturn',
        'DeviceReturnHeadoffice',
        'LoggedInUserData',
        'WinGrid',
        'DevReturnTemp',
        'DeviceReturnDetail',
        'HoApproveDtlTemp',
        'HeadOffliceReturnApproval',
        'DeviceApprovalHeadoffice'
    ],
    views: [
        'DeviceIssueModule.device_return.DeviceReturnGrid',
        'DeviceIssueModule.device_return.Detail',
        'DeviceIssueModule.device_return.NewDeviceReturn',
        'DeviceIssueModule.device_return.ListGridReturn',
        'DeviceIssueModule.device_return.DeviceReturnView',
        'DeviceIssueModule.device_return.DeviceReturnView',
        'DeviceIssueModule.HeadOfficeReturn.HeadofficeDeviceReturnGrid',
        'DeviceIssueModule.HeadOfficeReturn.HeadofficeNewDeviceReturn',
        'DeviceIssueModule.HeadOfficeReturn.HoListGridReturn',
        'DeviceIssueModule.HeadOfficeReturn.DetailHo',
        'DeviceIssueModule.HeadOfficeApproval.HeadofficeDeviceApprovalGrid',
        'DeviceIssueModule.HeadOfficeApproval.ApprovalDetailHo',
        'DeviceIssueModule.HeadOfficeReturn.DeviceReturnHoView'

    ],
    models: [
        'ImportDebitNoteDetailMHO', 'ImportDebitNoteDetailReturn'
    ],
    refs: [{
            ref: 'DeviceReturnGrid',
            selector: 'devicereturngrid'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        },
        {
            ref: 'NewDeviceReturn',
            selector: 'newdevicereturn'
        },
        {
            ref: 'ListGridReturn',
            selector: 'listgridreturn'
        },
        {
            ref: 'DetailHo',
            selector: 'DetailHo'
        },
        {
            ref: 'DeviceReturnView',
            selector: 'devicereturnview'
        },
        {
            ref: 'HeadofficeDeviceReturnGrid',
            selector: 'headofficedevicereturngrid'
        },
        {
            ref: 'HeadofficeDeviceReturnGrid',
            selector: 'headofficedevicereturngrid'
        },
        {
            ref: 'ApprovalDetailHo',
            selector: 'approvalDetailHo'
        },
        {
            ref: 'HeadofficeDeviceApprovalGrid',
            selector: 'headofficedeviceapprovalgrid'
        },
        {
            ref: 'HoListGridReturn',
            selector: 'holistgridreturn'
        },
        {
            ref: 'DeviceReturnHoView',
            selector: 'devicereturnhoview'
        },
        {
            ref: 'returndetailho',
            selector: 'returndetailho'
        }
    ],
    init: function () {
        //        //////console.log(this);
        var me = this;
        me.control({
            'devicereturngrid': {
                added: this.initializeGrid
            },
            'headofficedevicereturngrid': {
                added: this.initializeGridHo
            },
            'headofficedeviceapprovalgrid': {
                added: this.initializeGridHoApproval
            },
            'devicereturngrid ': {
                returnEdit: this.openUpdateForm
            },
            'headofficedevicereturngrid ': {
                returnEditHo: this.openUpdateFormHo
            },
            ' devicereturngrid ': {
                accept: this.AcceptDeviceReturn
            },
            ' headofficedevicereturngrid ': {
                accept: this.AcceptDeviceReturnHo
            },
            ' headofficedeviceapprovalgrid ': {
                accept: this.AcceptDeviceApprovalHo
            },
            ' headofficedevicereturngrid': {
                printHoReturn: this.printHoReturn
            },
            '  headofficedevicereturngrid': {
                printHoReturnDtl: this.printHoReturnDtl
            },
            'devicereturngrid button[action=open_window]': {
                click: this.openAddNew
            },
            'headofficedevicereturngrid button[action=open_window_ho]': {
                click: this.openAddNewHo
            },
            'devicereturngrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'headofficedevicereturngrid button[action=clearSearch]': {
                click: this.onClearSearchHo
            },
            '  devicereturngrid ': {
                returnView: this.moreViewOpen
            },
            '  headofficedevicereturngrid ': {
                returnViewho: this.moreViewOpenHo
            },
            '  headofficedeviceapprovalgrid ': {
                returnApprovalViewho: this.moreApprovalViewOpenHo
            },
            'returndetail button[action=scan]': {
                click: this.focusToIMEIfield
            },
            ' returndetail button[action=accept]': {
                click: this.acceptIMEI
            },
            ' returndetailho button[action=accept_ho]': {
                click: this.acceptIMEIHo
            },
            'debitNoteImeiHo': {
                added: this.initializeGridApprove
            },
            'returndetail button[action=done]': {
                click: this.acceptDone
            },
            'returndetailho button[action=done_ho]': {
                click: this.acceptDoneHo
            },
            ' returndetail button[action=prevOne]': {
                click: this.removeIMEI
            },
            'returndetail button[action=prevAll]': {
                click: this.reverce_all_imei
            },
            'returndetail panel field': {
                specialkey: this.isEnterKeyTwo
            },
            'returndetailho panel field': {
                specialkey: this.isEnterKeyTwoHo
            },
            'newdevicereturn button[action=cancel]': {
                click: this.insertFormCancelnew
            },
            'returndetailho button[action=cancel]': {
                click: this.cancelReturnDtl
            },
            'returndetailho panel field': {
                specialkey: this.isEnterKeyTwoHo
            },
            'newdevicereturn button[action=save]': {
                click: this.updataRecord
            },
            'newdevicereturn button[action=create]': {
                click: this.insertRecord
            },
            'headofficenewdevicereturn button[action=create_ho]': {
                click: this.insertRecordHo
            },
            'newdevicereturn button[action=scanimei]': {
                click: this.openImeiWindow
            },
            'headofficenewdevicereturn button[action=scanimei_ho]': {
                click: this.openImeiWindowHo
            },
            'headofficenewdevicereturn button[action=cancelnewho]': {
                click: this.insertFormCancel

            },
            /*' devicereturngrid': {
             itemdblclick: this.moreViewOpen
             },*/
            'devicereturnview button[action=cancel]': {
                click: this.insertFormCancel
            },
            'devicereturnhoview button[action=cancel]': {
                click: this.viewFormCancel
            },
            'devicereturnview button[action=cancel]': {
                click: this.viewFormCanceldr
            },
            'listgridreturn button[action=cancel]': {
                click: this.insertFormCancelImei
            },
            'holistgridreturn button[action=cancel]': {
                click: this.insertFormCancelImeiHo
            },
            'listgridreturn button[action=okay]': {
                click: this.showGrid
            },
            'holistgridreturn button[action=ho_okay]': {
                click: this.showGridHo
            },
            'listgridreturn button[action=add]': {
                click: this.addtoGrid
            },
            'holistgridreturn button[action=add_ho]': {
                click: this.addtoGridHo
            },
            'listgridreturn panel field': {
                specialkey: this.isEnterKey
            },
            'holistgridreturn panel field': {
                specialkey: this.isEnterKeyHo
            },
            'returndetailho checkcolumn[action=approveReturnChk]': {
                checkchange: this.approveReturnChk
            },
            'returndetailho checkcolumn[action=addToApprove]': {
                checkchange: this.addApproveReturnChk
            },
            'approvalDetailHo checkcolumn[action=addToApprove]': {
                checkchange: this.addApproveReturnChk
            },
            'approvalDetailHo checkcolumn[action=approveReturnChk]': {
                checkchange: this.approveReturnChk
            },
            'approvalDetailHo button[action=done_ho]': {
                click: this.acceptApprovalDoneHo
            },
        });
    },
    isEnterKey: function (field, e) {
        var me = this;
        if (e.getKey() === e.ENTER) {
            me.addtoGrid();
        }
    },
    insertFormCancelImeiHo: function (btn) {

        console.log(">>>>>>>>");

        var str = Ext.getCmp('holgrid').getStore();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    isEnterKeyHo: function (field, e) {
        var me = this;
        if (e.getKey() === e.ENTER) {
            me.addtoGridHo(field);
        }
    },
    approveReturnChk: function (me, rowIndex, checked, eOpts) {
        console.log("Return approve check changed....", rowIndex);


        var hoApproveTemp = Ext.getStore("HoApproveDtlTemp");

        console.log("tempory store ", hoApproveTemp);

        var storeHOReturnDetails = Ext.getStore("HeadOffliceReturnApproval");

        console.log("storeHOReturnDetails ", storeHOReturnDetails);

        var recHoRejectList = storeHOReturnDetails.getAt(rowIndex);

        console.log("recHoRejectList index", recHoRejectList);

        storeHOReturnDetails.remove(recHoRejectList);

        console.log("ho approve list ", recHoRejectList);

        hoApproveTemp.add(recHoRejectList.copy());

    },
    addApproveReturnChk: function (me, rowIndex, checked, eOpts) {

        console.log("Return approve back check changed....", rowIndex);

        var hoApproveTemp = Ext.getStore("HoApproveDtlTemp");

        var storeHOReturnDetails = Ext.getStore("HeadOffliceReturnApproval");

        var recHoRejectList = hoApproveTemp.getAt(rowIndex);

        storeHOReturnDetails.add(recHoRejectList.copy());


        hoApproveTemp.remove(recHoRejectList);


    },
    showGridHo: function (btn) {
        var mainGrid = Ext.getCmp('holmgrid');
        var winGrid = Ext.getCmp('holgrid');
        //        var mainData= mainGrid.getStore();
        var winData = winGrid.getStore();
        //        mainData.loadData(winData);
        mainGrid.bindStore(winData);

        //        var store = this.getDeviceIssueImeStore();
        //        var grid = this.getDeviceIssueGrid();
        //        store.load();

        //        grid.bindStore(store);
        // //console.log("winData",winData);
        //        grid.store2.loadData(store1.data)

        this.getController('CoreController').onInsertFormCancel(btn);

        var totalPrice = winData.sum('salesPrice');
        totalPrice = Ext.util.Format.number(totalPrice, '0000000.00');
        Ext.getCmp('totalPrice').setValue(totalPrice);
        //console.log(totalPrice);

    },
    isEnterKeyTwo: function (field, e) {
        var me = this;
        if (e.getKey() === e.ENTER) {
            me.acceptIMEI();
        }
    },
    isEnterKeyTwoHo: function (field, e) {
        var me = this;
        if (e.getKey() === e.ENTER) {
            me.acceptIMEIHo();
        }
    },
    initializeGrid: function (grid) {
        //////console.log('loading cat list');
        var userStore = this.getDeviceReturnStore();
        this.getController('CoreController').loadBindedStore(userStore);
        var loginStore = this.getLoggedInUserDataStore();
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        ////console.log(user);
        //        this.getController('Main').createDisStore(user);
        //        this.getController('Main').createSubdealerStore(user);
        var test = this.getController('Main').getStore('dsrStore');
        ////console.log(Ext.data.StoreManager.lookup('dsrStore'));
    },
    initializeGridHo: function (grid) {
        console.log('loading ho imei list');
        var userStoreHo = this.getDeviceReturnHeadofficeStore();
        this.getController('CoreController').loadBindedStore(userStoreHo);

        var loginStore = this.getLoggedInUserDataStore();
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        ////console.log(user);
        //        this.getController('Main').createDisStore(user);
        //        this.getController('Main').createSubdealerStore(user);
        var test = this.getController('Main').getStore('dsrStore');
        ////console.log(Ext.data.StoreManager.lookup('dsrStore'));
    },
    initializeGridHoApproval: function (grid) {
        console.log('loading ho imei list');
        var userStoreHo = this.getDeviceApprovalHeadofficeStore();
        this.getController('CoreController').loadBindedStore(userStoreHo);

        var loginStore = this.getLoggedInUserDataStore();
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        ////console.log(user);
        //        this.getController('Main').createDisStore(user);
        //        this.getController('Main').createSubdealerStore(user);
        var test = this.getController('Main').getStore('dsrStore');
        ////console.log(Ext.data.StoreManager.lookup('dsrStore'));
    },
    initializeGridApprove: function (grid) {
        console.log('loading ho imei list');
        var userStoreHo = this.getHeadOffliceReturnApprovalStore();
        this.getController('CoreController').loadBindedStore(userStoreHo);

        var loginStore = this.getLoggedInUserDataStore();
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        ////console.log(user);
        //        this.getController('Main').createDisStore(user);
        //        this.getController('Main').createSubdealerStore(user);
        var test = this.getController('Main').getStore('dsrStore');
        ////console.log(Ext.data.StoreManager.lookup('dsrStore'));
    },
    //HO print
    printHoReturn: function (veiw, record) {
        console.log('Print HO');
        var loginStore = this.getLoggedInUserDataStore();
        console.log('info ', loginStore.data.items[0].data);
        var uid = loginStore.data.getAt(0).get('extraParams');

        // var bisId = loginData.data.items[0].data.bisId;
        var returnNo = record.get('returnNo');
//        console.log('Returnz',returnNo);
//        console.log('bisId',uid);
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=ReturnsToHeadOffice.rptdesign&Return_No=" + returnNo, 'Head Office Return');
    },
    //End HO print
    //HO print DETAIL
    printHoReturnDtl: function (veiw, record) {
        var me = this;
        var returnNo = record.get('returnNo');

        var loginStore = this.getLoggedInUserDataStore();
        var bisid = loginStore.data.getAt(0).get('extraParams');
        //var bisId = loginData.data.items[0].data.bisId;
        console.log("recz " + bisid);
        if (bisid === 219) {
            console.log("record ", record);


            if (record.get('status') == 2 || record.get('status') == 3) {
                me.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DetailedReturnsToHeadOffice.rptdesign&Return_No=" + returnNo, 'Head Office Return Status');
            } else {

                Ext.Msg.confirm("Status Changes", "Pending IMEIs will be rejected. Are you sure?", function (btn) {
                    if (btn == 'yes') {
                        console.log('Print HO');
                        //service
                        Ext.Ajax.request({
                            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturnDtlHoRej?returnNo=' + returnNo,
                            method: "POST",
                            timeout: 600000,
                            async: true,
                            // isUpload: true,
                            // headers: {'Content-Type': 'multipart/form-data'},
//            jsonData: {
//                //excelBase64: excel.getValue()
//            },
                            success: function (response) {
                                Ext.getBody().unmask();
                                //this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DetailedReturnsToHeadOffice.rptdesign&Return_No=" +returnNo, 'Head Office Return Status'); 
                                //  Ext.Msg.alert("Success", "Update BIS ID Successfully");

                                me.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=DetailedReturnsToHeadOffice.rptdesign&Return_No=" + returnNo, 'Head Office Return Status');
                            },
                            failure: function (response, options) {
                                Ext.getBody().unmask();
                                //console.log("Fail to Update BIS ID");
                            }
                        });
                        //service
                    } else
                        console.log('zzzz');
                });

            }
        } else {
            Ext.Msg.alert("Error..", "You are not allowed to Reject.");
        }
    },
    //HO print DETAIL
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onClearSearchHo: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    updataRecord: function (btn) {

        var store = this.getDeviceReturnStore();
        var me = this;
        var form = btn.up('form');
        //console.log(store);
        var assignStore = Ext.getCmp('lmgrid').getStore();
        var user = this.getLoggedInUserDataStore().data.getAt(0).get('extraParams');
        //        //console.log(assignStore);
        var temp = [];
        var record = form.getRecord();
        //        DeviceReturn
        var rec = Ext.create('singer.model.DeviceReturn',
                form.getValues()
                );
        assignStore.data.each(function (item) {
            var intAray = Ext.create('singer.model.DeviceReturnDetail',
                    item.data
                    );
            console.log("array of item ", intAray);
            delete intAray.data.undefined;
            //            //console.log(intAray.data);
            temp.push(intAray.data);
            //            temp.push(item.data);

            //            //console.log(user);
            //            record.set('deviceReturnList', temp);
            //
            //            record.set('distributorId', user);
        });
        //console.log(record);
        rec.set('returnNo', record.data.returnNo);
        rec.set('deviceReturnList', temp);
        rec.set('dsrId', user);
        //        //console.log(record)
        //        this.getController('CoreController').onCreditNoteRecordUpdate(btn, this.getDeviceReturnStore(), 'Device Return Data Updated.', 'Updating Device Return', true, true, this.getDeviceReturnGrid());

        //        Ext.getBody().mask('Updating...', 'large-loading');

        Ext.Msg.confirm("Update Data", "Are you sure you want to change data?", function (button) {
            if (button == 'yes') {
                var loginData = me.getLoggedInUserDataStore();
                var sys = singer.AppParams.SYSTEM_CODE;
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturn',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(rec.data),
                    success: function (response) {
                        ////console.log('comming to here');
                        var responseData = Ext.decode(response.responseText);
                        if (responseData.returnFlag === '1') {
                            singer.LayoutController.notify('Record Added.', "Done...");
                            //btn.up('window').destroy();
                            store.load();
                        } else {
                            Ext.Msg.alert("Error Adding...", responseData.returnMsg);
                        }
                    },
                    failure: function () {
                        //                Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
                btn.up('window').destroy();
                store.load();
            }
        });

        //        var str = this.getNewDeviceReturn().down('grid').getStore();
        ////console.log(str);
        //        str.removeAll();
        //        str.sync();
    },
    insertRecord: function (btn) {
        console.log('btn', btn);
        console.log('Imei grid');
        //        var store = this.getDeviceReturnStore();
        //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Device Return Created.', 'Creating New Device Return', true, true, this.getDeviceReturnGrid(), false);
        //        var grid = this.DeviceReturnGrid();
        //        store.load();
        //        grid.bindStore(store);
        var subGrid = Ext.getCmp('lmgrid');
        var form = btn.up('form');
        var imes = [];
        var store = this.getDeviceReturnStore();
        var bt = btn;
        var loginData = this.getLoggedInUserDataStore();
        var assignStore = Ext.getStore('DeviceIssueIme');
        var sys = singer.AppParams.SYSTEM_CODE;
        subGrid.getStore().data.each(function (item) {
            var intAray = Ext.create('singer.model.DeviceReturnDetail',
                    item.data
                    );
            delete intAray.data.undefined;
            imes.push(intAray.data);
        });
        var mod = Ext.create('singer.model.DeviceReturn',
                form.getValues()
                );
        mod.set('deviceReturnList', imes);
        var TotP = Ext.getCmp('totalPrice').getValue();
        mod.set('price', TotP);
        mod.set('dsrId', loginData.data.getAt(0).get('extraParams'));
        //console.log(mod);

        if (assignStore.data.length !== 0) {
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/addDeviceReturn',
                method: 'PUT',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(mod.data),
                success: function (response) {
                    ////console.log('comming to here');
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
                        singer.LayoutController.notify('Record Added.', "Done...");
                        bt.up('window').destroy();
                        assignStore.removeAll();
                        store.load();
                    } else {
                        Ext.Msg.alert("Error Adding...", responseData.returnMsg);
                    }
                },
                failure: function () {
                    //                Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }

    },
    insertRecordHo: function (btn) {
        console.log('HO Create')
        console.log('btn', btn);

        //        var store = this.getDeviceReturnStore();
        //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Device Return Created.', 'Creating New Device Return', true, true, this.getDeviceReturnGrid(), false);
        //        var grid = this.DeviceReturnGrid();
        //        store.load();
        //        grid.bindStore(store);
        var subGrid = Ext.getCmp('holmgrid');
        var form = btn.up('form');
        var imes = [];
        var store = this.getDeviceReturnHeadofficeStore();
        var bt = btn;
        var loginData = this.getLoggedInUserDataStore();
        var assignStore = Ext.getStore('DeviceIssueIme');
        var sys = singer.AppParams.SYSTEM_CODE;
        subGrid.getStore().data.each(function (item) {
            var intAray = Ext.create('singer.model.DeviceReturnDetail',
                    item.data
                    );
            delete intAray.data.undefined;
            imes.push(intAray.data);
        });
        var mod = Ext.create('singer.model.DeviceReturn',
                form.getValues()
                );
        mod.set('deviceReturnList', imes);
        var TotP = Ext.getCmp('totalPrice').getValue();
        var Rsn = Ext.getCmp('reason').getValue();
        mod.set('price', TotP);
        mod.set('reason', Rsn);
        mod.set('dsrId', loginData.data.getAt(0).get('extraParams'));
        console.log(mod);

        if (assignStore.data.length !== 0) {
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/addDeviceReturnHeadOffice',
                method: 'PUT',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(mod.data),
                success: function (response) {
                    ////console.log('comming to here');
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
                        singer.LayoutController.notify('Record Added.', "Done...");
                        bt.up('window').destroy();
                        assignStore.removeAll();
                        store.load();
                    } else {
                        Ext.Msg.alert("Error Adding...", responseData.returnMsg);
                    }
                },
                failure: function () {
                    //                Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }

    },
    focusToIMEIfield: function (view, record) {
        Ext.getCmp('imeiID').focus('', 10);
    },
    acceptDone: function (btn) {
        var me = this;
        console.log('acceptDone');

//        var tempStore = this.getDeviceReturnDetailStore;
//        console.log('DeviceReturnDetail', tempStore);

        var imeis = [];
        var imeiArray = me.imeiList;
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var store = this.getDeviceReturnStore();

        var acceptImeiStore = Ext.getStore('DeviceReturnDetail');//Ext.getCmp('acceptedImei').getStore();
        console.log('acceptImeiStore.................', acceptImeiStore);

//        var Imei = Ext.create('singer.model.DeviceReturnDetail'//,
//                //form.getValues()
//                );

        var mod = Ext.create('singer.model.DeviceReturn'//,
                //form.getValues()
                );

        for (var i = 0; i < imeiArray.length; i++) {
            console.log('imeiArray[]', imeiArray[i]);
            imeis.push(imeiArray[i]);
        }

        console.log('imeis', imeis);

        mod.set('deviceReturnList', imeis);
        // var TotP = Ext.getCmp('totalPrice').getValue();
        var TotP = 123;

        var devReturnStore = Ext.getStore('DeviceReturn');

        var returnNo = parseInt(localStorage.getItem('selectedDevReturnNo'));
        var selectedRecNo = devReturnStore.find('returnNo', returnNo);

        var selectedRec = devReturnStore.getAt(selectedRecNo);

        mod.set('price', selectedRec.data.price);
        mod.set('distributorId', selectedRec.data.distributorId);
        mod.set('status', 2);
        mod.set('dsrId', selectedRec.data.dsrId);
        mod.set('returnNo', returnNo);
        mod.set('returnDate', selectedRec.data.returnDate);


        if (acceptImeiStore.getCount() > 0) {
            Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturn',
                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturnDtl',
                method: 'POST',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(mod.data),
                success: function (response) {
                    ////console.log('comming to here');
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
                        btn.up('window').destroy();
                        singer.LayoutController.notify('Record Added.', "Done...");
                        store.load();
                    } else {
                        Ext.Msg.alert("Error Adding...", responseData.returnMsg);
                    }
                },
                failure: function () {
                    //                Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }
    },
    //Selected imei accept done
    acceptDoneHo: function (btn) {

        var hoApproveDtlTemp = Ext.getStore("HoApproveDtlTemp");

        var me = this;
        console.log('acceptDone Ho', hoApproveDtlTemp);

        var imeis = [];
        var imeiArray = me.imeiList;
        var loginData = this.getLoggedInUserDataStore();


        hoApproveDtlTemp.each(function (rec) {
            var imeiNo = rec.get('imeiNo');

            console.log("rec list ", rec['data']);
            delete rec['data']['debitNoteNo'];
            delete rec['data']['customerCode'];
            delete rec['data']['shopCode'];
            delete rec['data']['bisId'];
            delete rec['data']['chk'];
            delete rec['data']['dateAccepted'];
            delete rec['data']['orderNo'];
            delete rec['data']['remarks'];
            delete rec['data']['salerPrice'];
            delete rec['data']['shopCode'];
            delete rec['data']['site'];


            imeis.push(rec['data']);
        });

        console.log("imei_list ", imeis);

        var sys = singer.AppParams.SYSTEM_CODE;

        var store = this.getDeviceReturnHeadofficeStore();

        //for Rejected imeis
        var debitImeiStore = Ext.getCmp('debitNoteImeiHo').getStore();

        //End for Rejected imeis

        var acceptImeiStore = Ext.getStore('DeviceReturnDetail');//Ext.getCmp('acceptedImei').getStore();



        var mod = Ext.create('singer.model.DeviceReturn'//,
                //form.getValues()
                );

//        for (var i = 0; i < imeiArray.length; i++) {
//            console.log('imeiArray[]', imeiArray[i]);
//            imeis.push(imeiArray[i]);
//        }

//        for (var i = 0; i < imeiArray.length; i++) {
//            console.log('imeiArray[]', imeiArray[i]);
//            imeis.push(imeiArray[i]);
//        }

        mod.set('deviceReturnList', imeis);
        // var TotP = Ext.getCmp('totalPrice').getValue();
        var TotP = 123;

        var devReturnStore = Ext.getStore('DeviceReturnHeadoffice');
//        var devReturnStore = Ext.getStore('DeviceReturn');
        console.log('dev return' + devReturnStore);

        var returnNo = parseInt(localStorage.getItem('selectedDevReturnNo'));
        var selectedRecNo = devReturnStore.find('returnNo', returnNo);
        console.log('select rec ---' + selectedRecNo);
        var selectedRec = devReturnStore.getAt(selectedRecNo);

        mod.set('price', selectedRec.data.price);
        mod.set('distributorId', selectedRec.data.distributorId);
        mod.set('status', 2);
        mod.set('dsrId', selectedRec.data.dsrId);
        mod.set('returnNo', returnNo);
        mod.set('returnDate', selectedRec.data.returnDate);


        if (hoApproveDtlTemp.getCount() > 0) {
            Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturn',
                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturnDtlHo',
                method: 'POST',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(mod.data),
                success: function (response) {
                    ////console.log('comming to here');
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
                        btn.up('window').destroy();
                        singer.LayoutController.notify('Record Added.', "Done...");
                        store.load();
                    } else {
                        Ext.Msg.alert("Error Adding...", responseData.returnMsg);
                    }
                },
                failure: function () {
                    //                Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert('Creating Error...', 'Please select at least one IMEI number.');
        }
    },
    acceptApprovalDoneHo: function (btn) {
        var me = this;
        Ext.Msg.confirm("Status Changes", "Others IMEIs will be rejected. Are you sure?", function (btn2) {
            if (btn2 == 'yes') {
                console.log("yes ");

                var hoApproveDtlTemp = Ext.getStore("HoApproveDtlTemp");

                
                console.log('acceptDone Ho', hoApproveDtlTemp);

                var imeis = [];
                var imeiArray = me.imeiList;
                var loginData = me.getLoggedInUserDataStore();


                hoApproveDtlTemp.each(function (rec) {
                    var imeiNo = rec.get('imeiNo');

                    console.log("rec list ", rec['data']);
                    delete rec['data']['debitNoteNo'];
                    delete rec['data']['customerCode'];
                    delete rec['data']['shopCode'];
                    delete rec['data']['bisId'];
                    delete rec['data']['chk'];
                    delete rec['data']['dateAccepted'];
                    delete rec['data']['orderNo'];
                    delete rec['data']['remarks'];
                    delete rec['data']['salerPrice'];
                    delete rec['data']['shopCode'];
                    delete rec['data']['site'];


                    imeis.push(rec['data']);
                });

                console.log("imei_list ", imeis);

                var sys = singer.AppParams.SYSTEM_CODE;

                var store = me.getHeadOffliceReturnApprovalStore();

                var mod = Ext.create('singer.model.DeviceReturn'//,
                        //form.getValues()
                        );


                mod.set('deviceReturnList', imeis);
                // var TotP = Ext.getCmp('totalPrice').getValue();
                var TotP = 123;

                var devReturnStore = Ext.getStore('DeviceApprovalHeadoffice');
//        var devReturnStore = Ext.getStore('DeviceReturn');
                console.log('dev return' + devReturnStore);

                var returnNo = parseInt(localStorage.getItem('selectedDevReturnNo'));
                var selectedRecNo = devReturnStore.find('returnNo', returnNo);
                console.log('select rec ---' + selectedRecNo);
                var selectedRec = devReturnStore.getAt(selectedRecNo);
                console.log("selected row ", selectedRec)
                mod.set('price', selectedRec.data.price);
                mod.set('distributorId', selectedRec.data.distributorId);
                mod.set('status', 2);
                mod.set('dsrId', selectedRec.data.dsrId);
                mod.set('returnNo', returnNo);
                mod.set('returnDate', selectedRec.data.returnDate);

                Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturn',
                    url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/editDeviceReturnApprovalHo',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(mod.data),
                    success: function (response) {
                        ////console.log('comming to here');
                        var responseData = Ext.decode(response.responseText);
                        if (responseData.returnFlag === '1') {
                            btn.up('window').destroy();
                            singer.LayoutController.notify('Record Added.', "Done...");
                            
                        } else {
                            Ext.Msg.alert("Error Adding...", responseData.returnMsg);
                        }
                        Ext.getStore("DeviceApprovalHeadoffice").load()
                    },
                    failure: function () {
                        //                Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });


            } else {
                console.log("yes ");
            }
        });



    },
    //End Selected imei accept done

    reverce_all_imei: function (btn) {
        var accepted_imei = Ext.getCmp('acceptedImei');
        var debit_note_imei = Ext.getCmp('debitNoteImei');
        var accepted_imei_Store = accepted_imei.getStore();
        var debit_note_imei_Store = debit_note_imei.getStore();

        accepted_imei_Store.data.each(function (item) {
            debit_note_imei_Store.add(item);
            accepted_imei_Store.remove(item);
        });
    },
    removeIMEI: function (btn) {
        var debitNoteIMEI = Ext.getCmp('debitNoteImei');
        var acceptedIMEI = Ext.getCmp('acceptedImei');
        var selectionModel = acceptedIMEI.getSelectionModel();

        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                debitNoteIMEI.getStore().add(record);
                acceptedIMEI.getStore().remove(record);
            });
        }
    },
    acceptIMEI: function (fieldName, value, start) {
        console.log('acceptIMEI');
        var me = this;
        var scannedIMEI = Ext.getCmp('imeiID').getValue();
        var debitImeiStore = Ext.getCmp('debitNoteImei').getStore();
        var acceptImeiStore = Ext.getCmp('acceptedImei').getStore(); //.bindStore(acceptimei);
        var res = debitImeiStore.findExact('imeiNo', scannedIMEI);
        var dup = acceptImeiStore.findExact('imeiNo', scannedIMEI);

        if (res !== -1) {
            var record = debitImeiStore.getAt(res);
            var loginData = this.getLoggedInUserDataStore();
            var uid = loginData.data.getAt(0).get('extraParams');
            record.data.bisId = uid;

            var mod = Ext.create('singer.model.DeviceReturnDetail'
                    );
            mod.set('imeiNo', record.data.imeiNo);
            mod.set('status', record.data.status);
            mod.set('modleDesc', record.data.modleDesc);
            mod.set('modleNo', record.data.modleNo);
            mod.set('salesPrice', record.data.salesPrice);


            console.log("model record ", record);
            console.log("model store ", mod);

            me.imeiList.push(mod.data);


            acceptImeiStore.add(mod);
            debitImeiStore.remove(record);
        } else if (dup === 0) {
            Ext.MessageBox.alert('Accept Error', 'The selected IMEI <b>' + scannedIMEI + '</b> is already accepted!', function () {
                Ext.getCmp('imeiID').focus('', 10);
            });
        } else {
            Ext.MessageBox.alert('Accept Error', 'The selected IMEI is not in the Debit Note', function () {
                Ext.getCmp('imeiID').focus('', 10);
            });
        }
        Ext.getCmp('imeiID').setValue('');
    },
    //Accept Selected imeis
    acceptIMEIHo: function (fieldName, value, start) {
        console.log('Accept Selected imeis');
        var me = this;
        var scannedIMEI = Ext.getCmp('imeiID').getValue();
        var debitImeiStore = Ext.getCmp('debitNoteImeiHo').getStore();
        var acceptImeiStore = Ext.getCmp('acceptedImeiHo').getStore(); //.bindStore(acceptimei);
        var res = debitImeiStore.findExact('imeiNo', scannedIMEI);
        var dup = acceptImeiStore.findExact('imeiNo', scannedIMEI);

        if (res !== -1) {
            var record = debitImeiStore.getAt(res);
            var loginData = this.getLoggedInUserDataStore();
            var uid = loginData.data.getAt(0).get('extraParams');
            record.data.bisId = uid;

            var mod = Ext.create('singer.model.DeviceReturnDetail'
                    );
            mod.set('imeiNo', record.data.imeiNo);
            mod.set('status', record.data.status);
            mod.set('modleDesc', record.data.modleDesc);
            mod.set('salesPrice', record.data.salesPrice);

            me.imeiList.push(mod.data);

            acceptImeiStore.add(mod);
            debitImeiStore.remove(record);

        } else if (dup === 0) {
            Ext.MessageBox.alert('Accept Error', 'The selected IMEI <b>' + scannedIMEI + '</b> is already accepted!', function () {
                Ext.getCmp('imeiID').focus('', 10);
            });
        } else {
            Ext.MessageBox.alert('Accept Error', 'The selected IMEI can not accept', function () {
                Ext.getCmp('imeiID').focus('', 10);
            });
        }
//        Ext.getCmp('imeiID').setValue('');
    },
    //End Accept Selected imeis
    moreViewOpen: function (view, record) {
        var me = this;


        this.getController('CoreController').onOpenViewMoreWindow(record, 'devicereturnview', "Device Return");
        var gird = Ext.getCmp('lvgrid').getStore();
        var store = this.getDevReturnTempStore();
        //                var store = Ext.create('Ext.data.Store', {
        //                        model: 'singer.model.DeviceReturnDetail'
        //                });
        ////console.log(gird);
        gird.removeAll();
        var returnNo = record.get('returnNo');
        var loginData = this.getLoggedInUserDataStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetail?returnNo=' + returnNo,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            success: function (response) {
                ////console.log('comming to here');
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                ////console.log(responseData.data[0].deviceImei);

                var data = responseData.data[0].deviceReturnList;
                //console.log(data);
                gird.add(data);
                //                                //console.log(store);

                //                                                                                     gird.bindStore(store);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    //Head return view
    moreViewOpenHo: function (view, record) {
        var me = this;


        this.getController('CoreController').onOpenViewMoreWindow(record, 'devicereturnhoview', "Device Return");
        var gird = Ext.getCmp('lvgridho').getStore();
        var store = this.getDevReturnTempStore();
        //                var store = Ext.create('Ext.data.Store', {
        //                        model: 'singer.model.DeviceReturnDetail'
        //                });
        ////console.log(gird);
        gird.removeAll();
        var returnNo = record.get('returnNo');
        var loginData = this.getLoggedInUserDataStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetailHeadOffice?returnNo=' + returnNo,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            success: function (response) {
                ////console.log('comming to here');
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                ////console.log(responseData.data[0].deviceImei);

                var data = responseData.data[0].deviceReturnList;
                //console.log(data);
                gird.add(data);
                //                                //console.log(store);

                //                                                                                     gird.bindStore(store);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    moreApprovalViewOpenHo: function (view, record) {
        var me = this;


        this.getController('CoreController').onOpenViewMoreWindow(record, 'devicereturnhoview', "Device Approval");
        var gird = Ext.getCmp('lvgridho').getStore();
        var store = this.getDevReturnTempStore();
        //                var store = Ext.create('Ext.data.Store', {
        //                        model: 'singer.model.DeviceReturnDetail'
        //                });
        ////console.log(gird);
        gird.removeAll();
        var returnNo = record.get('returnNo');
        var loginData = this.getLoggedInUserDataStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceApprovalDetailHeadOffice?issueNo=' + returnNo,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            success: function (response) {
                ////console.log('comming to here');
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                ////console.log(responseData.data[0].deviceImei);

                var data = responseData.data[0].deviceReturnList;
                //console.log(data);
                gird.add(data);
                //                                //console.log(store);

                //                                                                                     gird.bindStore(store);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    //End Head return view

    AcceptDeviceReturn: function (view, record) {
        console.log('record', record);
        rec = record;

        var loginData = this.getLoggedInUserDataStore();
        var bisId = loginData.data.getAt(0).get('extraParams').toString();
        var recBis = rec.data.dsrId.toString();

        console.log('Users : ', bisId, ' : ', recBis);

        if (bisId != recBis)
        {
            var form = Ext.widget('returndetail');
            var win = Ext.create("Ext.window.Window", {
                width: 500,
                layout: 'fit',
                iconCls: 'icon-verification',
                modal: true,
                constrain: true,
                title: 'Accept Device Return',
                items: [form]
            });
            win.show();

            localStorage.setItem('selectedDevReturnNo', record.data.returnNo);

            var gird = Ext.getCmp('acceptedImei').getStore();
            gird.removeAll();

            var grid = Ext.getCmp('debitNoteImei');

//            var returnIMEI = Ext.create('Ext.data.Store', {
//                model: 'singer.model.ImportDebitNoteDetailM'
//            });
            var returnIMEI = Ext.create('Ext.data.Store', {
                model: 'singer.model.ImportDebitNoteDetailReturn'
            });

//            var acceptIMEI = Ext.create('Ext.data.Store', {
//                model: 'singer.model.ImportDebitNoteDetailM'
//            });
            var acceptIMEI = Ext.create('Ext.data.Store', {
                model: 'singer.model.ImportDebitNoteDetailReturn'
            });

            var loginData = this.getLoggedInUserDataStore();
            Ext.Msg.wait('Please wait', 'Gathering information...', {
                interval: 300
            });

            console.log('acceptIMEI.getCount()', acceptIMEI.getCount());

            // if(acceptIMEI.getCount()>0){

            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetail?returnNo=' + record.data.returnNo,
                method: 'GET',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    'Content-Type': 'application/json'
                },
                success: function (response) {
                    var responseData = Ext.decode(response.responseText);
                    var returnIMEIlist = responseData.data[0].deviceReturnList;
                    console.log("returnIMEIlist ", returnIMEIlist);
                    returnIMEI.add(returnIMEIlist);
                    returnIMEI.data.each(function (itemAll) {
                        if (itemAll.data['status'] === 3) {//if partially accepted, accepted IMEIs goto acceptIMEI store
                            returnIMEI.remove(itemAll);
                            acceptIMEI.add(itemAll);
                        }
                    });
                    grid.bindStore(returnIMEI);
                    Ext.Msg.hide();

                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert("Error..", "You are not allowed to Accept.");
        }

    },
    //Head Accept
    AcceptDeviceReturnHo: function (view, record) {
        console.log('record', record);
        rec = record;

        var loginData = this.getLoggedInUserDataStore();
        var bisId = loginData.data.getAt(0).get('extraParams').toString();
        var recBis = rec.data.dsrId.toString();

        console.log('Users : ', bisId, ' : ', recBis);

        if (bisId != recBis)
        {
            var form = Ext.widget('returndetailho');
            var win = Ext.create("Ext.window.Window", {
                width: 1000,
                layout: 'fit',
                iconCls: 'icon-verification',
                modal: true,
                constrain: true,
                title: 'Accept Device Return',
                items: [form]
            });
            win.show();

            localStorage.setItem('selectedDevReturnNo', record.data.returnNo);

            var gird = Ext.getCmp('rejectedImeiHo').getStore();
            gird.removeAll();

            Ext.getStore("HoApproveDtlTemp").removeAll();


            var grid = Ext.getCmp('debitNoteImeiHo');

            var returnIMEI = Ext.create('Ext.data.Store', {
                storeId: 'storeHOReturnDetails',
                model: 'singer.model.ImportDebitNoteDetailMHO'
            });

            var acceptIMEI = Ext.create('Ext.data.Store', {
                model: 'singer.model.ImportDebitNoteDetailM'
            });

            var loginData = this.getLoggedInUserDataStore();
            Ext.Msg.wait('Please wait', 'Gathering information...', {
                interval: 300
            });

            console.log('acceptIMEI.getCount()', acceptIMEI.getCount());

            // if(acceptIMEI.getCount()>0){

            var hoApproveStore = Ext.getStore("HeadOffliceReturnApproval");

            hoApproveStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetailHeadOfficeList?returnNo=' + record.data.returnNo;

            localStorage.setItem("returnNumberApproveHo", record.data.returnNo);

            hoApproveStore.load();


//            Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetailHeadOffice?returnNo=' + record.data.returnNo,
//                method: 'GET',
//                headers: {
//                    userId: loginData.data.getAt(0).get('userId'),
//                    room: loginData.data.getAt(0).get('room'),
//                    department: loginData.data.getAt(0).get('department'),
//                    branch: loginData.data.getAt(0).get('branch'),
//                    countryCode: loginData.data.getAt(0).get('countryCode'),
//                    division: loginData.data.getAt(0).get('division'),
//                    organization: loginData.data.getAt(0).get('organization'),
//                    'Content-Type': 'application/json'
//                },
//                success: function (response) {
//                    var responseData = Ext.decode(response.responseText);
//                    var returnIMEIlist = responseData.data[0].deviceReturnList;
//                    returnIMEI.add(returnIMEIlist);
//                    returnIMEI.data.each(function (itemAll) {
//                        if (itemAll.data['status'] === 3) {//if partially accepted, accepted IMEIs goto acceptIMEI store
//                            returnIMEI.remove(itemAll);
//                            acceptIMEI.add(itemAll);
//                        }
//                    });
////                    grid.bindStore(returnIMEI);
            Ext.Msg.hide();
//
//                },
//                failure: function () {
//                    Ext.Msg.hide();
//                    Ext.Msg.alert("Error in connection", "Please try again later");
//                }
//            });
        } else {
            Ext.Msg.alert("Error..", "You are not allowed to Accept.");
        }

    },
    AcceptDeviceApprovalHo: function (view, record) {
        console.log('record', record);
        rec = record;

        var loginData = this.getLoggedInUserDataStore();
        var bisId = loginData.data.getAt(0).get('extraParams').toString();
        var recBis = rec.data.dsrId.toString();

        console.log('Users : ', bisId, ' : ', recBis);

        if (bisId != recBis)
        {
            var form = Ext.widget('approvalDetailHo');
            var win = Ext.create("Ext.window.Window", {
                width: 1000,
                layout: 'fit',
                iconCls: 'icon-verification',
                modal: true,
                constrain: true,
                title: 'Accept Device Return Issue',
                items: [form]
            });
            win.show();

            localStorage.setItem('selectedDevReturnNo', record.data.returnNo);

            var gird = Ext.getCmp('rejectedImeiHo').getStore();
            gird.removeAll();

            Ext.getStore("HoApproveDtlTemp").removeAll();


            var grid = Ext.getCmp('debitNoteImeiHo');

            var returnIMEI = Ext.create('Ext.data.Store', {
                storeId: 'storeHOReturnDetails',
                model: 'singer.model.ImportDebitNoteDetailMHO'
            });

            var acceptIMEI = Ext.create('Ext.data.Store', {
                model: 'singer.model.ImportDebitNoteDetailM'
            });

            var loginData = this.getLoggedInUserDataStore();
            Ext.Msg.wait('Please wait', 'Gathering information...', {
                interval: 300
            });

            console.log('acceptIMEI.getCount()', acceptIMEI.getCount());

            // if(acceptIMEI.getCount()>0){

            var hoApproveStore = Ext.getStore("HeadOffliceReturnApproval");

            hoApproveStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceApprovalDetailHeadOfficeList?issueNo=' + record.data.returnNo;

            localStorage.setItem("returnNumberApproveHo", record.data.returnNo);

            hoApproveStore.load();


//            Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetailHeadOffice?returnNo=' + record.data.returnNo,
//                method: 'GET',
//                headers: {
//                    userId: loginData.data.getAt(0).get('userId'),
//                    room: loginData.data.getAt(0).get('room'),
//                    department: loginData.data.getAt(0).get('department'),
//                    branch: loginData.data.getAt(0).get('branch'),
//                    countryCode: loginData.data.getAt(0).get('countryCode'),
//                    division: loginData.data.getAt(0).get('division'),
//                    organization: loginData.data.getAt(0).get('organization'),
//                    'Content-Type': 'application/json'
//                },
//                success: function (response) {
//                    var responseData = Ext.decode(response.responseText);
//                    var returnIMEIlist = responseData.data[0].deviceReturnList;
//                    returnIMEI.add(returnIMEIlist);
//                    returnIMEI.data.each(function (itemAll) {
//                        if (itemAll.data['status'] === 3) {//if partially accepted, accepted IMEIs goto acceptIMEI store
//                            returnIMEI.remove(itemAll);
//                            acceptIMEI.add(itemAll);
//                        }
//                    });
////                    grid.bindStore(returnIMEI);
            Ext.Msg.hide();
//
//                },
//                failure: function () {
//                    Ext.Msg.hide();
//                    Ext.Msg.alert("Error in connection", "Please try again later");
//                }
//            });
        } else {
            Ext.Msg.alert("Error..", "You are not allowed to Accept.");
        }

    },
    //End Head Accept
    openUpdateForm: function (view, record) {
        var me = this;
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        var dis = Ext.create('Ext.data.Store', {
            id: 'device_return_Store',
            fields: [
                'bisId',
                'bisDesc',
                'userId',
                'firstName',
                'lastName',
                'bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getUpperLevelList?bisType=3&bisId=' + user,
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });

//       console.log('newdevicereturn');
        this.getController('CoreController').openUpdateWindow(record, 'newdevicereturn', "Device Return");
        var form = this.getNewDeviceReturn().down('form').getForm();
        form.loadRecord(record);

        // //console.log(user);

//        form.findField('distributorId').bindStore(dis);

        //        form.findField('userId').setDisabled(true);
        //                form.findField('returnNo').hide();

        var ino = record.get('returnNo');
        var devIme = record.get('deviceReturnList');
        var dt = new Date(record.get('returnDate'));

        me.getNewDeviceReturn().down('datefield').setValue(dt);
        var gird = me.getNewDeviceReturn().down('grid').getStore();
        gird.removeAll();
        var returnNo = record.get('returnNo');
        var loginData = this.getLoggedInUserDataStore();

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetail?returnNo=' + returnNo,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            success: function (response) {
                ////console.log('comming to here');
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                ////console.log(responseData.data[0].deviceImei);

                var data = responseData.data[0].deviceReturnList;
                //console.log(data);
                gird.add(data);
                //                                //console.log(store);

                //                                                                                     gird.bindStore(store);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });



        //                detailStore.load({
        //                        scope: this,
        //                        params: {
        //                                returnNo: record.get('returnNo')
        //                        },
        //                        callback: function (detailsrecords, operation, success) {
        //                                me.getNewDeviceReturn().down('grid').bindStore(detailStore);
        //                                //console.log(me.getNewDeviceReturn().down('datefield'), " => eeeeeeeeeeee: ", dt);
        //                        }
        //                });
        ////console.log("ddddddd", devIme);
    },
    //update grid HO
    openUpdateFormHo: function (view, record) {
        var me = this;
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        var dis = Ext.create('Ext.data.Store', {
            id: 'device_return_Store_Ho',
            fields: [
                'bisId',
                'bisDesc',
                'userId',
                'firstName',
                'lastName',
                'bisName'
            ],
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getUpperLevelList?bisType=3&bisId=' + user,
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });

//       console.log('newdevicereturn');
        this.getController('CoreController').openUpdateWindow(record, 'headofficenewdevicereturn', "Device Return");
        var form = this.getHeadofficeNewDeviceReturn().down('form').getForm();
        form.loadRecord(record);

        // //console.log(user);

//        form.findField('distributorId').bindStore(dis);

        //        form.findField('userId').setDisabled(true);
        //                form.findField('returnNo').hide();

        var ino = record.get('returnNo');
        var devIme = record.get('deviceReturnList');
        var dt = new Date(record.get('returnDate'));

        me.getHeadofficeNewDeviceReturn().down('datefield').setValue(dt);
        var gird = me.getHeadofficeNewDeviceReturn().down('grid').getStore();
        gird.removeAll();
        var returnNo = record.get('returnNo');
        var loginData = this.getLoggedInUserDataStore();

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/getDeviceReturnDetail?returnNo=' + returnNo,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                'Content-Type': 'application/json'
            },
            success: function (response) {
                ////console.log('comming to here');
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                ////console.log(responseData.data[0].deviceImei);

                var data = responseData.data[0].deviceReturnList;
                //console.log(data);
                gird.add(data);
                //                                //console.log(store);

                //                                                                                     gird.bindStore(store);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });



        //                detailStore.load({
        //                        scope: this,
        //                        params: {
        //                                returnNo: record.get('returnNo')
        //                        },
        //                        callback: function (detailsrecords, operation, success) {
        //                                me.getNewDeviceReturn().down('grid').bindStore(detailStore);
        //                                //console.log(me.getNewDeviceReturn().down('datefield'), " => eeeeeeeeeeee: ", dt);
        //                        }
        //                });
        ////console.log("ddddddd", devIme);
    },
    //End update grid HO

    insertFormCancelnew: function (btn) {
        var str = this.getNewDeviceReturn().down('grid').getStore();
        ////console.log('str');
        //        str.removeAll();
        //        str.sync();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    cancelReturnDtl: function (btn) {

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    viewFormCancel: function (btn) {

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    viewFormCanceldr: function (btn) {

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    viewFormCancel: function (btn) {

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancelnewHo: function (btn) {
        var str = this.getHeadofficeNewDeviceReturn().down('grid').getStore();
        ////console.log('str');
        //        str.removeAll();
        //        str.sync();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancel: function (btn) {
        var str = Ext.getCmp('holmgrid').getStore();

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancelImei: function (btn) {
        var str = Ext.getCmp('lgrid').getStore();
        ////console.log(str);
        //str.removeAll();
        // str.sync();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    insertFormCancelnewHo: function (btn) {
        var str = this.getHeadofficeNewDeviceReturn().down('grid').getStore();
        ////console.log('str');
        //        str.removeAll();
        //        str.sync();
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    openAddNew: function () {
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        console.log(user);

        var me = this;

        Ext.Msg.wait('Please wait', 'Checking information...', {
            interval: 300
        });

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'DeviceReturns/checkDayEnd?bisId=' + user,
            method: 'GET',
            async: true,
            success: function (response) {

                Ext.Msg.hide();

                var responseData = Ext.decode(response.responseText);

                console.log(responseData);

                var result = responseData.totalRecords;

                if (result == 2) {
                    Ext.Msg.alert("Day End Function", "You have to complete the day end function to continue with device return", function () {


                    });
                } else {

                    var dis = Ext.create('Ext.data.Store', {
                        id: 'device_return_Store',
                        fields: [
                            'bisId',
                            'bisDesc',
                            'userId',
                            'firstName',
                            'lastName',
                            'bisName'
                        ],
                        pageSize: singer.AppParams.INFINITE_PAGE,
                        proxy: {
                            type: 'ajax',
                            url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getUpperLevelList?bisType=3&bisId=' + user,
                            reader: {
                                type: 'json',
                                root: 'data'
                            }
                        },
                        autoLoad: true
                    });
                    ////console.log(Ext.getStore('disStore'));

                    me.getController('CoreController').openInsertWindow('newdevicereturn');
                    var form = me.getNewDeviceReturn().down('form').getForm();
                    var gird = me.getNewDeviceReturn().down('grid').getStore();
                    form.findField('distributorId').bindStore(dis);
                    gird.removeAll();



                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error...", "Please try again later", function () {
                    Ext.getCmp('ltextf').focus('', 10);
                });
            }
        });



        //        form.findField('userId').setDisabled(true);
        //                form.findField('returnNo').hide();
        //        Ext.get("rgrid").enableDisplayMode().hide();
        //        Ext.get("rgrid").setBorder(0);
    },
    openAddNewHo: function () {
        console.log('Head odice');
        var loggedUser = this.getLoggedInUserDataStore();
        var user = loggedUser.data.getAt(0).get('extraParams');
        // //console.log(user);
//        var dis = Ext.create('Ext.data.Store', {
//            id: 'device_return_Store_Ho',
//            fields: [
//                'bisId',
//                'bisDesc',
//                'userId',
//                'firstName',
//                'lastName',
//                'bisName'
//            ],
//            pageSize: singer.AppParams.INFINITE_PAGE,
//            proxy: {
//                type: 'ajax',
//                url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getUpperLevelList?bisType=3&bisId=' + user,
//                reader: {
//                    type: 'json',
//                    root: 'data'
//                }
//            },
//            autoLoad: true
//        });
        console.log("Device return all>>>> 0000");
        ////console.log(Ext.getStore('disStore'));
        this.getController('CoreController').openInsertWindow('headofficenewdevicereturn');
        console.log("Device return all>>>>");
//        var form = this.getNewDeviceReturn().down('form').getForm();
        console.log("Device return all>>>> 22");
//        var gird = this.getNewDeviceReturn().down('grid').getStore();

        var grid = Ext.getStore("DeviceIssueIme");
        console.log("Device return all>>>> 33");
//        form.findField('distributorId').bindStore(dis);

        console.log("Device return all>>>> 44");

        grid.removeAll();

        console.log("Device return all>>>> 555");
        //        form.findField('userId').setDisabled(true);
        //                form.findField('returnNo').hide();
        //        Ext.get("rgrid").enableDisplayMode().hide();
        //        Ext.get("rgrid").setBorder(0);
    },
    openImeiWindow: function (view, record) {
        //        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //        var store = this.getMinorDefectsStore();
        //        this.getController('CoreController').loadBindedStore(store);

        var str = this.getNewDeviceReturn().down('grid').getStore();
        var grid = Ext.widget('listgridreturn');
        var winGrid = Ext.getCmp('lgrid');
        ////console.log(str);
        winGrid.bindStore(str);

        var win = Ext.create("Ext.window.Window", {
            width: 450,
            height: 300,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            resizable: false,
            id: 'deflWindow',
            items: [grid]
        });
        win.setTitle("IMEIs");
        //        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //////console.log(singer.AppParams.TEMP);
        win.show();
        Ext.getCmp('ltextf').focus('', 10);
        //Ext.getCmp('lgrid').getStore().removeAll();

    },
    openImeiWindowHo: function (view, record) {
        //        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //        var store = this.getMinorDefectsStore();
        //        this.getController('CoreController').loadBindedStore(store);
        console.log('HO imeis');

        var str = this.getHeadofficeNewDeviceReturn().down('grid').getStore();
        var grid = Ext.widget('holistgridreturn');
        var winGrid = Ext.getCmp('holgrid');
        ////console.log(str);
        winGrid.bindStore(str);



        var win = Ext.create("Ext.window.Window", {
            width: 450,
            height: 300,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            resizable: false,
            id: 'deflWindow',
            items: [grid]
        });
        win.setTitle("IMEIs");
        //        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //////console.log(singer.AppParams.TEMP);
        win.show();

        Ext.getCmp('ltextfho').focus('', 10);
        //Ext.getCmp('lgrid').getStore().removeAll();

    },
    showGrid: function (btn) {
        var mainGrid = Ext.getCmp('lmgrid');
        var winGrid = Ext.getCmp('lgrid');
        //        var mainData= mainGrid.getStore();
        var winData = winGrid.getStore();
        //        mainData.loadData(winData);
        mainGrid.bindStore(winData);

        //        var store = this.getDeviceIssueImeStore();
        //        var grid = this.getDeviceIssueGrid();
        //        store.load();

        //        grid.bindStore(store);
        // //console.log("winData",winData);
        //        grid.store2.loadData(store1.data)

        this.getController('CoreController').onInsertFormCancel(btn);

        var totalPrice = winData.sum('salesPrice');
        totalPrice = Ext.util.Format.number(totalPrice, '0000000.00');
        Ext.getCmp('totalPrice').setValue(totalPrice);
        //console.log(totalPrice);
    },
    addtoGrid: function () {

        var form = this.getListGridReturn().down('form').getForm().findField("enterimei").getValue();
        var str = this.getListGridReturn().down('grid').getStore();
        //         str.add({imei: form});

        var loginData = this.getWinGridStore();
        ////console.log(loginData);


        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');

        Ext.Msg.wait('Please wait', 'Finding Model No...', {
            interval: 300
        });

        Ext.Ajax.request({
            // url: singer.AppParams.JBOSS_PATH + 'Users/resetUserPassword?userId=' + userID + '&email=' + email + '',
            url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/getImeiDetails?imeiNo=' + form + '&bisId=' + bisId,
            method: 'GET',
            //            headers: {
            //                userId: loginData.data.getAt(0).get('userId'),
            //                room: loginData.data.getAt(0).get('room'),
            //                department: loginData.data.getAt(0).get('department'),
            //                branch: loginData.data.getAt(0).get('branch'),
            //                countryCode: loginData.data.getAt(0).get('countryCode'),
            //                division: loginData.data.getAt(0).get('division'),
            //                organization: loginData.data.getAt(0).get('organization'),
            //                'Content-Type': 'application/json'
            //            },
            //            params: JSON.stringify(temp),
            success: function (response) {

                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);

                //                //console.log("AAAAAAAAA b ", responseData.data[0].modleNo);

                if (responseData.totalRecords === 1) {
                    var modleDesc = responseData.data[0].modleDesc;
                    var SalPrice = responseData.data[0].salesPrice;
                    var modleNo = responseData.data[0].modleNo;


                    var chk = str.findExact('imeiNo', form);
                    ////console.log(chk);

                    if (chk > -1) {
                        Ext.Msg.alert("Error.", "Duplicate IMEI number", function () {
                            Ext.getCmp('ltextf').focus('', 10);
                        });
                    } else {
                        str.add({
                            imeiNo: form,
                            modleDesc: modleDesc,
                            salesPrice: SalPrice,
                            modleNo: modleNo
                        });
                        Ext.getCmp('ltextf').focus('', 10);
                    }



                    //                  str.
                } else {
                    Ext.Msg.alert("Invalid IMEI", "Invalid IMEI number", function () {
                        Ext.getCmp('ltextf').focus('', 10);
                    });
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error...", "Please try again later", function () {
                    Ext.getCmp('ltextf').focus('', 10);
                });
            }
        });

        //        var form = this.getImeiGridIssue().down('form').getForm().findField("enterimei").getValue();
        //        //console.log(form);
        //
        //        //console.log(str);


        var emb = this.getListGridReturn().down('form').getForm().findField("enterimei");
        emb.setValue("");
        Ext.getCmp('ltextf').focus('', 10);
        //        Ext.getCmp('imtextf').clearInvalid();
    },
    addtoGridHo: function (btn) {
        console.log('add imeis to list HO');
        var form = this.getHoListGridReturn().down('form').getForm().findField("enterimeiho").getValue();
        var str = this.getHoListGridReturn().down('grid').getStore();


        //         str.add({imei: form});

        var loginData = this.getWinGridStore();

        console.log("str store", str);


        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');

        Ext.Msg.wait('Please wait', 'Finding Model No...', {
            interval: 300
        });

        Ext.Ajax.request({
            // url: singer.AppParams.JBOSS_PATH + 'Users/resetUserPassword?userId=' + userID + '&email=' + email + '',
            url: singer.AppParams.JBOSS_PATH + 'ImeiMaster/getImeiDetails?imeiNo=' + form + '&bisId=' + bisId,
            method: 'GET',
            //            headers: {
            //                userId: loginData.data.getAt(0).get('userId'),
            //                room: loginData.data.getAt(0).get('room'),
            //                department: loginData.data.getAt(0).get('department'),
            //                branch: loginData.data.getAt(0).get('branch'),
            //                countryCode: loginData.data.getAt(0).get('countryCode'),
            //                division: loginData.data.getAt(0).get('division'),
            //                organization: loginData.data.getAt(0).get('organization'),
            //                'Content-Type': 'application/json'
            //            },
            //            params: JSON.stringify(temp),
            success: function (response) {

                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);

                //                //console.log("AAAAAAAAA b ", responseData.data[0].modleNo);

                if (responseData.totalRecords === 1) {
                    var modleDesc = responseData.data[0].modleDesc;
                    var SalPrice = responseData.data[0].salesPrice;
                    var modleNo = responseData.data[0].modleNo;
                    var margin = responseData.data[0].margin;


                    var chk = str.findExact('imeiNo', form);
                    ////console.log(chk);

                    if (chk > -1) {
                        Ext.Msg.alert("Error.", "Duplicate IMEI number", function () {
                            Ext.getCmp('ltextfho').focus('', 10);
                        });
                    } else {
                        console.log("total rec is 1 imeiNo " + form + " modleDesc " + modleDesc + " salesPrice " + SalPrice + " modleNo " + modleNo);

//                        str.add({
//                            imeiNo: form,
//                            modleDesc: modleDesc,
//                            salesPrice: SalPrice,
//                            modleNo: modleNo,
//                            margin : margin
//                        });

                        str.add(responseData.data);

                        console.log("str setSortState ", str);

                        Ext.getCmp("holgrid").getView().refresh();


                        // str.refresh();
                        Ext.getCmp('ltextfho').focus('', 10);
                        // Ext.getStore('WinGrid').load();

                    }



                    //                  str.
                } else {
                    Ext.Msg.alert("Invalid IMEI", "Invalid IMEI number", function () {
                        Ext.getCmp('ltextfho').focus('', 10);
                    });
                }
//                Ext.getStore('ltextfho').load();


//                grid.getView().refresh();
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error...", "Please try again later", function () {
                    Ext.getCmp('ltextfho').focus('', 10);
                });
            }
        });

        //        var form = this.getImeiGridIssue().down('form').getForm().findField("enterimei").getValue();
        //        //console.log(form);
        //
        //        //console.log(str);


        var emb = this.getHoListGridReturn().down('form').getForm().findField("enterimeiho");
        emb.setValue("");
        Ext.getCmp('ltextfho').focus('', 10);
        Ext.getCmp('ltextfho').clearInvalid();
    },
});