/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.controller.ModelListController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.modellistcontroller',
    controllers: ['CoreController'],
    stores: ['ModelLists'],
    models: ['ModelList'],
    views: [
        'Administrator.Model.ModelListGrid',
        'Administrator.Model.ViewModel'
    ],
    refs: [
        {
            ref: 'ModelListGrid',
            selector: 'modellistgrid'
        },
        {
            ref: 'ViewModel',
            selector: 'viewmodel'
        }
    ],
    init: function() {
        var me = this;
        me.control({
            'modellistgrid': {
                added: this.initializeGrid
            },
            'modellistgrid ': {
                viewModel: this.onViewModel
            },
            'modellistgrid  ': {
                editModel: this.onEditModel
            },
            'modellistgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'viewmodel button[action=cancel]': {
                click: this.insertFormCancel
            },
            'viewmodel button[action=v-cancel]': {
                click: this.onVCancel
            },
            'viewmodel button[action=save]': {
                click: this.onSave
            }
        });
    },
    
    initializeGrid: function(grid) {
        var store = this.getModelListsStore();
        this.getController('CoreController').loadBindedStore(store);
        this.getController('Main').createWarrentySchemeStore();
        this.getController('Main').createRepairCatStore();
    },
    
    onViewModel: function(view, record) {
//        this.getController('Main').createWarrentySchemeStore();
//        this.getController('Main').createRepairCatStore();
        this.getController('CoreController').onOpenViewMoreWindow(record, 'viewmodel', 'View');
        var form = this.getViewModel().down('form').getForm();
        form.findField('dealer_Margin').setReadOnly(true);//setDisabled(true);
        form.findField('distributor_Margin').setReadOnly(true);//setDisabled(true);
        form.findField('rep_Category_id').setReadOnly(true);//setDisabled(true);
        form.findField('wrn_Scheme_id').setReadOnly(true);//setDisabled(true);

        Ext.getCmp('viewmodel-create').setVisible(false);
        Ext.getCmp('viewmodel-save').setVisible(false);
        Ext.getCmp('viewmodel-cancel').setVisible(false);
        
        var modDel=form.findField('dealer_Margin').getValue();
        form.findField('dealer_Margin').setValue(modDel);
        
        var modDis=form.findField('distributor_Margin').getValue();
        form.findField('distributor_Margin').setValue(modDis);


//        form.findField('nic').hide();
    },
    
    insertFormCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    
    onVCancel: function(btn) {
//        this.getController('CoreController').onInsertFormCancel(btn);
        var window = btn.up('window');
        window.close();
    },
    
    onEditModel: function(view, record) {
        this.getController('CoreController').openUpdateWindow(record, 'viewmodel', 'Edit');
        Ext.getCmp('viewmodel-v-cancel').setVisible(false);
    },
    
    onSave: function(btn) {
        this.getController('CoreController').onRecordUpdate(btn, this.getModelListsStore(), 'Model list Data Updated.', 'Updating Model List', true, true, this.getModelListGrid());
    },
    
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    }
});

