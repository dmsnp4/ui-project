
Ext.define('singer.controller.AdminAcceptDebitNote', {
    extend: 'Ext.app.Controller',
//    alias: 'widget.debitnotecontroller',
    headDetail: null,
    imeiList: [],
    rec: null,
    start: 0,
    limit: singer.AppParams.ACCEPT_DEBIT_IMEI_LIMIT,
    controllers: ['CoreController'],
    stores: [
        'AcceptDebitNote', 'LoggedInUserData', 'ImportDebitNotesStore'
    ],
    views: [
        'Administrator.AdminDebitNote.Grid',
        'Administrator.AdminDebitNote.Detail',
        'Administrator.AdminDebitNote.Form',
        'Administrator.AdminDebitNote.View'
    ],
    refs: [
        {
            ref: 'Grid',
            selector: 'debitnotegridd'
        }, {
            ref: 'View',
            selector: 'debit_note_view'
        }, {
            ref: 'Detail',
            selector: 'admin_debit_detail'
        }, {
            ref: 'Form',
            selector: 'debitnoteform'
        }
    ],
    init: function () {
        //        //////console.log(this);
        var me = this;
        me.control({
            'debitnotegridd button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'debitnotegridd ': {
                onView: this.onView
            },
            'debitnotegridd  ': {
                onDetails: this.onDetails
            },
            '          admin_debit_detail button[action=refresh]': {
                click: this.onRefresh
            },
            'debitnotegridd': {
                added: this.initializeGrid
            },
            'debit_note_view button[action=done]': {
                click: this.insertFormCancel
            },
            '      admin_debit_detail button[action=loadMore]': {
                click: this.onLoadMore
            },
//            ' admin_debit_detail button[action=accept]': {
//                click: this.acceptIMEI
//            },
            'admin_debit_detail button[action=done]': {
                click: this.acceptDone
            },
            'admin_debit_detail button[action=accept_All]': {
                click: this.Accept_all_imei
            },
            'admin_debit_detail button[action=prevAll]': {
                click: this.reverce_all_imei
            },
//            'admin_debit_detail panel field': {
//                specialkey: this.isEnterKey
//            },
            ' admin_debit_detail button[action=prevAll]': {
                click: this.removeIMEI
            },
        });
    },
    reverce_all_imei: function () {
        console.log("reverce imei all");
        var accepted_imei = Ext.getCmp('admin_acceptedImei');
        var debit_note_imei = Ext.getCmp('admin_debitNoteImei');
        var accepted_imei_Store = accepted_imei.getStore();
        var debit_note_imei_Store = debit_note_imei.getStore();

        accepted_imei_Store.data.each(function (item) {
            debit_note_imei_Store.add(item);
            accepted_imei_Store.remove(item);
        });
    },
    Accept_all_imei: function () {
        // Ext.Msg.wait('Please wait', 'Please wait...', {
        //     interval: 300
        // });
        console.log("accept imei all");
        var debit_note_imei = Ext.getCmp('admin_debitNoteImei');
        var accepted_imei = Ext.getCmp('admin_acceptedImei');
        var debit_note_imei_Store = debit_note_imei.getStore();
        var accepted_imei_Store = accepted_imei.getStore();

        debit_note_imei_Store.data.each(function (item) {
            debit_note_imei_Store.remove(item);
            accepted_imei_Store.add(item);
        });
        // Ext.Msg.hide();
    },
    removeIMEI: function () {
        var debitNoteIMEI = Ext.getCmp('admin_debitNoteImei');
        var acceptedIMEI = Ext.getCmp('admin_acceptedImei');
        var selectionModel = acceptedIMEI.getSelectionModel();
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                debitNoteIMEI.getStore().add(record);
                acceptedIMEI.getStore().remove(record);
            });
        }
    },
//     isEnterKey: function (field, e) {
//        var me = this;
//        if (e.getKey() === e.ENTER) {
//            me.acceptIMEI();
//        }
//    },


    acceptDone: function (btn) {
        Ext.getBody().mask('Please wait...', 'loading');
        console.log("click accept done");
        var frmWindow = btn.up('window');
        var me = this;
        var parentStore = this.getAcceptDebitNoteStore();
        var imeiArray = me.imeiList;


        var doneBtn = Ext.getCmp('done');
        doneBtn.setDisabled(true);
        //-----------------------------------------------------------
        var debNoIM = Ext.getCmp('admin_acceptedImei').getStore().data.items.length;
        console.log(debNoIM);
        if ((debNoIM < 0) || (debNoIM === 0)) {
            me.headDetail.data.status = 1;
        } else {
            me.headDetail.data.status = 3;
        }
        console.log('status', me.headDetail.data.status);
        //-----------------------------------------------------------

        var accepte_imei_str = Ext.getCmp("admin_acceptedImei").getStore();
        var debit = Ext.create('singer.model.ImportDebitNotesModel',
                me.headDetail.data
                );
        var answer = [];
        accepte_imei_str.data.each(function (item) {
            answer.push(item.data);
        });
        debit.set('importDebitList', answer);

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/addacceptDebitDetialsAll',
            timeout: 500000,
            method: 'PUT',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(debit.data),
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    console.log('Timeout: 500000ms');
                    frmWindow.destroy();
                    parentStore.load();
                    Ext.getBody().unmask();
                    doneBtn.setDisabled(false);
                    Ext.Msg.alert("Success", responseData.returnMsg);

                } else {
                    console.log('Timeout: 500000ms');
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Error", responseData.returnMsg);
                    doneBtn.setDisabled(false);
                }
            },
            failure: function () {
                console.log('Timeout: 500000ms');
                Ext.getBody().unmask();
                Ext.Msg.hide();
                doneBtn.setDisabled(false);
                Ext.Msg.alert("Error in connection", "Please try again later", function () {
//                    Ext.getCmp('imeiID').focus('', 10);
                });
            }
        });
    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    initializeGrid: function (grid) {
        var store = this.getAcceptDebitNoteStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'debit_note_view', 'View');
    },
    onRefresh: function () {
        console.log("refesh value :", Ext.getCmp('admin_accept_load_count').value);
        var me = this;
        me.start = 0;
        var val = Ext.getCmp('admin_accept_load_count').value;
        if (val <= singer.AppParams.ACCEPT_DEBIT_IMEI_LIMIT) {
            me.limit = val;
        } else {
            Ext.getCmp('admin_accept_load_count').setValue(singer.AppParams.ACCEPT_DEBIT_IMEI_LIMIT);
        }
        me.onLoadMore(Ext.getCmp('admin_accept_load_more'));
    },
    onLoadMore: function (btn) {
        console.log("on load more");
        var me = this;
        var grid = Ext.getCmp('admin_debitNoteImei');
        var debitNoteDetail = grid.store;
        if (debitNoteDetail.count() > 0) {
            debitNoteDetail.removeAll();
        }
        console.log(debitNoteDetail);
        var debitNoteNo = this.rec.get('debitNoteNo');
        var loginData = this.getLoggedInUserDataStore();
        Ext.Msg.wait('Please wait', 'Gathering information...', {
            interval: 300
        });
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNoteDetialOnly?debitNoteNo=' + debitNoteNo + '&limit=' + me.limit + '&start=' + me.start + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response) {
                me.start += me.limit;
                var responseData = Ext.decode(response.responseText);
                var data = responseData.data[0].importDebitList;
                if (data.length === 0) {
                    btn.setDisabled(true);
                }
                console.log(data);
                debitNoteDetail.add(data);
                debitNoteDetail.data.each(function (itemAll) {
                    if (itemAll.data['status'] === 3) {
                        debitNoteDetail.remove(itemAll);
                        acceptimei.add(itemAll);
                    }
                });
                grid.bindStore(debitNoteDetail);
                Ext.Msg.hide();
//                Ext.getCmp('imeiID').focus('', 10);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later", function () {
//                    Ext.getCmp('imeiID').focus('', 10);
                });
            }
        });
    },
    onDetails: function (view, record) {
//        //console.log(record);
        var me = this;
        me.start = 0;
        this.rec = record;
        this.imeiList = [];
        this.headDetail = record;
        var form = Ext.widget('admin_debit_detail');
        var win = Ext.create("Ext.window.Window", {
            width: 500,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Accept Debit Note',
            items: [form]
        });
        win.show();
        var debitNoteDetail = Ext.create('Ext.data.Store', {
            model: 'singer.model.ImportDebitNoteDetailM'
        });

        var acceptimei = Ext.create('Ext.data.Store', {
            model: 'singer.model.ImportDebitNoteDetailM'
        });

        // get the id of DEBIT NOTE IMEI grid
        var grid = Ext.getCmp('admin_debitNoteImei');
        ////console.log(grid);
        var debitNoteNo = record.get('debitNoteNo');
        var loginData = this.getLoggedInUserDataStore();
        Ext.Msg.wait('Please wait', 'Gathering information...', {
            interval: 300
        });
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'AcceptDebitNotes/getAcceptDebitNoteDetialOnly?debitNoteNo=' + debitNoteNo + '&limit=' + me.limit + '&start=' + me.start,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response) {
                me.start += me.limit;
                var responseData = Ext.decode(response.responseText);
                var data = responseData.data[0].importDebitList;
                ////console.log(data);
                debitNoteDetail.add(data);
                debitNoteDetail.data.each(function (itemAll) {
                    if (itemAll.data['status'] === 3) {
                        debitNoteDetail.remove(itemAll);
                        acceptimei.add(itemAll);
                    }
                });
                grid.bindStore(debitNoteDetail);
                Ext.Msg.hide();
//                Ext.getCmp('imeiID').focus('', 10);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later", function () {
//                    Ext.getCmp('imeiID').focus('', 10);
                });
            }
        });

        var acceptimei = Ext.create('Ext.data.Store', {
            model: 'singer.model.ImportDebitNoteDetailM'
        });

        // get the id of the ACCEPTED IME grid and bind the store to it
        Ext.getCmp('admin_acceptedImei').bindStore(acceptimei);
//        Ext.getCmp('DebitNtNo').setText('Debit Note No: '+debitNoteNo);
    }
});