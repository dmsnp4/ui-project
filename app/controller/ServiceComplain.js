Ext.define('singer.controller.ServiceComplain', {
    extend: 'Ext.app.Controller',
    alias: 'widget.serviceComplain',
    controllers: ['CoreController'],
    stores: [
        'serviceComplain',
        'LoggedInUserData'
    ],
    views: [
        'ServiceModule.ServiceComplain.Maingrid',
        'ServiceModule.ServiceComplain.view',
        'ServiceModule.ServiceComplain.ReplytoCustomer'
    ],
    refs: [
        {
            ref: 'Maingrid',
            selector: 'complainGrid'
        },
        {
            ref: 'view',
            selector: 'complainview'
        },
        {
            ref: 'ReplytoCustomer',
            selector: 'rplyView'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'complainGrid': {
                added: this.initializeGrid
            },
            ' complainGrid': {
                onView: this.onView
            },
            ' complainview button[action=rplyOnView]': {
                click: this.openRplyToCustomer
            },
            ' rplyView button[action=submit]': {
                click: this.submitReply
            },
            '  complainGrid': {
                onRply: this.openreplyWindow
            },
            'complainGrid button[action=clearSearch]': {
                click: this.onClearSearch
            }
        });
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    openreplyWindow: function (view, record) {
        console.log('test');
        var win = Ext.widget('rplyView');
        win.show();
        Ext.getCmp('feedBId').setValue(record.data.feedBackId);
        Ext.getCmp('cussnme').setValue(record.data.customerName);
        Ext.getCmp('emaill').setValue(record.data.email);

        Ext.getCmp('sProduct').setValue(record.data.prduct);
        Ext.getCmp('wrkorderr').setValue(record.data.workOrderNo);
        Ext.getCmp('mdle').setValue(record.data.modelName);
        Ext.getCmp('workcomplaintype').setValue(record.data.feedBackType);
        Ext.getCmp('wrkcomplain').setValue(record.data.feedBack);
    },
    submitReply: function (btn) {
        //console.log('submitReply');
        var window = btn.up('window');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var form = btn.up('form');
        var store = this.getServiceComplainStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderFeedBacks/replyWorkOrderFeedBack',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(form.getValues()),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    ////console.log(responseData)
                    singer.LayoutController.notify('Record Created.', responseData.returnMsg);
                    window.destroy();
                    store.load();
                } else {
                    singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    openRplyToCustomer: function () {

        var win = Ext.widget('rplyView');
        win.show();
        Ext.getCmp('feedBId').setValue(Ext.getCmp('fbck').getValue());
        Ext.getCmp('cussnme').setValue(Ext.getCmp('custerName').getValue());
        Ext.getCmp('emaill').setValue(Ext.getCmp('maill').getValue());
        Ext.getCmp('sProduct').setValue(Ext.getCmp('gProduct').getValue());
        Ext.getCmp('wrkorderr').setValue(Ext.getCmp('orderORrcc').getValue());
        Ext.getCmp('mdle').setValue(Ext.getCmp('mdllle').getValue());
        Ext.getCmp('workcomplaintype').setValue(Ext.getCmp('typeCmlain').getValue());
        Ext.getCmp('wrkcomplain').setValue(Ext.getCmp('complin').getValue());
    },
    initializeGrid: function (grid) {
        var str = this.getServiceComplainStore();
        this.getController('CoreController').loadBindedStore(str);
    },
    onView: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'complainview', 'View');
    }
});