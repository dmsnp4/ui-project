 Ext.define('singer.controller.DsrShopVisitLocationController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.dsrlocationcontroller',
    map: null,
    markers: [],
    controllers: [
        'CoreController'
    ],
    stores: [
 //        'DsrCurrentLocations'
        'ShopVisits', 'ShopVisitsMap', 'LoggedInUserData','ShopVisitsMapDef'
    ],
    models: [
 //        'ShopVistCheck'
    ],
    views: [
 //        'DSRTrackingModule.ShowDsrLocation.ShowLocation'
        'DSRTrackingModule.ShopVisit.ShopsLocation'
    ],
    refs: [
        {
            ref: 'ShopsLocation',
            selector: 'shopvisitshoplocation'
        }
    ],
    init: function () {
        //        ////////console.log(this);
        var me = this;
        me.control({
            'shopvisitshoplocation': {
                added: this.initializeLocation
            },
            'shopvisitshoplocation button[action=searchShopVisit]': {
                click: this.onShopVisitSearch
            },
            'shopvisitshoplocation combobox[action=DistributorloadDSR]': {
                change: this.onDsrChange
            },
            'shopvisitshoplocation combobox[action=DSRloadLocations]': {
                change: this.onDSRloadLocations
            }

        });
    },
    initializeLocation: function (cmp) {
        //        var map = Ext.getCmp('dsrMap');
        //        this.map = map;

        var map = Ext.getCmp('shopVisit-map');
        //        var loginData = this.getLoggedInUserDataStore();
        //        var bisId = loginData.data.getAt(0).get('extraParams');
        //        var store = this.getDsrCurrentLocationsStore();
        this.map = map;

    },
    onDsrChange: function (cmp, newValue, oldValue, eOpts) {
        var store = Ext.getStore('dsrShop');
        var cmp = Ext.getCmp('shopvisitshoplocation-dsr');
        Ext.getBody().mask('Loading Data', 'large-loading');
        var me = this;
        cmp.reset();
        store.removeAll();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownList?bisId=' + newValue + '&bisType=1',
            method: "GET",
            timeout: 10000,
            success: function (response, options) {

                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                //console.log(responseData.data);
                //                store.add(responseData.data);
                //                cmp.bindStore(store);
                cmp.getStore().add(responseData.data);
            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                //singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                //                form.getForm().reset();

            }
        });
    },
    onShopVisitSearch: function (cmp) {
        //        //console.log(cmp)
        var distributor = Ext.getCmp('shopvisitshoplocation-distributor').getValue();
        var dsr = Ext.getCmp('shopvisitshoplocation-dsr').getValue();
        var date = Ext.getCmp('shopVisit-Date').getSubmitValue();
        var store = this.getShopVisitsMapStore();
        var shopMap = this.map;
        //console.log(distributor, dsr, date);
        Ext.getBody().mask('Establishing Connection', 'large-loading');
        var me = this;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ShopVistChecks/getDayMovement?dSR=' + dsr + '&visitDate=' + date + '',
//            params: val,
            method: "GET",
            timeout: 10000,
            success: function (response, options) {

                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                me.removeMarkers(store);
                store.add(responseData.data);
                var str = me.getShopVisitsMapDefStore();
                me.addDSRMarkers(str.data);
                me.addMarkers(store.data);
            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                form.getForm().reset();

            }
        });


    },
//    onCheckChange: function (cmp) {
//        var val = '';
//        cmp.items.each(function (item) {
//            if (item.checked) {
//                val += item.name + '=' + item.inputValue + '&';
//            } else {
//                val += item.name + '=' + '0' + '&';
//            }
//        });
//        var store = this.getDsrCurrentLocationsStore();
//        this.removeMarkers(store);
//        Ext.getBody().mask('Establishing Connection', 'large-loading');
//        var me = this;
//        Ext.Ajax.request({
//            url: singer.AppParams.JBOSS_PATH + 'BussinessTrackings/getBussinessTrackingList',
//            params: val,
//            method: "GET",
//            timeout: 10000,
//            success: function (response, options) {
//
//                Ext.getBody().unmask();
//                var responseData = Ext.decode(response.responseText);
//                ////console.log(responseData);
//                store.add(responseData.data);
//                me.addMarkers(store.data);
//            },
//            failure: function (response, options) {
//                Ext.getBody().unmask();
//                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
//                form.getForm().reset();
//
//            }
//        });
//    },
    addMarkers: function (data) {
        //console.log(data);
        var map = this.map;
        var markerArray = this.markers;
        var infowindow = new google.maps.InfoWindow();
        var combo=Ext.getCmp('shopvisitshoplocation-distributor').getRawValue();
        data.each(function (item) {
//                        //console.log(item.data);
            var data = item.data;
            var mark = new google.maps.Marker({
                map: map.gmap,
                draggable: false,
                animation: google.maps.Animation.DROP,
                //            icon: mapIcon,
                position: new google.maps.LatLng(data.latitude, data.longtitude)
            });

            //console.log(item.data.shopVisitItemList);
            var InfoContent='<tr><th>Item</th><th>No of Items</th></tr>';
            var lnth=item.data.shopVisitItemList.length;
            var i;
            for(i=0;i<lnth;i++){
                InfoContent+='<tr><td>'+item.data.shopVisitItemList[i].modleDesc+'</td><td>'+item.data.shopVisitItemList[i].itemCount+'</td></tr>';
            }
            google.maps.event.addListener(mark, 'mousedown', function (e) {
                infoBubble.close();
                infoBubble.open(map.gmap, this);
                //                infoBubble.record = {places: this.data};
                infoBubble.setContent([
                    '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
 //                           '<h1 id="firstHeading" class="firstHeading">' + data.firstName + '</h1>' +
                            '<div id="bodyContent">' +
                            '<p><b>ID:</b> ' + data.bisId
                            + '<br><b>User Name:</b>' + data.firstName
                            + '<br><b>Contact No: </b>' + data.contactNo 
                            + '<br><b>Distributor:</b> ' + data.parentName
                            + '<br><b>Sub Dealer:</b> ' + data.subDelerName
                            + '<br><b>Visit Time:</b> ' + data.visitDate
                            + '</p>' +
                            '<div id="SVCitemList"><table>'+InfoContent+'</table></div>'+
                            '</div>' +
                            '</div>'
                ].join(''));
            });
            var infoBubble = new google.maps.InfoWindow();
            
            markerArray.push(mark);
        });

    },
    removeMarkers: function (store) {
        var map = this.map;
        var mark = this.markers;
        store.removeAll();
        //        //console.log(map.gmap);
        for (var i = 0; i < mark.length; i++) {
            mark[i].setMap(null);
        }
    },
    
    onDSRloadLocations:function(){
        var map = this.map;
        var me=this;
        var markerArray = this.markers;
        var store = this.getShopVisitsMapDefStore();
        var dsr = Ext.getCmp('shopvisitshoplocation-dsr').getValue();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'UserTypeLists/getBusinessDownList?bisId=' + dsr + '&bisType=2',
            method: "GET",
            timeout: 10000,
            success: function (response, options) {
                me.removeMarkers(store);
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                store.add(responseData.data);
                me.addDSRMarkers(store.data);
            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                //singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);

            }
        });
    },
    
    addDSRMarkers: function (data) {

        var map = this.map;
        var markerArray = this.markers;
        var infowindow = new google.maps.InfoWindow();
        var combo=Ext.getCmp('shopvisitshoplocation-distributor').getRawValue();
        data.each(function (item) {
            ////console.log(singer.AppParams.BASE_PATH+'resources/images/dsrPont.png');
            var data = item.data;
            var mark = new google.maps.Marker({
                map: map.gmap,
                draggable: false,
                //animation: google.maps.Animation.DROP,
                //            icon: mapIcon,
                position: new google.maps.LatLng(data.latitude, data.longitude),
               icon: singer.AppParams.BASE_PATH+'/resources/images/dsrPont.png',
            });   
            
            
            
            google.maps.event.addListener(mark, 'mousedown', function (e) {
                infoBubble.close();
                infoBubble.open(map.gmap, this);
                //                infoBubble.record = {places: this.data};
                infoBubble.setContent([
                    '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
 //                           '<h1 id="firstHeading" class="firstHeading"</h1>' +
                            '<div id="bodyContent">' +
                            '<p><b>ID:</b> ' + data.bisId
                            + '<br><b>Bis Name:</b> ' + data.bisName
                            + '<br><b>Description:</b> ' + data.bisDesc
                            + '<br><b>Address:</b> ' + data.address 
                            + '<br><b>Contacts:</b> ' + data.teleNumber
                            + '</p>' +
                            '</div>' +
                            '</div>'
                ].join(''));
            });
            var infoBubble = new google.maps.InfoWindow();
            
            
            
            markerArray.push(mark);
        });

    },

});