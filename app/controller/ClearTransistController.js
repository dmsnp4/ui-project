Ext.define('singer.controller.ClearTransistController', {
    extend: 'Ext.app.Controller',
    views: [
        'Administrator.clearTransist.ClearTransistGrid',
        'Administrator.clearTransist.TransistClearView',
        'Administrator.clearTransist.TransistDeleteView'
    ],
    stores: [
        'ClearTransist', 'LoggedInUserData'
    ],
    refs: [
        {
            ref: 'Clear_transist_grid',
            selector: 'cleartransist'
        }, {
            ref: 'clear_transist_view',
            selector: 'cleartransview'
        }, {
            ref: 'cleat_transit_delete',
            selector: 'transitDelete'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'cleartransist': {
                added: this.initializeGrid
            },
            'cleartransist ': {
                clearTransView: this.onClearTranssit
            },
            'cleartransist  ': {
                tranDeleteView: this.onDeleteTranssit
            },
            'transitDelete button[action=transist_cancel] ': {
                click: this.DeleteTranssit
            },
            'cleartransist button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'transitDelete button[action=submit]': {
                click: this.onTransitDeleteSubmit
            },
            'cleartransview button[action=done]': {
                click: this.onDone
            }
        });
    },
    //**
    onTransitDeleteSubmit: function (btn) {
        //        //console.log('onTransitDeleteSubmit');
        var me = this;
        Ext.Msg.confirm("Clear Transit?", "Are you sure to clear?", function (btnn) {
            if (btnn === "yes") {
                var window = btn.up('window');
                var loginData = me.getLoggedInUserDataStore();
                var form = btn.up('form');
                var store = me.getClearTransistStore();
                //================
                var dataArry = [];
                //var clearSTR = Ext.getCmp('delClrGrid').getStore().data.items[0].data;
                var temp = Ext.getCmp('delClrGrid').getStore();
                //console.log(temp);
                temp.data.each(function (item) {
                    dataArry.push(item.data);
                });
                //        //console.log(dataArry)
                var transitM = Ext.create('singer.model.DeviceIssueM',
                    form.getValues()
                );
                //console.log(dataArry);
                transitM.set('deviceImei', dataArry);
                transitM.set('status', '5');
                //console.log(transitM);
                //        transitM.data.each(function (item) {
                //            dataArry.push(item.data);
                //        });
                ////console.log(a);
                //        transitM.set('iMEI', clearSTR.imeiNo);
                //transitM.set('woDetials',dataArry);
                //===============
                var sys = singer.AppParams.SYSTEM_CODE;

                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'ClearTransit/addClearTransit',
                    method: 'PUT',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system:sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(transitM.data),
                    success: function (response, btn) {
                        var responseData = Ext.decode(response.responseText);
                        if (responseData.returnFlag === '1') {
                            ////console.log(responseData)
                            window.destroy();
                            store.load();
                            singer.LayoutController.notify('Transit Cleared.', responseData.returnMsg);
                        } else {
                            singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
                        }

                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in connection", "Please try again later");
                    }
                });
            }
        });



        //        
        //        var store = this.getClearTransistStore();
        //        this.getController('CoreController').onRecordUpdate(btn, store, 'Clear Transist updated', '', true, true, this.getClear_transist_grid());

    },
    //**
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    //**
    DeleteTranssit: function () {
        this.getController('CoreController').onNewRecordCreate(btn, store, 'Transist Deleted', true, true, this.getClear_transist_grid(), false);
        var grid = this.getClear_transist_grid();
    },
    //**
    onClearTranssit: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'cleartransview', 'View');
    },
    onDeleteTranssit: function (grid, record) {
        var win = Ext.widget('transitDelete');
        var formWindow = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Clear Transit Inventory',
            items: [win]
        });
        //console.log('onDeleteTranssit');
        //console.log(record.data.issueNo);
        var issueNum = record.data.issueNo;


        formWindow.show();
        var form = formWindow.down('form');
        form.getForm().loadRecord(record);
        win.show();
        var grid = Ext.getCmp('delClrGrid');


        var loginData = this.getLoggedInUserDataStore();
        var sys=singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'ClearTransit/getClearTransitDetails?issueNo=' + issueNum,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system:sys,
                'Content-Type': 'application/json'
            },
            //   params: JSON.stringify(form.getValues()),
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);

                grid.getStore().removeAll();
                grid.getStore().add(responseData.data);

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });

    },
    initializeGrid: function (grid) {
        var me = this;
        var tempStore = me.getClearTransistStore();
        tempStore.load();
    },
    onDone:function(btn){
        //this.getController('CoreController').onCloseViewMoreWindow(cmp);
        //console.log('close');
        var win = btn.up('window');
        win.close();
    }

});