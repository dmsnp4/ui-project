Ext.define('singer.controller.ExceptionsController', {
    extend: 'Ext.app.Controller',
    views: [
        'exceptions.StoreReloader'
    ],
    stores: ['Exceptions'],
    models: ['Exception'],
    refs: [{
            ref: 'ExceptionWin',
            selector: 'storereloader'
        }],
    init: function() {
        var me = this;
        me.runner = undefined;
        me.control({
            'storereloader': {
                close: this.emptyExceptionStore
            },
            'storereloader button[action=refresh_app]': {
                click: this.refreshApplication
            }/*,
             'storereloader button[action=check_con]': {
             click: this.waituntilDBconSuccess
             } */
        });
    },
    refreshApplication: function() {
        window.location.href = singer.AppParams.BASE_PATH;
    },
    waituntilDBconSuccess: function(btn) {
        /* var runner = new Ext.util.TaskRunner();
         var task = runner.newTask({
         run: function() {
         Ext.Ajax.request({
         url: FXApp.AppParams.JBOSS_PATH + 'validateuser',
         params: "",
         method: "GET",
         timeout: 2000,
         success: function(response, options) {
         var responseData = Ext.decode(response.responseText);
         if (responseData.flag === 1) {
         runner.stop(task);
         }
         },
         failure: function() {
         FXApp.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again.', Ext.MessageBox.ERROR);
         }
         });
         },
         interval: 2000
         });
         runner.start(task);
         */
    },
    emptyExceptionStore: function() {
        var excStore = this.getExceptionsStore();
        excStore.removeAll();
    },
    openStoreReloaderWindow: function(store) {
        if (this.checkStoreReloaderIsOpened()) {
            var win = Ext.widget('storereloader');
            //win.show();
        }
        var expRec = Ext.create('singer.model.Exception', {
            store: store.storeId,
            detail: 'Cannot connect to server to get ' + store.storeId
        });
        this.exceptionsRecorder(expRec);
    },
    exceptionsRecorder: function(exceptionRec) {
        var excStore = this.getExceptionsStore();
        excStore.add(exceptionRec);
    },
    checkStoreReloaderIsOpened: function() {
        if (this.getExceptionWin() === undefined) {
            return true;
        } else {
            return false;
        }
    }
});

