

Ext.define('singer.controller.DistributorMapp', {
    extend: 'Ext.app.Controller',
    controllers: ['CoreController', 'Main'],
    stores: [
        'UserBusinessMappi', 'LoggedInUserData'
    ],
    models: [
        'UserBusinessMapping'
    ],
    views: [
        'Administrator.DistributorMapping.MappingPage'
    ],
    mapping_data: [],
    distributor_val: null,
    refs: [
        {
            ref: 'mapping',
            selector: 'mappingPage'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'mapping': {
                added: this.initializeMainView
            },
            'mapping button[action=add_to_grid] ': {
                click: this.AddERPreffenceToGrid
            },
            'mapping button[action=submit] ': {
                click: this.submit_mapping
            },
            'combobox[action=destributor_change]': {
                change: this.onSelect
            },
            ' mapping grid': {
                remDevIsuItem: this.removeDevIsuItem
            }
        });
    },
    removeDevIsuItem: function (grid, record) {
        console.log('remove');

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        console.log(record.data);
//        var mapping_data = record.data.status=9;
        record.data.status = 9
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'Distributors/editDistributors',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(record.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
//                        grid.getStore().load();
            }
        });
    },
    onSelect: function (btn, value) {

        //console.log(value);
        var bis_val = value;
        this.distributor_val = value;
        var Store = btn.getStore();
        //console.log(Store);
        var temp = Store.findExact('bisId', value);
        if (temp !== -1) {
            Ext.getCmp('erp_mapping_business_id').setValue(Store.getAt(temp).raw.bisId);
            Ext.getCmp('Distributor_Location').setValue(btn.findRecordByValue(value).raw.bisDesc);
            Ext.getCmp('Distributor_ID').setValue(btn.findRecordByValue(value).raw.bisId);
        }

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var store = Ext.getStore('UserBusinessMappi');
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'Distributors/getDistributors?bisId=' + bis_val,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                console.log(responseData.data);
                store.removeAll();
                store.add(responseData.data);
            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    initializeMainView: function () {
//        var store = this.getUserBusinessMappiStore();
//        this.getController('CoreController').loadBindedStore(store);
    },
    AddERPreffenceToGrid: function () {
        console.log('AddERPreffenceToGrid');
        var me = this;
        var businesGrid = Ext.getCmp('bisGrd').getStore();
        var distributor = Ext.getCmp('dstrb').rawValue;
        var ERPRffrn = Ext.getCmp('ERPRffrn').getValue();
        var bisId = Ext.getCmp('erp_mapping_business_id').getValue();
        var mod = Ext.create('singer.model.UserBusinessMapping');
        mod.set('bisName', distributor);
        mod.set('customerNo', ERPRffrn);
        mod.set('bisId', bisId);

        var grid = Ext.getCmp('bisGrd');
        var res = grid.getStore().find('customerNo', ERPRffrn);

        console.log('res', res);

        if (res === -1) {
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'Distributors/checkCustomerCode?customerCode=' + ERPRffrn,
                method: 'GET',
                success: function (response) {
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.returnFlag === '1') {
//                    Ext.Msg.alert("", responseData.returnMsg);
                        grid.getStore().add(mod.data);
                        me.mapping_data.push(mod.data);
                    } else {
                        Ext.Msg.alert("Error...", responseData.returnMsg);
                    }
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        } else {
            Ext.Msg.alert("Error...", ERPRffrn + " is already added.");
        }
    },
    submit_mapping: function () {
        var me = this;
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;

        var store = Ext.getStore('UserBusinessMappi');
        var new_map_dat = this.mapping_data;
        var distributor_bis_id = this.distributor_val;
        //console.log(distributor_bis_id)
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'Distributors/addDistributors?bisId=' + distributor_bis_id,
            method: 'PUT',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(new_map_dat),
            success: function (response) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
//                    store.load();
                    me.mapping_data = [];
                    singer.LayoutController.notify('Success!', 'Data Saved Successfully...');

//                    Ext.Msg.alert("", responseData.returnMsg);
                } else {
                    Ext.Msg.alert("Error...", responseData.returnMsg);
                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    }
});