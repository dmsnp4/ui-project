Ext.define('singer.controller.LoginController', {
    extend: 'Ext.app.Controller',
    views: ['Administrator.login.forms.LoginForm', 'singer.view.MenuContainer'],
    models: ['LoggedInUser'],
    stores: ['LoggedInUserData', 'ServiceUserType'],
    refs: [{
            ref: 'loginView',
            selector: 'apploginform'
        }],
    requires: ['Ext.app.Application'],
    init: function () {
        this.control({
            'apploginform panel button[action=login_handle]': {
                click: this.loginCheck
            },
            'apploginform panel field': {
                specialkey: this.isEnterKey
            },
            'apploginform panel button[action=login_cancel]': {
                click: this.loginCancel
            }
        });
    },
    isEnterKey: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.loginCheck(field);
        }
    },
    loginCheck: function (btn) {
        Ext.getBody().mask('Establishing Connection', 'large-loading');
        var me = this;
        var form = btn.up('form');
        var loggedinstore = this.getLoggedInUserDataStore();
        loggedinstore.removeAll();
        var mod = Ext.create('singer.model.LoggedInUser',
                form.getValues()
                );
        //console.log(mod.data)
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'Login/UserValidation',
            params: JSON.stringify(mod.data),
            method: "POST",
            timeout: 10000,
            headers: {
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response, options) {

                Ext.getBody().unmask();
                if (response.status === 200) {
                    var responseData = Ext.decode(response.responseText);


                    if (responseData.flag === "1000") {
                        responseData = responseData.data[0];
                        //console.log(responseData.extraParams);
                        if (responseData.extraParams === '0') {
                            singer.LayoutController.createErrorPopup("Login Failed", 'You are not assigned to a department...Contact Administrator...', Ext.MessageBox.WARNING);
                        } else {
//                            me.getController("Main").createApplicationView(3, true);
                            me.createUserSession({
                                userId: responseData.userId,
                                firstName: responseData.firstName,
                                lastName: responseData.lastName,
                                designation: responseData.designation,
                                commonName: responseData.commonName,
                                functions: responseData.functions,
                                room: responseData.room,
                                department: responseData.department,
                                branch: responseData.branch,
                                countryCode: responseData.countryCode,
                                division: responseData.division,
                                organization: responseData.organization,
                                nic: responseData.nic,
                                telephoneNumber: responseData.telephoneNumber,
                                email: responseData.email,
                                extraParams: responseData.extraParams
                            });
                        }
                        //                        var store = me.getLoggedInUserDataStore();


                        //                    var store = Ext.create("Ext.data.Store", {
                        //                        model: 'singer.model.LoggedInUser'
                        //                    });
                        //                        var temp = responseData.data;
                        //                        store.add(temp);
                        //                    //console.log(store.getAt(0).get("groups"));
                    } else {
                        Ext.getBody().unmask();
                        form.getForm().reset();
                        btn.up('panel').doLayout();
                        singer.LayoutController.createErrorPopup("Login Failed", responseData.exceptionMessages, Ext.MessageBox.WARNING);


                    }
                } else {
                    Ext.getBody().unmask();
                    singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                    form.getForm().reset();

                }

            },
            failure: function (response, options) {
                ////console.log('faild');
                Ext.getBody().unmask();
                singer.LayoutController.createErrorPopup("Network Connection Lost", 'Network Connection not available.<br> Please contact Admin or try again after few minutes.', Ext.MessageBox.ERROR);
                form.getForm().reset();

            }
        });
    },
    onSuccessLogin: function (responseMsg) {
        Ext.getBody().unmask();
        var loggedinstore = this.getLoggedInUserDataStore();
        var treeMainStoreDSR = this.getServiceUserTypeStore();

//        console.log('treeMainStoreDSR', treeMainStoreDSR);
//                 console.log('responseMsg',responseMsg);
        loggedinstore.add(responseMsg);
        this.getController('Main').createApplicationView(3, false);
        treeMainStoreDSR.load();
    },
    createUserSession: function (userOBJ) {
        Ext.getBody().mask('Preparing Session', 'Loading');
        var me = this;
        var form = Ext.create('Ext.form.Panel', {
            bodyPadding: 1,
            width: 1,
            layout: 'anchor',
            url: singer.AppParams.BASE_PATH + '/sessionHolder.jsp',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'hiddenfield',
            items: [{
                    name: 'loggedIn',
                    value: 'true'
                }, {
                    name: 'userObj',
                    value: JSON.stringify(userOBJ)
                }]
        });
        var responseMsg = null;
        form.submit({
            success: function (form, action) {
                responseMsg = Ext.decode(action.response.responseText);
                ////console.log(responseMsg);
                me.onSuccessLogin(responseMsg);
            },
            failure: function (form, action) {
                responseMsg = Ext.decode(action.response.responseText);
                me.onSuccessLogin(responseMsg);
            }
        });
    },
    loginCancel: function (btn) {
        var form = btn.up('form');
        form.getForm().reset();
    }
});