Ext.define('singer.controller.AssignMarksEventsController', {
    extend: 'Ext.app.Controller',
    controllers: ['CoreController'],
    stores: [
        'AssignMarksEvents',
        'EventMarks',
        'LoggedInUserData',
        'AssignMarksParticipants',
        'NonSaleEventRules'
    ],
    models: [
        'AssignMarksEvent',
        'EventMark',
        'AssignMarksParticipant',
        'NonSaleEventRule'
    ],
    views: [
        'Events.AssignMarks.AssignMarksEventsGrid',
        'Events.AssignMarks.AssignMarksView',
        'Events.AssignMarks.AssignMarksForm',
        'Events.AssignMarks.AssignMarksDetailsView'
    ],
    refs: [
        {
            ref: 'AssignMarksEventsGrid',
            selector: 'assignMarksEventsGrid'
        },
        {
            ref: 'AssignMarksView',
            selector: 'assignMarksView'
        },
        {
            ref: 'AssignMarksForm',
            selector: 'assignMarksForm'
        },
        {
            ref: 'AssignMarksDetailsView',
            selector: 'assignMarksDetailsView'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'assignMarksEventsGrid': {
                added: this.initializeAssignMarksEventsGrid
            },
            'assignMarksEventsGrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            ' assignMarksEventsGrid': {
                assign_marks_view: this.openViewMoreAssignMarksWindow
            },
            'assignMarksView button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'assignMarksView button[action=open_assign_points_form]': {
                click: this.openNewAssignMarksForm
            },
            'assignMarksView': {
                open_view_marks_win: this.openAssignedMarksViewWindow
            },
            ' assignMarksView': {
                open_edit_marks_win: this.openEditAssignedMarksWindow
            },
            ' assignMarksDetailsView button[action=close]': {
                click: this.closeWindow
            },
            'assignMarksForm button[action=cancel]': {
                click: this.closeWindow
            },
            'assignMarksForm': {
                participant_select_change: this.onParticipantComboChanged
            },
            ' assignMarksForm': {
                check_change_rules: this.onNonSalesRulesCheckChange
            },
            'assignMarksForm button[action=create]': {
                click: this.onCreateAssignEventRecord
            },
            'assignMarksForm button[action=save]': {
                click: this.onEditAssignEventRecord
            }
        });
    },
    initializeAssignMarksEventsGrid: function () {
        var store = this.getAssignMarksEventsStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    openViewMoreAssignMarksWindow: function (view, record, rowIndex) {
        var win = Ext.widget('assignMarksView');
        win.setTitle(record.get("eventName") + " - Point Details");
        win.down('form').getForm().findField("eventId").setValue(record.get("eventId"));
        var eventMarksStore = win.down("grid").getStore();
        eventMarksStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'EventAssignMarks/getEventAssignMarksList?eventId=' + record.get("eventId");
        singer.AppParams.TEMP = record.get('eventId');
        eventMarksStore.load();
        win.show();
    },
    openNewAssignMarksForm: function (btn) {
        var win = Ext.widget("assignMarksForm");
        win.setTitle("Assign Event Points");
        win.show();

        var eventId = this.getAssignMarksView().down('form').getForm().findField("eventId").getValue();

        var loginData = this.getLoggedInUserDataStore();
        win.down("form").getForm().findField("assignBy").setValue(loginData.first().get('userId'));
        win.down("form").getForm().findField("assignId").setValue(loginData.first().get('extraParams'));
        win.down("form").getForm().findField("eventId").setValue(eventId);


        var participantsStore = this.getAssignMarksParticipantsStore();
        participantsStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'EventAssignMarks/getEventParticipant?eventId=' + eventId;
        participantsStore.load();

        var nonSaleEventRulesStore = this.getNonSaleEventRulesStore();
        nonSaleEventRulesStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'EventAssignMarks/getEventNonSalesRuleList?eventId=' + eventId;
        var me = this;
        nonSaleEventRulesStore.load();

        Ext.getCmp("assignMarksForm-save").setVisible(false);
    },
    openAssignedMarksViewWindow: function (grid, record) {
        var win = Ext.widget("assignMarksDetailsView");
        win.setTitle("View Event Mark Details");
        win.down("form").getForm().loadRecord(record);
        win.show();
    },
    openEditAssignedMarksWindow: function (grid, record) {
        var win = Ext.widget("assignMarksForm");
        win.setTitle("Edit Event Points");
        win.show();

        var eventId = this.getAssignMarksView().down('form').getForm().findField("eventId").getValue();

        var loginData = this.getLoggedInUserDataStore();


        win.down("form").getForm().findField("assignTo").setReadOnly(true);
        win.down("form").getForm().loadRecord(record);

        win.down("form").getForm().findField("assignBy").setValue(loginData.first().get('userId'));
        win.down("form").getForm().findField("assignId").setValue(loginData.first().get('extraParams'));
        win.down("form").getForm().findField("eventId").setValue(eventId);

        var nonSaleEventRulesStore = this.getNonSaleEventRulesStore();
        nonSaleEventRulesStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'EventAssignMarks/getEventAssignNonSalesRule?eventId=' + eventId + "&userId=" + record.get("userId");
        var me = this;
        nonSaleEventRulesStore.load({
            callback: function () {
                //console.log("Loaded........");
                me.onNonSalesRulesCheckChange();
            }
        });

        Ext.getCmp("assignMarksForm-create").setVisible(false);
    },
    onParticipantComboChanged: function (combo, newVal) {
        var selectedRec = combo.findRecordByValue(newVal);
        if (selectedRec !== false) {
            var me = this;
            if (selectedRec.get("usedStatus") == 1) {
                var prompt = Ext.Msg.confirm('Points Already Assigned', 'Selected participant already has assigned points. Do you want to use modify previously assigned point?', function (btn) {
                    if (btn === 'yes') {
                        me.getAssignMarksForm().close();
                        var eventMarksStore = me.getEventMarksStore();
                        var eventMarkRec = eventMarksStore.findRecord("assignTo", newVal, 0, false, true, true);
                        me.openEditAssignedMarksWindow(me.getAssignMarksView().down('grid'), eventMarkRec);
                    } else {
                        combo.reset();
                    }
                });
            }
        }
    },
    onNonSalesRulesCheckChange: function (checked, record) {
        this.getAssignMarksForm().down("form").getForm().findField("totalPoints").setValue("<strong style='font-size: 20px;'>" + this.calculateTotalPoints() + "</strong>");
    },
    calculateTotalPoints: function () {
        var nonSalesRuleStore = this.getNonSaleEventRulesStore();
        var total = 0;
        nonSalesRuleStore.each(function (record, index) {
            if (record.get("status") === 1) {
                total += record.get("point");
            }
        });
        return total;
    },
    onCreateAssignEventRecord: function (btn) {
        var form = btn.up("form").getForm();
        var eventHeaderRecord = form.getValues();
        var eventMarks = [];
        var nonSalesRuleStore = this.getNonSaleEventRulesStore();
        nonSalesRuleStore.each(function (record, index) {
            if (record.get("status") === 1) {
                var eventTransaction = {};
                eventTransaction = eventHeaderRecord;
                eventTransaction.nonSaleId = record.get("nonSalesRule");
                eventTransaction.nonSalePoint = record.get("point");
                eventTransaction.userId = eventTransaction.assignTo;
                eventMarks.push(eventTransaction);
            }
        });
        if (eventMarks.length === 0) {
            singer.LayoutController.createErrorPopup("No Nonsale Rule Selected", "Please select at least one nonsale rule.", Ext.MessageBox.WARNING);
        } else {
            ////console.log(eventMarks);
            this.submitDataToServer("EventAssignMarks/addEventMarks", "PUT", eventMarks, "Assigning New Nonsale Rule Points...", "New Nonsale Rules Points Assigned Successfully.");
        }

    },
    onEditAssignEventRecord: function (btn) {
        var me=this;
        var form = btn.up("form").getForm();
        var eventHeaderRecord = form.getRecord();
        var eventMarks = [];
        var nonSalesRuleStore = this.getNonSaleEventRulesStore();
        nonSalesRuleStore.each(function (record, index) {
            if (record.get("status") === 1) {
                eventHeaderRecord.set(form.getValues());
                eventHeaderRecord.set("nonSaleId", record.get("nonSalesRule"));
                eventHeaderRecord.set("nonSalePoint", record.get("point"));
                eventHeaderRecord.set("userId", eventHeaderRecord.get("assignTo"));
                eventMarks.push(eventHeaderRecord.getData());
            }
        });
        if (eventMarks.length === 0) {
            singer.LayoutController.createErrorPopup("No Nonsale Rule Selected", "Please select at least one nonsale rule.", Ext.MessageBox.WARNING);
        } else {
            //console.log(eventMarks);
            Ext.Msg.confirm("Are you sure?", "Are you sure to save changes?", function (btnn) {
                if (btnn === "yes") {
                    me.submitDataToServer("EventAssignMarks/editEventMarks", "POST", eventMarks, "Modifying Nonsale Rule Points...", "Modified Nonsale Rules Points Assigned Successfully.");
                }
            });
        }

    },
    submitDataToServer: function (submitUri, httpType, dataObj, loadingMsg, successMsg) {
        Ext.getBody().mask(loadingMsg, 'loading');
        var me = this;
        var store = this.getEventMarksStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + submitUri,
            method: httpType,
            headers: me.getController("CoreController").appendLDAPHeadersToRequest(),
            params: Ext.encode(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    singer.LayoutController.notify("Operation Success", successMsg);
                } else {
                    singer.LayoutController.createErrorPopup("Error Occured", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
                me.getAssignMarksForm().close();
                store.load();
            },
            failure: function () {
                Ext.getBody().unmask();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    closeWindow: function (btn) {
        var win = btn.up("window");
        win.close();
    }
});