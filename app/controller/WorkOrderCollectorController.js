


Ext.define('singer.controller.WorkOrderCollectorController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.collectorcontroller',
    controllers: ['CoreController'],
    stores: [
        'LoggedInUserData',
        'WorkOrderCollector'
    ],
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    views: [
        'ServiceModule.QuickWorkOrderCollector.WorkOrderCollectorGrid',
        'ServiceModule.QuickWorkOrderCollector.EditCollector'
    ],
    temp: null,
    rwIndx: null,
    refs: [
        {
            ref: 'Form',
            selector: 'Form'
        },
        {
            ref: 'newquickorder',
            selector: 'newquickorder'
        },
        {
            ref: 'btnSave',
            selector: 'btnSaveAddQuickWork'
        },
        {
            ref: 'QuickWorkOrderView',
            selector: 'quickworkOrderOpenView'
        },
        {
            ref: 'EditQuickWorkOrder',
            selector: 'editquickworkorder'
        },
        {
            ref: 'ca',
            selector: 'QuickWorkOrderView'
        },
        {
            ref: 'WorkOrderCollectorGrid',
            selector: 'workordercollectorgrid'
        },
        {
            ref: 'EditCollector',
            selector: 'editcollector'
        }

    ],
    init: function () {
        var me = this;
        console.log('qqqqq');
        me.control({
            'workordercollectorgrid': {
                added: this.initializeGrid
            },
            'workordercollectorgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'QuickWorkOrdersGrid ': {
                onView: this.onView
            },
            'QuickWorkOrdersGrid': {
                viewinfo: this.AddForm

            },
            'workordercollectorgrid ': {
                editquickeditcollector: this.editQuickForm
            },
            'workordercollectorgrid  ': {
                printquickeditcollector: this.printquickeditcollector
            },
            'workordercollectorgrid button[action=open_window]': {
                click: this.openAddNewQuickWorkOrderCollector
            },
            'QuickWorkOrdersGrid button[action=viewinfo]': {
                click: this.viewQuickWorkOrder
            },
            'newquickorder button[action=create]': {
                click: this.openAddNewOrder
            },
            'editcollector button[action=save]': {
                click: this.saveQuickWO
            },
            'editcollector button[action=cancel]': {
                click: this.cancelColWO
            },
            'quickworkOrderOpenView button[action=cancelView]': {
                click: this.cancelViewQuickWO
            },
            'newquickorder': {
                on_nic_validity_change: this.onNicFieldValidityChange
            },
            ' workOrderOpenForm': {
                on_nic_change: this.onNicValChange
            }
        });
    },
    onCancelBtnWorkOrderViewMore: function (btn) {
        var window = btn.up('window');
        window.close();
    },
    onSaveBtnWorkOrderViewMore: function (btn) {
        var form = btn.up('form').getForm();
        var record = form.getRecord();
        this.onChangeWorkOrder(form.findField("status").getValue(), record, 1);
        btn.up('window').close();
    },
    printquickeditcollector: function (grid, rec) {
        var workOrderNo = rec.get('workOrderNo');
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=quickworkorder.rptdesign&WorkOrderNo=" + workOrderNo, 'Quick Work Order Collector');
    },
    openViewMoreWindow: function (view, record, rowIndex) {
        var win = Ext.widget('SlectionW');
        win.down("form").getForm().loadRecord(record);
        if (this.getApprovalForm().down("form").getForm().findField("status").getValue() == '1') {
            Ext.getCmp("select_or_unselect_radio_grp").setVisible(true);
        }
        win.setTitle('Add or Remove Work Order');

        win.show();
    },
    onChangeWorkOrder: function (checked, record, rowIndex) {
        var form = this.getApprovalForm().down("form").getForm();
        if (checked) {
            record.set("status", 1);
            form.findField("paymentPrice").setValue(parseFloat(form.findField("paymentPrice").getValue()) + parseFloat(record.get("amount")));
        } else {
            record.set("status", 0);
            form.findField("paymentPrice").setValue(parseFloat(form.findField("paymentPrice").getValue()) - parseFloat(record.get("amount")));
        }
    },
    authorizeWithRemoteServer: function (dataObj, loadingMsg, successMsg, onErrorMsg, uri) {
        Ext.getBody().mask(loadingMsg, 'loading');
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        var me = this;
        var store = this.getRepairPaymentStore();
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + uri,
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                if (responseData.returnFlag === '1') {
                    singer.LayoutController.notify("Operation Success", successMsg);

                } else {
                    singer.LayoutController.createErrorPopup("Error Occured", responseData.returnMsg, Ext.MessageBox.WARNING);
                }
                me.getApprovalForm().close();
                me.getController('CoreController').loadBindedStore(store);
            },
            failure: function () {
                Ext.getBody().unmask();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onChequeInsertBtnClicked: function (btn) {
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();

        Ext.Msg.confirm('Apply Cheque Collection to Repair Payment Record', 'Are you sure you want to Apply Cheque to current repair payment?', function (btn) {
            if (btn === 'yes') {
                record.set("status", 5);
                record.set("chequeNo", form.findField("chequeNo").getValue());
                record.set("chequeDetails", form.findField("chequeDetails").getValue());
                record.set("chequeDate", Ext.Date.format(form.findField("chequeDate").getValue(), "Y-m-d"));
                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;
                me.authorizeWithRemoteServer(dataObj, "Appling Cheque to Repair Payment", "Cheque Aplied to Repair Payment", "", "PaymentApproves/addChequeDetails");
            }
        });
    },
    onSettleBtnClicked: function (btn) {
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();

        Ext.Msg.confirm('Settle Repair Payment Record', 'Are you sure you want to settle current repair payment?', function (btn) {
            if (btn === 'yes') {
                record.set("status", 4);
                record.set("settleAmount", parseFloat(form.findField("settleAmount").getValue()));
                record.set("poNumber", form.findField("poNumber").getValue());
                record.set("poDate", Ext.Date.format(form.findField("poDate").getValue(), "Y-m-d"));
                record.set("remarks", form.findField("remarks").getValue());
                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;
                me.authorizeWithRemoteServer(dataObj, "Settle Repair Payment", "Repair Payment Settled", "", "PaymentApproves/addSettleAmount");
            }
        });
    },
    onApproveBtnClicked: function (btn) {
        var me = this;
        var form = btn.up('form').getForm();
        var grid = btn.up('form').down('grid');
        var record = form.getRecord();

        Ext.Msg.confirm('Accept Repair Payment Record', 'Are you sure you want to approve current repair payment?', function (btn) {
            if (btn === 'yes') {
                record.set("status", 2);
                record.set("paymentPrice", parseFloat(form.findField("paymentPrice").getValue()));
                record.set("remarks", form.findField("remarks").getValue());
                var detailsStore = grid.getStore();
                ////console.log(record, grid.getStore());
                var dataObj = record.data;
                var detailRecords = [];
                detailsStore.each(function (rec, index) {
                    detailRecords.push(rec.data);
                });
                dataObj.woPaymentDetials = detailRecords;
                //console.log(dataObj, form.findField("paymentPrice").getValue());
                me.authorizeWithRemoteServer(dataObj, "Approving Repair Payment", "Repair Payment Approved", "", "PaymentApproves/addApprovePayment");
            }
        });


//        var loginData = this.getLoggedInUserDataStore();
//        var sys = singer.AppParams.SYSTEM_CODE;
//        var win = btn.up('window');
//        win.destroy();
//        Ext.Ajax.request({
//                url: singer.AppParams.JBOSS_PATH + 'PaymentApproves/addApprovePayment',
//                method: 'POST',
//                headers: {
//                    userId: loginData.data.getAt(0).get('userId'),
//                    room: loginData.data.getAt(0).get('room'),
//                    department: loginData.data.getAt(0).get('department'),
//                    branch: loginData.data.getAt(0).get('branch'),
//                    countryCode: loginData.data.getAt(0).get('countryCode'),
//                    division: loginData.data.getAt(0).get('division'),
//                    organization: loginData.data.getAt(0).get('organization'),
//                    system: sys,
//                    'Content-Type': 'application/json'
//                },
//                params: JSON.stringify(win.getValues()),
//                success: function(response, btn) {
//                    var responseData = Ext.decode(response.responseText);
//                    if (responseData.returnFlag === '1') {
//                        win.close();
//                    } else {
//                        singer.LayoutController.createErrorPopup("Record Insertion Fail.", responseData.returnMsg, Ext.MessageBox.WARNING);
//                    }
//
//                },
//                failure: function() {
//                    Ext.Msg.hide();
//                    Ext.Msg.alert("Error in connection", "Please try again later");
//                }
//            });
    },
//    submitSelection: function(btn, view, record, rowIndex) {
//        var chkk = Ext.getCmp('chkbx').getValue();
//        //console.log(chkk);
//        var win = btn.up('form').up('form');
////        //console.log(Ext.getStore('WorkOrderPaymentListwithPrice').data.items[0].data);
//        if (chkk === true) {
//            //console.log('true');
//            this.str.removeAt(this.rwIndx);
//            win.destroy();
//        } else {
//            //console.log('false');
//        }
//
//    },
    openSelectOrUnselectWindow: function (view, record, rowIndex) {
        var me = this;
//        this.str = view.getStore();
//        this.rwIndx = rowIndex;
//        //console.log(view.getStore().removeAt(rowIndex));
//        var actionTake = record.data.actionTake;
//        var amount = record.data.amount;
//        var defectDesc = record.data.defectDesc;
//        var defectType = record.data.defectType;
//        var imeiNo = record.data.imeiNo;
//        var laborCost = record.data.laborCost;
//        var nonWarrantyVerifType = record.data.nonWarrantyVerifType;
//        var paymentDate = record.data.paymentDate;
//        var paymentId = record.data.paymentId;
//        var referenceNo = record.data.referenceNo;
//        var remarks = record.data.remarks;
//        var seqNo = record.data.seqNo;
//        var spareCost = record.data.spareCost;
//        var order = record.data.order;
//        var status = record.data.status;
//        var tecnician = record.data.tecnician;
//        var warrantyVerifType = record.data.warrantyVerifType;
//        var workOrderNo = record.data.workOrderNo;
        ////console.log(imeiNo);

//        var form = Ext.widget('SlectionW');
//        form.getForm().loadRecord(record);
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            modal: true,
//            constrain: true,
//            title: 'Add Or Remove Work Order',
//            items: [form]
//        });
//        win.show();
//        Ext.getCmp('WOno').setValue(workOrderNo);
//        Ext.getCmp('Wverification').setValue(warrantyVerifType);
//        Ext.getCmp('wrnty').setValue("NO DATA");
//        Ext.getCmp('nonWrnty').setValue(nonWarrantyVerifType);
//        Ext.getCmp('techn').setValue(tecnician);
//        Ext.getCmp('dfct').setValue(defectType);
//        Ext.getCmp('action').setValue(actionTake);
//        Ext.getCmp('imeei').setValue(imeiNo);
//        Ext.getCmp('inviceNo').setValue("NO DATA");
//        Ext.getCmp('sprPrts').setValue(spareCost);
//        Ext.getCmp('ThrdInvNo').setValue("NO DATA");
//        Ext.getCmp('ThrdPrice').setValue("NO DATA");
    },
    cancelAproveWindow: function (btn) {
        var window = btn.up('window');
        window.close();
    },
    AddForm: function (view, record) {
        console.log(record);
        this.getController('CoreController').onOpenViewMoreWindow(record, 'quickworkOrderOpenView', 'More Details of ' + record.get('imeiNo'));
        // Ext.getCmp("AddForm").hidden = false;
//       this.getController('CoreController').onOpenViewMoreWindow('QuickWorkOrderView');
        console.log('Add New Form......');
        //this.getController('CoreController').onOpenViewMoreWindow(record, 'workOrderOpenView', 'More Details of ' + record.get('workOrderNo'));
        //Ext.getCmp("AddForm").setVisible(true);
    },
    editQuickForm: function (view, record) {
        console.log('qqqqqqqq');
        this.getController('CoreController').openUpdateWindow(record, 'editcollector', "Edit");
//        this.getController('CoreController').onOpenViewMoreWindow(record, 'quickworkOrderOpenView', 'More Details of ' + record.get('imeiNo'));
        ///
//        var me = this;
//        console.log("EDIT");
//       
//        var form = this.getNewDeviceIssue().down('form').getForm();
//        //this.focusToFirstFieldinForm(form);
//        var ino = record.get('issueNo');
//        //        var deviceImeiStore = this.getDeviceIssueStore();
//        var devIme = record.get('deviceImei');
//
//        var dt = new Date(record.get('issueDate'));
//
//        me.getNewDeviceIssue().down('datefield').setValue(dt);
//        ////console.log("ddddddd", devIme);
//        var DeviceIssueImeStore = Ext.create('Ext.data.Store', {
//            model: 'singer.model.DeviceIssueIme'
//        });
//
//        var abcd = Ext.getCmp('rad').getValue();
//        this.changeRdioGrp2("", abcd);
//        var detailStore = me.getNewDeviceIssue().down('grid').getStore();
//        detailStore.load({
//            scope: this,
//            params: {
//                issueNo: record.get('issueNo')
//            },
//            callback: function (detailsrecords, operation, success) {
//                me.getNewDeviceIssue().down('grid').bindStore(detailStore);
//                //console.log(me.getNewDeviceIssue().down('datefield'), " => eeeeeeeeeeee: ", dt);
//            }
//        });
        ///
    },
    openViewWindow: function (view, record) {
        console.log('openViewWindow');
//        var frnName = record.data.setviceName;
//        //console.log(record.data.setviceName);
//        singer.AppParams.TEMP = record.get('paymentId');
        var store = this.getWorkOrderPaymentListwithPriceStore();
        ////console.log(store.getProxy().api.read);
        if (record.get("status") == 1) {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails?paymentId=' + record.get("paymentId");
        } else {
            store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'PaymentApproves/getApprovePaymentDetails?paymentId=' + record.get("paymentId") + "&status=1";
        }

//        this.getController('CoreController').loadBindedStore(store);
//        store.load();
        store.load();
        var win = Ext.widget('appove');
        var form = win.down("form");
        var formBasic = form.getForm();
        formBasic.loadRecord(record);
        //console.log(record.get("poDate"), record.get("chequeDate"));
        formBasic.findField("chequeDate").setValue(record.get("chequeDate"));
        formBasic.findField("poDate").setValue(record.get("poDate"));

        var windowTitle = "Repair Payments Approving";
        if (record.get("status") === 1) {
            Ext.getCmp("ActiveChangeColumn").hidden = false;
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_approve").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_reject").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", false, false],
                ["paymentPrice", true, true],
                ["remarks", false, true]
            ], true);
        } else if (record.get("status") === 2) {
            windowTitle = "Repair Payments Settlement";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_settle").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", false, false],
                ["poNumber", false, false],
                ["poDate", false, false],
                ["remarks", false, true]
            ], false);
        } else if (record.get("status") === 4) {
            windowTitle = "Repair Payments Cheque Payment Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_cancel").setVisible(true);
//            Ext.getCmp("repair_payment_approve_form_save").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", false, false],
                ["chequeDate", false, false],
                ["chequeDetails", false, true]
            ], false);
        } else if (record.get("status") === 5) {
            windowTitle = "Completed Repair Payments Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", true, true],
                ["chequeDate", true, true],
                ["chequeDetails", true, true]
            ], true);
        } else if (record.get("status") === 3) {
            windowTitle = "Rejected Repair Payments Approving";
            Ext.getCmp("repair_payment_approve_form_close").setVisible(true);
            showAndEnableFormFields([
                ["setviceName", true, true],
                ["paymentPrice", true, true],
                ["settleAmount", true, true],
                ["poNumber", true, true],
                ["poDate", true, true],
                ["remarks", true, true],
                ["chequeNo", true, true],
                ["chequeDate", true, true],
                ["chequeDetails", true, true]
            ], true);
        }
        //1st stage form
        function showAndEnableFormFields(fields, showGrid) {
            form.down("grid").setVisible(showGrid);
            form.down("label").setVisible(showGrid);
            for (var i = 0; i < fields.length; i++) {
                formBasic.findField(fields[i][0]).allowBlank = fields[i][2];
                formBasic.findField(fields[i][0]).setVisible(true);
                formBasic.findField(fields[i][0]).setReadOnly(fields[i][1]);
            }
        }
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            //iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: windowTitle,
//            items: [form]
//        });
        win.setTitle(windowTitle);
        win.show();
//        Ext.getCmp('Franchise').setValue(frnName);
    },
    initializeGrid: function (grid) {
        //var store = this.getRepairPaymentStore();
        var store = this.getWorkOrderCollectorStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    //////////////
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onView: function (view, record) {
        console.log('XXXX');
        this.getController('CoreController').onOpenViewMoreWindow(record, 'wrkOrderview', 'Work Order Opening details of WORK ORDER NO :' + record.get('workOrderNo'));
    },
    openAddNewQuickWorkOrderCollector: function (btn) {
        this.getController('CoreController').openInsertWindow('editcollector');

        console.log('Open New Quick WO Collector Form.....');
        // Ext.getCmp('newdeviceissue-create').show();

        var bisId = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');

        Ext.getCmp('hdnQBisId').setValue(bisId);
    },
    viewQuickWorkOrder: function (btn) {
        console.log('showVIEW');
        this.getController('CoreController').openInsertWindow('QuickWorkOrderView');
        //Ext.getCmp('newdeviceissue-create').show();
    },
    openAddNewOrder: function (btn) {
        //this.getController('CoreController').openInsertWindow('newquickorder');
        this.getController('CoreController').onNewRecordCreate(btn, this.getQuickWorkOrderStore(), 'New Quick Work Order Created.', 'Creating New Quick Work Order', true, true, this.getForm(), 'user');

//        console.log('zzzzzz');
//        var form = btn.up("form").getForm();
//        var rec = Ext.create("singer.model.QuickWorkOrderM", form.getValues());
//        this.submitDataToServer("WorkOrders/addQuickWorkOrder", "PUT", rec.getData(), "Creating new Quick Work Order");
//       
//       form.destroy();
    },
    submitDataToServer: function (submitUri, httpType, dataObj, loadingMsg) {
        Ext.getBody().mask(loadingMsg, 'loading');
        var createBtn = Ext.getCmp('btnSaveAddQuickWork-create');
//        createBtn.setDisabled(true);x
        console.log('vfdsbsgfg');
        var me = this;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + submitUri,
            method: httpType,
            headers: me.getController("CoreController").appendLDAPHeadersToRequest(),
            params: Ext.encode(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
//                createBtn.setDisabled(false);
                var responseData = Ext.decode(response.responseText);
                //callback(responseData);
            },
            failure: function () {
                Ext.getBody().unmask();
                createBtn.setDisabled(false);
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    saveQuickWO: function (btn) {
        console.log('Quick Work Order Update.....');

        this.getController('CoreController').onRecordUpdate(btn, this.getWorkOrderCollectorStore(), 'Quick Work Order is Updated.', 'Updating Quick Workorder.', true, true, this.getForm());
    },
    cancelColWO: function (btn) {


        this.getController('CoreController').onInsertFormCancel(btn);
    },
    cancelViewQuickWO: function (btn) {
        console.log('Quick Work Order Cancel.....');

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    onNicFieldValidityChange: function (field, isValid) {
        var form = field.up("form").getForm();
        if (isValid) {
            // field.nextSibling("button").setDisabled(false);
            Ext.getCmp('newquickorder-save').setDisabled(false);
//            Ext.getCmp('newquickorder-create').setDisabled(false);
            // form.findField("email2").setDisabled(false);

        } else {
//            field.nextSibling("button").setDisabled(true);
            Ext.getCmp('newquickorder-save').setDisabled(true);
//            Ext.getCmp('newquickorder-create').setDisabled(true);
            //form.findField("email2").setDisabled(true);
        }
    },
    onNicValChange: function (field, newVal, oldVal) {
        var form = field.up("form").getForm();
        // form.findField("customerName").reset();
        // form.findField("customerAddress").reset();
        //  form.findField("workTelephoneNo").reset();
        form.findField("email").reset();
        //  form.findField("email2").reset();
        //  form.findField("email2").setVisible(true);
        //  form.findField("email2").allowBlank = false;
    },
    onNicVerifyBtnClick: function (btn) {
        var form = btn.up("form").getForm();
        var field = btn.previousSibling("nicfield");
        var callback = function (response) {
            var responseData = response.data[0];
            if (response.totalRecords > 0) {
                form.findField("customerName").setValue(responseData.customerName);
                form.findField("customerAddress").setValue(responseData.address);
                form.findField("workTelephoneNo").setValue(responseData.contactNo);
                form.findField("email").setValue(responseData.email);
                form.findField("email2").setValue(responseData.email);
                form.findField("email2").setDisabled(true);
                form.findField("email2").allowBlank = true;
            } else {
                singer.LayoutController.createErrorPopup("Warning", responseData.returnMessage, Ext.MessageBox.WARNING);
            }
        };
        this.verifyWithServer("Customers/checkCustomerDetal?customerNIC=" + field.getValue(), "GET", "Verifying NIC Number", callback);
    }
    //cancelViewQuickWO
});







