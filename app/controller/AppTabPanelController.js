
Ext.define('singer.controller.AppTabPanelController', {
    extend: 'Ext.app.Controller',
    views: ['AppTabTitleContainer', 'AppTabPanel'],
    refs: [{
            ref: 'TabTitleContainer',
            selector: 'appTabTitleContainer'
        }, {
            ref: 'ContentView',
            selector: 'appcontentContainer'
        }, {
            ref: 'appTabPanel',
            selector: 'appTabPanel'
        }],
    stores: ['LoggedInUserData'],
    init: function() {
        this.tokenDelimiter = ':';
        this.control({
            'appTabPanel': {
                tabchange: this.onTabChange
            },
            ' appcontentContainer': {
                afterrender: this.onContentPanelAfterRender
            }
        });
    },
    onContentPanelAfterRender: function() {
        var me = this;
        var loggedinstore = this.getLoggedInUserDataStore();
        var menuObj = Ext.encode(loggedinstore.first().get('Menus'));
        ////console.log(menuObj);
        var menuController = this.getController('MenuController');
        var mainController = this.getController('Main');
        var tokenDelimiter = this.tokenDelimiter;

        Ext.History.init(function() {
            var runner = new Ext.util.TaskRunner();
            var compPreLoader = Ext.create('Ext.container.Container', {
                height: 4,
                maxWidth: 0,
                lastStore: '',
                id: 'state_preloader',
                style: {
                    background: "red"
                },
                renderTo: Ext.getBody()
            });
            me.getContentView().insert(0, compPreLoader);
            var task = runner.newTask({
                run: function() {
                    if (mainController.checkApplicationSettingsInitialized()) {
                        runner.stop(task);
                        compPreLoader.maxWidth = me.getContentView().getWidth();
                        compPreLoader.animate({
                            duration: 300,
                            to: {
                                width: me.getContentView().getWidth()
                            },
                            listeners: {
                                afteranimate: function() {
                                    me.getContentView().remove(compPreLoader);
                                }
                            }
                        });
                        if (window.location.href.indexOf("#") !== -1) {
                            var token = (window.location.href).split(singer.AppParams.BASE_PATH + "/#")[1];
                            if (token.length !== 0) {
                                var parts, length, i;
                                if (token) {
                                    parts = token.split(tokenDelimiter);
                                    length = parts.length;
                                    for (i = 0; i < length - 1; i++) {
                                        var tabID = decodeURI(parts[i + 1].split("&")[1]);
                                        var tabTitle = decodeURI(parts[i + 1].split("&")[2]);
                                        var tabAlias = decodeURI(parts[i + 1].split("&")[0]);
                                        if (menuObj.indexOf(tabTitle) !== -1) {
                                            if (Ext.isDefined(Ext.getCmp(tabID)) && Ext.isDefined(Ext.getCmp(parts[i]))) {
                                                Ext.getCmp(parts[i]).setActiveTab(Ext.getCmp(tabID));
                                            } else {
                                                var menutItem = {
                                                    text: tabTitle
                                                };
                                                menuController.updateClickedTabTitle(me.getContentView(), tabTitle);
                                                menuController.createMenuTab(menutItem, me.getContentView(), tabAlias);
                                            }
                                        } else {
                                            Ext.Msg.show({
                                                title: "Bad Request",
                                                msg: "Requested Page is not available...",
                                                buttonText: {yes: "OK"},
                                                icon: Ext.MessageBox.WARNING,
                                                closable: false,
                                                fn: function(btn) {
                                                    if (btn === "yes") {
                                                        window.location.href = singer.AppParams.BASE_PATH;
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                interval: 100
            });
            runner.start(task);
        });
        Ext.History.on('change', function(token) {
            Ext.WindowMgr.each(function(comp) {
             //   if (comp.isWindow)
                    //comp.destroy();
                        //comp.close();
            }); 
            var parts, length, i;
            if (token) {
                parts = token.split(tokenDelimiter);
                length = parts.length;
                for (i = 0; i < length - 1; i++) {
                    var tabID = decodeURI(parts[i + 1].split("&")[1]);
                    var tabTitle = decodeURI(parts[i + 1].split("&")[2]);
                    var tabAlias = decodeURI(parts[i + 1].split("&")[0]);
                    if (menuObj.indexOf(tabTitle) !== -1) {
                        if (Ext.isDefined(Ext.getCmp(tabID))) {
                            Ext.getCmp(parts[i]).setActiveTab(Ext.getCmp(tabID));
                        } else {
                            var menutItem = {
                                text: tabTitle
                            };
                            menuController.updateClickedTabTitle(me.getContentView(), tabTitle);
                            menuController.createMenuTab(menutItem, me.getContentView(), tabAlias);
                        }
                    } else {
                        Ext.Msg.show({
                            title: "Bad Request",
                            msg: "Requested Page is not available...",
                            buttonText: {yes: "OK"},
                            icon: Ext.MessageBox.WARNING,
                            closable: false,
                            fn: function(btn) {
                                if (btn === "yes") {
                                    window.location.href = singer.AppParams.BASE_PATH;
                                }
                            }
                        });
                    }
                }
            }
        });
    },
    onTabChange: function(tabPanel, crntTab) {
        this.getController('MenuController').updateClickedTabTitle(this.getContentView(), crntTab.title);
        if (Ext.isDefined(crntTab.store)) {
            if (Ext.isDefined(crntTab.getStore().tree)) {
                if (crntTab.getStore().storeId === 'CategoryTree') {
                    this.getController('CoreController').loadBindedStore(Ext.getStore('AssetCategories'));
                }
                if (crntTab.getStore().storeId === 'LocationTree') {
                    this.getController('CoreController').loadBindedStore(Ext.getStore('AssetLocations'));
                }
            } else {
                if(crntTab.getStore().storeId==='UploadStore'){
                    
                }else if (!crntTab.getStore().isLoading()) {
                    this.getController('CoreController').loadBindedStore(crntTab.getStore());
                }
            }
        }
        var tokenDelimiter = this.tokenDelimiter;
        var tabs = [],
                ownerCt = tabPanel.ownerCt,
                oldToken, newToken;

        tabs.push(crntTab.xtype + "&" + crntTab.id + "&" + crntTab.title.split('&nbsp;')[0]);
        tabs.push(tabPanel.id);
        while (ownerCt && ownerCt.is('tabpanel')) {
            tabs.push(ownerCt.id);
            ownerCt = ownerCt.ownerCt;
        }
        newToken = tabs.reverse().join(tokenDelimiter);
        oldToken = Ext.History.getToken();
        if (oldToken === null || oldToken.search(newToken) === -1) {
            Ext.History.add(newToken);
        }
    }
});