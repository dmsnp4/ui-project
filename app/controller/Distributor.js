

Ext.define('singer.controller.Distributor', {
    extend: 'Ext.app.Controller',
//    alias: 'widget.wrkOrdercontroller',
    controllers: ['CoreController'],
    stores: [
       'LoggedInUserData'
    ],
    views: [
        'DashBoard.Distributor'
    ],
    refs: [
        {
            ref: 'Distributor',
            selector: 'distributorView'
        }
        
        
    ],
    init: function () {
        var me = this;
        me.control({
            'distributorView': {
                added: this.initialize
            },
            'distributorView combo[action=search]': {
                change: this.DSRdroppdown
            },
   
        });
    },
    

    initialize: function () {
        ////console.log('comming to initialize');
        var loginStore = this.getLoggedInUserDataStore();
        var user = loginStore.data.getAt(0).get('userId');
        ////console.log(Ext.getStore('dsrStore'));
        this.getController('Main').createDsrStore(user);
    },
    
     DSRdroppdown: function (btn, value) {
//        var store = this.getUsersStore();
        ////console.log('comming to DSRdroppdown');
        var loginStore = this.getLoggedInUserDataStore();
        var user = loginStore.data.getAt(0).get('userId');
        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + "UserTypeLists/getUserTypeList?userId="+user+"&bisType=" + value + "";
//        store.load();
    },


});

