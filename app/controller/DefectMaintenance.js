/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.controller.DefectMaintenance', {
    extend: 'Ext.app.Controller',
    alias: 'widget.defectmaintenance',
    //    requirs:['NewUserController'],
    controllers: ['CoreController'],
    stores: [
        'Defects', 'MinorDefects'
    ],
    models: [
        'Defects', 'MinorDefects'
    ],
    views: [
        'Administrator.defects.MajorDefectGrid',
        'Administrator.defects.MinorDefectGrid',
        'Administrator.defects.NewMajorDefect',
        'Administrator.defects.NewMinorDefect',
        'Administrator.defects.MajorDefectView',
        'Administrator.defects.MinorDefectView'
    ],
    refs: [
 //        {
 //            ref: 'Main',
 //            selector: 'mainview',
 //            autoCreate: true,
 //            xtype: 'mainview'
 //        },
        {
            ref: 'MajorDefectGrid',
            selector: 'majordefectgrid'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        }, {
            ref: 'MajorDefectView',
            selector: 'majordefectview'
        }, {
            ref: 'MinorDefectView',
            selector: 'minordefectview'
        },
        {
            ref: 'NewMajorDefect',
            selector: 'newmajordefect'
        },
        {
            ref: 'MinorDefectGrid',
            selector: 'minordefectgrid'
        }, {
            ref: 'NewMinorDefect',
            selector: 'newminordefect'
        }],
    init: function () {
        //        //////console.log(this);
        var me = this;
        me.control({
            'majordefectgrid': {
                added: this.initializeGridMajor
            },
            //            'minordefectgrid': {
            //                added: this.initializeGridMinor
            //            },
            'majordefectgrid ': {
                defectEdit: this.openUpdateFormMajor
            },
            'minordefectgrid ': {
                defectEdit: this.openUpdateFormMinor
            },
            'majordefectgrid button[action=open_window]': {
                click: this.openAddNewMajor
            },
            'minordefectgrid button[action=open_window]': {
                click: this.openAddNewMinor
            },
            'majordefectgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'minordefectgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'newmajordefect button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newminordefect button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newmajordefect button[action=save]': {
                click: this.updataRecordMajor
            },
            'newminordefect button[action=save]': {
                click: this.updataRecordMinor
            },
            'newmajordefect button[action=create]': {
                click: this.insertRecordMajor
            },
            'newminordefect button[action=create]': {
                click: this.insertRecordMinor
            },
            'editmajordefect button[action=cancel]': {
                click: this.insertFormCancel
            },
            'editminordefect button[action=cancel]': {
                click: this.insertFormCancel
            },
            '  majordefectgrid': {
                addMinor: this.openAddMinorGrid
            },
            'minordefectgrid button[action=goback]': {
                click: this.insertFormCancel
            },
            '  majordefectgrid  ': {
                itemdblclick: this.moreViewOpenMajor
            },
            '  minordefectgrid  ': {
                itemdblclick: this.moreViewOpenMinor
            },
            'majordefectview button[action=cancel]': {
                click: this.insertFormCancel
            },
            'minordefectview button[action=cancel]': {
                click: this.insertFormCancel
            }
            //            ' majordefectgrid': {
            //                itemdblclick: this.openAddMinorGrid
            //            },
            //            ' minordefectgrid': {
            //                itemdblclick: this.moreViewOpen
            //            }
            //            'userview button[action=cancel]': {
            //                click: this.insertFormCancel
            //            }
            //            'usergrid': {
            //                added: this.initializeGrid
            //            },
            //            'usergrid button[action=open_window]': {
            //                click: this.openAddNew
            //            }
        });
    },
    //initializeGrid: function() {
    //var groupStore = this.getUserGroupsStore();//Ext.data.StoreManager.lookup('UserGroups');
    //    var defectStore =  this.getDefectStore(); //Ext.data.StoreManager.lookup('Users');

    //    //////console.log(userStore);
    ////////console.log(groupStore);
    //    this.getController('CoreController').loadBindedStore(defectStore);
    //    this.registerNewVTypes();
    //},

    // moreViewOpen: function(view, record) {

    //     this.getController('CoreController').onOpenViewMoreWindow(record, 'userview', record.get('userId') + "'s details");
    // },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    moreViewOpenMinor: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'minordefectview', record.get('minDefectCode') + "'s details");
    },
    moreViewOpenMajor: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'majordefectview', 'View');
    },
    initializeGridMajor: function (grid) {
        //////console.log('loading cat list');
        var userStore = this.getDefectsStore();
        this.getController('CoreController').loadBindedStore(userStore);
    },
    //    initializeGridMinor: function(grid) {
    //        //////console.log('loading cat list');
    //        var userStore = this.getMinorDefectsStore();
    //        this.getController('CoreController').loadBindedStore(userStore);
    //    },
    openUpdateFormMajor: function (view, record) {

        this.getController('CoreController').openUpdateWindow(record, 'newmajordefect', "Major Defect: " + record.get('mjrDefectCode'));
        var form = this.getNewMajorDefect().down('form').getForm();

        //        form.findField('code').setDisabled(true);
        //        form.findField('mjrDefectCode').hide();
        //        form.findField('code').setDisabled(true);
        //form.findField('nic').hide();
        //        //////console.log(record.get("userGroups"));
        //        
        //        var userGroupsStore = Ext.create("Ext.data.Store",{
        //            model : 'SingerTest.model.UserGroups'
        //        });
        //        var userGroupsArray = record.get("userGroups");
        //        for (var i = 0; i < userGroupsArray.length; i++) {
        //             var singleGroup = Ext.create("SingerTest.model.UserGroups",{
        //                groupId : userGroupsArray[i].groupId,
        //                groupName :  userGroupsArray[i].groupName,
        //            });
        //            userGroupsStore.add(singleGroup);
        //            //var groupName = 
        //        }
        //        
        //form.findField('password1').setValue(record.get('password'));
        //            form.findField('password').clearInvalid();

    },
    openUpdateFormMinor: function (view, record) {

        this.getController('CoreController').openUpdateWindow(record, 'newminordefect', "Minor Defect: " + record.get('minDefectCode'));
        var form = this.getNewMinorDefect().down('form').getForm();
        //        this.focusToFirstFieldinForm(form);
        //        form.findField('code').setDisabled(true);
        form.findField('minDefectCode').hide();
        form.findField('mrjDefectCode').hide();

        //form.findField('nic').hide();
        //        //////console.log(record.get("userGroups"));
        //        
        //        var userGroupsStore = Ext.create("Ext.data.Store",{
        //            model : 'SingerTest.model.UserGroups'
        //        });
        //        var userGroupsArray = record.get("userGroups");
        //        for (var i = 0; i < userGroupsArray.length; i++) {
        //             var singleGroup = Ext.create("SingerTest.model.UserGroups",{
        //                groupId : userGroupsArray[i].groupId,
        //                groupName :  userGroupsArray[i].groupName,
        //            });
        //            userGroupsStore.add(singleGroup);
        //            //var groupName = 
        //        }
        //        
        //form.findField('password1').setValue(record.get('password'));
        //            form.findField('password').clearInvalid();

    },
    insertFormCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    openAddMinorGrid: function (view, record) {
        //////console.log(record.get('mjrDefectCode'));
        singer.AppParams.TEMP = record.get('mjrDefectCode');
        var store = this.getMinorDefectsStore();
        //store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'MinorDefects/getMinorDefects?mrjDefectCode=1&mindefectcode=0&mindefectdesc=all&mindefectstatus=0'
        this.getController('CoreController').loadBindedStore(store);
        var grid = Ext.widget('minordefectgrid');

        var win = Ext.create("Ext.window.Window", {
            width: 700,
            height: 500,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            id: 'defWindow',
            items: [grid]
        });
        win.setTitle("Major Code: " + record.get('mjrDefectCode'));
        singer.AppParams.TEMP = record.get('mjrDefectCode');
        //////console.log(singer.AppParams.TEMP);
        win.show();

    },
    openAddNewMajor: function () {
        this.getController('CoreController').openInsertWindow('newmajordefect');
    },
    openAddNewMinor: function () {
        this.getController('CoreController').openInsertWindow('newminordefect');
        var grid = this.getMinorDefectGrid();
        var id = singer.AppParams.TEMP; //Ext.getCmp('tempWindow').title.substring(0, 1);
        var form = this.getNewMinorDefect().down('form').getForm();
        //////console.log(id);
        form.findField('mrjDefectCode').setValue(id);
        form.findField('mrjDefectCode').setVisible(false);
        form.findField('minDefectCode').hide();

    },
    updataRecordMajor: function (btn) {
        var me=this;
        var bt = btn;
        Ext.MessageBox.confirm('Are you sure?', 'Do you want to save changes?', function (btn) {
            if (btn === 'yes') {
                me.getController('CoreController').onRecordUpdate(bt, me.getDefectsStore(), 'Category Data Updated.', 'Updating Category', true, true, me.getMajorDefectGrid());
                var store = me.getDefectsStore();
                var grid = me.getMajorDefectGrid();
//                store.load();
//                grid.bindStore(store);
            }
        });


    },
    updataRecordMinor: function (btn) {
        var me=this;
        var bt=btn;
        Ext.MessageBox.confirm('Are you sure?', 'Do you want to save changes?', function (btn) {
            if (btn === 'yes') {
                var form = me.getNewMinorDefect().down('form').getForm();
                var id = form.findField('mrjDefectCode').getValue();

                var store = me.getMinorDefectsStore();
                store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'MinorDefects/getMinorDefects?mjrdefectcode=' + id;

                me.getController('CoreController').onRecordUpdate(bt, store, 'Minor Defect Updated.', 'Updating Minor Defect', true, true, me.getMinorDefectGrid());
                var store = me.getMinorDefectsStore();
                //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Issue Created.', 'Creating New Issue', true, true, this.getDeviceIssueGrid(), user);
                //       // thisme.getController('Main').initializeAppReq();
                var grid = me.getMinorDefectGrid();
                store.load();
                grid.bindStore(store);
            }
        });
    },
    insertRecordMajor: function (btn) {

        var store = this.getDefectsStore();
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Defect Created.', 'Creating New Defect', true, true, this.getMajorDefectGrid(), false);
        var grid = this.getMajorDefectGrid();
        //        store.load();
        //        grid.bindStore(store);
    },
    insertRecordMinor: function (btn) {

        //        var store = this.getMinorDefectsStore();
        //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Minor Defect Created.', 'Creating New Minor Defect', true, true, this.getMinorDefectGrid(), false);
        //        var grid = this.getMinorDefectGrid();
        //        store.load();
        //        grid.bindStore(store);
        var form = this.getNewMinorDefect().down('form').getForm();
        var id = form.findField('mrjDefectCode').getValue();

        var store = this.getMinorDefectsStore();
        //        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getLevels?levelcateid=' + id;
        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'MinorDefects/getMinorDefects?mjrdefectcode=' + id;
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Minor Defect Created.', 'Creating New Minor Defect', true, true, this.getMinorDefectGrid(), false);
        //        store.load();
    }
    //    updateFormSubmitMajor: function(btn) {
    ////        var me = this;
    //        //////console.log('the store');
    //        //////console.log(this.getDefectsStore());
    //        this.getController('CoreController').onRecordUpdate(btn, this.getDefectsStore(), 'Defect Data Updated.', 'Updating Defect', true, true, this.getMajorDefectGrid());
    //    },
    //    updateFormSubmitMinor: function(btn) {
    ////        var me = this;
    //        //////console.log('the store');
    //        //////console.log(this.getDefectsStore());
    //        this.getController('CoreController').onRecordUpdate(btn, this.getDefectsStore(), 'Defect Data Updated.', 'Updating Defect', true, true, this.getMinorDefectGrid());
    //    }
    //    registerNewVTypes: function() {
    //        var me = this;
    //        Ext.apply(Ext.form.field.VTypes, {
    //            ValidExtCode: function(val, field) {
    //                var form = field.up('form').getForm();
    //                var invalidExtCode = false;
    //                if (!Ext.getCmp('supplierform-create').isHidden()) {
    //                    invalidExtCode = me.checkExtCodeExist(val);
    //                } else if (!Ext.getCmp('supplierform-save').isHidden() && (form.findField('current_ext_code').getValue() !== val)) {
    //                    invalidExtCode = me.checkExtCodeExist(val);
    //                }
    //                return !invalidExtCode;
    //            },
    //            ValidExtCodeText: 'Ext Code is not available. Please use different Ext Code.'
    //        });
    //    },

});