
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.controller.JobStatus', {
    extend: 'Ext.app.Controller',
//    alias: 'widget.statusMainview',

    controllers: ['CoreController', 'Main'],
    stores: [
        'LoggedInUserData', 'GetWorkOrderList', 'WorkOrdeMaintainceDeatails', 'WorkOrderOpening', 'WorkOrderMaintainAdd',
        'DefectStore', 'WorkOrderAllGetList'
    ],
    models: [
        'JobStatusDefect', 'JobstatusParts', 'JobStatusestimate', 'WorkOrderMaintain', 'LoggedInUser'
    ],
    views: [
        'ServiceModule.JobStatus.JobStatusmainview',
        'ServiceModule.WorkOrderMaintenace.MainGrid',
        'ServiceModule.WorkOrderMaintenace.AddNew',
        'ServiceModule.WorkOrderMaintenace.SelectWorkOrder',
        'ServiceModule.WorkOrderMaintenace.SelectWorkOrder2',
        'ServiceModule.WorkOrderMaintenace.AddDefects',
        'ServiceModule.WorkOrderMaintenace.AddSpareParts',
        'ServiceModule.WorkOrderMaintenace.MaintenanceView',
        'Administrator.InventoryUploadFunction.AddNewImeis'
    ],
    refs: [
        {
            ref: 'JobStatusmainview',
            selector: 'statusMainview'
        },
        // -->            

        {
            ref: 'MainGrid',
            selector: 'wrkOrdrMaintneGrid'
        },
        {
            ref: 'AddNew',
            selector: 'addnewform'
        },
        {
            ref: 'SelectWorkOrder2',
            selector: 'selectWrkOrderGrid2'
        },
        {
            ref: 'AddDefects',
            selector: 'addDeffectWindow'
        },
        {
            ref: 'AddSpareParts',
            selector: 'addSparePartsWindow'
        },
        {
            ref: 'MaintenanceView',
            selector: 'wrkOmaintenaceView'
        },
        {
            ref: 'EditWorkMaintaince',
            selector: 'editForm'
        },
        {
            ref: 'AddNewImeis',
            selector: 'addimeis'
        }


        // -->
    ],
    init: function () {
        var me = this;
        me.control({
            'statusMainview': {
                added: this.initializeMainView
            },
            'statusMainview button[action=view_job_status] ': {
                click: this.loadJobDetails
            },
            'statusMainview button[action=clear_search] ': {
                click: this.clearDetails
            },
            'statusMainview button[action=select_Work_Order]': {
                click: this.openSelectWorkOrderWindow
            },
            'selectWrkOrderGrid2': {
                selectWrkOrder2: this.clickOnSelectWorkOrder2
            },
            'grid#defctGrid': {
                itemclick: this.defectGridItemClick
            },
            'grid#prtsGrid': {
                itemclick: this.partGridItemClick
            },
            'grid#jbestimategrid': {
                itemclick: this.estimateGridItemClick
            },
            'addimeis button[action=saveExcelData]': {
                click: this.saveExcel
            },
            'uploadImeisForm button[action=uploadsave]': {
                click: this.saveuploadExcel
            },
            'uploadImeisForm button[action=uploadreg]': {
                click: this.warrentyRegUploadExcel
            },
            'uploadImeisForm button[action=cancel]': {
                click: this.warrentyRegCancel
            },
            'addimeis button[action=saveToExcel]': {
                click: this.saveToExcel
            },
            'uploadImeisForm button[action=saveToExcelExImeis]': {
                click: this.saveToExcelExImeis
            },
            'addimeis button[action=deleteExcelData]': {
                click: this.deleteImeis
            },
            'addimeis button[action=setDefault]': {
                click: this.setDefaultBisId
            },
            'addimeis button[action=updateDebitBisId]': {
                click: this.setDebitBisId
            }

        });
    },
    estimateGridItemClick: function (me, record, item, index, e, eOpts) {
        console.log('EEEEEE: ', record);
        Ext.Msg.show({title: 'Spare Parts', msg: 'Est.Ref No: ' + record.data.esRefNo + '<br> Status: ' + record.data.statusMsg, minWidth: 900});
    },
    partGridItemClick: function (me, record, item, index, e, eOpts) {
        console.log('PPPPP: ', record);
        Ext.Msg.show({title: 'Spare Parts', msg: 'Part Desc: ' + record.data.partDesc + '<br> Price: ' + record.data.partSellPrice, minWidth: 900});
    },
    defectGridItemClick: function (me, record, item, index, e, eOpts) {
        console.log('RRRRRRRRL: ', record);
        Ext.Msg.show({title: 'Defect', msg: 'Major: ' + record.data.majDesc + '<br> Minor: ' + record.data.minDesc, minWidth: 900});
    },
    initializeMainView: function () {
        //console.log('job status window');
    },
    clickOnSelectWorkOrder2: function (btn, rowIndex) {
        console.log('Click On Select Work Order', rowIndex.data);
        var wrkNO = rowIndex.data.workOrderNo;
        var immi = rowIndex.data.imeiNo;
        Ext.getCmp('job_status_WO_field').setValue(wrkNO);
//        Ext.getCmp('addnewform-imeiNo').setValue(immi);
//
//        var repLevelStr = Ext.getStore('ReplevelSTR');
//        repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + immi;
//        repLevelStr.load();

        var cancelFrm = btn.up('window');
        cancelFrm.close();
        var under = Ext.getCmp('addnewform-underWar');
        var non = Ext.getCmp('addnewform-nonWar');
//        if (rowIndex.get('warrentyVrifType') === 1) {
//            under.show();
//            under.allowBlank = false;
//            non.hide();
//            non.allowBlank = true;
//        } else {
//            under.hide();
//            under.allowBlank = true;
//            non.show();
//            non.allowBlank = false;
//        }

        console.log("Work No :" + wrkNO);

        // ---------> Adding load job details method


        //var bt = btn;
        var WOnum = wrkNO;
        var imei = "";
        var nic = "";
        var rcc = "";

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;

        //MASK
        Ext.Msg.wait('Please wait', 'Gathering information...', {
            interval: 300
        });


        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderInquries/getWorkOrderStatusInquire?workOrderNo=' + WOnum + '&imeiNo=' + imei + '&customerNic=' + nic + '&rccReference=' + rcc,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response, btn) {
                var responseData = Ext.decode(response.responseText);

                if (parseInt(responseData.totalRecords) == -9) {
                    Ext.Msg.alert("Error", "Problem in the Database.");
                } else {

                    if (responseData.data[0] === undefined) {
                        console.log("work_order_status_inquire_method call1");
                        Ext.Msg.alert("Error", "There is no such a WO number");

                        //                    bt.up('form').getForm().setValue('');
                        //                    console.log(Ext.getCmp('maintainForm').getForm());
                        //                    Ext.getCmp('maintainForm').getForm().setValue('');
                        //                    Ext.getCmp('maintainForm').getForm().setValue('');

                        //   console.log("RESPONSE >>>",response);

                        var defectgrd = Ext.getCmp('defctGrid').getStore();
                        var prtsgrd = Ext.getCmp('prtsGrid').getStore();
                        var estimategrd = Ext.getCmp('jbestimategrid').getStore();
                        defectgrd.removeAll();
                        prtsgrd.removeAll();
                        estimategrd.removeAll();

                        Ext.getCmp('job_status_WO_field').setValue('');
                        Ext.getCmp('job_status_IMEI_field').setValue('');
                        Ext.getCmp('job_status_NIC_field').setValue('');
                        Ext.getCmp('job_status_RCC_field').setValue('');

                    } else {
                        //                     var WOopen = Ext.getCmp('maintainForm');
                        Ext.getCmp('workIdLbl').setText(' Work Order No : ' + responseData.data[0].workOrderNo);
                        console.log("work_order_status_inquire_method call2");
                        console.log("RESPONSE >>>", response);
                        // bt.up('form').getForm().setValues(responseData.data[0]);
                        Ext.getCmp('basicDetailsForm').getForm().setValues(responseData.data[0]);
                        Ext.getCmp('maintainForm').getForm().setValues(responseData.data[0]);
                        Ext.getCmp('closeForm').getForm().setValues(responseData.data[0]);

                        var defectgrd = Ext.getCmp('defctGrid').getStore();
                        //Get height of the first grid row panel var row = firstgrid.getView().getNode("row number of first grid panel which you want increase");

                        //Apply the first grid row heigt to second grid row
                        // var row = defectgrd.getView().getNode(1);
                        // var height = Ext.get(row).getHeight();
                        //Ext.get(row).setHeight('100px');

                        var prtsgrd = Ext.getCmp('prtsGrid').getStore();
                        var estimategrd = Ext.getCmp('jbestimategrid').getStore();
                        //  var defect
                        defectgrd.removeAll();
                        defectgrd.add(responseData.data[0].woDefect);

                        prtsgrd.removeAll();
                        prtsgrd.add(responseData.data[0].partList);

                        estimategrd.removeAll();
                        estimategrd.add(responseData.data[0].woEstimate);

                        //   Ext.getCmp('job_status_WO_field').setValue('');
                        //   Ext.getCmp('job_status_IMEI_field').setValue('');
                        //   Ext.getCmp('job_status_NIC_field').setValue('');
                        //   Ext.getCmp('job_status_RCC_field').setValue('');


                        if (responseData.data[0].woDefect > 0) {
                            Ext.getCmp('defect_field').setValue(responseData.data[0].woDefect[0].defects);
                            console.log("work_order_status_inquire_method call3");
                        }
                    }

                }


                Ext.Msg.hide();
            }
        });


        // <--------

    },
    loadJobDetails: function (btn) {
        var bt = btn;
        var WOnum = Ext.getCmp('job_status_WO_field').getValue();
        var imei = Ext.getCmp('job_status_IMEI_field').getValue();
        var nic = Ext.getCmp('job_status_NIC_field').getValue();
        var rcc = Ext.getCmp('job_status_RCC_field').getValue();
//        var actTake = Ext.getCmp('actionTakenJS').getValue();
//        var inv = Ext.getCmp('invoiceJS').getValue();
//        var WOMR = Ext.getCmp('woMainRemarksJS').getValue();

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;

        //MASK
        Ext.Msg.wait('Please wait', 'Gathering information...', {
            interval: 300
        });



//       Ext.getBody().unmask();

//        Ext.getBody().mask('Please wait...', 'loading');
//
//        Ext.getBody().unmask();



        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrderInquries/getWorkOrderStatusInquire?workOrderNo=' + WOnum + '&imeiNo=' + imei + '&customerNic=' + nic + '&rccReference=' + rcc,
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            success: function (response, btn) {

                var responseData = Ext.decode(response.responseText);
                console.log("responseData", responseData.totalRecords);
                var responseCount = responseData.totalRecords;

                if (responseCount == 1000000) {

                    console.log(">> DB DOWN");
                    Ext.Msg.alert("Operation failed", "Cannot connect to the database. Please contact the system Admin");
                    console.log(">> DB DOWN2");

                } else if (responseData.data[0] === undefined) {
                    console.log("work_order_status_inquire_method call1");


//                    bt.up('form').getForm().setValue('');
//                    console.log(Ext.getCmp('maintainForm').getForm());
//                    Ext.getCmp('maintainForm').getForm().setValue('');
//                    Ext.getCmp('maintainForm').getForm().setValue('');

                    //   console.log("RESPONSE >>>",response);

                    var defectgrd = Ext.getCmp('defctGrid').getStore();
                    var prtsgrd = Ext.getCmp('prtsGrid').getStore();
                    var estimategrd = Ext.getCmp('jbestimategrid').getStore();
                    defectgrd.removeAll();
                    prtsgrd.removeAll();
                    estimategrd.removeAll();

                    Ext.getCmp('job_status_WO_field').setValue('');
                    Ext.getCmp('job_status_IMEI_field').setValue('');
                    Ext.getCmp('job_status_NIC_field').setValue('');
                    Ext.getCmp('job_status_RCC_field').setValue('');

                    Ext.Msg.alert("Error", "There is no such a WO number");
                    console.log(">> DB DOWN33");
                } else {
                    console.log("else");
//                     var WOopen = Ext.getCmp('maintainForm');
                    Ext.getCmp('workIdLbl').setText(' Work Order No : ' + responseData.data[0].workOrderNo);
                    console.log("work_order_status_inquire_method call2");
                    console.log("RESPONSE >>>", response);
                    bt.up('form').getForm().setValues(responseData.data[0]);
                    Ext.getCmp('maintainForm').getForm().setValues(responseData.data[0]);
                    Ext.getCmp('closeForm').getForm().setValues(responseData.data[0]);

                    var defectgrd = Ext.getCmp('defctGrid').getStore();
                    var prtsgrd = Ext.getCmp('prtsGrid').getStore();
                    var estimategrd = Ext.getCmp('jbestimategrid').getStore();
                    //  var defect
                    defectgrd.removeAll();
                    defectgrd.add(responseData.data[0].woDefect);

                    prtsgrd.removeAll();
                    prtsgrd.add(responseData.data[0].partList);

                    estimategrd.removeAll();
                    estimategrd.add(responseData.data[0].woEstimate);

                    //   Ext.getCmp('job_status_WO_field').setValue('');
                    //   Ext.getCmp('job_status_IMEI_field').setValue('');
                    //   Ext.getCmp('job_status_NIC_field').setValue('');
                    //   Ext.getCmp('job_status_RCC_field').setValue('');


                    if (responseData.data[0].woDefect > 0) {
                        Ext.getCmp('defect_field').setValue(responseData.data[0].woDefect[0].defects);
                        console.log("work_order_status_inquire_method call3");
                    }
                }
                Ext.Msg.hide();
            }
        });
    },
    loadJobDetails2: function (btn) {
    },
    openSelectWorkOrderWindow: function () {
        console.log('test');
        console.log('test - Open Select Work Order Window');
        // var form = Ext.widget('addnewform');

        var store = this.getWorkOrderAllGetListStore();

        this.getController('CoreController').loadBindedStore(store);

        var form = Ext.widget('selectWrkOrderGrid2');
        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Select Work Order',
            items: [form]
        });
        win.show();



        //  var form = Ext.widget('selectWrkOrderGrid');
//       var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Add New',
//            items: [form]
//        });
//        var loginData = this.getLoggedInUserDataStore();
//        //console.log(loginData.data.getAt(0).get('commonName'));
//        Ext.getCmp('technicianName').setValue(loginData.data.getAt(0).get('commonName'));
//        Ext.getCmp('testGrid').getStore().removeAll();
//        Ext.getCmp('spareN').getStore().removeAll();
//        Ext.getCmp('addnewform-save').hide();
//        Ext.getCmp('addnewform-submit').show();
//        win.show();
        // var MaintainWorkOrderform = Ext.widget('addnewform');
        //var store = this.getWorkOrderOpeningStore();
//        var store = this.getWorkOrderMaintainAddStore();
//
//        this.getController('CoreController').loadBindedStore(store);
//        //=====================
////            var repLevelStr = this.getReplevelSTRStore();
////            repLevelStr.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'addLevel/getRepairLevelsForImei?imie_no=' + record.get("eventId");
////        
//        //=====================
//        var form = Ext.widget('selectWrkOrderGrid');
//        var win = Ext.create("Ext.window.Window", {
//            layout: 'fit',
//            iconCls: 'icon-verification',
//            modal: true,
//            constrain: true,
//            title: 'Add New',
//            items: [form]
//        });
        win.show();
    },
    clearDetails: function (btn) {
        console.log('showClear');
        //this.getController('CoreController').openInsertWindow('QuickWorkOrderView');
        //Ext.getCmp('newdeviceissue-create').show();
        Ext.getCmp('job_status_WO_field').setValue('');
        Ext.getCmp('job_status_IMEI_field').setValue('');
        Ext.getCmp('job_status_NIC_field').setValue('');
        Ext.getCmp('job_status_RCC_field').setValue('');
        Ext.getCmp('name_field').setValue('');
        Ext.getCmp('address_field').setValue('');
        Ext.getCmp('nic_field').setValue('');
        Ext.getCmp('telephone_field').setValue('');
        Ext.getCmp('email_field').setValue('');

        Ext.getCmp('imei_field').setValue('');
        Ext.getCmp('product_field').setValue('');
        Ext.getCmp('brand_field').setValue('');
        Ext.getCmp('model_field').setValue('');
        Ext.getCmp('acc_retained_field').setValue('');
        Ext.getCmp('defect_field').setValue('');
        Ext.getCmp('rcc_retained_field').setValue('');
        Ext.getCmp('dist_shop_field').setValue('');
        Ext.getCmp('date_sale_field').setValue('');
        Ext.getCmp('remark_field').setValue('');

        Ext.getCmp('warranty_type_field').setValue('');
        Ext.getCmp('received_date_field').setValue('');
        Ext.getCmp('non_war_field').setValue('');
        Ext.getCmp('non_war_remark_field').setValue('');
        Ext.getCmp('cus_complain_field').setValue('');

        Ext.getCmp('actionTaken').setValue('');
        Ext.getCmp('invoiceNo').setValue('');
        Ext.getCmp('remark_field').setValue('');

        Ext.getCmp('tech').setValue('');
        Ext.getCmp('workIdLbl').setText('');

        var defectgrd = Ext.getCmp('defctGrid').getStore();
        defectgrd.removeAll();
        var prtsgrd = Ext.getCmp('prtsGrid').getStore();
        prtsgrd.removeAll();
        var estimategrd = Ext.getCmp('jbestimategrid').getStore();
        estimategrd.removeAll();
        // defectgrd.store.removeAll(true);
        Ext.getCmp('repair_level_field').setValue('');
        Ext.getCmp('repair_amount_field').setValue('');
        Ext.getCmp('repair_status_field').setValue('');
        Ext.getCmp('total_cost_field').setValue('');

        Ext.getCmp('deliver_type_field').setValue('');
        Ext.getCmp('transfer_location_field').setValue('');
        Ext.getCmp('courier_no_field').setValue('');
        Ext.getCmp('gate_pass_field').setValue('');
        Ext.getCmp('delivery_date_field').setValue('');
        Ext.getCmp('remark2').setValue('');

    },
    //Inventory Function
    saveExcel: function (btn) {
        Ext.getBody().mask('Please wait...');
        console.log("IMEISSS");
        var excel = Ext.getCmp('image_path_srv1');
        var me = this;

//        var siteId = Ext.getStore('LoggedInUserData').getAt(0).data.extraParams;
        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;





        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'InventoryUpload/putExcel',
            method: "PUT",
            timeout: 50000,
            async: false,
            isUpload: true,
            headers: {'Content-Type': 'multipart/form-data'},
            jsonData: {
                excelBase64: excel.getValue()
            },
            success: function (response) {

                var responseData = Ext.decode(response.responseText);
                console.log("responseData", responseData)
                if (responseData == 9) {
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Failed", "Insertion was failed");
                } else {
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Success", "Insert Successfully");
                }

                Ext.getBody().unmask();
                Ext.Msg.alert("Success", "Insert Successfully");

            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                console.log("Fail to upload Excel Sheet");
            }
        });
    },
    saveToExcel: function (btn) {
        console.log("EXCELLL");
//        var excel = Ext.getCmp('image_path_srv1');
//        var me = this;

        var url = singer.AppParams.JBOSS_PATH + 'InventoryUpload/includeExcel';
        window.open(url, '_blank');

        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;

    },
    saveToExcelExImeis: function (btn) {
        console.log("EXCELLL");
       

        var url = singer.AppParams.JBOSS_PATH + 'InventoryUpload/includeImeiExcel';
        window.open(url, '_blank');

       // var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;

    },
    deleteImeis: function (btn) {
        console.log("EXCELLL");
        var excel = Ext.getCmp('image_path_srv1');
        var me = this;

//        var siteId = Ext.getStore('LoggedInUserData').getAt(0).data.extraParams;
        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;



        Ext.MessageBox.confirm('Delete', 'Are you sure ?', function (btn) {
            if (btn === 'yes') {
                Ext.getBody().mask('Please wait...');
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'InventoryUpload/deleteImeis',
                    method: "PUT",
                    timeout: 5000,
                    async: false,
                    isUpload: true,
                    headers: {'Content-Type': 'multipart/form-data'},
                    jsonData: {
                        excelBase64: excel.getValue()
                    },
                    success: function (response) {
                        Ext.getBody().unmask();
                        Ext.Msg.alert("Success", "Deleted Successfully");

                    },
                    failure: function (response, options) {
                        Ext.getBody().unmask();
                        console.log("Fail to Delete");
                    }
                });
            }
        });
    },
    setDefaultBisId: function (btn) {
        console.log("BISID");
        var excel = Ext.getCmp('image_path_srv1');
        var me = this;

//        var siteId = Ext.getStore('LoggedInUserData').getAt(0).data.extraParams;
        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;

        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'InventoryUpload/updateMaster',
            method: "POST",
            timeout: 5000,
            async: false,
            isUpload: true,
            headers: {'Content-Type': 'multipart/form-data'},
            jsonData: {
                excelBase64: excel.getValue()
            },
            success: function (response) {
                Ext.Msg.alert("Success", "Update Bis Id Successfully");

            },
            failure: function (response, options) {
                console.log("Fail to Update Bis Id");
            }
        });
    },
    setDebitBisId: function (btn) {
        console.log("updateDebitNoteBisID BISID");

        var me = this;
        var bisId = Ext.getCmp('bis_id').getValue();

//        Ext.getBody().mask('Please wait...');
    
    
    if(bisId != ""){
        //MASK
        Ext.Msg.wait('Please wait', 'Updating information...', {
            interval: 300
        });


        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'InventoryUpload/updateDebitNoteBisID?bisId=' + bisId,
            method: "POST",
            timeout: 600000,
            async: true,
            // isUpload: true,
            // headers: {'Content-Type': 'multipart/form-data'},
//            jsonData: {
//                //excelBase64: excel.getValue()
//            },
            success: function (response) {
                Ext.getBody().unmask();
                Ext.Msg.alert("Success", "Update BIS ID Successfully");

            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                console.log("Fail to Update BIS ID");
            }
        });
    }else{
         Ext.Msg.alert("Information", "Please enter BIS ID ");
    }
    },
    
    //UPLOAD IMEI
    saveuploadExcel: function (btn) {
        Ext.getBody().mask('Please wait...');
        console.log("Upload IMEISSS");
        var excel = Ext.getCmp('image_path_srv2');
        var me = this;

//        var siteId = Ext.getStore('LoggedInUserData').getAt(0).data.extraParams;
        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;





        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'InventoryUpload/putExcelImeiUpload',
            method: "PUT",
            timeout: 1200000,
            async: true,
            isUpload: true,
            headers: {'Content-Type': 'multipart/form-data'},
            jsonData: {
                excelBase64: excel.getValue()
            },
            success: function (response) {

                var responseData = Ext.decode(response.responseText);
                console.log("responseData", responseData)
                if (responseData == 9) {
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Failed", "Insertion was failed");
                } else {
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Success", "Insert Successfully");
    }

                Ext.getBody().unmask();
                Ext.Msg.alert("Success", "Insert Successfully");

            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                console.log("Fail to upload Excel Sheet");
            }
});
    },
    //END UPLOAD IMEI
    
    //WARRENTY REG CANCEL
    warrentyRegCancel: function (btn) {

        this.getController('CoreController').onInsertFormCancel(btn);
    },
    
    //END WARRENTY REG CANCEL
    //WARRANTY REG UPLOAD
    
    warrentyRegUploadExcel: function (btn) {
        Ext.getBody().mask('Please wait...');
        console.log("Warrenty Reg Upload IMEISSS");
//        var excel = Ext.getCmp('image_path_srv2');
//        var me = this;
//
////        var siteId = Ext.getStore('LoggedInUserData').getAt(0).data.extraParams;
//        var userId = Ext.getStore('LoggedInUserData').getAt(0).data.userId;

       Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'InventoryUpload/addUploadImiesWarrentyReg',
            method: "PUT",
            timeout: 4800000,
            async: true,
            //isUpload: true,
            headers: {'Content-Type': 'multipart/form-data'},
//            jsonData: {
//                excelBase64: excel.getValue()
//            },
          
            success: function (response) {
                var url = singer.AppParams.JBOSS_PATH + 'InventoryUpload/includeImeiExcel';
                window.open(url, '_blank');
                var responseData = Ext.decode(response.responseText);
                console.log("responseData", responseData)
                if (responseData == 9) {
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Failed", "Insertion was failed");
                } else {
                    Ext.getBody().unmask();
                    
                    Ext.Msg.alert("Success", "Insert Successfully");
                }
     
                Ext.getBody().unmask();
                
                Ext.Msg.alert("Success", "Insert Successfully");
                
            },
            failure: function (response, options) {
                Ext.getBody().unmask();
                
                console.log("Fail to upload Excel Sheet");
            }
            
        });
        
        
        
    }
    //END WARRANTY REG UPLOAD
});