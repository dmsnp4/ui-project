Ext.define('singer.controller.StockandClearanceRepairController', {
    extend: 'Ext.app.Controller',
    models: [
        'WorkOrderOpen',
        'WarrentyRegistration',
        'WorkOrderMaintainDetails'
    ],
    stores: [
        'WorkOrderOpening',
        'LoggedInUserData',
        'WorkOrderHistory',
        'StockClearanceRepair',
        'SelectStockWorkOrder',
        'GetQuickWorkOrderListForSelect'
    ],
    views: [
        'ServiceModule.StockAndClearanceRepair.WorkOrderOpenFormForStockRepair',
        'ServiceModule.StockAndClearanceRepair.WorkOrderOpenGridForStockRepair',
        'ServiceModule.StockAndClearanceRepair.WorkOrderOpenView',
        'ServiceModule.StockAndClearanceRepair.RegisterNewImeiForm',
        'ServiceModule.WorkOrderOpen.RepairHistoryView',
        'ServiceModule.StockAndClearanceRepair.OpenStockQuickWorkOrderForm',
        'ServiceModule.WorkOrderOpen.HistoryDetailView',
        'ServiceModule.StockAndClearanceRepair.selectStockWorkOrderForm'
    ],
    refs: [
        {
            ref: 'WorkOrderOpenFormForStockRepair',
            selector: 'WorkOrderOpenFormForStockRepair'
        },
        {
            ref: 'WorkOrderOpenGridForStockRepair',
            selector: 'WorkOrderOpenGridForStockRepair'
        },
        {
            ref: 'RegisterNewImeiForm',
            selector: 'registerNewImeiForm'
        },
        {
            ref: 'RepairHistoryView',
            selector: 'repairHistoryView'
        },
        {
            ref: 'OpenStockQuickWorkOrderForm',
            selector: 'OpenStockQuickWorkOrderForm'
        },
        {
            ref: 'WorkOrderOpenView',
            selector: 'workOrderOpenView'
        },
        {
            ref: 'HistoryDetailView',
            selector: 'historydetailview'
        },
        {
            ref: 'selectStockWorkOrderForm',
            selector: 'selectStockWorkOrderForm'
        }
    ],
    init: function () {
        var me = this;
        me.control({
            'WorkOrderOpenGridForStockRepair': {
                added: this.initializeGrid
            },
            'WorkOrderOpenGridForStockRepair button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'WorkOrderOpenGridForStockRepair button[action=open_new_work_order_window]': {
                click: this.onOpenCreateWorkOrderWindow
            },
            'WorkOrderOpenGridForStockRepair button[action=open_quick_work_order_window]': {
                click: this.onOpenCreateQuickWorkOrderWindow
            },
            ' WorkOrderOpenGridForStockRepair': {
                on_open_view: this.onViewWorkOrderDetails
            },
            '  WorkOrderOpenGridForStockRepair': {
                itemdblclick: this.onViewWorkOrderDetails
            },
            '   WorkOrderOpenGridForStockRepair': {
                on_open_delete: this.onDeleteWorkOrderDetails
            },
            '    WorkOrderOpenGridForStockRepair': {
                on_open_edit: this.onEditWorkOrderDetails
            },
            '     WorkOrderOpenGridForStockRepair': {
                on_open_print: this.onPrintWorkOrderReport
            },
            'workOrderOpenForm': {
                on_nic_validity_change: this.onNicFieldValidityChange
            },
            ' workOrderOpenForm': {
                on_nic_change: this.onNicValChange
            },
            '   WorkOrderOpenFormForStockRepair': {
                on_imei_validity_change: this.onImeiValidityChange
            },
            '    WorkOrderOpenFormForStockRepair': {
                on_imei_change: this.onImeiChange
            },
            '  WorkOrderOpenFormForStockRepair button[action=verify_nic]': {
                click: this.onNicVerifyBtnClick
            },
            'WorkOrderOpenFormForStockRepair button[action=register_imei]': {
                click: this.onRegisterNewImeiBtnClick
            },
            'WorkOrderOpenFormForStockRepair button[action=view_repair_history]': {
                click: this.onClickRepairHistoryBtn
            },
            'WorkOrderOpenFormForStockRepair button[action=verify_imei]': {
                click: this.onImeiVerifyBtnClick
            },
            'historydetailview button[action=cancel]': {
                click: this.closeWindow
            },
            'registerNewImeiForm button[action=cancel]': {
                click: this.closeWindow
            },
            'registerNewImeiForm button[action=create_new_imei]': {
                click: this.onCreateNewImeiWarrantyBtnClick
            },
            'WorkOrderOpenFormForStockRepair button[action=cancel]': {
                click: this.onWorkOrdercloseWindow
            },
            'WorkOrderOpenFormForStockRepair button[action=create]': {
                click: this.onClickWorkOrderCreateBtn
            },
            'OpenStockQuickWorkOrderForm button[action=save]': {
                click: this.onClickWorkOrderSaveBtn
            },
            'repairHistoryView': {
                close: this.onCloseRepairHistoryView
            },
            ' repairHistoryView': {
                view_repair_history_print: this.onPrintRepairHistoryView
            },
            'repairHistoryView button[action=clearSearch]': {
                click: this.onClearSearchRepairHistory
            },
            'repairHistoryView button[action=cancel]': {
                click: this.closeWindow
            },
            'OpenStockQuickWorkOrderForm button[action=cancel]': {
                click: this.closeWindow
            },
            'openQuickWorkOrderForm button[action=quickWorkVerify]': {
                click: this.onQuickWorkVerify
            },
            'OpenStockQuickWorkOrderForm': {
                on_work_order_change: this.onWorkOrderChange
            },
            'OpenStockQuickWorkOrderForm button[action=search_quick_work_order]': {
                click: this.onClickQuickWOSearchBtn
            },
            'OpenStockQuickWorkOrderForm button[action=create]': {
                click: this.onCreateQuickWorkOrder
            },
            'workOrderOpenView button[action=cancel]': {
                click: this.closeWindow
            },
            'selectStockWorkOrderForm': {
                selectWrkOrderForWOOPenAction: this.selectWrkOrderForWOOPen
            }
        });
    },
    registerNewVTypes: function () {
        var me = this;
        Ext.apply(Ext.form.field.VTypes, {
            phoneVtype: function (val, field) {
                var invalidImeiNo = false;
                if (!/^[0-9]*$/.test(field.getValue())) {
                    invalidImeiNo = true;
                }
                return !invalidImeiNo;
            },
            phoneVtypeText: 'Phone Number is Not Valid.',
            imeiVtype: function (val, field) {
                var invalidImeiNo = false;
                if (field.nextSibling('hidden').getValue() == '0') {
                    invalidImeiNo = true;
                }
                return !invalidImeiNo;
            },
            imeiVtypeText: 'IMEI is not Valid. Please verify entered IMIE number',
            emailMatchVtype: function (val, field) {
                var emailNotMatch = false;
                var form = field.up("form").getForm();
                if (!form.findField("email2").isHidden()) {
                    if (form.findField("email").getValue() != form.findField("email2").getValue()) {
                        emailNotMatch = true;
                    }
                }
                return !emailNotMatch;
            },
            emailMatchVtypeText: 'Emails not match. Please enter same email address.'
        });
    },
    closeWindow: function (btn) {
        var window = btn.up('window');
        window.close();
    },
    initializeGrid: function (grid) {
        var store = this.getStockClearanceRepairStore();
        this.getController('CoreController').loadBindedStore(store);
        this.registerNewVTypes();
    },
    onViewWorkOrderDetails: function (grid, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'workOrderOpenView', 'More Details of ' + record.get('workOrderNo'));
    },
    onPrintRepairHistoryView: function (grid, record) {
        //        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=WOmaintain.rptdesign&woNUM=" + record.get('workOrderNo'), 'Report of ' + record.get('workOrderNo') + " - Repair History Details");
        //        //console.log(record.getData())

        var win = Ext.widget('historydetailview');

        var form = win.down('form');
        //        //console.log(record)
        form.getForm().setValues(record.getData());
        form.getForm().findField('repairDate').setValue(new Date(record.getData().repairDate));

        var defects = Ext.create('Ext.data.Store', {
            model: 'singer.model.WorkOrderDefect'
        });
        Ext.getCmp('historydetailview-defectsGrid').bindStore(defects);

        var spares = Ext.create('Ext.data.Store', {
            model: 'singer.model.WorkOrderMaintainDetails'
        });
        Ext.getCmp('historydetailview-spares').bindStore(spares);

        var maintainDetail = Ext.create('Ext.data.Store', {
            model: 'singer.model.WorkOrderMaintain'
        });
        Ext.getCmp('historydetailview-woDetails').bindStore(maintainDetail);

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Gathering Information...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'WorkOrders/getWorkOrderDetail?workOrderNo=' + record.getData().workOrderNo + '',
            method: 'GET',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            //            params: JSON.stringify(x),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                //                //console.log(responseData);
                if (responseData.data.length !== 0) {
                    defects.add(responseData.data[0].woDefect);
                    spares.add(responseData.data[0].woMaintainDetials);
                    maintainDetail.add(responseData.data[0].woDetials);
                    //                    var form = win.down('form');
                    form.getForm().setValues(responseData.data[0]);


                } else {
                    Ext.Msg.alert("Error...", "Something went wrong gathering more information...");
                }

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
        win.show();
    },
    onPrintWorkOrderReport: function (grid, record) {
        var loginData = this.getLoggedInUserDataStore();
        //console.log(loginData.data.items[0].data);
        var loggeduser = loginData.data.items[0].data.firstName;

//        var docWindow = window.print();//(singer.AppParams.REPO + replaceWithNewFormat("doc"), '_blank');
        //window.onafterprint();
        //docWindow.focus();
        //console.log('eer');

        //this.getController('CoreController').viewDirectPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + record.get('workOrderNo') + "&BIS_ID=" + loggeduser, 'Report of ' + record.get('workOrderNo') + " - Work Order");
        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + record.get('workOrderNo') + "&BIS_ID=" + loggeduser, 'Report of ' + record.get('workOrderNo') + " - Work Order");

        var wonum = record.get('workOrderNo');
        var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + wonum + "&BIS_ID=" + loggeduser + 'Report of ' + wonum + " - Work Order";
        console.log('URI', URI);
        console.log('REPO', singer.AppParams.REPO);
    },
//    onPrintWorkOrderReport: function(grid, record) {
//        var loginData = this.getLoggedInUserDataStore();
//        var loggeduser = loginData.data.items[0].data.firstName;
//        var wonum = record.get('workOrderNo');
//        this.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + record.get('workOrderNo'), 'Report of ' + record.get('workOrderNo') + " - Work Order");
//        var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + wonum + "&BIS_ID=" + loggeduser + 'Report of ' + wonum + " - Work Order";
//        console.log('URI', URI);
//        console.log('REPO', singer.AppParams.REPO);
//    },
    onDeleteWorkOrderDetails: function (grid, record) {
        var me = this;
        var store = this.getWorkOrderOpeningStore();
        Ext.Msg.confirm('Remove Selected Work Order?', "Remove Selected Work Order : <b>" + record.get('workOrderNo') + "</b>", function (btn) {
            if (btn === 'yes') {
                var callback = function (response) {
                    if (response.returnFlag === '1') {
                        store.load();
                        singer.LayoutController.notify("Work Order Removed", "Work Order Removed from System.");
                    } else {
                        singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
                    }
                };
                me.submitDataToServer("WorkOrders/deleteWorkOrder", "POST", {
                    "workOrderNo": record.get('workOrderNo')
                }, "Removing work order..", callback);
            }
        });
    },
    onEditWorkOrderDetails: function (grid, record) {
        var me = this;
        this.getController('CoreController').openUpdateWindow(record, 'workOrderOpenForm', 'Modify Details of ' + record.get('workOrderNo'));
        var form = this.getWorkOrderOpenForm().down("form").getForm();
        //        form.findField("customerNic").setValue(record.get("customerName"));
        form.findField("customerNic").setReadOnly(true);

        form.findField("customerName").setValue(record.get("customerName"));
        form.findField("customerName").setReadOnly(true);

        form.findField("customerAddress").setValue(record.get("customerAddress"));
        form.findField("customerAddress").setReadOnly(true);

        form.findField("workTelephoneNo").setValue(record.get("workTelephoneNo"));
        form.findField("workTelephoneNo").setReadOnly(true);

        form.findField("email").setValue(record.get("email"));
        form.findField("email").setReadOnly(true);

        form.findField("email2").setValue(record.get("email"));
        form.findField("email2").setReadOnly(true);

        form.findField("imeiNo").setVisible(false);
        form.findField("imeiNo").setReadOnly(true);

        form.findField("product").setValue(record.get("product"));
        form.findField("product").setReadOnly(true);

        form.findField("brand").setValue(record.get("brand"));
        form.findField("brand").setReadOnly(true);

        form.findField("modleNo").setValue(record.get("modleNo"));
        form.findField("modleNo").setReadOnly(true);
        form.findField("assessoriesRetained").setReadOnly(true);
        form.findField("defectNo").setReadOnly(true);
        form.findField("rccReference").setReadOnly(true);
        form.findField("dateOfSale").setReadOnly(true);
        form.findField("serviceBisId").setReadOnly(true);
        form.findField("remarks").setReadOnly(true);
        form.findField("customerComplain").setReadOnly(true);
        Ext.getCmp('workOrderOpenForm-verifyimei').hide();
        Ext.getCmp('workOrderOpenForm-verify_nic').hide();
        Ext.getCmp('WO_Open_Edit_RDate').hide();
        form.findField("isImeiVerified").setValue("1");
        form.findField("modleNo").setReadOnly(true);

        // form.findField("expireStatus").setValue(record.get("expireStatus"));

        if (record.get("expireStatus") == 0) {
//            form.findField("expireStatus").setValue('Under Warranty');
        }

        if (record.get("expireStatus") == 1) {
//            form.findField("expireStatus").setValue('Out of Warranty');
        }

//        form.findField("code").show();
//        form.findField("registerDate").disable();

//        Ext.getCmp('WrntTyp').disable();
//        Ext.getCmp('wrntTYPCmbo').show();
//        //console.log(record.get('registerDate'));
//        var temp = new Date(record.get('registerDate'));
//        form.findField("registerDate").setValue(temp);
        form.findField("imeiNo").clearInvalid();
        //console.log('warranty type');
        //console.log(record.getData());
        if (record.getData().warrentyVrifType === 2) {
            form.findField('nonWarrantyRemarks').show();
        } else {
            form.findField('nonWarrantyRemarks').hide();
        }
    },
    onClickWorkOrderSaveBtn: function (btn) {
        var form = btn.up("form").getForm();
        var loginData = this.getLoggedInUserDataStore();
        //console.log("Form data",form.getValues())
        var rec = form.getRecord();//Ext.create("singer.model.WorkOrderOpen", form.getValues());

        rec.set(form.getValues());
        rec.set('bisId', loginData.data.getAt(0).get('extraParams'));
        //console.log("REcord",rec);
        //        var rec = Ext.create("singer.model.WorkOrderOpen", form.getValues());
        var me = this;
        var store = this.getWorkOrderOpeningStore();
        var callback = function (response) {
            if (response.returnFlag === '1') {
                store.load();
                singer.LayoutController.notify("Work Order Modified", "Work Order Modified.");
                btn.up("window").close();
            } else {
                singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
            }
        };
        //console.log(form.getRecord().getData())
        this.submitDataToServer("WorkOrders/editWorkOrder", "POST", rec.getData(), "Modifying  Work Order", callback);
    },
    onRegisterNewImeiBtnClick: function (btn) {
        this.onOpenRegisterNewImeiCreateWindow();
    },
    onWorkOrderChange: function (field, newVal) {
        if (!Ext.isEmpty(field.getValue())) {
            Ext.getCmp("openQuickWorkOrderForm-searchwo").setDisabled(false);
        } else {
            Ext.getCmp("openQuickWorkOrderForm-searchwo").setDisabled(true);
        }
        field.up("form").getForm().getFields().each(function (field) {
            if (field.getName() != "workOrderNo") {
                field.reset();
            }
        });
    },
    selectWrkOrderForWOOPen: function (grid, rec) {

        var form = this.getOpenStockQuickWorkOrderForm().down('form').getForm();

        var callback = function (response) {
            if (rec != undefined && rec != null) {
                rec.set("defectNo", parseInt(rec.get("defectNo")));
                form.loadRecord(rec);
                Ext.getCmp('cmbPayType1').setValue(2);

            } else {
                singer.LayoutController.createErrorPopup("No Work Orders Found", "No Work orders found. Please Enter Corrent Work Order Number and Search.", Ext.MessageBox.WARNING);
            }

            var cancelFrm = grid.up('window');
            cancelFrm.close();
        };
        this.verifyWithServer("WorkOrders/getWorkOrderDetails?workOrderNo=" + rec.data.workOrderNo, "GET", "Verifying NIC Number", callback);

    },
    onClickQuickWOSearchBtn: function (btn) {

//        var form = btn.up("form").getForm();
//        var callback = function (response) {
//            if (response.totalRecords > 0) {
//                var firstWoDataObj = response.data[0];
//                var rec = Ext.create("singer.model.WorkOrderOpen", firstWoDataObj);
//                rec.set("defectNo", parseInt(rec.get("defectNo")));
//                form.loadRecord(rec);
//                Ext.getCmp('cmbPayType1').setValue(2);
//                
//            } else {
//                singer.LayoutController.createErrorPopup("No Work Orders Found", "No Work orders found. Please Enter Corrent Work Order Number and Search.", Ext.MessageBox.WARNING);
//            }
//        };
//        this.verifyWithServer("WorkOrders/getWorkOrderDetails?workOrderNo=" + form.findField("workOrderNo").getValue(), "GET", "Verifying NIC Number", callback);


        var form = Ext.widget('selectStockWorkOrderForm');
        var store = Ext.getStore("GetQuickWorkOrderListForSelect");

        console.log(store);
        store.removeAll();
        store.load();


        var win = Ext.create("Ext.window.Window", {
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Select Work Order',
            items: [form]
        });
        win.show();


    },
    onCreateQuickWorkOrder: function (btn) {
        console.log('sddd');
        var form = btn.up("form").getForm();
        var loginData = this.getLoggedInUserDataStore();

        var rec = Ext.create("singer.model.WorkOrderOpen", form.getValues());
        //console.log(rec);
        rec.set('bisId', loginData.data.getAt(0).get('extraParams'));
        //         //console.log(rec.set('expirestate',1));

        var me = this;
        var store = this.getWorkOrderOpeningStore();
        var callback = function (response) {
            if (response.returnFlag === '1') {
                store.load();
                singer.LayoutController.notify("Work Order Modified", " Quick Work Order Modified.");
                btn.up("window").close();
            } else {
                singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
            }
        };
        this.QWOsubmitDataToServer("WorkOrders/editWorkOrder", "POST", rec.getData(), "Modifying Quick Work Order", callback);
    },
    onImeiChange: function (field) {
        if (!Ext.isEmpty(field.getValue())) {
            Ext.getCmp("WorkOrderOpenFormForStockRepair-verifyimei").setDisabled(false);
            //            Ext.getCmp("workOrderOpenForm-newimei").setDisabled(false);
        } else {
            Ext.getCmp("WorkOrderOpenFormForStockRepair-verifyimei").setDisabled(true);
            //            Ext.getCmp("workOrderOpenForm-newimei").setDisabled(true);
        }
        field.nextSibling('hidden').setValue("0");
        this.resetImieDependFields();
    },
    resetImieDependFields: function () {
        var form = this.getWorkOrderOpenFormForStockRepair().down("form").getForm();
        form.findField("product").reset();
        form.findField("brand").reset();
        form.findField("modleNo").reset();
        form.findField("product").setReadOnly(true);
        form.findField("brand").setReadOnly(true);
        form.findField("modleNo").setReadOnly(true);
        form.findField("product").allowBlank = true;
        form.findField("brand").allowBlank = true;
        form.findField("modleNo").allowBlank = true;
        form.findField("product").afterLabelTpl = '';
        form.findField("brand").afterLabelTpl = '';
        form.findField("modleNo").afterLabelTpl = '';
        form.findField("product").clearInvalid();
        form.findField("brand").clearInvalid();
        form.findField("modleNo").clearInvalid();
    },
    onImeiValidityChange: function (field) {
        var val = field.getValue();
        var form = field.up("form").getForm();
        if (val == 0) {
            Ext.getCmp("WorkOrderOpenFormForStockRepair-viewrepairhistory").setDisabled(true);
            form.findField("imeiNo").markInvalid("IMEI Number is not Verified or Invalid. Please verify entered IMEI number");
        } else {
            form.findField("imeiNo").clearInvalid();
            Ext.getCmp("WorkOrderOpenFormForStockRepair-viewrepairhistory").setDisabled(false);
        }
    },
    getVerificationCallBack: function () {

    },
    onImeiVerifyBtnClick: function (btn) {
        var form = btn.up("form").getForm();
        var field = form.findField("imeiNo");
        if (!Ext.isEmpty(field.getValue())) {
            var me = this;
            var callback = function (response) {
                me.resetImieDependFields();
                switch (response.totalRecords) {
                    case 0:
                        singer.LayoutController.createErrorPopup("Warning", "No Data Found", Ext.MessageBox.WARNING);
                        form.findField("isImeiVerified").setValue('0');
                        break;
                    case 0:
                        var responseData = response.data[0];
                        //singer.LayoutController.createErrorPopup("Warrenty Not Registered", field.getValue() + " Warrenty not registered. Any way you can proceed.", Ext.MessageBox.WARNING);
                        Ext.Msg.confirm('Warrenty Not Registered', field.getValue() + " Warrenty not registered. You can proceed further or </br> You want to register new warranty for this IMEI number?", function (btn) {
                            if (btn === 'yes') {
                                //open register Imei Window
                                form.findField("product").setValue(responseData.product);
                                form.findField("brand").setValue(responseData.brand);
                                form.findField("modleNo").setValue(responseData.modleNo);
                                me.onOpenRegisterNewImeiCreateWindow(null, responseData);
//                                Ext.getCmp('workOrderOpenForm-verifyimei').hide();
                            }
                        });
                        form.findField("isImeiVerified").setValue('3');
                        form.findField("product").setReadOnly(false);
                        form.findField("brand").setReadOnly(false);
                        form.findField("modleNo").setReadOnly(false);
                        form.findField("product").allowBlank = false;
                        form.findField("brand").allowBlank = false;
                        form.findField("modleNo").allowBlank = false;
                        form.findField("product").afterLabelTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';
                        form.findField("brand").afterLabelTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';
                        form.findField("modleNo").afterLabelTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';

                        break;
                    case 2:
                        singer.LayoutController.createErrorPopup("Warning", "IMEI Number not found. You can't proceed further.", Ext.MessageBox.WARNING);
                        form.findField("isImeiVerified").setValue('0');
                        break;
                    case 3:
                        singer.LayoutController.createErrorPopup("Warning", "IMEI Number is already registered. <br> You can't use registered IMEI for Stock Repair.", Ext.MessageBox.WARNING);
                        form.findField("isImeiVerified").setValue('0');
                        break;
                    case 1:
                        var responseData = response.data[0];
                        console.log(responseData);
                        form.findField("product").setValue(responseData.product);
                        form.findField("brand").setValue(responseData.brand);
                        form.findField("modleNo").setValue(responseData.modleDesc);
                        form.findField("DateOfSale").setValue(responseData.purchaseDate);
                        break;
                    case 5 :
                        var responseData = response.data[0];
                        console.log(responseData);
                        form.findField("product").setValue(responseData.product);
                        form.findField("brand").setValue(responseData.brand);
                        form.findField("modleNo").setValue(responseData.modleDesc);
                        form.findField("DateOfSale").setValue(responseData.purchaseDate);
                        break;
                    default:
                        singer.LayoutController.createErrorPopup("Warning", "Cannot open the work order for this IMEI.", Ext.MessageBox.WARNING);
                        break;
                }



            };

            var wrType = Ext.getCmp('cmbPayType').getValue();
            var s = Ext.getStore("WarrntyType");
            console.log(s);
            console.log("wrrr ", wrType);




            this.verifyWithServer('WarrantyScheme/getImeiMasterDetails?imeiNo=' + field.getValue() + '&warrentyType=' + wrType, "GET", "Verifying IMEI Number", callback);
        } else {
            field.focus();
        }
    },
    onQuickWorkVerify: function (btn) {
        var form = btn.up("form").getForm();
        var field = form.findField("imeiNo");
        if (!Ext.isEmpty(field.getValue())) {
            var me = this;
            console.log("Sdffdxfvgrf gghrjnmf");
            var callback = function (response) {
                //console.log(response)
                //                me.resetImieDependFields();
                switch (response.totalRecords) {
                    case 0:
                        singer.LayoutController.createErrorPopup("Warning", "No Data Found", Ext.MessageBox.WARNING);
                        form.findField("isImeiVerified").setValue('0');
                        break;
                    case 3:
                        //singer.LayoutController.createErrorPopup("Warrenty Not Registered", field.getValue() + " Warrenty not registered. Any way you can proceed.", Ext.MessageBox.WARNING);
                        singer.LayoutController.createErrorPopup('Warrenty Not Registered', field.getValue() + " Warrenty not registered...", Ext.MessageBox.WARNING);
                        //                        {
                        //                        if (btn === 'yes') {
                        //                            //open register Imei Window
                        ////                            me.onOpenRegisterNewImeiCreateWindow();
                        //                        }
                        //                    });
                        ////                    form.findField("isImeiVerified").setValue('3');
                        //                    form.findField("product").setReadOnly(false);
                        //                    form.findField("brand").setReadOnly(false);
                        //                    form.findField("modleNo").setReadOnly(false);
                        //                    form.findField("product").allowBlank = false;
                        //                    form.findField("brand").allowBlank = false;
                        //                    form.findField("modleNo").allowBlank = false;
                        //                    form.findField("product").afterLabelTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';
                        //                    form.findField("brand").afterLabelTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';
                        //                    form.findField("modleNo").afterLabelTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';
                        break;
                    case 2:
                        singer.LayoutController.createErrorPopup("Warning", "IMEI Number not found. You can't proceed further.", Ext.MessageBox.WARNING);
                        //                    form.findField("isImeiVerified").setValue('0');
                        break;
                    case 1:
                        var responseData = response.data[0];
                        //                    form.findField("isImeiVerified").setValue('1');
                        if (responseData.expireStatus == 0) {
                            singer.LayoutController.createErrorPopup("Warranty Expired", "<b>" + field.getValue() + "</b> - Warranty already expired. Expired Expired on : " + responseData.warrantyExpireDate + ". Any way you can proceed.", Ext.MessageBox.WARNING);
                        } else {

                        }
                        //                    form.findField("product").setValue(responseData.product);
                        //                    form.findField("brand").setValue(responseData.brand);
                        //                    form.findField("modleNo").setValue(responseData.modleNo);
                        break;
                    default:
                        singer.LayoutController.createErrorPopup("Warning", "Cannot open the work order for this IMEI", Ext.MessageBox.WARNING);
                        break;
                }
            };
            this.verifyWithServer('WarrantyScheme/getImeiMasterDetailswithWarranty?imeiNo=' + field.getValue(), "GET", "Verifying IMEI Number", callback);
        } else {
            field.focus();
        }
    },
    onNicFieldValidityChange: function (field, isValid) {
        var form = field.up("form").getForm();
        if (isValid) {
            field.nextSibling("button").setDisabled(false);
            form.findField("email2").setDisabled(false);

        } else {
            field.nextSibling("button").setDisabled(true);
            form.findField("email2").setDisabled(true);
        }
    },
    onNicValChange: function (field, newVal, oldVal) {
        var form = field.up("form").getForm();
        form.findField("customerName").reset();
        form.findField("customerAddress").reset();
        form.findField("workTelephoneNo").reset();
        form.findField("email").reset();
        form.findField("email2").reset();
        form.findField("email2").setVisible(true);
        form.findField("email2").allowBlank = false;
    },
    onNicVerifyBtnClick: function (btn) {
        var form = btn.up("form").getForm();
        var field = btn.previousSibling("nicfield");
        var callback = function (response) {
            var responseData = response.data[0];
            if (response.totalRecords > 0) {
                form.findField("customerName").setValue(responseData.customerName);
                form.findField("customerAddress").setValue(responseData.address);
                form.findField("workTelephoneNo").setValue(responseData.contactNo);
                form.findField("email").setValue(responseData.email);
                form.findField("email2").setValue(responseData.email);
                form.findField("email2").setDisabled(true);
                form.findField("email2").allowBlank = true;
            } else {
                singer.LayoutController.createErrorPopup("Warning", responseData.returnMessage, Ext.MessageBox.WARNING);
            }
        };
        this.verifyWithServer("Customers/checkCustomerDetal?customerNIC=" + field.getValue(), "GET", "Verifying NIC Number", callback);
    },
    verifyWithServer: function (submitUri, httpType, loadingMsg, callback) {
        var test = Ext.getCmp('workOrderOpenForm-create');
        var createBtn = Ext.getCmp('workOrderOpenForm-create');
        // createBtn.setDisabled(true);
        console.log("verifyWithServer", test);
        Ext.getBody().mask(loadingMsg, 'loading');
        var me = this;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + submitUri,
            method: httpType,
            headers: me.getController("CoreController").appendLDAPHeadersToRequest(),
            success: function (response) {
                Ext.getBody().unmask();
                var responseData = Ext.decode(response.responseText);
                callback(responseData);
                //createBtn.setDisabled(false);
            },
            failure: function () {
                createBtn.setDisabled(false);
                Ext.getBody().unmask();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onView: function (view, record) {
        //this.getController('CoreController').onOpenViewMoreWindow(record, 'workOrderOpenForm', record.get('workOrderNo') + " - Exchange Reference Details");
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    onOpenRegisterNewImeiCreateWindow: function (btn, gotData) {
        //console.log(gotData);
        this.getController('CoreController').openInsertWindow('registerNewImeiForm');
        var workOrderopenForm = this.getWorkOrderOpenFormForStockRepair().down("form").getForm();
        var registerImeiform = this.getRegisterNewImeiForm().down("form").getForm();
        this.getRegisterNewImeiForm().setTitle(workOrderopenForm.findField("imeiNo").getValue() + " - Register New IMEI Warranty");
        registerImeiform.findField("imeiNo").setValue(workOrderopenForm.findField("imeiNo").getValue());
        registerImeiform.findField("customerNIC").setValue(workOrderopenForm.findField("customerNic").getValue());
        registerImeiform.findField("contactNo").setValue(workOrderopenForm.findField("workTelephoneNo").getValue());
        registerImeiform.findField("customerName").setValue(workOrderopenForm.findField("customerName").getValue());
        registerImeiform.findField("email").setValue(workOrderopenForm.findField("email").getValue());
        registerImeiform.findField("customerAddress").setValue(workOrderopenForm.findField("customerAddress").getValue());
        registerImeiform.findField("modleNo").setValue(workOrderopenForm.findField("modleNo").getValue());
        registerImeiform.findField("warantyType").setValue(gotData.warrantyScheme);
        registerImeiform.findField("sellingPrice").setValue(gotData.salesPrice);
        var custEmail = Ext.getCmp("customerEmail").getValue(); // this.getWorkOrderOpenForm();
//        //console.log("BBBBBBBBBBBBBBB: ", custEmail);

        Ext.getCmp("cstMl").setValue(custEmail);

        registerImeiform.findField("bisId").setValue(this.getLoggedInUserDataStore().first().get("extraParams"));
    },
    onCreateNewImeiWarrantyBtnClick: function (btn) {
        var form = this.getRegisterNewImeiForm().down("form").getForm();
        var workOrderopenForm = this.getWorkOrderOpenForm().down("form").getForm();
        var warrantyRegModel = Ext.create('singer.model.WarrentyRegistration', form.getValues());
        var me = this;
        var callback = function (response) {
            if (response.returnFlag === '1') {
                singer.LayoutController.notify("Operation Success", "IMEI Warranty Registered");
                workOrderopenForm.findField("imeiNo").setValue(form.findField("imeiNo").getValue());
                workOrderopenForm.findField("customerNic").setValue(form.findField("customerNIC").getValue());
                workOrderopenForm.findField("workTelephoneNo").setValue(form.findField("contactNo").getValue());
                workOrderopenForm.findField("customerName").setValue(form.findField("customerName").getValue());
                workOrderopenForm.findField("proofOfPurches").setValue(form.findField("proofPurchas").getValue());
                workOrderopenForm.findField("dateOfSale").setValue(new Date(form.findField("registerDate").getValue()));
                workOrderopenForm.findField("email").setValue(form.findField("email").getValue());

                me.resetImieDependFields();

                var verifyImeiCallBakc = function (res) {
                    var responseData = res.data[0];
                    var field = workOrderopenForm.findField("imeiNo");
                    workOrderopenForm.findField("isImeiVerified").setValue('1');
                    if (responseData.expireStatus === 1) {
                        //console.log('warranty verify');
                        singer.LayoutController.createErrorPopup("Warranty Expired", "<b>" + field.getValue() + "</b> - Warranty already expired. Expired Expired on : " + responseData.warrantyExpireDate + ". Any way you can proceed.", Ext.MessageBox.WARNING);
                    }
                    workOrderopenForm.findField("product").setValue(responseData.product);
                    workOrderopenForm.findField("brand").setValue(responseData.brand);
                    workOrderopenForm.findField("modleNo").setValue(responseData.modleNo);
                    workOrderopenForm.findField("expireStatus").setValue(responseData.expireStatus);
                    //console.log(responseData.expireStatus);
                }
                me.verifyWithServer('WarrantyScheme/getImeiMasterDetailswithWarranty?imeiNo=' + form.findField("imeiNo").getValue(), "GET", "Verifying IMEI Number", verifyImeiCallBakc);
                me.getRegisterNewImeiForm().close();
            } else {
                singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
            }
        };
        this.submitDataToServer("WarrentyRegistrations/addWarrentyRegistration", "PUT", warrantyRegModel.getData(), "Creating new Warranty Registration", callback);
    },
    onOpenCreateWorkOrderWindow: function (btn) {
        console.log("open");
        this.getController('CoreController').openInsertWindow('WorkOrderOpenFormForStockRepair');
        this.getWorkOrderOpenFormForStockRepair().down("form").getForm().findField("isImeiVerified").setValue("0");
        var workOrderopenForm = this.getWorkOrderOpenFormForStockRepair().down("form").getForm();
        workOrderopenForm.findField("expireStatus").setReadOnly(true);
        workOrderopenForm.findField("createDate").hide().setDisabled(true);
        Ext.getCmp('cmbPayType').setDisabled(false);
    },
    onOpenCreateQuickWorkOrderWindow: function (btn) {
        Ext.getStore('woDrp').load();
        this.getController('CoreController').openInsertWindow('OpenStockQuickWorkOrderForm');
        this.getOpenStockQuickWorkOrderForm().setTitle("Create New Quick Work Order");
    },
    onQuickWorkOrderSearchByWO: function (btn) {

    },
    submitDataToServer: function (submitUri, httpType, dataObj, loadingMsg, callback) {
        var me = this;
        Ext.getBody().mask(loadingMsg, 'loading');
        var createBtn = Ext.getCmp('workOrderOpenForm-create');
//        createBtn.setDisabled(true);x
        //delete dataObj.PaymentType;
        console.log('>>>>>>>>><<<<<<<<<<<<<:: ', dataObj);
        var me = this;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + submitUri,
            method: httpType,
            headers: me.getController("CoreController").appendLDAPHeadersToRequest(),
            params: Ext.encode(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
//                createBtn.setDisabled(false);
                var responseData = Ext.decode(response.responseText);
                callback(responseData);
                me.getStockClearanceRepairStore().load();
            },
            failure: function () {
                Ext.getBody().unmask();
                createBtn.setDisabled(false);
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    QWOsubmitDataToServer: function (submitUri, httpType, dataObj, loadingMsg, callback) {
        Ext.getBody().mask(loadingMsg, 'loading');
        var createBtn = Ext.getCmp('OpenStockQuickWorkOrderForm-create');
        createBtn.setDisabled(true);
        console.log('vfdsbsgfg');
        // delete dataObj.PaymentType;
        var me = this;
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + submitUri,
            method: httpType,
            headers: me.getController("CoreController").appendLDAPHeadersToRequest(),
            params: Ext.encode(dataObj),
            success: function (response) {
                Ext.getBody().unmask();
                createBtn.setDisabled(false);
                var responseData = Ext.decode(response.responseText);
                callback(responseData);
            },
            failure: function () {
                Ext.getBody().unmask();
                createBtn.setDisabled(false);
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    onCloseRepairHistoryView: function (win) {
        var grid = win.down("grid");
        grid.getStore().getProxy().api.read = singer.AppParams.JBOSS_PATH + "WorkOrders/getWorkOrderList";
        grid.getStore().load();
    },
    onClearSearchRepairHistory: function (btn) {
        var imeiNo = btn.up("form").getForm().findField("imeiNo").getValue();
        var store = this.getWorkOrderHistoryStore();
        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + "WorkOrders/getWorkOrderList?imeiNo=" + imeiNo;

        var allColumns = btn.up("window").down("grid").columns;
        for (var i = 0; i < allColumns.length; i++) {
            if (!Ext.isEmpty(allColumns[i].searchableField)) {
                var field = allColumns[i].searchableField;
                if (field.getValue() !== "") {
                    field.setValue("");
                }
            }
        }
        store.load();
    },
    onClickRepairHistoryBtn: function (btn) {
        var form = btn.up("form").getForm();
        var store = this.getWorkOrderHistoryStore();
        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + "WorkOrders/getWorkOrderList?imeiNo=" + form.findField("imeiNo").getValue();
        store.load();
        var win = Ext.widget("repairHistoryView");
        win.down("form").getForm().findField("imeiNo").setValue(form.findField("imeiNo").getValue());
        win.setTitle(form.findField("imeiNo").getValue() + " - Repair History View");
        win.show();
    },
    onWorkOrdercloseWindow: function (btn) {
        this.getController("CoreController").accordionFormBeforeClose(btn.up("window"));
    },
//    onClickWorkOrderCreateBtn: function(btn) {
//        var form = btn.up("form").getForm();
//        form.findField("createDate").hide().setDisabled(true);
//        var loginData = this.getLoggedInUserDataStore();
//        var rec = Ext.create("singer.model.WorkOrderOpen", form.getValues());
//        rec.set('bisId', loginData.data.getAt(0).get('extraParams'))
//        var me = this;
//        var store = this.getWorkOrderOpeningStore();
//
//        var callback = function(response) {
//            if (response.returnFlag === '1') {
//                store.load();
//                singer.LayoutController.notify("Work Order Created", "New Work Order Created.");
//                Ext.Msg.confirm('Work order Created - ' + form.findField("imeiNo").getValue(), "Do you want to print IMEI : <b>" + form.findField("imeiNo").getValue() + "</b></br> Work Order No : <b>" + response.returnRefNo + "</b>", function(btn) {
//                    if (btn === 'yes') {
//                        //call print url
//                        me.getController('CoreController').viewPrintReadyReport("BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + response.returnRefNo, 'Report of ' + response.returnRefNo + " - Work Order");
//                    }
//                });
//                btn.up("window").close();
//            } else {
//                singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
//            }
//        };
//        this.submitDataToServer("WorkOrders/addWorkOrder", "PUT", rec.getData(), "Creating new Work Order", callback);
//    }

    onClickWorkOrderCreateBtn: function (btn) {
        var me = this;//dont remove
        var Btn = btn;//dont remove

        //  var createbtn = Ext.getCmp('workOrderOpenForm-create');

        var form = btn.up("form").getForm();

//        if ((Ext.getCmp('customerNic').getValue() === '') || (Ext.getCmp('Name').getValue() === '')
//                || (Ext.getCmp('Address').getValue() === '') || (Ext.getCmp('Telephone').getValue() === '')
//                || (Ext.getCmp('customerEmail').getValue() === '') || (Ext.getCmp('Re-typeEmail').getValue() === '')
//                || (Ext.getCmp('IMEI').getValue() === '') || (Ext.getCmp('AccessoriesRetained').getValue() === '')
//                || (Ext.getCmp('Defect').getValue() === '') || (Ext.getCmp('RCCReference').getValue() === '')
//                || (Ext.getCmp('Remarks').getValue() === '') || (Ext.getCmp('DistributorShop').getValue() === '')) {
//            singer.LayoutController.createErrorPopup("Error Occured", 'Fill the Required fields!', Ext.MessageBox.WARNING);
//        }
//        
//        else {
        //  createbtn.setDisabled(true);
        Ext.Msg.confirm('Confirm Work Order', "Do you want to create the Work Order?", function (btn) {
            if (btn === 'yes') {


                //   form.findField("createDate").hide().setDisabled(true);
                var loginData = me.getLoggedInUserDataStore();
                var rec = Ext.create("singer.model.WorkOrderOpen", form.getValues());
                var model = Ext.getCmp("modleNoForStockRepair").getValue();
                var modelNo = Ext.getStore('modStore').find("model_Description", model)


                rec.set('bisId', loginData.data.getAt(0).get('extraParams'));
                rec.set("modleNo", modelNo);
                rec.set("warrentyVrifType", 0);

                var store = me.getWorkOrderOpeningStore();

                var callback = function (response) {
                    if (response.returnFlag === '1') {
                        store.load();
                        singer.LayoutController.notify("Work Order Created", "New Work Order Created.");
                        Ext.Msg.confirm('Work order Created - ' + form.findField("imeiNo").getValue(), "Do you want to print IMEI : <b>" + form.findField("imeiNo").getValue() + "</b></br> Work Order No : <b>" + response.returnRefNo + "</b>", function (btn) {
                            if (btn === 'yes') {
                                //call print url
                                var URI = singer.AppParams.REPO + "BirtReportController?ReportFormat=html&ReportName=wo_OPENING.rptdesign&workOrderNo=" + response.returnRefNo;
                                console.log('URI', URI);

                                var ajaxreq;
                                function makeHttpObject() {
                                    try {
                                        return new XMLHttpRequest();
                                    } catch (error) {
                                    }
                                    try {
                                        return new ActiveXObject("Msxml2.XMLHTTP");
                                    } catch (error) {
                                    }
                                    try {
                                        return new ActiveXObject("Microsoft.XMLHTTP");
                                    } catch (error) {
                                    }

                                    throw new Error("Could not create HTTP request object.");
                                }
                                var request = makeHttpObject();
                                request.open("GET", URI, true);
                                request.send(null);
                                request.onreadystatechange = function () {
                                    if (request.readyState == 4) {
                                        ajaxreq = request.responseText;

                                        var myWindow = window.open('', '', 'width=500,height=500');
                                        myWindow.document.write('<html><head>');
                                        myWindow.document.write('<title>' + '' + '</title>');
                                        myWindow.document.write('<link rel="Stylesheet" type="text/css" href="http://dev.sencha.com/deploy/ext-4.0.1/resources/css/ext-all.css" />');
                                        myWindow.document.write('<script type="text/javascript" src="http://dev.sencha.com/deploy/ext-4.0.1/bootstrap.js"></script>');
                                        myWindow.document.write('</head><body>');
                                        myWindow.document.write('<div>' + ajaxreq + '</div>');
                                        myWindow.document.write('</body></html>');

                                        setInterval(function () {
                                            myWindow.print();
                                        }, 3000);

//                                console.log('ajaxreqhhhh', ajaxreq);
//                                console.log('response.returnRefNo', response.returnRefNo);
                                    }
                                };
                            }
                        });
                        Btn.up("window").close();
                    } else {
                        singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
                    }
                };
                console.log('rec.getData()', rec.getData());
                me.submitDataToServer("WorkOrders/addStockClearanceWorkOrder", "PUT", rec.getData(), "Creating new Work Order", callback);
            } else {
                singer.LayoutController.createErrorPopup("Error Occured", response.returnMsg, Ext.MessageBox.WARNING);
            }
        });
//            }
//        }

    }
});