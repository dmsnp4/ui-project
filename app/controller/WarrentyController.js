/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.controller.WarrentyController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.warrentycontroller',
    controllers: ['CoreController'],
    oldValue: null,
    stores: [
        'Warrenty', 'LoggedInUserData'
    ],
    views: [
        'Administrator.Warrenty.WarrentyAdjGrid',
        'Administrator.Warrenty.ViewWarrentyAdj',
        'Administrator.Warrenty.EditWrrentyAdj'
    ],
    refs: [
        {
            ref: 'WarrentyAdjGrid',
            selector: 'warrentyadjgrid'
        },
        {
            ref: 'ViewWarrentyAdj',
            selector: 'viewwarrentyadj'
        },
        {
            ref: 'EditWrrentyAdj',
            selector: 'editwarrentyadj'
        }
    ],
    init: function () {
        //        //////console.log(this);
        var me = this;
        me.control({
            'warrentyadjgrid': {
                added: this.initializeGrid
            },
            'warrentyadjgrid ': {
                viewWarrentyAdj: this.onViewWarrentyAdj
            },
            'warrentyadjgrid   ': {
                onDelete: this.onDelete
            },
            'warrentyadjgrid  ': {
                editWarrentyAdj: this.onEditWarrentyAdj
            },
            'warrentyadjgrid button[action=clearSearch]': {
                click: this.onClearSearch
            },
            'viewwarrentyadj button[action=cancel]': {
                click: this.onCancel
            },
            'editwarrentyadj button[action=save]': {
                click: this.onSave
            },
            'editwarrentyadj button[action=create]': {
                click: this.onCreate
            },
            'editwarrentyadj button[action=cancel]': {
                click: this.onCancel
            },
            'warrentyadjgrid button[action=open_window]': {
                click: this.openAddNew
            }
        });
    },
    initializeGrid: function (grid) {
        var store = this.getWarrentyStore();
        this.getController('CoreController').loadBindedStore(store);
    },
    onDelete: function (grid, record) {
        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.MessageBox.confirm('Are you sure?', 'Are you sure you want to Delete?', function (btn) {
            if (btn === 'yes') {
                Ext.Msg.wait('Please wait', 'Resetting Users Password...', {
                    interval: 300
                });
                Ext.Ajax.request({
                    url: singer.AppParams.JBOSS_PATH + 'WarrantyAdjustment/deleteWarrantyAdjustment',
                    method: 'POST',
                    headers: {
                        userId: loginData.data.getAt(0).get('userId'),
                        room: loginData.data.getAt(0).get('room'),
                        department: loginData.data.getAt(0).get('department'),
                        branch: loginData.data.getAt(0).get('branch'),
                        countryCode: loginData.data.getAt(0).get('countryCode'),
                        division: loginData.data.getAt(0).get('division'),
                        organization: loginData.data.getAt(0).get('organization'),
                        system: sys,
                        'Content-Type': 'application/json'
                    },
                    params: JSON.stringify(record.data),
                    success: function (response) {
                        Ext.Msg.hide();
                        var responseData = Ext.decode(response.responseText);
                        grid.getStore().load();
                    },
                    failure: function () {
                        Ext.Msg.hide();
                        Ext.Msg.alert("Error in resetting password", "Please try again later");
                    }
                });
            }
        });
    },
    onViewWarrentyAdj: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'viewwarrentyadj', 'View');
    },
    onEditWarrentyAdj: function (view, record) {
        //        //console.log();
        //        var field = view.up('form').getForm().findField('extendedDays');
        this.oldValue = record.get('extendedDays');

        this.getController('CoreController').openUpdateWindow(record, 'editwarrentyadj', 'Edit');
        var form = this.getEditWrrentyAdj().down('form').getForm();
        Ext.getCmp('fieldtwo').focus('', 1);
        form.findField('iMEI').setDisabled(true);
        Ext.getCmp('editwarrentyadj-viewExtendDays').setValue(form.findField('extendedDays').getValue());
        form.findField('extendedDays').setValue('');
    },
    onCancel: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    onSave: function (btn) {
        //        //console.log(btn.up(form));
        var me = this;
        var field = btn.up('form').getForm().findField('extendedDays');
        var newValue = parseInt(field.getValue());
        if (newValue !== 0) {
            var oldVal = parseInt(this.oldValue);
            newValue = newValue + oldVal;
            Ext.Msg.confirm("Extended Days", "Total Extended Days.</br><center>" + newValue + "</center>", function (btnn) {
                if (btnn === 'yes') {
                    field.setValue(newValue);
                    me.getController('CoreController').onRecordUpdate(btn, me.getWarrentyStore(), 'Category Data Updated.', 'Updating Category', true, true, me.getWarrentyAdjGrid());
                    
                }
            });
        }





        //////console.log(this.getWarrentyStore());
    },
    onCreate: function (btn) {
        var store = this.getWarrentyStore();
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Warrenty Adjestment Created.', 'Creating New Warrenty Adjestment', true, true, this.getWarrentyAdjGrid(), false);
        store.load();
        this.getWarrentyAdjGrid().bindStore(store);
    },
    openAddNew: function () {
        this.getController('CoreController').openInsertWindow('editwarrentyadj');
        Ext.getCmp('editwarrentyadj-viewExtendDays').hide();
    },
    onClearSearch: function (btn) {
        this.getController('CoreController').onClearSearch(btn);
    },
    expandChangedNode: function (response, orgRecord, executionType) {
        var me = this;
        var treeCatStore = this.getCatGrid().getStore();
        var crntNode = treeCatStore.getNodeById(orgRecord.get('asset_cat_id'));
        if (Ext.isDefined(crntNode)) {
            var parentTreeNode = crntNode;
            var depthOfNode = crntNode.getDepth();
            if (executionType === 1) {
                depthOfNode = crntNode.getDepth() + 1;
            }
            for (var i = 0; i < depthOfNode; i++) {
                parentTreeNode = parentTreeNode.parentNode;
                if (parentTreeNode !== null) {
                    parentTreeNode.expand();
                }
            }
            if (parseInt(response) === 1) {
                if (executionType === 2) {
                    me.getCatGrid().getView().addRowCls(crntNode, 'before-sync-row-style');
                    var task = new Ext.util.DelayedTask(function () {
                        me.getCatGrid().getView().removeRowCls(crntNode, 'before-sync-row-style');
                    });
                    task.delay(2000);
                }
            }
        }
    },
});