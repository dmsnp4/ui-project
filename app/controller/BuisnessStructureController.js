Ext.define('singer.controller.BuisnessStructureController', {
    extend: 'Ext.app.Controller',
    localVar: null,
    bsId: null,
    parentID: null,
    sub: null,
    beforeStoreData: null,
    dataUn: [],
    temp: null,
    selectedNode: null,
    views: [
        'Administrator.BuisnessStructure.BuisnessStructureGrid',
        'Administrator.BuisnessStructure.AddSubDepartmentForm',
        'Administrator.BuisnessStructure.AddNewDepartmentForm',
        'Administrator.BuisnessStructure.UserAssignForm',
        'Administrator.BuisnessStructure.UserAssignGridFrom'
    ],
    stores: [
        'BuisnessStore',
        'BuisnessTree',
        'BuisnessCategories',
        'UserAssignStore',
        /***************/
        'Users',
        'UserToBussiness',
        'LoggedInUserData',
        'BusinessStructreUsers'
    ],
    models: [
//        'BusinessMTest'
    ],
    refs: [{
            ref: 'BuisGrid',
            selector: 'buisnessStructure'
        }, {
            ref: 'addDept',
            selector: 'addDepartmentForm'
        }, {
            ref: 'addSubDept',
            selector: 'addSubDepartmentForm'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        }, {
            ref: 'AssignUser',
            selector: 'userassign'
        }, {
            ref: 'AssiguserGrid',
            selector: 'userAssignGridForm'
        }, {
            ref: 'UserAssignForm',
            selector: 'userassign'
        }],
    init: function () {
        //        //////console.log(this.getBuisnessTreeStore());
        //////console.log("Business controller init");
        var me = this;
        me.control({
            'buisnessStructure': {
                added: this.initializeGrid
            },
            'buisnessStructure treeview': {
                drop: this.onDrop
            },
            'buisnessStructure treeview ': {
                beforedrop: this.onBeforeDrop
            },
            'buisnessStructure button[action=onExpandAll]': {
                click: this.onExpandAll
            },
            'buisnessStructure button[action=onCollapseAll]': {
                click: this.onCollapseAll
            },
            'buisnessStructure button[action=open_user_assign_window]': {
                click: this.openUserAssign
            },
            'userassign button[action=open_user_assign_grid_window]': {
                click: this.openUserAssignGrid
            },
            'userassign button[action=done]': {
                click: this.onDone
            },
            'addDepartmentForm button[action=cancel]': {
                click: this.insertFormCancel
            },
            'addSubDepartmentForm button[action=cancel]': {
                click: this.insertFormCancel
            },
            'addSubDepartmentForm button[action=Sub_Department_submit]': {
                click: this.insertSubDepartmentRecord
            },
            'addDepartmentForm button[action=submit_department]': {
                click: this.updataRecord
            },
            ' addDepartmentForm button[action=create]': {
                click: this.insertDepartmentRecord //insertFormSubmit   
            },
            '  buisnessStructure button[action=open_Edit_window]': {
                click: this.loadInToEditWindow
            }, '  buisnessStructure button[action=test]': {
                click: this.test
            },
            '  buisnessStructure button[action=open_add_department_form]': {
                click: this.loadInToAddDepartmentFormWindow
            },
            '  buisnessStructure button[action=open_sub_department_form]': {
                click: this.loadInToSubDepartmentFormWindow
            },
            //////////////////////
            ' buisnessStructure': {
                itemclick: this.treeItemClick
            },
            ///////////////////////
            'userAssignGridForm ': {
                show: this.initAssingnGrid
            },
            'userAssignGridForm button[action=nextOne]': {
                click: this.assignOneToUser
            },
            'userAssignGridForm button[action=nextAll]': {
                click: this.assignAllToUser
            },
            'userAssignGridForm button[action=prevOne]': {
                click: this.removeOneFromUser
            },
            'userAssignGridForm button[action=prevAll]': {
                click: this.removeAllFromUser
            },
            'userAssignGridForm button[action=cancel]': {
                click: this.insertFormCancel
            },
            'userAssignGridForm button[action=create]': {
                click: this.onCreate
            },
            ////////////////
        });
    },
    initAssingnGrid: function () {
        //        var assignGrid = Ext.getCmp('assignedUsers');
        //        //console.log(assignGrid.getStore());
        //        this.tempStore = assignGrid.getStore();
    },
    onExpandAll: function () {
        var tree = Ext.getCmp('BusinessStructure');
        Ext.Msg.wait('Please wait', 'Expanding...', {
            interval: 300
        });
        tree.expandAll(function () {
            Ext.Msg.hide();
        });
    },
    onCollapseAll: function () {
        var tree = Ext.getCmp('BusinessStructure');
//        //console.log(tree);
        tree.collapseAll();
    },
    assignOneToUser: function () {
        var allGrid = Ext.getCmp('availableUsers');
        var assignGrid = Ext.getCmp('assignedUsers');
        var selectionModel = allGrid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                allGrid.getStore().remove(record);
                assignGrid.getStore().insert(0, record);
            });
        }

    },
    assignAllToUser: function () {
        var allGrid = Ext.getCmp('availableUsers');
        var assignGrid = Ext.getCmp('assignedUsers');
        var allUserStore = allGrid.getStore();
        var userGroupStore = assignGrid.getStore();

        allUserStore.data.each(function (item) {
            allUserStore.remove(item);
            userGroupStore.add(item);
        });

    },
    removeOneFromUser: function () {
        var me = this;
        var allGrid = Ext.getCmp('availableUsers');
        var assignGrid = Ext.getCmp('assignedUsers');
        var temp;
        var selectionModel = assignGrid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                allGrid.getStore().add(record);
                assignGrid.getStore().remove(record);
                //                record.data.extraParams = me.localVar;
                //                singer.ParamController.UN_USR.push(record.data);

                temp = record.data;
            });
        }
        //        this.dataUn.push(temp);
        //        //console.log(this.dataUn);
    },
    removeAllFromUser: function () {
        var me = this;
        var allGrid = Ext.getCmp('availableUsers');
        var assignGrid = Ext.getCmp('assignedUsers');
        var allUserStore = allGrid.getStore();
        var userGroupStore = assignGrid.getStore();
        var temp = [];
        userGroupStore.data.each(function (item) {
            allUserStore.add(item);
            userGroupStore.remove(item);
            //            item.data.extraParams = this.localVar;
            temp.push(item.data);
        });
        //        this.dataUn = temp;
    },
    treeItemClick: function (view, record) {
        //        //console.log('ccccccccccc');
        //                Ext.record.get('id','text','parentId','leaf');
        //                Ext.record.childNodes.length;


        // have all the information about the node
        //Node id
        //Node Text
        //Parent Node
        //Is the node a leaf?
        //No of child nodes

    },
    loadInToSubDepartmentFormWindow: function () {
        var me = this;
        this.sub = true;

        var allGrid = this.getBuisGrid();
        var selectionModel = allGrid.getSelectionModel();

        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                me.bsId = record.get('bisId');
                //                //console.log(record);
                if (record.getData().status === 1) {
                    me.getController('CoreController').openInsertWindow('addDepartmentForm');
                    Ext.getCmp('businesForm').setTitle("Add Sub-Department");
                } else {
                    Ext.Msg.alert("Error...", "Department is Inactive...");
                }
            });
        } else {
            Ext.Msg.alert("Error...", "Please select a department");
        }

    },
    loadInToAddDepartmentFormWindow: function () {
        var me = this;
        this.sub = false;

        var allGrid = this.getBuisGrid();
        var selectionModel = allGrid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            me.selectedNode = selectionModel.getSelection();
            selectionModel.getSelection().forEach(function (record, index, array) {
//                //console.log(record);

                if (record.getData().status === 1) {
                    me.parentID = record.get('bisPrtnId');
                    me.getController('CoreController').openInsertWindow('addDepartmentForm');
                    Ext.getCmp('businesForm').setTitle("Add Department");
                } else {
                    Ext.Msg.alert("Error...", "Department is Inactive...");
                }

                //                me.bsId = record.get('bisId');

                //                //console.log(parentID);
                //                me.getController('CoreController').openInsertWindow('addDepartmentForm');
                //                Ext.getCmp('businesForm').setTitle("Add Department");
            });
        } else {
            Ext.Msg.alert("Error...", "Please select a department");
        }

    },
    insertDepartmentRecord: function (btn) {
        //        //console.log(btn.up('form').getForm());
        var me = this;
        var form = btn.up('form').getForm();
        var store = this.getBuisnessStoreStore();

        if (me.sub === true) {
            form.findField('bisPrtnId').setValue(me.bsId);
//            //console.log(me.bsId);
            me.insertFormSubmit(btn);

            //*****************************************
            //            me.getController('CoreController').onNewRecordCreate(btn, store, 'New Department Created.', 'Creating New Department', true, true, this.getBuisGrid(), false, true);
        } else if (me.sub === false) {
            //            form.findField('bisId').setValue(me.bsId);
            form.findField('bisPrtnId').setValue(me.parentID);
//            //console.log(me.bsId);
            me.insertFormSubmit(btn);

            //********************************
            //            me.getController('CoreController').onNewRecordCreate(btn, store, 'New Department Created.', 'Creating New Department', true, true, this.getBuisGrid(), false, true);
        }
        //        form.findField('bisPrtnId').setValue(this.bsId);
        //        //console.log();
        //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Department Created.', 'Creating New Department', true, true, this.getBuisGrid(), false);
        //        var grid = this.getBuisGrid();
        //        var bStore = this.getBuisnessStoreStore();
        //        bStore.load();
        //////console.log(bStore);
    },
    insertSubDepartmentRecord: function (btn) {
        var store = this.getBuisnessStoreStore();
        var addSubBtn = Ext.getCmp('supplierform-save');
        addSubBtn.setDisabled(true);
        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Sub Department Created.', 'Creating New Department', true, true, this.getBuisGrid(), false);
        addSubBtn.setDisabled(false);
        var grid = this.getBuisGrid();
        var bStore = this.getBuisnessStoreStore();
        bStore.load();
        //////console.log(bStore);
    },
    loadInToEditWindow: function () {
        var me = this;
        //////console.log("coming to assignOneToUser function");
        var allGrid = this.getBuisGrid();
        var selectionModel = allGrid.getSelectionModel();

        if (selectionModel.hasSelection()) {
            selectionModel.getSelection().forEach(function (record, index, array) {
                console.log("record",record);
                //                allGrid.getStore().remove(record);
                //                assignGrid.getStore().add(record);
                me.getController('CoreController').openUpdateWindow(record, 'addDepartmentForm', record.get('bisName') + "'s details");
                //                me.getController('CoreController').loadBindedStore(getUserToBussinessStore);
            });
        }

    },
    checkIsEnterKey: function (field, e) {
        if (e.getKey() === e.ENTER && (field.getXType() !== 'combobox')) {
            if (!Ext.getCmp('categorymaintenanceform-create').isHidden()) {
                this.insertFormSubmit(field);
            } else if (Ext.getCmp('categorymaintenanceform-create').isHidden()) {
                this.updateFormSubmit(field);
            }
        }
    },
    initializeGrid: function (grid) {
        var me = this;
        //////console.log("init business Structure grid");
        var store = this.getBuisnessStoreStore();
        this.getController('CoreController').loadBindedStore(store);
        // var catStore = this.getAssetCategoriesStore();
        // this.getController('CoreController').loadBindedStore(catStore);
        //        this.registerNewVTypes();
    },
    loadDropdowns: function () {
        var dsr = Ext.getStore('dsrStore');
        var dis = Ext.getStore('disStore');
        var sub = Ext.getStore('subStore');
        if (dsr.getCount() === 0) {
            dsr.load();
        } else if (dis.getCount() === 0) {
            dis.load();
        } else if (sub.getCount() === 0) {
            sub.load();
        }
    },
    openAddNew: function () {
        this.getController('CoreController').openInsertWindow('addDepartmentForm');
        //        this.getController('Main').createBuisnessStructureStore();
        this.loadDropdowns();
        //this.getController('CoreController').openUpdateWindow(record, 'viewmodel', record.get('') + "'s details");
        // var userGroupStore = this.getUserGroupsStore();
        // this.getController('CoreController').loadBindedStore(userGroupStore);
    },
    openAddSub: function () {
        this.getController('CoreController').openInsertWindow('addSubDepartmentForm');
        this.loadDropdowns();
    },
    openUserAssign: function () {
        var me = this;
        var store = this.getUserAssignStoreStore();
        var win = Ext.widget('userassign');

        //        win.show();


        //        this.getController('Main').setHeaders(store);
        //        this.getController('CoreController').loadBindedStore(store);

        var bisGrid = this.getBuisGrid();
        var selectionModel = bisGrid.getSelectionModel();
        var bsid;
        if (selectionModel.hasSelection()) {
            win.show();
            //            me.loadDropdowns();
            selectionModel.getSelection().forEach(function (record, index, array) {
                //                me.getController('CoreController').openUpdateWindow(record, 'addSubDepartmentForm', record.get('bisName') + "'s details");
                bsid = record.get('bisId');
                me.localVar = bsid;
                me.temp = record;
//                //console.log(bsid);
            });
        } else {
            Ext.Msg.alert("Error...", "Please select a department");
        }
        //        //console.log(bsid);


        store.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'UserBussinessStructures/getUserBusinessStructures?bisId=' + bsid;
        store.load();
        //        this.getController('CoreController').loadBindedStore(store);

        ////console.log('coming to user assign');
    },
    openUserAssignGrid: function () {
        // this.getController('CoreController').openInsertWindow('userAssignGridForm');
        var win = Ext.widget('userAssignGridForm');
        //        //console.log('user assign grid');
        win.show();
        var me = this;
        var assign = Ext.create('Ext.data.Store', {
            model: 'singer.model.BuisnessAssignedUsersM',
        });
        var allUsers = Ext.create('Ext.data.Store', {
            model: 'singer.model.BuisnessAssignedUsersM',
            id: '',
            pageSize: singer.AppParams.INFINITE_PAGE,
            proxy: {
                type: 'ajax',
                //                url: singer.AppParams.JBOSS_PATH + 'RepairCategories/getRapairCategories?reCateStatus=' + id,
                url: singer.AppParams.JBOSS_PATH + 'UserBussinessStructures/getUserBusinessStructures',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: false
        });
        allUsers.load();
        //        //console.log("comming to user assign controller");
        //        this.getController('CoreController').openInsertWindow('userAssignGridForm');
        //        var userAssignGrid = this.getUserAssignStoreStore();
        //        this.getController('Main').setHeaders(userAssignGrid);

        // Ext.getCmp('allUserGroups').bindStore(userAssignGrid.load());


        //        //console.log(Ext.getCmp('availableUsers'));
        var userStore = this.getBusinessStructreUsersStore();
        userStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'Users/getAllUsers?extraParams=0' + '';
        this.getController('Main').setHeaders(userStore);
        //        Ext.getCmp('availableUsers').bindStore(userStore.load());
        var assignStore = this.getUserToBussinessStore();
        assignStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + 'UserBussinessStructures/getUserBusinessStructures?bisId=' + this.localVar + '';
        userStore.load({
            callback: function (records, operation, success) {
//                //console.log('records:', records);
                records.forEach(function (items) {
//                     //console.log(items.getData().extraParams);
                    if (items.getData().extraParams !== 0) {
//                        //console.log(items)
                        userStore.remove(items);
                    }
                });
                //                 //console.log('store', userStore);
                //                userStore.data.each(function (item) {
                //                    //                     //console.log(item);
                //                    
                //
                //                });
                var ind = null;
                while (ind !== -1) {
                    ind = userStore.findExact('status', "9");
                    userStore.removeAt(ind);
                }
                Ext.getCmp('availableUsers').bindStore(userStore);
            }
        });
        //        //console.log(userStore);
        var temp = [];
        Ext.getCmp('assignedUsers').bindStore(assignStore.load({
            callback: function () {
                //                me.beforeStoreData = assignStore.data;

                assignStore.data.each(function (item) {
                    //                    //console.log(item);
                    temp.push(item.data);
                });
                me.beforeStoreData = temp;
//                //console.log(me.beforeStoreData);
            }
        }));
        //        var form = win.down('form');
        // this.focusToFirstFieldinForm(form);
        // form.items.items[0].focus(false, 200);

    },
    insertFormCancel: function (btn) {
        //        var userStore = this.getUsersStore();
        //                userStore.getProxy().api.read = singer.AppParams.JBOSS_PATH + '/getUsers?extraParams=0' + '';
        //        Ext.getCmp('availableUsers').bindStore(userStore.load());
        //        //console.log('ssssssssss');
        //        //console.log(Ext.getCmp('availableUsers'));
        this.getController('CoreController').onInsertFormCancel(btn);

    },
    insertFormReset: function (btn) {
        this.getController('CoreController').onInsertFormReset(btn);
    },
    //cmp, store, successMsg, processMsg, isWindow, hasGrid, grid,isNormalLoad
//    insertFormSubmit: function (btn) {
//        //this.getController('CoreController').onNewRecordCreate(btn, this.getAssetCategoriesStore(), '', '', true, true, this.getCatGrid(), true);
//        var form = btn.up('form').getForm();
//        var store = this.getAssetCategoriesStore();
//        var me = this;
//
//
//
//        if (form.isValid()) {
//
//            if (me.sub === true) {
//                form.findField('bisPrtnId').setValue(me.bsId);
//                store.insert(0, form.getValues());
//                me.syncCategoryStore(store, btn.up('form'), 'Loading Categories', 'New Category Creating', 'New Category Created', 'Category Created');
//                //            //console.log(me.bsId);
//                //                me.getController('CoreController').onNewRecordCreate(btn, store, 'New Department Created.', 'Creating New Department', true, true, this.getBuisGrid(), false, true);
//            } else if (me.sub === false) {
//                //            form.findField('bisId').setValue(me.bsId);
//                form.findField('bisPrtnId').setValue(me.parentID);
//                store.insert(0, form.getValues());
//                me.syncCategoryStore(store, btn.up('form'), 'Loading Categories', 'New Category Creating', 'New Category Created', 'Category Created');
//                //            //console.log(me.bsId);
//                //                me.getController('CoreController').onNewRecordCreate(btn, store, 'New Department Created.', 'Creating New Department', true, true, this.getBuisGrid(), false, true);
//            }
//
//
//
//            //            store.insert(0, form.getValues());
//            //            me.syncCategoryStore(store, btn.up('form'), 'Loading Categories', 'New Category Creating', 'New Category Created', 'Category Created');
//        } else {
//            this.getController('CoreController').focusToFirstDirtyFieldinForm(btn.up('form'));
//        }
//    },
    updateFormCancel: function (btn) {
        this.getController('CoreController').onUpdateFormCancel(btn);
    },
    updateFormReset: function (btn) {
        var formPanel = btn.up('form');
        var form = formPanel.getForm();
        form.findField('current_cat_code').setValue(form.getRecord().get('asset_cat_code'));
        var me = this;
        var changedField = this.getController('CoreController').checkRecordDataisUpdated(formPanel);
        if (changedField !== false) {
            Ext.Msg.confirm("Reset Form?", "Record is modified.</br> You want to reset modified data before save it?", function (btnn) {
                if (btnn === "yes") {
                    form.loadRecord(form.getRecord());
                    me.getController('CoreController').focusToFirstFieldinForm(btn.up('form'));
                    me.setSuperCategoryCombo(form);
                } else {
                    changedField.focus(false, 300);
                }
            });
        } else {
            form.loadRecord(form.getRecord());
            me.getController('CoreController').focusToFirstFieldinForm(btn.up('form'));
        }
        me.setSuperCategoryCombo(form);
    },
    updataRecord: function (btn) {
        var form = btn.up('form').getForm();
        var record = form.getValues();
        ////console.log(record);
        // //console.log(record);
        //        var store = this.getBuisnessStoreStore();
        //        //console.log(store);
        ////console.log(btn.up('form').getForm());
        //        this.getController('CoreController').onRecordUpdate(btn, store, 'New DEpartmnet aded',"", true, true, this.getBuisGrid());
        //        this.getController('CoreController').onRecordUpdate(btn, store, 'Department Updated', 'Updating Department', true, true, this.getBuisGrid());
        //        this.getController('CoreController').onRecordUpdate(btn, store, 'Category Data Updated.', 'Updating Category', true, true, this.getBuisGrid());

        // record.set(form.getValues());
        this.updateBusinessStore(this.getBuisnessStoreStore(), record, btn.up('form'), 'Loading Categories', 'Updating Category Details', 'Category Details Updated', 'Category Updated.');

    },
    updateBusinessStore: function (store, updatingRecord, form, loadingMsg, ProcessMsg, SuccessMsg, title) {
        var me = this;
        var link = store.getProxy().api.update;
        ////console.log(link);
        //         //console.log();
        var val = form.getValues();
        var rec = form.getRecord();
        var mod = Ext.create('singer.model.BuisnessM',
                updatingRecord
                );
        // store.set("bisPrtnId", dropData.bisId);
        ////console.log(mod.data);

        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        if (form.isValid()) {
            Ext.Msg.wait('Please wait', 'loadingMsg', {
                interval: 300
            });
            Ext.Ajax.request({
                url: link,
                method: 'POST',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(mod.data),
                success: function (response) {
                    Ext.Msg.hide();
                    var responseData = Ext.decode(response.responseText);
                    ////console.log(responseData);
                    if (responseData.returnFlag === '1') {
                        form.up('window').destroy();
                        //                         Ext.getStore('BuisnessStore').load();
                        rec.set(val);
                    } else {
                        Ext.Msg.alert("Error...", responseData.returnMsg, function (cmp) {
                            form.up('window').destroy();
                        });

                    }
                    //                    
                    // form.destroy();
                    // parentStore.load();
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });
        }


    },
    //**
    updateFormSubmit: function (btn) {
        var store = this.getBuisnessStoreStore();
        //        this.getController('CoreController').onNewRecordCreate(btn, store, 'New Category Created.', 'Creating New Category', true, true, this.getBuisGrid(), false);
        var grid = this.getBuisGrid();
        //        store.load();
        //        grid.bindStore(store);
        //        var form = btn.up('form').getForm();
        //        var record = form.getRecord();
        //        if (form.isValid() && this.getController('CoreController').checkRecordDataisUpdated(btn.up('form')) !== false) {
        //            record.set(form.getValues());
        //            this.updateCategoryStore(this.getAssetCategoriesStore(), record, btn.up('form'), 'Loading Categories', 'Updating Category Details', 'Category Details Updated', 'Category Updated.');
        //        }
    },
    moreViewOpen: function (view, record) {
        this.getController('CoreController').onOpenViewMoreWindow(record, 'categorymaintenanceview', 'Full Details of ' + record.get('asset_cat_desc'));
    },
    moreViewClose: function (btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    onRowEdit: function (editorP, obj) {
        this.getController('CoreController').onRowUpdate(obj, 'Category Details Updated.');
    },
    onRowDelete: function (grid, record) {
        var me = this;
        Ext.Msg.confirm("Remove Category From System?", 'Are you sure you want to Remove </br><b>' + record.get('asset_cat_desc') + '</b> from System?', function (btn) {
            if (btn === "yes") {
                me.getAssetCategoriesStore().remove(record);
                me.syncCategoryStore(me.getAssetCategoriesStore(), null, 'Loading Categories', 'Removing Category', 'Category Removed Successfully.', 'Category Removed.', true);
            }
        });
        //this.getController('CoreController').onRowDelete(grid, rowIndex, 'Category Removed.');
    },
    onRefreshGrid: function (btn) {
        this.getController('CoreController').onResetReadProxy(this.getAssetCategoriesStore(), 'getcategory?asset_cat_id=all&asset_desc=all&asset_category_type=all&asset_parent_Category_Id=all', false);
    },
    checkChangesBeforeClose: function (window) {
        var isClosed = false;
        isClosed = this.getController('CoreController').checkChangesBeforeClose(window, 'categorymaintenanceform');
        return isClosed;
    },
    onCreate: function (btn) {
        var me = this;
        //        //console.log(this.beforeStore);
        //        var store = this.();
        var assignGrid = Ext.getCmp('assignedUsers');
        var store = assignGrid.getStore();
        var form = this.getUserAssignForm();
//        //console.log(form);
        var parentStore = this.getBuisnessStoreStore();
        ////console.log(store.getCount());

        if (store.getCount() <= 0) {
            Ext.Msg.alert("Error", "Department Have no users");
            //            Ext.getCmp('exportDirtyBtn').setDisabled(true);
        } else {
            var dat = me.beforeStoreData;
            //            var Un = [];
            var dataAs = [];
            store.data.each(function (item) {
                item.data.extraParams = me.localVar;
                var temp = me.findById(dat, item.data.userId)
//                //console.log(temp);
                if (temp !== -1) {
                    me.beforeStoreData.splice(temp, 1);
                }
                dataAs.push(item.data);
            });
//            //console.log(me.beforeStoreData);
            var x = {
                unassignedUsers: me.beforeStoreData, //singer.ParamController.UN_USR,
                assignedUsers: dataAs
            };
            //            //console.log(data);
            var loginData = this.getLoggedInUserDataStore();
            var sys = singer.AppParams.SYSTEM_CODE;
            Ext.Msg.wait('Please wait', 'Adding Users...', {
                interval: 300
            });
            Ext.Ajax.request({
                url: singer.AppParams.JBOSS_PATH + 'UserBussinessStructures/editBusinessStructureUser_LDAP_DB',
                method: 'PUT',
                headers: {
                    userId: loginData.data.getAt(0).get('userId'),
                    room: loginData.data.getAt(0).get('room'),
                    department: loginData.data.getAt(0).get('department'),
                    branch: loginData.data.getAt(0).get('branch'),
                    countryCode: loginData.data.getAt(0).get('countryCode'),
                    division: loginData.data.getAt(0).get('division'),
                    organization: loginData.data.getAt(0).get('organization'),
                    system: sys,
                    'Content-Type': 'application/json'
                },
                params: JSON.stringify(x),
                success: function (response) {
                    Ext.Msg.hide();
                    var responseData = Ext.decode(response.responseText);
                    var error = responseData.returnMsg;
                    ////console.log(responseData);
                    if (responseData.returnFlag === '1') {
                        btn.up().up('form').up('window').destroy();
                        form.down('grid').getStore().load();
                        //                        var te = me.temp;
                        //                        te.bisUserCount = form.down('grid').getStore().getCount();
                        //                        //console.log(te);
                        //                        me.temp.set(te);
                        //                        me.temp = form.down('grid').getStore().getCount();

                    } else {
                        singer.LayoutController.createErrorPopup("Record Insertion Fail.", error, Ext.MessageBox.WARNING);
                    }
                    //                    
                    //                    form.destroy();
                    //                    parentStore.load();
                    me.dataUn = [];
                },
                failure: function () {
                    Ext.Msg.hide();
                    Ext.Msg.alert("Error in connection", "Please try again later");
                }
            });



        }

        //       
    },
    onBeforeDrop: function (node, data, overModel, dropPosition, dropHandlers) {
        //        //console.log(data)
        //        dropHandlers.wait = true;





        var dragingData = data.records[0].data;
        var dropData = overModel.data;
        //        //console.log()
        if (dropData.status === 2) {
            dropHandlers.cancelDrop();
//            //console.log('drop canceled')
        } else {
            dropHandlers.processDrop();
        }





        //        var allRecords = data.records;
        //        var drpRecords = [];
        //        for (var i = 0; i < allRecords.length; i++) {
        //            if (allRecords[i].get('role_type') === gridType) {
        //                drpRecords.push(allRecords[i]);
        //            }
        //        }
        //        if (allRecords.length === drpRecords.length) {
        //            for (var i = 0; i < drpRecords.length; i++) {
        //                var result = store.findRecord('role_id', drpRecords[i].get('role_id'), 0, false, true, true);
        //                if (result === null) {
        //                    dropHandlers.processDrop();
        //                    roleGrid.getSelectionModel().deselectAll(true);
        //                } else {
        //                    dropHandlers.cancelDrop();
        //                }
        //            }
        //        } else {
        //            dropHandlers.cancelDrop();
        //        }
    },
    onDrop: function (node, data, overModel, dropPosition, eOpts) {
        var me = this;
        //        //console.log('data : ',data);
        //        //console.log('droppos : ', overModel);
        //        var temp = data.records;
        //        //console.log(temp[0].data);
        var dragingData = data.records[0].data;
        var dropData = overModel.data;
        //        //console.log(dragingData);


        var store = Ext.create('singer.model.BuisnessM',
                dragingData
                );
        store.set("bisPrtnId", dropData.bisId);
        delete store.data.id;

        //        dragingData.bisPrtnId = dropData.bisPrtnId;
//                //console.log(store.data);



        var loginData = this.getLoggedInUserDataStore();
        var sys = singer.AppParams.SYSTEM_CODE;
        Ext.Msg.wait('Please wait', 'Changing Department...', {
            interval: 300
        });
        Ext.Ajax.request({
            url: singer.AppParams.JBOSS_PATH + 'BussinessStructures/editBussinessStructure',
            method: 'POST',
            headers: {
                userId: loginData.data.getAt(0).get('userId'),
                room: loginData.data.getAt(0).get('room'),
                department: loginData.data.getAt(0).get('department'),
                branch: loginData.data.getAt(0).get('branch'),
                countryCode: loginData.data.getAt(0).get('countryCode'),
                division: loginData.data.getAt(0).get('division'),
                organization: loginData.data.getAt(0).get('organization'),
                system: sys,
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(store.data),
            success: function (response) {
                Ext.Msg.hide();
                var responseData = Ext.decode(response.responseText);
                ////console.log(responseData);
                if (responseData.returnFlag === '1') {

                    var store = me.getBuisnessStoreStore();
                    //                    me.expandChangedNode(response,dropData,1)
                    //                    store.load();
                } else {
                    Ext.Msg.alert("Error updating...", responseData.returnMsg);
                }
                //                    

            },
            failure: function () {
                Ext.Msg.hide();
                Ext.Msg.alert("Error in connection", "Please try again later");
            }
        });
    },
    findById: function (source, id) {
        for (var i = 0; i < source.length; i++) {
            if (source[i].userId === id) {
                return i;
            }
        }
        return -1;
    },
    test: function () {
        var me = this;
        var crntNode = me.selectedNode[0];


        if (Ext.isDefined(crntNode)) {
//            //console.log('went through', crntNode);
            var parentTreeNode = crntNode;
            var depthOfNode = crntNode.getDepth();
//            if (executionType === 1) {
//                depthOfNode = crntNode.getDepth() + 1;
//            }
            for (var i = 0; i < depthOfNode; i++) {
                parentTreeNode = parentTreeNode.parentNode;
                if (parentTreeNode !== null) {
//                    //console.log(parentTreeNode);
                    parentTreeNode.expand();
                }
            }
//            if (parseInt(response) === 1) {
//                if (executionType === 2) {
//                    me.getCatGrid().getView().addRowCls(crntNode, 'before-sync-row-style');
//                    var task = new Ext.util.DelayedTask(function () {
//                        me.getCatGrid().getView().removeRowCls(crntNode, 'before-sync-row-style');
//                    });
//                    task.delay(2000);
//                }
//            }
        }
    },
    expandChangedNode: function (response, orgRecord, executionType) {
        var me = this;
        var treeCatStore = this.getBuisGrid().getStore();
        var crntNode = treeCatStore.getNodeById(orgRecord.get('bisId'));
//        //console.log(crntNode)
//        var crntNode = me.selectedNode[0];
        if (Ext.isDefined(crntNode)) {
//            //console.log('went through');
            var parentTreeNode = crntNode;
            var depthOfNode = crntNode.getDepth();
            if (executionType === 1) {
                depthOfNode = crntNode.getDepth() + 1;
            }
            for (var i = 0; i < depthOfNode; i++) {
                parentTreeNode = parentTreeNode.parentNode;
                if (parentTreeNode !== null) {
//                    //console.log(parentTreeNode);
                    parentTreeNode.expand();
                }
            }
//            if (parseInt(response) === 1) {
//                if (executionType === 2) {
//                    me.getCatGrid().getView().addRowCls(crntNode, 'before-sync-row-style');
//                    var task = new Ext.util.DelayedTask(function () {
//                        me.getCatGrid().getView().removeRowCls(crntNode, 'before-sync-row-style');
//                    });
//                    task.delay(2000);
//                }
//            }
        }
    },
    onDone: function (cmp) {
        //        //console.log(cmp)
        var form = cmp.previousSibling()
//        //console.log(form)
        var te = this.temp;
        te.bisUserCount = form.getStore().getCount();
//        //console.log(te);
        this.temp.set(te);
        cmp.up('window').destroy();
    },
    insertFormSubmit: function (btn) {
        //this.getController('CoreController').onNewRecordCreate(btn, this.getAssetCategoriesStore(), '', '', true, true, this.getCatGrid(), true);
        var createBtn = Ext.getCmp('addDepartmentForm-create');
        var form = btn.up('form').getForm();
        var store = this.getBuisnessStoreStore();
        var me = this;
        if (form.isValid()) {
            store.insert(0, form.getValues());
            createBtn.setDisabled(true);
            me.syncCategoryStore(store, btn.up('form'), 'Loading Categories', 'New Category Creating', 'New Category Created', 'Category Created');
            createBtn.setDisabled(false);
        } else {
            //            this.getController('CoreController').focusToFirstDirtyFieldinForm(btn.up('form'));
        }
    },
    syncCategoryStore: function (store, form, loadingMsg, ProcessMsg, SuccessMsg, title, isDelete) {
        var me = this;
        var executingRecord = store.getModifiedRecords().length > 0 ? store.getModifiedRecords() : store.getRemovedRecords();
        store.on('beforesync', function () {
            Ext.getBody().mask(ProcessMsg, 'large-loading');
        });
        if (!isDelete) {
            form.up("window").setDisabled(true);
        }
        ////console.log(executingRecord[0]);
//        //console.log(executingRecord[0].get('bisPrtnId'));
        var parentExcuteRecord = store.findRecord('bisId', executingRecord[0].get('bisPrtnId'), 0, false, true, true);
//        //console.log(parentExcuteRecord);
        store.sync({
            success: function (batch) {
                Ext.getBody().unmask();
                if (!isDelete) {
                    form.up("window").setDisabled(false);
                }
                //                var response = batch.proxy.getReader().jsonData;
                var response = batch.proxy.getReader().jsonData['returnFlag'];
                var resMsg = batch.proxy.getReader().jsonData['returnMsg'];
                if (response === '1') {
                    //                    FXApp.LayoutController.notify(title, SuccessMsg);
                    if (!isDelete) {
                        //                        me.getController('CoreController').focusToFirstFieldinForm(form);
                        form.getForm().reset();
                    }
                    Ext.getBody().mask(loadingMsg, 'loading');
                } else {
                    singer.LayoutController.createErrorPopup("Error Occured.", resMsg, Ext.MessageBox.WARNING);
                }
                store.load({
                    callback: function (records, operation, success) {
                        if (Ext.getBody().isMasked()) {
                            Ext.getBody().unmask();
                            if (!isDelete) {
                                me.expandChangedNode(response, parentExcuteRecord, 1);
                            } else {
                                me.expandChangedNode(response, parentExcuteRecord, 0);
                            }
                            form.up('window').close();
                        }
                        if (operation.success === false) {
                            me.getController('ExceptionsController').openStoreReloaderWindow(store);
                        }
                    }
                });
            },
            failure: function (batch) {
                Ext.getBody().unmask();
                FXApp.LayoutController.createErrorPopup("Unexpected Error Occured.", "Connection problem with server. </br> Please check network connection.", Ext.MessageBox.ERROR);
                store.rejectChanges();
            }
        });
    },
});