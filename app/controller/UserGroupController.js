/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.controller.UserGroupController', {
    extend: 'Ext.app.Controller',
    alias: 'widget.usergroupcontroller',
    controllers: ['CoreController'],
    stores: [
        'UserGroups'
    ],
    models: [
        'UserGroups'
    ],
    views: [
        'Administrator.userGroups.UserGroup',
        'Administrator.userGroups.NewGroup',
        'Administrator.userGroups.GroupUsersGrid',
        'Administrator.users.UserView',
        'Messages.AreYouSureWithRemark'
    ],
    refs: [
        {
            ref: 'UserGroup',
            selector: 'usergroup'
        }, {
            ref: 'CoreController',
            selector: 'corecontroller'
        }, {
            ref: 'NewGroup',
            selector: 'newgroup'
        }, {
            ref: 'GroupUsersGrid',
            selector: 'groupusersgrid'
        },
        {
            ref: 'UserView',
            selector: 'userview'
        },
        {
            ref: 'AreYouSureWithRemark',
            selector: 'areyousurewithremark'
        }
    ],
    init: function() {
        var me = this;
        me.control({
            'usergroup': {
                added: this.initializeGrid
            },
            'usergroup button[action=open_window]': {
                click: this.openAddNew
            },
            'usergroup ': {
                assignUsers: this.viewAssignUser
            },
            'usergroup  ': {
                editGroup: this.oneditGroup
            },
            'newgroup button[action=cancel]': {
                click: this.insertFormCancel
            },
            'newgroup button[action=create]': {
                click: this.onCreate
            },
            'groupusersgrid': {
                viewUser: this.onviewUser
            },
            'groupusersgrid ': {
                unAssign: this.onunAssign
            },
            'areyousurewithremark button[action=cancel]': {
                click: this.insertFormCancel
            }
        });
    },
    initializeGrid: function(grid) {
        var groupsStore = this.getUserGroupsStore();
        this.getController('CoreController').loadBindedStore(groupsStore);
    },
    onCreate: function(btn) {

    },
    openAddNew: function() {
        this.getController('CoreController').openInsertWindow('newgroup');
    },
    insertFormCancel: function(btn) {
        this.getController('CoreController').onInsertFormCancel(btn);
    },
    viewAssignUser: function() {
//        //////console.log('done');
        var grid = Ext.widget('groupusersgrid');
        var win = Ext.create("Ext.window.Window", {
            width: 700,
            height: 500,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Assing Users',
            items: [grid]
        });
        win.show();
    },
    onviewUser: function(view, record) {
//        //////console.log('view clicked');
        this.getController('CoreController').onOpenViewMoreWindow(record, 'userview', record.get('userId') + "'s details");
    },
    onunAssign: function(view, record) {
        var form = Ext.widget('areyousurewithremark');
        var win = Ext.create("Ext.window.Window", {
            width: 500,
            height: 200,
            layout: 'fit',
            iconCls: 'icon-verification',
            modal: true,
            constrain: true,
            title: 'Are you sure?',
            items: [form]
        });
        win.show();
    },
    oneditGroup: function(view, record) {
//        //////console.log('done');
        this.getController('CoreController').openUpdateWindow(record, 'newgroup', record.get('') + "'s details");
    }
});


