Ext.define("singer.LayoutController", {
    singleton: true,
    createErrorPopup: function(title, msg, type) {
        Ext.MessageBox.show({
            title: title,
            msg: msg,
            buttons: Ext.MessageBox.OK,
            icon: type
        });
    },
    changeViewport: function(removeComp, addComp) {
        Ext.getCmp("mainPanel").remove(removeComp);
        Ext.getCmp("mainPanel").add(addComp);
    },
    changeUsernameSet: function(text) {
        Ext.getCmp("userNameSet").update(text);
    },
    notify: function(tit, msg) {
        Ext.create('widget.uxNotification', {
            title: tit,
            position: 'br',
            useXAxis: true,
            height : 120,
            width:200,
            manager: 'instructions',
            cls: 'ux-notification-light',
            html: '<b>&nbsp;&nbsp;&nbsp;'+msg+'</b><img align="left" src="resources/images/completed.png" />',
            autoCloseDelay: 3000,
            slideBackDuration: 1000,
            slideInDuration: 800,
            slideInAnimation: 'elasticIn',
            slideBackAnimation: 'elasticIn',
            listeners : {
                destroy : function(){
//                    console.log(Ext.ComponentQuery.query('[name='+(document.activeElement).getAttribute('id')+']')[0].focus(300,false));
                }
            }
        }).show();
    }
});