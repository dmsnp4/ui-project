Ext.define('singer.ParamController', {
    singleton: true,
    EXCEL_FILE_OBJ: '',
    EXCEL_MAX_ROW_COUNT: 100,
//    ASSETS_COLUMNS_HEADERS: ["Asset Code", "Description", "Date Purchased",
//        "Supplier", "Asset Status", "Tax Status", "Asset Life Time", "Major Category", "Minor Category", "Micro Category", "Company",
//        "Division", "Department", "Room", "Brand", "Make", "Model",
//        "Product ID", "Reference Code", "Serial Number", "Manufacturer", "Depreciate Percentage", "Total Cost", "Book Value",
//        "Cost Price ACCT", "Cost Price Tax", "Improved Cost ACCT", "Improved Cost Tax", "First Year Allowance Tax", "Dep Precentage Tax",
//        "Accumulated Dep for ACCT", "Accumulated Dep for TAX", "Life Span for ACCT", "Life Span for Tax", "Scrap Value",
//        "Disposal Amount", "Date in Usage", "Last improved Date", "Last Dep Date ACCT", "Last Dep Date Tax", "Disposal Date"],
    EXCEL_JSON_DATA: {},
    CURRENCY_SYMBOL: '<b>Rs. </b>',
    LAST_CLICKED_MENU: '',
    VERIFICATION_GROUP_REC: {},
    SESSION_CHECKER: null,
    TASKER: null,
    PRELOADING_STORES: [
        "Configurations", "allCategoriesStore",
        "allLocationsStore", "allAssetsStore",
        "allExtPartyStore", "allAcctControlStore"
    ],
    MAXCAT_TYPES: 5,
    MAXLOC_LEVELS: 4,
    TX_DEPRECIATION: 1,
    UN_USR: []
            /*
             EXCEL_VALIDATION_FIELDS : [{"seq":0,"field":"asset_code","vtype":"req","ftype":"text"},
             {"seq":1,"field":"asset_cat_id","vtype":"req","ftype":"numb"},
             {"seq":2,"field":"asset_description","vtype":"req","ftype":"text"}
             ]
             */
});