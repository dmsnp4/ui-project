
Ext.define('singer.view.MenuContainer', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.appmainmenu',
    border: false,
    flex: 1,
    maxWidth: 171,
    style: {
       // background : "url('plugins/images/header-bg.png')"
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        margin: 3
    },
    initComponent: function() {
        var menu = this;
        

//        var administrator = Ext.create('Ext.button.Button', {
//            text: 'Administrator',
//            menu: [
//                {text: 'User Maintenance',action:'user'},
//                {text: 'Business Structure', action:'buisness'},
//                {text: 'User Groups', action:'userGroup'},
//                {text: 'Clear Transit', action:'clearTransist'},
//                {text: 'Catagory List', action: 'category'},
//                {text: 'Repair Category',action:'repCat'},
//                {text: 'Defect codes', action:'defectCodes'},
//                {text: 'Spares Maintenance',action:'spare'},
//                {text: 'Model List Maintenance',action:'modelList'},
//                {text:'Warranty Adjustment', action:'warrentyAdj'},
//                {text:'Administrator Reports', action:'reports'}
//            ]
//        });
//        
//        var deviceIssuModule = Ext.create('Ext.button.Button', {
//            text: 'Device Issue Module',
//            menu: [
//                {text: 'Import Debit Notes',action:'import_debit_notes'},
//                {text: 'Accept Debit Notes', action:'debitNotes'},
//                {text:'Adjust IMEI', action:'adjustImei'},
//                {text: 'IMEI Maintenance', action:'imeiMaintenace'},
//                {text: 'Device Issue Function',action:'device_issue'},
//                {text: 'Device Return', action: 'device_return'},
//                {text: 'Credit Note Issue Function', action: 'credit_note'},
//                {text: 'Replacement Returns', action:'repacement_return'},
//                {text: 'Dashboard',action:''},
//                {text: 'Device Issue reports',action:'deviceIssueReports'}
//                // {text: 'Buisness Structure', iconCls: 'icon-category', action: 'category'}
//            ]
//        });
//        
//        var serviceModule = Ext.create('Ext.button.Button', {
//            text: 'Service',
//            menu: [
//                {text: 'Work Order Maintenance',action:'wrkOrderMaintain'},
//                {text: 'Work Order Opening',action:'wrkOrderOpening'},
//                {text: 'Reapair Payments Aprrove',action:'rpirAprove'},
//                {text: 'Exchange Function',action:'exchngeFunction'},
//                {text: 'Work Order Closing',action:'woClosing'},
//                {text: 'Work Order Transfer',action:'woTransfer'},
//                {text: 'Repair Payments',action:'repairPayments'},
//            ]
//        });
//        menu.items=[administrator,deviceIssuModule];

        //========================================================================
     

        menu.callParent();
    }
});

