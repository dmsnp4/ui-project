/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('singer.view.Footer' ,{
    extend: 'Ext.container.Container',
    alias: 'widget.footercontainer',
    layout : 'fit',
    minHeight : 30,
    maxHeight : 30,
    padding: 5,
        style: {
        backgroundColor: '#ffffff',
        borderTop: '3px #6bbde8 solid',
        boxShadow: '0px 0px 3px #000000'
    },
    html : '<div style="width :320px; margin: auto; height: 20px; text-align: center;">\n\
            <img src="plugins/images/dms-footer-logo.png" />\n\
            <b style="float: right;">Powered by DMS Software Division</b></div>'
});