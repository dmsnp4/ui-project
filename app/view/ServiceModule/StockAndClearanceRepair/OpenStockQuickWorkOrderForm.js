

Ext.define('singer.view.ServiceModule.StockAndClearanceRepair.OpenStockQuickWorkOrderForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.OpenStockQuickWorkOrderForm',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    height: "80%",
    initComponent: function () {

        var quickWorkOrderWin = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
            items: quickWorkOrderWin.buildItems(),
            buttons: quickWorkOrderWin.buildButtons()
        });
        quickWorkOrderWin.items = [formPanel];
        quickWorkOrderWin.callParent();
    },
    buildItems: function () {
        var woTypes = Ext.create('Ext.data.Store', {
            fields: ['PaymentType', 'PaymentTypeDesc'],
            data: [
//                {"PaymentType": 1, "PaymentTypeDesc": "Customer repairs"},
                {"PaymentType": 2, "PaymentTypeDesc": "Stock Repair"},
                {"PaymentType": 3, "PaymentTypeDesc": "Clearance Repairs"}
            ]
        });
        return [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [
//                    {
//                        xtype: 'combo',
//                        fieldLabel: 'Work Order No',
//                        name: 'workOrderNo',
//                        flex: 4,
//                        store: 'woDrp',
//                        displayField: 'workOrderNo',
//                        valueField: 'workOrderNo',
//                        visible: true,
//                        queryMode: 'remote',
//                        listeners: {
//                            change: function (field, newVal, oldVal) {
//                              //  this.up("window").fireEvent("on_work_order_change", field, newVal);
//                            }
//                        }
//                    },
                        
                   
                    {
                      xtype: 'textfield',
                      fieldLabel: 'Work Order No',
                      name: 'workOrderNo',
                      flex: 3,
                      readOnly: true
                  },
                  {
                      xtype: 'button',
                      text: 'Search',
                      disabled: false,
                      id: 'openQuickWorkOrderForm-searchwo',
                      action: 'search_quick_work_order',
                      flex: 1,
                      margin: "0px 0px 0px 5px"
                  }
                        
                       
                  
                ]
            },
             {
                fieldLabel: 'Received Date',
               // name: 'createDate',
                xtype: 'datefield',
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                value: new Date(),
                readOnly: true
            },
           
            {
                id: 'imeii',
                fieldLabel: 'IMEI',
                name: 'imeiNo'
                //readOnly: true
            },
            
            {
                xtype: 'combo',
                id: 'cmbPayType1',
                fieldLabel: 'Workorder Type',
                name: 'paymentType',
                store: woTypes, 
                displayField: 'PaymentTypeDesc',
                valueField: 'PaymentType',
                queryMode: 'remote',
                allowBlank: false,
                value: 2

            },

         
            {
                fieldLabel: 'Product',
                name: 'product'
                //readOnly: true
            },
            {
                fieldLabel: 'Brand',
                name: 'brand'
                //readOnly: true
            },
            {
                fieldLabel: 'Model',
                xtype: 'combobox',
                name: 'modleNo',
                emptyText: 'Select Model',
                store: 'modStore',
                displayField: 'model_Description',
                valueField: 'model_No',
                afterLabelTextTpl: "",
                queryMode: 'local'
            },
            {
                fieldLabel: 'Accesseries Reatained',
                name: 'assessoriesRetained'
                //readOnly: true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Defect',
                name: 'defectNo',
                store: 'mjrdefectStr',
                displayField: 'mjrDefectDesc',
                valueField: 'mjrDefectCode',
                queryMode: 'local'
            },
            {
                fieldLabel: 'Rcc Reference',
                name: 'rccReference'
                //readOnly: true
            },
//            {
//                fieldLabel: 'Date Of Sale',
//                name: 'dateOfSale',
//                xtype: 'datefield',
//                allowBlank: true,
//                afterLabelTextTpl: '' ,
////                maxValue: new Date(),
//                format: 'Y-m-d',
//                altFormats: 'm/d/Y',
//            },
            
           
//            {
//                xtype: 'combo',
//                fieldLabel: 'Service Agent',
//                name: 'serviceBisId',
//                store: 'distributorstr',
//                readOnly : true,
//                displayField: 'bisName',
//                valueField: 'bisId',
//                queryMode: 'local',
//                allowBlank: true,
//                afterLabelTextTpl: ''
//            },

            {
                fieldLabel: 'Service Agent',
                name: 'serviceBisDesc',
                readOnly: true
            },
            {
                xtype:'hiddenfield',
                name: 'serviceBisId'
            },

            {
                xtype:'hiddenfield',
                fieldLabel: 'Warranty Type',
                name: 'warrentyVrifType',
//                id:'expirestate',
                readOnly:true,
                allowBlank: true,
                afterLabelTextTpl: ''
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Warranty Type',
                name: 'expireStatus',
                listeners: {
                        scope: this,
                        change: function (me, newValue, oldValue) {
                            //console.log(newValue);
                            if (newValue === 2) {
                                me.setValue('Out Of Warranty');
                            } else {
                                me.setValue('Under Warranty');
                            }
                        }
                    }
            },
            {
                xtype: 'hiddenfield',
                fieldLabel: 'Warranty Type',
                name: 'expireStatus',
            },
            {
                    xtype: 'combo',
                    fieldLabel: 'Distributor/Shop',
                    id: 'DistributorShop',
                    name: 'serviceBisId',
                    store: 'distributorstr',
                    displayField: 'bisName',
                    valueField: 'bisId',
                    queryMode: 'remote',
                    listeners: {
                        change: function (cmp, newValue, oldValue) {
                            console.log('newValue', newValue);
                            console.log('cmp', cmp.findRecordByValue(newValue));

                            if (newValue !== undefined) {
                                cmp.nextSibling().setValue(cmp.findRecordByValue(newValue).getData().address)
                            }
                        }
                    },
                    allowBlank: false,
                    afterLabelTextTpl: ""
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Distributor/Shop Address',
                    afterLabelTextTpl: "",
                    allowBlank: false
                },
                {
                    xtype: 'textareafield',
                    fieldLabel: 'Remarks',
                    id: 'Remarks',
                    name: 'remarks',
                    allowBlank: false,
                    regex: /^[a-zA-Z_ 0-9\,'']+$/
                }

        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, '->',
            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                formBind: true,
                hidden: true,
                id: 'OpenStockQuickWorkOrderForm-save'
            },
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                formBind: true,
                hidden: true,
                id: 'OpenStockQuickWorkOrderForm-create'
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});