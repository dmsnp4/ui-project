Ext.define('singer.view.ServiceModule.StockAndClearanceRepair.WorkOrderOpenFormForStockRepair', {
    extend: 'Ext.window.Window',
    alias: 'widget.WorkOrderOpenFormForStockRepair',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    title: 'Add New Work Order',
    width: 500,
    height: "80%",
    initComponent: function () {
        var workOrderOpen = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            overflowY: 'auto',
            bodyPadding: '0',
            layout: {
                type: 'accordion',
                titleCollapse: true,
                animate: true,
                activeOnTop: false
            },
            fieldDefaults: {
                anchor: '100%',
                msgTarget: 'side',
                labelAlign: 'left',
                labelWidth: 180,
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
            items: workOrderOpen.buildItems(),
            buttons: workOrderOpen.buildButtons()
        });
        workOrderOpen.items = [formPanel];
        workOrderOpen.callParent();
    },
    buildCustomerInformation: function () {
        return {
            xtype: 'panel',
            title: "Customer Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'textfield',
                margin: '10 10 0 10'
            },
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'workOrderNo'
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'nicfield',
                            fieldLabel: 'NIC',
                            name: 'customerNic',
                            id: 'customerNic',
                            // value: '890222536V',
                            flex: 4,
                            listeners: {
                                validitychange: function (field, isValid) {
                                    this.up("window").fireEvent("on_nic_validity_change", field, isValid);
                                },
                                change: function (field, newVal, oldVal) {
                                    this.up("window").fireEvent("on_nic_change", field, newVal);
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Verify',
                            disabled: true,
                            action: 'verify_nic',
                            flex: 1,
                            margin: "0px 0px 0px 5px",
                            id: 'workOrderOpenForm-verify_nic'
                        }
                    ]
                },
                {
                    fieldLabel: 'Name',
                    id: 'Name',
                    name: 'customerName'
                },
                {
                    fieldLabel: 'Address',
                    id: 'Address',
                    name: 'customerAddress'
                },
                {
                    fieldLabel: 'Telephone',
                    id: 'Telephone',
                    name: 'workTelephoneNo',
                    vtype: 'phoneVtype',
                    regex: /^[0-9_]+$/,
                    maxLength: 10,
                    minLength: 10
                },
                {
                    fieldLabel: 'Email',
                    id: 'customerEmail',
                    name: 'email',
                    regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                },
                {
                    fieldLabel: 'Re-type Email',
                    id: 'Re-typeEmail',
                    name: 'email2',
                    regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    vtype: 'emailMatchVtype'
                }
            ]
        };
    },
    buildProductInformation: function () {
        return {
            xtype: 'panel',
            title: "Product Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'textfield',
                margin: '10 10 0 10'
            },
            items: [
//                {
//                    fieldLabel: 'IMEI',
//                    id: 'IMEI',
//                    name: 'imeiNo',
//                    vtype: 'imeiVtype',
//                    listeners: {
//                        validitychange: function (field, isValid) {
//                            this.up("window").fireEvent("on_imei_validity_change", field, isValid);
//                        },
//                        change: function (field, newVal, oldVal) {
//                            this.up("window").fireEvent("on_imei_change", field);
//                        }
//                    }
//                },
//                {
//                    xtype: 'hidden',
//                    name: 'isImeiVerified',
//                    listeners: {
//                        change: function (field, newVal, oldVal) {
//                            this.up("window").fireEvent("on_imei_validity_change", field);
//                        }
//                    }
//                },
//                {
//                    xtype: 'container',
//                    layout: {
//                        type: 'hbox',
//                        align: 'stretch'
//                    },
//                    defaults: {
//                        margin: 5
//                    },
//                    items: [
//                        //                        {
//                        //                            xtype: 'button',
//                        //                            id: 'workOrderOpenForm-newimei',
//                        //                            text: 'Register New IMEI',
//                        //                            action: 'register_imei',
//                        //                            disabled: true,
//                        //                            flex: 1
//                        //                        }, 
//                        {
//                            xtype: 'button',
//                            id: 'workOrderOpenForm-verifyimei',
//                            text: 'Verify IMEI',
//                            disabled: true,
//                            action: 'verify_imei',
//                            flex: 1
//                        }
//                    ]
//                },
                {
                    fieldLabel: 'Product',
                    name: 'product',
                    readOnly: true,
                    allowBlank: true,
                    afterLabelTextTpl: ""
                },
                {
                    fieldLabel: 'Brand',
                    name: 'brand',
                    readOnly: true,
                    allowBlank: true,
                    afterLabelTextTpl: ""
                },
                {
                    fieldLabel: 'Model',
                    xtype: 'combobox',
                    name: 'modleNo',
                    id : 'modleNoForStockRepair',
                    emptyText: 'Select Model',
                    store: 'modStore',
                    displayField: 'model_Description',
                    valueField: 'modleNo',
                    readOnly: true,
                    allowBlank: true,
                    afterLabelTextTpl: "",
                    queryMode: 'local'
                },
                {
                    fieldLabel: 'Accessories Retained',
                    id: 'AccessoriesRetained',
                    name: 'assessoriesRetained'
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Defect',
                    id: 'Defect',
                    name: 'defectNo',
                    store: 'mjrdefectStr',
                    displayField: 'mjrDefectDesc',
                    valueField: 'mjrDefectCode',
                    queryMode: 'local'
                },
                {
                    fieldLabel: 'RCC Reference',
                    id: 'RCCReference',
                    name: 'rccReference'
                },
                {
                    fieldLabel: 'Date Of Sale',
                    id: 'DateOfSale',
                    name: 'dateOfSale',
                    xtype: 'datefield',
                    maxValue: new Date(),
                    format: 'Y/m/d',
                    //                    value: new Date(),
                    altFormats: 'm/d/Y',
                    allowBlank: true,
                    readOnly: false,
                    afterLabelTextTpl: ''
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Distributor/Shop',
                    id: 'DistributorShop',
                    name: 'serviceBisId',
                    store: 'distributorstr',
                    displayField: 'bisName',
                    valueField: 'bisId',
                    queryMode: 'remote',
                    listeners: {
                        change: function (cmp, newValue, oldValue) {
                            console.log('newValue', newValue);
                            console.log('cmp', cmp.findRecordByValue(newValue));

                            if (newValue !== undefined) {
                                cmp.nextSibling().setValue(cmp.findRecordByValue(newValue).getData().address)
                            }
                        }
                    },
                    allowBlank: false,
                    afterLabelTextTpl: ""
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Distributor/Shop Address',
                    afterLabelTextTpl: "",
                    allowBlank: false
                },
                {
                    xtype: 'textareafield',
                    fieldLabel: 'Remarks',
                    id: 'Remarks',
                    name: 'remarks',
                    allowBlank: false,
                    regex: /^[a-zA-Z_ 0-9\,'']+$/
                }
            ]
        };
    },
    buildWarrentyInformation: function () {
        var woTypes = Ext.create('Ext.data.Store', {
            fields: ['PaymentType', 'PaymentTypeDesc'],
            data: [
//                {"PaymentType": 1, "PaymentTypeDesc": "Customer repairs"},
                {"PaymentType": 2, "PaymentTypeDesc": "Stock Repair"},
                {"PaymentType": 3, "PaymentTypeDesc": "Clearance Repairs"}
            ]
        });
        
        
        return {
            xtype: 'panel',
            title: "Warranty Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'textfield',
                margin: '10 10 0 10'
            },
            items: [{
                    fieldLabel: 'IMEI',
                    id: 'IMEI',
                    name: 'imeiNo',
                    listeners: {
                        validitychange: function (field, isValid) {
                            this.up("window").fireEvent("on_imei_validity_change", field, isValid);
                        },
                        change: function (field, newVal, oldVal) {
                            this.up("window").fireEvent("on_imei_change", field);
                        }
                    }
                },
                {
                    xtype: 'hidden',
                    name: 'isImeiVerified',
                    listeners: {
                        change: function (field, newVal, oldVal) {
                            this.up("window").fireEvent("on_imei_validity_change", field);
                        }
                    }
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    defaults: {
                        margin: 5
                    },
                    items: [
                        {
                            xtype: 'combo',
                            id: 'cmbPayType',
                            fieldLabel: 'Workorder Type',
//                    readOnly: true,
                            name: 'paymentType',
                            store: woTypes, //'WoPayTypeStore',
                            displayField: 'PaymentTypeDesc',
                            valueField: 'PaymentType',
                            //disabled: true,
                            //hidden: true,
                            queryMode: 'remote',
//                    allowBlank: 'true',
                            allowBlank: false,
                            value: 2
//                    listeners: {
//                        scope: this,
//                        change: function (me, newValue, oldValue) {
//                            //                        //console.log(newValue)
//                            if (newValue !== '' || newValue !== null) {
//                                me.nextSibling().allowBlank = false;
//                                me.nextSibling().show();
//                                me.nextSibling().afterLabelTextTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
//                            } else {
//                                me.nextSibling().allowBlank = true;
//                                me.nextSibling().hide();
//                            }
//                        }
//                    }
                        },
                        //                        {
                        //                            xtype: 'button',
                        //                            id: 'workOrderOpenForm-newimei',
                        //                            text: 'Register New IMEI',
                        //                            action: 'register_imei',
                        //                            disabled: true,
                        //                            flex: 1
                        //                        }, 
                        {
                            xtype: 'button',
                            id: 'WorkOrderOpenFormForStockRepair-verifyimei',
                            text: 'Verify IMEI',
                            disabled: true,
                            action: 'verify_imei',
                            flex: 1
                        }
                    ]
                },
                {
                    xtype: 'button',
                    text: 'View Repair History',
                    action: 'view_repair_history',
                    disabled: true,
                    id: 'WorkOrderOpenFormForStockRepair-viewrepairhistory'
                },
                {
                    fieldLabel: 'Received Date',
                    name: 'createDate',
                    readOnly: true
                },
                {
                    fieldLabel: 'Received Date',
                    id: 'WO_Open_Edit_RDate',
//                    name: 'createDate',
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    altFormats: 'm/d/Y',
                    value: new Date(),
                    readOnly: true
                },
//                {
//                    xtype:'displayfield',
//                    fieldLabel: 'Warranty Type',
//                    id:'WrntTyp'
//                },
//                 {
//                    xtype:'hiddenfield',
//                    fieldLabel: 'Warranty Type',
//                    name: 'expireStatus',
////                    readOnly:true,
////                    listeners: {
////                        scope: this,
////                        change: function (me, newValue, oldValue) {
////                            //console.log(newValue);
////                            if (newValue === 2) {
////                                
////                            } else {
////                                
////                            }
////                        }
////                    }
//                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Warranty Type',
                    id : 'warrantyTypeForStock',
                    name: 'expireStatus',
                    store: 'WrrntyType',
                    displayField: 'description',
                    valueField: 'code',
                    allowBlank: true,
                    afterLabelTextTpl: "",
                    queryMode: 'remote',
                    listeners: {
                        scope: this,
                        change: function (me, newValue, oldValue) {
                            //console.log(newValue);
                            if (newValue === 1) {
//                                me.previousSibling().setValue('Out Of Warranty');
                                me.nextSibling().allowBlank = false;
                                me.nextSibling().show();
                                me.nextSibling().afterLabelTextTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>';
                                me.nextSibling().nextSibling().allowBlank = false;
                                me.nextSibling().nextSibling().show();
                                me.nextSibling().nextSibling().afterLabelTextTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
                            } else {
//                                me.previousSibling().setValue('Under Warranty');
                                me.nextSibling().allowBlank = true;
                                me.nextSibling().hide();
                                me.nextSibling().nextSibling().allowBlank = true;
                                me.nextSibling().nextSibling().hide();
                            }
                        }
                    }
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Non Warranty Type',
                    name: 'nonWarrentyVrifType',
                    store: 'NonWrrntyType',
                    displayField: 'description',
                    valueField: 'code',
                    queryMode: 'remote',
                    allowBlank: 'true',
                    afterLabelTextTpl: '',
                    hidden: true,
                    listeners: {
                        scope: this,
                        change: function (me, newValue, oldValue) {
                            //                        //console.log(newValue)
                            if (newValue !== '' || newValue !== null) {
                                me.nextSibling().allowBlank = false;
                                me.nextSibling().show();
                                me.nextSibling().afterLabelTextTpl = '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
                            } else {
                                me.nextSibling().allowBlank = true;
                                me.nextSibling().hide();
                            }
                        }
                    }
                },
                {
                    xtype: 'textareafield',
                    grow: true,
                    fieldLabel: 'Non Warranty Remarks',
                    name: 'nonWarrantyRemarks',
                    allowBlank: 'true',
                    hidden: true,
                    afterLabelTextTpl: ''
                },
                {
                    xtype: 'textareafield',
                    grow: true,
                    fieldLabel: 'Customer Complain',
                    name: 'customerComplain',
                    afterLabelTextTpl: '',
                    allowBlank: 'true',
                },
                {
                    xtype: 'hiddenfield',
                    name: 'proofOfPurches'
                }
            ]
        };
    },
    buildItems: function () {
        return [
//            this.buildCustomerInformation(),
            this.buildWarrentyInformation(),
            this.buildProductInformation()
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }
            ,
            '->'
                    ,
            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                formBind: true,
                hidden: true,
                id: 'WorkOrderOpenFormForStockRepair-save'
            },
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                formBind: true,
                hidden: true,
                id: 'WorkOrderOpenFormForStockRepair-create'
            }
        ];
    }
});