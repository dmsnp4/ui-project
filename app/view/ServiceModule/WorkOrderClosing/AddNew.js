

Ext.define('singer.view.ServiceModule.WorkOrderClosing.AddNew', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addNewWorkOrderclosing',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 530,
    initComponent: function () {
        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
//            {
//                xtype: 'container',
//                layout: 'hbox',
//                defaultType: 'textfield',
//                items: [
//                    {
//                        id: 'wrkOrdNo',
//                        margin: '10 0 10 0',
//                        fieldLabel: 'Work Order No',
//                        name: 'workOrderNo',
//                        allowBlank: false
//                    },
////                    {
////                        margin: '10 0 10 20',
////                        xtype: 'button',
////                        text: 'Search',
////                        action: 'search_Work_Order'
////                    },
////                    {
////                        id: 'veryfy',
////                        xtype: 'label',
////                        hidden: true,
////                        margin: '10 0 10 20',
////                        text: 'Verified',
////                     }
//                           ]
//            },
            {
                xtype: 'combo',
                fieldLabel: 'WO Number',
                name: 'workOrderNo',
                id : "workOrderCloseWorkOrderNo",
                allowBlank: false,
                store: 'clseWOnum',
                displayField: 'workOrderNo',
                valueField: 'workOrderNo',
                queryMode: 'remote',
                listeners: {
                    beforequery: function (record) {
                        record.query = new RegExp(record.query, 'i');
                        record.forceAll = true;
                    },
                    change: function (cmp) {
                        var me = this;

                        var woVal = cmp.getValue();


                        Ext.Ajax.request({
                            url: singer.AppParams.JBOSS_PATH + "WorkOrders/getWorkOrderDetailsByWo",
                            params: {woNumber: woVal},
                            success: function (response) {

                                var output = JSON.parse(response.responseText);

                                var defect = output.data[0];
                                var accessories = output.data[1];
                                Ext.getCmp("resolveDefect").setFieldLabel("Resolve Customer complaint / Defect (" + defect + ")");
                                Ext.getCmp("accessories_delivered").setFieldLabel("Retained Accessories (" + accessories + ")  Delivered? ");

                            },
                            method: "GET"
                        });


                    }
                }
            },
            {
                xtype: 'hiddenfield',
                name: 'status'
            },
            {
                xtype: 'combo',
                fieldLabel: 'Repair Status',
                name: 'repairid',
                id: 'repairNAME',
                maxLength: 100,
                allowBlank: false,
                store: 'clseRprStore',
                displayField: 'description',
                valueField: 'code',
                visible: true,
                queryMode: 'remote',
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        var rpairStatus = cmp.nextSibling();
                        if (oldValue !== undefined) {
                            if (oldValue !== newValue) {
                                if (cmp.up('window').title === 'Add New') {
                                    rpairStatus.hide();
                                } else {
                                    rpairStatus.show();
//                                    rpairStatus.allowBlank=false;
                                }
                            }
                        }
                    }
                }
            },
            {
                id: 'RstsREMARK',
                fieldLabel: 'Remark',
                name: 'remarks',
                maxLength: 100,
//                allowBlank: false,
                hidden: true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Delivery Type',
                name: 'deleveryId',
                //             name:'deleveryType',
                maxLength: 100,
                allowBlank: false,
                store: 'DeliverType',
                displayField: 'description',
                valueField: 'code',
                visible: true,
                queryMode: 'remote',
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        var courNo = cmp.nextSibling().nextSibling();
                        if (newValue === 2) {
                            courNo.show();
                            courNo.allowBlank = false;
                        } else {
                            courNo.hide();
                            courNo.allowBlank = true;
                            courNo.setValue('');
                        }
                    }
                }
            },
            {
                xtype: 'combo',
                fieldLabel: 'Transfer Location',
                name: 'locationId',
                maxLength: 100,
                allowBlank: false,
                store: 'distributorstr',
                displayField: 'bisName',
                valueField: 'bisId',
                visible: true,
                queryMode: 'remote',
            },
            {
                fieldLabel: 'Courier No',
                name: 'courierNo',
                maxLength: 100,
                allowBlank: false,
                hidden: true
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Delivery Date',
                name: 'deleverDate',
                maxLength: 100,
                allowBlank: false,
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false,
                minValue: new Date(),
            },
            {
                fieldLabel: 'Gate Pass Or Other Reference',
                name: 'gatePass',
                maxLength: 100,
//                allowBlank: false
            },
            {
                xtype: "checkboxfield",
                fieldLabel: "Resolved Customer complaint / Defect",
                anchor: '100%',
                name: "resolveDefect",
                id: "resolveDefect"
            },
            {
                xtype: "checkboxfield",
                fieldLabel: "Visual Inspection Test",
                name: "visualInspectionTest",
                id: "visualInspectionTest"
            },
            {
                xtype: "checkboxfield",
                fieldLabel: "Engineering Test",
                name: "engineeringTest",
                id: "engineeringTest"
            },
            {
                xtype: "checkboxfield",
                fieldLabel: "Call / SMS / MMS / Data / Wifi / Bluetooth",
                name: "callSms",
                id: "callSms"
            },
            {
                xtype: "textfield",
                fieldLabel: "Install SW Version ",
                allowBlank: false,
                name: "swVersion",
                id: "swVersion"
            },
            {
                xtype: "checkboxfield",
                fieldLabel: "Retained Accessories Delivered? ",
                name: "accessories_delivered",
                id: "accessories_delivered"
            }
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Save',
                action: 'editNewSubmit',
                formBind: true,
                id: 'addNewWorkOrderclosing-save',
            },
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Create',
                action: 'addNewSubmit',
                formBind: true,
                id: 'addNewWorkOrderclosing-create',
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'work_order_closing_cancel'
            },
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});