


Ext.define('singer.view.ServiceModule.WorkOrderClosing.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.editWorkOrderclosing',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    
    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
//            {xtype: 'hiddenfield',name: 'bisId',id:'businessId'},
            {
               xtype: 'container',
               layout: 'hbox',
               defaultType: 'textfield',
                    items:[
                          {
                            id:'wrkOrdNo',  
                            margin:'10 0 10 0',
                            fieldLabel: 'Work Order No',
                            name: 'workOrderNo',
                            allowBlank: false
                          },
                        {
                            margin:'10 0 10 20',
                            xtype:'button',
                            text:'Search',
                            action:'search_Work_Order'
                        },
                                    ]
            },

            {
             xtype:'combo',
             fieldLabel: 'Repair Status',
             name: 'repairid',
             maxLength: 100,
             allowBlank: false,
             store:'RepStatus',
             displayField: 'description',  
             valueField: 'code',
             visible: true,
             queryMode: 'remote',
            },
            {
             xtype:'combo',
             fieldLabel: 'Deleivery Type',
             name: 'deleveryId',
             maxLength: 100,
             allowBlank: false,
             store:'DeliverType',
             displayField: 'description',  
             valueField: 'code',
             visible: true, 
             queryMode: 'remote',
            },
            {
             xtype:'combo',
             fieldLabel: 'Trancefer Location',
             name: 'locationId',
             maxLength: 100,
             allowBlank: false,
             store:'distributorstr',
             displayField: 'bisName',  
             valueField: 'bisId',
             visible: true,
             queryMode: 'remote',
            },
            {fieldLabel: 'Courier No',name: 'courierNo',maxLength: 100,allowBlank: false},
            {xtype: 'datefield',fieldLabel: 'Delivery Date',name: 'deleverDate',maxLength: 100,
                allowBlank: false,
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false},
            {fieldLabel: 'Gate Pass Or Other Refference',name: 'gatePass',maxLength: 100,allowBlank: false},
        ];
    },
    buildButtons: function () {
        return [
             {
                xtype: 'button',
                text: 'Submit',
                action: 'submit_edit',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'work_order_closing_cancel'
            },
           
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});