Ext.define('singer.view.ServiceModule.WorkOrderClosing.View', {
    extend: 'Ext.window.Window',
    alias: 'widget.WOclosingView',
    iconCls: 'icon-user',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    initComponent: function () {
        var WOclose = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: WOclose.buildItems(),
            buttons: WOclose.buildButtons()
        });
        WOclose.items = [formPanel];
        WOclose.callParent();
    },
    buildItems: function () {

        return [
            {
                fieldLabel: 'Work Order No:',
                name: 'workOrderNo',
                listeners: {
                    afterrender: function (comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        } else {
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Repair Status:',
                name: 'repairSatus'
            },
            {
                fieldLabel: 'Delivery Type:',
                name: 'deleveryType',
            },
            {
                fieldLabel: 'Transfer Location:',
                name: 'transferLocation'
            },
            {
                id: 'currierrNo',
                fieldLabel: 'Courier No:',
                name: 'courierNo',
                hidden: true,
            },
            {
                fieldLabel: 'Delivery Date:',
                name: 'deleverDate'
            },
            {
                fieldLabel: 'Gate Pass Or the Refference:',
                name: 'gatePass'
            },
            {
                xtype: "checkboxfield",
                fieldLabel: 'Resolve Customer complaint / Defect:',
                id: 'viewResolveDefect',
                style : {
//                    font-weight : "bold"
                },
                readOnly : true
            },
            {
                xtype: "checkboxfield",
                fieldLabel: 'Visual Inspection Test:',
                id: 'viewInspection',
                readOnly : true
            },
            {
                xtype: "checkboxfield",
                fieldLabel: 'Engineering Test:',
                id: 'viewEngineering',
                readOnly : true
            },
            {
                xtype: "checkboxfield",
                fieldLabel: 'Call / SMS / MMS / Data / Wifi / Bluetooth:',
                id: 'viewCallSms',
                readOnly : true
            },
            {
                fieldLabel: 'Install SW Version:',
                id: 'viewSwVersion'
            },
            {
                xtype: "checkboxfield",
                fieldLabel: 'Retained Accessories Delivered? :',
                id: 'viewDelivered',
                readOnly : true
            }
        ];
    },
    buildButtons: function () {
        return ['->', {
                text: 'Close',
                action: 'View_cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});






