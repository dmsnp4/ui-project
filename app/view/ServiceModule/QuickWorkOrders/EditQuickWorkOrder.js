
Ext.define('singer.view.ServiceModule.QuickWorkOrders.EditQuickWorkOrder', {
    extend: 'Ext.window.Window',
    alias: 'widget.editquickworkorder',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    height: "80%",
    requires: [
        'Ext.form.Panel'
    ],
    initComponent: function() {
            console.log("SSSSSSSSSSss");

        var workOrderOpenView = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            overflowY: 'auto',
            bodyPadding: '0',
            layout: {
                type: 'accordion',
                titleCollapse: true,
                animate: true,
                activeOnTop: false
            },
            fieldDefaults: {
                xtype: 'displayfield',
                anchor: '100%',
                msgTarget: 'side',
                labelAlign: 'left',
                labelWidth: 180,
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>'
            },
            items: workOrderOpenView.buildItems(),
            buttons: workOrderOpenView.buildButtons()
        });
        workOrderOpenView.items = [formPanel];
        workOrderOpenView.callParent();
    },
    buildCustomerInformation: function() {
        return {
            xtype: 'panel',
            title: "Customer Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {type: 'vbox', align: 'stretch'},
            defaults: {
                xtype: 'displayfield',
                margin: '10 10 0 10'
            },
            items: [
                {
                    fieldLabel: 'Imei',
                    name: 'imeiNo',
                    listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
                },
                {
                    fieldLabel: 'Product',
                    name: 'product'
                },
                {
                    fieldLabel: 'Model',
                    name: 'modleNo'
                },
                {
                    fieldLabel: 'NIC',
                    name: 'customerNic'
                },
                {
                    fieldLabel: 'Customer Name',
                    name: 'customerName'
                },
                {
                    fieldLabel: 'Contact No:',
                    name: 'workTelephoneNo'
                },
                {
                    fieldLabel: 'Email:',
                    name: 'email'
                },
                {
                    fieldLabel: 'Defect',
                    name: 'defectNo'
                },
                {
                    fieldLabel: 'Accessories',
                    name: 'assessoriesRetained'
                }
            ]
        };
    },
    
    
    buildItems: function() {
        return [
            this.buildCustomerInformation()
            
        ];
    },
    buildButtons: function() {
        return [
            '->',
            {
                xtype: 'button',
                text: 'Close',
                action: 'cancel'
            }
        ];
    }
});





