

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.ServiceModule.QuickWorkOrders.AddForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.newquickorder',
    loadMask: true,
    modal: true,
//    iconCls: 'icon-user',
    title: 'Add New Quick Order',
//    store: 'Status',
//    modal: true,
    layout: 'fit',
    constrain: true,
    width: 600,
    height: 480,
    initComponent: function () {
        console.log('add form');
        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 300,
//            loadMask: true,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
//            items: quickWorkOrderWin.buildItems(),
//            buttons: quickWorkOrderWin.buildButtons()
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });
//
        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
//
    },
    buildItems: function () {
//
//
//        var stat = Ext.create('Ext.data.Store', {
//            fields: ['status', 'value'],
//            data: [
//                {"status": 2, "value": "Complete"},
//                {"status": 1, "value": "Pending"},
////                {"status": 3, "value": "Unconfirmed"}
//            ]
//        });se
        return [
            {
                fieldLabel: 'Issue No',
                name: 'issueNo',
                xtype: 'hiddenfield',
                maxLength: 10
//                visible: true
//                vtype: 'ValidUserID',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/
            },
            {
                xtype: 'hiddenfield',
//                fieldLabel: 'distri',
                name: 'distributerID'
            },
            {
                xtype: 'panel',
                layout: 'hbox',
                id: 'panle-abc',
                items:[
                    {       
                        xtype: 'textfield',
                        id: 'txtIMEINo',
                        name: 'imeiNo',
                        fieldLabel: 'Imei No',
                        flex: 4
                    },
                    {
                        xtype: 'button',
                        text: 'Verify',
                        action: 'verifyImei',
                        flex:1,
                        style: 'margin-top: 10px;    margin-left: 4px;'
                    }
                ]
            },
            
            {
                fieldLabel: 'Product',
                id: 'qimeiProduct',
                xtype: 'textfield',
                name: 'product',
                store: 'radioStr',
                queryMode: 'local',
//                visible: true,
                editable: false
//                allowBlank: false
            },
            {
              xtype:'hiddenfield',
              id : 'expireStatus',
              name : 'expireStatus'
            },
            {
                fieldLabel: 'Model',
                xtype: 'combobox',
                name: 'modleNo',
                emptyText: 'Select Model',
                store: 'modStore',
                displayField: 'model_Description',
                valueField: 'model_No',
                afterLabelTextTpl: "",
                queryMode: 'local',
                id: 'qIMEIModel'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'NIC',
                name: 'customerNic',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                regex: /^[0-9_V]+$/,
                minLength: 10,
                maxLength: 10
//                listeners: {
//                                validitychange: function (field, isValid) {
//                        this.up("window").fireEvent("on_nic_validity_change", field, isValid);
//                    },
//                    change: function (field, newVal, oldVal) {
//                        this.up("window").fireEvent("on_nic_change", field, newVal);
//                    }
//                }

            },
            {
                xtype: "textfield",
                //id: 'txtCus',
                fieldLabel: 'Customer Name',
                action: 'clickRdio',
                name: 'customerName',
                columns: 3
            },
            {
                xtype: "textfield",
                // id: 'txtNo',
                name: 'workTelephoneNo',
                fieldLabel: 'Contact No',
                regex: /^[0-9_]+$/,
                action: 'clickRdio',
                allowBlank: true,
                maxLength: 10
            },
            {
                xtype: "textfield",
                //id: 'txtEmail',
                fieldLabel: 'Email',
                name: 'email',
                action: 'clickRdio',
                regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            },
            {
                xtype: 'combo',
                fieldLabel: 'Defect',
                id: 'Defect',
                name: 'defectNo',
                store: 'mjrdefectStr',
                displayField: 'mjrDefectDesc',
                valueField: 'mjrDefectCode',
                queryMode: 'local'
            },
            {
                xtype: "textfield",
                id: 'txtAcc',
                name: 'assessoriesRetained',
                fieldLabel: 'Accessories',
                action: 'clickRdio',
                allowBlank: true
            },

            {
                xtype: 'label',
                html: '__________________________________________________________________________________'
            },
            {
                xtype: 'combo',
                fieldLabel: 'Service Agent',
                id: 'DistributorShop',
                //name: 'serviceCenter',
                name:'serviceCenterId',
                store: 'servicestr',
//                store: 'distributorstr',
                displayField: 'bisName',
                valueField: 'bisId',
                queryMode: 'remote',
                listeners: {
                    change: function (cmp, newValue, oldValue) {
                      console.log('My Val - change:::: ', cmp.getValue());
                    },
                    select: function (cmp, newValue, oldValue) {
                        console.log(newValue);
                        var recordsOfRow = cmp.findRecordByValue(newValue);
                        var index = newValue[0]['index'];
                        console.log("index", newValue);
                        var store = Ext.getStore("servicestr");

//                           console.log('store', store.findRecord('bisName'));
                        var address = store.data.items[index].data['address'];

                        Ext.getCmp('addrs').setValue(address);
                        console.log('My Val:::: ', cmp.getValue());
                    },
                    load: function (store, records, success) {
                        console.log(records);
                    }
                }

            },
//            {
//                fieldLabel: 'Service Agent',
//                xtype: 'combobox',
//                name: 'bisName',
//                emptyText: 'Select Service Agent',
//                store: 'servicestr',
//                displayField: 'bisName',
//                valueField: 'bisId',
//                afterLabelTextTpl: "",
//                queryMode: 'local'
//            },
//            {
//                fieldLabel: 'Service Agent',
//                xtype: 'combobox',
//                name: 'bisName',
//                emptyText: 'Select Service Agent',
//                store: 'servicestr',
//                displayField: 'bisName',
//                valueField: 'bisId',
//                afterLabelTextTpl: "",
//                queryMode: 'local'
//            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Service Agent Address',
                name: "serviceCenterAddress",
                afterLabelTextTpl: "",
                allowBlank: false,
                id: 'addrs'
            },
            {
                xtype: 'textareafield',
                fieldLabel: 'Remarks',
                id: 'Remarks',
                name: 'remarks',
                allowBlank: false,
                regex: /^[a-zA-Z_ 0-9\,'']+$/
            },
            {
                xtype: 'hiddenfield',
                name: 'bisId',
                id: 'hdnQBisId'
            },
            {
                xtype: 'hiddenfield',
                name: 'paymentType',
                id: 'quick-wo-pattype',
                value: 1
            },



//


//

//            {
//                xtype: 'grid',
//                frame: false,
//                id: 'igrid',
//                store: 'DeviceIssueIme',
//                border: false,
//                columns: [
//                    {xtype: 'actioncolumn',
//                        text: '',
//                        align: 'center',
//                        flex: 1,
//                        items: [{
//                                icon: 'resources/images/delete-icon.png',
//                                //action: 'remDevIsuItem',
//                                tooltip: 'Remove',
//                                handler: function(grid, rowIndex, colIndex) {
//                                    var rec = grid.getStore().getAt(rowIndex);
//                                    var me=this;
//                                    Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function(button) {
//                                        if (button == 'yes') {
//                                            grid.getStore().removeAt(rowIndex);
//                                            me.up('window').fireEvent('remDevIsuItem');
//                                        } else {
//                                            return false;
//                                        }
//                                    });
//
//                                }
//                            }]
//
//                    }
//                    
//                    
//                ]
////                 viewConfig: {
//////                    emptyText: 'Click a button to show a dataset',
////                    deferEmptyText: false,
////                    stripeRows: false,
////                    loadMask: false
////                }
//            }
        ];
    },
    buildButtons: function () {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newquickorder-save',
                formBind: true,
                hidden: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newquickorder-create',
                formBind: true,
                hidden: false
            }
//            {
//                xtype: 'button',
//                text: 'test',
//                handler: function(me) {
//                    ////console.log(this);
//                }
//            }
        ];
    },
    buildDockedItems: function () {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
//                id: 'userform-reset-insert'
                    }, {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newquickorder-save',
                        formBind: true,
                        hidden: true
//                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
//                        ui: 'success',
                        action: 'create',
                        id: 'newquickorder-create',
                        formBind: true,
                        hidden: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function () {
        return [];
    }

});
