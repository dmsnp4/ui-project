Ext.define('singer.view.ServiceModule.QuickWorkOrders.Form', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.QuickWorkOrdersGrid',
    height: 300,
    width: 1000,
    layout: 'fit',
    store: 'QuickWorkOrder',
    forceFit: true,
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    },
                    '->',
                    {
                        text: 'Add Quick Work Order',
                        action: 'open_window'
                                //id: 'printpaybtn'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'QuickWorkOrder',
                dock: 'bottom',
                displayInfo: true
            }
        ];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'Work Order No',
                dataIndex: 'workOrderNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Imei',
                dataIndex: 'imeiNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'product'
//                renderer: function (value) {
//                    console.log('value',value);
//                    if (value === '0') {
//                        return "Out Of Warranty";
//                    }
//                    else if (value === '1') {
//                        return "Under Warranty";
//                    } else {
//                        return "-";
//                    }
//                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modleDesc'
            },
            {
                xtype: 'searchablecolumn',
                text: 'NIC',
                dataIndex: 'customerNic'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Customer Name',
                dataIndex: 'customerName'
            },
            //New add grid coloumns
            {
                xtype: 'searchablecolumn',
                text: 'Contact Number',
                dataIndex: 'workTelephoneNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Email',
                dataIndex: 'email'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Defect',
                dataIndex: 'defectNo'
            },
//            {
//                xtype: 'searchablecolumn',
//                text: 'Customer Complain',
//                dataIndex: 'invoice'
//            },
            {
                xtype: 'searchablecolumn',
                text: 'Accesorries',
                dataIndex: 'assessoriesRetained'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Service Agent',
                dataIndex: 'serviceCenter'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Service Agent Address',
                dataIndex: 'serviceCenterAddress'
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                header: 'View',
                align: 'center',
                //width: 100,
                flex: 1,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('viewinfo', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'Edit ',
                menuDisabled: true,
                items: [{
//                        icon: 'resources/images/edit.png',
//                        tooltip: 'Edit',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('quickedit', grid, rec);
//                            console.log(this.up('grid'));
//                        },
//                        isDisabled: function (view, rowIndex, colIndex, item, record) {
//                        if ((record.get('status') === 2)||(record.get('status') === 3)) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('quickedit', grid, record);
//                            ////console.log(this.up('grid'));
                        }


                    }]
            },
            {
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'Print Receipt',
                width: 100,
                items: [{
                        icon: 'resources/images/details.png', // Use a URL in the icon config
                        tooltip: 'Receipt',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('qwoReport', grid, record);
                            //                            //console.log(this.up('grid'));
                        }
//                        isDisabled: function(view, rowIndex, colIndex, item, record) {
//                            // Returns true if 'editable' is false (, null, or undefined)
//                            if ((record.get('status') === 2)||record.get('status') === 3) {
//                                return false;
//                            } else {
//                                return true;
//                            }
//                        }
                    }]
            }
//            {
//                xtype: 'actioncolumn',
//                width: 100,
//                text: 'Delete ',
//                menuDisabled: true,
//                items: [{
//                        icon: 'resources/images/delete-icon.png',
//                        tooltip: 'Delete',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('on_open_delete', grid, rec);
//                        },
//                        isDisabled: function (view, rowIndex, colIndex, item, record) {
//                        if (record.get('status') === 1) {
//                            return false;
//                        } else {
//                            return true;
//                        }
//                    }
//                    }]
//            }
        ];
    }


});





