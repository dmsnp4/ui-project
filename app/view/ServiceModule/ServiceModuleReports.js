

Ext.define('singer.view.ServiceModule.ServiceModuleReports', {
    extend: 'Ext.container.Container',
    alias: 'widget.serviceMReports',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    overflowY: 'auto',
    initComponent: function () {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.buildMasterReportsContainer()
        ];
        inquiryCmp.callParent();
    },
    buildMasterReportsContainer: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": '1',
                    "value": "Analyze"
                },
                {
                    "status": '2',
                    "value": "Viewable"
                }
            ]
        });

        return Ext.create('Ext.panel.Panel',
                {
                    title: "Service Reports",
                    layout: {type: 'vbox', align: 'stretch', pack: 'start'},
                    items: [{
                            xtype: 'form',
                            flex: 1,
                            padding: '0',
                            overflowY: 'auto',
                            border: false,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            margin: '20 10 20 200',
                                            xtype: 'datefield',
                                            fieldLabel: 'Start Date',
                                            id: 'startDate_service_reports',
                                            name: '',
                                            allowBlank: false,
                                            value: new Date(),
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            altFormats: 'm/d/Y',
                                            editable: false
                                        },
                                        {
                                            margin: '20 10 20 200',
                                            xtype: 'datefield',
                                            fieldLabel: 'End Date',
                                            name: '',
                                            id: 'endDate_service_reports',
                                            allowBlank: false,
                                            value: new Date(),
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            altFormats: 'm/d/Y',
                                            editable: false
                                        },
                                        {
                                            margin: '20 10 20 200',
                                            width: 600,
                                            xtype: 'treecombo',
                                            fieldLabel: 'Select Location',
                                            store: 'ServiceUserType',
                                            treeStore: 'UserTypeListTree', //tree store is ok
                                            valueField: 'bisId',
                                            displayField: 'bisName',
                                            name: 'location_struct',
                                            id: 'service_location_struct_id'//sh, catch this @controller
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Defect Codes Repair Report',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'W_Nrpt'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Spare Parts Usage '
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'sparePrtDelay'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            margin: '30 0 30 200',
                                            xtype: 'label',
                                            text: 'FFR Rate Report',
                                        },
                                        {
                                            margin: '30 10 30 0',
                                            xtype: 'combo',
                                            id: 'FFR_value',
                                            width: 200,
                                            store: 'modStore',
                                            queryMode: 'remote',
                                            displayField: 'model_Description',
                                            valueField: 'model_No'
                                        },
                                        {
                                            margin: '30 0 30 10',
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'ffrReport'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Franchise Payment WO Not Send Summary Report',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'frnchise_payment_not_summary'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Franchise Payment WO Send Summary Report'
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'frnchise_payment_summary'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Un-Attendent Quick work order Report',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'quickWrkOrder'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Repeat Repair Report'
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'repairRpt'
                                        }]
                                },
                                //----------------------
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        labelWidth: 160,
                                        width: 170
                                    },
                                    items: [
                                        {
                                            margin: '30 0 30 200',
                                            xtype: 'label',
                                            text: 'Repair Report'
                                        },
                                        {
                                            margin: '30 10 30 0',
                                            xtype: 'combobox',
                                            id: 'rep_type',
                                            width: 200,
                                            store: stat,
                                            queryMode: 'local',
                                            displayField: 'value',
                                            valueField: 'status'
                                        },
                                        {
                                            margin: '30 10 30 0',
                                            width: 200,
                                            xtype: 'treecombo',
                                            //fieldLabel: 'Select Location',
                                            store: 'ServiceUserType',
                                            treeStore: 'UserTypeListTree', //tree store is ok
                                            valueField: 'bisId',
                                            displayField: 'bisName',
                                            name: 'location_struct',
                                            id: 'location_struct_id_rep',
                                            listeners: {
                                                change: function (comp, rec) {
                                                    console.log(comp, rec);
                                                    //comp.up('window').fireEvent('onChangeTreeCombo', comp, rec);
                                                }
                                            }//sh, catch this @controller
                                        },
                                        {
                                            margin: '30 0 30 10',
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'rp_report'
                                        }]
                                },
                                //--------------------
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Franchise Payment Report',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'franchiseRpt'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Delay Date Report'
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'delay_date'
                                        }]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Exchange Report'
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'exchange_report'
                                        }]
                                }
                            ]
                        }
                    ]
                });
    }


});
