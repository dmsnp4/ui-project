Ext.define('singer.view.ServiceModule.ExchangeRefference.ViewExchange', {
    extend: 'Ext.window.Window',
    alias: 'widget.exchangeView',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    height: "70%",
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {

        return [
            {
                fieldLabel: 'Exchange Refference No:',
                name: 'exchangeReferenceNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Work Order:',
                name: 'workOrderNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'IMEI No:',
                name: 'imeiNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Reason:',
                name: 'reasonDesc',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Type Of Exchange:',
                name: 'exchangeDesc',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Customer Name:',
                name: 'customerName',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Customer NIC:',
                name: 'customerNIC',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Product:',
                name: 'product',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Brand:',
                name: 'brand',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Model:',
                name: 'modelDesc',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Date Of Sale:',
                name: 'warrantyRegDate',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Warrenty Status:',
                name: 'warrantyDesc',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Exchanged Date:',
                name: 'exchangeDate',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }
                    }
                }
            }
        ];
    },
    buildButtons: function() {
        return ['->', {
                text: 'Close',
                action: 'cancel'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});






