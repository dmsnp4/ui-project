Ext.define('singer.view.ServiceModule.ExchangeRefference.ExchangeReferenceForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.exchangeReferenceForm',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    title: 'Add New Exchange Reference',
    width: 500,
    height: "80%",
    initComponent: function () {
        var exchangeRefForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false
            },
            items: exchangeRefForm.buildItems(),
            buttons: exchangeRefForm.buildButtons()
        });
        exchangeRefForm.items = [formPanel];
        exchangeRefForm.callParent();
    },
    buildItems: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 2, "value": "Cancel"},
                {"status": 1, "value": "Pending"}
                //                {"status": 3, "value": "Unconfirmed"}
            ]
        });
        return [
            {
                xtype: 'displayfield',
                name: 'exchangeReferenceNo',
                fieldLabel: 'Exchange Reference No',
                hidden: true,
                listeners: {
                    afterrender: function (comp) {
                        comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                    }
                }
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Work Order:',
                name: 'workOrderNo',
                readOnly: true,
                id: "exchangeRefWo"
//                queryMode: 'remote',
//                listeners: {
//                    change: function (combo, newValue) {
//                        //console.log('change');
//                        //if (Ext.getCmp("exchangeReferenceForm-save").isVisible() === false) {
//                        this.up('window').fireEvent('wo_change', combo, newValue);
//                        //}
//                    }
//                }
            },
            {
                xtype: 'button',
                text: 'Select Work Order Number',
                id : "exchangeRefSelectWoBtn",
                width : "100%",
                action: 'select_Work_Order',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false
            },
            {fieldLabel: 'IMEI No:', id: "exchangeRefImei", name: 'imeiNo', allowBlank: true},
            {fieldLabel: 'Customer Name:', id: "exchangeRefCustomerName", name: 'customerName', allowBlank: true},
            {fieldLabel: 'Customer NIC:', name: 'customerNIC', id: "exchangeRefCustomerNic", allowBlank: true},
            {fieldLabel: 'Product:', name: 'product', id: "exchangeRefProduct", allowBlank: true},
            {fieldLabel: 'Brand:', name: 'brand', id: "exchangeRefBrand", allowBlank: true},
            {fieldLabel: 'Model:', name: 'modelDesc', id: "exchangeRefModel", allowBlank: true},
            {fieldLabel: 'Date Of Sale:', name: 'warrantyRegDate', id: "exchangeRefDateOfSale", allowBlank: true},
            {fieldLabel: 'Warranty Status:', name: 'warrantyDesc', id: "exchangeRefWarrantyDesc", allowBlank: true},
            {
                xtype: 'combo',
                fieldLabel: 'Reason:',
                name: 'reason',
                store: 'exchngeReason',
                displayField: 'description',
                queryMode: 'remote',
                valueField: 'code'
            },
            {
                xtype: 'combo',
                fieldLabel: 'Type Of Exchange:',
                name: 'typeOfExchange',
                store: 'exchngeType',
                displayField: 'description',
                queryMode: 'remote',
                valueField: 'code'
            },
//            {
//                xtype: 'combo',
//                fieldLabel: 'Select shop:',
//                name: 'bisId',
//                store: 'distributorstr',
//                displayField: 'bisName',
//                queryMode: 'remote',
//                valueField: 'bisId'
//            },
            {
                fieldLabel: 'Status',
                xtype: 'combobox',
                name: 'status',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                editable: false
            },
//            {
//                fieldLabel: 'Exchange Date',
////                name: 'estimateClosingDate',
//                xtype: 'datefield',
//                format: 'Y-m-d',
//                altFormats: 'm/d/Y',
//                editable: false,
//                allowBlank: false,
//                minValue:new Date(),
//            },
            {
                xtype: 'textareafield',
                grow: true,
                fieldLabel: 'Remarks:',
                name: 'remarks',
                allowBlank: true,
                afterLabelTextTpl: ''
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'The handset is within the warrenty period',
                name: 'wrtPeriodFlag',
                inputValue: 1
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'The IMEI no of the warrenty card is matching with that of the hand set',
                name: 'wrtImeiFlag',
                inputValue: 1
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'There is no indication of an unauthorized repair',
                name: 'wrtDamageFlag',
                inputValue: 1
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'There is no indication of physically damaged(Inside/Outside)',
                name: 'unAuthRepairFlag',
                inputValue: 1
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'The handset is not water damaged',
                name: 'waterDamage',
                inputValue: 1
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'The reported problem by the customer and diagnosed by the engineer, reproduciable',
                name: 'reprodicbleFlag',
                inputValue: 1
            },
            {
                xtype: 'hiddenfield',
                name: 'bisId',
                id: 'ext-bisId'
            }

        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            },
            '->',
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                id: 'exchangeReferenceForm-create',
                formBind: true,
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'exchangeReferenceForm-save',
                formBind: true,
                hidden: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});


