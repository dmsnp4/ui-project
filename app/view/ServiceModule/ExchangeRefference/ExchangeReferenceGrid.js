Ext.define('singer.view.ServiceModule.ExchangeRefference.ExchangeReferenceGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.exchangeReferenceGrid',
    loadMask: true,
    store: 'ExchangeReferences',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var grid = this;
        grid.columns = grid.buildColumns();
        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->',
                    {
                        text: 'Add New',
                        action: 'add_new'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'ExchangeReferences',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function () {

        return [
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Exchange Refferens No',
                dataIndex: 'exchangeReferenceNo',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Work Order',
                dataIndex: 'workOrderNo',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'IMEI No',
                dataIndex: 'imeiNo',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'ERP No',
                dataIndex: 'erpCode',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Reason',
                dataIndex: 'reasonDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            
             {
                width: 100,
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'status',
                renderer: function (value) {
                    if (value===1) {
                        return "Pending";
                    }else if(value===2){
                        return 'Cancel';
                    }else if(value===3){
                        return 'Approved';
                    } else {
                        return 'Reject';
                    }
                }
            }, 
            
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Type Of Exchange',
                dataIndex: 'exchangeDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Customer Name',
                dataIndex: 'customerName',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Customer NIC',
                dataIndex: 'customerNIC',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'product',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Brand',
                dataIndex: 'brand',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modelDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Date Of Sale',
                dataIndex: 'warrantyRegDate',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Warrenty Status',
                dataIndex: 'warrantyDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Exchanged Date',
                dataIndex: 'exchangeDate',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'View ',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/view-icon.png',
                    tooltip: 'View ',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('onView', grid, rec);
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Edit ',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/edit.png',
                    tooltip: 'Edit',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('onEdit', grid, rec);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        // Returns true if 'editable' is false (, null, or undefined)
                        if (record.get('status') === 3) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Delete ',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/delete-icon.png',
                    tooltip: 'Delete',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('ondelete', grid, rec);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        // Returns true if 'editable' is false (, null, or undefined)
                        if (record.get('status') === 3) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 150,
                text: 'Print Receipt ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/printerr.png',
                        tooltip: 'Print Receipt',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('on_exchange_print', grid, rec);
                        }
                    }]
            }

        ];
    }
});