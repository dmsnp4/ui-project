Ext.define('singer.view.ServiceModule.ExchangeRefference.ApproveDeviceExchange', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ApproveDeviceExchange',
    loadMask: true,
    store: 'ApproveDeviceExchangeStore',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var grid = this;
        grid.columns = grid.buildColumns();
        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->',
                    {
                        text: 'Add New',
                        action: 'add_new'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'ApproveDeviceExchangeStore',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function () {

        return [
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Exchange Refferens No',
                dataIndex: 'exchangeReferenceNo',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Work Order',
                dataIndex: 'workOrderNo',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'IMEI No',
                dataIndex: 'imeiNo',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'ERP No',
                dataIndex: 'erpCode',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Reason',
                dataIndex: 'reasonDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            
             {
                width: 100,
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'status',
                renderer: function (value) {
                    if (value===1) {
                        return "Pending";
                    }else if(value===2){
                        return 'Cancel';
                    }else if(value===3){
                        return 'Approved';
                    } else {
                        return 'Rejected';
                    }
                }
            }, 
            
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Type Of Exchange',
                dataIndex: 'exchangeDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Customer Name',
                dataIndex: 'customerName',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Customer NIC',
                dataIndex: 'customerNIC',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'product',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Brand',
                dataIndex: 'brand',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modelDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Date Of Sale',
                dataIndex: 'warrantyRegDate',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Warrenty Status',
                dataIndex: 'warrantyDesc',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 200,
                xtype: 'searchablecolumn',
                text: 'Exchanged Date',
                dataIndex: 'exchangeDate',
                renderer: function (value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Approve',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/approved.png',
                    tooltip: 'Approve',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('onApprove', grid, rec);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        // Returns true if 'editable' is false (, null, or undefined)
                        if (record.get('status') === 1) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Reject',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/deactivate.png',
                    tooltip: 'Reject',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('onReject', grid, rec);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        // Returns true if 'editable' is false (, null, or undefined)
                        console.log('LLLLLLLLLL: ', record.get('status'));
                        if (record.get('status') === 1) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    }]
            },
           

        ];
    }
});