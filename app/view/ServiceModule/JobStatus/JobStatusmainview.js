


Ext.define('singer.view.ServiceModule.JobStatus.JobStatusmainview', {
    extend: 'Ext.container.Container',
    alias: 'widget.statusMainview',
    require : ['Ext.MessageBox'],
    layout: {
        type: 'hbox',
    },
    overflowY: 'auto',
    initComponent: function () {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.WorkOrderOpenContainer(),
            this.maintainController(),
            this.WOclosecontaner(),
        ];
        inquiryCmp.callParent();
    },
    WorkOrderOpenContainer: function () {
        return Ext.create('Ext.panel.Panel',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    flex: 1,
                    items: [
                        {
                            title: 'WO open status',
                            xtype: 'form',
                            id:'basicDetailsForm',
                            padding: '0',
                            autoScroll: true,
                            overflowY: 'auto',
                            border: 0,
                            items: [
//                        {
//                            xtype: 'combo',
//                            fieldLabel: 'WO Number',
//                            id:'wORDERn',
//                            name: 'workOrderNo',
//                            allowBlank: false,
//                            store: 'wrkordern',
//                            displayField: 'workOrderNo',
//                            valueField: 'workOrderNo',
//                            queryMode: 'remote',
//                            margin: '10 10 10 10',
//                            width: 300,
//                            listeners:{
//                                  beforequery: function (record) {
//                                  record.query = new RegExp(record.query, 'i');
//                                  record.forceAll = true;
//                                }
//                            }
//                        },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'WO No:',
                                    margin: '10 10 10 10',
                                    name: 'job_status_WO_field_name',
                                    id: 'job_status_WO_field'
                                },
                                {
                                    margin: '10 0 10 20',
                                    xtype: 'button',
                                    id:'selectWoBtn1',
                                    text: 'Select Work Order',
                                    action: 'select_Work_Order',
                                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'IMEI No:',
                                    margin: '10 10 10 10',
                                    name: '',
                                    id: 'job_status_IMEI_field'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'RCC Ref No:',
                                    margin: '10 10 10 10',
                                    name: '',
                                    id: 'job_status_RCC_field'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'NIC No:',
                                    margin: '10 10 10 10',
                                    name: '',
                                    id: 'job_status_NIC_field'
                                },
                                
                                {
                                    xtype: 'panel',
                                    dock: 'top',
                                    
                                    items:[{
                                     xtype: 'label',
                                        forId: 'workIdLbl',
                                        id:'workIdLbl',
                                        name: 'workOrderNo',
                                        text:'',
                                        style:'display : block; padding : 10px 10px 10px 100px'
                                        //margins: '50 10 10 10',
                                    },
                                    {
                                   // style: 'background-color: #DCDCDC;'
                                    
                                            //dock: 'top',

                                            buttons: [
                                                {
                                                    text: 'View',
                                                    margin: '10 10 10 10',
                                                    action: 'view_job_status',
                                                    loadMask: true
                                                },
                                                '->',
                                                {
                                                    text: 'Clear',
                                                    action: 'clear_search'
                                                            //id: 'printpaybtn'
                                                }

                                            ]
                                    }]
                                        
                                },
//                        {
//                            xtype:'button',
//                            text:'View',
//                            margin: '10 10 10 10',
//                            action:'view_job_status',
//                            width:'250px'
//                        },
                                {
                                    xtype: 'fieldset',
                                    title: 'Customer Details',
                                    //collapsible: true,
                                    overflowY: 'auto',
                                    overflowX: 'auto',
                                    margin: '5 5 5 5',
                                    defaults: {
                                        xtype: 'label',
                                        labelAlign: 'left',
                                        beforeLabelTextTpl: '<b>',
                                        afterLabelTextTpl: '</b>',
                                        margin: '10 20 20 5',
                                        style: 'display:block'
                                    },
                                    items: [
                                        {xtype: 'textfield', fieldLabel: 'Name:', name: 'customerName', readOnly: true,id: 'name_field'},
                                        {xtype: 'textareafield', fieldLabel: 'Address:', name: 'customerAddress', readOnly: true,id: 'address_field'},
                                        {xtype: 'textfield', fieldLabel: 'NIC:', name: 'customerNic', readOnly: true,id: 'nic_field'},
                                        {xtype: 'textfield', fieldLabel: 'Telephone:', name: 'customerTelephoneNo', readOnly: true,id: 'telephone_field'},
                                        {xtype: 'textfield', fieldLabel: 'Email:', name: 'email', readOnly: true,id: 'email_field'}
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    title: 'Product Details',
                                    margin: '5 5 5 5',
                                    collapsible: true,
                                    defaults: {
                                        xtype: 'label',
                                        labelAlign: 'left',
                                        beforeLabelTextTpl: '<b>',
                                        afterLabelTextTpl: '</b>',
                                        labelWidth: 120,
                                        margin: '10 20 20 5',
                                        style: 'display:block'
                                    },
                                    items: [
                                        {xtype: 'textfield', fieldLabel: 'IMEI:', name: 'imeiNo', readOnly: true,id: 'imei_field'},
                                        {xtype: 'textfield', fieldLabel: 'Product:', name: 'product', readOnly: true,id: 'product_field'},
                                        {xtype: 'textfield', fieldLabel: 'Brand:', name: 'brand', readOnly: true,id: 'brand_field'},
                                        {xtype: 'textareafield', fieldLabel: 'Model:', name: 'modleDesc', readOnly: true,id: 'model_field'},
                                        {xtype: 'textfield', fieldLabel: 'Acc retained:', name: 'accesoriesRetained', readOnly: true,id: 'acc_retained_field'},
                                        //{xtype: 'textfield', fieldLabel: 'Defect:', name: 'woDefect', readOnly: true,id: 'defect_field'},
                                        {xtype: 'textfield', fieldLabel: 'Defect:', name: 'defects', readOnly: true,id: 'defect_field'},
                                        {xtype: 'textfield', fieldLabel: 'RCC ref no:', name: 'rccReference', readOnly: true,id: 'rcc_retained_field'},
                                        {xtype: 'textareafield', fieldLabel: 'Dist shop:', name: 'distShop', readOnly: true,id: 'dist_shop_field'},
                                        {xtype: 'textfield', fieldLabel: 'Date Of Sale:', name: 'dateOfSale', readOnly: true,id: 'date_sale_field'},
                                        {xtype: 'textareafield', fieldLabel: 'Remarks:', name: 'remark', readOnly: true,id: 'remark_field'}
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    title: 'Warranty Details',
                                    collapsible: true,
                                    margin: '5 5 5 5',
                                    defaults: {
                                        xtype: 'label',
                                        labelAlign: 'left',
                                        beforeLabelTextTpl: '<b>',
                                        afterLabelTextTpl: '</b>',
                                        labelWidth: 120,
                                        margin: '10 0 10 0',
                                        style: 'display:block'
                                    },
                                    items: [
                                        {xtype: 'textfield', fieldLabel: 'Warranty Type:', name: 'warrantyType', readOnly: true,id: 'warranty_type_field'},
                                        {xtype: 'textfield', fieldLabel: 'Received Date:', name: 'createDate', readOnly: true,id: 'received_date_field'},
                                        {xtype: 'textfield', fieldLabel: 'Non warr type:', name: 'nonWarrentyVrifType', readOnly: true,id: 'non_war_field'},
                                        {xtype: 'textareafield', fieldLabel: 'Non warr remarks:', name: 'nonWarrantyRemarks', readOnly: true,id: 'non_war_remark_field'},
                                        {xtype: 'textareafield', fieldLabel: 'Cust complain:', name: 'customerComplain', readOnly: true,id: 'cus_complain_field'}

//                                        

                                    ]
                                }
                            ]
                        }
                    ]

                });
    },
    maintainController: function () {
        return Ext.create('Ext.panel.Panel',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start',
                    },
                    flex: 1,
                    items: [
                        {
                            title: 'WO Maintain Status',
                            xtype: 'form',
                            id: 'maintainForm',
                            padding: '0',
                            overflowY: 'auto',
                            border: 0,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'vbox',
                                    defaults: {
                                        xtype: 'textfield',
                                        margin: '10 10 10 10',
                                        labelWidth: 150,
                                        width: 300
//                                flex:1,
                                    },
                                    flex: 1,
                                    items: [
                                        {
                                            fieldLabel: 'Technician',
                                            name: 'technician',
                                            id:'tech'
                                        },
                                        {
                                            xtype : 'textareafield',
                                            fieldLabel: 'Action Taken',
                                            name: 'actionTaken',
                                            id:'actionTaken'
                                        },
                                        {   
                                            fieldLabel: 'Invoice',
                                            name: 'invoiceNo',
                                            id:'invoiceNo'
                                        },
                                        {
                                            xtype : 'textareafield',
                                            fieldLabel: 'WO Maintenance Remarks',
                                            name: 'remark2',
                                            id:'remark2'
                                        },
                                        
                                        
                                        {
                                            xtype: 'grid',
                                            store: 'jobDefect',
                                            id: 'defctGrid',
                                            columns: [
                                                {
                                                    text: 'Major Defect',
                                                    dataIndex: 'majDesc',
                                                    flex: 2,                                                    
                                                    cellWrap: true,
                                                    //width: 200,
                                                    autoSizeColumn: true
                                                },
                                                {
                                                    text: 'Minor Defect',
                                                    dataIndex: 'minDesc',
                                                    cellWrap: true,
                                                    flex: 1,
                                                    width: 200,
                                                    autoSizeColumn: true
                                                }
                                            ]
                                           // rows: []
                                        },
                                        {
                                            xtype: 'gridpanel',
                                            store: 'jobParts',
                                            id: 'prtsGrid',
                                            columns: [
                                                {
                                                    text: 'Spare Part',
                                                    dataIndex: 'partDesc',
                                                    //flex: 1,
                                                    width: 200
                                                },
                                                {
                                                    text: 'Price',
                                                    dataIndex: 'partSellPrice',
                                                    flex: 1,
                                                    //width: 200
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'gridpanel',
                                            store: 'jobestimate',
                                            id: 'jbestimategrid',
                                            columns: [
                                                {
                                                    text: 'Estimate No',
                                                    dataIndex: 'esRefNo',
                                                   // flex: 1,
                                                   // width: 200,
                                                    autoSizeColumn: true
                                                },
                                                {
                                                    text: 'Staus',
                                                    dataIndex: 'statusMsg',
                                                    //flex: 1,
                                                    width: 200,
                                                    autoSizeColumn: true
                                                }
//                                    {
//                                        text: 'Details',
//                                        dataIndex: '',
//                                        flex: 1
//                                    }
                                            ]
                                        },
//                                {
//                                        xtype: 'fieldset',
//                                        title: 'Estimate Details',
//                                        margin: '5 5 5 5',
//                                        collapsible: true,
//                                        defaults:{
//                                            xtype:'label',
//                                            labelAlign: 'left',
//                                            beforeLabelTextTpl: '<b>',
//                                            afterLabelTextTpl: '</b>',
//                                            labelWidth: 120,
//                                            margin: '10 20 20 5',
//                                            style:'display:block'
//                                        },
//                                         items: [
//                                                    {xtype:'textfield',fieldLabel:'Estimated Closing Day',name:'estClosingDate',readOnly: true},
//                                                    {xtype:'textfield',fieldLabel:'Estimate Details',name:'',readOnly: true},
//                                                    {xtype:'textfield',fieldLabel:'Estimate No:',name:'',readOnly: true},
//                                                    {xtype:'textfield',fieldLabel:'Estimate status',name:'',readOnly: true}
//                                                 ]
//                                 },
                                        {
                                            fieldLabel: 'Repair Level',
                                            name: 'repairLevelName',
                                            readOnly: true,
                                            id:'repair_level_field'
                                        },
                                        {
                                            fieldLabel: 'Repair Price',
                                            name: 'repairAmount',
                                            readOnly: true,
                                            id:'repair_amount_field'
                                        },
                                        {
                                            fieldLabel: 'Repair Status',
                                            name: 'repairStatusMsg',
                                            readOnly: true,
                                            id:'repair_status_field'
                                        },
//                                {
//                                  fieldLabel:'Estimated closing Date',
//                                  name:'estClosingDate',
//                                  readOnly: true
//                                },
                                        {
                                            fieldLabel: 'Total cost',
                                            name: 'totalCost',
                                            readOnly: true,
                                            id:'total_cost_field'
                                        }
                                    ]}
                            ]
                        }
                    ]
                });
    },
    WOclosecontaner: function () {
        return Ext.create('Ext.panel.Panel', {
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start',
            },
            flex: 1,
            items: [
                {
                    title: 'WO Close Status',
                    xtype: 'form',
                    id: 'closeForm',
                    padding: '0',
                    overflowY: 'auto',
                    border: 0,
                    items: [
                        {
                            xtype: 'fieldcontainer',
                            layout: 'vbox',
                            defaults: {
                                xtype: 'textfield',
                                margin: '10 10 10 10',
                                labelWidth: 150,
//                                width: 300
                                flex: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Deliver Type',
                                    name: 'deliveryType',
                                    readOnly: true,
                                    id:'deliver_type_field'
                                },
                                {
                                    fieldLabel: 'Transfer Location',
                                    xtype : "textareafield",
                                    name: 'transferLocation',
                                    readOnly: true,
                                    id:'transfer_location_field'
                                },
                                {
                                    fieldLabel: 'Courier No',
                                    name: 'courierNo',
                                    readOnly: true,
                                    id:'courier_no_field'
                                },
                                {
                                    fieldLabel: 'Gate pass',
                                    name: 'gatePass',
                                    readOnly: true,
                                    id:'gate_pass_field'
                                },
                                {
                                    fieldLabel: 'Deliver date',
                                    name: 'deliveryDate',
                                    readOnly: true,
                                    id:'delivery_date_field'
                                }
                            ]}
                    ]
                }
            ]
        });
    }
    
    
    
    
});