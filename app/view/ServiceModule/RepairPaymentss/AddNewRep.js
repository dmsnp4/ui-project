Ext.define('singer.view.ServiceModule.RepairPaymentss.AddNewRep', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.addnewrep',
    height: 300,
    width: 850,
    layout: 'fit',
    store: 'RepairPaymentWithPrice', //WorkOrderOpening',
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'RepairPaymentWithPrice',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildColumns: function () {
        
        return [
            {
                xtype: 'actioncolumn',
                header: 'Select',
                id:'selectActionCol',
                width: 100,
                items: [{
                        icon: 'resources/images/Select.png', // Use a URL in the icon config
                        tooltip: 'click to select Work Order',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('selectWrkOrder', grid, record,rowIndex);
                        }
                    }]
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Work Order No',
                dataIndex: 'workOrderNo'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Warranty Verification',
                dataIndex: 'warrentyTypeDesc'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'IMEI',
                dataIndex: 'imeiNo'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Defect',
                dataIndex: 'defectDesc'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: ' Invoice No',
                dataIndex: 'invoice'
            },
            
        ];
    }


});