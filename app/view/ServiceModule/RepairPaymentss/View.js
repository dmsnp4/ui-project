Ext.define('singer.view.ServiceModule.RepairPaymentss.View', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.viewRepairPayments',
    height: 300,
    width: 1000,
    layout: 'fit',
    store: 'RepairPaymentDetl',
    forceFit: true,
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    },
                    '->',
                    {
                        text: 'Print Payment Memo',
                        action: 'print_payment_memo',
                        id: 'printpaybtn'
                    },
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'RepairPaymentDetl',
                dock: 'bottom',
                displayInfo: true
            }
        ];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'WorkOrder No',
                dataIndex: 'workOrderNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Warranty Verification',
                dataIndex: 'warrantyVerifType',
                renderer: function (value) {
                    console.log('value',value);
                    if (value === '0') {
                        return "Out Of Warranty";
                    }
                    else if (value === '1') {
                        return "Under Warranty";
                    } else {
                        return "-";
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'IMEI',
                dataIndex: 'imeiNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Defect',
                dataIndex: 'defectDesc'
            },
            {
                xtype: 'searchablecolumn',
                text: ' Invoice No',
                dataIndex: 'invoice'
            }
        ];
    }


});


