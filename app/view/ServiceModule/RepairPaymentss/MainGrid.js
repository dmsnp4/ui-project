Ext.define('singer.view.ServiceModule.RepairPaymentss.MainGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.repairPayGrid',
    store: 'WOList',
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },
    buildColumns: function () {
        return [
            {
                xtype: 'searchablecolumn',
                header: 'Payment ID',
                dataIndex: 'paymentId',
                width: 100,
            },
            {
                xtype: 'searchablecolumn',
                header: 'Payment Total',
                dataIndex: 'amount',
                width: 100,
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                header: 'Ref Date',
                dataIndex: 'paymentDate',
                width: 150,
            },
            {
                xtype: 'searchablecolumn',
                header: 'Purchase Order',
                dataIndex: 'poNo',
                width: 150,
            },
            {
                xtype: 'searchablecolumn',
                header: 'Purchase Order Date',
                dataIndex: 'poDate',
                width: 150,
            },
            {
                xtype: 'searchablecolumn',
                header: 'Cheque No',
                dataIndex: 'chequeNo',
                width: 150,
            },
            {
                xtype: 'searchablecolumn',
                header: 'Cheque Date',
                dataIndex: 'chequeDate',
                width: 150,
//                renderer: function (value) {
//                    if (value === '') {
//                        return "-";
//                    }
//                }
            },
            {
                xtype: 'searchablecolumn',
                header: 'Status',
                dataIndex: 'status',
                width: 100,
                renderer: function (value) {
                    //console.log(value);
                    if (value === 1) {
                        return "Pending";
                    }
                    else if (value === 5) {
                        return "Paid";
                    } else {
                        return "Approved";
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                header: 'View',
                align: 'center',
                width: 150,
                flex: 1,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('view', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'Print',
                align: 'center',
                width: 150,
                flex: 1,
                items: [{
                        icon: 'resources/images/printerr.png', // Use a URL in the icon config
                        tooltip: 'Print',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('print', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            }

        ];
    },
    buildDockedItems: function () {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }
                    , '->',
//                    {
//                        iconCls: 'icon-search',
//                        text: 'Print Payment Summary',
//                        action: 'print_payment_summary',
//                    },
                    {
                        iconCls: 'icon-search',
                        text: 'Repair Payment',
                        action: 'repair_payment',
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'WOList',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function () {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});


