Ext.define('singer.view.ServiceModule.RepairPaymentss.AddRepairPayments', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addRpairPayment',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 800,
    height: 600,
    store: '',
    requires: [
        'Ext.data.*',
        'Ext.form.*'
    ],
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {
        //console.log('build items');

        return [
            {
                xtype: 'fieldset',
                title: 'Repair Payment',
                defaultType: 'textfield',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                items: [
                    {
                        xtype: 'moneyfield',
                        fieldLabel: 'Payment For Selected Jobs(Rs.)',
                        value: 0,
                        allowBlank: false,
                        id: 'selctedJob'
                    },
                    {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'NBT',
                        combineErrors: true,
                        msgTarget: 'side',
                        layout: 'hbox',
                        defaults: {
                            flex: 1,
                            hideLabel: true
                        },
                        items: [
                            {
                                xtype: 'moneyfield',
                                name: 'nbtValue',
                                fieldLabel: 'NBT',
                                margin: '0 5 0 0',
                                id: 'nbt'
                            },
                            {
                                xtype: 'checkbox',
                                margin: '0 5 0 0',
                                id: 'addNbt',
                                name: 'nbtApplicable',
                                listeners: {
                                    change: function (me, newValue, oldValue, eOpts) {
                                        if (newValue === true) {

                                            var vatValue = Ext.getCmp('nbt').getValue();
//                                           var currentTotal =parseFloat(me.nextNode().nextNode().nextNode().nextNode().value) ;
                                            var currentTotal = Ext.getCmp('addRpairPayment_tot').getValue();
                                            var newTOTAL = vatValue + currentTotal;
                                            Ext.getCmp('addRpairPayment_tot').setValue(newTOTAL.toString());
                                        } else {
//                                            var currntTOT =  parseFloat(me.nextNode().nextNode().nextNode().nextNode().value);
                                            var currntTOT = Ext.getCmp('addRpairPayment_tot').getValue();
                                            var vatValue = Ext.getCmp('nbt').getValue();
                                            var newTOT = currntTOT - vatValue;
                                            Ext.getCmp('addRpairPayment_tot').setValue(newTOT.toString());
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'VAT',
                        combineErrors: true,
                        msgTarget: 'side',
                        layout: 'hbox',
                        defaults: {
                            flex: 1,
                            hideLabel: true
                        },
                        items: [
                            {
                                xtype: 'moneyfield',
                                name: 'vatValue',
                                id: 'vat',
                                fieldLabel: 'VAT',
                                margin: '0 5 0 0'
                            },
                            {
                                xtype: 'checkbox',
                                margin: '0 5 0 0',
                                id: 'addVat',
                                name: 'vatApplicable',
                                listeners: {
                                    change: function (me, newValue, oldValue, eOpts) {
                                        if (newValue === true) {
                                            var vatValue = Ext.getCmp('vat').getValue();
//                                           var currentTotal =parseFloat(me.nextNode().nextNode().nextNode().nextNode().value) ;
                                            var currentTotal = Ext.getCmp('addRpairPayment_tot').getValue();
                                            var newTOTAL = vatValue + currentTotal;
                                            Ext.getCmp('addRpairPayment_tot').setValue(newTOTAL.toString());
//                                            Ext.getCmp('repair_tot_old').setValue(newTOTAL.toString());
                                        } else {
//                                            var currntTOT =  parseFloat(me.nextNode().nextNode().nextNode().nextNode().value);
                                            var currntTOT = Ext.getCmp('addRpairPayment_tot').getValue();
                                            var vatValue = Ext.getCmp('vat').getValue();
                                            var newTOT = currntTOT - vatValue;
                                            Ext.getCmp('addRpairPayment_tot').setValue(newTOT.toString());
//                                            Ext.getCmp('repair_tot_old').setValue(newTOT.toString());
//                                            Ext.getCmp('vat').setValue('0');
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'ESSD Account No',
                        name: 'essdAccountNo',
                        allowBlank: false,
                        id: 'acountNo',
                        regex: /^[0-9]+$/
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'ESSD % ',
                        id: 'addRpairPayment-essd'
                    },
                    {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'Deduction',
                        combineErrors: true,
                        msgTarget: 'side',
                        layout: 'hbox',
                        defaults: {
                            flex: 1,
                            hideLabel: true
                        },
                        items: [
                            {
                                xtype: 'moneyfield',
                                name: '',
                                margin: '0 5 0 0',
                                id: 'deduct_val',
                                listeners : {
                                    change : function(me, newValue, oldValue, eOpts){
                                        
                                        var chkDeduct = Ext.getCmp("chkDeduct").getValue();
                                             if (chkDeduct == true) {
                                            console.log("true chk");

                                            var total = parseFloat(Ext.getCmp('selctedJob').getValue());

                                            var deductionVAl = parseFloat(Ext.getCmp('deduct_val').getValue());
                                            var deduction = total - deductionVAl;

                                            var getNbtStatus = Ext.getCmp("addNbt").getValue();
                                            var getVatStatus = Ext.getCmp("addVat").getValue();

                                            var newNbtValue = deduction * singer.AppParams.NBT;
                                            var newVatValue = (deduction + newNbtValue) * singer.AppParams.TAX;

                                            Ext.getCmp("nbt").setValue(newNbtValue);
                                            Ext.getCmp("vat").setValue(newVatValue);

                                            if (getNbtStatus) {

                                                deduction = deduction * (1 + singer.AppParams.NBT);

                                            }

                                            if (getVatStatus) {
                                                deduction = deduction * (1 + singer.AppParams.TAX);
                                            }

                                            console.log('deduction', deduction);


                                            Ext.getCmp("addRpairPayment_tot").setValue(deduction);

                                        } 
                                    }
                                }
                            },
                            {
                                xtype: 'checkbox',
                                margin: '0 5 0 0',
                                id: 'chkDeduct',
                                listeners: {
                                    change: function (me, newValue, oldValue, eOpts) {

                                        if (newValue == true) {
                                            console.log("true chk");

                                            var total = parseFloat(Ext.getCmp('selctedJob').getValue());

                                            var deductionVAl = parseFloat(Ext.getCmp('deduct_val').getValue());
                                            var deduction = total - deductionVAl;

                                            var getNbtStatus = Ext.getCmp("addNbt").getValue();
                                            var getVatStatus = Ext.getCmp("addVat").getValue();

                                            var newNbtValue = deduction * singer.AppParams.NBT;
                                            var newVatValue = (deduction + newNbtValue) * singer.AppParams.TAX;

                                            Ext.getCmp("nbt").setValue(newNbtValue);
                                            Ext.getCmp("vat").setValue(newVatValue);

                                            if (getNbtStatus) {

                                                deduction = deduction * (1 + singer.AppParams.NBT);

                                            }


                                            if (getVatStatus) {
                                                deduction = deduction * (1 + singer.AppParams.TAX);
                                            }

                                            console.log('deduction', deduction);


                                            Ext.getCmp("addRpairPayment_tot").setValue(deduction);

                                        } else {
                                            console.log("false dedcut");

                                            var total = parseFloat(Ext.getCmp('selctedJob').getValue());

                                            var getNbtStatus = Ext.getCmp("addNbt").getValue();
                                            var getVatStatus = Ext.getCmp("addVat").getValue();

                                            var newNbtValue = total * singer.AppParams.NBT;
                                            var newVatValue = (total + newNbtValue) * singer.AppParams.TAX;

                                            Ext.getCmp("nbt").setValue(newNbtValue);
                                            Ext.getCmp("vat").setValue(newVatValue);

                                            if (getNbtStatus) {

                                                total = total * (1 + singer.AppParams.NBT);

                                            }


                                            if (getVatStatus) {
                                                total = total * (1 + singer.AppParams.TAX);
                                            }

                                            console.log('total', total);

                                            Ext.getCmp("addRpairPayment_tot").setValue(total);

                                        }

//                                        if (newValue === true) {
//
//                                            console.log("true");
//
//                                            var total = parseFloat(Ext.getCmp('selctedJob').getValue());
//                                            var deductionVAl = parseFloat(Ext.getCmp('deduct_val').getValue());
//                                            var deduction = total - deductionVAl;
//                                            console.log("deduct ", deduction);
//
////                                            var newVat = deduction * singer.AppParams.TAX;
//                                            var newNbt = deduction * (singer.AppParams.NBT);
//
//                                            console.log("newvat" + newVat + "nbt" + newNbt);
//
//                                            Ext.getCmp('addRpairPayment_tot').setValue(deduction);
//
//                                            Ext.getCmp("vat").setValue(newVat);
//                                            Ext.getCmp("nbt").setValue(newNbt);
//
//
//                                            var initTot = Ext.getCmp("selctedJob").getValue();
//                                            console.log("selctedJob",initTot);
//                                            var finalTotl = 0;
//
//                                            finalTotl = finalTotl + initTot;
//
//                                            if (Ext.getCmp("addNbt").getValue()) {
//                                                finalTotl = finalTotl + parseFloat(Ext.getCmp("nbt").getValue());
//                                            }
//
//                                            if (Ext.getCmp("addVat").getValue()) {
//                                                finalTotl = finalTotl + parseFloat(Ext.getCmp("vat").getValue());
//                                            }
//
//                                            finalTotl = finalTotl - deductionVAl;
//
//                                            Ext.getCmp('addRpairPayment_tot').setValue(finalTotl);
//
//
//
//                                        } else {
//
//                                            console.log("false");
//
//                                            var total = parseFloat(Ext.getCmp('addRpairPayment_tot').getValue());
//                                            var deductionVAl = parseFloat(Ext.getCmp('deduct_val').getValue());
//                                            var deduction = total - deductionVAl;
//
//                                            var newVat = deduction * singer.AppParams.TAX;
//                                            var newNbt = deduction * singer.AppParams.NBT;
//
//                                            console.log("newvat" + newVat + "nbt" + newNbt);
//
//                                            Ext.getCmp('addRpairPayment_tot').setValue(deduction);
//
//                                            Ext.getCmp("vat").setValue(newVat);
//                                            Ext.getCmp("nbt").setValue(newNbt);
//
//
//                                            console.log("insta", deduction);
//                                            Ext.getCmp('addRpairPayment_tot').setValue(deduction);
//
//                                            var initTot = Ext.getCmp("selctedJob").getValue();
//
//                                            var finalTotl = 0;
//
//                                            finalTotl = finalTotl + parseFloat(initTot);
//
//                                            if (Ext.getCmp("addNbt").getValue()) {
//                                                finalTotl = finalTotl + parseFloat(Ext.getCmp("nbt").getValue());
//                                            }
//
//                                            if (Ext.getCmp("addVat").getValue()) {
//                                                finalTotl = finalTotl + parseFloat(Ext.getCmp("vat").getValue());
//                                            }
//
//
//                                            Ext.getCmp('addRpairPayment_tot').setValue(finalTotl);
//
//
//                                        }
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'moneyfield',
                        fieldLabel: 'Total',
                        name: 'amount',
                        allowBlank: true,
                        id: 'addRpairPayment_tot',
                        readOnly: true
                    },
                    {
                        //margin: '0 5 0 0',
                        xtype: 'button',
                        text: 'Select Work Order',
                        action: 'select_Work_Order'
                                //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                                //allowBlank: false
                    },
                    {
                        margin: '0 0 30 0',
                        xtype: 'grid',
                        frame: false,
                        overflowY: 'auto',
                        //store: 'RepairPaymentWithPrice',
                        store: 'RepairPaymentWithPriceSelected',
                        border: true,
                        id: 'RepairPayGrid',
                        columns: [
                            {
                                //xtype: 'searchablecolumn',
                                text: 'WO No',
                                dataIndex: 'workOrderNo',
                                flex: 1
                            },
                            {
                                //xtype: 'searchablecolumn',
                                text: 'Warranty Verification',
                                dataIndex: 'warrentyTypeDesc',
                                flex: 1
                            },
                            {
                                //xtype: 'searchablecolumn',
                                text: 'IMEI',
                                dataIndex: 'imeiNo',
                                flex: 1
                            },
                            {
                                //xtype: 'searchablecolumn',
                                text: 'Defect',
                                dataIndex: 'defectDesc',
                                flex: 1
                            },
                            {
                                //xtype: 'searchablecolumn',
                                text: 'Invoice No',
                                dataIndex: 'invoice',
                                flex: 1
                            },
                            {
                                //xtype: 'searchablecolumn',
                                text: 'Spares Cost',
                                dataIndex: 'spareCost',
                                flex: 1,
                                renderer: function (value) {
                                    return Ext.util.Format.number(value, '0,000.00');
                                }
                            },
                            {
                                //xtype: 'searchablecolumn',
                                text: 'Labour Cost',
                                dataIndex: 'laborCost',
                                flex: 1,
                                renderer: function (value) {
                                    return Ext.util.Format.number(value, '0,000.00');
                                }
                            },
                            {//delete action column
                                xtype: 'actioncolumn',
                                width: 100,
                                text: 'Remove ',
                                //menuDisabled: true,
                                items: [{
                                        icon: 'resources/images/delete-icon.png',
                                        tooltip: 'Remove',
                                        handler: function (grid, rowIndex, colIndex) {
                                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                                            this.up('grid').fireEvent('on_open_delete', grid, rec, rowIndex);
                                        }
//                                        isDisabled: function (view, rowIndex, colIndex, item, record) {
//                                            if (record.get('status') === 1) {
//                                                return false;
//                                            } else {
//                                                return true;
//                                            }
//                                        }
                                    }]
                            },
                            {
                                xtype: 'checkcolumn',
                                text: 'Select',
                                flex: 1,
                                dataIndex: 'chk',
                                listeners: {
                                    checkchange: function (cmp, rowIndex, checked, eOpts) {
                                        var store = cmp.up('grid').getStore();
                                        if (checked === true) {

                                            var chkNBT = Ext.getCmp('addNbt').getValue();
                                            var chkVAT = Ext.getCmp('addVat').getValue();


                                            var spareCost = store.getAt(rowIndex).data.spareCost;
                                            var labrCost = store.getAt(rowIndex).data.laborCost;
                                            var costAmnt = spareCost + labrCost;
                                            var paymentforSelectedJob = Ext.getCmp('selctedJob').getValue();
                                            costAmnt += parseFloat(paymentforSelectedJob);

                                            var nbtamount = singer.AppParams.NBT * costAmnt;
                                            var nbtCostTOT = nbtamount + costAmnt;
                                            var VATamount = singer.AppParams.TAX * nbtCostTOT;
//                                            var total = nbtamount+costAmnt+VATamount;
//                                            
                                            Ext.getCmp('selctedJob').setValue(costAmnt.toString());
                                            Ext.getCmp('addRpairPayment_tot').setValue(costAmnt.toString());
                                            Ext.getCmp('nbt').setValue(nbtamount.toString());
                                            Ext.getCmp('vat').setValue(VATamount.toString());

                                            if (chkNBT === true) {
                                                costAmnt += nbtamount;
                                                Ext.getCmp('addRpairPayment_tot').setValue(costAmnt);
                                                Ext.getCmp('repair_tot_old').setValue(costAmnt);
                                            }

                                            if (chkVAT === true) {
                                                costAmnt += VATamount;
                                                Ext.getCmp('addRpairPayment_tot').setValue(costAmnt);
                                                Ext.getCmp('repair_tot_old').setValue(costAmnt);
                                            }

                                        } else {
                                            var chkNBT = Ext.getCmp('addNbt').getValue();
                                            var chkVAT = Ext.getCmp('addVat').getValue();

                                            var crrntVal = Ext.getCmp('selctedJob').getValue();
                                            crrntVal = parseFloat(crrntVal);
//                                            var store = Ext.getStore('RepairPaymentWithPrice');
                                            var spareCost = store.getAt(rowIndex).data.spareCost;
                                            var labrCost = store.getAt(rowIndex).data.laborCost;
                                            var costAmnt = spareCost + labrCost;

                                            var nbtamount = singer.AppParams.NBT * costAmnt;
                                            var nbtCostTOT = nbtamount + costAmnt;
                                            var VATamount = singer.AppParams.TAX * nbtCostTOT;

                                            //console.log("VATamount>",VATamount);

                                            var dedutVal = crrntVal - costAmnt;

                                            var nbtamount = singer.AppParams.NBT * costAmnt;
                                            var currentNBT = Ext.getCmp('nbt').getValue();
                                            var totalNBT = currentNBT - nbtamount;

                                            //var VATamount  = singer.AppParams.NBT *costAmnt;
                                            var currentVAT = Ext.getCmp('vat').getValue();
                                            var totalVAT = currentVAT - VATamount;

                                            Ext.getCmp('selctedJob').setValue(dedutVal);
                                            Ext.getCmp('addRpairPayment_tot').setValue(dedutVal);
                                            Ext.getCmp('nbt').setValue(totalNBT);
                                            Ext.getCmp('vat').setValue(totalVAT);
                                            if (chkNBT === true) {
                                                ////console.log(totalNBT);
                                                dedutVal += totalNBT;
                                                Ext.getCmp('addRpairPayment_tot').setValue(dedutVal);
                                                Ext.getCmp('repair_tot_old').setValue(dedutVal);
                                            }

                                            if (chkVAT === true) {
                                                dedutVal += totalVAT;
                                                Ext.getCmp('addRpairPayment_tot').setValue(dedutVal);
                                                Ext.getCmp('repair_tot_old').setValue(dedutVal);
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
            //            String workOrderNo,
            //            String imeiNo,
            //            String warrantyVerifType,
            //            String defectDesc,
            //            int status,
            //            String order,
            //            String type,
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Submit',
                action: 'submit_repair_payment_form',
                id: 'submit',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel_repair_payment_Form',
                id: ''
                        //                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});