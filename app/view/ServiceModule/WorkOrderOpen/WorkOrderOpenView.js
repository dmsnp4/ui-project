Ext.define('singer.view.ServiceModule.WorkOrderOpen.WorkOrderOpenView', {
    extend: 'Ext.window.Window',
    alias: 'widget.workOrderOpenView',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    height: "80%",
    initComponent: function() {
        var workOrderOpenView = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            overflowY: 'auto',
            bodyPadding: '0',
            layout: {
                type: 'accordion',
                titleCollapse: true,
                animate: true,
                activeOnTop: false
            },
            fieldDefaults: {
                xtype: 'displayfield',
                anchor: '100%',
                msgTarget: 'side',
                labelAlign: 'left',
                labelWidth: 180,
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>'
            },
            items: workOrderOpenView.buildItems(),
            buttons: workOrderOpenView.buildButtons()
        });
        workOrderOpenView.items = [formPanel];
        workOrderOpenView.callParent();
    },
    buildCustomerInformation: function() {
        return {
            xtype: 'panel',
            title: "Customer Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {type: 'vbox', align: 'stretch'},
            defaults: {
                xtype: 'displayfield',
                margin: '10 10 0 10'
            },
            items: [
                {
                    fieldLabel: 'NIC',
                    name: 'customerNic',
                    listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
                },
                {
                    fieldLabel: 'Name',
                    name: 'customerName'
                },
                {
                    fieldLabel: 'Address',
                    name: 'customerAddress'
                },
                {
                    fieldLabel: 'Telephone',
                    name: 'workTelephoneNo'
                },
                {
                    fieldLabel: 'Email',
                    name: 'email'
                }
            ]
        };
    },
    buildProductInformation: function() {
        return {
            xtype: 'panel',
            title: "Product Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {type: 'vbox', align: 'stretch'},
            defaults: {
                xtype: 'displayfield',
                margin: '10 10 0 10'
            },
            items: [
                {
                    fieldLabel: 'IMEI',
                    name: 'imeiNo'
                },
                {
                    fieldLabel: 'Product',
                    name: 'product'
                },
                {
                    fieldLabel: 'Brand',
                    name: 'brand'
                },
                {
                    fieldLabel: 'Model',
                    name: 'modleDesc'
                },
                {
                    fieldLabel: 'Accessories Retained',
                    name: 'assessoriesRetained'
                },
                {
                    fieldLabel: 'Defect',
                    name: 'defectDesc'
                },
                {
                    fieldLabel: 'RCC Reference',
                    name: 'rccReference'
                },
                {
                    fieldLabel: 'Date Of Sale',
                    name: 'dateOfSale'
                },
                {
                    fieldLabel: 'Distributor/Shop',
                    name: 'serviceBisDesc'
                },
                {
                    fieldLabel: 'Remarks',
                    name: 'remarks'
                }
            ]
        };
    },
    buildWarrentyInformation: function() {
        return {
            xtype: 'panel',
            title: "Warranty Information",
            padding: '0',
            height: 248,
            overflowY: 'auto',
            layout: {type: 'vbox', align: 'stretch'},
            defaults: {
                xtype: 'displayfield',
                margin: '10 10 0 10'
            },
            items: [
                {
                    fieldLabel: 'Received Date',
                    name: 'createDate'
                },
                {
                    fieldLabel: 'Warranty Type',
                    name: 'warrantyStatusMsg'
                },
                {
                    fieldLabel: 'Non Warranty Type',
                    name: 'nonWarrentyVrifType',
                    
                    xtype: 'combo',
//                    fieldLabel: 'Non Warranty Type',
//                    name: 'nonWarrentyVrifType',
                    id : 'viewNonWarrentyVrifType',
                    store: 'NonWrrntyType',
                    displayField: 'description',
                    valueField: 'code',
                    queryMode: 'remote',
                    readOnly:true
//                    allowBlank: 'true',
//                    afterLabelTextTpl: '',
//                    hidden: true,
                },
                {
                    fieldLabel: 'Non Warranty Remarks',
                    name: 'nonWarrantyRemarks',
                    id : "viewNonWarrentyRemarks"
                },
                {
                    fieldLabel: 'Customer Complain',
                    name: 'customerComplain'
                }
            ]
        };
    },
    buildItems: function() {
        return [
            this.buildCustomerInformation(),
            this.buildProductInformation(),
            this.buildWarrentyInformation()
        ];
    },
    buildButtons: function() {
        return [
            '->',
            {
                xtype: 'button',
                text: 'Close',
                action: 'cancel'
            }
        ];
    }
});


