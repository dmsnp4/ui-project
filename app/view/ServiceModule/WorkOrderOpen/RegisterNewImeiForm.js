Ext.define('singer.view.ServiceModule.WorkOrderOpen.RegisterNewImeiForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.registerNewImeiForm',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    height: "80%",
    initComponent: function() {
        var registerIMEIWin = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
            items: registerIMEIWin.buildItems(),
            buttons: registerIMEIWin.buildButtons()
        });
        registerIMEIWin.items = [formPanel];
        registerIMEIWin.callParent();
    },
    buildItems: function() {
        return [
            {
                xtype: 'hiddenfield',
                name: 'warrntyRegRefNo'
            }, 
            {
                xtype: 'hiddenfield',
                name: 'modleDescription'
            }, 
            {
                xtype: 'hiddenfield',
                name: 'product'
            },
            {
                xtype: 'hiddenfield',
                name: 'bisId'
            },
            {
                xtype: 'hiddenfield',
                name: 'email'
            },
            {
                fieldLabel: 'Serial No(IMEI):',
                name: 'imeiNo',
                maxLength: 100
            },
            {
                fieldLabel: 'NIC',
                xtype: 'nicfield',
                name: 'customerNIC'
            },
            {
                fieldLabel: 'Customer Name',
                name: 'customerName',
                maxLength: 100
            },
            {
                fieldLabel: 'Customer Address',
                name: 'customerAddress',
                maxLength: 100
            },
            {
                fieldLabel: 'Contact No',
                name: 'contactNo',
                maxLength: 10,
                minLength: 10,
                allowBlank: true,
                afterLabelTextTpl: '',
                regex: /^[0-9_]+$/
            },
            {
                id:'cstMl',
                fieldLabel: 'Customer Email',
//                name: 'email',               
                allowBlank: false,
//                afterLabelTextTpl: '',
//                regex: /^[0-9_]+$/
            },
            {
                fieldLabel: 'Date Of Birth',
                name: 'dateOfBirth',
                xtype: 'datefield',
                maxValue: new Date(),
                format: 'Y-m-d',
                value: new Date(),
                allowBlank: true,
                afterLabelTextTpl: ''
            },
            {
                fieldLabel: 'Customer Code',
                name: 'customerCode',
                maxLength: 100,
                allowBlank: true,
                afterLabelTextTpl: ''
            },
            {
                fieldLabel: 'Shop Code',
                name: 'shopCode',
                maxLength: 100
            },
            {
                fieldLabel: 'Model',
                xtype: 'combobox',
                name: 'modleNo',
                emptyText: 'Select Model',
                store: 'modStore',
                displayField: 'model_Description',
                valueField: 'model_No',
                queryMode: 'remote',
                readOnly:true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Warranty Scheme',
                name: 'warantyType',
                store: 'WarrentySchStore',
                displayField: 'schemeName',
                valueField: 'schemeId',
                allowBlank: true,
                readOnly:true,
                afterLabelTextTpl: '',
                queryMode: 'local'
            },
            {
                fieldLabel: 'Site',
                name: 'siteDesc',
                maxLength: 100,
                allowBlank: true,
                afterLabelTextTpl: ''
            },
            {
                fieldLabel: 'Order No',
                name: 'orderNo',
                maxLength: 100
            },
            {
                xtype: 'moneyfield',
                fieldLabel: 'Sales Price(Rs.)',
                name: 'sellingPrice',
                maxLength: 100
            },
            {
                fieldLabel: 'Proof Of Purchase Refference',
                name: 'proofPurchas',
                maxLength: 100,
//                allowBlank: true,
//                afterLabelTextTpl: ''
            },
            {
                fieldLabel: 'Registered Date',
                name: 'registerDate',
                xtype: 'datefield',
                maxValue: new Date(),
                format: 'Y-m-d',
//                value: new Date(),
//                allowBlank: true,
//                afterLabelTextTpl: ''
            }
        ];
    },
    buildButtons: function() {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, '->',
            {
                xtype: 'button',
                text: 'Create',
                action: 'create_new_imei',
                id: 'registerNewImeiForm-create',
                hidden: true,
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Save',
                action: 'save_imei',
                id: 'registerNewImeiForm-save',
                hidden: true,
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }
});


