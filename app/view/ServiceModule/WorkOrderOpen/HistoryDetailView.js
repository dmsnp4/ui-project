Ext.define('singer.view.ServiceModule.WorkOrderOpen.HistoryDetailView', {
    extend: 'Ext.window.Window',
    alias: 'widget.historydetailview',
    loadMask: true,
    //    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Repair Details',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: '60%',
    height: '80%',
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: '90%',
            height: '90%',
            bodyPadding: 5,
            overflowY: 'auto',
            overflowX: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0',
                readOnly: true
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Planing"
                },
                {
                    "status": 3,
                    "value": "Start"
                },
                {
                    "status": 9,
                    "value": "Closed"
                }
            ]
        });
        return [
            {
                fieldLabel: 'Work Order No',
                name: 'workOrderNo',
            },
            {
                fieldLabel: 'Warranty',
                name: 'warrantyStatusMsg',
            },
//            {
//                fieldLabel: 'Technician',
//                name: 'tecnician',
//            },
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h2>Defects</h2>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'grid',
                maxHeight: 200,
                //                store: 'CreateEventBisTypes',
                id: 'historydetailview-defectsGrid',
                columns: [
                    {
                        text: 'Major Defect',
                        dataIndex: 'majDesc',
                        flex: 1,
                        menuDisabled:true
                    },
                    {
                        text: 'Minor Defect',
                        dataIndex: 'minDesc',
                        flex: 1,
                        menuDisabled:true
                    }],
            },
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h2>Spares</h2>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'grid',
                maxHeight: 200,
                //                store: 'CreateEventBisTypes',
                id: 'historydetailview-spares',
                columns: [
                    {
                        text: 'Spare Part',
                        dataIndex: 'partDesc',
                        flex: 1,
                        menuDisabled:true
                    }],
            },
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h2>Work Order Details</h2>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'grid',
                maxHeight: 200,
                //                store: 'CreateEventBisTypes',
                id: 'historydetailview-woDetails',
                columns: [
                    {
                        text: 'Invoice No',
                        dataIndex: 'invoiceNo',
                        flex: 1,
                        menuDisabled:true
                    },
                    {
                        text:'Technician',
                        dataIndex:'technician',
                        flex:1,
                        menuDisabled:true
                    },
                    {
                        text:'Action Taken',
                        dataIndex:'actionTaken',
                        menuDisabled:true
                    },
                    {
                        text:'Remarks',
                        dataIndex:'remarks',
                        menuDisabled:true
                    }
                ],
            },
//            {
//                fieldLabel: 'Repair',
//                name: 'repairLevelDesc'
//            },
//            {
//                fieldLabel: 'Remarks',
//                name: 'remarks'
//            },
            {
                fieldLabel: 'Repair Date',
                name: 'repairDate'
            }

            ];
    },
    buildButtons: function () {
        return ['->', {
            text: 'Done',
            action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});