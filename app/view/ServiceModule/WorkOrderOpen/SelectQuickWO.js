Ext.define('singer.view.ServiceModule.WorkOrderOpen.SelectQuickWO', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.SelectQuickWO',
    height: 300,
    width: 1000,
    layout: 'fit',
    loadMask: true,
    store: 'GetQuickWorkOrderListForSelect',
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();
        console.log("exchange select grid load");
        me.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'GetQuickWorkOrderListForSelect',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'actioncolumn',
                header: 'Select',
                width: 100,
                items: [{
                        icon: 'resources/images/Select.png', // Use a URL in the icon config
                        tooltip: 'click to select Work Order',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            
                            console.log('select wo');
                            
                            this.up('grid').fireEvent('selectQuickWrkOrderOPenAction', grid, record);
                        }
                    }]
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Work Order No',
                dataIndex: 'workOrderNo'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Name',
                dataIndex: 'customerName'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Address',
                dataIndex: 'customerAddress'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Telephone',
                dataIndex: 'workTelephoneNo'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'E-mail',
                dataIndex: 'email'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'NIC',
                dataIndex: 'customerNic'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'IMEI',
                dataIndex: 'imeiNo'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'product'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Brand',
                dataIndex: 'brand'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modleDesc'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Defect',
//                dataIndex: 'mjrDefectDesc'
                dataIndex: 'defectDesc'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'RCC Reference',
                dataIndex: 'rccReference'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Date Of Sale',
                dataIndex: 'dateOfSale'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Proof Of Purchase',
                dataIndex: 'proofOfPurches'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Distributor/DSR ',
                dataIndex: 'binName'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Warranty Type',
                dataIndex: 'warrantyStatusMsg'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'status'
            },
           
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Delay Days',
                dataIndex: 'delayDate'
            }


        ];
    }


});