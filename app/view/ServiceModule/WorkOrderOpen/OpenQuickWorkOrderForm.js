

Ext.define('singer.view.ServiceModule.WorkOrderOpen.OpenQuickWorkOrderForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.openQuickWorkOrderForm',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    height: "80%",
    initComponent: function () {

        var quickWorkOrderWin = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
            items: quickWorkOrderWin.buildItems(),
            buttons: quickWorkOrderWin.buildButtons()
        });
        quickWorkOrderWin.items = [formPanel];
        quickWorkOrderWin.callParent();
    },
    buildItems: function () {
        var warTypes = Ext.create('Ext.data.Store', {
            fields: ['expireStatus', 'warrantyStatusMsg'],
            data: [
                {"expireStatus": 0, "warrantyStatusMsg": "Select Warranty"},
                {"expireStatus": 1, "warrantyStatusMsg": "Under Warranty"},
                {"expireStatus": 2, "warrantyStatusMsg": "Out of Warranty"}
            ]
        });
        return [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [
//                    {
//                        xtype: 'combo',
//                        fieldLabel: 'Work Order No',
//                        name: 'workOrderNo',
//                        flex: 4,
//                        store: 'woDrp',
//                        displayField: 'workOrderNo',
//                        valueField: 'workOrderNo',
//                        visible: true,
//                        queryMode: 'remote',
//                        listeners: {
//                            change: function (field, newVal, oldVal) {
//                                this.up("window").fireEvent("on_work_order_change", field, newVal);
//                            }
//                        }
//                    },

                    {
                        xtype: 'textfield',
                        fieldLabel: 'Work Order No',
                        name: 'workOrderNo',
                        readOnly: true
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        disabled: false,
                        id: 'openQuickWorkOrderForm-searchwo',
                        action: 'search_quick_work_order',
                        flex: 1,
                        margin: "0px 0px 0px 5px"
                    }
                ]
            },
            {
                xtype: 'nicfield',
                fieldLabel: 'NIC',
                name: 'customerNic'
                        //readOnly: true
            },
            {
                id: 'imeii',
                fieldLabel: 'IMEI',
                name: 'imeiNo'
                        //readOnly: true
            },
//            {
//                xtype: 'button',
//                text: 'Verify IMEI',
////                disabled: true,
//                id: 'openQuickWorkOrderForm-verifyImei',
//                action: 'quickWorkVerify',
//                flex: 1,
//                margin: "10 5 5 10"
//            },
            {
                fieldLabel: 'Name',
                name: 'customerName'
                        //readOnly: true
            },
            {
                fieldLabel: 'Address',
                name: 'customerAddress'
                        //readOnly: true
            },
            {
                fieldLabel: 'Telephone',
                name: 'workTelephoneNo'
                        //readOnly: true
            },
            {
                fieldLabel: 'E-mail',
                name: 'email'
                        //readOnly: true
            },
            {
                fieldLabel: 'Product',
                name: 'product'
                        //readOnly: true
            },
            {
                fieldLabel: 'Brand',
                name: 'brand'
                        //readOnly: true
            },
            {
                fieldLabel: 'Model',
                xtype: 'combobox',
                name: 'modleNo',
                emptyText: 'Select Model',
                store: 'modStore',
                displayField: 'model_Description',
                valueField: 'model_No',
                afterLabelTextTpl: "",
                queryMode: 'local'
            },
            {
                fieldLabel: 'Accesseries Reatained',
                name: 'assessoriesRetained'
                        //readOnly: true
            },
            {
                xtype: 'combo',
                id: 'qwo-defect',
                fieldLabel: 'Defect',
                //name: 'defectNo',
                store: 'mjrdefectStr',
                displayField: 'mjrDefectDesc',
                valueField: 'mjrDefectCode',
                queryMode: 'local',
                listeners: {
                    change: function (field, newVal, oldVal) {
                        console.log('New Val:: ', newVal);
                        Ext.getCmp('hdn-qwo-defect').setValue(newVal);
                    }
                }
            },
            {
                xtype: 'hiddenfield',
                id: 'hdn-qwo-defect',
                name: 'defectNo',
            },
            {
                fieldLabel: 'Rcc Reference',
                name: 'rccReference'
                        //readOnly: true
            },
            {
                fieldLabel: 'Date Of Sale',
                name: 'dateOfSale',
                xtype: 'datefield',
                allowBlank: true,
                afterLabelTextTpl: '',
//                maxValue: new Date(),
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
            },
            {
                fieldLabel: 'Delivery Date',
                name: 'deleveryDate',
                xtype: 'datefield',
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                minValue: new Date(),
                allowBlank: true
            },
            {
                fieldLabel: 'Proof Of purchase Reference',
                name: 'proofOfPurches'
                        //readOnly: true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Service Agent',
                name: 'serviceBisId',
                store: 'distributorstr',
                readOnly: true,
                displayField: 'bisName',
                valueField: 'bisId',
                queryMode: 'local',
                allowBlank: true,
                afterLabelTextTpl: ''
            },
//            {
//                xtype:'hiddenfield',
//                fieldLabel: 'Warranty Type',
//                name: 'expireStatus',
////                id:'expirestate',
//                readOnly:true,
//                
//            },
//            {
//                xtype: 'displayfield',
//                fieldLabel: 'Warranty Type',
//                name: 'expireStatus',
//                listeners: {
//                        scope: this,
//                        change: function (me, newValue, oldValue) {
//                            //console.log(newValue);
//                            if (newValue === 2) {
//                                me.setValue('Out Of Warranty');
//                            } else {
//                                me.setValue('Under Warranty');
//                            }
//                        }
//                    }
//            },
            {
                xtype: 'hiddenfield',
                fieldLabel: 'Warranty Type',
                name: 'nonWarrentyVrifType',
                value: 5
            },
            {
                xtype: 'combo',
                id: 'war-status',
                fieldLabel: 'Warranty Type',
                name: 'warrentyVrifType',
                store: warTypes,
                readOnly: false,
                displayField: 'warrantyStatusMsg',
                valueField: 'expireStatus',
                queryMode: 'local',
                allowBlank: true,
                afterLabelTextTpl: ''
            },
            {
                xtype: 'textareafield',
                fieldLabel: 'Remarks',
                id: 'Remarks',
                name: 'remarks',
                allowBlank: false,
                regex: /^[a-zA-Z_ 0-9\,'']+$/
            }

        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, '->',
            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                formBind: true,
                hidden: true,
                id: 'openQuickWorkOrderForm-save'
            },
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                formBind: true,
                hidden: true,
                id: 'openQuickWorkOrderForm-create'
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});