Ext.define('singer.view.ServiceModule.WorkOrderOpen.WorkOrderOpenGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.workOrderOpenGrid',
    loadMask: true,
    store: 'WorkOrderOpening',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    forceFit: false,
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function() {
        var grid = this;
        grid.columns = grid.buildColumns();
        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function(grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->',
                    {
                        text: 'Open Quick Work Order',
                        action: 'open_quick_work_order_window'
                    },
                    {
                        text: 'Add New Work Order',
                        action: 'open_new_work_order_window'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'WorkOrderOpening',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function() {
        return [
            {
                id: 'wrkOno',
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Work Order No',
                dataIndex: 'workOrderNo',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Name',
                dataIndex: 'customerName',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Address',
                dataIndex: 'customerAddress',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Telephone',
                dataIndex: 'workTelephoneNo',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'E-mail',
                dataIndex: 'email',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'NIC',
                dataIndex: 'customerNic',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'IMEI',
                dataIndex: 'imeiNo',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'product',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Brand',
                dataIndex: 'brand',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modleDesc',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Defect',
                dataIndex: 'defectDesc',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'RCC Reference',
                dataIndex: 'rccReference',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Date Of Sale',
                dataIndex: 'dateOfSale',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Proof Of Purchase',
                dataIndex: 'proofOfPurches',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Distributor',
                dataIndex: 'serviceBisDesc',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                text: 'Warranty Type',
                dataIndex: 'warrantyStatusMsg'
            },
            
            {
//                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'statusMsg'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Delay Days',
                dataIndex: 'delayDate',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'View ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View ',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('on_open_view', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Edit ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('on_open_edit', grid, rec);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                        if ((record.get('status') === 2)||(record.get('status') === 3)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Delete ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/delete-icon.png',
                        tooltip: 'Delete',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('on_open_delete', grid, rec);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                        if (record.get('status') === 1) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 150,
                text: 'Print Receipt ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/printerr.png',
                        tooltip: 'Print Receipt',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('on_open_print', grid, rec);
                        }
                    }]
            }

        ];
    }
});




