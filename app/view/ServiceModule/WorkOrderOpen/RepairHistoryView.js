Ext.define('singer.view.ServiceModule.WorkOrderOpen.RepairHistoryView', {
    extend: 'Ext.window.Window',
    alias: 'widget.repairHistoryView',
    layout: 'fit',
    autoScroll: true,
    modal: true,
    constrain: true,
    loadMask: true,
    width: "80%",
    height: "80%",
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.grid.*'
    ],
    initComponent: function() {
        var repairHistoryView = this;
        var gridPanel = Ext.create('Ext.grid.Panel', {
            forceFit: true,
            minHeight: 200,
            selType: 'rowmodel',
            store: 'WorkOrderHistoryForWoOpen',
            columns: repairHistoryView.buildColumns(),
            dockedItems: repairHistoryView.buildDockedItems()
        });
        repairHistoryView.items = [gridPanel];
        repairHistoryView.buttons = [
            {
                xtype : 'button',
                text : 'Cancel',
                action : 'cancel'
            }
        ];
        repairHistoryView.callParent();
    },
    buildColumns: function() {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'WO Number',
                dataIndex: 'workOrderNo',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'WO Date',
                dataIndex: 'createDate',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Defect',
                dataIndex: 'defectDesc',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Repair',
                dataIndex: 'repairStatusMsg',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Repair Date',
                dataIndex: 'repairDate',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Warrenty Status',
                dataIndex: 'warrantyStatusMsg',
                renderer: function(value) {
                    if (Ext.isEmpty(value)) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                text: 'View',
                width: 50,
                items: [{
                        tooltip: 'View WO maintain details',
                        icon: 'resources/images/view-icon.png',
                        width: 50,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('window').fireEvent('view_repair_history_print', grid, rec);
                        }
                    }]
            }

        ];
    },
    buildDockedItems: function() {
        return [{
                xtype: 'form',
                dock: 'top',
                items : [
                    {
                        xtype : 'hiddenfield',
                        name : 'imeiNo'
                    }
                ],
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'WorkOrderHistoryForWoOpen',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});










