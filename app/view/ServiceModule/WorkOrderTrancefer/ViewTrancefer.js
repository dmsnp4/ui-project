


Ext.define('singer.view.ServiceModule.WorkOrderTrancefer.ViewTrancefer', {
    extend: 'Ext.window.Window',
    alias: 'widget.tranceferView',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {

        return [
            {
                id:'transNo',
                fieldLabel: 'Work Order No:',
                name: 'workOrderNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Transfered Location:',
                name: 'bisName'
            },
            {
                fieldLabel: 'Transfered Date:',
                name: 'transferDate'
            },
            {
                fieldLabel: 'Transfered Reason:',
                name: 'transferReason'
            },
            {
                fieldLabel: 'Transfered Delivery Type:',
                name: 'deleveryTypeDesc'
            },
            {
                id:'trancferEmail',
                fieldLabel: 'Courier E-mail:',
                name: 'courierEmail',
                hidden:true
            },
            {
                fieldLabel: 'Returned Date:',
                name: 'returnDate'
            },
//            {
//                fieldLabel: 'Returned Delivery Type:',
//                name: 'retunDeleveryType'
//            },
            {
                fieldLabel: 'Returned Reason:',
                name: 'returnReason'
            },
            {
                fieldLabel: 'Returned Delivery Details:',
                name: 'returnDeleveryDetails'
            },
        ];
    },
    buildButtons: function () {
        return ['->', 
            {
                text: 'Close',
                action: 'view_wrk_order_cancel'
            },
            {
                text: 'Print',
                action: 'print_trnacfer'
            }
                ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});






