Ext.define('singer.view.ServiceModule.WorkOrderTrancefer.MainGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.wrkOrdrTranceferGrid',
    store: 'GetWorkOrderTrancefer',
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function() {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },
    buildColumns: function() {
        return [
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Work Order No',
                dataIndex: 'workOrderNo'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Transfer Franchise ',
                dataIndex: 'bisName'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Transfer Date',
                dataIndex: 'transferDate'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Return Date',
                dataIndex: 'returnDate'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Reason',
                dataIndex: 'transferReason'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Delivery Type',
                dataIndex: 'deleveryTypeDesc'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Courier E-mail',
                dataIndex: 'courierEmail'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Transfer Status',
                dataIndex: 'status',
                renderer: function(value, meta) {
                    switch (value) {
                        case 1:
                            meta.tdCls = 'WOpneding';
                            return 'Pending';
                            break;

                        case 2:
                            meta.tdCls = 'WOAccpeted';
                            return 'Accepted';
                            break;

                        case 3:
                            meta.tdCls = 'WOreject';
                            return 'Returned';
                            break;
                        
                        case 4:
                            meta.tdCls = 'WOAccpeted';
                            return 'Accepted';
                            break;
                            
                        default:
                            return "****";
                            break;
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                header: 'View',
                width: 100,
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('onView', grid, record);
                            //                            ////console.log(this.up('grid'));
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'Accept',
                width: 100,
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/approved.png', // Use a URL in the icon config
                        tooltip: 'Accept',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('accept', grid, record);
                        },
                        isDisabled: function(view, rowIndex, colIndex, item, record) {
                            //console.log(record.get('status'))

                            var sto = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
                            //console.log(record.getData());

                            if (sto === record.getData().bisId) {

                                if (record.get('status') === 3 ||record.get('status') === 1) {
                                    return false;
                                } else {
                                    return true;
                                }
                            } else {
                                return true;
                            }
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'Return',
                width: 100,
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/return.png', // Use a URL in the icon config
                        tooltip: 'Return',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('returnn', grid, record);
                        },
                        isDisabled: function(view, rowIndex, colIndex, item, record) {
                            //console.log(record.get('status'))
                            var sto = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');
                            //console.log(sto);

                            if (sto === record.getData().bisId) {
                                if (record.get('status') === 2 ||record.get('status') === 4) {
                                    return false;
                                }else{
                                    return true;
                                }
                            }else{
                                return true;
                            }

//                        if (sto === record.getData().tansferLocationId) {
//                            if(record.get('statusMsg') === 'Return'){
//                                return true;
//                            }else{
//                                return false;
//                            }
//                        } else {
//                            if (record.get('status') === 3 || record.get('status') === 1) {
//                                return true;
//                            } else {
//                                return false;
//                            }
//                        }


                        }
                    }]
            }
//            {
//                    xtype: 'actioncolumn',
//                    header: 'Edit',
//                    width: 100,
//                    menuDisabled:true,
//                    items: [{
//                            icon: 'resources/images/edit.jpg', // Use a URL in the icon config
//                            tooltip: 'Edit',
//                            handler: function (grid, rowIndex, colIndex) {
//                                    var record = (grid.getStore().getAt(rowIndex));
//                                    this.up('grid').fireEvent('edit', grid, record);
//                                    //                            ////console.log(this.up('grid'));
//                    },
//                    isDisabled: function (view, rowIndex, colIndex, item, record) {
//                        // Returns true if 'editable' is false (, null, or undefined)
//                        if (record.get('status') === 3 || record.get('status') === 4) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
//                    }]
//            },
//			{
//				xtype: 'actioncolumn',
//				header: 'Delete',
//				width: 100,
//                                menuDisabled:true,
//				items: [{
//					icon: 'resources/images/delete-icon.png', // Use a URL in the icon config
//					tooltip: 'Delete',
//					handler: function (grid, rowIndex, colIndex) {
//						var record = (grid.getStore().getAt(rowIndex));
//						this.up('grid').fireEvent('ondelete', grid, record);
//						//                            ////console.log(this.up('grid'));
//
//                    },
//                    isDisabled: function (view, rowIndex, colIndex, item, record) {
//                        // Returns true if 'editable' is false (, null, or undefined)
//                        if (record.get('status') === 3 || record.get('status') === 4) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
//                    }]
//            }

        ];
    },
    buildDockedItems: function() {
        return [{
                xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }
                    , '->', {
                        iconCls: 'icon-search',
                        text: 'Add New',
                        action: 'addNew'
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'GetWorkOrderTrancefer',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});