Ext.create('Ext.data.Store', {
    id: 'TrnsStatusSTR',
    fields: ['status', 'value'],
    data: [
//        {
//            "status": 2,
//            "value": "Still"
//        },
        {
            "status": 1,
            "value": "Pending"
        },
        {
            'status': 3,
            'value': 'Return'
        },
        {
            'status': 4,
            'value': 'Completed'
        }
    ]
});


Ext.define('singer.view.ServiceModule.WorkOrderTrancefer.AddNew', {
    extend: 'Ext.form.Panel',
    //    extend: 'window',
    alias: 'widget.addNewWorkOrderTrancefer',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 530,
    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
 
            {
                xtype: 'combo',
                fieldLabel: 'WO Number',
                name: 'workOrderNo',
                allowBlank: false,
                store: 'WOtransfrNo',
                displayField: 'workOrderNo',
                valueField: 'workOrderNo',
                queryMode: 'remote',
//                renderTo: Ext.getBody(),
                listeners:{
                      beforequery: function (record) {
                      record.query = new RegExp(record.query, 'i');
                      record.forceAll = true;
                    }
                }
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Transfer service Franchise',
                name: 'transferLocationDesc',
            },
            {
                xtype: 'combo',
                fieldLabel: 'Transfer service Franchise',
                name: 'bisId',
                maxLength: 100,
                allowBlank: false,
                store: 'bisAssignList',
                displayField: 'bisName',
                valueField: 'bisId',
                visible: true,
                listeners:{
                      beforequery: function (record) {
                      record.query = new RegExp(record.query, 'i');
                      record.forceAll = true;
                    }
                }
            },
            {
                xtype: 'hiddenfield',
                name: 'woTransNo'
            },
            {
                id:'addNew-TransferDate',
                fieldLabel: 'Transfer Date',
                name: 'transferDate',
                maxLength: 100,
                xtype: 'datefield',
                allowBlank: false,
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false,
                minValue:new Date(),
            },
            /////////////////////////////
            {
                id:'addNew-ReturnDate',
                fieldLabel: 'Returned Date',
                name: 'returnDate',
                maxLength: 100,
                xtype: 'datefield',
                allowBlank: false,
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false,
                minValue:new Date(),
            },
            {
                fieldLabel: 'Transfer Reason',
                name: 'transferReason',
                maxLength: 100,
            },
            {
                fieldLabel: 'Return Reason',
                name: 'returnReason',
                maxLength: 100,
            },
            
            ////////////////////////////////////////////////////////
            {
                xtype: 'combo',
                fieldLabel: ' Delivery Type',
                name: 'deleveryType',
                maxLength: 100,
                allowBlank: false,
                store: 'DeliverType',
                displayField: 'description',
                valueField: 'code',
                visible: true,
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        var courMail1 = cmp.nextSibling();
                        var courMail2 = cmp.nextSibling().nextSibling();
                        if(newValue===2){                            
                            courMail1.show();
                            courMail2.show();
                            courMail1.allowBlank = false;
                            courMail1.vtype = 'email';
                            courMail2.allowBlank = false;
                        }else{
                            courMail1.hide();
                            courMail2.hide();
                            courMail1.allowBlank = true;
                            courMail1.setValue('');
                            courMail1.vtype = null;
                            courMail2.allowBlank = true;
                            courMail2.setValue(' ');
                        }
                    }
                }
            },
            {
                id: 'curiermail1',
                fieldLabel: 'Courier E-mail',
                name: 'courierEmail',
                maxLength: 100,
                allowBlank: false,
                hidden:true
            },
            {
                id: 'curiermail2',
                fieldLabel: 'Re-type Email',
                name: 'courierEmail2',
                allowBlank: false,
                hidden:true
            },
            {
                fieldLabel: 'Transfer Delivery Details',
                name: 'deleverDetils',
                maxLength: 100
            },
            {
                fieldLabel: 'Return Delivery Details',
                name: 'returnDeleveryDetails',
                maxLength: 100
            },
            {
                xtype: 'combo',
                id:'transStatus',
                fieldLabel: ' Status',
                name: 'status',
                maxLength: 100,
                allowBlank: false,
                store: 'TrnsStatusSTR',
                displayField: 'value',
                valueField: 'status',
                visible: true,
                queryMode: 'remote',
//               // readOnly: true,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }

                }

            }
        ];
    },
    buildButtons: function () {
        return [
 //            {
 //                xtype: 'button',
 //                text: 'Submit',
 //                action: 'add_work_order_transfer_submit',
 //                formBind: true
 //            },
//            {
//                xtype: 'button',
//                maxWidth: 100,
//                text: 'Save',
//                action: 'editNewSubmit',
//                formBind: true,
//                id: 'addNewWorkOrderTrancefer-save',
//            },
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Return',
                action: 'returnTransfer',
                formBind: true,
                id: 'addNewWorkOrderTrancefer-Return',
            },
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Transfer',
                action: 'addNewSubmit',
                formBind: true,
                id: 'addNewWorkOrderTrancefer-transfer',
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'work_order_transfer_cancel'
            },
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});