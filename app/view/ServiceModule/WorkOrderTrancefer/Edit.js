////Ext.create('Ext.data.Store', {
////    id:'str',
////            fields: ['status', 'value'],
////            data: [
////                {"status": 1, "value": "A"},
////                {"status": 2, "value": "B"}
////            ]
////        });
//
//
//Ext.define('singer.view.ServiceModule.WorkOrderTrancefer.Edit', {
//	extend: 'Ext.form.Panel',
//	alias: 'widget.editWorkOrderTrancefer',
//	layout: 'fit',
//	modal: true,
//	constrain: true,
//	loadMask: true,
//	width: 500,
//
//	initComponent: function () {
//
//		var registerIMEI = this;
//		var formPanel = Ext.create('Ext.form.Panel', {
//			width: 500,
//			bodyPadding: 5,
//			overflowY: 'auto',
//			defaults: {
//				xtype: 'textfield'
//			},
//			fieldDefaults: {
//				anchor: '100%',
//				labelAlign: 'left',
//				msgTarget: 'side',
//				labelWidth: 180,
//				margin: '10 0 10 0'
//			},
//			items: registerIMEI.buildItems(),
//			buttons: registerIMEI.buildButtons()
//		});
//
//		registerIMEI.items = [formPanel];
//
//		registerIMEI.callParent();
//	},
//	buildItems: function () {
//		return [
//			{
//				xtype: 'hiddenfield',
//				name: 'woTransNo'
//			},
//			{
//				xtype: 'container',
//				layout: 'hbox',
//				defaultType: 'textfield',
//				items: [
//					{
//						id: 'wrkOno',
//						margin: '10 0 10 0',
//						fieldLabel: 'Work Order No',
//						name: 'workOrderNo',
//						allowBlank: false
//                          },
//					{
//						margin: '10 0 10 20',
//						xtype: 'button',
//						text: 'Search',
//						action: 'search_Work_Order'
//                        },
//                                    ]
//            },
//			{
//				xtype: 'combo',
//				fieldLabel: 'Trancefer service Franchise',
//				name: 'bisId',
//				maxLength: 100,
//				allowBlank: false,
//				store: 'bisAssignList',
//				displayField: 'bisName',
//                valueField: 'bisId',
//				visible: true,
//            },
//
// //            xtype: 'datefield',
//			{
//				fieldLabel: 'Trancefer Date',
//				name: 'transferDate',
//				maxLength: 100,
//				allowBlank: false
//			},
//			{
//				fieldLabel: 'Reason',
//				name: 'transferReason',
//				maxLength: 100,
//				allowBlank: false
//			},
//			{
//				xtype: 'combo',
//				fieldLabel: 'Deleivery Type',
//				name: 'deleveryType',
//				maxLength: 100,
//				allowBlank: false,
//				store: 'DeliverType',
//				displayField: 'description',
//				valueField: 'code',
//				visible: true,
//            },
//			{
//				fieldLabel: 'Courier E-mail',
//				name: 'courierEmail',
//				maxLength: 100,
//				allowBlank: false
//			},
//            //{fieldLabel: 'Retype E-mail',name: '',maxLength: 100,allowBlank: false},
//			{
//				fieldLabel: 'Delivery Details',
//				name: 'deleverDetils',
//				maxLength: 100,
//				allowBlank: false
//			},
//			{
//				xtype: 'combo',
//				fieldLabel: 'Trancefer Status',
//				name: 'status',
//				maxLength: 100,
//				allowBlank: false,
//				store: 'str',
//				displayField: 'value',
//				valueField: 'status',
//				visible: true,
//            },
//        ];
//	},
//	buildButtons: function () {
//		return [
//			{
//				xtype: 'button',
//				text: 'Submit',
//				action: 'submit_edit',
//				formBind: true
//            },
//			{
//				xtype: 'button',
//				text: 'Cancel',
//				action: 'work_order_transfer_cancel'
//            },
//
//        ];
//	},
//	buildDockedItems: function () {
//		return [];
//	},
//
//	buildPlugins: function () {
//		return [];
//	}
//});