Ext.define('singer.view.ServiceModule.QuickWorkOrderCollector.WorkOrderCollectorGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.workordercollectorgrid',
    height: 300,
    width: 1000,
    layout: 'fit',
    store: 'WorkOrderCollector',
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }

                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'WorkOrderCollector',
                dock: 'bottom',
                displayInfo: true
            }
        ];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'Work Order No',
                dataIndex: 'workOrderNo',
//                width : 150
            },
            {
                xtype: 'searchablecolumn',
                text: 'IMEI',
                dataIndex: 'imeiNo',
//                width : 200
            },
            {
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'product',
//                width : 250
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modleDesc',
//                width : 250
            },
            {
                xtype: 'searchablecolumn',
                text: 'NIC',
                dataIndex: 'customerNic'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Customer Name',
                dataIndex: 'customerName'
            },
            //New add grid coloumns
            {
                xtype: 'searchablecolumn',
                text: 'Contact Number',
                dataIndex: 'workTelephoneNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Email',
                dataIndex: 'email'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Defect',
                dataIndex: 'defectNo'
            },
//            {
//                xtype: 'searchablecolumn',
//                text: 'Customer Complain',
//                dataIndex: 'invoice'
//            },
            {
                xtype: 'searchablecolumn',
                text: 'Accesorries',
                dataIndex: 'assessoriesRetained'
            },
            {
                xtype: 'searchablecolumn',
                text: 'POD No',
                dataIndex: 'podNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Collector Name',
                dataIndex: 'collectorName'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Collector NIC',
                dataIndex: 'collectorNic'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Collector Tel',
                dataIndex: 'contactNo'
            },
//            {
//                xtype: 'actioncolumn',
//                width: 100,
//                header: 'View',
//                align:'center',
//                //width: 100,
//                flex:1,
//                items: [{
//                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
//                        tooltip: 'View',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('viewinfo', grid, record);
////                            ////console.log(this.up('grid'));
//                        }
//                    }]
//            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Edit ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            console.log(this.up('grid'));
                            this.up('grid').fireEvent('editquickeditcollector', grid, record);
                        }


                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Print ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/printerr.png', // Use a URL in the icon config
                        tooltip: 'Print',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('printquickeditcollector', grid, record);

                        }


                    }]
            }

        ];
    }


});








