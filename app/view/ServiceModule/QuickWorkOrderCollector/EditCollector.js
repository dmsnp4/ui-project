

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.ServiceModule.QuickWorkOrderCollector.EditCollector', {
    extend: 'Ext.window.Window',
    alias: 'widget.editcollector',
    loadMask: true,
    modal: true,
//    iconCls: 'icon-user',
    title: 'Add New Quick Order Collector',
//    store: 'Status',
//    modal: true,
    layout: 'fit',
    constrain: true,
    width: 600,
    height: 320,
    initComponent: function () {
        console.log('add form');
        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 300,
//            loadMask: true,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });
        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
    },
    buildItems: function () {

        return [

            {
                xtype: 'textfield',
                fieldLabel: 'POD No',
                //dataIndex: 'podNo'
                name: 'podNo',
                action: 'txtPodno'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Collector Name',
                name: 'collectorName',
                action: 'txtCollctorName',
                allowBlank : false
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Collector NIC',
                name: 'collectorNic',
                regex: /^[0-9]{9}[vVxX]$/,
                minLength: 10,
                maxLength: 10,
                action: 'txtCollctorNic'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Collector Tel',
                name: 'contactNo',
                regex: /^[0-9_]+$/,
                maxLength: 10,
                minLength: 10,
                action: 'txtCollctorTel'
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Service Agent Name',
                name: 'serviceCenter',
                allowBlank : true
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Service Agent Address',
                name: 'serviceCenterAddress',
                allowBlank : true
            }

        ];
    },
    buildButtons: function () {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'editcollector-save',
                formBind: true,
                hidden: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'editcollector-create',
                formBind: true,
                hidden: false
            }

        ];
    },
    buildPlugins: function () {
        return [];
    }

});
