

Ext.define('singer.view.ServiceModule.RepairPaymentsApprove.DeductAmountInput', {
    extend: 'Ext.window.Window',
    alias: 'widget.deductAdd',
    layout: 'fit',
    title:'Input Deduct Amount',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 300,
    
    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
            {
                fieldLabel: 'Deduct Amount',
                name: 'deductAmount',
                allowBlank : false,
                regex: /[0-9_]/,
                maxLength:6,
                id:'deductionAmount'
            }
        ];
    },
    buildButtons: function () {
        return [
             {
                xtype: 'button',
                text: 'Submit',
                action: 'submit',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});
