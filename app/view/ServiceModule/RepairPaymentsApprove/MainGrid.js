Ext.define('singer.view.ServiceModule.RepairPaymentsApprove.MainGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.RepairPaymentsApproveGrid',
    store: 'RepairPayment',
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },
    buildColumns: function () {
        return [
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Payment ID',
                dataIndex: 'paymentId'
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Service Franchise',
                dataIndex: 'setviceName'
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Payment Total(Rs.)',
                dataIndex: 'paymentPrice',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Ref Date',
                dataIndex: 'refDate'
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'PO Number',
                dataIndex: 'poNumber',
                renderer: function (value) {
                    if (value === "" || value === null) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'PO Date',
                dataIndex: 'poDate',
                renderer: function (value) {
                    if (value === "" || value === null) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Cheque No',
                dataIndex: 'chequeNo',
                renderer: function (value) {
                    if (value === "" || value === null) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Cheque Date',
                dataIndex: 'chequeDate',
                renderer: function (value) {
                    if (value === "" || value === null) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
//                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Status',
                dataIndex: 'status',
                renderer: function (value) {
                    switch (value) {
                        case 1 :
                            return 'Pending';
                            break;

                        case 2 :
                            return 'Approved';
                            break;

                        case 3 :
                            return 'Rejected';
                            break;

                        case 4 :
                            return 'Settled';
                            break;

                        case 5 :
                            return 'Paid';
                            break;

                        default :
                            return "";
                            break;
                    }
                }
            },
//            {
////                flex: 1,
//                xtype: 'searchablecolumn',
//                header: 'Service Franchise',
//                dataIndex: 'setviceName'
//            },
            {
//                flex: 1,
                xtype: 'actioncolumn',
                header: 'Authorize',
                align: 'center',
                //width: 100,
                items: [{
                        icon: 'resources/images/approved.png', // Use a URL in the icon config
                        tooltip: 'Approve',
                        handler: function (grid, rowIndex, colIndex) {
                            console.log("sds");
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('authorize', grid, record);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'View',
                align: 'center',
                flex: 1,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('aprove', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'Print Report',
                menuDisabled: true,
                align: 'center',
                width: 100,
                items: [{
                        icon: 'resources/images/printerr.png', // Use a URL in the icon config
                        tooltip: 'Print Report',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            var payId = record['data']['paymentId'];
                            this.up('grid').fireEvent('PrintReceiptForWOSummary',payId);
                            console.log(this.up('grid'));
                        }
                    }]
            }

        ];
    },
    buildDockedItems: function () {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }

                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'RepairPayment',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function () {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});


