Ext.define('singer.view.ServiceModule.RepairPaymentsApprove.SelectOrUnselectWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.SlectionW',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: '70%',
    height: '80%',
    initComponent: function() {
        var repair = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 430,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 140,
                margin: '10 0 10 0'
            },
            items: repair.buildItems(),
            buttons: repair.buildButtons()
        });
        repair.items = [formPanel];
        repair.callParent();
    },
    buildItems: function() {

        return [
            {
                // id:'WOno',
                fieldLabel: 'Work Order No:',
                name: 'workOrderNo'
            },
            {
                // id:'Wverification',
                fieldLabel: 'Warrenty verification:',
                name: 'warrentyTypeDesc'
            },
            {
                // id:'wrnty',
                fieldLabel: 'Warrenty:',
                name: ''
            },
            {
                // id:'nonWrnty',
                fieldLabel: 'Non-Warrenty:',
                name: ''
            },
            {
                // id:'techn',
                fieldLabel: 'Technician:',
                name: 'tecnician'
            },
            {
                // id:'dfct',
                fieldLabel: 'Defect:',
                name: 'defectDesc'
            },
            {
                // id:'action',
                fieldLabel: 'Action Taken:',
                name: 'actionTake'
            },
            {
                // id:'imeei',
                fieldLabel: 'IMEI:',
                name: 'imeiNo'
            },
            {
                // id:'imeei',
                fieldLabel: 'Amount (Rs.):',
                name: 'amount'
            },
            {
                // id:'inviceNo',
                fieldLabel: 'Invoice No:',
                name: 'referenceNo'
            },
            {
                xtype: 'label',
                text: 'Spare Parts',
                style: "float: left; margin: 0px 0px 10px 0px;"
            },
            {
                margin: '0 0 30 0',
                xtype: 'grid',
                overflowY: 'auto',
                frame: false,
                border: true,
                height: 200,
                columns: [
                    {
                        text: 'Spare Part',
                        dataIndex: 'workOrderNo',
                        flex: 4
                    },
                    {
                        text: 'Third party used',
                        dataIndex: 'workOrderNo',
                        flex: 3
                    },
                    {
                        text: 'Invoice No',
                        dataIndex: 'warrentyTypeDesc',
                        flex: 3
                    },
                    {
//                        xtype: 'searchablecolumn',
                        text: 'Price(Rs)',
                        dataIndex: 'imeiNo',
                        flex: 2
                    }
                ]
            },
            {
                xtype: 'radiogroup',
                fieldLabel: 'Status',
                columns: 1,
                vertical: true,
                id: 'select_or_unselect_radio_grp',
                hidden: true,
                items: [
                    {boxLabel: 'Add Work order', name: 'status', inputValue: '1'},
                    {boxLabel: 'Remove Work order', name: 'status', inputValue: '0'}
                ]
            }
        ];
    },
    buildButtons: function() {
        return [{
                text: 'Cancel',
                action: 'submit_cancel'
            }, '->', {
                text: 'Save',
                action: 'submit_selection'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});









