Ext.define('singer.view.ServiceModule.RepairPaymentsApprove.ApprovalForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.appove',
    layout: 'fit',
    autoScroll: true,
    store: 'ReplacementReturnStore',
    modal: true,
    constrain: true,
    loadMask: true,
    width: "70%",
    height: "80%",
    requires: [
        'Ext.selection.CellModel',
        'Ext.data.*',
        'Ext.form.*'
    ],
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

//        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
//            clicksToEdit: 1
//        });

        newForm.items = [formPanel];
        newForm.callParent();
    },
    buildItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: 'paymentId'
            },
            {
                id: 'paymentStatus',
                xtype: 'hiddenfield',
                name: 'status'
            },
            {
                fieldLabel: 'Service Franchise',
                name: 'setviceName',
                xtype: "textfield",
                id: 'serviceFranchiseID', //sh
                hidden: true,
                editable: false
            },
            {
                xtype: 'moneyfield',
                fieldLabel: 'Requested Payment(Rs.)',
                name: 'paymentPrice',
                //id : "paymentPricePaymentApprove",
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'moneyfield',
                fieldLabel: 'Settle Payment(Rs.)',
                name: 'settleAmount',
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'label',
                text: 'Work Orders',
                style: "float: left; margin: 0px 0px 10px 0px;",
                hidden: true
            },
            {
                margin: '0 0 20 0',
                xtype: 'grid',
                id: "WorkOrderPaymentListwithPriceGrid",
                store: 'WorkOrderPaymentListwithPrice',
                overflowY: 'auto',
                frame: false,
                border: true,
                height: 200,
                hidden: true,
                width: "60%", //800,
                columns: [
                    {
                        text: 'Work Order no',
                        dataIndex: 'workOrderNo',
                        flex: 2,
                        xtype: 'searchablecolumn'
                    },
                    {
                        text: 'Warranty Verification',
                        dataIndex: 'warrentyTypeDesc',
                        flex: 2,
                        xtype: 'searchablecolumn'
                    },
                    {
                        text: 'IMEI',
                        dataIndex: 'imeiNo',
                        flex: 2,
                        xtype: 'searchablecolumn'
                    },
                    {
                        text: 'Defect',
                        dataIndex: 'defectDesc',
                        flex: 2
                    },
                    {
                        text: 'Amount',
                        dataIndex: 'amount',
                        flex: 2
                    },
                    {
                        text: 'Delay Days',
                        dataIndex: 'delayDays',
                        flex: 2
                    },
                    {
                        text: 'Deduct Amount',
                        dataIndex: 'deductionAmount',
                        flex: 2
                    },
                    {
                        xtype: 'actioncolumn',
                        header: 'Deduct Amount',
                        //width: 40,
                        flex: 2,
                        stateful: true,
                        id: 'deduct-Amount-action',
                        items: [{
                                icon: 'resources/images/Select.png', // Use a URL in the icon config
                                tooltip: 'click to add Deduct amount',
                                handler: function (grid, rowIndex, colIndex) {
                                    var record = (this.up('grid').getStore().getAt(rowIndex));

                                    this.up('window').fireEvent('deductAmt', grid, record);
                                }
                            }]
                    },
                    {
                        xtype: 'actioncolumn',
                        header: 'Cancel Deduct Amount',
                        //width: 40,
                        flex: 2,
                        id: 'cancel-deduct-Amount-action',
                        items: [{
                                icon: 'resources/images/clear-icon.png', // Use a URL in the icon config
                                tooltip: 'click to cancel Deduct amount',
                                handler: function (grid, rowIndex, colIndex) {
                                    var record = (this.up('grid').getStore().getAt(rowIndex));

                                    this.up('window').fireEvent('onWorkorderDeduct', grid, record);
                                }
                            }]
                    },
                    {
                        text: 'Active',
                        dataIndex: 'status',
                        xtype: 'checkcolumn',
//                      width: 50,
                        flex: 2,
                        name: 'ActiveChangeColumn',
                        id: 'ActiveChangeColumn',
                        hidden: true,
                        listeners: {
                            checkchange: function (column, rowIndex, checked, eOpts) {
                                console.log('check');
                                console.log(checked);
                                var record = (this.up('grid').getStore().getAt(rowIndex));


                                this.up('window').fireEvent('onWorkorderChange', checked, record, rowIndex);
                            }
                        }
                        //hidden : (Ext.getCmp("paymentStatus").getValue()=="1" ? false : true)
                    },
//                    {
//                        text: 'Deduct',
//                        dataIndex: '',
//                        xtype: 'checkcolumn',
//                        width: 50,
//                        name : 'deductCheck',
//                        id: 'deduct-Amount-check',
//                        hidden: false,
//                        listeners: {
//                            checkchange: function (column, rowIndex, checked, eOpts) {                                
//                                var record = (this.up('grid').getStore().getAt(rowIndex));
//                                this.up('window').fireEvent('onWorkorderDeduct', checked, record, rowIndex);
//                            }
//                            
//                        }
//                        //hidden : (Ext.getCmp("paymentStatus").getValue()=="1" ? false : true)
//                    }
//                    {
//                        xtype: 'actioncolumn',
//                        header: 'Add', //'Cancel Or Unselect',
//                        width: 100,
//                        flex: 2,
//                        items: [{
//                                icon: 'resources/images/clear-icon.png', // Use a URL in the icon config
//                                tooltip: 'Cancel Or Unselect',
//                                handler: function(grid, rowIndex, colIndex) {
//                                    var record = (grid.getStore().getAt(rowIndex));
//                                    this.up('form').up('form').fireEvent('unselectt', grid, record, rowIndex);
//                                }
//                            }]
//                    },
//       
////////********************this was removed***********
//                    {
//                        xtype: 'actioncolumn',
//                        header: 'View', //'Cancel Or Unselect',
//                        width: 150,
////                        flex: 2,
//                        items: [{
//                                icon: 'resources/images/view-icon.png', // Use a URL in the icon config
//                                tooltip: 'View more',
//                                handler: function(grid, rowIndex, colIndex) {
//                                    var record = (grid.getStore().getAt(rowIndex));
//                                    this.up('window').fireEvent('viewmore', grid, record, rowIndex);
//                                }
//                            }]
//                    }
                ]
            },
            {
                xtype: 'textfield',
                fieldLabel: 'PO Number',
                name: 'poNumber',
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'datefield',
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                fieldLabel: 'PO Date',
                name: 'poDate',
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cheque Number',
                name: 'chequeNo',
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'datefield',
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                fieldLabel: 'Cheque Date',
                name: 'chequeDate',
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cheque Collected Details',
                name: 'chequeDetails',
                readOnly: true,
                hidden: true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Remarks',
                id: "paymentApprovingRemarks",
                name: 'remarks',
                readOnly: true,
                hidden: true
            }

        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'exit_form',
                id: 'repair_payment_approve_form_cancel',
                hidden: true
            },
            '->',
            {
                xtype: 'button',
                text: 'Approve',
                action: 'accept',
                id: 'repair_payment_approve_form_approve',
                formBind: true,
                hidden: true
            },
//            {
//                xtype: 'button',
//                text: 'Reject',
//                action: 'reject',
//                id: 'repair_payment_approve_form_reject',
//                formBind: true,
//                hidden: true
//            },
            {
                xtype: 'button',
                text: 'Settle Payment',
                action: 'settle',
                id: 'repair_payment_approve_form_settle',
                formBind: true,
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Save',
                action: 'finish',
                id: 'repair_payment_approve_form_save',
                formBind: true,
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Save Without Approve',
                id: "Save-Without-Approve",
                action: 'tempsave',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Close',
                action: 'close',
                id: 'repair_payment_approve_form_close',
                hidden: true
            }

        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});







