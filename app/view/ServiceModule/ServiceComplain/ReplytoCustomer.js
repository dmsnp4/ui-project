

Ext.define('singer.view.ServiceModule.ServiceComplain.ReplytoCustomer', {
    extend: 'Ext.window.Window',
    alias: 'widget.rplyView',
    layout: 'fit',
    title:'Service Complain',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    
    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
//            {xtype: 'hiddenfield',name: 'bisId',id:'businessId'},
            {xtype: 'hiddenfield',name: 'feedBackId',id:'feedBId'},
            {xtype: 'hiddenfield',fieldLabel: 'customer Name',name: 'customerName',id:'cussnme'},
            {xtype: 'hiddenfield',fieldLabel: 'email',name: 'email',id:'emaill'},
            {fieldLabel: 'Work Order',name: 'workOrderNo',id:'wrkorderr'},
            {fieldLabel: 'Product',name: 'prduct',id:'sProduct'},
            {fieldLabel: 'Model',name: 'modelName',id:'mdle'},
            {fieldLabel: 'Work Order FeedBack Type',name: 'feedBackType',id:'workcomplaintype'},
            {fieldLabel: 'FeedBack',name: 'feedbackTypeDesc',id:'wrkcomplain'},
            {xtype: 'textareafield',fieldLabel: 'Reply',name: 'woReply',alowBlank:false},
        ];
    },
    buildButtons: function () {
        return [
             {
                xtype: 'button',
                text: 'Submit',
                action: 'submit',
                formBind: true
            }
//            {
//                xtype: 'button',
//                text: 'Cancel',
//                action: ''
//            },
           
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});
