
Ext.define('singer.view.ServiceModule.ServiceComplain.Maingrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.complainGrid',
    loadMask: true,
    store: 'serviceComplain',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var grid = this;
        grid.columns = grid.buildColumns();
        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'
                          }]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'serviceComplain',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function () {

        return [
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Complain No',
                dataIndex: 'feedBackId'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Complain Date',
                dataIndex: 'complainDate'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Complain Type',
                dataIndex: 'feedbackTypeDesc'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Product',
                dataIndex: 'prduct'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modelName'
            },
            
//            {"workOrderNo":"WO000005",
//            "feedBackId":1,
//            "feedBackType":0,
//            "feedBack":"No Attention",
//            "prduct":"Smartphone",
//            "modelName":"Ascend P6",
//            "feedbackTypeDesc":"Hard",
//            "user":null,
//            "complainDate":null,
//            "purchaseDate":"2014/12/04",
//            "purchaseShop":"Singer (Sri Lanka) PLC.",
//            "serviceCenter":"Distributor CN A",
//            "woOpenDate":"2014/12/08",
//            "deleveryDate":"2014/12/10",
//            "status":1,
//            "customerName":"ruwan",
//            "telephoneNo":"1234567890",
//            "address":"malabe",
//            "imeiNo":"100000000000041",
//            "email":"rua@gmail.com",
//            "customerNic":"123456789V",
//            "warrenty":"One to One Warrenty",
//            "rccRefe":"hdh"}
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Purchased shop',
                dataIndex: 'purchaseShop'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Date Of Purchased',
                dataIndex: 'purchaseDate'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Work Order Quick',
                dataIndex: 'workOrderNo'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Service Center',
                dataIndex: 'serviceCenter'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Work Order Open Date',
                dataIndex: 'woOpenDate'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Work Order Delivery Date',
                dataIndex: 'deleveryDate'
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                text: 'Work Order Status',
                dataIndex: 'warrenty'
            },
             {
                width:150,
//                xtype: 'searchablecolumn',
                text: 'Reply Status',
                dataIndex: 'replyStatus'
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'View ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View ',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('onView', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Reply ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Reply',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('onRply', grid, rec);
                        }
                    }]
            }

        ];
    }
});


