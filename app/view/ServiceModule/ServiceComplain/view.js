
Ext.define('singer.view.ServiceModule.ServiceComplain.view', {
    extend: 'Ext.window.Window',
    alias: 'widget.complainview',
    modal: true,
    constrain: true,
    layout: 'fit',
    id:'viewForm',
    width: 430,
    height:'80%',
    initComponent: function () {
        var WOclose = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: WOclose.buildItems(),
            buttons: WOclose.buildButtons()
        });
        WOclose.items = [formPanel];
        WOclose.callParent();
    },
    buildItems: function () {

        return [
            {
                id:'fbck',
                xtype:'hiddenfield',
                name: 'feedBackId',
            },
            {
                fieldLabel: 'Customer Name:',
                name: 'customerName',
                id:'custerName',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                xtype: 'hiddenfield',
                fieldLabel: 'E-mail:',
                name: 'email',
                id:'maill'
            },
            {
                fieldLabel: 'Address:',
                name: 'address'
            },
            {
                fieldLabel: 'Telephone No:',
                name: 'telephoneNo'
            },
            {
                fieldLabel: 'NIC:',
                name: 'customerNic'
            },
            {
                fieldLabel: 'IMEI:',
                name: 'imeiNo'
            },
            {
                fieldLabel: 'Product:',
                name: 'prduct',
                id:'gProduct'
            },
            {
                fieldLabel: 'Model:',
                name: 'modelName',
                id:'mdllle'
            },
            {
                fieldLabel: 'Date Of Purchased:',
                name: 'purchaseDate'
            },
            {
                fieldLabel: 'Purchased Shop:',
                name: 'purchaseShop'
            },
            {
                fieldLabel: 'Warrenty Details:',
                name: 'warrenty'
            },
            {
                fieldLabel: 'Work Order Or RCC:',
                name: 'rccRefe',
                id:'orderORrcc'
            },
            {
                fieldLabel: 'Work Order Opened Date:',
                name: 'woOpenDate'
            },
            {
                fieldLabel: 'Work Order Deleivery date:',
                name: 'deleveryDate'
            },
            {
                fieldLabel: 'Service Center:',
                name: 'serviceCenter'
            },
//            {
//                fieldLabel: 'Repair Status:',
//                name: 'status'
//            },
            {
                fieldLabel: 'Work Order Feedback type:',
                name: 'feedbackTypeDesc',
                id:'typeCmlain'
            },
            {
                fieldLabel: 'FeedBack:',
                name: 'feedBack',
                id:'complin'
            }
            
        ];
    },
    

    buildButtons: function () {
        return ['->', 
                {
                    text: 'Reply',
                    action: 'rplyOnView' 
                }
               ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});






