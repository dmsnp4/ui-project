Ext.define('singer.view.ServiceModule.WorkOrderMaintenace.MainGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.wrkOrdrMaintneGrid',
    store: 'GetWorkOrderList',
    minHeight: 200,
    selType: 'rowmodel',

    initComponent: function () {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },

    buildColumns: function () {
        return [

            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Work Order No',
                dataIndex: 'workOrderNo',
            },
//            {
//                width: 150,
//                xtype: 'searchablecolumn',
//                header: 'Warranty Status ',
//                dataIndex: 'warrantyTypedesc',
//            }
//            ,
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Technician',
                dataIndex: 'technician'
            },
//            {
//                width: 150,
//                xtype: 'searchablecolumn',
//                header: 'Major Defect',
//                //                dataIndex: ''
//            },
//            {
//                width: 150,
//                xtype: 'searchablecolumn',
//                header: 'Minor Defect',
//                //                dataIndex: 'technician'
//            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'IMEI',
                dataIndex: 'iemiNo'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Model',
                dataIndex: 'modelDesc'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Repair Level',
                dataIndex: 'levelDesc'
            },
//            {
//                width: 150,
//                xtype: 'searchablecolumn',
//                header: 'Estimate Status',
//                dataIndex: 'estimateStatusMsg'
//            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Status',
                dataIndex: 'statusMsg'
            },
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Found In',
                dataIndex: 'transferStatus'
            },
            {
                xtype: 'actioncolumn',
                header: 'Estimate',
                menuDisabled: true,
                width: 100,
                items: [{
                    icon: 'resources/images/edit.png', // Use a URL in the icon config
                    tooltip: 'Estimate',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = (grid.getStore().getAt(rowIndex));
                        this.up('grid').fireEvent('estimate', grid, record, rowIndex);
                        //                            ////console.log(this.up('grid'));

                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        // Returns true if 'editable' is false (, null, or undefined)
                        //console.log(record.get('warrantyVrifType'))
                        // this is changed according to the kasun's request
//                        if (record.get('warrantyVrifType') === 1 ||record.get('repairStatus') === '2' || record.get('repairStatus') === '3'  ) {
                        if (record.get('repairStatus') === '2' || record.get('repairStatus') === '3' ) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'Delete',
                menuDisabled: true,
                width: 100,
                items: [{
                    icon: 'resources/images/delete-icon.png', // Use a URL in the icon config
                    tooltip: 'Delete',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = (grid.getStore().getAt(rowIndex));
                        this.up('grid').fireEvent('del', grid, record, rowIndex);
                        //                            ////console.log(this.up('grid'));

                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
//                        //console.log(record)
                        // Returns true if 'editable' is false (, null, or undefined)
                        if (record.get('repairStatus') === '2' || record.get('repairStatus') === '3') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            }, {
                xtype: 'actioncolumn',
                header: 'Edit',
                width: 100,
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/edit.png', // Use a URL in the icon config
                    tooltip: 'Edit',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = (grid.getStore().getAt(rowIndex));
                        this.up('grid').fireEvent('editt', grid, record);
                        //                            ////console.log(this.up('grid'));
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        // Returns true if 'editable' is false (, null, or undefined)
                        if (record.get('repairStatus') === '2' || record.get('repairStatus') === '3') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'View',
                width: 100,
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                    tooltip: 'view',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = (grid.getStore().getAt(rowIndex));
                        this.up('grid').fireEvent('vieww', grid, record);
                        //                            ////console.log(this.up('grid'));
                    }
                    }]
            }

        ];
    },

    buildDockedItems: function () {
        return [{
            xtype: 'form',
            bodyPadding: 2,
            frame: false,
            border: false,
            fieldDefaults: {
                labelWidth: 100,
                labelAlign: 'top'
            },

            buttons: [
                {
                    text: 'Clear Search',
                    action: 'clearSearch'
                    }
                    , '->', {
                    iconCls: 'icon-search',
                    text: 'Add New',
                    action: 'addNew',

                    }
                ]
            }, {
            xtype: 'pagingtoolbar',
            store: 'GetWorkOrderList',
            dock: 'bottom',
            displayInfo: true
            }];
    },
    buildPlugins: function () {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});