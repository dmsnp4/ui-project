//MaintenanceView
//wrkOmaintenaceView
Ext.create('Ext.data.Store', {
    id: 'str',
    fields: ['status', 'value'],
    data: [
        {
            "status": 1,
            "value": "A"
        },
        {
            "status": 2,
            "value": "B"
        }
    ]
});

Ext.define('singer.view.ServiceModule.WorkOrderMaintenace.MaintenanceView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.wrkOmaintenaceView',
    layout: 'fit',
    modal: true,
    constrain: true,
    //    loadMask: true,
    width: 700,
    height: 600,
    //    store: '',
    requires: [
        'Ext.data.*',
        'Ext.form.*'
    ],
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0',
                readOnly:true
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];
        //        newForm.button=newForm.buildButtons();
        newForm.callParent();
    },
    buildItems: function () {
        ////console.log('build items');
        return [
            {
                xtype: 'container',
                layout: 'hbox',
                defaultType: 'textfield',
                items: [
                    {
                        id: 'wrkN',
                        margin: '10 0 10 10',
                        fieldLabel: 'Work Order No',
                        name: 'workOrderNo',
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false
                    },
//                    {
//                        margin: '10 0 10 20',
//                        xtype: 'button',
//                        text: 'Select Work Order',
//                        action: 'select_Work_Order',
////                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                        allowBlank: false
//                    },
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Maintain Detail',
                defaultType: 'textfield',
                layout: 'anchor',
                id: 'addnewform-fields',
                defaults: {
                    anchor: '100%'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
//                        defaultType: 'textfield',
                        items: [
//                            {
//                                margin: '0 0 5 0',
//                                xtype: 'combo',
//                                name: 'warrantyVrifType',
//                                fieldLabel: 'Warranty',
//                                labelWidth: 100,
//                                width: 250,
////                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                                allowBlank: false,
//                                //                                        allowBlank: false,
//                                store: 'WrrntyType',
//                                displayField: 'description',
//                                valueField: 'code',
//                                visible: true,
//                                queryMode: 'remote',
//                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                                allowBlank: false,
//                            },
//                            {
//                                margin: '0 0 5 20',
//                                xtype: 'combo',
//                                name: 'nonWarrantyType',
//                                labelWidth: 100,
//                                width: 250,
//                                fieldLabel: 'Non Warranty',
////                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                                allowBlank: false,
//                                //                                        allowBlank: false,
//                                store: 'NonWrrntyType',
//                                displayField: 'description',
//                                valueField: 'code',
//                                visible: true,
//                                queryMode: 'remote',
//                            },
                        ]
                    },
                    {
                        name: 'tecnician',
                        fieldLabel: 'Technician',
                        flex: 2,
                        emptyText: 'Technician Name',
                        name: 'technician',
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false,
                        //                        allowBlank: false
                    },
//                    {
//                        margin: '30 0 10 0',
//                        xtype: 'button',
//                        action: 'add_defect',
//                        text: 'Add Defects',
////                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                        allowBlank: false,
//                    },
                    {
                        margin: '0 0 30 0',
                        xtype: 'grid',
                        frame: false,
                        id: 'testGrid',
                        store: 'DefectStore',
                        border: true,
                        columns: [
                            {
                                id: 'majorClmn',
                                text: 'Major Defect',
                                dataIndex: 'majDesc',
                                flex: 1
                            },
                            {
                                id: 'minorClmn',
                                text: 'Minor Defect',
                                dataIndex: 'minDesc',
                                flex: 1
                            }
                        ],
                    },
                    {
                        xtype: 'textareafield',
                        fieldLabel: 'Action Taken',
                        name: 'actionTaken',
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false,
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'IMEI',
                        name: 'iemiNo',
                        id: 'addnewform-imeiNo',
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false,
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Invoice No',
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false,
                    },
//                    {
//                        margin: '30 0 10 0',
//                        xtype: 'button',
//                        action: 'spare_parts',
//                        text: 'Add Spare Parts',
////                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                        allowBlank: false,
//                    },
                    {
                        margin: '0 0 30 0',
                        xtype: 'grid',
                        frame: false,
                        store: 'sparePrtStr',
                        id: 'spareN',
                        border: true,
                        columns: [
                            {
                                text: 'Spare Part',
                                dataIndex: 'partNo',
                                flex: 1
                            },
                            {
                                text: 'Price',
                                dataIndex: 'partSellPrice',
                                flex: 1
                            },
                            {
                                text: 'Third Party Used',
                                dataIndex: 'thirdPartyFlag',
                                flex: 1,
                                renderer: function (value) {
                                    if (value === 1) {
                                        return "Yes";
                                    }
                                    if (value === 0) {
                                        return "No";
                                    }

                                }
                            }
                        ],
//                        listeners: {
//                            added: function (cmp, component, index, eOpts) {
//                                //console.log(index);
//
//                            }
//                        }
                    },
//                    {
//                        xtype: 'combo',
//                        fieldLabel: 'Repair Level',
//                        store: 'ReplevelSTR',
//                        displayField: 'levelDesc',
//                        valueField: 'levelId',
//                        visible: true,
//                        name: 'repairLevelId',
//                        action: 'rprLevel',
//                        queryMode: 'remote',
////                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                        allowBlank: false,
//                    },
                     {
                        xtype: 'combo',
                        fieldLabel: 'Repair Level',
                        store: 'ReplevelSTR',
                        displayField: 'levelName',
                        valueField: 'levelId',
                        visible: true,
                        name: 'repairLevelId',
                        action: 'rprLevel',
                        queryMode: 'remote',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
 //                    {
 //                        xtype: 'combo',
 //                        fieldLabel: 'Repair Level',
 //                        store: 'ReplevelSTR',
 //                        displayField: 'levelDesc',
 //                        valueField: 'levelId',
 //                        visible: true,
 //                        name: 'repairLevelId'
 //
 //                        },

                    {
                        id: 'levelPrice',
                        xtype: 'moneyfield',
                        fieldLabel: 'Price for the Repair Level',
                        name: 'repairAmount',
                        readOnly: 'true',
                        listeners: {
                            change: function (cmp, newValue, oldValue, eOpts) {
                                var txt = Ext.getCmp('addnewform-totCost');
                                if (oldValue > -1) {
                                    txt.setValue(parseFloat(txt.getValue()) - parseFloat(oldValue) + parseFloat(newValue));
                                } else {
                                    txt.setValue(parseFloat(txt.getValue()) + parseFloat(newValue));
                                }
                            }
                        }
                    },
 //                    {
 //                        xtype: 'textfield',
 //                        fieldLabel: 'Price for the Repair Level',
 ////                        name: '',
 //
 //                        },
                    {
                        xtype: 'textareafield',
                        fieldLabel: 'Remarks',
                        name: 'remarks',
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name: 'repairStatus',
                        store: 'RepStatus',
                        displayField: 'description',
                        valueField: 'code',
                        visible: true,
                        queryMode: 'remote',
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false,
                    },
                    {
                        fieldLabel: 'Estimated Closing Date',
                        name: 'estimateClosingDate',
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        altFormats: 'm/d/Y',
                        editable: false,
//                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                        allowBlank: false,
                    },
                    {
                        xtype: 'label',
                        text: 'Estimate Details',
                        id: 'addnewform-estTitle',
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Estimate No',
                        readOnly: 'true',
                        id: 'addnewform-estNo',
                        hidden: true,
                        name: 'esRefNo'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Estimate Status',
                        name: 'status',
                        store: 'EstStat',
                        displayField: 'description',
                        valueField: 'code',
                        hidden: true,
                        queryMode: 'remote',
                        id: 'addnewform-estStat'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Customer Reference',
                        //                        readOnly: 'true',
                        id: 'addnewform-cusRef',
                        hidden: true,
                        name: 'customerRef'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Payment Option',
                        name: 'paymentOption',
                        store: 'PayOpt',
                        displayField: 'description',
                        valueField: 'code',
                        hidden: true,
                        queryMode: 'remote',
                        id: 'addnewform-payOpt'
                    },
//                    {
//                        xtype: 'button',
//                        action: 'getEst',
//                        text: 'Generate Estimate',
//                        margin: '2 2 2 2',
//                        hidden: true,
//                        id: 'addnewform-genEst'
//            },
//                    {
//                        xtype: 'button',
//                        action: 'printEst',
//                        text: 'Print Estimate',
//                        margin: '2 2 2 2',
//                        hidden: true,
//                        id: 'addnewform-printEst'
//                    },
//                    {
//                        xtype: 'button',
//                        action: 'mailCus',
//                        text: 'Email to Customer',
//                        margin: '2 2 2 2',
//                        hidden: true,
//                        id: 'addnewform-mailCus'
//                    },
                    {
                        xtype: 'moneyfield',
                        fieldLabel: 'Total Cost',
                        readOnly: 'true',
                        id: 'addnewform-totCost',
                        value: '0'
                    }
                ]
            },
        ];
    },
    buildButtons: function () {
        return [
//            {
//                xtype: 'button',
//                text: 'Submit',
//                action: 'submit_wrkOrdr_Maintence_form',
//                id: 'addnewform-submit',
//                formBind: true
//            },
//            {
//                xtype: 'button',
//                text: 'Save',
//                action: 'save',
//                id: 'addnewform-save',
//                formBind: true
//            },
            {
                xtype: 'button',
                text: 'Done',
                action: 'done',
//                id: '',
                //                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});