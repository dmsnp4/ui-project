

Ext.create('Ext.data.Store', {
    id:'repairStatusStr',
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Waiting For Repair"},
                {"status": 2, "value": "Waiting For Spares"},
                {"status": 3, "value": "Parts In Transits"},
                {"status": 4, "value": "Q.C Pending"},
                {"status": 5, "value": "Repair Complete"},
                {"status": 6, "value": "Can not Repair"},
                {"status": 7, "value": "S/W pending"},
            ]
        });

Ext.define('singer.view.ServiceModule.WorkOrderMaintenace.EditWorkMaintaince', {
    extend: 'Ext.form.Panel',
    alias: 'widget.editForm',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 700,
    height:700,
    store: '',
    requires: [
        'Ext.data.*',
        'Ext.form.*'
    ],
        initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {
        ////console.log('build items');
        return [

            {
               xtype: 'container',
               layout: 'hbox',
               defaultType: 'textfield',
                    items:[
                          {
                            id:'wrkN',
                            margin:'10 0 10 10',
                            fieldLabel: 'Work Order No',
                            name: 'workOrderNo',
//                            allowBlank: false
                          },
                        {
                            margin:'10 0 10 20',
                            xtype:'button',
                            text:'Select Work Order',
                            action:'select_Work_Order'
                        },
                                    ]
            },
            {
                xtype: 'fieldset',
                title: 'Warranty Verification',
                defaultType: 'textfield',
                layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:[
                        {
                           xtype: 'container',
                           layout: 'hbox',
                           defaultType: 'textfield',
                                items:[
                                     {
                                        margin: '0 0 5 0',
                                        xtype:'combo',
                                        name: 'warrentyVrifType',
                                        fieldLabel: 'Warranty',
                                        labelWidth: 100,
                                        width: 250,
//                                        allowBlank: false,
                                        store:'WrrntyType',
                                        displayField: 'description',  
                                        valueField: 'code',
                                        visible: true,
                                    },
                                    {
                                        margin: '0 0 5 20',
                                        xtype:'combo',
                                        name: 'nonWarrentyVrifType',
                                        labelWidth: 100,
                                        width: 250,
                                        fieldLabel: 'Non Warranty',
//                                        allowBlank: false,
                                        store:'NonWrrntyType',
                                        displayField: 'description',  
                                        valueField: 'code',
                                        visible: true,
                                    },
                                    ]
                        },
                        {
                        name: 'tecnician',
                        fieldLabel: 'Technician',
                        flex: 2,
                        emptyText: 'Technician Name',
//                        allowBlank: false
                        },
                        {
                            margin:'30 0 10 0',
                            xtype:'button',
                            action:'add_defect',
                            text:'Add Defects'
                        },
                        
                        {
                            margin:'0 0 30 0',
                            xtype: 'grid',
                
                             frame:false,
                             id: 'testGrid',
                             store: 'newTestStore',
                             border: true,
                             columns: [
                                {id:'majorClmn',text: 'Major Defect', dataIndex: 'mjrDefectDesc',flex: 1},
                                {id:'minorClmn',text: 'Minor Defect', dataIndex: 'minDefectDesc',flex: 1}
                            ],
                        },
                        {
                            xtype:'textareafield',
                            fieldLabel:'Action Taken',
                            name:'actionTaken',
//                            allowBlank: false
                        },
                        {
                            xtype:'textfield',
                            fieldLabel:'IMEI',
                            name:'imeiNo',
//                            allowBlank: false
                        },
                        {
                            xtype:'textfield',
                            fieldLabel:'Invoice No',
                            name:'invoiceNo',
//                            allowBlank: false
                        },
                        {
                            margin:'30 0 10 0',
                            xtype:'button',
                            action:'spare_parts',
                            text:'Add Spare Parts'
                        },
                        
                        {
                            margin:'0 0 30 0',
                             xtype: 'grid',
                             frame:false,
                             store: 'sparePartN',
                             id:'spareN',
                             border: true,
                             columns: [
                                {text: 'Spare Part', dataIndex: 'partNo',flex: 1},
                                {text: 'Price', dataIndex: 'selliingPrice',flex: 1},
                                {text: 'Third Party Used', dataIndex: 'thirdPartyFlag',flex: 1}
                            ],
                        },
                        {
                            xtype:'combo',
                            fieldLabel:'Repair Level',
                            store:'ReplevelSTR',
                            displayField: 'levelName',  
                            valueField: 'levelId',
                            visible: true,
                            name:'levelCatId'
                        },
                        {
                            xtype:'textareafield',
                            fieldLabel:'Remarks',
                            name:'remarks',
                        },
                        //==============================
                         {
                            xtype:'combo',
                            fieldLabel:'Status',
                            name:'repairStatus',
//                            store:'RepStatus',
                            stores:'repairStatusStr',
                            displayField: 'value',  
                            valueField: 'status',
                            visible: true,
                        },
                        {
                            fieldLabel:'Estimated Closing Date',
                            name:'',
                            xtype: 'datefield',
//                            allowBlank: false,
                            format: 'Y-m-d',
                            altFormats: 'm/d/Y',
                            editable: false
                        },
                        {
                            xtype:'displayfield',
                            fieldLabel:'Total Cost: ',
                        }
                    ]
            },
           
            
        ];
    },
    buildButtons: function () {
        return [

            {
                xtype: 'button',
                text: 'Submit',
                action: 'submit_wrkOrdr_Maintence_form',
                id: '',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel_wrkOrdr_Maintence_Form',
                id: '',
//                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});


 




