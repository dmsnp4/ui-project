Ext.define('singer.view.ServiceModule.WorkOrderMaintenace.AddDefects', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addDeffectWindow',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 400,
    store: '',
    requires: [
        'Ext.data.*',
        'Ext.form.*'
    ],
        initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {
        ////console.log('build items');
        return [
            {
                id:'mjrD',
                xtype:'combo',
                fieldLabel:'Major Defect',
                name:'majorCode',
                store:'mjrdefectStr',
                displayField: 'mjrDefectDesc',
                valueField: 'mjrDefectCode',
                visible: true,
                allowBlank:false,
                queryMode: 'remote',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                listeners: {
                            change: function (field, newVal, oldVal) {
                               var store= Ext.getStore('minordefectStr');
                                store.getProxy().url=singer.AppParams.JBOSS_PATH + 'MinorDefects/getMinorDefects?mrjDefectCode='+newVal;
                                store.load();

                            }
                        }
            },
            {
                id:'minD',
                xtype:'combo',
                fieldLabel:'Minor Defect',
                name:'minorCode',
                store:'minordefectStr',
                displayField: 'minDefectDesc',
                valueField: 'minDefectCode',
                visible: true,
                allowBlank:false,
                queryMode: 'remote',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',

            }
        ];
    },
    buildButtons: function () {
        return [

            {
                xtype: 'button',
                text: 'Done',
                action: 'submit_Defect_Codes_form',
                formBind: true
            },
//            {
//                xtype: 'button',
//                text: 'Cancel',
//                action: 'cancel_wrkOrdr_Maintence_Form',
//                id: '',
//                formBind: true
//            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});


 




