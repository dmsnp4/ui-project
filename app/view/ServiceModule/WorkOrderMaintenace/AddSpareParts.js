 Ext.define('singer.view.ServiceModule.WorkOrderMaintenace.AddSpareParts', {
     extend: 'Ext.form.Panel',
     alias: 'widget.addSparePartsWindow',
     layout: 'fit',
     modal: true,
     constrain: true,
     loadMask: true,
     width: 800,
     height: '80%',
     //    store: '',
     requires: [
        'Ext.data.*',
        'Ext.form.*'
    ],
     initComponent: function () {

         var newForm = this;
         var formPanel = Ext.create('Ext.form.Panel', {
             width: 500,
             bodyPadding: 5,
             overflowY: 'auto',
             defaults: {
                 xtype: 'textfield'
             },
             fieldDefaults: {
                 anchor: '100%',
                 labelAlign: 'left',
                 msgTarget: 'side',
                 labelWidth: 180,
                 margin: '10 0 10 0'
             },
             items: newForm.buildItems(),
             buttons: newForm.buildButtons()
         });

         newForm.items = [formPanel];

         newForm.callParent();
     },
     buildItems: function () {
         ////console.log('build items');
         return [
             {
                 id: 'partNo',
                 xtype: 'textfield',
                 fieldLabel: 'Spare Part No',
                 //                name:'partNo'
            },
             {
                 id: 'descrip',
                 xtype: 'textfield',
                 fieldLabel: 'Part Description',
                 //                name:'partDescS'
            },
             {
                 width: 200,
                 margin: '0 0 20 500',
                 xtype: 'button',
                 text: 'Search',
                 action: 'search_spare_partsDetails'
            },
             {
                 margin: '0 0 30 0',
                 xtype: 'grid',
                 frame: false,
                 store: 'sparePrtStr_temp',
                 id: 'spare',
                 border: true,
                 maxHeight: 200,
                 columns: [
                     {
                         text: 'Spare Part',
                         dataIndex: 'erpPartNo',
                         flex: 1
                     },
                     {
                         text: 'Part Description',
                         dataIndex: 'partDesc',
                         flex: 1
                     },
                     {
                         text: 'Price',
                         dataIndex: 'partSellPrice',
                         flex: 1,
                         renderer: function (value) {
                             return Ext.util.Format.number(value, '0,000.00');
                         }
                     },
                     {
                         xtype: 'checkcolumn',
                         //                         action: 'test',
                         text: 'Select',
                         dataIndex: 'select',
                         //                         flex: 1
                     }
                            ],
                        },
             {
                 xtype: 'checkbox',
                 boxLabel: 'Third Party',
                 name: 'chk',
                 listeners: {
                     change: function (cmp, newValue, oldValue, eOpts) {
                         //console.log(newValue);
                         Ext.getCmp('addSparePartsWindow-invoiceNo').setVisible(newValue);
                         Ext.getCmp('addSparePartsWindow-tot').setVisible(newValue);
                         Ext.getCmp('addSparePartsWindow-tot').allowBlank = !newValue;

                     }
                 }
            },
             {
                 xtype: 'container',
                 layout: 'hbox',
                 defaultType: 'textfield',
                 items: [
                     {
                         id: 'addSparePartsWindow-invoiceNo',
                         margin: '10 0 10 0',
                         fieldLabel: 'Invoice No',
                         name: 'invoiceNo',
                         hidden: true
                          },
                     {
                         id: 'addSparePartsWindow-tot',
                         margin: '10 0 10 10',
                         fieldLabel: 'Price(Rs.)',
                         hidden: true,
                         xtype: 'moneyfield',
                         //                         readOnly: true,
                         //                            name: '',
                        },
                                    ]
            }
        ];
     },
     buildButtons: function () {
         return [

             {
                 xtype: 'button',
                 text: 'Done',
                 action: 'submit_Add_SapreParts_form',
                 //                 id: '',
                 formBind: true
            },
 //            {
 //                xtype: 'button',
 //                text: 'Cancel',
 //                action: 'cancel_wrkOrdr_Maintence_Form',
 //                id: '',
 //                formBind: true
 //            }
        ];
     },
     buildDockedItems: function () {
         return [];
     },
     buildPlugins: function () {
         return [];
     }
 });