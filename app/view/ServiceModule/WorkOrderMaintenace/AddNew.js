Ext.create('Ext.data.Store', {
    id: 'repairStatusStr',
    fields: ['status', 'value'],
    data: [
        {"status": 1, "value": "Waiting For Repair"},
        {"status": 2, "value": "Waiting For Spares"},
        {"status": 3, "value": "Parts In Transits"},
        {"status": 4, "value": "Q.C Pending"},
        {"status": 5, "value": "Repair Complete"},
        {"status": 6, "value": "Can not Repair"},
        {"status": 7, "value": "S/W pending"},
    ]
});

Ext.define('singer.view.ServiceModule.WorkOrderMaintenace.AddNew', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addnewform',
    layout: 'fit',
    modal: true,
    constrain: true,
    //    loadMask: true,
    width: 700,
    height: 600,
    //    store: '',
    requires: [
        'Ext.data.*',
        'Ext.form.*'
    ],
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];
        //        newForm.button=newForm.buildButtons();
        newForm.callParent();
    },
    buildItems: function () {
        ////console.log('build items');
        return [
            {
                xtype: 'container',
                layout: 'hbox',
                defaultType: 'textfield',
                items: [
                    {
                        id: 'wrkN',
                        margin: '10 0 10 10',
                        fieldLabel: 'Work Order No',
                        name: 'workOrderNo',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                        readOnly: true
                    },
                    {
                        margin: '10 0 10 20',
                        xtype: 'button',
                        id:'selectWoBtn',
                        text: 'Select Work Order',
                        action: 'select_Work_Order',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false
                    },
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Maintain Detail',
                defaultType: 'textfield',
                layout: 'anchor',
                id: 'addnewform-fields',
                defaults: {
                    anchor: '100%'
                },
                items: [
//                    {
//                        xtype: 'container',
//                        layout: 'hbox',
//                        defaultType: 'textfield',
//                        items: [
//                            {
//                                margin: '0 0 5 0',
//                                xtype: 'combo',
//                                name: 'warrantyVrifType',
//                                fieldLabel: 'Warranty',
//                                labelWidth: 100,
//                                width: 250,
//                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                                //                                allowBlank: false,
//                                //                                        allowBlank: false,
//                                store: 'WrrntyTypeMaintain', //'WrrntyType',
//                                displayField: 'description',
//                                valueField: 'code',
//                                visible: true,
//                                queryMode: 'remote',
//                                //                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                                allowBlank: false,
//                                id: 'addnewform-underWar'
//                            },
//                            {
//                                margin: '0 0 5 20',
//                                xtype: 'combo',
//                                name: 'nonWarrantyType',
//                                labelWidth: 100,
//                                width: 250,
//                                fieldLabel: 'Non Warranty',
//                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                                //                                allowBlank: false,
//                                //                                        allowBlank: false,
//                                store: 'NonWrrntyType',
//                                displayField: 'description',
//                                valueField: 'code',
//                                visible: true,
//                                queryMode: 'remote',
//                                id: 'addnewform-nonWar'
//                            },
//                        ]
//                    },
                    {
                        name: 'technician',
                        fieldLabel: 'Technician',
                        flex: 2,
                        readOnly: true,
                        id: 'technicianName',
                        emptyText: 'Technician Name',
                        //                        name: 'technician',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                        //                        id:'addnewform-technician'
                        //                        allowBlank: false
                    },
                    {
                        margin: '30 0 10 0',
                        xtype: 'button',
                        action: 'add_defect',
                        text: 'Add Defects',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
                    {
                        margin: '0 0 30 0',
                        xtype: 'grid',
                        frame: false,
                        id: 'testGrid',
                        store: 'DefectStore',
                        border: true,
                        columns: [
                            {
                                xtype: 'actioncolumn',
                                text: '',
                                align: 'center',
                                flex: 0.5,
                                items: [{
                                        icon: 'resources/images/delete-icon.png',
                                        tooltip: 'Delete',
                                        handler: function (grid, rowIndex, colIndex) {
                                            var rec = grid.getStore().getAt(rowIndex);
                                            var me = this;
                                            Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function (button) {
                                                if (button == 'yes') {
                                                    grid.getStore().removeAt(rowIndex);
                                                    me.up('window').fireEvent('remDevIsuItem');
                                                } else {
                                                    return false;
                                                }
                                            });

                                        }
                                    }]

                            },
                            {
                                id: 'majorClmn',
                                text: 'Major Defect',
                                dataIndex: 'majDesc',
                                flex: 1
                            },
                            {
                                id: 'minorClmn',
                                text: 'Minor Defect',
                                dataIndex: 'minDesc',
                                flex: 1
                            }
                        ],
                    },
                    {
                        xtype: 'textareafield',
                        fieldLabel: 'Action Taken',
                        name: 'actionTaken',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'IMEI',
                        name: 'iemiNo',
                        id: 'addnewform-imeiNo',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Invoice No',
                        name: 'invoiceNo',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
                    {
                        margin: '30 0 10 0',
                        xtype: 'button',
                        action: 'spare_parts',
                        text: 'Add Spare Parts',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
                    {
                        margin: '0 0 30 0',
                        xtype: 'grid',
                        frame: false,
                        store: 'sparePrtStr',
                        id: 'spareN',
                        border: true,
                        columns: [
                            {xtype: 'actioncolumn',
                                text: '',
                                align: 'center',
                                flex: 0.5,
                                items: [{
                                        icon: 'resources/images/delete-icon.png',
                                        tooltip: 'Delete',
                                        handler: function (grid, rowIndex, colIndex) {
                                            //console.log('coming to handler');
                                            var rec = grid.getStore().getAt(rowIndex);
                                            var me = this;
                                            Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function (button) {
                                                if (button === 'yes') {
                                                    grid.getStore().removeAt(rowIndex);
                                                    //console.log(rec.data.partSellPrice)
                                                    var nwVal = rec.data.partSellPrice;
                                                    var txt = Ext.getCmp('addnewform-temp');
                                                    var dis = Ext.getCmp('addnewform-totCost');
                                                    txt.setValue(parseFloat(txt.getValue()) - parseFloat(nwVal));
                                                    dis.setValue(Ext.util.Format.number(txt.getValue(), '0,000.00'));

//                                                    me.up('window').fireEvent('remDevIsuItem');
                                                } else {
                                                    return false;
                                                }
                                            });

                                        }
                                    }]

                            },
                            {
                                text: 'Spare Part',
                                dataIndex: 'partNo',
                                flex: 1
                            },
                            {
                                text: 'Price',
                                dataIndex: 'partSellPrice',
                                flex: 1,
                                renderer: function (value) {
                                    return Ext.util.Format.number(value, '0,000.00');
                                }
                            },
                            {
                                text: 'Third Party Used',
                                dataIndex: 'thirdPartyFlag',
                                flex: 1,
                                renderer: function (value) {
                                    if (value === 1) {
                                        return "Yes";
                                    }
                                    if (value === 0) {
                                        return "No";
                                    }

                                }
                            }
                        ],
                        listeners: {
                            added: function (cmp, component, index, eOpts) {
                                //console.log(index);

                            }
                        }
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Repair Level',
                        store: 'ReplevelSTR',
                        displayField: 'levelName',
                        valueField: 'levelId',
                        visible: true,
                        name: 'repairLevelId',
                        action: 'rprLevel',
                        queryMode: 'local',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
                    {
                        id: 'levelPrice',
                        xtype: 'moneyfield',
                        fieldLabel: 'Price for the Repair Level',
                        name: 'repairAmount',
                        readOnly: 'true',
                        renderer: function (value) {
                            return Ext.util.Format.number(value, '0,000.00');
                        },
                        //                        listeners: {
                        //                            change: function (cmp, newValue, oldValue, eOpts) {
                        //                                //console.log(newValue)
                        //                                
                        ////                                cmp.setValue(Ext.util.Format.number(parseFloat(newValue), '0,000.00'));
                        ////                                if (oldValue > -1) {
                        ////                                    txt.setValue(parseFloat(txt.getValue()) - parseFloat(oldValue) + parseFloat(newValue));
                        ////                                } else {
                        ////                                    txt.setValue(parseFloat(txt.getValue()) + parseFloat(newValue));
                        ////                                }
                        //                                
                        //                            }
                        //                        }
                    },
                    //                    {
                    //                        xtype: 'textfield',
                    //                        fieldLabel: 'Price for the Repair Level',
                    ////                        name: '',
                    //
                    //                        },
                    {
                        xtype: 'textareafield',
                        fieldLabel: 'Remarks',
                        name: 'remarks',
                    },
                    //=========================================
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name: 'repairStatus',
                        store: 'RepStatus',
                        displayField: 'description',
                        valueField: 'code',
                        visible: true,
                        queryMode: 'remote',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                    },
//                    {
//                            xtype:'combo',
//                            fieldLabel:'Status',
//                            name:'repairStatus',
////                            store:'RepStatus',
//                            stores:'repairStatusStr',
//                            displayField: 'value',  
//                            valueField: 'status',
//                            visible: true,
//                        },
                    {
                        fieldLabel: 'Estimated Closing Date',
                        name: 'estimateClosingDate',
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        altFormats: 'm/d/Y',
                        editable: false,
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
//                        minValue:new Date(),
//                        listeners: {
//                            scope: this,
//                            change: function (me, newValue, oldValue) {
//                                me.setMinValue(newValue);
//                            }
//                        }
                    },
                    {
                        xtype: 'label',
                        text: 'Estimate Details',
                        id: 'addnewform-estTitle',
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Estimate No',
                        readOnly: 'true',
                        id: 'addnewform-estNo',
                        hidden: true,
                        name: 'esRefNo'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Estimate Status',
                        name: 'status',
                        store: 'EstStat',
                        displayField: 'description',
                        valueField: 'code',
                        hidden: true,
                        queryMode: 'remote',
                        id: 'addnewform-estStat'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Customer Reference',
                        //                        readOnly: 'true',
                        id: 'addnewform-cusRef',
                        hidden: true,
                        name: 'customerRef'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Payment Option',
                        name: 'paymentOption',
                        store: 'PayOpt',
                        displayField: 'description',
                        valueField: 'code',
                        hidden: true,
                        queryMode: 'remote',
                        id: 'addnewform-payOpt'
                    },
                    {
                        xtype: 'button',
                        action: 'getEst',
                        text: 'Generate Estimate',
                        margin: '2 2 2 2',
                        hidden: true,
                        id: 'addnewform-genEst'
                    },
                    {
                        xtype: 'button',
                        action: 'printEst',
                        text: 'Print Estimate',
                        margin: '2 2 2 2',
                        hidden: true,
                        id: 'addnewform-printEst',
                        disabled: true
                    },
                    {
                        xtype: 'button',
                        action: 'mailCus',
                        text: 'Email to Customer',
                        margin: '2 2 2 2',
                        hidden: true,
                        id: 'addnewform-mailCus',
                        disabled: true
                    },
                    {
                        xtype: 'textfield', //'textfield',
                        fieldLabel: 'Total Cost',
                        readOnly: 'true',
                        id: 'addnewform-totCost',
                        value: '0',
                        //                        listeners: {
                        //                            change: function (cmp, newValue, oldValue, eOpts) {
                        //                                cmp.setValue(Ext.util.Format.number(parseFloat(newValue), '0,000.00'));
                        //                            }
                        //                        }
                    },
                    {
                        xtype: 'hiddenfield',
                        id: 'addnewform-temp',
                        value: '0'
                    }
                ]
            },
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Submit',
                action: 'submit_wrkOrdr_Maintence_form',
                id: 'addnewform-submit',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'addnewform-save',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Close',
                action: 'cancel_wrkOrdr_Maintence_Form',
                id: '',
                //                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});