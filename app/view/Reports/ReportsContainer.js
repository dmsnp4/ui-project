

Ext.define('singer.view.Reports.ReportsContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.reportsContainer',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    overflowY: 'auto',
    initComponent: function() {
        ////console.log('skjkasl');
        var inquiryCmp = this;
        inquiryCmp.items = [
          this.buildMasterReportsContainer(),
        ];
        inquiryCmp.callParent();
    },

    buildMasterReportsContainer: function() {
        return Ext.create('Ext.panel.Panel',
        {
            title: "Administrator Reports",
            layout: {type: 'vbox', align: 'stretch', pack: 'start'},
            
            items: [{
                    xtype: 'form',
                    flex: 1,
                    padding: '0',
                    overflowY: 'auto',
                    border: false,
                    items: [

                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 300 20 10',
                                    text:'Clear Transit',
                                    name: 'cleartransitreport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 0px',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showTransitReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 295 20 10',
                                    text:'Category List',
                                    name: 'categorylistreport',
                                    xtype: 'label'
                                },
                               
                                {
                                    margin: '20 0 20 3px',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showCategoryReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            defaults: {
                                xtype: 'textfield',
                                margin: '20 0 20 300',
                            },
                            items: [
                                {
                                    margin: '20 270 20 10',
                                    text:'Repair Categories',
                                    name: 'repaircategoriesreport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 -2px',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showRepairCategoriesReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            defaults: {
                                xtype: 'textfield',
                                //margin: '20 0 20 300',
                            },
                            items: [
                                {
                                    margin: '20 295 20 10',
                                    text:'Repair Levels',
                                    name: 'repairlevelsreport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 0',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showRepairLevelsReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 260 20 10',
                                    text:'Major Defect Codes',
                                    name: 'majordefectreport',
                                    xtype: 'label'
                                },
                                {
                                     margin: '20 0 20 0',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showMajorReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 260 20 10',
                                    text:'Minor Defect Codes',
                                    name: 'minordefectreport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 0',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showMinorReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 255 20 10',
                                    text:'Spares Maintenance',
                                    name: 'sparesmaintenancereport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 -3px',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showSparesReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 238 20 10',
                                    text:'Model List Maintenance',
                                    name: 'modellistmaintenancereport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 -6px',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showModelReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 240 20 10',
                                    text:'Warranty Adjustments',
                                    name: 'warrantyadjustmentreport',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 0',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showWarrantyReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 215 20 10',
                                    text:'Customer Complain Report',
                                    name: 'cusCmplainRpt',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 0',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showComplainReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 215 20 10',
                                    text:'IMEI Exception Report ',
                                    name: 'exceptionRpt',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 20',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showExceptionReport'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 215 20 10',
                                    text:'User List Report ',
                                    name: 'userList',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 60',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'showUserList'
                                }
                            ]},
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '20 0 20 300',
//                            },
                            items: [
                                {
                                    margin: '20 215 20 10',
                                    text:'SQL Reports ',
                                    name: 'exceptionRpt',
                                    xtype: 'label'
                                },
                                {
                                    margin: '20 0 20 80',
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'SQL Reports',
                                    action: 'showSQLReport'
                                }
                            ]}
//                        {
//                            xtype: 'fieldcontainer',
//                            layout: 'hbox',
//                            items: [
//                                {
//                                    margin: '20 0 20 0',
//                                    flex: 1,
//                                    maxWidth: 100,
//                                    xtype: 'button',
//                                    text: 'SQL Reports',
//                                    action: 'showSQLReport'
//                                }
//                            ]
//                        }
                    ]
                }
                 ]
        });
    }


});
