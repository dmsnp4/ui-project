Ext.define('singer.view.exceptions.StoreReloader', {
    extend: 'Ext.window.Window',
    alias: 'widget.storereloader',
    width: 400,
    height: 350,
    closable: false,
    title: 'Unexpected Errors occured',
    modal: true,
    constrain : true,
    iconCls: 'icon-connection-lost',
    bodyPadding: 10,
    overflowY: 'auto',
    initComponent: function() {
        this.items = this.buildItems();
        this.dockedItems = this.buildDockedItems();
        this.callParent(arguments);
    },
    buildItems: function() {
        var headerDesction = Ext.create('Ext.Component', {
            html: '<b style="color:firebrick;">* Please click refresh application button.</b>',
            width: 330,
            height: 40
        });
        var expTpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<div style="margin-bottom: 10px; border-bottom: 1px solid #e5e5e5;" class="thumb-wrap">',
                '<span>Error occured while processing &nbsp;</span>',
                '<span style="color: red; font-weight: 600;">{store}&nbsp;&nbsp; - &nbsp;</span>',
                '<span>{detail}</span>',
                '</div>',
                '</tpl>'
                );
        var tmplView = Ext.create('Ext.view.View', {
            store: Ext.data.StoreManager.lookup('Exceptions'),
            tpl: expTpl,
            itemSelector: 'div.thumb-wrap',
            emptyText: 'No exceptions'
        });
        return [headerDesction, tmplView];
    },
    buildDockedItems: function() {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                dock: 'bottom',
                style: {
                    backgroundColor: '#dfeaf2 !important'
                },
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: ['->', {
                        iconCls: 'icon-refresh icon-white',
                        text: 'Refresh Application',
                        action: 'refresh_app',
                        ui: 'primary',
                        id: 'storereloader_refresh_app'
                    }, {
                        hidden: true,
                        iconCls: 'icon-time',
                        text: 'Wait',
                        action: 'check_con'
                    }]
            }];
    }
});


