


Ext.define('singer.view.Administrator.DistributorMapping.MappingPage', {
    extend: 'Ext.container.Container',
    alias: 'widget.mapping',
    store: 'UserBusinessMappi',
    layout: {
        type: 'hbox',
        align: 'stretch',
//        layout: 'fit'
    },
//    overflowY: 'auto',
    minHeight: 200,
    initComponent: function () {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.WorkOrderOpenContainer(),
            this.maintainController(),
        ];
        inquiryCmp.callParent();
    },
    WorkOrderOpenContainer: function () {
        return Ext.create('Ext.panel.Panel',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start',
                    },
                    flex: 1,
                    cls: 'spareMleft',
                    items: [
                        {
                            xtype: 'form',
                            padding: '0',
                            autoScroll: true,
                            overflowY: 'auto',
                            border: 0,
                            items: [
                                {
                                    xtype: 'combo',
                                    action: 'destributor_change',
                                    fieldLabel: 'Distributor',
                                    id: 'dstrb',
                                    name: '',
                                    allowBlank: false,
                                    store: 'BisMapStr',
                                    displayField: 'bisName',
                                    valueField: 'bisId',
                                    queryMode: 'remote',
                                    margin: '10 10 10 10'
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'Distributor_Location',
                                    name: '',
                                    fieldLabel: 'Distributor Location',
                                    margin: '10 10 10 10',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'Distributor_ID',
                                    name: '',
                                    fieldLabel: 'Distributor ID',
                                    margin: '10 10 10 10',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'ERPRffrn',
                                    name: '',
                                    fieldLabel: 'ERP Refference Code',
                                    margin: '10 10 10 10',
                                    allowBlank: false,
                                    regex: /^[a-zA-Z_ 0-9\,'']+$/,
                                    listeners:
                                            {
                                                scope: this,
                                                change: function (me, newValue, oldValue, eOpts)
                                                {
                                                    var typedVal = newValue;
                                                    me.setValue(typedVal.toUpperCase());
                                                }
                                            }
                                },
                                {
                                    xtype: 'hiddenfield',
                                    name: 'bisId',
                                    id: 'erp_mapping_business_id',
                                    fieldLabel: 'bis id',
                                    margin: '10 10 10 10',
                                },
                                {
                                    xtype: 'button',
                                    text: '>>>>',
                                    width: 100,
                                    margin: '10 10 10 160',
                                    action: 'add_to_grid',
                                    formBind: true
                                },
                                {
                                    xtype: 'button',
                                    text: 'Submit',
                                    width: 100,
                                    margin: '10 10 10 160',
                                    action: 'submit',
                                    formBind: true
                                }
                            ]
                        }
                    ]

                });
    },
    maintainController: function () {
        return Ext.create('Ext.panel.Panel',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start',
                    },
                    flex: 2,
                    cls: 'spareMright',
                    items: [
                        {
                            padding: '0',
                            autoScroll: true,
                            overflowY: 'auto',
                            border: 0,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'vbox',
                                    autoScroll: true,
                                    height: 600,
                                    overflowY: 'auto',
                                    defaults: {
                                        margin: '10 10 10 10',
//                                labelWidth: 150,
                                        width: 500
                                    },
                                    flex: 1,
                                    items: [
                                        {
                                            xtype: 'grid',
                                            height: '20',
                                            store: 'UserBusinessMappi',
                                            id: 'bisGrd',
                                            columns: [
//                                    {
////                                        xtype: 'searchablecolumn',
//                                        id:'mapping_destributor',
//                                        text: 'Distributor',
//                                        dataIndex: 'bisName',
//                                        hideable: false,
//                                        flex: 2
//                                    },
                                                {
                                                    id: 'mapping_cus_no',
                                                    text: 'ERP Refference Code',
                                                    dataIndex: 'customerNo',
                                                    hideable: false,
                                                    flex: 1
                                                },
//                                    {
////                                        xtype: 'searchablecolumn',
//                                        text: 'Status',
//                                        dataIndex: 'status',
//                                        hideable: false,
//                                        flex: 2
//                                    },
                                                {
                                                    xtype: 'actioncolumn',
                                                    text: 'Delete',
                                                    align: 'center',
                                                    flex: 1,
                                                    items: [{
                                                            icon: 'resources/images/delete-icon.png',
                                                            tooltip: 'Delete',
                                                            handler: function (grid, rowIndex, colIndex) {
                                                                var rec = grid.getStore().getAt(rowIndex);
                                                                var me = this;
                                                                Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function (button) {
                                                                    if (button == 'yes') {
                                                                        grid.getStore().removeAt(rowIndex);
                                                                        console.log(me.up('grid'));
                                                                        me.up('grid').fireEvent('remDevIsuItem', grid, rec);
                                                                    } else {
                                                                        return false;
                                                                    }
                                                                });

                                                            }
                                                        }]

                                                },
                                            ]
                                        }

                                    ]}
                            ]
                        }
                    ]
                });
    }
});



