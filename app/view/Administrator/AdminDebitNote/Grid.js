

Ext.define('singer.view.Administrator.AdminDebitNote.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.debitnotegridd',
    loadMask: true,
    store: 'AcceptDebitNote',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    forceFit: false,
    minHeight: 200,
    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var grid = this;
        grid.columns = grid.buildColumns();
        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->']
            },
            {
                xtype: 'pagingtoolbar',
                store: 'AcceptDebitNote',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function () {
        
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {
                    "name": "3",
                    "value": "Complete"
                },
                {
                    "name": "1",
                    "value": "Pending"
                }
            ]
        });

        return [
            {
                xtype: 'searchablecolumn',
                text: 'Debit Note No',
                dataIndex: 'debitNoteNo'
            }, {
                xtype: 'searchablecolumn',
                text: 'Order No',
                dataIndex: 'orderNo'
            }, {
                xtype: 'searchablecolumn',
                text: 'Invoice No ',
                dataIndex: 'invoiceNo'
            }, 
            {
                xtype: 'searchablecolumn',
                text: 'Contract No ',
                dataIndex: 'contactNo'
            },
             //  String partNo,
//            int orderQty,
//            int deleveryQty,
//            String deleveryDate,
//            int headStatus,
//            String customerNo,
//            String order,
//            String type,
//            int start,
//            int limt)
//
            {
                xtype: 'searchablecolumn',
                text: 'Part No',
                dataIndex: 'partNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Order Qty',
                dataIndex: 'orderQty'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Delivered Qty',
                dataIndex: 'deleveryQty'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Delivered Date',
                dataIndex: 'deleveryDate'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Head Status',
                dataIndex: 'headStatus',
//                renderer: function(value) {
//                    if (value === 3) {
//                        return "Complete";
//                    }
//                    if (value === 1) {
//                        return "Pending";
//                    }
//                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Customer No',
                dataIndex: 'customerNo'
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'View',
                align:'center',
                items: [{
                        icon: 'resources/images/view-icon.png',
                        text: 'View ',
                        tooltip: 'View Debit Note',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('onView', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Details',
                align:'center',
                items: [{
                        icon: 'resources/images/view-details.png',
                        tooltip: 'Details',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('onDetails', grid, rec);
                        }
                    }]
            }

        ];
    }
});


