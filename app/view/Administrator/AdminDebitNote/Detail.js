

Ext.define('singer.view.Administrator.AdminDebitNote.Detail', {
    extend: 'Ext.form.Panel',
    alias: 'widget.admin_debit_detail',
    modal: true,
    layout: 'fit',
    constrain: true,
    width: 500,
    resizable: false,
    loadMask: true,
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
//            items: newUserForm.buildItems(),
        });
        var groupItems = Ext.create('Ext.container.Container', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            //            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formBottom = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.bottomButtons(),
        });

        var formTop = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.topButtons(),
        });

        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {},
                items: [
                    formTop,
//            {
//                xtype: 'label',
//                text: 'Load data amount:'
//            },
//            {
//                        xtype: 'textarea',
//                        id: 'DebitNtNo'
////                        style: {
////                            //float: 'right',
////                            'text-align': 'right',
////                            height:'5px',
//////                            margin: '5px',
////                            'font-weight': 'bold',
////                            color: '#333'
////                        }
//
//            },
//            {
//                xtype:'button',
//                text:'Load',
//                action:'sort',
//                margin: '2 1 20 1'
//            },
                    groupItems,
                    formPanel,
                    formBottom
                ]
            }
        ];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
        return Ext.create('Ext.Panel', {
            width: 500,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    flex: 3,
                    items: [
                        {
                            xtype: 'grid',
                            height: 400,
                            id: 'admin_debitNoteImei',
                            columns: [
                                {
                                    text: 'Debit Note IMEI',
                                    dataIndex: 'imeiNo',
                                    flex: '1',
                                    menuDisabled: true
                                }
                            ]
                        },
                        {
                            xtype: 'button',
                            text: 'Load More',
                            align: 'center',
                            action: 'loadMore',
                            id:'admin_accept_load_more',
                            style: 'margin:10px auto 10px 30%'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    flex: 1,
                    items: [
                        {xtype: 'label',
                            forId: 'myFieldId',
                            text: 'Amount',
                            margin: '5 1 1 1'

                        },
                        {
                            xtype: 'textfield',
                            text: 'Refresh',
                            blankText: '100',
                            margin: '5 1 1 1',
                            id:'admin_accept_load_count',
                            value:100
                        },
                        {
                            xtype: 'button',
                            text: 'Refresh',
                            action: 'refresh',
                            margin: '10 10 10 2',
                            tooltip: 'Accept All IMEI'
                        },
                        {
                            xtype: 'button',
                            text: '>>',
                            action: 'accept_All',
                            margin: '150 0 0 18',
                            tooltip: 'Accept All IMEI'
                        },
                        {
                            xtype: 'button',
                            text: '<<',
                            action: 'prevAll',
                            margin: '20 0 0 18',
                            tooltip: 'Reverce All IMEI'
                        }
                    ]
                }, {
                    xtype: 'panel',
                    flex: 3,
                    items: [
                        Ext.create('Ext.grid.Panel',
                                {
                                    id: 'admin_acceptedImei', //Do not duplicate the name
                                    height: 400,
                                    columns: [
                                        {
                                            text: 'Accepted IMEI',
                                            dataIndex: 'imeiNo',
                                            flex: '1',
                                            menuDisabled: true,
                                        }
                                    ]
                                })
                    ]
                }]
        });
    },
//    buildItems: function () {
//        return [
////            {
////                fieldLabel: 'IMEI',
////                id: 'imeiID',
////                name: '',
////            }
//        ];
//    },
    bottomButtons: function () {
        return [
//            {
//                xtype: 'button',
//                text: 'Scan',
//                action: 'scan'
//            },
//            {
//                xtype: 'button',
//                text: 'Remove',
//                formBind: true
//            },
            '->',
//            {
//                xtype: 'button',
//                text: 'Accept',
//                action: 'accept',
//                formBind: true
//            },
            {
                xtype: 'button',
                id:'done',
                text: 'Done',
                action: 'done',
                formBind: true
            },
        ];
    },
    topButtons: function () {
    },
    buildPlugins: function () {
        return [];
    }

});