
Ext.define('singer.view.Administrator.AdminDebitNote.View', {
    extend: 'Ext.window.Window',
    alias: 'widget.debit_note_view',
    iconCls: 'icon-user',
    title: 'Debit Note',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield',
                cls:'ViewTable'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        
        return [
            {
                fieldLabel: 'Debit Note No',
                name: 'debitNoteNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            }, 
            {
                fieldLabel: 'Order No',
                name: 'orderNo'
            }, 
            {
                fieldLabel: 'Invoice No',
                name: 'invoiceNo'
            }, 
            {
                fieldLabel: 'Contact No',
                name: 'contactNo'
            }, 
            {
                fieldLabel: 'Part No',
                name: 'partNo'
            }, 
            {
                fieldLabel: 'Order Qty',
                name: 'orderQty'
            },
            {
                fieldLabel: 'Delivered Qty',
                name: 'deleveryQty'
            },
            {
                fieldLabel: 'Delivery Date',
                name: 'deleveryDate'
            }, 
            {
                fieldLabel: 'Head Status',
                name: 'headStatus',
            },
            {
                fieldLabel: 'Customer No',
                name: 'customerNo'
            },
            {
                fieldLabel: 'Site Description',
                name: 'siteDescription'
            },
        ];
    },
    buildButtons: function () {
        return ['->', {
                text: 'Done',
                action: 'done'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});



