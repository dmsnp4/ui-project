

Ext.define('singer.view.Administrator.login.forms.LoginForm', {
    extend: 'Ext.container.Container',
//    extend: 'Ext.panel.Panel',
    alias: 'widget.apploginform',
    layout: {type: 'hbox', align: 'middle', pack: 'center'},
    initComponent: function () {
        var loginForm = Ext.create('Ext.form.Panel', {
            minWidth: 350,
            bodyPadding: 10,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'textfield',
            items: [{
                    fieldLabel: 'Username',
                    name: 'userId',
//                    value : 'hasha41',
                    allowBlank: false
                }, {
                    fieldLabel: 'Password',
                    name: 'password',
//                    value : '12345678',
                    inputType: 'password',
                    allowBlank: false
                }],
            buttons: [{
                    text: 'Sign In',
                    action: 'login_handle'
                },
//                {
//                    text: 'Cancel',
//                    action: 'login_cancel'
//                }
            ]
        });
        this.items = [{xtype: 'panel', style: {
                    backgroundColor: '#ffffff',
                    boxShadow: '0px 0px 2px #000000'
                }, title: 'Sign in', items: [loginForm]}];

        this.callParent(arguments);
    }
});