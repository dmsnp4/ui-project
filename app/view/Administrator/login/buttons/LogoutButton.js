
Ext.define('singer.view.Administrator.login.buttons.LogoutButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.logoutBtn',
    scale: 'large',
    margin: '0 0 0 0',
    iconAlign : 'left',
    textAlign : 'right',
    ui: 'default',
    minWidth: 185,
    maxWidth: 185,
    menu: [
        {text: 'Change Password', action: 'password_change'},
        {text: 'Sign Out', action: 'logout'}
    ],
    
    initComponent: function() {
        this.callParent(arguments);
    }
});
