/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.Administrator.login.buttons.LoginButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.loginBtn',
    scale: 'large',
    action: 'login',
    maxWidth: 40,
    minWidth : 40,
    margin: '0 0 0 0',
    iconCls : 'icon-large-signin',
    initComponent: function() {
        this.callParent(arguments);
    }
});
