


//Ext.define('singer.view.Administrator.login.ChangePassword', {
//    extend: 'Ext.container.Container',
////    extend: 'Ext.panel.Panel',
//    alias: 'widget.changepassword',
//    //layout: {type: 'hbox', align: 'middle', pack: 'center'},
//    initComponent: function () {
//        var loginForm = Ext.create('Ext.form.Panel', {
////            minWidth: 500,
//            bodyPadding: 10,
//            layout: 'anchor',
//            defaults: {
//                //anchor: '100%'
//            },
//            defaultType: 'textfield',
//            items: [
//                {
//        //            width: '300',
////                    xtype:'textfield',
//                    fieldLabel: 'Current Password',
//                    inputType: 'password',
//                    name: 'password',
//                    allowBlank: false
//                },
//                {
////                    xtype:'textfield',
//                    id: "txt_newPIN",
//                    fieldLabel: 'New Password',
//                    inputType: 'password',
//                    allowBlank: false
//                },
//                {
////                    xtype:'textfield',
//                    id: "txt_confirmPIN",
//                    fieldLabel: 'Confirm New Password',
//                    name: 'newPassword',
//                    inputType: 'password',
//                    allowBlank: false
//                }
//            ],
//            buttons: [
//                {
//                    text: 'Submit',
//                    action: 'submit',
//                    formBind: true
//                },
//                {
//                    text: 'Reset Form',
//                    handler: function () {
//                        this.up('form').getForm().reset();
//                    }
//                }
//            ]
//        });
//        this.items = [{xtype: 'panel', style: {
//                    backgroundColor: '#ffffff',
//                    boxShadow: '0px 0px 2px #000000'
//                },  items: [loginForm]}];
//
//        this.callParent(arguments);
//    }
//});

//======================================================



Ext.define('singer.view.Administrator.login.ChangePassword', {
    extend: 'Ext.form.Panel',
    alias: 'widget.changepassword',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 530,
    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
//            {xtype: 'hiddenfield',name: 'bisId',id:'businessId'},
            {
        //            width: '300',
//                    xtype:'textfield',
                    fieldLabel: 'Current Password',
                    inputType: 'password',
                    name: 'password',
                    allowBlank: false
                },
                {
//                    xtype:'textfield',
                    id: "txt_newPIN",
                    fieldLabel: 'New Password',
                    inputType: 'password',
                    allowBlank: false
                },
                {
//                    xtype:'textfield',
                    id: "txt_confirmPIN",
                    fieldLabel: 'Confirm New Password',
                    name: 'newPassword',
                    inputType: 'password',
                    allowBlank: false
                }
        ];
    },
    buildButtons: function () {
        return [
                {
                    text: 'Submit',
                    action: 'submit',
                    formBind: true
                },
                {
                    text: 'Reset Form',
                    handler: function () {
                        this.up('form').getForm().reset();
                    }
                }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});