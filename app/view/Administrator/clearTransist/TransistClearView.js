Ext.define('singer.view.Administrator.clearTransist.TransistClearView', {
    extend: 'Ext.window.Window',
    alias: 'widget.cleartransview',
    iconCls: 'icon-user',
    title: 'Clear Transisit Inventory',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {
        return [
            {
                fieldLabel: 'Issue No:',
                name: 'issueNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Issue Date:',
                name: 'issueDate'
            },
            {
                fieldLabel: 'From Location:',
                name: 'fromName'
            },
            {
                fieldLabel: 'To Location:',
                name: 'toName'
            },
        ];
    },
    

    buildButtons: function() {
        return [{
                text: 'Close',
                action: 'done'
        }];
    },
    
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});
