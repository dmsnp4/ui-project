
Ext.define('singer.view.Administrator.clearTransist.ClearTransistGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cleartransist',
    store: 'ClearTransist',
//    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
    
    initComponent: function() {
        ////console.log("initComponent of spare maintenance grid");
        var clearTransistGrid = this;
        clearTransistGrid.columns = clearTransistGrid.buildColumns();
        clearTransistGrid.dockedItems = clearTransistGrid.buildDockedItems();
        clearTransistGrid.callParent();
    },
    
    buildColumns: function() {
        return [
            {
                xtype: 'searchablecolumn',
                header: 'Issue No',
                dataIndex: 'issueNo',
                flex: 1
            },
            {
                xtype: 'searchablecolumn',
                header: 'Issue Date',
                dataIndex: 'issueDate',
                flex: 1
            },
            {
                xtype: 'searchablecolumn',
                header: 'From Location',
                dataIndex: 'fromName',
                flex: 1
            },
            {
                xtype: 'searchablecolumn',
                header: ' To Location',
                dataIndex: 'toName',
                flex: 1
            },
//            {
//                header: 'clear',
//                xtype: 'actioncolumn',
//                width: 150,
//                menuDisabled: true,
//                items: [{
//                        icon: 'resources/res/clear-icon.png', // Use a URL in the icon config
//                        tooltip: 'Clear',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('tranDeleteView', grid, record);
//                         //   alert("clear " + record.get('name'));
//                            //grid.getStore().remove(record);
//                        }
//                    }]
//            },
            {
                header: 'Details',
                xtype: 'actioncolumn',
                width: 150,
                menuDisabled: true,
                items: [{
                        icon: 'resources/res/details.png', // Use a URL in the icon config
                        tooltip: 'Detail',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('tranDeleteView', grid, record);
                        }
                    }]
            },
            {
                header: 'View',
                xtype: 'actioncolumn',
                menuDisabled: true,
                width: 150,
                items: [{
                        icon: 'resources/res/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('clearTransView', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            }

        ];
    },
    
    buildDockedItems: function() {
        return [{xtype: 'form',
//                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },

                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch'
                                //  id: 'suppliersgrid_search'
                    }]
            }, {
                xtype: 'pagingtoolbar',
                store: 'ClearTransist',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});