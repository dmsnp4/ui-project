


Ext.define('singer.view.Administrator.clearTransist.TransistDeleteView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.transitDelete',
    width: 500,
    bodyPadding: 5,
    overflowY: 'auto',
    defaults: {
        xtype: 'textfield'
    },
    initComponent: function() {
        ////console.log('delete view form loading');
        var me = this;
        me.items = me.buildItems();
        me.buttons = me.buildButtons();
        me.callParent();
    },
    buildItems: function() {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Pending"},
                {"status": 2, "value": "Inprogress"},
                {"status": 5, "value": "Clear"}
            ]
        });

        return [
            {
                fieldLabel: 'Issue No',
                name: 'issueNo'
            },
//            {
//                fieldLabel: 'Status',
//                xtype: 'combo',
//                name: 'status',
//                store: stat,
//                queryMode: 'local',
//                displayField: 'value',
//                editable: false,
//                valueField: 'status',
////                listeners: {
////                    scope: this,
////                    beforeRender: function(me) {
////                        var val = me.getValue();
////                        var cmp = Ext.getCmp('newcategory-remark');
////                        if (val === null) {
////                            me.setValue(1);
////                            cmp.setValue('empty');
////                        }
//////                        var cmp = Ext.getCmp('newcategory-remark');
////                        cmp.setVisible(false);
////
////                    },
////                    change: function(me) {
////                        var cmp = Ext.getCmp('newcategory-remark');
////                        cmp.setValue("");
////                        cmp.setVisible(true);
////                    }
////                }
//            },
            {
                xtype: 'textareafield',
                fieldLabel: 'Reason',
                name: 'remarks',
                allowBlank:false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
            },
            {
                margin: '0 0 30 0',
                xtype: 'grid',
                store: 'testGrid',
                id: 'delClrGrid',
                frame: false,
                border: true,
                columns: [
                    {text: 'Issue No', dataIndex: 'issueNo', flex: 1},
                    {text: 'IMEI No', dataIndex: 'imeiNo', flex: 1},
                ]
            },
//            
//     =============================       
//            
//            {
//                xtype: 'checkboxfield',
//                fieldLabel: 'Clear',
//                listeners: {
//                    change: function(checkbox, newValue, oldValue, eOpts) {
//                        var textArea = checkbox.up('form').down('textareafield');
//                        if (newValue, checkbox.getValue()===true) {
//                            textArea.show();
//                             checkbox.nextSibling().setValue('2');
//                        }
//                        else {
//                            textArea.hide();
//                             checkbox.nextSibling().setValue('1');
//                        }
//                    },
//                    afterRender: function(me) {
//                        var textArea = me.up('form').down('textareafield');
//                        textArea.hide();
//                    }
//                }
//
//            },
//            {
//                xtype: 'textareafield',
//                allowBlank: false,
//                fieldLabel: 'Reason',
//                name: 'remarks'
//            },
//            {
//                xtype: 'hiddenfield',
//                fieldLabel: 'Status',
//                name: 'status'
//            },
//            {
//                xtype: 'hiddenfield',
//                name: 'debitNoteNo'
//            },
//            {
//                xtype: 'hiddenfield',
//                name: 'iMEI'
//            }
        ];
    },
    buildButtons: function() {
        return [
            {
                xtype: 'button',
                text: 'submit',
                action: 'submit',
                formBind: true
            }
        ];
    }
});


      