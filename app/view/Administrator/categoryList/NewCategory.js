

Ext.define('singer.view.Administrator.categoryList.NewCategory', {
    extend: 'Ext.window.Window',
    alias: 'widget.newcategory',
    loadMask: true,
    modal: true,
//    iconCls: 'icon-user',
    title: 'Add New Category',
//    store: 'Status',
//    modal: true,       
    layout: 'fit',
    constrain: true,
    width: 500,
//    height: 200,
    initComponent: function() {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 300,
//            loadMask: true,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });
//        newUserForm.items=this.buildItems();
//        newUserForm.items = [
//            {
//                xtype: 'container',
//                layout: {
//                    type: 'vbox',
//                    align: 'stretch'
//                },
//                defaults: {
//                    flex: 5
//                },
//                items: [
//                    formPanel,
//                    this.buildGroupItems()
//                ]
//            }
//        ];
//        newUserForm.dockedItems = newUserForm.buildDockedItems();
////        supplierForm.items = [vbox];
        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function() {
//        return Ext.create('Ext.Panel', {
//            width: 400,
////                height: 300,
//           // title: "User Groups",
////            layout: {
////                type: 'hbox',
////                align: 'stretch'
////            },
////                renderTo: document.body,
////            items: [{
////                    xtype: 'panel',
////                        title: 'Inner Panel One',
//                    flex: 2,
//                    items: [
////                        Ext.create('Ext.grid.Panel', {
//////                                title: 'Simpsons',
//////                            store: 'UserGroups',
////                            columns: [
////                                {text: 'Available Groups', dataIndex: 'groupName'}
////                            ]
//////                                height: 200,
//////                                width: 400,
//////                                renderTo: Ext.getBody()
////                        })
////                    ]
////                },
////                {
////                    xtype: 'panel',
//////                        title: 'Inner Panel One',
////                    flex: 2,
////                    layout: 'anchor',
////                    items: [
////                        Ext.create('Ext.Button', {
////                            text: '>',
////                            renderTo: Ext.getBody(),
////                            handler: function() {
////                                alert('You clicked the button!');
////                            }
////                        }),
////                        Ext.create('Ext.Button', {
////                            text: '>>',
////                            renderTo: Ext.getBody(),
////                            handler: function() {
////                                alert('You clicked the button!');
////                            }
////                        })
////                    ]
////                }, 
////                {
////                    xtype: 'panel',
//////                        title: 'Inner Panel Three',
////                    flex: 2,
////                    items: [
////                        Ext.create('Ext.grid.Panel', {
////                            id: 'UserGroupsGrid',
////                            columns: [
////                                {text: 'Assigned Groups', dataIndex: 'groupName'}
////                            ]
////                        })
////                    ]
////                }
//                    ]
//        });
    },
    buildItems: function() {
//
//        var userstore = Ext.data.StoreMgr.lookup('Users').getAt(0).data.nic;
//        //var group = userstore.data.items[6].data.userGroups;
//
//        ////console.log("Users Store ............", userstore); //userstore.data.items[6].data.userGroups);

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
            ]
        });
        return [
            {
                fieldLabel: 'ID',
                name: 'cateId',
                maxLength: 10,
//                visible: true
//                vtype: 'ValidUserID',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/
            },
            {
                xtype:'capfield',
                fieldLabel: 'Name',
                name: 'cateName',
                maxLength: 40,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
//                vtype: 'ValidUserID',
                regex: /^[a-zA-Z0-9]*$/ ,
                stripCharsRe: /(^\s+|\s+$)/g,
                id:'CatName',
                listeners: {
                    afterrender: function(field) {
                      field.focus();
                    }
                  }
            },
//             {
//                fieldLabel: 'NIC',
//                name: 'nic',
//                maxLength: 10,
//                minLength: 10,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
////                vtype: 'ValidUserID',
//                regex: /^[V0-9_]+$/
//            }, 
            {
                fieldLabel: 'Description',
                name: 'cateDesc',
                id: 'tex',
                maxLength: 150,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
//                regex: /^[a-zA-Z0-9 ]+$/,
                regex:/^[a-zA-Z0-9][a-zA-Z0-9 ]*[a-zA-Z0-9]$/,
//                stripCharsRe: /(^\s+|\s+$)/g,
                validator:function(tex){
        if(this.allowBlank===false && Ext.util.Format.trim(tex).length===0)
          return false;
        else
          return true;
    }
                
            },
                
//                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
            
//            {
//                fieldLabel: 'Last Name',
//                name: 'lname',
//                maxLength: 10,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
////                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
//            }, {
//                fieldLabel: 'Common Name',
//                name: 'comName',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                maxLength: 10,
////                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
//            }, {
//                fieldLabel: 'Designation',
//                name: 'designation',
//                maxLength: 10,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
////                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
//            }, {
//                fieldLabel: 'Telephone No',
//                name: 'telePhone',
//                maxLength: 10,
//                minLength: 10,
////                vtype: 'int',
//                allowBlank: true,
//                regex: /^[0-9_]+$/
//            }, {
////                xtype: 'textfield',
//                fieldLabel: 'Email',
//                name: 'email',
////                maxLength: 10,
//                vtype: 'email',
//                allowBlank: true
////                regex: /^[V0-9_]+$/
//            },
            {
                fieldLabel: 'Status',
                xtype: 'combobox',
                name: 'cateStatus',
                store: stat,
//                maxLength: 2,
//                id: 'cateStatus',
                queryMode: 'local',
                displayField: 'value',
                editable: false,
                valueField: 'status',
                listeners: {
                    scope: this,
                    beforeRender: function(me) {
                        var val = me.getValue();
                        var cmp = Ext.getCmp('newcategory-remark');
                        if (val === null) {
                            me.setValue(1);
                            cmp.setValue('empty');
                        }
//                        var cmp = Ext.getCmp('newcategory-remark');
                        cmp.setVisible(false);

                    },
//                    afterRender: function(me) {
////                        ////console.log(this.next());
//                        var val = me.getValue();
//                        var cmp = Ext.getCmp('newcategory-remark');
//                        if (val === null) {
//                            me.setValue(1);
//                            cmp.setValue('empty');
//                        } else if (val === 1) {
////                            cmp.setVisible(false);
//                            cmp.setValue('empty');
//                        } else {
//                            cmp.setVisible(true);
////                            cmp.setValue('');
//                        }
//                    },
                    change: function(me) {
                        var cmp = Ext.getCmp('newcategory-remark');
                        cmp.setValue("");
                        cmp.setVisible(true);
//                        var val = me.getValue();
//                        if (val === 1) {
//                            cmp.setVisible(false);
////                            if (cmp.getValue() === '')
////                                cmp.setValue('empty');
//
//                        } else {
//                            cmp.setVisible(true);
////                            if (cmp.getValue() === 'empty')
////                                cmp.setValue('');
//                        }
                    }
                }
            }, {
                fieldLabel: 'Remark',
                name: 'remarks',
                id: 'newcategory-remark',
                maxLength: 100,
//                regex: /^[a-zA-Z_]+$/,
                allowBlank: false,
//                visible: false,
                listeners: {
                     scope: this,
                    beforeRender: function(me) {
                        var valr = me.getValue();
                        var cmpp = Ext.getCmp('newcategory-remark');
//                        //console.log('valr is');
//                         //console.log(valr);
                        if (valr === "") {
                          
////                            me.setValue(1);
                            cmpp.setValue('empty');
                        }
//                        var cmp = Ext.getCmp('newcategory-remark');
//                        cmp.setVisible(false);

                    }
                    
                }
//                listeners: {
//                    scope: this,
//                    change: function(me){
//                        
//                        var cmp1= Ext.getCmp('remark');
//                        var cmp2= Ext.getCmp('cateStatus');
//                        if (cmp2 === '2'){
//                            cmp1.allowBlank=false;
//                        }
//                    }
//                }
//                xtype: 'textfield',
//                minLength: 1
//                regex: /^[a-zA-Z]{1,}$/
//                submitEmptyText: false
//                allowBlank: false
            }
//           
        ];
    },
    buildButtons: function() {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newcategory-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newcategory-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
//                id: 'userform-reset-insert'
                    }, {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newcategory-save',
                        formBind: true
//                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
//                        ui: 'success',
                        action: 'create',
                        id: 'newcategory-create',
                        formBind: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function() {
        return [];
    }

});
