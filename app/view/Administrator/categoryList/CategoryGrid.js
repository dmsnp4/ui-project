/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.categoryList.CategoryGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.categorygrid',
    loadMask: true,
    store: 'CategoryList',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging',
    ],
//    forceFit: true,
    minHeight: 200,
//    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var usersGrid = this;
        usersGrid.columns = usersGrid.buildColumns();

//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        usersGrid.dockedItems = usersGrid.buildDockItems(this);
        usersGrid.features = usersGrid.buildFeatures();
        usersGrid.callParent();
    },
    buildDockItems: function (userGrid) {
        return [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
                        xtype: 'pagingtoolbar',
                        store: 'CategoryList',
                        dock: 'bottom',
                        displayInfo: true
                    }]
            }, {xtype: 'form',
//                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action:'clearSearch',
                        handler: function () {
//                            userGrid.filters.clearFilters();

//                            var me = this;
//                            var tablePanel = this.up('gridpanel'), allColumns = tablePanel.columns;
//                            var store = tablePanel.getStore();
//                            for (var i = 0; i < allColumns.length; i++) {
//                                if (!Ext.isEmpty(allColumns[i].searchableField)) {
//                                    var field = allColumns[i].searchableField;
//
//                                    ////console.log(field.getValue());
//                                    if (field.getValue() !== "") {
//                                        field.setValue("");
//                                    }
////                         
//                                }
//                            }
//                            store.getProxy().api.read
//                            store.load();   
                        },
                        
                        minWidth: 100,
                        minHeight: 10
                    },
//                    {
//                        iconCls: 'icon-search',
//                        text: 'Add Sub Department',
//                        action: 'open_Add_Sub_Department_window',
//                        minWidth: 100,
//                        minHeight: 10
//                    },
                    '->',
//                    {
//                        iconCls: 'icon-search',
//                        text: 'Edit',
//                        action: 'open_Edit_window',
//                        minWidth: 100,
//                        minHeight: 10
//                    }, 
                    {
                        iconCls: 'icon-search',
                        text: 'Add New Category',
                        action: 'open_window',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }
//            {
//                xtype: 'toolbar',
////                xtype: 'form',
//                dock: 'top',
//                items: [{
//                        xtype: 'button',
//                        text: 'Clear Search',
//                        scale: 'large',
////                        icon: 'resources/res/img.jpg',
//                        minWidth: 100,
//                        minHeight: 50,
//                        handler: function() {
//                            userGrid.filters.clearFilters();
//                        }
//
//                    }, '->', {
//                        xtype: 'button',
//                        text: 'Add New Category',
//                        scale: 'large',
////                        icon: 'resources/res/img.jpg',
//                        action: 'open_window',
//                        minWidth: 100,
//                        minHeight: 50
//
//                    }]
//            }
        ];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": "1", "value": "Active"},
                {"name": "2", "value": "Inactive"}
            ]
        });
        return [
//            {text: 'User ID', dataIndex: 'USER_ID'},
//            {text: 'First Name', dataIndex: 'FIRST_NAME', flex: 1},
//            {text: 'Last Name', dataIndex: 'LAST_NAME'},
//            {text: 'Common Name', dataIndex: 'COMMON_NAME'},
//            {text: 'Designation', dataIndex: 'DESIGNATION'},
//            {text: 'Department', dataIndex: 'BIS_ID'},
//            {text: 'Tel.No', dataIndex: 'TELEPHONE_NO'},
//            {text: 'Email', dataIndex: 'EMAIL'},
//            {text: 'Reg.Date', dataIndex: 'REGISTERED_DATE'},
//            {text: 'Inactive Date', dataIndex: 'INACTIVE_DATE'},
//            {text: 'Status', dataIndex: 'STATUS'},

            {
                xtype: 'searchablecolumn',
                text: ' Category ID',
                dataIndex: 'cateId',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Name',
                dataIndex: 'cateName',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Description',
                dataIndex: 'cateDesc',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
//                searchable: false,
                text: 'Status',
                dataIndex: 'cateStatus',
                flex: 2,
                renderer: function (value) {
                    if (value === 1) {
                        return "Active";
                    }
                    if (value === 2) {
                        return "Inactive";
                    }

                }
            }, {
                xtype: 'actioncolumn',
                text: 'Edit',
                menuDisabled: true,
                width: 70,
                items: [{
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('categoryEdit', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                text: 'View',
                menuDisabled: true,
                width: 70,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('categoryView', grid, rec);
                        }
                    }]
            }

        ];
    },
    buildFeatures: function () {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'cateId',
                    type: 'int'
//                    width: 50
                }, {
                    type: 'string',
                    dataIndex: 'cateName'
                }, {
                    type: 'string',
                    dataIndex: 'cateDesc'
                }, {
                    dataIndex: 'cateStatus',
                    type: 'int'
                }
            ]
        };
        return [filtersCfg];
    }

});

