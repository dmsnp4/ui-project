Ext.define('singer.view.Administrator.warrentyScheme.NewScheme', {
    extend: 'Ext.window.Window',
    alias: 'widget.newscheme',
    title: 'Warranty Scheme',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
//    height: 600,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
            ]
        });
        return [
            {
                fieldLabel: 'Warranty Scheme',
                name: 'schemeName',
                maxLength: 100,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/
            },
            {
                fieldLabel: 'Warranty Period',
                name: 'period',
                maxLength: 100,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                regex: /^[0-9]+$/
            },
            {
                fieldLabel: 'Active Status',
                xtype: 'combobox',
                editable: false,
                name: 'status',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                allowBlank: false,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
//                        ////console.log(me.getValue());
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }


                    }
                }
            }
        ];
    },
    buildButtons: function () {
        return [
//            , {
//                text: 'Cancel',
//                action: 'cancel'
//
//            }, {
//                text: 'Save',
//                action: 'save'
//
//            }

            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newscheme-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newscheme-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});


