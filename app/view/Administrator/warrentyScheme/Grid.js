Ext.define('singer.view.Administrator.warrentyScheme.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.warschgrid',
    loadMask: true,
    store: 'WarrantySchemes',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.toolbar.Paging'
    ],
    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var thsiGrid = this;
        thsiGrid.columns = thsiGrid.buildColumns();
        thsiGrid.dockedItems = thsiGrid.buildDockItems(this);
        thsiGrid.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->', {
                        text: 'Add New Warranty Scheme',
                        action: 'open_window'
                    }]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'WarrantySchemes',
                dock: 'bottom',
                displayInfo: true
            }
//            
        ];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": 1, "value": "Active"},
                {"name": 2, "value": "Inactive"}
            ]
        });
        return [
            {
                xtype: 'searchablecolumn',
                text: 'Shchema ID',
                dataIndex: 'schemeId'
            }, {
                xtype: 'searchablecolumn',
                text: 'Warranty Scheme',
                dataIndex: 'schemeName'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Warranty Period',
                dataIndex: 'period'
            },{
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'status',
                renderer: function (value) {
                    if (value === 1) {
                        return "Active";
                    }
                    if (value === 2) {
                        return "Inactive";
                    }

                }
            },            
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'Edit',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('ediWarScheme', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'View',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('viewWarScheme', grid, rec);
                        }
                    }]
            }
//            

        ];
    },
});

