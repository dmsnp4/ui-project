




Ext.define('singer.view.Administrator.sparesMaintenance.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.editspares',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    
    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [           
            {
                fieldLabel: 'Part No',
                name: 'partNo',
                readOnly: 'true'
            },
             {
                fieldLabel: 'Part Code',
                name: 'erpPartNo',
                readOnly: 'true'
            },
             {
                fieldLabel: 'Part Description',
                name: 'partDesc',
                readOnly: 'true'
            },
            {
                fieldLabel: 'Alternate Part Exist',
                name: 'partExist',
                readOnly: 'true'
            },
             {
                fieldLabel: 'Part Product Code',
                name: 'partPrdCode',
                readOnly: 'true'
            },
             {
                fieldLabel: 'Part Product Description',
                name: 'partPrdDesc',
                readOnly: 'true'
            },
             {
                fieldLabel: 'Part Product Family',
                name: 'partPrdFamily',
                readOnly: 'true'
            },
            {
                fieldLabel: 'Part Product Family Description',
                name: 'partPrdFamilyDesc',
                readOnly: 'true'
            },
            {
                fieldLabel: 'Commodity Group 1',
                name: 'partGroup1',
                readOnly: 'true'
            },
            {
                fieldLabel: 'Commodity Group 2',
                name: 'partGroup2',
                readOnly: 'true'
            },
            {
                fieldLabel: 'Selling Price',
                name: 'partSellPrice'
            },
            {
                fieldLabel: 'Part Cost',
                name: 'partCost',
                readOnly: 'true'
            }
        ];
    },
    buildButtons: function () {
        return [
             {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});