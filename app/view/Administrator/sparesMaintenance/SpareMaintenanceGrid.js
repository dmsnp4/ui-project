


Ext.define('singer.view.Administrator.sparesMaintenance.SpareMaintenanceGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sparemaintenancegrid',
//    requires: [
//        'Ext.grid.*'
//    ],
    store: 'Spares',
    //forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',

    initComponent: function() {
        ////console.log("initComponent of spare maintenance grid");
        var spareMaintenancegrid = this;
        spareMaintenancegrid.columns = spareMaintenancegrid.buildColumns();
        spareMaintenancegrid.dockedItems = spareMaintenancegrid.buildDockedItems();
        spareMaintenancegrid.callParent();
    },
    buildColumns: function() {
        
        return [
            {
                xtype: 'searchablecolumn',
                header: 'Part No',
                dataIndex: 'partNo',
              //  flex: 1
            },
             {
                xtype: 'searchablecolumn',
                header: 'Part Code',
                dataIndex: 'erpPartNo',
              //  flex: 1
            },
            {
                xtype: 'searchablecolumn',
                header: 'Part Description',
                dataIndex: 'partDesc',
              //  flex: 2
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Alternate part Exist',
                dataIndex: 'partExist',
              //  flex: 2,
                renderer: function(val) {
                    if (val === "M") {
                        return "Yes";
                    } else {
                        return "No";
                    }
                }
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Part Product Code',
                dataIndex: 'partPrdCode',
              //  flex: 3
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Part Product Description No.',
                dataIndex: 'partPrdDesc',
              //  flex: 4
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Part Product Family',
                dataIndex: 'partPrdFamily',
              //  flex: 4
                        //  resizable: false
            }, {
                xtype: 'searchablecolumn',
                header: ' Product Family Description',
                dataIndex: 'partPrdFamilyDesc',
              //  flex: 4
                        //  resizable: false
            }, {
                xtype: 'searchablecolumn',
                header: 'Commodity Group 1',
                dataIndex: 'partGroup1',
             //   flex: 3
                        //  resizable: false
            }, {
                xtype: 'searchablecolumn',
                header: 'Commodity Group 2',
                dataIndex: 'partGroup2',
              //  flex: 3
                        //  resizable: false
            }, {
                xtype: 'searchablecolumn',
                header: 'Selling Price(Rs)',
                dataIndex: 'partSellPrice',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
              //  flex: 2
                        //  resizable: false
            },{
                xtype: 'searchablecolumn',
                header: 'Part Cost(Rs)',
                dataIndex: 'partCost',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
               // flex: 2
                        //  resizable: false
            }, {
                header: 'View',
                xtype: 'actioncolumn',
                 menuDisabled: true,
                width: 50,
                items: [{
                        icon: 'resources/res/view-icon.png', // Use a URL in the icon config
                        tooltip: 'view',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('sparesView', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            },
            {
                header: 'Edit',
                xtype: 'actioncolumn',
                menuDisabled: true,
                width: 50,
                items: [{
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('editSpares', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            }
        ];
    },
    buildDockedItems: function() {
        return [{xtype: 'form',
//                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch'
                                //  id: 'suppliersgrid_search'
                    }]
            }, {
                xtype: 'pagingtoolbar',
                store: 'Spares',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});