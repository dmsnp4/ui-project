

Ext.define('singer.view.Administrator.sparesMaintenance.SpareMaintenanceView', {
    extend: 'Ext.window.Window',
    alias: 'widget.spareView',
    iconCls: 'icon-user',
    title: 'System User Details',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 610,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 200,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        return [{
                fieldLabel: 'Part No:',
                name: 'partNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            
            {
                fieldLabel: 'Part Code',
                name: 'erpPartNo',
                 listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Part Description',
                name: 'partDesc'
            },
            {
                fieldLabel: 'Alternate Part Exist',
                name: 'partExist',
                renderer: function (val) {
                    if (val === "M") {
                        return "Yes";
                    } else {
                        return "No";
                    }
                }
            },
            {
                fieldLabel: 'Part Product Code',
                name: 'partPrdCode'
            }, 
            {
                fieldLabel: 'Part Product Family',
                name: 'partPrdFamilyDesc'
            },
            {
                fieldLabel: 'Commodity Group 1',
                name: 'partGroup1'
            },
            {
                fieldLabel: 'Commodity Group 2',
                name: 'partGroup2'
            },
            {
                fieldLabel: 'Part Product Description',
                name: 'partPrdDesc'
            },
            {
                fieldLabel: 'Selling Price(Rs.)',
                name: 'partSellPrice',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            }, {
                fieldLabel: 'Part Cost(Rs.)',
                name: 'partCost',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            }
        ];
    },
    buildButtons: function () {
        return ['->', {
                text: 'Done',
                action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});
