
Ext.define('singer.view.Administrator.BuisnessStructure.UserAssignGridFrom', {
    extend: 'Ext.window.Window',
    alias: 'widget.userAssignGridForm',
    constrain: true,
    layout: 'fit',
    modal: true,
    width: 620,
    minWidth: 610,
//    height: 520,
    resizable: false,
    title: 'Assign User',
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 620,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
        });

        var groupItems = Ext.create('Ext.container.Container', {
            width: 620,
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formButtons = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.buildButtons(),
        });

        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                },
                items: [
                    formPanel,
                    groupItems,
                    formButtons
                ]
            }
        ];
        newUserForm.callParent();
    },
    buildGroupItems: function () {

        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    type: 'string',
                    dataIndex: 'userId'
                }
            ]
        };
        return Ext.create('Ext.Panel', {
            width: 610,
//            height: 500,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [{
                    xtype: 'panel',
                    flex: 3,
                    items: [
                        Ext.create('Ext.grid.Panel', {
                            id: 'availableUsers',
                            height: 500,
                            columns: [
                                {text: 'Available Users', dataIndex: 'userId', flex: '1'}
                            ],
                            features: [filtersCfg]
                        })
                    ]
                }, {
                    xtype: 'panel',
                    flex: 1,
                    layout: {type: 'vbox', align: 'center', pack: 'center'},
                    items: [
                        {
                            xtype: 'button',
                            text: '>',
                            tooltip: 'Assign one user',
                            action: 'nextOne',
                            width: '80%',
                            margin: '5'
                        },
                        {
                            xtype: 'button',
                            text: '>>',
                            tooltip: 'Assign all users',
                            action: 'nextAll',
                            width: '80%',
                            margin: '5'
                        },
                        {
                            xtype: 'button',
                            text: '<<',
                            action: 'prevAll',
                            tooltip: 'Unassign all users',
                            width: '80%',
                            margin: '5'
                        }, {
                            xtype: 'button',
                            text: '<',
                            tooltip: 'Unassign one user',
                            action: 'prevOne',
                            width: '80%',
                            margin: '5'
                        }
                    ]
                }, {
                    xtype: 'panel',
                    flex: 3,
                    items: [
                        Ext.create('Ext.grid.Panel', {
                            id: 'assignedUsers', //Do not duplicate the name
//                            store:'UserToBussiness',
                            height: 500,
                            columns: [
                                {text: 'Assigned Users', dataIndex: 'userId', flex: '1'}
                            ],
                            features: [filtersCfg]
                        })
                    ]
                }]
        });
    },
    buildItems: function () {
    },
    buildButtons: function () {
        return [
            {
//              iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            },
//            {
//                xtype: 'button',
//                text: 'Save',
//                action: 'save',
//                id: 'userAssignGridForm-save',
//                formBind: true
//            },
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                id: 'create',
                formBind: true,
            }

        ];
    },
    buildDockedItems: function () {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
                    },
//                    {
//                        xtype: 'button',
//                        text: 'Save',
//                        action: 'save',
//                        id: 'userAssignGridForm-save',
//                        formBind: true
//                    },
                    {
                        xtype: 'button',
                        text: 'Create',
                        action: 'create',
                        id: 'create',
                        formBind: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function () {
        return [];
    }

});