Ext.define('singer.view.Administrator.BuisnessStructure.BuisnessStructureGrid', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.buisnessStructure',
    //    id: 'bussinessTree',
    title: 'Buisness structure',
    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.tree.*'
    ],
    forceFit: true,
    selType: 'rowmodel',
    rootVisible: false,
    store: 'BuisnessTree',
    viewConfig: {
        plugins: {
            ptype: 'treeviewdragdrop'
        },
        getRowClass: function (record, index, rowParams) {
            if (record.data.status === 2) {
                return 'BSincCell';
            }
        }
    },
    useArrows: true,
    initComponent: function () {
        var buisnensGrid = this;
        buisnensGrid.columns = buisnensGrid.buildColumns();
        buisnensGrid.dockedItems = buisnensGrid.buildDockedItems();
        buisnensGrid.callParent();
    },
    buildColumns: function () {
        return [
            {
                xtype: 'treecolumn',
                header: 'Department Name',
                dataIndex: 'bisName',
                flex: 8,
            }, {
                header: 'Users',
                dataIndex: 'bisUserCount',
                flex: 2
            },



        ];
    },
    buildDockedItems: function () {
        return [{
                xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                style: {
                    backgroundColor: '#dfeaf2 !important'
                },
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                items: [
                    {
                        xtype: 'container',
                        combineErrors: true,
                        layout: 'hbox',
                        margin: '5',
                        items: [
                        ]
                    }
                ],
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Add Department',
                        action: 'open_add_department_form',
                        minWidth: 100,
                        minHeight: 10
                    }, {
                        iconCls: 'icon-search',
                        text: 'Add Sub Department',
                        action: 'open_sub_department_form',
                        minWidth: 100,
                        minHeight: 10
                    }, '->', {
                        text: 'Expand all',
                        action: 'onExpandAll',
                        minWidth: 100,
                        minHeight: 10
                    },
                    {
                        text: 'Collapse All',
                        action: 'onCollapseAll',
                        minWidth: 100,
                        minHeight: 10
                    }, '->', {
                        iconCls: 'icon-search',
                        text: 'Edit',
                        action: 'open_Edit_window',
                        minWidth: 100,
                        minHeight: 10
                    }, 
//           {
//                        text: 'Test',
//                        action: 'test',
//                        minWidth: 100,
//                        minHeight: 10,
////                        handler: function () {
////                            var pan = this.up('panel').up('panel');
////                            var s = pan.getSelectionModel().getSelection();
////                            //console.log(s);
////                            if (s[0])
////                            //                                alert(s[0].data.bisName + ' was selected');
////                                s.expand();
////                            else alert('no selected');
////                        }
//                    }, 
           {
                        iconCls: 'icon-search',
                        text: 'Users',
                        action: 'open_user_assign_window',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }

        ];
    },
});