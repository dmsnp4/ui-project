Ext.define('singer.view.Administrator.BuisnessStructure.UserAssignForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.userassign',
    title: 'Assign users for departments',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 610,
    minWidth: 610,
    height: 580,
    resizable: true,
    initComponent: function () {
        var assign = this;

        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 520,
            overflowY: 'auto',
            bodyPadding: '5 15 20 15',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                allowBlank: false,
                labelWidth: 200,
                margin: '10 0 1 30'
            },
            items: assign.buildItems()
        });
        assign.items = [formPanel];
        assign.callParent();
    },
    
    buildItems: function () {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    type: 'string',
                    dataIndex: 'firstName'
                }
            ]
        };
        return [
            {
                xtype: 'button',
                width: '150',
                text: 'Assign Users',
                action: 'open_user_assign_grid_window'
            }, {
                xtype: 'splitter'
            }, {
                xtype: 'splitter'
            }, {
                xtype: 'label',
                text: 'Assigned Users'
            }, {
                xtype: 'splitter'
            }, {
                xtype: 'grid',
                height: 450,
                store: 'UserAssignStore',
                columns: [
                    {
                        header: 'User ID',
                        dataIndex: 'userId'
                    },
                    {
                        header: 'User Name',
                        flex: 2,
                        dataIndex: 'firstName'
                    },
//                            {
//                                header:'status',
//                                flex: 1,
//                                dataIndex:'status',
//                                
//                            },
//                            {
//                                xtype: 'hiddenfield',
//                                dataIndex:'bisId'
//                            }
                ],
                features: [filtersCfg]
            },
            
            {
                xtype:'button',
                text:'Done',
                action:'done',
                margin: '2 1 20 1'
            },
            
        ];

    },
});