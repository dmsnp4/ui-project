Ext.define('singer.view.Administrator.BuisnessStructure.AddNewDepartmentForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.addDepartmentForm',
    title: 'Add Department',
    modal: true,
    id: 'businesForm',
    constrain: true,
    layout: 'fit',
    width: 510,
    minWidth: 510,
    height: 480,
    resizable: true,
    initComponent: function () {
        var addDepartmentForm = this;

        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            height: 420,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                allowBlank: false,
                labelWidth: 200,
                margin: '10 0 10 0',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
            },
            items: addDepartmentForm.buildItems(),
            buttons: addDepartmentForm.buildButtons(),
        });
        addDepartmentForm.items = [formPanel];
        addDepartmentForm.callParent();
    },
    buildItems: function () {

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Active"
                },
                {
                    "status": 2,
                    "value": "Inactive"
                }
            ]
        });

        var statOfline = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Allow"
                },
                {
                    "status": 2,
                    "value": "Deny"
                }
            ]
        });
        var statOfline2 = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Block"
                },
                {
                    "status": 2,
                    "value": "Unblock"
                }
            ]
        });
        var statOfline3 = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "VDSR1"
                },
                {
                    "status": 2,
                    "value": "VDSR2"
                }
            ]
        });
        var yesno = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Yes"
                },
                {
                    "status": 2,
                    "value": "No"
                }
            ]
        });

        return [{
                fieldLabel: 'Department Name',
                name: 'bisName',
                maxLength: 60
            },
            //            {
            //                fieldLabel: 'Department Type',
            //                name: 'structType',
            //            },
            {
                fieldLabel: 'Department Type',
                name: 'structType',
                xtype: 'combobox',
                store: 'bsnsTypeStr',
                queryMode: 'local',
                displayField: 'bisTypeDesc',
                valueField: 'bisTypeId',
                maxLength: 60,
                allowBlank: false,
                listeners: {
                    scope: this,
                    change: function (me, newValue, oldValue) {
                        var title = Ext.getCmp('addDepartmentForm-poTitle');
                        var cheq = Ext.getCmp('addDepartmentForm-cheqTitle');
                        var essd = Ext.getCmp('addDepartmentForm-eddsPct');
                        var inv = Ext.getCmp('addDepartmentForm-essdAcnt');
                        var erp = Ext.getCmp('addDepartmentForm-erpCode');
                        if (newValue === 8 || newValue === 7) {
                            title.show();
                            title.allowBlank = false;
                            cheq.show();
                            cheq.allowBlank = false;
                            essd.show();
                            essd.allowBlank = false;
                            inv.show();
                            inv.allowBlank = false;
                            erp.hide();
                            erp.allowBlank = true;
                            //                            Ext.getCmp('addDepartmentForm-erpCode').show();
                        } else if (newValue === 3) {
                            erp.show();
                            erp.allowBlank = false;
                            title.hide();
                            title.allowBlank = true;
                            cheq.hide();
                            cheq.allowBlank = true;
                            essd.hide();
                            essd.allowBlank = true;
                            inv.hide();
                            inv.allowBlank = true;
                        } else {
                            title.hide();
                            title.allowBlank = true;
                            cheq.hide();
                            cheq.allowBlank = true;
                            essd.hide();
                            essd.allowBlank = true;
                            inv.hide();
                            inv.allowBlank = true;
                            erp.hide();
                            erp.allowBlank = true;
                        }
                    }
                }
            },
            //fileds for distributro
            {
                fieldLabel: 'ERP Code',
                name: 'erpCode',
                id: 'addDepartmentForm-erpCode',
                disabled: true,
                hidden: true
            },
            {
                fieldLabel: 'PO Title',
                name: 'poTitle',
                id: 'addDepartmentForm-poTitle',
                allowBlank: true,
                hidden: true
            },
            {
                fieldLabel: 'Cheque Title',
                name: 'cheqTitle',
                id: 'addDepartmentForm-cheqTitle',
                allowBlank: true,
                hidden: true
            },
            {
                fieldLabel: 'ESSD Account No',
                name: 'essdAcntNo',
                id: 'addDepartmentForm-essdAcnt',
                allowBlank: true,
                hidden: true
            },
            {
                fieldLabel: 'ESSD %',
                name: 'eddsPct',
                id: 'addDepartmentForm-eddsPct',
                allowBlank: true,
                hidden: true,
                regex: /^[0-9_]+$/,
                listeners: {
                    scope: this,
                    change: function (me, newValue, oldValue) {
//                        //console.log(newValue)
                        if (newValue > 100) {
                            me.setValue('100');
                        }
                    }
                }
            },
//            {
//                fieldLabel: 'Invoic No',
//                name: 'invoiceNo',
//                id: 'addDepartmentForm-invoice',
//                hidden: true,
//                allowBlank: true,
//            },
            {
                fieldLabel: 'Credit Dealer',
                name: 'category1',
                xtype: 'checkbox',
              
                valueField: 'bsCatId',
                maxLength: 40,
                allowBlank: false,
            }, 
            {
                fieldLabel: 'Base Credit Limit',
                name: 'category1',
                xtype: 'textfield',
                value : '40000',
                valueField: 'bsCatId',
                maxLength: 40,
                allowBlank: false,
            }, 
            {
                fieldLabel: 'Max Credit Limit increase % for TM',
                name: 'category1',
                xtype: 'textfield',
                value : '25',
                valueField: 'bsCatId',
                maxLength: 40,
                allowBlank: false,
            }, 
           
            {
                fieldLabel: 'Max Credit Limit increase % TM',
                name: 'category1',
                xtype: 'textfield',
                value : '10',
                valueField: 'bsCatId',
                maxLength: 40,
                allowBlank: false,
            }, 
            
            {
                fieldLabel: 'Credit Block',
                name: 'statusOfline',
                xtype: 'combobox',
                store: statOfline2,
                queryMode: 'local',
                editable : false,
                displayField: 'value',
                valueField: 'status',
                allowBlank: false,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(2);
                        }
                    }
                }
            },
            {
                fieldLabel: 'Virtual DSR',
                name: 'statusOfline',
                xtype: 'combobox',
                store: statOfline3,
                queryMode: 'local',
                editable : false,
                displayField: 'value',
                valueField: 'status',
                allowBlank: false,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(2);
                        }
                    }
                }
            },
         
            {
                fieldLabel: 'Category 1',
                name: 'category1',
                xtype: 'combobox',
                store: 'BisIdsSto',
                queryMode: 'local',
                displayField: 'bsCateName',
                valueField: 'bsCatId',
                maxLength: 40,
                allowBlank: false,
            }, {
                fieldLabel: 'Category 2',
                name: 'category2',
                xtype: 'combobox',
                store: 'BisIdsSto',
                queryMode: 'local',
                displayField: 'bsCateName',
                valueField: 'bsCatId',
                maxLength: 40,
                allowBlank: false,
            }, {
                fieldLabel: 'Address',
                name: 'address',
                maxLength: 150,
                allowBlank: false,
            }, {
                fieldLabel: 'Contact No',
                name: 'teleNumber',
                //                allowBlank: false,
                xtype: 'textarea',
                maxLength: 35,
                minLength: 10,
                regex: /^[0-9_,\n]+$/,
                allowBlank: false,
            }, {
                fieldLabel: 'Show In Map',
                name: 'mapFlag',
                xtype: 'combobox',
                store: yesno,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                allowBlank: false,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    },
                    change: function (me, newValue, oldValue) {
                        var lan = Ext.getCmp('addDepartmentForm-lan');
                        var lon = Ext.getCmp('addDepartmentForm-lon');
                        if (newValue === 1) {
                            lan.show();
                            lan.allowBlank = false;
                            lon.show();
                            lon.allowBlank = false;
                        } else {
                            lan.hide();
                            lan.allowBlank = true;
                            lon.hide();
                            lon.allowBlank = true;
                        }
                    }
                }
            }, {
                fieldLabel: 'Longitude',
                name: 'logitude',
                allowBlank: false,
                id: 'addDepartmentForm-lon',
                //allowBlank: true,
                hidden: true,
                maxLength: 15,
                regex: /^[0-9]/,
            }, {
                fieldLabel: 'Latitude',
                name: 'latitude',
                allowBlank: false,
                id: 'addDepartmentForm-lan',
                //allowBlank: true,
                hidden: true,
                maxLength: 15,
                regex: /^[0-9]/,
            }, {
                fieldLabel: 'Description',
                name: 'bisDesc',
                //                allowBlank: true,
                maxLength: 150,
                allowBlank: false,
            }, {
                fieldLabel: 'Allow Ofline',
                name: 'statusOfline',
                xtype: 'combobox',
                store: statOfline,
                queryMode: 'local',
                editable : false,
                displayField: 'value',
                valueField: 'status',
                allowBlank: false,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(2);
                        }
                    }
                }
            },
            {
                fieldLabel: 'Status',
                name: 'status',
                xtype: 'combobox',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                allowBlank: false,
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }
                }
            },
            {
                xtype: 'hiddenfield',
                name: 'bisId',
                allowBlank: true,
                //                queryMode: 'local'
                //                displayField: 'user',
                //                valueField: 'user',
            },
            {
                xtype: 'hiddenfield',
                name: 'bisPrtnId',
                allowBlank: true,
                //                queryMode: 'local'
                //                displayField: 'user',
                //                valueField: 'user',
            }
        ];
    },
    buildButtons: function () {
        return [
            {
                //iconCls: 'icon-ban-circle icon-white',
                text: 'Cancel',
                //ui: 'danger',
                action: 'cancel'
            }, {
                //iconCls: 'icon-ok icon-white',
                text: 'Submit',
                //ui: 'success',
                action: 'submit_department',
                id: 'addDepartmentForm-save',
                //formBind: true
            }, {
                xtype: 'button',
                text: 'Create',
                //                        ui: 'success',
                action: 'create',
                id: 'addDepartmentForm-create',
                //                formBind: true
            }];
    }

});