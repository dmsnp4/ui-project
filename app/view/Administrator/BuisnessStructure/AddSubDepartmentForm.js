
Ext.define('singer.view.Administrator.BuisnessStructure.AddSubDepartmentForm',{
    extend: 'Ext.window.Window',
    alias: 'widget.addSubDepartmentForm',
    iconCls: 'icon-category',
    title: 'Add Sub Department',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 510,
    minWidth: 510,
    height: 480,
    resizable: true,
    
     initComponent: function() {
        var addDepartmentForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            height: 420,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                allowBlank: false,
                labelWidth: 200,
                margin: '10 0 10 0'
            },
            items: addDepartmentForm.buildItems(),
            buttons: addDepartmentForm.buildButtons(),
      
        });
        addDepartmentForm.items = [formPanel];
        addDepartmentForm.callParent();
    },
          
            buildItems: function() {
        
                var stat = Ext.create('Ext.data.Store', {
                fields: ['status', 'value'],
                data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
                ]
            });
                
                return [ {
                        fieldLabel: 'Department Name',
                        name: 'bisName',
                        maxLength: 99
                    }, {
                        fieldLabel: 'Catogorey 1',
                        name: 'category1',
                        xtype: 'combobox',
                        store: 'BuisnessCategories',
                        queryMode: 'local',
                        displayField: 'bsCatId',
                        valueField: 'bsCatId',
                        allowBlank: false,
                    }, {
                        fieldLabel: 'Catogory 2',
                        name: 'category2',
                        xtype: 'combobox',
                        store: 'BuisnessCategories',
                        queryMode: 'local',
                        displayField: 'bsCatId',
                        valueField: 'bsCatId',
                        allowBlank: false,
                    }, {
                        fieldLabel: 'Address',
                        name: 'address',
                        maxLength: 99,
                        allowBlank: false,
                    }, {
                        fieldLabel: 'Contact No',
                        name: 'teleNumber',
                        allowBlank: true,
                        maxLength: 10,
                        allowBlank: false,
                        //afterLabelTextTpl: ''
                    }, {
                        fieldLabel: 'Show In Map',
                        name: 'mapFlag',
                        xtype: 'combobox',
                        store: 'BuisnessStore',
                        queryMode: 'local',
                        displayField: 'mapFlag',
                        valueField: 'mapFlag',
                        allowBlank: false,
                    }, {
                        fieldLabel: 'Longitude',
                        name: 'logitude',
                        maxLength: 59,
                        allowBlank: false,
                    }, {
                        fieldLabel: 'Latitude',
                        name: 'latitude',
                        maxLength: 59,
                        allowBlank: false,
                    }, {
                        fieldLabel: 'Description',
                        name: 'bisDesc',
                        allowBlank: true,
                        maxLength: 59
                    }, {
                        fieldLabel: 'Status',
                        name: 'status',
                        xtype: 'combobox',
                        store: stat,
                        queryMode: 'local',
                        displayField: 'value',
                        valueField: 'status',
                        allowBlank: false,
                    }];
            },
            
            buildButtons: function (){
                return [{
                //iconCls: 'icon-ban-circle icon-white',
                text: 'Cancel',
                //ui: 'danger',
                action: 'cancel'
            },   {
                //iconCls: 'icon-ok icon-white',
                text: 'Submit',
                //ui: 'success',
                action: 'Sub_Department_submit',
                id: 'supplierform-save'
                //formBind: true
            }];
            }
});


