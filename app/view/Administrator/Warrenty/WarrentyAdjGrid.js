Ext.define('singer.view.Administrator.Warrenty.WarrentyAdjGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.warrentyadjgrid',
    loadMask: true,
    store: 'Warrenty',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
//    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var thisGrid = this;
        thisGrid.columns = thisGrid.buildColumns();
//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        thisGrid.dockedItems = thisGrid.buildDockItems(this);
        thisGrid.features = thisGrid.buildFeatures();
        thisGrid.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->', {
                        text: 'Add Warranty Adjustment',
                        action: 'open_window'
                    }]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'Warrenty',
                dock: 'bottom',
                displayInfo: true
            }
//            {
//                xtype: 'toolbar',
//                dock: 'bottom',
//                items: [{
//                        xtype: 'pagingtoolbar',
//                        store: 'RepairCategories',
//                        dock: 'bottom',
//                        displayInfo: true
//                    }]
//            }
        ];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": 1, "value": "Active"},
                {"name": 2, "value": "Inactive"}
            ]
        });
        return [
//            {
//                text: 'Status',
//                dataIndex: 'status',
//                isVisible:true
//            },
            {
                flex:1,
                xtype: 'searchablecolumn',
                text: 'IMEI No',
                dataIndex: 'iMEI'
            }, {
                flex:1,
                xtype: 'searchablecolumn',
                text: 'Extension Days',
                dataIndex: 'extendedDays'
            },
            {
                xtype: 'actioncolumn',
                width: 150,
                text: 'View',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('viewWarrentyAdj', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 150,
                text: 'Edit',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('editWarrentyAdj', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 150,
                text: 'Delete',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/delete-icon.png',
                        tooltip: 'Delete',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('onDelete', grid, rec);
                        }
                    }]
            }
//            {
//                xtype: 'actioncolumn',
//                width: 50,
//                items: [{
//                        icon: 'resources/res/remove_user.png',
//                        tooltip: 'Unassign User',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('userView', grid, rec);
//                        }
//                    }]
//            }

        ];
    },
    buildFeatures: function () {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'groupId',
                    type: 'numeric'

                }, {
                    type: 'string',
                    dataIndex: 'groupName'
                }
            ]
        };
        return [filtersCfg];
    }

});

