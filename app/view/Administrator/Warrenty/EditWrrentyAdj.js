/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.Warrenty.EditWrrentyAdj', {
    extend: 'Ext.window.Window',
    alias: 'widget.editwarrentyadj',
    //    xtype:'userview',
    iconCls: 'icon-user',
    //    title: 'Repair Category List',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    //    height: 240,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            //            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        return [{
                fieldLabel: 'IMEI No',
                name: 'iMEI',
                allowBlank: false,
                maxLength: 20,
                regex: /^[0-9a-zA-Z]/
            },
            {
                fieldLabel: 'Extenstion Days',
                name: 'extendedDays',
                id: 'fieldtwo',
                regex: /^[0-9]+$/,
                allowBlank: false,
                maxLength: 30
            },
            {
                fieldLabel: 'Extenstion Days',
                xtype:'displayfield',
                id: 'editwarrentyadj-viewExtendDays',
                readOnly:true
            }
        ];
    },
    buildButtons: function () {
        return ['->', {
                text: 'Close',
                action: 'cancel'
            },
            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'editwarrentyadj-save',
                formBind: true,
                //                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
                //                        ui: 'success',
                action: 'create',
                id: 'editwarrentyadj-create',
                formBind: true,
                //                visible: false
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});