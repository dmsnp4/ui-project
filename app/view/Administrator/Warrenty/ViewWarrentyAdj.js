/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.Warrenty.ViewWarrentyAdj', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewwarrentyadj',
//    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Repair Category List',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
//    height: 240,
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {
        return [
            {
                fieldLabel: 'IMEI No',
                maxLength: 15,
                name: 'iMEI',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Extension Days',
                name: 'extendedDays'
            }
        ];
    },
    buildButtons: function() {
        return ['->', {
                text: 'Close',
                action: 'cancel'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});



