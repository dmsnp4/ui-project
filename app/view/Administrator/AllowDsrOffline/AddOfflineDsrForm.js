

Ext.define('singer.view.Administrator.AllowDsrOffline.AddOfflineDsrForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.addOfflinedsrForm',
    loadMask: true,
    modal: true,
    title: 'Change Credit Limit',
    layout: 'fit',
    constrain: true,
    width: 650,
    initComponent: function () {

        var newDefectForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newDefectForm.buildItems(),
            buttons: newDefectForm.buildButtons()
        });

        newDefectForm.items = [formPanel];
        newDefectForm.callParent();
    },
    buildItems: function () {

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Active"
                },
                {
                    "status": 2,
                    "value": "Inactive"
                }
            ]
        });
        var statOfline = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            id: 'storeOfflineAllow',
            data: [
                {
                    "status": 1,
                    "value": "Allow"
                },
                {
                    "status": 2,
                    "value": "Deny"
                }
            ]
        });
        var yesno = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Yes"
                },
                {
                    "status": 2,
                    "value": "No"
                }
            ]
        });

        return [

            {
                xtype: 'label',
                flex: 1,
                id: 'current-bis-loc'
            },
            //items
            {
                xtype: 'displayfield',
                fieldLabel: 'Dealer Name',
                value: 'Dealer 1',
                // name: "serviceCenterAddress",
                afterLabelTextTpl: "",
                allowBlank: false,
                id: 'addrs'
            },

            {

                xtype: "textfield",
                id: 'contact_no',
                name: 'workTelephoneNo',
                fieldLabel: 'New Tempory Credit Limit %',
                regex: /^[0-9_]+$/,
                //action: 'clickRdio',
                readOnly: false,
                //allowBlank: true,
                maxLength: 10
            },
            {

                xtype: "displayfield",
                id: 'contact_noiu',
                name: 'workTelephoneNo',
                fieldLabel: 'New Tempory Credit Limit (Rs.)',
                regex: /^[0-9_]+$/,
                //action: 'clickRdio',
                readOnly: true,
                value : '510000',
                //allowBlank: true,
                maxLength: 10
            },
//            {
//
//                xtype: "textfield",
//                id: 'contact_no5',
//                name: 'workTelephoneNo',
//                fieldLabel: 'Current Credit Limit %',
//                regex: /^[0-9_]+$/,
//                //action: 'clickRdio',
//                readOnly: true,
//                //allowBlank: true,
//                maxLength: 10
//            },
            {

                xtype: "textfield",
                id: 'contact_no51',
                name: 'workTelephoneNo',
                fieldLabel: 'Distributor Credit Limit %',
                value: '5',
                regex: /^[0-9_]+$/,
                //action: 'clickRdio',
                readOnly: true,
                //allowBlank: true,
                maxLength: 10
            },
            {

                xtype: "textfield",
                id: 'contact_no52',
                name: 'workTelephoneNo',
                fieldLabel: 'TM Credit Limit %',
                regex: /^[0-9_]+$/,
                value: '15',
                //action: 'clickRdio',
                readOnly: true,
                //allowBlank: true,
                maxLength: 10
            },
            {

                xtype: "textfield",
                id: 'contact_no542',
                name: 'workTelephoneNo',
                fieldLabel: 'Base Credit Limit',
                value: '500000',
                regex: /^[0-9_]+$/,
                //action: 'clickRdio',
                readOnly: true,
                //allowBlank: true,
                maxLength: 10
            },
//            {
//                xtype: 'textfield',
//                fieldLabel: 'Contact No:',
//                //margin: '10 10 10 10',
//                name: '',
//                id: 'contact_no'
//
//            },

            //End items



//            {
//                xtype: 'combo',
//                fieldLabel: 'Distributor/Shop',
//                id: 'select-swap-bis-id',
//                store: 'distributorstr',
//                displayField: 'bisName',
//                valueField: 'bisId',
//                queryMode: 'remote',
//                hidden: true
//            },

            {
                xtype: 'hiddenfield',
                id: 'hdn-bis-id',
                value: ''
            }
        ];

    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'imei-swap-save',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});

