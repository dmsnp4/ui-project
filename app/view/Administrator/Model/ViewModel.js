
Ext.define('singer.view.Administrator.Model.ViewModel', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewmodel',
//    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Repair Category List',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
//    height: 240,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 150,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        return [
//            
            {
                fieldLabel: 'Model No',
                name: 'model_No',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Part Description',
                name: 'model_Description'
            },
            {
                fieldLabel: 'Alternate Part Exists',
                name: 'altPartsExistDesc',
//                renderer: function (value) {
//                    if (value === '1') {
//                        return "Yes";
//                    }
//                    if (value === '2') {
//                        return "No";
//                    }
//
//                }
            },
            {
                fieldLabel: 'ERP Code',
                name: 'erp_part'
            }, {
                fieldLabel: 'Part Product Code Descriptin',
                name: 'art_Prd_Code_Desc'
            }, {
                fieldLabel: 'Part Product Family',
                name: 'part_Prd_Family'
            }, {
                fieldLabel: 'Commodity Group 1',
                name: 'commodity_Group_1'
            }, {
                fieldLabel: 'Commodity Group 2',
                name: 'commodity_Group_2'
            }, {
                fieldLabel: 'Part Product Family Description',
                name: 'part_Prd_Family_Desc'
            }, {
                fieldLabel: 'Selling Price(Rs.)',
                name: 'selling_Price',
                regex: /^[0-9.,_]+$/,
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                },
            }, {
                fieldLabel: 'Cost(Rs.)',
                name: 'cost_Price',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                },
                regex: /^[0-9.,_]+$/,
            },
//            {
//                fieldLabel: 'Dealer Margin',
////                name: 'fname'
//            },
            {
                xtype: 'textfield',
                fieldLabel: 'Delar Margin(%)',
                name: 'dealer_Margin',
                maxLength: 10,
                regex: /(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/,
            }, {
                xtype: 'textfield',
                fieldLabel: 'Distributor Margin(%)',
                name: 'distributor_Margin',
                maxLength: 10,
                regex: /(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/,
            }, {
                xtype: 'combobox',
                fieldLabel: 'Repair Category',
                name: 'rep_Category_id',
                store: 'RepCats',
                queryMode: 'local',
                displayField: 'reCateDesc',
                valueField: 'reCateId',
                allowBlank: false
            },
            {
                xtype: 'combobox',
                fieldLabel: 'Warranty Scheme',
                name: 'wrn_Scheme_id',
                store: 'WarrentySchStore',
                queryMode: 'local',
//                displayField: 'reCateId',
//                valueField: 'reCateId',
                displayField: 'schemeName',
                valueField: 'schemeId',
                allowBlank: false
            },
//            {
//                xtype: 'combobox',
//                fieldLabel: 'Warrenty Scheme',
//                name: 'wrn_Scheme_id',
//                store:'WarrentySchemeStore',
//                queryMode: 'local',
//                displayField: 'schemeId',
//                valueField: 'schemeId',
//                allowBlank: false
//            }

        ];
    },
    buildButtons: function () {
        return ['->',
            {
                text: 'Close',
                action: 'v-cancel',
                id: 'viewmodel-v-cancel'
            }, {
                text: 'Close',
                action: 'cancel',
                id: 'viewmodel-cancel'
            },
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'viewmodel-save',
                formBind: true,
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'viewmodel-create',
                formBind: true,
                visible: false
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});



