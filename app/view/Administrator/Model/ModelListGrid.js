Ext.define('singer.view.Administrator.Model.ModelListGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.modellistgrid',
    loadMask: true,
    store: 'ModelLists',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
//    forceFit: true,
    minHeight: 200,
//    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var thsiGrid = this;
        thsiGrid.columns = thsiGrid.buildColumns();
//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        thsiGrid.dockedItems = thsiGrid.buildDockItems(this);
        thsiGrid.features = thsiGrid.buildFeatures();
        thsiGrid.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'ModelLists',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": 1, "value": "Yes"},
                {"name": 2, "value": "No"}
            ]
        });
        return [
//            
            {
                xtype: 'searchablecolumn',
                text: 'Model No',
                dataIndex: 'model_No',
//                flex: '1'
            }, {
                xtype: 'searchablecolumn',
                text: 'Model Description',
                dataIndex: 'model_Description'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Alternate Part Description',
                dataIndex: 'altPartsExistDesc',
//                renderer: function (value) {
//                    if (value === 1) {
//                        return "Yes";
//                    }
//                    if (value === 2) {
//                        return "No";
//                    }
//
//                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'ERP Code',
                dataIndex: 'erp_part'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Part Product Description',
                dataIndex: 'art_Prd_Code_Desc'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Part Product Family',
                dataIndex: 'part_Prd_Family'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Product Family Description',
                dataIndex: 'part_Prd_Family_Desc'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Commodity Group1',
                dataIndex: 'commodity_Group_1'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Commodity Group2',
                dataIndex: 'commodity_Group_2'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Selling Price(Rs.)',
                dataIndex: 'selling_Price',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Cost(Rs.)',
                dataIndex: 'cost_Price',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Dealer Margin(%)',
                dataIndex: 'dealer_Margin',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Distributor Margin(%)',
                dataIndex: 'distributor_Margin',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Repair Category',
                dataIndex: 'rep_Category'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Warranty Schema',
                dataIndex: 'wrn_Scheme'
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'View',
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config  
                        tooltip: 'View',
                        width: 50,
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('viewModel', grid, rec);
//                            ////console.log('clicked', this.up('grid'));
//                            this.getController('UserGroupController').viewUser(grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'Edit',
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('editModel', grid, rec);

                        }
                    }]
            }

        ];
    },
    buildFeatures: function () {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'groupId',
                    type: 'numeric'

                }, {
                    type: 'string',
                    dataIndex: 'groupName'
                }
            ]
        };
        return [filtersCfg];
    }

});


