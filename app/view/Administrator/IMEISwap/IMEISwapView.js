Ext.define('singer.view.Administrator.IMEISwap.IMEISwapView', {
    extend: 'Ext.window.Window',
    alias: 'widget.IMEISwapView',
    loadMask: true,
    modal: true,
    title: 'Swap IMEI',
    layout: 'fit',
    constrain: true,
    width: 500,
    initComponent: function() {

        var newDefectForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newDefectForm.buildItems(),
            buttons: newDefectForm.buildButtons()
        });

        newDefectForm.items = [formPanel];
        newDefectForm.callParent();
    },
    buildItems: function() {
        return [
            {
                xtype: 'panel',
                layout: 'hbox',
                style: 'border: none;',
                items:[
                    {
                        xtype: 'textfield',
                        fieldLabel: 'IMEI',
                        flex: 5,
                        id: 'swap-imei'
                    },
                    {
                        xtype: 'button',
                        flex: 1,
                        action: 'validate',
                        text: 'validate',
                        id: 'validate-bis-loc',
                        style: 'margin-top: 10px; margin-left: 14px;'
                    },
                     {
                        xtype: 'label',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'label',
                flex: 1,
                id: 'current-bis-loc'
            },
           
            
            
            
            {
                xtype: "radiogroup",
                id: 'radio-group-select',
                fieldLabel: 'Change',
                action: 'clickRdio',
                columns: 3,
                items: [
                    {
                        boxLabel: 'Change IMEI',
                        checked: true,
                        id: 'dsrrad',
                        name: 'type',
                        inputValue: '1'
                    },
                    {
                        boxLabel: 'Change Location',
                        name: 'type',
                        id: 'subrad',
                        inputValue: '2'
                    }
                ]
            },
            
            {
                xtype: 'combo',
                fieldLabel: 'Distributor/Shop',
                id: 'select-swap-bis-id',
                store: 'distributorstr',
                displayField: 'bisName',
                valueField: 'bisId',
                queryMode: 'remote',
                hidden: true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'IMEI',
                flex: 5,
                id: 'enter-swap-imei',
                regex: /^[0-9_V]+$/,
                minLength: 15,
                maxLength: 15
            },
            {
                xtype: 'hiddenfield',
                id: 'hdn-bis-id',
                value: ''
            }
        ];

    },
    buildButtons: function() {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'imei-swap-save',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});

