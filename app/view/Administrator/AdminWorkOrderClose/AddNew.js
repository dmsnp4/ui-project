

Ext.define('singer.view.Administrator.AdminWorkOrderClose.AddNew', {
    extend: 'Ext.form.Panel',
    alias: 'widget.admin_add_wo_close',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 530,

    initComponent: function () {

        var registerIMEI = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: registerIMEI.buildItems(),
            buttons: registerIMEI.buildButtons()
        });

        registerIMEI.items = [formPanel];

        registerIMEI.callParent();
    },
    buildItems: function () {
        return [
            {
                xtype: 'combo',
                fieldLabel: 'WO Number',
                name: 'workOrderNo',
                allowBlank: false,
                store: 'clseWOnum',
                displayField: 'workOrderNo',
                valueField: 'workOrderNo',
                queryMode: 'remote',
                listeners:{
                      beforequery: function (record) {
                      record.query = new RegExp(record.query, 'i');
                      record.forceAll = true;
                    }
                }
            },
            {
                xtype: 'hiddenfield',
                name: 'status'
            },
            {
                xtype: 'combo',
                fieldLabel: 'Repair Status',
                name: 'repairid',
                id:'repairNAME',
                maxLength: 100,
                allowBlank: false,
                store: 'clseRprStore',
                displayField: 'description',
                valueField: 'code',
                visible: true,
                queryMode: 'remote',
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        var rpairStatus = cmp.nextSibling();
                        if (oldValue !== undefined) {
                            if (oldValue !== newValue) {
                                if(cmp.up('window').title === 'Add New'){
                                    rpairStatus.hide();
                                }else{
                                    rpairStatus.show();
//                                    rpairStatus.allowBlank=false;
                                }
                            }
                        }
                    }
                }
            },
            {
                id:'RstsREMARK',
                fieldLabel: 'Remark',
                name: 'remarks',
                maxLength: 100,
//                allowBlank: false,
                hidden:true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Delivery Type',
                name: 'deleveryId',
                //             name:'deleveryType',
                maxLength: 100,
                allowBlank: false,
                store: 'DeliverType',
                displayField: 'description',
                valueField: 'code',
                visible: true,
                queryMode: 'remote',
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        var courNo = cmp.nextSibling().nextSibling();
                        if(newValue===2){                            
                            courNo.show();
                            courNo.allowBlank = false;
                        }else{
                            courNo.hide();
                            courNo.allowBlank = true;
                            courNo.setValue('');
                        }
                    }
                }
            },
            {
                xtype: 'combo',
                fieldLabel: 'Transfer Location',
                name: 'locationId',
                maxLength: 100,
                allowBlank: false,
                store: 'distributorstr',
                displayField: 'bisName',
                valueField: 'bisId',
                visible: true,
                queryMode: 'remote',
            },
            {
                fieldLabel: 'Courier No',
                name: 'courierNo',
                maxLength: 100,
                allowBlank: false,
                hidden:true
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Delivery Date',
                name: 'deleverDate',
                maxLength: 100,
                allowBlank: false,
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false,
                minValue:new Date(),
            },
            {
                fieldLabel: 'Gate Pass Or Other Reference',
                name: 'gatePass',
                maxLength: 100,
//                allowBlank: false
            }
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Save',
                action: 'editNewSubmit',
                formBind: true,
                id: 'addNewWorkOrderclosing-save',
            },
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Create',
                action: 'addNewSubmit',
                formBind: true,
                id: 'addNewWorkOrderclosing-create',
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'work_order_closing_cancel'
            },

        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }
});