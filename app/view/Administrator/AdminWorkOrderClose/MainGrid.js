


Ext.define('singer.view.Administrator.AdminWorkOrderClose.MainGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.admin_wo_close',
    store: 'GetWorkOrderClose',
    minHeight: 200,
    selType: 'rowmodel',
    
    initComponent: function() {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },
    
    buildColumns: function() {
        return [
            
            {
                width:150,
                xtype: 'searchablecolumn',
                header: 'Work Order No',
                dataIndex: 'workOrderNo',
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                header: 'Repair Status ',
                dataIndex: 'repairSatus',
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                header: 'Delivery Type ',
                dataIndex: 'deleveryType',
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                header: 'Transfer Location ',
                dataIndex: 'transferLocation',
            },

            {
                width:150,
                xtype: 'searchablecolumn',
                header: ' Courier No',
                dataIndex: 'courierNo',
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                header: 'Delivery Date ',
                dataIndex: 'deleverDate',
            },
            {
                width:150,
                xtype: 'searchablecolumn',
                header: 'Gate Pass ',
                dataIndex: 'gatePass',
            },
            
            {
                xtype: 'actioncolumn',
                header: 'View',
                width: 100,
                menuDisabled:true,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('onView', grid, record);
//                            ////console.log(this.up('grid'));
                        }
                    }]
            },
//            {
//                xtype: 'actioncolumn',
//                header: 'Edit',
//                width: 100,
//                menuDisabled:true,
//                items: [{
//                        icon: 'resources/images/edit.png', // Use a URL in the icon config
//                        tooltip: 'Edit',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('onedit', grid, record);
////                            ////console.log(this.up('grid'));
//                        }
//                    }]
//            },
//            {
//                xtype: 'actioncolumn',
//                header: 'Delete',
//                width: 100,
//                menuDisabled:true,
//                items: [{
//                        icon: 'resources/images/delete-icon.png', // Use a URL in the icon config
//                        tooltip: 'Delete',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('ondelete', grid, record);
////                            ////console.log(this.up('grid'));
//                        }
//                    }]
//            },
//            {
//                xtype: 'actioncolumn',
//                header: 'Print Receipt',
//                menuDisabled:true,
//                width: 100,
//                items: [{
//                        icon: 'resources/images/printerr.png', // Use a URL in the icon config
//                        tooltip: 'Print Receipt',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('prntRecipt', grid, record);
////                            ////console.log(this.up('grid'));
//                        }
//                    }]
//            }
            

        ];
    },
    
    buildDockedItems: function() {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },

                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }
                    ,'->',{
                        iconCls: 'icon-search',
                        text: 'Add New',
                        action: 'add_new',
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                 store: 'GetWorkOrderClose',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});


