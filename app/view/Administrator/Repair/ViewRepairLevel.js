/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.Repair.ViewRepairLevel',{
    extend: 'Ext.window.Window',
    alias: 'widget.viewrepairlevel',
//    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Repair Category List',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
//    height: 240,
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {
        return [
            
            {
                fieldLabel: 'Level Code',
                name: 'levelCode'
            },
            {
                fieldLabel: 'Level Description',
                name: 'levelDesc'
            },{
                fieldLabel: 'Repair Category',
                name: 'levelCatId'
            },{
                fieldLabel: 'Price (Rs.)',
                name: 'levelPrice',
                renderer: function(value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
//            {
//                fieldLabel: 'Status',
//                name: 'levelStatus',
//                renderer: function(value) {
//                    if (value === 1) {
//                        return "Active";
//                    }
//                    if (value === 2) {
//                        return "Inactive";
//                    }
//
//                }
//            }
        ];
    },
    buildButtons: function() {
        return ['->', {
                text: 'Close',
                action: 'cancel'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});



