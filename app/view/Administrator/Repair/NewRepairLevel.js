/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.Repair.NewRepairLevel', {
    extend: 'Ext.window.Window',
    alias: 'widget.newrepairlevel',
    title: 'Repair Levels - Category - 1',
    layout: 'fit',
    constrain: true,
    modal: true,
    width: 500,
//    height: 600,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
            ]
        });
        return [{
                fieldLabel: 'Cat id',
                name: 'levelCatId',
                visible: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
            }, {
                xtype:'capfield',
                fieldLabel: 'Level Name',
                name: 'levelName',
                allowBlank: false,
                maxLength: 40,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                listeners: {
                    afterrender: function(field) {
                      field.focus();
                    }
                  }
            },
            {
                fieldLabel: 'Level Description',
                name: 'levelDesc',
                maxLength: 100,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/
            }, {
                fieldLabel: 'Price (Rs.)',
                name: 'levelPrice',
                maxLength: 10,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                regex: /^[0-9.,_]+$/,
                listeners: {
                    scope: this,
                    afterrender: function (me) {
                        var value = Ext.util.Format.number(me.getValue(), '0,000.00');
                        me.setValue(value);
                    }
//                  
                }

            },
            {
                fieldLabel: 'Active Status',
                xtype: 'combobox',
                name: 'levelStatus',
                editable: false,
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }
                }
            }
        ];
    },
    buildButtons: function () {
        return [
//            , {
//                text: 'Cancel',
//                action: 'cancel'
//
//            }, {
//                text: 'Save',
//                action: 'save'
//
//            }

            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newrepairlevel-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newrepairlevel-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});




