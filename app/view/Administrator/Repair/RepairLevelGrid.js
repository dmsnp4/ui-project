
Ext.define('singer.view.Administrator.Repair.RepairLevelGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.repairlevelgrid',
//    title: 'Assing Users',
    height: 300,
    width: 800,
    layout: 'fit',
    store: 'RepairLevels',
//    forceFit: true,
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->', {
                        text: 'Add New Repair Level',
                        action: 'open_window'
                    }]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'RepairLevels',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": 1, "value": "Active"},
                {"name": 2, "value": "Inactive"}
            ]
        });
        return [
            {
                xtype: 'searchablecolumn',
                text: 'Level Code',
                dataIndex: 'levelCode',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Level Name',
                dataIndex: 'levelName',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Level Description',
                dataIndex: 'levelDesc',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Repair Category',
                dataIndex: 'levelCatId',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Price(Rs.)',
                dataIndex: 'levelPrice',
                flex:2,
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'levelStatus',
                flex:2,
                renderer: function (value) {
                    if (value === 1) {
                        return "Active";
                    }
                    if (value === 2) {
                        return "Inactive";
                    }

                }
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'View',
                menuDisabled: true,
                flex:1,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config  
                        tooltip: 'View',
                        width: 50,
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('viewLevel', grid, rec);
//                            
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'Edit',
                menuDisabled: true,
                flex:1,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('editLevel', grid, rec);

                        }
                    }]
            }

        ];
    }


});




