Ext.define('singer.view.Administrator.users.UserView', {
    extend: 'Ext.window.Window',
    alias: 'widget.userview',
    //    xtype:'userview',
    iconCls: 'icon-user',
    title: 'System User Details',
    modal: true,
    constrain: true,
    loadMask: true,
    layout: 'fit',
    width: 430,
    //    height: 240,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            //            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        return [{
                fieldLabel: 'User ID',
                name: 'userId',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
 //            {
 //                fieldLabel: 'User Type',
 //                name: 'user_type',
 //                listeners: {
 //                    afterrender: function(comp) {
 //                        if (parseInt(comp.getValue()) === 1)
 //                            comp.setValue('Admin');
 //                        if (parseInt(comp.getValue()) === 2)
 //                            comp.setValue('Default User');
 //                        if (parseInt(comp.getValue()) === 3)
 //                            comp.setValue('Inquiry User');
 //                    }
 //                }
 //
 //            }, 
            {
                fieldLabel: 'First Name',
                name: 'firstName'
            },
            {
                fieldLabel: 'Last Name',
                name: 'lastName'
            },
            {
                fieldLabel: 'Common Name',
                name: 'commonName'
            }, {
                fieldLabel: 'Designation',
                name: 'designation'
            },
            {
                fieldLabel: 'Telephone Number',
                name: 'telephoneNumber'
            },
            {
                fieldLabel: 'Email',
                name: 'email'
            },
            {
                fieldLabel: 'Active Status',
                name: 'status',
                renderer: function (value) {
                    if (value === '1') {
                        return "Active";
                    }
                    if (value === '9') {
                        return "Inactive";
                    }

                },
            },
            {
                fieldLabel: 'Registred Date',
                name: 'createdOn'
            },
            {
                fieldLabel: 'Inactive Date',
                name: 'inactivedOn'
            },
            {
                xtype: 'container',
                items: [
                    {
                        xtype: 'grid',
                        id: 'UserGroupsGrid',
                        height: 150,
                        columns: [
                            {
                                text: 'Assigned Groups',
                                dataIndex: 'groupName',
                                flex: '1'
                            }
                            ]
                    }
                ]
                }
        ];
    },
    buildButtons: function () {
        return ['->', {
            text: 'Close',
            action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});