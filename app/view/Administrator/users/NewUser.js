Ext.define('singer.view.Administrator.users.NewUser', {
    extend: 'Ext.window.Window',
    alias: 'widget.newuser',
    //    iconCls: 'icon-user',
    title: 'Add New User',
    //    store: 'Status',
    modal: true,
    layout: 'fit',
    constrain: true,
    width: 500,
    resizable: true,
    loadMask: true,
    //    maskOnDisable: false,
    //    height: '90%',
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            height: 300,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                //                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                //                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            //            buttons: newUserForm.buildButtons(),
            //            items: groupItems

        });


        var groupItems = Ext.create('Ext.container.Container', {
            width: 500,
            //            height: 300,
            bodyPadding: 5,
            overflowY: 'auto',
            //            defaults: {
            //                xtype: 'textfield'
            //            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                //                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                //                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formButtons = Ext.create('Ext.form.Panel', {
            //            width: 500,
            //            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                //                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                //                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.buildButtons(),
            //            buttons: newUserForm.buildButtons(),
            //            items: groupItems

        });
        //        newUserForm.items = [formPanel, groupItems];
        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                    //                    flex: 5
                },
                items: [
                    formPanel,
 //                    this.buildGroupItems()
                    groupItems,
                    formButtons
                ]
            }
        ];


        //        newUserForm.dockedItems = newUserForm.buildDockedItems()
        //        supplierForm.items = [vbox];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
        return Ext.create('Ext.Panel', {
            width: 500,
            //            height: 100,
            title: "User Groups",
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            //                renderTo: document.body,
            items: [{
                xtype: 'panel',
                //                        title: 'Inner Panel One',
                flex: 3,
                items: [
                        Ext.create('Ext.grid.Panel', {
                        height: 150,
                        id: 'allUserGroups',
                        //                            ddGroup: 'firstGridDDGroup',
                        //                            enableDragDrop: true,
                        //                            viewConfig: {
                        //                                plugins: {
                        //                                    ptype: 'gridviewdragdrop',
                        //                                    dragGroup: 'firstGridDDGroup',
                        //                                    dropGroup: 'secondGridDDGroup'
                        //                                }
                        //                            },
                        columns: [
                            {
                                text: 'Available Groups',
                                dataIndex: 'groupName',
                                flex: '1'
                            }
                            ],
                        features: [
                            {
                                ftype: 'filters',
                                autoReload: false, //don't reload automatically
                                local: true, //only filter locally
                                // filters may be configured through the plugin,
                                // or in the column definition within the headers configuration
                                filters: [
                                    {
                                        type: 'string',
                                        dataIndex: 'groupName'
                                }
                            ]
                        }
                    ]
                        //                                height: 200,
                        //                                width: 400,
                        //                                renderTo: Ext.getBody()
                    })
                    ]
                }, {
                xtype: 'panel',
                //                        title: 'Inner Panel One',
                flex: 1,
                //                    layout:'fit',
                height: 150,
                layout: {
                    type: 'vbox',
                    align: 'center',
                    pack: 'center'
                },
                //                    buttonAlign: 'center',
                items: [
                    {
                        xtype: 'button',
                        text: '>',
                        action: 'nextOne',
                        //                            height: '25%'
                        width: '80%',
                        margin: '5'
                        },
                    {
                        xtype: 'button',
                        text: '>>',
                        action: 'nextAll',
                        width: '80%',
                        margin: '5'
                        },
                    {
                        xtype: 'button',
                        text: '<<',
                        action: 'prevAll',
                        width: '80%',
                        margin: '5'
                        }, {
                        xtype: 'button',
                        text: '<',
                        action: 'prevOne',
                        width: '80%',
                        margin: '5'
                        },
 //                        
                    ]
                }, {
                xtype: 'panel',
                //                        title: 'Inner Panel Three',
                flex: 3,
                items: [
                        Ext.create('Ext.grid.Panel', {
                        id: 'UserGroupsGrid',
                        height: 150,
                        //                            ddGroup: 'secondGridDDGroup',
                        //                            enableDragDrop: true,
                        //                            viewConfig: {
                        //                                plugins: {
                        //                                    ptype: 'gridviewdragdrop',
                        //                                    dragGroup: 'firstGridDDGroup',
                        //                                    dropGroup: 'secondGridDDGroup'
                        //                                }
                        //                            },
                        columns: [
                            {
                                text: 'Assigned Groups',
                                dataIndex: 'groupName',
                                flex: '1'
                            }
                            ],
                        features: [
                            {
                                ftype: 'filters',
                                autoReload: false, //don't reload automatically
                                local: true, //only filter locally
                                // filters may be configured through the plugin,
                                // or in the column definition within the headers configuration
                                filters: [
                                    {
                                        type: 'string',
                                        dataIndex: 'groupName'
                                }
                            ]
                        }
                    ]
                    })
                    ]
                }]
        });
    },
    buildItems: function () {
        //
        //        var userstore = Ext.data.StoreMgr.lookup('Users').getAt(0).data.nic;
        //        //var group = userstore.data.items[6].data.userGroups;
        //
        //        ////console.log("Users Store ............", userstore); //userstore.data.items[6].data.userGroups);

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": '1',
                    "value": "Active"
                },
                {
                    "status": '9',
                    "value": "Inactive"
                }
            ]
        });
        return [
            {
                fieldLabel: 'User ID',
                name: 'userId',
                maxLength: 20,
                //                vtype: 'ValidUserID',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                // regex: /^[a-zA-Z0-9_]+$/
            }, {
                xtype: 'nicfield',
                fieldLabel: 'NIC',
                name: 'nic',
                maxLength: 10,
                minLength: 10,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                //                vtype: 'ValidUserID',
                //                regex: /^[VX0-9_]+$/,
                //                listeners: {
                //                    change: function (cmp, newValue, oldValue, eOpts) {
                //                        //                                //console.log(newValue.length);
                //                        var len = newValue.length;
                //                        if (len === 10) {
                //                            //                            Ext.getCmp('warrantyregform-submit').setDisabled(false);
                //                            var lastVal = newValue.charAt(9);
                //                            if (lastVal === 'v' ||
                //                                lastVal === 'V' ||
                //                                lastVal === 'x' ||
                //                                lastVal === 'X') {
                //                                if (lastVal === 'v') {
                //                                    var val = newValue.slice(0, 9) + 'V';
                //                                    cmp.setValue(val);
                //                                } else if (lastVal === 'x') {
                //                                    var val = newValue.slice(0, 9) + 'X';
                //                                    cmp.setValue(val);
                //                                }
                //                            } else {
                //                                var val = newValue.slice(0, 9) + 'V';
                //                                cmp.setValue(val);
                //                            }
                //                        } else if (len > 10) {
                //                            var val = newValue.substring(0, 10);
                //                            cmp.setValue(val);
                //                        }
                //                    }
                //                }
            }, {
                fieldLabel: 'First Name',
                name: 'firstName',
                maxLength: 20,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                //                vtype: 'ValidUserID',
                regex: /^[a-zA-Z 0-9_]+$/
            }, {
                fieldLabel: 'Last Name',
                name: 'lastName',
                maxLength: 20,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                //                vtype: 'ValidUserID',
                regex: /^[a-zA-Z 0-9_]+$/
            }, {
                fieldLabel: 'Common Name',
                name: 'commonName',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                maxLength: 60,
                //                vtype: 'ValidUserID',
                regex: /^[a-zA-Z_ 0-9]+$/
            }, {
                fieldLabel: 'Designation',
                name: 'designation',
                maxLength: 50,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                //                vtype: 'ValidUserID',
                regex: /^[a-zA-Z_ 0-9]+$/
            }, {
                fieldLabel: 'Telephone No',
                name: 'telephoneNumber',
                maxLength: 10,
                minLength: 10,
                //                vtype: 'int',
                allowBlank: false,
                regex: /^[0-9_]+$/,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
            }, {
                //                xtype: 'textfield',
                fieldLabel: 'Email',
                name: 'email',
                maxLength: 60,
                vtype: 'email',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                //                regex: /^[V0-9_]+$/
            },
            {
                //                xtype: 'textfield',
                fieldLabel: 'Retype Email',
                name: 'email2',
                maxLength: 60,
                vtype: 'email',
                allowBlank: false,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                //                regex: /^[V0-9_]+$/
            },
            {
                fieldLabel: 'Active Status',
                xtype: 'combobox',
                name: 'status',
                editable: false,
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        //                        ////console.log(me.getValue());
                        if (me.getValue() === null) {
                            me.setValue('1');
                        }
                    },
                    change: function (me, newValue, oldValue) {
                        if (oldValue !== undefined) {
                            if (oldValue !== newValue) {
                                Ext.getCmp('userview-remarks').show();
                                Ext.getCmp('userview-remarks').allowBlank = false;
                            }
                        }
                    }
                }
            },
            {
                xtype: 'textarea',
                name: 'remarks',
                fieldLabel: 'Remark',
                id: 'userview-remarks',
                hidden: true

            },
            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Reset Password',
                action: 'resetPass'
                //                id: 'userform-reset-insert'
            }
            // {
            //     fieldLabel: 'groups',
            //     name:'groups'
            // },
 //            {
 //                    fieldLabel: 'userID',
 //                name:'createdBy'
 //            }
 //           
        ];
    },
    buildButtons: function () {
        return [
            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
                //                id: 'userform-reset-insert'
            }, {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newuser-save',
                formBind: true
                //                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
                //                        ui: 'success',
                action: 'create',
                id: 'newuser-create',
                formBind: true,
            }
 //           
        ];
    },
    buildDockedItems: function () {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
                        //                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
                        //                id: 'userform-reset-insert'
                    }, {
                        //                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newuser-save',
                        formBind: true
                        //                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
                        //                        ui: 'success',
                        action: 'create',
                        id: 'newuser-create',
                        formBind: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function () {
        return [];
    }

});