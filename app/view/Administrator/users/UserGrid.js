/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.users.UserGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usergrid',
    loadMask: true,
    store: 'Users',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    //    forceFit: true,
    minHeight: 200,
    //    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var usersGrid = this;
        usersGrid.columns = usersGrid.buildColumns();

        //        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        usersGrid.dockedItems = usersGrid.buildDockItems(this);
        usersGrid.features = usersGrid.buildFeatures();
        usersGrid.callParent();
    },
    buildDockItems: function (userGrid) {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
                    xtype: 'pagingtoolbar',
                    store: 'Users',
                    dock: 'bottom',
                    displayInfo: true
                    }]
            },
            {
                xtype: 'form',
                //                xtype: 'form',
                //                bodyPadding: 2,
                frame: false,
                border: false,
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->',
//                    {
//                        xtype: 'combo',
//                        anyMatch: true,
//                        autoSelect: true,
//                        store: 'userIds',
//                        queryMode: 'local',
//                        displayField: 'userId',
//                        valueField: 'userId',
//                        action: 'search',
//                        id: 'usergrid-searchDrp'
//                        //                        listeners: {
//                        //                            scope: this,
//                        //                            change: function(me){
//                        ////                                //console.log('value changed');
//                        //                                this.getController('UserMaintenence').userSearch(me.getValue());
//                        //                            }
//                        //                        }
//                        //                        allowBlank: false
//                    }, '->',
                    {
                        text: 'Add New User',
                        //                        icon: 'resources/res/img.jpg',
                        action: 'open_window'
                    }]
            }];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {
                    "status": '1',
                    "value": "Active"
                },
                {
                    "status": '9',
                    "value": "Inactive"
                }
            ]
        });
        return [
            {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'User ID',
                dataIndex: 'userId',
                //                flex: 2,
            }, {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'First Name',
                dataIndex: 'firstName',
                //                flex: 2
            }, {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Last Name',
                dataIndex: 'lastName',
                //                flex: 2
            }, {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Common Name',
                dataIndex: 'commonName',
                //                flex: 2
            }, {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Designation',
                dataIndex: 'designation',
                //                flex: 2
            },
 //            {
 //                // xtype: 'searchablecolumn',
 //                // searchable: false,
 //                // sortable: false,
 //                menuDisabled: true,
 //                text: 'department',
 //                dataIndex: 'bisName',
 //                flex: 2
 //            },
            {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Tel.No',
                dataIndex: 'telephoneNumber',
                //                flex: 2
            }, {
                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Email',
                dataIndex: 'email',
                //                flex: 2
            }, {
//                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Reg.Date',
                dataIndex: 'createdOn',
                //                flex: 2
            }, {
//                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Inactive Date',
                dataIndex: 'inactivedOn',
                //                flex: 2
            }, {
//                xtype: 'searchablecolumn',
                // searchable: false,
                 sortable: false,
                menuDisabled: true,
                text: 'Status',
                dataIndex: 'status',
                //                flex: 2,
                renderer: function (value) {
                    if (value === '1') {
                        return "Active";
                    }
                    if (value === '9') {
                        return "Inactive";
                    }

                }
            }, {
                xtype: 'actioncolumn',
                width: 50,
                text: 'Edit',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/edit.png', // Use a URL in the icon config
                    tooltip: 'Edit',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = (grid.getStore().getAt(rowIndex));
                        this.up('grid').fireEvent('userEdit', grid, record);
                        //                            ////console.log(this.up('grid'));
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                text: 'View',
                menuDisabled: true,
                items: [{
                    icon: 'resources/images/view-icon.png',
                    tooltip: 'View',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        //                            alert("Terminate " + rec.get('first_name'));
                        this.up('grid').fireEvent('userView', grid, rec);
                    }
                    }]
            }

        ];
    },
    buildFeatures: function () {

    }

});