
Ext.define('singer.view.Administrator.defects.NewMinorDefect', {
    extend: 'Ext.window.Window',
    alias: 'widget.newminordefect',
    loadMask: true,
        modal: true,
//    iconCls: 'icon-user',
    title: 'Add New Defect Code',
//    store: ['Status', 'UserGroup'],
//    store:'UserGroups',
//    modal: true,
    layout: 'fit',
    constrain: true,
    width: 500,
//    height: 200,
    initComponent: function() {

        var newDefectForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newDefectForm.buildItems(),
            buttons: newDefectForm.buildButtons()
        });
//         var vbox = Ext.create('Ext.Panel', {
//            width: 500,
//            height: 600,
////            title: "VBoxLayout Panel",
//
//            layout: {
//                type: 'vbox',
//                align: 'center'
//            },
////            renderTo: document.body,
////            items: [{
////                    xtype: 'panel',
////                    title: 'Inner Panel One',
////                    width: 500,
////                    flex: 4
////                },
////                {
////                    xtype: 'panel',
////                    title: 'Inner Panel Two',
////                    width: 250,
////                    flex: 2
////                }]
//        });
//        var hbox = Ext.create('Ext.Panel', {
//            width: 500,
//            height: 600,
////            title: "VBoxLayout Panel",
//
//            layout: {
//                type: 'hbox',
//                align: 'center'
//            },
////            renderTo: document.body,
//            items: [{
//                    xtype: 'panel',
//                    title: 'Inner Panel One',
//                    width: 500,
//                    flex: 2
//                },
//                {
//                    xtype: 'panel',
//                    title: 'Inner Panel Two',
//                    width: 250,
//                    flex: 2
//                }]
//        });
//        vbox.items=[formPanel, hbox];
        newDefectForm.items = [formPanel];
//        supplierForm.items = [vbox];
        newDefectForm.callParent();
    },
    buildItems: function() {

//        var defectstore = Ext.data.StoreMgr.lookup('Defects').getAt(0).data.nic;
        //var group = userstore.data.items[6].data.userGroups;

//        ////console.log("Defects Store ............", defectstore); //userstore.data.items[6].data.userGroups);

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
            ]
        });
        return [
            {
              fieldLabel: 'Code',
                name: 'minDefectCode',
//                id:'code',
                maxLength: 10,
//                vtype: 'ValidUserID',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/,
                visible: false
            },
                {
                fieldLabel: 'Major Defect ID',
                name: 'mrjDefectCode',
                maxLength: 10,
//                visible: false,
            },

            {
                xtype:'capfield',
                fieldLabel: 'Description',
                name: 'minDefectDesc',
                maxLength: 100,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
               regex:/^[a-zA-Z0-9][a-zA-Z0-9 ]*[a-zA-Z0-9]$/,
                listeners: {
                    afterrender: function(field) {
                      field.focus();
                    }
                  }
//                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
            }, {
                fieldLabel: 'Status',
                xtype: 'combobox',
//                maxLength: 2,
                name: 'minDefectStatus',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                editable: false,
                valueField: 'status',
                 listeners: {
                    scope: this,
                    afterRender: function(me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }
                }
            }         
//            {
//                
//                fieldLabel: 'Active Status',
//                xtype: 'combobox',
//                fields:
//                        ['index', 'value']
//                , data: [
//                    {index: '1', value: 'Active'},
//                    {index: '0', value: 'Inactive'}
//                ]
//                , valueField: 'index'
//                , displayField: 'value'
//                , mode: 'local'
//            },
//            {
//                id: 0
//                , fields:
//                        [
//                            'myId', //numeric value is the key
//                            'myText' //the text value is the value
//                        ]
//                , data:yourData
//                , valueField: 'myId'
//                , displayField: 'myText'
//                , mode: 'local'
//            },
//            ,
//            {
//                fieldLabel: 'Name',
//                name: 'name',
//                maxLength: 59
//            }, {
//                fieldLabel: 'Designation',
//                name: 'designation',
//                maxLength: 59
//            }, {
//                fieldLabel: 'Password',
//                name: 'password1',
//                inputType: 'password',
//                minLength: 8,
//                maxLength: 19,
//                vtype: 'ValidPassword'
//            }, {
//                fieldLabel: 'Repeat Password',
//                name: 'password',
//                inputType: 'password',
//                minLength: 8,
//                maxLength: 19,
//                validator: function(value) {
//                    var password1 = this.previousSibling('[name=password1]');
//                    return (value === password1.getValue()) ? true : 'Passwords not match.';
//                }
//            }, {
//                xtype: 'hiddenfield',
//                name: 'status',
//                value: 1
//            }
        ];
    },
    buildButtons: function() {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newminordefect-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newminordefect-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});

