/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.defects.MajorDefectView', {
    extend: 'Ext.window.Window',
    alias: 'widget.majordefectview',
//    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Major Defect View',
    modal: true,
    constrain: true,
    loadMask: true,
    layout: 'fit',
    width: 430,
//    height: 240,
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {
        return [
            {
                fieldLabel: 'Major Code',
                name: 'mjrDefectCode'
            },
            {
                fieldLabel: 'Description',
                name: 'mjrDefectDesc'
            },
            {
                fieldLabel: 'Status',
                name: 'mjrDefectStatus',
                renderer: function(value) {
                    if (value === '1') {
                        return "Active";
                    }
                    if (value === '2') {
                        return "Inactive";
                    }

                }
            }
        ];
    },
    buildButtons: function() {
        return ['->', {
                text: 'Close',
                action: 'cancel'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});

