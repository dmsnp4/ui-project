/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.defects.MajorDefectGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.majordefectgrid',
    loadMask: true,
    store: 'Defects',
    Title: 'Major Codes',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    forceFit: true,
    minHeight: 200,
//    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function() {
        var majordefectsGrid = this;
        majordefectsGrid.columns = majordefectsGrid.buildColumns();
//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        majordefectsGrid.dockedItems = majordefectsGrid.buildDockItems(this);
        majordefectsGrid.features = majordefectsGrid.buildFeatures();
        majordefectsGrid.callParent();
    },
    buildDockItems: function(majordefectGrid) {
        return [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [
                    {
                        xtype: 'pagingtoolbar',
                        store: 'Defects',
                        dock: 'bottom',
                        displayInfo: true
                       }]
            }, {xtype: 'form',
//                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action:'clearSearch',
//                        handler: function() {
//                            majordefectGrid.filters.clearFilters();},
                        minWidth: 100,
                        minHeight: 10
                    }, '->', 
                    {
                        iconCls: 'icon-search',
                        text: 'Add New Major Code',
                        action: 'open_window',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }
//            {
//                xtype: 'toolbar',
//                Title: 'Major Codes',
//                dock: 'top',
//                items: [{
//                        xtype: 'button',
//                        text: 'Clear Search',
//                        scale: 'large',
////                        icon: 'resources/res/img.jpg',
//                        minWidth: 100,
//                        minHeight: 50,
//                        handler: function() {
//                            majordefectGrid.filters.clearFilters();
//                        }
//
//                    }, '->', {
//                        xtype: 'button',
//                        text: 'Add New Major Code',
//                        scale: 'large',
////                        icon: 'resources/res/img.jpg',
//                        action: 'open_window',
//                        minWidth: 100,
//                        minHeight: 50
//
//                    }]
//            }
        ];
    },
    buildColumns: function() {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": "1", "value": "Active"},
                {"name": "2", "value": "Inactive"}
            ]
        });
        return [
//            {text: 'User ID', dataIndex: 'USER_ID'},
//            {text: 'First Name', dataIndex: 'FIRST_NAME', flex: 1},
//            {text: 'Last Name', dataIndex: 'LAST_NAME'},
//            {text: 'Common Name', dataIndex: 'COMMON_NAME'},
//            {text: 'Designation', dataIndex: 'DESIGNATION'},
//            {text: 'Department', dataIndex: 'BIS_ID'},
//            {text: 'Tel.No', dataIndex: 'TELEPHONE_NO'},
//            {text: 'Email', dataIndex: 'EMAIL'},
//            {text: 'Reg.Date', dataIndex: 'REGISTERED_DATE'},
//            {text: 'Inactive Date', dataIndex: 'INACTIVE_DATE'},
//            {text: 'Status', dataIndex: 'STATUS'},

            {
                xtype: 'searchablecolumn',
                text: 'Code',
                dataIndex: 'mjrDefectCode',
                flex: 1
            }, {
                xtype: 'searchablecolumn',
                text: 'Description',
                dataIndex: 'mjrDefectDesc',
                flex: 1
            }, {
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'mjrDefectStatus',
                renderer: function(value) {
                    if (value === 1) {
                        return "Active";
                    }
                    if (value === 2) {
                        return "Inactive";
                    }

                }
            }, {
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'Add Minor Defects',
                width: 150,
                items: [{
                        icon: 'resources/images/unknown.png', // Use a URL in the icon config
                        tooltip: 'Add Minor Defects',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('addMinor', grid, record);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'Edit',
                width: 70,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                           this.up('grid').fireEvent('defectEdit', grid, rec);
                        }
                    }]
            }

        ];
    },
    buildFeatures: function() {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'mjrDefectCode',
                    type: 'numeric'
//                    width: 50
                }, {
                    type: 'string',
                    dataIndex: 'mjrDefectDesc'
                }, {
                    dataIndex: 'mjrDefectStatus',
                    type: 'string'
                }
            ]
        };
        return [filtersCfg];
    }

});

