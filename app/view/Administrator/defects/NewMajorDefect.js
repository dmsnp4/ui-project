
Ext.define('singer.view.Administrator.defects.NewMajorDefect', {
    extend: 'Ext.window.Window',
    alias: 'widget.newmajordefect',
    loadMask: true,
        modal: true,
//    iconCls: 'icon-user',
    title: 'Add New Defect Code',
//    store: 'Status',
//    modal: true,
    layout: 'fit',
    constrain: true,
    width: 500,
//    height: 200,
    initComponent: function() {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 300,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()

        });
//        
//        newUserForm.items = [
//            {
//                xtype: 'container',
//                layout: {
//                    type: 'vbox',
//                    align: 'stretch'
//                },
//                defaults: {
//                    flex: 5
//                },
//                items: [
//                    formPanel,
//                    this.buildGroupItems()
//                ]
//            }
//        ];
//        newUserForm.dockedItems = newUserForm.buildDockedItems();
//        supplierForm.items = [vbox];
           newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function() {
//        return Ext.create('Ext.Panel', {
//            width: 400,
////                height: 300,
//           // title: "User Groups",
////            layout: {
////                type: 'hbox',
////                align: 'stretch'
////            },
////                renderTo: document.body,
////            items: [{
////                    xtype: 'panel',
////                        title: 'Inner Panel One',
//                    flex: 2,
//                    items: [
////                        Ext.create('Ext.grid.Panel', {
//////                                title: 'Simpsons',
//////                            store: 'UserGroups',
////                            columns: [
////                                {text: 'Available Groups', dataIndex: 'groupName'}
////                            ]
//////                                height: 200,
//////                                width: 400,
//////                                renderTo: Ext.getBody()
////                        })
////                    ]
////                },
////                {
////                    xtype: 'panel',
//////                        title: 'Inner Panel One',
////                    flex: 2,
////                    layout: 'anchor',
////                    items: [
////                        Ext.create('Ext.Button', {
////                            text: '>',
////                            renderTo: Ext.getBody(),
////                            handler: function() {
////                                alert('You clicked the button!');
////                            }
////                        }),
////                        Ext.create('Ext.Button', {
////                            text: '>>',
////                            renderTo: Ext.getBody(),
////                            handler: function() {
////                                alert('You clicked the button!');
////                            }
////                        })
////                    ]
////                }, 
////                {
////                    xtype: 'panel',
//////                        title: 'Inner Panel Three',
////                    flex: 2,
////                    items: [
////                        Ext.create('Ext.grid.Panel', {
////                            id: 'UserGroupsGrid',
////                            columns: [
////                                {text: 'Assigned Groups', dataIndex: 'groupName'}
////                            ]
////                        })
////                    ]
////                }
//                    ]
//        });
    },
    buildItems: function() {
//
//        var userstore = Ext.data.StoreMgr.lookup('Users').getAt(0).data.nic;
//        //var group = userstore.data.items[6].data.userGroups;
//
//        ////console.log("Users Store ............", userstore); //userstore.data.items[6].data.userGroups);

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
            ]
        });
        return [
//            {
//                fieldLabel: 'Code',
//                name: 'mjrDefectCode',
//                maxLength: 10,
////                vtype: 'ValidUserID',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                 regex: /^[1-9][0-9]*$/,
//                 stripCharsRe: /(^\s+|\s+$)/g,
//            },
//             {
//                fieldLabel: 'NIC',
//                name: 'nic',
//                maxLength: 10,
//                minLength: 10,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
////                vtype: 'ValidUserID',
//                regex: /^[V0-9_]+$/
//            }, 
            {
                xtype:'capfield',
                fieldLabel: 'Description',
                name: 'mjrDefectDesc',
                maxLength: 100,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                regex:/^[a-zA-Z0-9][a-zA-Z0-9 ]*[a-zA-Z0-9]$/,
//                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
            },
//            {
//                fieldLabel: 'Last Name',
//                name: 'lname',
//                maxLength: 10,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
////                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
//            }, {
//                fieldLabel: 'Common Name',
//                name: 'comName',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                maxLength: 10,
////                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
//            }, {
//                fieldLabel: 'Designation',
//                name: 'designation',
//                maxLength: 10,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
////                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
//            }, {
//                fieldLabel: 'Telephone No',
//                name: 'telePhone',
//                maxLength: 10,
//                minLength: 10,
////                vtype: 'int',
//                allowBlank: true,
//                regex: /^[0-9_]+$/
//            }, {
////                xtype: 'textfield',
//                fieldLabel: 'Email',
//                name: 'email',
////                maxLength: 10,
//                vtype: 'email',
//                allowBlank: true
////                regex: /^[V0-9_]+$/
//            },
            {
                fieldLabel: 'Status',
                xtype: 'combobox',
                name: 'mjrDefectStatus',
//                maxLength: 2,
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                editable: false,
                valueField: 'status',
                 listeners: {
                    scope: this,
                    afterRender: function(me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }
                }
            }
//           
        ];
    },
    buildButtons: function() {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newmajordefect-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newmajordefect-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
//                id: 'userform-reset-insert'
                    }, {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newmajordefect-save',
                        formBind: true
//                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
//                        ui: 'success',
                        action: 'create',
                        id: 'newmajordefect-create',
                        formBind: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function() {
        return [];
    }

});
