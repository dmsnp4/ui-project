Ext.create('Ext.data.Store', {
    storeId: 'simpsonsStore',
    fields: ['name', 'email', 'phone'],
    data: {'items': [
            {'name': 'Lisa', "email": "lisa@simpsons.com", "phone": "555-111-1224","value":35},
            {'name': 'Bart', "email": "bart@simpsons.com", "phone": "555-222-1234","value":72},
            {'name': 'Homer', "email": "home@simpsons.com", "phone": "555-222-1244","value":53},
            {'name': 'Marge', "email": "marge@simpsons.com", "phone": "555-222-1254","value":18}
        ]},
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});




Ext.define('singer.view.Administrator.userGroups.GroupUsersGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.groupusersgrid',
//    title: 'Assing Users',
    height: 300,
    width: 700,
    layout: 'fit',
    initComponent: function() {
        var me = this;
        var tempStore = Ext.create('Ext.data.Store', {
            storeId: 'simpsonsStore',
            fields: ['name', 'email', 'phone'],
            data: {'items': [
                    {'name': 'Lisa', "email": "lisa@simpsons.com", "phone": "555-111-1224", "value":35},
                    {'name': 'Bart', "email": "bart@simpsons.com", "phone": "555-222-1234", "value":72},
                    {'name': 'Homer', "email": "home@simpsons.com", "phone": "555-222-1244", "value":53},
                    {'name': 'Marge', "email": "marge@simpsons.com", "phone": "555-222-1254", "value":18}
                ]},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            }
        });
        me.store=tempStore;
        me.columns=me.buildColumns();
//        var grid = Ext.create('Ext.grid.Panel', {
//            store: tempStore,
//            id: 'groupusersgrid',
//            columns: me.buildColumns(),
//            renderTo: Ext.getBody()
//
//        });

//        me.items = [grid];
        me.callParent();
    },
    buildColumns: function() {
//        var status = Ext.create('Ext.data.Store', {
//            fields: ['name', 'value'],
//            data: [
//                {"name": 1, "value": "Active"},
//                {"name": 2, "value": "Inactive"}
//            ]
//        });
        return [
            {text: 'Group ID', dataIndex: 'name'},
            {text: 'First Name', dataIndex: 'name'},
            {text: 'Last Name', dataIndex: 'email'},
            {text: 'Designation', dataIndex: 'name'},
            {text: 'Inactive Date', dataIndex: 'name'},
            {
                xtype: 'actioncolumn',
                width: 50,
//                        
                items: [{
                        icon: 'resources/res/view.png', // Use a URL in the icon config  
                        tooltip: 'View',
                        width: 50,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('viewUser', grid, rec);
//                            ////console.log('clicked', this.up('grid'));
//                            this.getController('UserGroupController').viewUser(grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                items: [{
                        icon: 'resources/res/remove_user.png',
                        tooltip: 'Unassign User',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('unAssign', grid, rec);

                        }
                    }]
            }
//            {
//                text: 'Status',
//                dataIndex: 'status',
//                renderer: function(value) {
//                    if (value === 1) {
//                        return "Active";
//                    }
//                    if (value === 2) {
//                        return "Inactive";
//                    }
//
//                }
//            }
//            {text: 'User ', dataIndex: 'USER_ID'},
//            {text: 'First Name', dataIndex: 'FIRST_NAME', flex: 1},
//            {text: 'Last Name', dataIndex: 'LAST_NAME'},
//            {text: 'Common Name', dataIndex: 'COMMON_NAME'},
//            {text: 'Designation', dataIndex: 'DESIGNATION'},
//            {text: 'Department', dataIndex: 'BIS_ID'},
//            {text: 'Tel.No', dataIndex: 'TELEPHONE_NO'},
//            {text: 'Email', dataIndex: 'EMAIL'},
//            {text: 'Reg.Date', dataIndex: 'REGISTERED_DATE'},
//            {text: 'Inactive Date', dataIndex: 'INACTIVE_DATE'},
//            {text: 'Status', dataIndex: 'STATUS'},

//            {
//                text: 'Group ID',
//                dataIndex: 'groupId'
//            }, {
//                text: 'Group Name',
//                dataIndex: 'groupName'
//            }, {
//                text: 'Status',
//                dataIndex: 'status',
//                renderer: function(value) {
//                    if (value === 1) {
//                        return "Active";
//                    }
//                    if (value === 2) {
//                        return "Inactive";
//                    }
//
//                }
//            }, {
//                xtype: 'actioncolumn',
//                width: 50,
////                        
//                items: [{
//                        icon: 'resources/res/add_user.png', // Use a URL in the icon config  
//                        tooltip: 'Assign User',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('userView', grid, rec);
//                        }
//                    }]
//            },
//            {
//                xtype: 'actioncolumn',
//                width: 50,
//                items: [{
//                        icon: 'resources/res/edit.jpg',
//                        tooltip: 'Edit',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('userView', grid, rec);
//                        }
//                    }]
//            }
//            {
//                xtype: 'actioncolumn',
//                width: 50,
//                items: [{
//                        icon: 'resources/res/remove_user.png',
//                        tooltip: 'Unassign User',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('userView', grid, rec);
//                        }
//                    }]
//            }

        ];
    }


});


