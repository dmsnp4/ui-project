/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Administrator.userGroups.UserGroup', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usergroup',
    loadMask: true,
    modal: true,
    store: 'UserGroups',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    forceFit: true,
    minHeight: 200,
//    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function() {
        var thsiGrid = this;
        thsiGrid.columns = thsiGrid.buildColumns();
//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        thsiGrid.dockedItems = thsiGrid.buildDockItems(this);
        thsiGrid.features = thsiGrid.buildFeatures();
        thsiGrid.callParent();
    },
    buildDockItems: function(grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        handler: function() {
                            grid.filters.clearFilters();
                        }

                    }, '->', {
                        text: 'Add New Group ',
                        action: 'open_window'
                    }]
            }];
    },
    buildColumns: function() {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": 1, "value": "Active"},
                {"name": 2, "value": "Inactive"}
            ]
        });
        return [
//            {text: 'User ID', dataIndex: 'USER_ID'},
//            {text: 'First Name', dataIndex: 'FIRST_NAME', flex: 1},
//            {text: 'Last Name', dataIndex: 'LAST_NAME'},
//            {text: 'Common Name', dataIndex: 'COMMON_NAME'},
//            {text: 'Designation', dataIndex: 'DESIGNATION'},
//            {text: 'Department', dataIndex: 'BIS_ID'},
//            {text: 'Tel.No', dataIndex: 'TELEPHONE_NO'},
//            {text: 'Email', dataIndex: 'EMAIL'},
//            {text: 'Reg.Date', dataIndex: 'REGISTERED_DATE'},
//            {text: 'Inactive Date', dataIndex: 'INACTIVE_DATE'},
//            {text: 'Status', dataIndex: 'STATUS'},

            {
                text: 'Group ID',
                dataIndex: 'groupId'
            }, {
                text: 'Group Name',
                dataIndex: 'groupName'
            }, {
                text: 'Status',
                dataIndex: 'status',
                renderer: function(value) {
                    if (value === 1) {
                        return "Active";
                    }
                    if (value === 2) {
                        return "Inactive";
                    }

                }
            }, {
                xtype: 'actioncolumn',
                width: 50,
//                        
                items: [{
                        icon: 'resources/images/add_user.png', // Use a URL in the icon config  
                        tooltip: 'Assign User',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('assignUsers', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 50,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit User Group',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('editGroup', grid, rec);
                        }
                    }]
            }
//            {
//                xtype: 'actioncolumn',
//                width: 50,
//                items: [{
//                        icon: 'resources/res/remove_user.png',
//                        tooltip: 'Unassign User',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('userView', grid, rec);
//                        }
//                    }]
//            }

        ];
    },
    buildFeatures: function() {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'groupId',
                    type: 'numeric'

                }, {
                    type: 'string',
                    dataIndex: 'groupName'
                }
            ]
        };
        return [filtersCfg];
    }

});

