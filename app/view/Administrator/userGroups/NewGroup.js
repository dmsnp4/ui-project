Ext.define('singer.view.Administrator.userGroups.NewGroup', {
    extend: 'Ext.window.Window',
    alias: 'widget.newgroup',
    title: 'New User Group',
    layout: 'fit',
    constrain: true,
    modal: true,
    width: 500,
//    height: 600,
    initComponent: function() {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function() {

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 1, "value": "Active"},
                {"status": 2, "value": "Inactive"}
            ]
        });
        return [
            {
                fieldLabel: 'Group ID',
                name: 'groupId',
//                maxLength: 19,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                regex: /^[a-zA-Z0-9_]+$/
            }, {
                fieldLabel: 'Group Name',
                name: 'groupName',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
            },
            {
                fieldLabel: 'Active Status',
                xtype: 'combobox',
                name: 'status',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                listeners: {
                    scope: this,
                    beforeRender: function(me) {
                        var val = me.getValue();
                        var cmp = Ext.getCmp('remark');
                        if (val === null) {
                            me.setValue(1);
                            cmp.setValue('empty');
                        }
                    },
                    afterRender: function(me) {
//                        ////console.log(this.next());
                        var val = me.getValue();
                        var cmp = Ext.getCmp('remark');
                        if (val === null) {
                            me.setValue(1);
                            cmp.setValue('empty');
                        } else if (val === 1) {
//                            cmp.setVisible(false);
                            cmp.setValue('empty');
                        } else {
                            cmp.setVisible(true);
//                            cmp.setValue('');
                        }
                    },
                    change: function(me) {
                        var cmp = Ext.getCmp('remark');
                        var val = me.getValue();
                        if (val === 1) {
                            cmp.setVisible(false);
                            if (cmp.getValue() === '')
                                cmp.setValue('empty');

                        } else {
                            cmp.setVisible(true);
                            if (cmp.getValue() === 'empty')
                                cmp.setValue('');
                        }
                    }
                }
            }, {
                fieldLabel: 'Remark',
                name: 'remarks',
                id: 'remark',
                allowBlank: false,
                visible: false
                
            },
            //user groups
            Ext.create('Ext.Panel', {
                width: 400,
//                height: 300,
                title: "Functions",
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
//                renderTo: document.body,
                items: [{
                        xtype: 'panel',
//                        title: 'Inner Panel One',
                        flex: 2,
                        items: [
                            Ext.create('Ext.grid.Panel', {
//                                title: 'Simpsons',
                                store: Ext.data.StoreMgr.lookup(''),
                                columns: [
                                    {text: 'Available Functions', dataIndex: 'groupName'}
                                ]
//                                height: 200,
//                                width: 400,
//                                renderTo: Ext.getBody()
                            })
                        ]
                    }, {
                        xtype: 'panel',
//                        title: 'Inner Panel One',
                        flex: 2,
                        layout: 'anchor',
                        items: [
                            Ext.create('Ext.Button', {
                                text: '>',
                                renderTo: Ext.getBody(),
                                handler: function() {
                                    alert('You clicked the button!');
                                }
                            }),
                            Ext.create('Ext.Button', {
                                text: '>>',
                                renderTo: Ext.getBody(),
                                handler: function() {
                                    alert('You clicked the button!');
                                }
                            })
                        ]
                    }, {
                        xtype: 'panel',
//                        title: 'Inner Panel Three',
                        flex: 2,
                        items: [
                            Ext.create('Ext.grid.Panel', {
                                columns: [
                                    {text: 'Assigned Functions', dataIndex: 'groupName'}
                                ]
                            })
                        ]
                    }]
            })

        ];
    },
    buildButtons: function() {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newgroup-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newgroup-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});
