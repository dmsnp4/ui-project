/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



Ext.define('singer.view.Administrator.UploadImeis.UploadImeisForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.uploadImeisForm',
    loadMask: true,
    modal: true,
    title: 'Upload Imeis',
    layout: 'fit',
    constrain: true,
    width: 650,
    initComponent: function() {

        var newDefectForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            height:120,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newDefectForm.buildItems(),
            buttons: newDefectForm.buildButtons()
        });

        newDefectForm.items = [formPanel];
        newDefectForm.callParent();
    },
    buildItems: function() {

//        var stat = Ext.create('Ext.data.Store', {
//            fields: ['status', 'value'],
//            data: [
//                {
//                    "status": 1,
//                    "value": "Active"
//                },
//                {
//                    "status": 2,
//                    "value": "Inactive"
//                }
//            ]
//        });
//        var statOfline = Ext.create('Ext.data.Store', {
//            fields: ['status', 'value'],
//            id : 'storeOfflineAllow',
//            data: [
//                {
//                    "status": 1,
//                    "value": "Allow"
//                },
//                {
//                    "status": 2,
//                    "value": "Deny"
//                }
//            ]
//        });
//        var yesno = Ext.create('Ext.data.Store', {
//            fields: ['status', 'value'],
//            data: [
//                {
//                    "status": 1,
//                    "value": "Yes"
//                },
//                {
//                    "status": 2,
//                    "value": "No"
//                }
//            ]
//        });

        return [
//            {
//                xtype: 'panel',
//                layout: 'hbox',
//                style: 'border: none;',
//                items: [
//                    {
//                        fieldLabel: 'DSR',
//                        xtype: 'combobox',
//                        emptyText: 'Select DSR',
//                        name: 'dSRId',
//                        width: 450,
//                        store: 'DSRListStore',
//                        id: 'dsr',
//                        queryMode: 'remote',
//                        displayField: 'bisName',
//                        valueField: 'bisId',
////                visible: true,
//                        editable: false,
//                        listeners: {
//                            change: function(cmp, newValue, oldValue) {
//                                console.log('My Val - change:::: ', cmp.getValue());
////                      Ext.getStore('radioStr').load();
//                                    
//                                    var bisId = 
//                                
//                                Ext.getBody().mask('Please wait...', 'loading');
//                                Ext.Ajax.request({
//                                    url: singer.AppParams.JBOSS_PATH + 'DsrAllowOffline/getDsrAllowOfflineStatus',
//                                    method: "GET",
//                                    headers: {
//                                        'Content-Type': 'application/json'
//                                    },
//                                    params : {bisId : cmp.getValue()},
//                                    timeout: 6000,
//                                    async: true,
//                                    // isUpload: true,
//                                    // headers: {'Content-Type': 'multipart/form-data'},
////            jsonData: {
////                //excelBase64: excel.getValue()
////            },
//                                    success: function(response) {
//
//                                        var responseData = Ext.decode(response.responseText);
//
//                                        if (responseData.returnFlag === '9') {
//                                            
//                                            Ext.Msg.alert("Error", responseData.returnMsg);
//                                            
//                                        } else {
//                                            
//                                            var getStatus = responseData.returnFlag;
//                                            console.log("status ",getStatus);
//                                            Ext.getCmp("allowOffline").setValue(getStatus);
//                                            
//                                        }
//
//
//                                        Ext.getBody().unmask();
//
//
//
//                                    },
//                                    failure: function(response, options) {
//                                        Ext.getBody().unmask();
//                                        console.log("Fail to fetch");
//                                    }
//                                }
//
//                                );
//
//
//
//
//
//                            },
//                            select: function(cmp, newValue, oldValue) {
//                                console.log("New ", newValue);
//
//                                var recordsOfRow = cmp.findRecordByValue(newValue);
//                                var index = newValue[0]['index'];
//                                console.log("index", newValue);
//                                //var store = Ext.getStore("radioStr");
////                           console.log('store', store.findRecord('bisName'));
////                        var bisName = store.data.items[0].data['bisName'];
//                                var bisName = newValue[0].data['bisName'];
//                                var teleNumber = newValue[0].data['teleNumber'];
//                                console.log("bis name", bisName);
//
//                                Ext.getCmp('addrs').setValue(bisName);
//                                Ext.getCmp('contact_no').setValue(teleNumber);
//                                console.log('My Val:::: ', cmp.getValue());
//                            },
//                            load: function(store, records, success) {
//                                console.log(records);
//                            }
//                        }
////                allowBlank: false
//                    },
//                    {
//                        xtype: 'label',
//                        flex: 1
//                    }
//                ]
//            },
//            {
//                xtype: 'label',
//                flex: 1,
//                id: 'current-bis-loc'
//            },
//            //items
//            {
//                xtype: 'displayfield',
//                fieldLabel: 'DSR Name',
//                // name: "serviceCenterAddress",
//                afterLabelTextTpl: "",
//                allowBlank: false,
//                id: 'addrs'
//            },
            {
                xtype: 'filefield',
//                                    xtype: 'fileuploadfield',
                anchor: '100%',
                id: 'image_path_selector2',
                emptyText: 'Select File',
                name: 'fileData',
                fieldLabel: 'Select File',
                allowBlank: false,
                forceSelection: true,
                listeners:
                        {
                            afterrender: function(field)
                            {
                                document.getElementById('image_path_selector2').addEventListener('change', function(event)
                                {
                                    var file = event.target.files[0];
                                    var reader = new FileReader();
                                    reader.onloadend = function(evt)
                                    {
                                        var textNode = evt.target.result;
                                        Ext.getCmp('image_path_srv2').setValue(textNode);
                                    };
                                    reader.readAsDataURL(file);

                                },
                                        false
                                        );
                            },
                            render: function(fileField, eOpts)
                            {
                                var imageLength = Ext.getCmp('image_path_srv2').getValue().length;
                                if (imageLength > 10) {
                                    fileField.setRawValue('img.xlsx');
                                }
                            }
                        }
            },
            {
                id: 'image_path_srv2',
                xtype: 'hiddenfield'
            }
//            {
//                xtype: "textfield",
//                id: 'contact_no',
//                name: 'workTelephoneNo',
//                fieldLabel: 'Contact No',
//                regex: /^[0-9_]+$/,
//                //action: 'clickRdio',
//                readOnly: true,
//                //allowBlank: true,
//                maxLength: 10
//            },
//            {
//                xtype: 'textfield',
//                fieldLabel: 'Contact No:',
//                //margin: '10 10 10 10',
//                name: '',
//                id: 'contact_no'
//
//            },
//            {
//                fieldLabel: 'Enable Offline',
//                id: 'allowOffline',
//                name: 'statusOfline',
//                xtype: 'combobox',
//                // store: statOfline,
//                queryMode: 'local',
//                editable: false,
//                displayField: 'value',
//                valueField: 'status',
//                allowBlank: false,
//                listeners: {
//                    scope: this,
//                    change: function(me) {
//                        if (me.getValue() == 1) {
//                            console.log("stats 1");
//                            Ext.getCmp("allowOffline").setValue(1);
//                        } else {
//                            console.log("stats 2");
//                            Ext.getCmp("allowOffline").setValue(2);
//                        }
//                    }
//                }
//            },
            //End items



//            {
//                xtype: 'combo',
//                fieldLabel: 'Distributor/Shop',
//                id: 'select-swap-bis-id',
//                store: 'distributorstr',
//                displayField: 'bisName',
//                valueField: 'bisId',
//                queryMode: 'remote',
//                hidden: true
//            },

//            {
//                xtype: 'hiddenfield',
//                id: 'hdn-bis-id',
//                value: ''
//            }
        ];

    },
    buildButtons: function() {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, 
            {
                xtype: 'button',
                text: 'Save',
                action: 'uploadsave',
                id: 'imei-swap-save',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Warranty Registration',
                action: 'uploadreg',
               // id: 'imei-swap-reg',
                //formBind: true
            },
//            {
//                                            text: 'CreateExcel',
//                                            action: 'saveToExcelExImeis'
//                                                    //id: 'printpaybtn'
//            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});



