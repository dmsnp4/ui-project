


Ext.define('singer.view.Administrator.InventoryUploadFunction.AddNewImeis', {
    extend: 'Ext.container.Container',
    alias: 'widget.addimeis',
    layout: {
        type: 'hbox',
    },
    overflowY: 'auto',
    initComponent: function () {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.WorkOrderOpenContainer(),
            this.maintainController(),
            this.WOclosecontaner(),
        ];
        inquiryCmp.callParent();
    },
    WorkOrderOpenContainer: function () {
        return Ext.create('Ext.panel.Panel',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    flex: 1,
                    items: [
                        {
                            title: 'Upload IMEIs',
                            xtype: 'form',
                            padding: '0',
                            autoScroll: true,
                            overflowY: 'auto',
                            border: 0,
                            items: [
//                                {
//                                    xtype: 'textfield',
//                                    fieldLabel: 'Bis Id:',
//                                    margin: '10 10 10 10',
//                                    name: '',
//                                    id: 'bis_id'
//                                },
                                {
                                    xtype: 'filefield',
//                                    xtype: 'fileuploadfield',
                                    anchor: '100%',
                                    id: 'image_path_selector1',
                                    emptyText: 'Select File',
                                    name: 'fileData',
                                    fieldLabel: 'Select File',
                                    allowBlank: false,
                                    forceSelection: true,
                                    listeners:
                                        {
                                            afterrender: function (field)
                                            {
                                                document.getElementById('image_path_selector1').addEventListener('change', function (event)
                                                {
                                                    var file = event.target.files[0];
                                                    var reader = new FileReader();
                                                    reader.onloadend = function (evt)
                                                    {
                                                        var textNode = evt.target.result;
                                                        Ext.getCmp('image_path_srv1').setValue(textNode);
                                                    };
                                                    reader.readAsDataURL(file);

                                                },
                                                        false
                                                        );
                                            },
                                            render: function (fileField, eOpts)
                                            {
                                                var imageLength = Ext.getCmp('image_path_srv1').getValue().length;
                                                if (imageLength > 10) {
                                                    fileField.setRawValue('img.xlsx');
                                                }
                                            }
                                        }
                                },
                                {
                                    id: 'image_path_srv1',
                                    xtype: 'hiddenfield'
                                },
//                                {
//                                xtype: 'fileuploadfield',
//                                fieldLabel: 'Select Attachment (xlsx formats only)',
//                                regex: /\.(xlsx)$/i,
//                                id: 'image_path_selector1',
//                                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                                allowBlank: false,
//                                listeners:
//                                        {
//                                            afterrender: function (field)
//                                            {
//                                                document.getElementById('image_path_selector1').addEventListener('change', function (event)
//                                                {
//                                                    var file = event.target.files[0];
//                                                    var reader = new FileReader();
//                                                    reader.onloadend = function (evt)
//                                                    {
//                                                        var textNode = evt.target.result;
//                                                        Ext.getCmp('image_path_srv1').setValue(textNode);
//                                                    };
//                                                    reader.readAsDataURL(file);
//
//                                                },
//                                                        false
//                                                        );
//                                            },
//                                            render: function (fileField, eOpts)
//                                            {
//                                                var imageLength = Ext.getCmp('image_path_srv1').getValue().length;
//                                                if (imageLength > 10) {
//                                                    fileField.setRawValue('C:\\fakepath\\img.jpg');
//                                                }
//                                            }
//                                        }
//                            },
//                                {
//                                    xtype: 'button',
//                                    text: 'save',
//                                    action: 'saveExcelData'
//                                },
//                                {
//                                    xtype: 'button',
//                                    text: 'CreateExcel',
//                                    action: 'saveToExcel'
//                                }
{
                                    xtype: 'panel',
                                    dock: 'top',
                                    buttons: [
                                        {
                                            text: 'Delete',
                                            margin: '10 10 10 10',
                                            action: 'deleteExcelData',
                                            loadMask: true
                                        },
                                        {
                                            text: 'Save',
                                            margin: '10 10 10 10',
                                            action: 'saveExcelData',
                                            loadMask: true
                                        },
                                        '->',
                                        {
                                            text: 'CreateExcel',
                                            action: 'saveToExcel'
                                                    //id: 'printpaybtn'
                                        }
                                        
                                        
                                    ]
                                }
                            ]
                        }
                    ]

                });
    },
    maintainController: function () {
        
        return Ext.create('Ext.panel.Panel',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start',
                    },
                    flex: 1,
                    items: [
                        {
                            title: 'Update Status',
                            xtype: 'form',
                            id: 'dndetail',
                            padding: '0',
                            overflowY: 'auto',
                            border: 0,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'vbox',
                                    defaults: {
                                        xtype: 'textfield',
                                        margin: '10 10 10 10',
                                        labelWidth: 150,
                                        width: 300,
                                        
//                                flex:1,
                                    },
                                    flex: 1,
                                    items: [
//                                        {
//                                            fieldLabel: 'Bis Id:',
//                                            //name: 'technician',
//                                            id: 'bis_id'
//                                        }
                                      {
                                    xtype: 'textfield',
                                    fieldLabel: 'Bis Id:',
                                    margin: '10 10 10 10',
                                    name: '',
                                    id: 'bis_id'
                                },
                                {
                                    xtype: 'panel',
                                    dock: 'top',
                                    buttons: [
                                        {
                                            text: 'Update BisId',
                                            margin: '10 10 10 10',
                                            action: 'updateDebitBisId',
                                            loadMask: true
                                        }
//                                        {
//                                            text: 'Set Master',
//                                            action: 'setDefault'
//                                                    //id: 'printpaybtn'
//                                        }
//                                        {
//                                            text: 'Save',
//                                            margin: '10 10 10 10',
//                                            action: 'saveExcelData',
//                                            loadMask: true
//                                        },
//                                        '->',
//                                        {
//                                            text: 'CreateExcel',
//                                            action: 'saveToExcel'
//                                                    //id: 'printpaybtn'
//                                        },
//                                        {
//                                            text: 'Set Default',
//                                            action: 'setDefault'
//                                                    //id: 'printpaybtn'
//                                        }
                                        
                                    ]
                                }
//                                        {
//                                            fieldLabel: 'Action Taken',
//                                            name: 'actionTaken',
//                                            id: 'techs'
//                                        },
//                                        {
//                                            fieldLabel: 'Invoice',
//                                            name: 'technicianzz',
//                                            id: 'techss'
//                                        },
//                                        {
//                                            fieldLabel: 'WO Maintenance Remarks',
//                                            name: 'technicianzzz',
//                                            id: 'techsss'
//                                        },
                                        
                                        
                                        
//                                    
//                                         
                                       
                                    ]}
                            ]
                        }
//END
                    ]
                });
    },
    WOclosecontaner: function () {
        return Ext.create('Ext.panel.Panel', {
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start',
            },
            flex: 1,
            items: [
//                {
//                    title: 'WO Close Status',
//                    xtype: 'form',
//                    id: 'closeForm',
//                    padding: '0',
//                    overflowY: 'auto',
//                    border: 0,
//                    items: [
//                        {
//                            xtype: 'fieldcontainer',
//                            layout: 'vbox',
//                            defaults: {
//                                xtype: 'textfield',
//                                margin: '10 10 10 10',
//                                labelWidth: 150,
////                                width: 300
//                                flex: 1
//                            },
//                            items: [
//                                {
//                                    fieldLabel: 'Deliver Type',
//                                    name: 'deliveryType',
//                                    readOnly: true,
//                                    id: 'deliver_type_field'
//                                },
//                                {
//                                    fieldLabel: 'Transfer Location',
//                                    name: 'transferLocation',
//                                    readOnly: true,
//                                    id: 'transfer_location_field'
//                                },
//                                {
//                                    fieldLabel: 'Courier No',
//                                    name: 'courierNo',
//                                    readOnly: true,
//                                    id: 'courier_no_field'
//                                },
//                                {
//                                    fieldLabel: 'Gate pass',
//                                    name: 'gatePass',
//                                    readOnly: true,
//                                    id: 'gate_pass_field'
//                                },
//                                {
//                                    fieldLabel: 'Deliver date',
//                                    name: 'deliveryDate',
//                                    readOnly: true,
//                                    id: 'delivery_date_field'
//                                }
//                            ]}
//                    ]
//                }
            ]
        });
    }
});