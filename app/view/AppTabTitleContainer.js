/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('singer.view.AppTabTitleContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.appTabTitleContainer',
    layout: 'fit',
    padding :'10 3 5 3',
    margin : '0 10 0 10',
    baseCls : 'tabTitle',
    style : {
         borderBottom: '1px #6bbde8 solid'
    },
    initComponent: function() {
        var taby = this;
        taby.callParent();
    }
});

