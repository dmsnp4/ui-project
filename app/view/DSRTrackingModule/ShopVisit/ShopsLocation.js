 Ext.define('singer.view.DSRTrackingModule.ShopVisit.ShopsLocation', {
     extend: 'Ext.panel.Panel',
     alias: 'widget.shopvisitshoplocation',

     //    title: 'Assing Users',
     height: 300,
     width: 700,
     layout: 'fit',
     initComponent: function () {
         var me = this;
         me.items = this.generateMap();
         me.dockedItems = this.searchForm();
         me.callParent();

     },

     searchForm: function () {
         return [
             {
                 xtype: 'form',
                 items: [
                    
                     {
                        xtype: 'panel',
                        layout: 'hbox',
                        border: false,
                        items: [
                            {
                                margin: '10 30 10 5',
                                xtype: 'combobox',
                                fieldLabel: 'Distributor',
                                //                         name: 'selctdistributor',
                                queryMode: 'local',
                                valueField: 'bisId',
                                displayField: 'bisName',
                                //                         editable: false,
                                //                         emptyText: 'Select..',
                                width: 260,
                                labelWidth: 70,
                                store: 'ShopVisits',
                                id: 'shopvisitshoplocation-distributor',
                                action: 'DistributorloadDSR',
                                allowBlank: false,
                            },
                            {
                                margin: '10 50 10 5',
                                xtype: 'combobox',
                                fieldLabel: 'DSR',
                                name: 'selctdsr',
                                queryMode: 'local',
                                valueField: 'bisId',
                                displayField: 'userId',
                                emptyText: 'Select..',
                                width: 200,
                                labelWidth: 30,
                                id: 'shopvisitshoplocation-dsr',
                                action: 'DSRloadLocations',
                                store: 'dsrShop',
                                //editable: false,
                                allowBlank: false,
                                //                                hidden : true
                            },
                            {
                                xtype: 'datefield',
                                margin: '10 0 10 5',
                                fieldLabel: 'Date',
                                name: 'DSRmvDate',
                                id: 'shopVisit-Date',
                                width: 215,
                                labelWidth: 30,
                                format: 'Y/m/d',
                                altFormats: 'Y/m/d',
                                //                                hidden : true
                                value: new Date(),
                                allowBlank: false,
                            },
                            {
                                xtype: 'button',
                                text: 'Search',
                                action: 'searchShopVisit',
                                margin: '10 0 10 25',
                                formBind: true
                            }

                        ]
                    },
 //                     {
 //                         xtype: 'panel',
 //                         layout: 'hbox',
 //                         border: false,
 //
 //                         items: [
 //
 // //                            {
 // //                                xtype: 'timefield',
 // //                                margin: '10 80 10 5',
 // //                                name: 'TimeFrom',
 // //                                fieldLabel: 'From',
 // //                                width: 220,
 // //                                labelWidth:70,
 // ////                                id:'DSRmvFromTime',
 // //                                format: 'H:i',
 // //                                altFormats:'H:i',
 // ////                                hidden : true
 // //                            },
 // //                            {
 // //                                xtype: 'timefield',
 // //                                margin: '10 0 10 5',
 // //                                name: 'TimeTo',
 // //                                fieldLabel: 'To',
 // //                                width: 215,
 // //                                labelWidth:50,
 // ////                                id:'DSRmvToTime',
 // //                                format: 'H:i',
 // //                                altFormats:'H:i',
 // ////                                hidden : true
 // //                            }
 //                        ]
 //                    },
                     {
                         xtype: 'button',
                         text: 'Search',
                         margin: '10 0 10 80',
                         action: 'loadlocations',
                         //                        id:'hisLoadBtn',
                         hidden: true
                    }
                ]
            }
        ];
     },

     generateMap: function () {
         return [
             {

                 xtype: 'gmappanel',
                 gmapType: 'map',
                 id: 'shopVisit-map',
                 center: {
                     lat: 7.8775394,
                     lng: 80.7003428
                 },
                 mapOptions: {
                     zoom: 7,
                     mapTypeId: google.maps.MapTypeId.ROADMAP
                 }
            }

        ];
     }
 });




 //icon: 'http://192.0.0.222:8080/singer/resources/images/googDroppin.png'