
Ext.define('singer.view.DSRTrackingModule.EnableUserTracking.EnHomeView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.enhomeview',
    model:'dsrtrackingMod',
    requires: [
        'Ext.form.*',
        'Ext.data.*',
        'Ext.grid.*',
    ],
    frame: false,
    forceFit: true,
    minHeight: 200,
    bodyPadding: 30,
    initComponent: function()
    {
        var thisForm = this;
        thisForm.items = thisForm.buildLayout();
        this.buttons=[{
                        text: 'Submit',
                        action:'submitbtn'
                    }]
        thisForm.callParent();
        
                    
    },
    buildLayout: function()
    {
//        var status = Ext.create('Ext.data.Store', {
//            fields: ['name', 'value'],
//            data: [
//                {"status": '1', "value": "Active"},
//                {"status": '9', "value": "Inactive"}
//            ]
//        });

        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    type: 'string',
                    dataIndex: 'parantName',
                },
                {
                    type: 'string',
                    dataIndex: 'userId'
                },
                {
                    type: 'string',
                    dataIndex: 'firstName'
                }
            ]
        };

        var value=1;
        return [
            {
                xtype: 'container',
                margin: '0 0 0 0',
                items: [
                    {
                        xtype: 'radiogroup',
                        anchor: 'none',
                        
                        layout: {
                            autoFlex: false
                        },
                        defaults: {
                            name: 'selectType',
                            margin: '0 60 0 0',
                            
                        },
//                        items: [
//                            {
//                                inputValue: 'selectdsr',
//                                boxLabel: 'Select DSRs',
//                                checked: true,
//                                id:'1'
//                            },
//                            {
//                                inputValue: 'selectany',
//                                boxLabel: 'Select any user',
//                                handler: this.onAnyUser,
//                                id:'2'
//                            }
//                        ]
                    },
                    {
//                        margin: '10 0 10 5',
//                        xtype: 'textfield',
//                        name: 'anyusername',
//                        emptyText: 'User name',
//                        width: 260,
//                        id:'anyusername',
//                        hidden : true
                        
                    },
                    {
                        margin: '10 0 10 5',
                        xtype: 'combobox',
                        fieldLabel: 'Select User Department',
                        store:'bsnsTypeStr',
                        name: 'selectuser',
                        valueField: 'bisTypeId',
                        displayField: 'bisTypeDesc',
                        typeAhead: true,
                        emptyText: 'Select user..',
                        width: 260,
                        id:'selectedusers',
                        action:'load',
                        queryMode: 'local'
                    },
                    {
                        xtype: 'button',
                        text: 'Get users',
                        margin: '15 0 15 191',
                        action:'getusers',
                        hidden : true,
                        id:'getuser'
                    },
                    {
                        xtype: 'grid',
                        store:'dsrtracking',
                        id:'userListGrid',
                        hidden : true,
                        height:380,
                        columns: [
                            {
                                text: 'Parent', 
                                dataIndex: 'parantName',
                                flex:2
                            },
                            {
                                text: 'User ID', 
                                dataIndex: 'userId',
                                flex:2
                            },
                            {
                                text: 'User Name', 
                                dataIndex: 'firstName',
                                flex:2
                            },
                            {
                                text: 'Enable Tracking', 
                                dataIndex: 'enableFlag',
                                xtype: 'checkcolumn',
                                flex:1
                                //enabled: true,
                            }
                        ],
                        features: [filtersCfg]
                    }
                ]
            }
        ];
    },
    onAnyUser:function(arg,bool){
        if(bool===true)
        {
            Ext.getCmp('anyusername').show();
            Ext.getCmp('getuser').show();
        }
            
        else if(bool===false)
        {
            Ext.getCmp('anyusername').hide();
            Ext.getCmp('getuser').hide();
        }
    }


});
