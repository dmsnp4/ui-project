
Ext.define('singer.view.DSRTrackingModule.DSRMovementHistory.DSRMovement', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dsrovement',
//    title: 'Assing Users',
    height: 300,
    width: 700,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        me.items = this.generateMap();
        me.dockedItems = this.searchForm();
        me.callParent();

    },
    searchForm: function () {
        return[
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        border: false,
                        items: [
//                            {
//                                margin: '10 30 10 5',
//                                xtype: 'combobox',
//                                fieldLabel: 'Distributor',
//                                name: 'selctdistributor',
//                                valueField: 'bisId',
//                                displayField: 'bisName',
//                                //editable: false,
//                                emptyText: 'Select..',
//                                width: 260,
//                                labelWidth: 70,
//                                queryMode: 'local',
//                                store: 'dsrmovements',
//                                id: 'selctdistributor',
//                                action: 'DistributorloadDSR',
//                                allowBlank: false
//                            },
                            {
                                margin: '10 30 10 5',
                                xtype: 'treecombo',
                                fieldLabel: 'Bis Id ',
                                store: 'ServiceUserType',
                                treeStore: 'UserTypeListTree', //tree store is ok
                                valueField: 'bisId',
                                emptyText: 'Select..',
                                width: 480,
                                labelWidth: 50,
                                displayField: 'bisName',
                                name: 'selctdistributor',
                                id: 'selctdistributor',
                                allowBlank: false
                            },
                            {
                                margin: '10 5 10 5',
                                xtype: 'combobox',
                                fieldLabel: 'User Id ',
                                name: 'selctdsr',
                                valueField: 'userId',
                                displayField: 'userId',
                                emptyText: 'Select..',
                                width: 200,
                                labelWidth: 50,
                                id: 'DistributorDSRset',
                                action: 'DSRloadLocations',
                                store: 'dsrmovementsDSRList',
                                editable: false,
                                hidden: true,
                                allowBlank: false,
                                queryMode: 'local'
                            },
                            
                        ]
                    },
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        border: false,
                        items: [
                            {
                                xtype: 'timefield',
                                margin: '0 2 10 0',
                                name: 'TimeFrom',
                                fieldLabel: '  From',
                                width: 140,
                                labelWidth: 35,
                                id: 'DSRmvFromTime',
                                format: 'H:i',
                                altFormats: 'H:i',
                                hidden: true,
                                emptyText: "HH:MM",
                                allowBlank: false
                            },
                            {
                                xtype: 'timefield',
                                margin: '0 0 10 5',
                                name: 'TimeTo',
                                fieldLabel: 'To',
                                width: 140,
                                labelWidth: 35,
                                id: 'DSRmvToTime',
                                format: 'H:i',
                                altFormats: 'H:i',
                                hidden: true,
                                emptyText: "HH:MM",
                                allowBlank: false
                            },
                            {
                                xtype: 'datefield',
                                margin: '0 0 10 5',
                                fieldLabel: 'Date',
                                name: 'DSRmvDate',
                                id: 'DSRmvDate',
                                width: 190,
                                labelWidth: 30,
                                format: 'Y/m/d',
                                altFormats: 'Y/m/d',
                                hidden: true,
                                emptyText: "YYYY/MM/DD",
                                allowBlank: false
                            },
                            {
                                xtype: 'button',
                                text: 'Search',
                                margin: '0 0 10 10',
                                action: 'loadlocations',
                                id: 'hisLoadBtn',
                                hidden: true,
                                formBind: true
                            }
                        ]
                    }

                ]
            }
        ];
    },
    generateMap: function () {
        return[
            {
                xtype: 'gmappanel',
                gmapType: 'map',
                id: 'dsrmove',
                center: {
                    lat: 7.8775394,
                    lng: 80.7003428
                },
                mapOptions: {
                    zoom: 7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            }

        ];
    }
});




//icon: 'http://192.0.0.222:8080/singer/resources/images/googDroppin.png'