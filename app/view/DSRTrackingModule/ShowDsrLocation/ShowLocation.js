
Ext.define('singer.view.DSRTrackingModule.ShowDsrLocation.ShowLocation', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dsrshowlocation',
//    title: 'Assing Users',
    height: 300,
    width: 700,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        me.items = this.createMap();
        me.dockedItems = this.createForm();
        me.callParent();
    },
    createForm: function () {
        return[
            {
                xtype: 'form',
                layout: 'hbox',
                items: [
                    {
                        flex: 1,
                        xtype: 'checkboxgroup',
                        action:'check',
//                        fieldLabel: 'one',
                        columns: 5,
                        vertical: true,
                        items: [
                            {boxLabel: 'Shops', name: 'shops', inputValue: '9',cls:'Chkshop'},
                            {boxLabel: 'Sub Dealers', name: 'subdeler', inputValue: '2',cls:'ChkSubDeal'},
                            {boxLabel: 'Distributors', name: 'distributor', inputValue: '3',cls:'ChkDist'},
                            {boxLabel: 'Service Centers', name: 'service', inputValue: '8',cls:'ChkServ'},
                            
                        ]
                    },
                    {
                        xtype: 'label',
                        text: '',
                        id: 'countLbl',
                        margin: 5
                    }
//                    {
//                        flex: 1,
//                        xtype: 'checkboxgroup',
//                        action:'check',
//                        columns: 2,
//                        vertical: true,
//                        items: [
//                            
//                        ]
//                    }
                ]
            }
        ];
    },
    createMap: function () {
        return[
            {
                xtype: 'gmappanel',
                gmapType: 'map',
                id:'dsrMap',
                center: {
//                    geoCodeAddr: "221B Baker Street",
//                    position: new google.maps.LatLng(6.871820, 79.856843),
                    lat: 7.292173,
                    lng: 80.640542,
//                    marker: {
//                        title: 'Holmes Home home'
//                    }
                },
                markers: [
//                    {
//                        lat: 7.292173,
//                        lng: 80.640542,
//                        title: 'yesssss!!!!!',
//                        animation: google.maps.Animation.BOUNCE
//                    }, {
//                        lat: 7.292173,
//                        lng: 80.740542,
//                        title: 'yesssss!!!!!',
//                        animation: google.maps.Animation.BOUNCE
//                    }
                ],
//                polyline: [
//                    {
//                        path: [
//                            new google.maps.LatLng(7.292173, 80.840542),
//                            new google.maps.LatLng(7.292173, 80.940542)
//                        ],
//                        strokeColor: "#0000FF",
//                        strokeOpacity: 0.8,
//                        strokeWeight: 2
//                    }
//                ],
                mapOptions: {
                    zoom: 7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            }
        ];
    }

});




