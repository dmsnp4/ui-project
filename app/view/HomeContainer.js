
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.HomeContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.apphomeContainer',
    layout: {type: 'hbox', align: 'stretch'},
    border: false,
    style: {
        boxShadow: '0px 0px 5px #000000',
        borderRadius : '5px'
    },
    initComponent: function() {
        var home = this;
        home.items = [];
        home.callParent();
    }
});


