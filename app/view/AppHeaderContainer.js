/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.AppHeaderContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.appheadercontainer',
    layout: {type: 'hbox', align: 'middle'},
    padding: '10 10 10 10',
    minHeight: 75,
    maxHeight: 75,
    style: {
        backgroundColor: '#ffffff',
        borderBottom: '3px #b40404 solid',
        boxShadow: '0px 0px 3px #000000'
    },
    initComponent: function() {
        this.items = [
            {
                xtype: 'container',
                // html: '<img src="resources/images/singer-logo.png" style="position: absolute; top : -25px;" />',
                html: '<img src="resources/images/output_9QsoX9.gif" style="position: absolute; top : 0px;width: 50px;" />' +
                        '<div class="mainTitle">Integrated Solution for Singer Digital Media<p>SINGER (Sri Lanka) PLC</p></div>',
                header: 'Integrated Solution for Singer Digital Media(0.11)',
                flex: 5
            },
        ];
        this.callParent(arguments);
    }
});

