/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('singer.view.Messages.AreYouSureWithRemark', {
    extend: 'Ext.form.Panel',
    alias: 'widget.areyousurewithremark',
    title: 'Are you sure you want to unassign the user?',
    width: 500,
//            height: 600,
    bodyPadding: 5,
    overflowY: 'auto',
    defaults: {
        xtype: 'textfield'
    },
//    fieldDefaults: {
//        anchor: '100%',
//        labelAlign: 'left',
//        msgTarget: 'side',
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//        labelWidth: 180,
//        margin: '10 0 10 0'
//    },
    initComponent: function() {
        var me = this;
        me.items = me.buildItems();
        me.buttons = me.buildButtons();
        me.callParent();
    },
    buildItems: function() {
        return [{
                xtype: 'textareafield',
                fieldLabel: 'Remark',
                name: 'user_id',
//                maxLength: 19,
//                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z0-9_]+$/
            }];
    },
    buildButtons: function() {
        return [
            {
                xtype: 'button',
                text: 'Yes',
                action: 'cancel'
            },
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            },
//            {
////                iconCls: 'icon-repeat',
//                xtype: 'button',
//                text: 'Save',
//                action: 'save',
//                id: 'newuser-save',
//                formBind: true
////                id: 'userform-reset-update'
//            },
//            {
//                xtype: 'button',
//                text: 'Create',
////                        ui: 'success',
//                action: 'create',
//                id: 'newuser-create',
//                formBind: true
//            }
        ];
    },
});