Ext.define('singer.view.Messages.TestGrid', {
    extend: 'Ext.grid.Panel',
    requires: ['singer.view.Messages.SearchTrigger'],
    alias: 'widget.ownersGrid',
    store: 'Owners',
    columns: [{
            dataIndex: 'id',
            width: 50,
            text: 'ID'
        }, {
            dataIndex: 'name',
            text: 'Name',
            items: [{
                    xtype: 'searchtrigger',
                    autoSearch: true
                }]
        }]

});
