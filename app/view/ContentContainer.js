Ext.define('singer.view.ContentContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.appcontentContainer',
    layout: {type: 'vbox', align: 'stretch'},
    border: false,
    flex : 2,
    style: {
        backgroundColor: '#ffffff',
        boxShadow: '0px 0px 2px #000000',
        backgroundImage : 'url(resources/images/bgW.png)',
        backgroundPosition : 'center',
        backgroundRepeat : 'no-repeat'
    },
    initComponent: function() {
        var contentCont = this;
        contentCont.items = [];
        contentCont.callParent();
    }
});
