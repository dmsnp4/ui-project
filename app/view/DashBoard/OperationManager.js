



Ext.define('singer.view.DashBoard.OperationManager', {
    extend: 'Ext.container.Container',
    alias: 'widget.operationM',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    overflowY: 'auto',
    initComponent: function() {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.buildMasterReportsContainer(),
        ];
        inquiryCmp.callParent();
        
        
    },
    buildMasterReportsContainer: function() {
        
        return Ext.create('Ext.panel.Panel',
        {
            items:[
                {
                    xtype:'form',
                    html: '<iframe src="http://192.0.0.222:8080/dashbuilder" width="100%" height="750px" overflow: auto;"></iframe >',
                }
            ]
        });
        
    }
    

});
