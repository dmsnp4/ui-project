Ext.define('singer.view.AppTabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.appTabPanel',
    id : 'appTabContainer',
    border: false,
    layout:'fit',
    margin :'5 10 5 10',
    flex : 5,
    initComponent: function() {
        var taby = this;
        taby.items = [];
        taby.callParent();
    }
});
