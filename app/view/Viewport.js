Ext.define('singer.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.container.Viewport',
        'Ext.layout.container.Fit',
//        'singer.view.users.UserGrid',
        'singer.view.AppContainer',
        'singer.view.AppHeaderContainer'
    ],
    layout: {
        type: "vbox",
        align: "stretch"
    },
    items: [{
//            xtype: 'usergridcontroller'
        }],
    
    border: false,
    initComponent: function() {
        var mainViewport = this;
        mainViewport.items = [{xtype: 'appheadercontainer',flex : 2},{xtype: 'appcontainer',flex : 9}];
        mainViewport.callParent();
    }
    
    
});


