/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Events.CreateEvent.PointScheme', {
    extend: 'Ext.window.Window',
    alias: 'widget.pointscheme',
    title: 'Enter Points Scheme',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    //    height: 600,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            //            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        var typ = Ext.create('Ext.data.Store', {
            fields: ['type', 'value'],
            data: [
                {
                    "type": 1,
                    "value": "Qty"
                },
                {
                    "type": 2,
                    "value": "Value"
                }
            ]
        });
        return [

            {
                fieldLabel: 'Sales Type',
                name: 'schemeSaleType',
                xtype: 'combobox',
                store: typ,
                displayField: 'value',
                valueField: 'type',
                allowBlank:false,
                queryMode: 'local'
            },
            {
                fieldLabel: 'No of Units',
                name: 'schmeNoOfUnit',
                allowBlank:false,
                regex: /^[0-9]+$/,
                value:'0'
            },
            {
                fieldLabel: 'Fixed Points',
                name: 'scheamfixedPoint',
                allowBlank:false,
                regex: /^[0-9]+$/,
                value:'0'
            },
            {
                xtype: 'checkboxfield',
                fieldLabel: 'Float',
                name: 'schmefloatFlag',
                id: 'pointscheme-floatFlag',
                listeners: {
                    change: function (me, newValue, oldValue, eOpts) {
                        //console.log(newValue);
                        Ext.getCmp('pointscheme-salesTypes').setVisible(newValue);
                        Ext.getCmp('pointscheme-salesPoints').setVisible(newValue);
                        Ext.getCmp('pointscheme-assignpoints').setVisible(newValue);
                    }
                }
            },
            {
                fieldLabel: 'Additional Sales Type',
                name: 'schmeAddnSalesType',
                xtype: 'combobox',
                store: 'SalesTypes', // Ext.data.StoreManager.lookup('simpsonsStore'),
                displayField: 'description',
                valueField: 'code',
                allowBlank: true,
                id: 'pointscheme-salesTypes',
                hidden: true,
                queryMode: 'local'
            },
            {
                fieldLabel: 'Additoinal Sales Points',
                name:'schmeAddnSalesPoint',
                id: 'pointscheme-salesPoints',
                hidden: true,
                value:'0',
                regex: /^[0-9]+$/

            },
            {
                fieldLabel: 'Assign Points',
                id: 'pointscheme-assignpoints',
                hidden: true,
                name: 'schmeAssignPoint',
                regex: /^[0-9]+$/,
                value:'0'
            }

        ];
    },
    buildButtons: function () {
        return [

            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
                //                id: 'userform-reset-insert'
            }, {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Enter',
                action: 'onEnter',
                //                id: 'newrepaircategory-save',
                formBind: true
                //                id: 'userform-reset-update'
            },
 //            {
 //                xtype: 'button',
 //                text: 'Create',
 ////                        ui: 'success',
 //                action: 'create',
 //                id: 'newrepaircategory-create',
 //                formBind: true
 //            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});