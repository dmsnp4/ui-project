/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Events.CreateEvent.ViewSalesRules', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewsalesrules',
    title: 'Add Event Rules',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    //    height: 600,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            //            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'displayfield',
                readOnly: true,
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Active"
                },
                {
                    "status": 2,
                    "value": "Inactive"
                }
            ]
        });
        return [
            {
                fieldLabel: 'Product Family',
                name: 'productFamily',
                xtype: 'combobox',
                store: 'ProductFamily', //Ext.data.StoreManager.lookup('Brands'),
                displayField: 'description',
                valueField: 'description',
                allowBlank: false,
                queryMode: 'local'
            },
            {
                fieldLabel: 'Brand',
                name: 'barnd',
                xtype: 'combobox',
                store: 'Brands', // Ext.data.StoreManager.lookup('simpsonsStore'),
                displayField: 'description',
                valueField: 'description',
                allowBlank: false,
                queryMode: 'local'
            },
            {
                fieldLabel: 'Model',
                name: 'modleNo',
                xtype: 'combobox',
                store: 'modStore', //Ext.data.StoreManager.lookup('modStore'),
                displayField: 'model_Description',
                valueField: 'model_No',
                allowBlank: false,
                queryMode: 'local'
            },
            {
                fieldLabel: 'Period',
                name: 'period',
                //                regex: /^[0-9]+$/,
                allowBlank: false,
                xtype: 'combobox',
                store: 'EventPeriods', // Ext.data.StoreManager.lookup('simpsonsStore'),
                displayField: 'description',
                valueField: 'code',
                queryMode: 'local'
            },
            {
                fieldLabel: 'No of intervals',
                name: 'intervals',
                //				xtype: 'combobox',
                //				store: Ext.data.StoreManager.lookup('simpsonsStore'),
                //				displayField: 'description',
                //				valueField: 'code',
                allowBlank: false,
                regex: /^[0-9]+$/,
                //                queryMode: 'local'
            },
            {
                fieldLabel: 'Event Accepted No of Intervals',
                name: 'acceptedInteval',
                allowBlank: false
            },
            {
                fieldLabel: 'Sales',
                name: 'salesType',
                xtype: 'combobox',
                store: 'SalesTypes', // Ext.data.StoreManager.lookup('simpsonsStore'),
                displayField: 'description',
                valueField: 'code',
                allowBlank: false,
                queryMode: 'local'
            },
            {
                fieldLabel: 'Points',
                name: 'point',
                allowBlank: false
            },
//            {
//                xtype: 'button',
//                text: 'Enter Point Scheme',
//                action: 'onPointScheme'
//            },
//            {
//                xtype: 'checkboxfield',
//                fieldLabel: 'Rules Satisfy Rules Schema',
//                name: 'pointSchemeFlag',
//                allowBlank: false,
//                //                queryMode: 'local'
//            },
            {
                fieldLabel: 'Minimum Requirnment',
                name: 'minReq',
                allowBlank: false
            }

        ];
    },
    buildButtons: function () {
        return [

            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Done',
                action: 'cancel'
                //                id: 'userform-reset-insert'
            }, 
//            {
//                //                iconCls: 'icon-repeat',
//                xtype: 'button',
//                text: 'Submit',
//                action: 'onSubmit',
//                id: 'addeventrules-create',
//                formBind: true
//                //                                id: 'userform-reset-update'
//            },
//            {
//                xtype: 'button',
//                text: 'Save',
//                //                        ui: 'success',
//                action: 'Save',
//                id: 'addeventrules-save',
//                formBind: true
//             }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});