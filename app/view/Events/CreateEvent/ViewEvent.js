Ext.define('singer.view.Events.CreateEvent.ViewEvent', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewevent',
    loadMask: true,
    //    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Event Details',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: '90%',
    height: '90%',
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
           width: '90%',
            height: '90%',
            bodyPadding: 5,
            overflowY: 'auto',
            overflowX: 'auto',
            margin: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0',
                readOnly: true
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Planing"
                },
                {
                    "status": 3,
                    "value": "Start"
                },
                {
                    "status": 9,
                    "value": "Closed"
                }
            ]
        });
        return [
            {
                fieldLabel: 'Event Name',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                name: 'eventName',
                maxLength: 20,
            },
            {
                fieldLabel: 'Description',
                name: 'eventDesc',
            },
            {
                xtype: 'datefield',
                //                anchor: '100%',
                fieldLabel: 'Start Date',
                name: 'startDate',
                maxValue: new Date(),
                format: 'Y-m-d',
            },
            {
                xtype: 'datefield',
                fieldLabel: 'End Date',
                name: 'endDate',
                format: 'Y-m-d',
            },
            //department and users grid
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h3>Participants</h3><hr>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'grid',
                maxHeight: 200,
                store: 'CreateEventBisTypes',
                id: 'neweventform-EventBisTypes',
                columns: [
//                    {
//                        xtype: 'checkcolumn',
//                        dataIndex: 'chCol',
//                        action: 'chChange',
//                    },
                    {
                        text: 'Department Type',
                        dataIndex: 'bisStruTypeName',
                        flex: 1
                    },
                    {
                        text: 'Total Users',
                        dataIndex: 'bisStruTypeCount',
                        flex: 1
                    }],
            },
 //            {
 //                xtype: 'button',
 //                text: 'Change Participants',
 //                action: 'onChangeParticipants',
 //                margin: '2 2 2 2'
 //            },
 //            {
 //                xtype: 'button',
 //                text: 'Add Event Rules Sales',
 //                action: 'onAddEventRules',
 //                margin: '2 2 2 2'
 //            },
            //Rules Grid
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h3>Sales Rules</h3><hr>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'container',
                width: 1600,
                //                maxHeight: 200,
                items: [
                    {
                        xtype: 'grid',
                        forceFit: true,
                        maxHeight: 200,
                        maxWidth: '90%',
                        //                        width: '2000',

                        //                layout: 'fit',

                        store: 'EventRules', //Ext.data.StoreManager.lookup('simpsonsStore'),
                        columns: [
                            {
                                text: 'Product Family',
                                dataIndex: 'productFamily'
                            },
                            {
                                text: 'Brand',
                                dataIndex: 'barnd',
                                //                        flex: 1
                            },
                            {
                                text: 'Model',
                                dataIndex: 'modleDesc'
                            },
 //							{
 //								text: 'Period',
 //								dataIndex: 'period'
 //                            },
                            {
                                text: 'No of Intervals',
                                dataIndex: 'intervals'
                            },
                            {
                                text: 'Accepted No of Intervals',
                                dataIndex: 'acceptedInteval'
                            },
 //							{
 //								text: 'Sales',
 //								dataIndex: 'salesType'
 //                            },
                            {
                                text: 'Points',
                                dataIndex: 'point'
                            },
                            {
                                text: 'Points Schema Fixed',
                                dataIndex: 'pointSchemeFlag',
                                renderer: function (value) {
                                    if (value === 1) {
                                        return "Yes";
                                    }
                                    if (value === 0) {
                                        return "No";
                                    }

                                }
                            },
 //							{
 //								text: 'Float Increment'
 //                            },
 //							{
 //								text: 'Float Points'
 //                            },
 //							{
 //								text: 'Min Requirnmet',
 //                            },
 //							{
 //								text: 'Total Achivement'
 //                            },
 //                            {
 //                                xtype: 'actioncolumn',
 //                                text: 'View',
 //                                menuDisabled: true,
 //                                width: 70,
 //                                items: [{
 //                                    icon: 'resources/images/view-icon.png',
 //                                    tooltip: 'View',
 //                                    handler: function (grid, rowIndex, colIndex) {
 //                                        var rec = grid.getStore().getAt(rowIndex);
 //                                        //console.log(rec);
 //                                        this.up('window').fireEvent('createEventView', grid, rec);
 //
 //                                    }
 //                                    }]
 //                            },
 //                            {
 //                                xtype: 'actioncolumn',
 //                                text: 'Edit',
 //                                menuDisabled: true,
 //                                width: 70,
 //                                items: [{
 //                                    icon: 'resources/images/edit.png',
 //                                    tooltip: 'Edit',
 //                                    handler: function (grid, rowIndex, colIndex) {
 //                                        var rec = grid.getStore().getAt(rowIndex);
 //                                        this.up('window').fireEvent('createEventEdit', grid, rec, rowIndex);
 //                                    }
 //                                    }]
 //                            },
 //                            {
 //                                xtype: 'actioncolumn',
 //                                text: 'Delete',
 //                                menuDisabled: true,
 //                                width: 70,
 //                                items: [{
 //                                    icon: 'resources/images/delete-icon.png',
 //                                    tooltip: 'Delete',
 //                                    handler: function (grid, rowIndex, colIndex) {
 //                                        var rec = grid.getStore().getAt(rowIndex);
 //                                        this.up('window').fireEvent('createEventDelete', grid, rec);
 //                                    }
 //                                    }]
 //                            },
                        ],
                    },
                ]
            },
 //            {
 //                xtype: 'button',
 //                text: 'Add Event Rules Non-Sales',
 //                action: 'onAddNonSalesRules',
 //                margin: '2 2 2 2'
 //            },
            //non Sales ruls grid
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h3>Non Sales Rules</h3><hr>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'grid',
                maxHeight: '200',
                forceFit: true,
                store: 'EventNonSales', //Ext.data.StoreManager.lookup('simpsonsStore'),
                columns: [
                    {
                        text: 'Description',
                        dataIndex: 'description'
                    },
                    {
                        text: 'Target',
                        dataIndex: 'taget'

                    },
                    {
                        text: 'Archivement',
                        dataIndex: 'point'
                    },
//                    {
//                        xtype: 'actioncolumn',
//                        text: 'View',
//                        menuDisabled: true,
//                        width: 70,
//                        items: [{
//                            icon: 'resources/images/view-icon.png',
//                            tooltip: 'View',
//                            handler: function (grid, rowIndex, colIndex) {
//                                var rec = grid.getStore().getAt(rowIndex);
//                                this.up('window').fireEvent('nonSalesView', grid, rec);
//                            }
//                            }]
//                    },
 //                    {
 //                        xtype: 'actioncolumn',
 //                        text: 'Edit',
 //                        menuDisabled: true,
 //                        width: 70,
 //                        items: [{
 //                            icon: 'resources/images/edit.png',
 //                            tooltip: 'Edit',
 //                            handler: function (grid, rowIndex, colIndex) {
 //                                var rec = grid.getStore().getAt(rowIndex);
 //                                this.up('window').fireEvent('nonSalesEdit', grid, rec, rowIndex);
 //                            }
 //                            }]
 //                    },
 //                    {
 //                        xtype: 'actioncolumn',
 //                        text: 'Delete',
 //                        menuDisabled: true,
 //                        width: 70,
 //                        items: [{
 //                            icon: 'resources/images/delete-icon.png',
 //                            tooltip: 'Delete',
 //                            handler: function (grid, rowIndex, colIndex) {
 //                                var rec = grid.getStore().getAt(rowIndex);
 //                                this.up('window').fireEvent('nonSalesDelete', grid, rec);
 //                            }
 //                            }]
 //                    },
                ]
            },
 //            {
 //                xtype: 'button',
 //                text: 'Award Detail',
 //                action: 'onAwardDetail',
 //                margin: '2 2 2 2'
 //            },
            //award detail grid
            {
                xtype: 'label',
                //                forId: 'myFieldId',
                html: '<h3>Award Detail</h3><hr>',
                //                margin: '0 0 0 10'
            },
            {
                xtype: 'grid',
                maxHeight: '200',
                forceFit: true,
                store: 'EventAwards', //Ext.data.StoreManager.lookup('simpsonsStore'),
                columns: [
                    {
                        text: 'Place',
                        dataIndex: 'palce'
                    },
                    {
                        text: 'Award',
                        dataIndex: 'award'
                    },
                    //					{
                    //						text: 'Selected Candidate'
                    //                    },
 //                    {
 //                        xtype: 'actioncolumn',
 //                        text: 'View',
 //                        menuDisabled: true,
 //                        width: 70,
 //                        items: [{
 //                            icon: 'resources/images/view-icon.png',
 //                            tooltip: 'View',
 //                            handler: function (grid, rowIndex, colIndex) {
 //                                var rec = grid.getStore().getAt(rowIndex);
 //                                this.up('window').fireEvent('awardView', grid, rec);
 //                            }
 //                            }]
 //                    },
 //                    {
 //                        xtype: 'actioncolumn',
 //                        text: 'Edit',
 //                        menuDisabled: true,
 //                        width: 70,
 //                        items: [{
 //                            icon: 'resources/images/edit.png',
 //                            tooltip: 'Edit',
 //                            handler: function (grid, rowIndex, colIndex) {
 //                                var rec = grid.getStore().getAt(rowIndex);
 //                                this.up('window').fireEvent('awardEdit', grid, rec);
 //                            }
 //                            }]
 //                    },
 //                    {
 //                        xtype: 'actioncolumn',
 //                        text: 'Delete',
 //                        menuDisabled: true,
 //                        width: 70,
 //                        items: [{
 //                            icon: 'resources/images/delete-icon.png',
 //                            tooltip: 'Delete',
 //                            handler: function (grid, rowIndex, colIndex) {
 //                                var rec = grid.getStore().getAt(rowIndex);
 //                                this.up('window').fireEvent('awardDelete', grid, rec);
 //                            }
 //                            }]
 //                    },
                ]
            },
            {
                fieldLabel: 'Threshold',
                name: 'threshold'
            },
            {
                fieldLabel: 'Status',
                name: 'status',
                xtype: 'combobox',
                store: stat,
                displayField: 'value',
                valueField: 'status'
            },
            {
                fieldLabel: 'Comments from Event Manager',
                name: 'eventMrgComment',
                xtype: 'textareafield'
            },
            {
                fieldLabel: 'Comments from Marketing Manager/Brand Manager',
                name: 'mktMrgComment',
                xtype: 'textareafield'
            },
            {
                name: 'eventId',
                xtype: 'hidden'
            }

        ];
    },
    buildButtons: function () {
        return ['->', {
            text: 'Done',
            action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});