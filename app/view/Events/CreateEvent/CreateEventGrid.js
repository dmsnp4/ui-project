Ext.define('singer.view.Events.CreateEvent.CreateEventGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.createeventgrid',
    loadMask: true,
    store: 'CreateEvent', //Ext.data.StoreManager.lookup('simpsonsStore'),
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    layout: 'fit',
    //    forceFit: true,
    minHeight: 200,
    initComponent: function () {
        var usersGrid = this;
        usersGrid.columns = usersGrid.buildColumns();
        usersGrid.dockedItems = usersGrid.buildDockItems(this);
        usersGrid.callParent();
    },
    buildDockItems: function (userGrid) {
        return [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
                        xtype: 'pagingtoolbar',
                        store: 'CreateEvent',
                        dock: 'bottom',
                        displayInfo: true
                    }]
            }, {
                xtype: 'form',
                //                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch',
                        minWidth: 100,
                        minHeight: 10
                    },
                    '->',
                    {
                        iconCls: 'icon-search',
                        text: 'Add New Event',
                        action: 'open_window',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }
        ];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {
                    "name": "1",
                    "value": "Active"
                },
                {
                    "name": "2",
                    "value": "Inactive"
                }
            ]
        });
        return [
            {
                xtype: 'searchablecolumn',
                text: 'Event ID',
                dataIndex: 'eventId',
//                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Event Name',
                dataIndex: 'eventName',
                flex: 1
            }, {
                xtype: 'searchablecolumn',
                text: 'Start Date',
                dataIndex: 'startDate',
//                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'End Date',
                dataIndex: 'endDate',
//                flex: 2
            }, 
//            {
//                xtype: 'searchablecolumn',
//                text: 'Rules',
//                //                dataIndex: 'cateDesc',
////                flex: 2
//            },
            {
                xtype: 'searchablecolumn',
                text: 'Threshold',
                dataIndex: 'threshold',
//                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'statusMsg',
//                flex: 2,
//                renderer: function (value) {
//                    if (value === 1) {
//                        return "Planing";
//                    }
//                    if (value === 3) {
//                        return "Start";
//                    }
//                    if (value === 9) {
//                        return "Closed";
//                    }
//                }
            }, {
                xtype: 'actioncolumn',
                text: 'Start',
                menuDisabled: true,
                width: 70,
                items: [{
                        icon: 'resources/images/start.png',
                        tooltip: 'Start',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('onEventStart', grid, record);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                            // Returns true if 'editable' is false (, null, or undefined)
                            if (record.get('status') === 3) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                text: 'View',
                menuDisabled: true,
                width: 70,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('onEventView', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                text: 'Edit',
                menuDisabled: true,
                width: 70,
                items: [{
                        icon: 'resources/images/edit.png',
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('onEventEdit', grid, record);
//                        //console.log(this.up('grid'));
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
//                        //console.log(record.get('status'));
                            // Returns true if 'editable' is false (, null, or undefined)
                            if (record.get('status') === 3) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }]
            },
//			{
//				xtype: 'actioncolumn',
//				text: 'Edit',
//				menuDisabled: true,
//				width: 70,
//				items: [{
//					icon: 'resources/images/edit.png',
//					tooltip: 'View',
//					handler: function (grid, rowIndex, colIndex) {
//						var rec = grid.getStore().getAt(rowIndex);
//						this.up('grid').fireEvent('eventEdit', grid, rec);
//					},
//					isDisabled: function (view, rowIndex, colIndex, item, record) {
//						// Returns true if 'editable' is false (, null, or undefined)
////						return !record.get('status');
//                        if (rec.get('status') === 1) {
//							return true;
//						} else {
//							return false;
//						}
//					}
//                    }]
//            }

        ];
    }

});