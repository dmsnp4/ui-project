Ext.define('singer.view.Events.CreateEvent.CreateEventForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.neweventform',
    loadMask: true,
    modal: true,
    title: 'Add New Event',
    layout: 'fit',
    //    constrain: true,
    width: '90%',
    height: '90%',
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            //            width: 800,
            width: '90%',
            height: '90%',
            bodyPadding: 5,
            overflowY: 'auto',
            overflowX: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 10 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });

        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function () {

    },
    buildItems: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Planing"
                },
//                {
//                    "status": 3,
//                    "value": "Start"
//                },
                {
                    "status": 9,
                    "value": "Closed"
                }
            ]
        });
        return [
            {
                xtype: 'fieldset',
                title: 'Event Description',
                collapsible: true,
                defaultType: 'textfield',
                items: [
                    {
                        fieldLabel: 'Event Name',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false,
                        name: 'eventName',
                        maxLength: 20,
            },
                    {
                        fieldLabel: 'Description',
                        name: 'eventDesc',
            },
                    {
                        xtype: 'datefield',
                        //                anchor: '100%',
                        fieldLabel: 'Start Date',
                        name: 'startDate',
                        //                        maxValue: new Date(),
                        allowBlank: false,
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        format: 'Y-m-d',
                        listeners: {
                            scope: this,
                            change: function (me, newValue, oldValue) {
                                //                                //console.log()
                                me.nextSibling().setMinValue(newValue);
                            }
                        }
            },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'End Date',
                        name: 'endDate',
                        format: 'Y-m-d',
                        allowBlank: false,
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>'
                        //                        minValue: this.previousSibling().getValue()
            }
                ]
            },

            //department and users grid
            {
                xtype: 'fieldset',
                title: 'Participants',
                collapsible: true,
                defaultType: 'textfield',
                items: [
                    {
                        xtype: 'grid',
                        maxHeight: 200,
                        store: 'CreateEventBisTypes',
                        id: 'neweventform-EventBisTypes',
                        columns: [
                            {
                                xtype: 'checkcolumn',
                                dataIndex: 'chCol',
                                action: 'chChange',
                                //                                 listeners: {
                                //                            checkchange: function(column, rowIndex, checked, eOpts) {
                                //                                var record = (this.up('form').getStore().getAt(rowIndex));
                                //                                this.up('window').fireEvent('chChange', checked, record, rowIndex);
                                //                            }
                                //                        }
                    },
                            {
                                text: 'Department Type',
                                dataIndex: 'bisStruTypeName',
                                flex: 1
                    },
                            {
                                text: 'Total Users',
                                dataIndex: 'bisStruTypeCount',
                                flex: 1
                    }],
            },
                    {
                        xtype: 'button',
                        text: 'Change Participants',
                        action: 'onChangeParticipants',
                        margin: '10 2 10 2'
            }
                ]
            },

            //Rules Grid
            {
                xtype: 'fieldset',
                title: 'Rules',
                collapsible: true,
                defaultType: 'textfield',
                width: '90%',
                height: '90%',
                bodyPadding: 5,
                overflowY: 'auto',
                overflowX: 'auto',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add Event Rules Sales',
                        action: 'onAddEventRules',
                        margin: '10 2 10 2'
            },
                    {
                        xtype: 'container',
                        width: 1600,
                        //                maxHeight: 200,
                        items: [
                            {
                                xtype: 'grid',
                                forceFit: true,
                                maxHeight: 200,
                                maxWidth: '90%',
                                //                        width: '2000',

                                //                layout: 'fit',

                                store: 'EventRules', //Ext.data.StoreManager.lookup('simpsonsStore'),
                                columns: [
                                    {
                                        text: 'Product Family',
                                        dataIndex: 'productFamily'
                            },
                                    {
                                        text: 'Brand',
                                        dataIndex: 'barnd',
                                        //                        flex: 1
                            },
                                    {
                                        text: 'Model',
                                        dataIndex: 'modleDesc'
                            },
 //							{
 //								text: 'Period',
 //								dataIndex: 'period'
 //                            },
                                    {
                                        text: 'No of Intervals',
                                        dataIndex: 'intervals'
                            },
                                    {
                                        text: 'Accepted No of Intervals',
                                        dataIndex: 'acceptedInteval'
                            },
                                    {
                                        text: 'Sales',
                                        dataIndex: 'salesType',
                                        renderer: function (value) {
                                            if (value === 1) {
                                                return "Qty";
                                            }
                                            if (value === 0) {
                                                return "Value";
                                            }

                                        }
                             },
                                    {
                                        text: 'Points',
                                        dataIndex: 'point'
                            },
                                    {
                                        text: 'Points Schema Fixed',
                                        dataIndex: 'pointSchemeFlag',
                                        renderer: function (value) {
                                            if (value === 1) {
                                                return "Yes";
                                            }
                                            if (value === 0) {
                                                return "No";
                                            }

                                        }
                            },
                                    {
                                        text: 'Float Increment',
                                        dataIndex: 'schmefloagFlagString',

                             },
                                    {
                                        text: 'Float Points',
                                        dataIndex: 'schmefloatPoint'
                             },
                                    {
                                        text: 'Min Requirnmet',
                                        dataIndex: 'minReq'
                             },
                                    {
                                        text: 'Total Achivement',
                                        dataIndex: 'totalAchivement'
                             },
                                    {
                                        xtype: 'actioncolumn',
                                        text: 'View',
                                        menuDisabled: true,
                                        width: 70,
                                        items: [{
                                            icon: 'resources/images/view-icon.png',
                                            tooltip: 'View',
                                            handler: function (grid, rowIndex, colIndex) {
                                                var rec = grid.getStore().getAt(rowIndex);
                                                //                                                //console.log(rec);
                                                this.up('window').fireEvent('createEventView', grid, rec);

                                            }
                                    }]
                            },
                                    {
                                        xtype: 'actioncolumn',
                                        text: 'Edit',
                                        menuDisabled: true,
                                        width: 70,
                                        items: [{
                                            icon: 'resources/images/edit.png',
                                            tooltip: 'Edit',
                                            handler: function (grid, rowIndex, colIndex) {
                                                var rec = grid.getStore().getAt(rowIndex);
                                                this.up('window').fireEvent('createEventEdit', grid, rec, rowIndex);
                                            }
                                    }]
                            },
                                    {
                                        xtype: 'actioncolumn',
                                        text: 'Delete',
                                        menuDisabled: true,
                                        width: 70,
                                        items: [{
                                            icon: 'resources/images/delete-icon.png',
                                            tooltip: 'Delete',
                                            handler: function (grid, rowIndex, colIndex) {
                                                var rec = grid.getStore().getAt(rowIndex);
                                                this.up('window').fireEvent('createEventDelete', grid, rec);
                                            }
                                    }]
                            },
                        ],
                    },
                ]
            },
                    {
                        xtype: 'button',
                        text: 'Add Event Rules Non-Sales',
                        action: 'onAddNonSalesRules',
                        margin: '10 2 10 2'
            },
            //non Sales ruls grid
                    {
                        xtype: 'grid',
                        maxHeight: '200',
                        forceFit: true,
                        store: 'EventNonSales', //Ext.data.StoreManager.lookup('simpsonsStore'),
                        columns: [
                            {
                                text: 'Description',
                                dataIndex: 'description'
                    },
                            {
                                text: 'Target',
                                dataIndex: 'taget'

                    },
                            {
                                text: 'Archivement',
                                dataIndex: 'point'
                    },
                            {
                                xtype: 'actioncolumn',
                                text: 'View',
                                menuDisabled: true,
                                width: 70,
                                items: [{
                                    icon: 'resources/images/view-icon.png',
                                    tooltip: 'View',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('window').fireEvent('nonSalesView', grid, rec);
                                    }
                            }]
                    },
                            {
                                xtype: 'actioncolumn',
                                text: 'Edit',
                                menuDisabled: true,
                                width: 70,
                                items: [{
                                    icon: 'resources/images/edit.png',
                                    tooltip: 'Edit',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('window').fireEvent('nonSalesEdit', grid, rec, rowIndex);
                                    }
                            }]
                    },
                            {
                                xtype: 'actioncolumn',
                                text: 'Delete',
                                menuDisabled: true,
                                width: 70,
                                items: [{
                                    icon: 'resources/images/delete-icon.png',
                                    tooltip: 'Delete',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('window').fireEvent('nonSalesDelete', grid, rec);
                                    }
                            }]
                    },
                ]
            },
                    {
                        xtype: 'button',
                        text: 'Award Detail',
                        action: 'onAwardDetail',
                        margin: '10 2 10 2'
            },
            //award detail grid
                    {
                        xtype: 'grid',
                        maxHeight: '200',
                        forceFit: true,
                        store: 'EventAwards', //Ext.data.StoreManager.lookup('simpsonsStore'),
                        columns: [
                            {
                                text: 'Place',
                                dataIndex: 'palce'
                    },
                            {
                                text: 'Award',
                                dataIndex: 'award'
                    },
                    //					{
                    //						text: 'Selected Candidate'
                    //                    },
                            {
                                xtype: 'actioncolumn',
                                text: 'View',
                                menuDisabled: true,
                                width: 70,
                                items: [{
                                    icon: 'resources/images/view-icon.png',
                                    tooltip: 'View',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('window').fireEvent('awardView', grid, rec);
                                    }
                            }]
                    },
                            {
                                xtype: 'actioncolumn',
                                text: 'Edit',
                                menuDisabled: true,
                                width: 70,
                                items: [{
                                    icon: 'resources/images/edit.png',
                                    tooltip: 'Edit',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('window').fireEvent('awardEdit', grid, rec);
                                    }
                            }]
                    },
                            {
                                xtype: 'actioncolumn',
                                text: 'Delete',
                                menuDisabled: true,
                                width: 70,
                                items: [{
                                    icon: 'resources/images/delete-icon.png',
                                    tooltip: 'Delete',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('window').fireEvent('awardDelete', grid, rec);
                                    }
                            }]
                    },
                ]
            },
                ]
            }
            ,
            {
                fieldLabel: 'Threshold',
                name: 'threshold',
                regex: /^[0-9]+$/,
                allowBlank:false
            },
            {
                fieldLabel: 'Status',
                name: 'status',
                xtype: 'combobox',
                store: stat,
                displayField: 'value',
                valueField: 'status'
            },
            {
                fieldLabel: 'Comments from Event Manager',
                name: 'eventMrgComment',
                xtype: 'textareafield'
            },
            {
                fieldLabel: 'Comments from Marketing Manager/Brand Manager',
                name: 'mktMrgComment',
                xtype: 'textareafield'
            },
            {
                name: 'eventId',
                xtype: 'hidden'
            }

        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }, {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'neweventform-save',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                id: 'neweventform-create',
                formBind: true
            }
        ];
    },
    //   

});