/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Events.CreateEvent.NonSales', {
    extend: 'Ext.window.Window',
    alias: 'widget.nonsales',
    title: 'Non Sales',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    //    height: 600,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            //            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        var typ = Ext.create('Ext.data.Store', {
            fields: ['type', 'value'],
            data: [
                {
                    "type": 1,
                    "value": "Qty"
                },
                {
                    "type": 2,
                    "value": "Value"
                }
            ]
        });
        return [
            {
                fieldLabel: 'Description',
                name: 'description',
                allowBlank:false
            },
            {
                fieldLabel: 'Sales',
                name: 'salesType',
                xtype: 'combobox',
                store: 'SalesTypes', // Ext.data.StoreManager.lookup('simpsonsStore'),
                displayField: 'description',
                valueField: 'code',
                allowBlank: false,
                id: 'nonsales-salesCombo',
                queryMode: 'local'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Target',
                name: 'taget',
                allowBlank:true,
//                regex: /^[0-9]+$/,
            },
            {
                fieldLabel: 'Achieve Points',
                name: 'point',
                allowBlank:false,
                regex: /^[0-9]+$/
            }
        ];
    },
    buildButtons: function () {
        return [

            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
                //                id: 'userform-reset-insert'
            }, {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Submit',
                action: 'onSubmit',
                id: 'nonsales-create',
                formBind: true
                //                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Save',
                //                        ui: 'success',
                action: 'save',
                id: 'nonsales-save',
                formBind: true
             }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});