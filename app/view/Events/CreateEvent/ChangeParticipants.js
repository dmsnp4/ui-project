/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Events.CreateEvent.ChangeParticipants', {
    extend: 'Ext.window.Window',
    alias: 'widget.changeparticipants',
    title: 'Change Prticipants',
    layout: 'fit',
    constrain: true,
    modal: true,
    width: 620,
    //    height: 600,
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 620,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
        });

        var groupItems = Ext.create('Ext.container.Container', {
            width: 620,
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formButtons = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.buildButtons(),
        });

        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {},
                items: [
                    formPanel,
                    groupItems,
                    formButtons
                ]
            }
        ];
        newUserForm.callParent();
    },
    buildGroupItems: function () {

        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    type: 'string',
                    dataIndex: 'firstName'
                }
            ]
        };
        return Ext.create('Ext.Panel', {
            width: 610,
            //            height: 500,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'panel',
                flex: 3,
                items: [
                        Ext.create('Ext.grid.Panel', {
                        id: 'UnAssinedUsers',
                        height: 500,
                        title: 'Unassined Users',
                        forceFit: true,
                        //						store: '',
                        columns: [
                            {
                                text: 'Users',
                                dataIndex: 'userId',
                            },
                            {
                                text: 'Department Type',
                                dataIndex: 'bisTypeDesc'
                            }
                            ],
                        features: [
                            {
                                ftype: 'filters',
                                autoReload: false, //don't reload automatically
                                local: true, //only filter locally
                                // filters may be configured through the plugin,
                                // or in the column definition within the headers configuration
                                filters: [
                                    {
                                        type: 'string',
                                        dataIndex: 'userId'
                                },
                                    {
                                        type: 'string',
                                        dataIndex: 'bisTypeDesc'
                                }
                            ]
                        }
                    ]
                    })
                    ]
                }, {
                xtype: 'panel',
                flex: 1,
                layout: {
                    type: 'vbox',
                    align: 'center',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: '>',
                        tooltip: 'Assign one user',
                        action: 'nextOne',
                        width: '50%',
                        margin: '5'
                        },
                    {
                        xtype: 'button',
                        text: '>>',
                        tooltip: 'Assign all users',
                        action: 'nextAll',
                        width: '50%',
                        margin: '5'
                        },
                    {
                        xtype: 'button',
                        text: '<<',
                        action: 'prevAll',
                        tooltip: 'Unassign all users',
                        width: '50%',
                        margin: '5'
                        }, {
                        xtype: 'button',
                        text: '<',
                        tooltip: 'Unassign one user',
                        action: 'prevOne',
                        width: '50%',
                        margin: '5'
                        }
                    ]
                }, {
                xtype: 'panel',
                flex: 3,
                items: [
                        Ext.create('Ext.grid.Panel', {
                        id: 'AsssignedUsers', //Do not duplicate the name
                        //                            store:'UserToBussiness',
                        height: 500,
                        title: 'Assigned Users',
                        forceFit: true,
                        columns: [
                            {
                                text: 'Users',
                                dataIndex: 'userId',
                            },
                            {
                                text: 'Department Type',
                                dataIndex: 'bisTypeDesc'
                            }
                            ],
                        features: [
                            {
                                ftype: 'filters',
                                autoReload: false, //don't reload automatically
                                local: true, //only filter locally
                                // filters may be configured through the plugin,
                                // or in the column definition within the headers configuration
                                filters: [
                                    {
                                        type: 'string',
                                        dataIndex: 'userId'
                                },
                                    {
                                        type: 'string',
                                        dataIndex: 'bisTypeDesc'
                                }
                            ]
                        }
                    ]
                    })
                    ]
                }]
        });
    },
    buildItems: function () {},
    buildButtons: function () {
        return [
            {
                //              iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            },
 //            {
 //                xtype: 'button',
 //                text: 'Save',
 //                action: 'save',
 //                id: 'userAssignGridForm-save',
 //                formBind: true
 //            },
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                id: 'create',
                formBind: true,
            }

        ];
    },
    //	buildDockedItems: function () {
    //		return [
    //			{
    //				xtype: 'toolbar',
    //				dock: 'bottom',
    //				items: ['->',
    //					{
    //						xtype: 'button',
    //						text: 'Cancel',
    //						action: 'cancel'
    //                    },
    // //                    {
    // //                        xtype: 'button',
    // //                        text: 'Save',
    // //                        action: 'save',
    // //                        id: 'userAssignGridForm-save',
    // //                        formBind: true
    // //                    },
    //					{
    //						xtype: 'button',
    //						text: 'Submit',
    //						action: 'onSubmit',
    //						id: 'create',
    //						formBind: true
    //                    }
    //                ]
    //            }
    //        ];
    //	},
    buildPlugins: function () {
        return [];
    }

});