Ext.define('singer.view.Events.CreateEvent.AwardDetailView', {
    extend: 'Ext.window.Window',
    alias: 'widget.awarddetailview',
    loadMask: true,
    //    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Award Detail View',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    //    height: 240,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            //            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        return [
            {
                fieldLabel: 'Place',
                name: 'palce'
            },
            {
                fieldLabel: 'Award',
                name: 'award'
            },
//            {
//                fieldLabel: 'Selected Candidate',
//                name: 'cateDesc'
//            },

        ];
    },
    buildButtons: function () {
        return ['->', {
            text: 'Done',
            action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});