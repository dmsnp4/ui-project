Ext.define('singer.view.Events.CreateEvent.NonSalesView', {
    extend: 'Ext.window.Window',
    alias: 'widget.nonsalesview',
    loadMask: true,
    //    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Non Sales Details',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    //    height: 240,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            //            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {
        return [
            {
                fieldLabel: 'Description',
                name: 'description'
            },
            {
                fieldLabel: 'Target',
                name: 'taget'
            },
            {
                fieldLabel: 'Archievement',
                name: 'point'
            },
            
        ];
    },
    buildButtons: function () {
        return ['->', {
            text: 'Done',
            action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});