/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.Events.CreateEvent.AwardDetail', {
    extend: 'Ext.window.Window',
    alias: 'widget.awarddetail',
    title: 'Award Details',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    //    height: 600,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            //            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        var typ = Ext.create('Ext.data.Store', {
            fields: ['type', 'value'],
            data: [
                {
                    "type": 1,
                    "value": "Qty"
                },
                {
                    "type": 2,
                    "value": "Value"
                }
            ]
        });
        return [
            {
                fieldLabel: 'Place',
                name: 'palce',
                allowBlank: false
            },
            {
                fieldLabel: 'Award',
                name: 'award',
                allowBlank: false
            },
 //            {
 //                fieldLabel: 'Selected Candidate',
 //                xtype: 'combobox',
 //                store: Ext.data.StoreManager.lookup('simpsonsStore'),
 //                displayField: 'name',
 //                valueField: 'name'
 //            }
        ];
    },
    buildButtons: function () {
        return [

            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
                //                id: 'userform-reset-insert'
            }, {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Submit',
                action: 'onSubmit',
                id: 'awarddetail-create',
                formBind: true
                //                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Save',
                //                        ui: 'success',
                action: 'save',
                id: 'awarddetail-save',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});