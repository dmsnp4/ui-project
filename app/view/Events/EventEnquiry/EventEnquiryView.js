Ext.define('singer.view.Events.EventEnquiry.EventEnquiryView', {
    extend: 'Ext.window.Window',
    alias: 'widget.eventenquiryview',
    loadMask: true,
    modal: true,
    title: 'Add New Event',
    layout: 'fit',
    constrain: true,
    width: '86%',
    height: '86%',
    renderTo: Ext.getBody(),
    autoScroll: true,
    stores: [
        'EventInquireDetails'
    ],
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            //width: 100,
            bodyPadding: 15,
            overflowY: 'auto',
            overflowX: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                //labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });

        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function () {

    },
    buildItems: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Active"
                },
                {
                    "status": 2,
                    "value": "Inactive"
                }
            ]
        });
        return [
            {xtype: 'fieldset',
                title: 'Sales',
                collapsible: true,
                defaultType: 'textfield',
                items: [
            {
                fieldLabel: 'Event Name',
                //                name: 'Event Name',
                name:'eventName'
            },
            {
                fieldLabel: 'Description',
                //                name: 'cateName',
                name: 'eventDesc',
            },
            {
                //xtype: 'datefield',
                //                anchor: '100%',
                fieldLabel: 'Start Date',
                //                name: 'from_date',
               // maxValue: new Date(),
                name:'startDate',
                //format: 'Y m d',
            },
            {
               // xtype: 'datefield',
                fieldLabel: 'End Date',
                //format: 'd m Y',
                name: 'endDate'
            },
//            {
//                fieldLabel: 'Participants',
//                name: 'participants'
//            },
//            {
//                fieldLabel: 'Rules',
//                name: 'rules'
//            },
            {
                fieldLabel: 'Event Threshold',
                name: 'threshold'
            },
            {
                fieldLabel: 'Status',
                name: 'status'
            },
            {
                fieldLabel: 'Comments from Event Manager',
                name: 'eventMrgComment'
            },
            {
                fieldLabel: 'Comments from Marketing Manager/Brand Manager',
                name: 'mktMrgComment'
            },]},
    
            {xtype: 'fieldset',
                title: 'Sales',
                collapsible: true,
                defaultType: 'textfield',
                items: [
    
//            {
//                xtype: 'label',
//                forId: 'myFieldId',
//                text: 'Rules:',
//                margin: '25 0 15 0',
//                style: 'display:block'
//            },
//            {
//                xtype: 'label',
//                forId: 'myFieldId',
//                text: 'Sales',
//                margin: '20 0 10 20',
//                style: 'display:block;border-bottom: 1px dotted #ccc;padding-bottom: 5px;'
//            },
            {
                xtype: 'panel',
                //autoScroll:true,
                items: [
                    {
                        xtype: 'grid',
                        
                        overflowY: 'auto',
                        height:'50%',
                        id:'EventEnqDetailGrid',
                        //store: Ext.data.StoreManager.lookup('EventInquireDetails'),
                        store:'EventInquireDetails',
                        columns: [
                            {
                                menuDisabled:true,
                                text: 'Product Family',
                                dataIndex: 'productFamily',
                                //width:'100'
                            },
                            {
                                menuDisabled:true,
                                text: 'Brand',
                                dataIndex: 'barnd',
                                //width:'80'
                            },
                            {
                                menuDisabled:true,
                                text: 'Model',
                                dataIndex: 'modleDesc',
                                //width:'120'
                            },
                            {
                                menuDisabled:true,
                                text: 'Period',
                                dataIndex: 'period',
                                //width:'80'
                            },
                            {
                                menuDisabled:true,
                                text: 'No of Intervals',
                                dataIndex: 'intervals',
                                //width:'120'
                            },
                            {
                                menuDisabled:true,
                                text: 'Accepted No of Intervals',
                                dataIndex: 'accept_intervals',
                                //width:'120'
                            },
                            {
                                menuDisabled:true,
                                text: 'Salse',
                                dataIndex: 'salse',
                                //width:'80'
                                
                            },
                            {menuDisabled:true,
                                text: 'Points',
                                dataIndex: 'points',
                                width:'10'
                            },
                            {
                                menuDisabled:true,
                                text: 'Point Schema-Fixed',
                                dataIndex: 'points_fixed',
                                //width:'120'
                            },
                            {
                                menuDisabled:true,
                                text: 'Float-Increment',
                                dataIndex: 'float_Inc',
                                //width:'100'
                            },
                            {
                                menuDisabled:true,
                                text: 'Float-Points',
                                dataIndex: 'float_points',
                                //width:'100'
                            },
                            {
                                menuDisabled:true,
                                text: 'Min Requirement',
                                dataIndex: 'min_req',
                                //width:'100'
                            },
                            {
                                menuDisabled:true,
                                text: 'Total Achievement',
                                dataIndex: 'tot_achieve',
                                //width:'120'
                            }
                        ]
                    },
                ]
            },
                ]},
            {xtype: 'fieldset',
                title: 'Description',
                collapsible: true,
                defaultType: 'textfield',
                items: [
//            {
//                xtype: 'label',
//                forId: 'myFieldId',
//                text: 'Non-Sales',
//                margin: '25 0 10 20',
//                style: 'display:block;border-bottom: 1px dotted #ccc;padding-bottom: 5px;'
//            },
            {
                xtype: 'grid',
                store:'eventNonSalesRuleList',
                id:'EvntEnqNonSaleGrid',
                //store: Ext.data.StoreManager.lookup('simpsonsStore'),
                columns: [
                    {
                        menuDisabled:true,
                        text: 'Description',
                        dataIndex: 'description',
                        flex:2
                    },
                    {
                        menuDisabled:true,
                        text: 'Target',
                        dataIndex: 'taget',
                        flex:1
                        //                        flex: 1
                    },
//                    {
//                        menuDisabled:true,
//                        text: 'Acheivement',
//                        dataIndex: 'phone',
//                        flex:1
//                    },

                ],
            },]},
            {xtype: 'fieldset',
                title: 'Place',
                collapsible: true,
                defaultType: 'textfield',
                items: [
    
//            {
//                xtype: 'label',
//                forId: 'myFieldId',
//                text: 'Awards',
//                margin: '25 0 10 20',
//                style: 'display:block;border-bottom: 1px dotted #ccc;padding-bottom: 5px;'
//            },
            
            {
                store:'evenAwardList',
                xtype: 'grid',
                id:'EvntEnqAwrdGrid',
                //store: Ext.data.StoreManager.lookup('simpsonsStore'),
                columns: [
                    {
                        menuDisabled:true,
                        text: 'Place',
                        dataIndex: 'palce',
                        flex:2
                    },
                    {
                        menuDisabled:true,
                        text: 'Award',
                        dataIndex: 'award',
                        flex: 1
                    },
                    {
                        menuDisabled:true,
                        text: 'Selected Candidate',
                        dataIndex: 'phone',
                        flex:1
                    },

                ],
            },
                ]},


            //            {
            //                fieldLabel: 'Status',
            //                xtype: 'combobox',
            //                name: 'cateStatus',
            //                store: stat,
            //                //                maxLength: 2,
            //                //                id: 'cateStatus',
            //                queryMode: 'local',
            //                displayField: 'value',
            //                editable: false,
            //                valueField: 'status',
            //                listeners: {
            //                    scope: this,
            //                    beforeRender: function (me) {
            //                        var val = me.getValue();
            //                        var cmp = Ext.getCmp('newcategory-remark');
            //                        if (val === null) {
            //                            me.setValue(1);
            //                            cmp.setValue('empty');
            //                        }
            //                        //                        var cmp = Ext.getCmp('newcategory-remark');
            //                        cmp.setVisible(false);
            //
            //                    },
            //
            //                    change: function (me) {
            //                        var cmp = Ext.getCmp('newcategory-remark');
            //                        cmp.setValue("");
            //                        cmp.setVisible(true);
            //
            //                    }
            //                }
            //            }, {
            //                fieldLabel: 'Remark',
            //                name: 'remarks',
            //                id: 'newcategory-remark',
            //                maxLength: 100,
            //
            //                allowBlank: false,
            //
            //                listeners: {
            //                    scope: this,
            //                    beforeRender: function (me) {
            //                        var valr = me.getValue();
            //                        var cmpp = Ext.getCmp('newcategory-remark');
            //
            //                        if (valr === "") {
            //                            cmpp.setValue('empty');
            //                        }
            //
            //                    }
            //
            //                }
            //
            //            }
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Done',
                action: 'cancel'
            }, 
//            {
//                xtype: 'button',
//                text: 'Save',
//                action: 'save',
//                id: 'neweventform-save',
//                formBind: true
//            },
//            {
//                xtype: 'button',
//                text: 'Create',
//                action: 'create',
//                id: 'neweventform-create',
//                formBind: true
//            }
        ];
    },
    //    buildDockedItems: function () {
    //        return [
    //            {
    //                xtype: 'toolbar',
    //                dock: 'bottom',
    //                items: ['->',
    //                    {
    //                        xtype: 'button',
    //                        text: 'Cancel',
    //                        action: 'cancel'
    //
    //                    }, {
    //
    //                        xtype: 'button',
    //                        text: 'Save',
    //                        action: 'save',
    //                        id: 'neweventform-save',
    //                        formBind: true
    //
    //                    },
    //                    {
    //                        xtype: 'button',
    //                        text: 'Create',
    //
    //                        action: 'create',
    //                        id: 'neweventform-create',
    //                        formBind: true
    //                    }
    //                ]
    //            }
    //        ];
    //    },

});