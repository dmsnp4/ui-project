Ext.define('singer.view.Events.EventEnquiry.EventEnquiryGrid', {
   extend: 'Ext.grid.Panel',
    alias: 'widget.eventenquirygrid',
    store: 'EventInquireList',
    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
    
    initComponent: function() {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },
    
    buildColumns: function() {
        return [
             {
                xtype: 'searchablecolumn',
                text: 'Event ID',
                dataIndex: 'eventId',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Event Name',
                dataIndex: 'eventName',
                flex: 2
            },
//            {
//                xtype: 'searchablecolumn',
//                text: 'Participants Type',
//                dataIndex: 'type',
//                flex: 3
//            },
            {
                xtype: 'searchablecolumn',
                text: 'Start Date',
                dataIndex: 'startDate',
                flex: 2
            },  {
                xtype: 'searchablecolumn',
                text: 'End Date',
                dataIndex: 'endDate',
                flex: 2
            },
            //              int eventId,
//             String eventName,
//             String eventDesc,
//             String startDate,
//             String endDate,
//             String threshold,
//             String eventAchievement,
//             String order,
//             String type,
            
//            {
//                xtype: 'searchablecolumn',
//                header: 'Rules',
//                 dataIndex: '',
//                flex: 2
//            }, 
            {
                xtype: 'searchablecolumn',
                header: 'Threshold',
                 dataIndex: 'threshold',
                flex: 2
            },{
                xtype: 'searchablecolumn',
                header: 'Achievement',
                dataIndex: 'eventAchievement',
                flex: 2,
                //type: 'Numeric',
                fields: ['phone'],
                position: 'left',
                label: {
                        renderer: function(v) {
                           return ((v * 100).toFixed(0)).concat('%');
                                         }
                                   }
                
            }, {
                xtype: 'searchablecolumn',
                header: 'Status',
                dataIndex: 'status',
                flex: 2,
                cls:'stat',
                renderer: function (value, meta) {
                    if (value === 1) {
                        meta.tdCls = 'actCell';
                        return "Active";
                    }
                    if (value === 0) {
                        meta.tdCls = 'incCell';
                        return "Inactive";
                    }

                }
            }, 
            {
                xtype: 'actioncolumn',
                header: 'View',
                menuDisabled: true,
                width: 60,
                align: 'center',
                items: [{
                    icon: 'resources/images/view-icon.png',
                    tooltip: 'View',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('eventView', grid, rec);
                    }
                    }]
            },
            {
                xtype: 'actioncolumn',
                header: 'Participants',
                menuDisabled: true,
                width: 100,
                align: 'center',
                items: [{
                    icon: 'resources/images/participants.png',
                    tooltip: 'Participants',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('ParticipantsView', grid, rec);
                    },
                    
                    }]
            }

        ];
    },
    
    buildDockedItems: function() {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },

                buttons: [
                   {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }]
            }, {
                xtype: 'pagingtoolbar',
                store: 'EventInquireList',
                dock: 'bottom',
                displayInfo: true
            }];
    },
//    buildPlugins: function() {
//        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
//            clicksToMoveEditor: 1,
//            autoCancel: false
//        });
//        return [rowEditing];
//    }
});


