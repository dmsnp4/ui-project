Ext.define('singer.view.Events.EventEnquiry.EventEnquiryPartiView', {
    extend: 'Ext.window.Window',
    alias: 'widget.eventenquirypartiview',
    loadMask: true,
    modal: true,
    title: 'Add New Event',
    layout: 'fit',
    constrain: true,
    width: '86%',
    height: '86%',
    renderTo: Ext.getBody(),
    autoScroll: true,
    stores: [
        'EventInquireDetails'
    ],
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 800,
            bodyPadding: 15,
            overflowY: 'auto',
            overflowX: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });

        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function () {

    },
    buildItems: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Active"
                },
                {
                    "status": 2,
                    "value": "Inactive"
                }
            ]
        });
        return [
            {
                fieldLabel: 'Participant',
                name:'eventName'
            },
            {
                fieldLabel: 'Location',
                name: 'eventDesc'
            },
            {
                fieldLabel: 'Participant Type',
                name:'startDate'
            },
            {
                fieldLabel: 'Current Achievement',
                name: 'endDate'
            },
            {
                xtype: 'label',
                forId: 'myFieldId',
                text: 'Sales Rules',
                margin: '25 0 10 20',
                style: 'display:block;border-bottom: 1px dotted #ccc;padding-bottom: 5px; font-weight: bold'
            },
            
            {
                xtype: 'panel',
                //autoScroll:true,
                items: [
                    {
                        xtype: 'grid',
                        
                        overflowY: 'auto',
                        height:'50%',
                        id:'EventEnqDetailGrid',
                        //store: Ext.data.StoreManager.lookup('EventInquireDetails'),
                        store:'EventInquireDetails',
                        columns: [
                            {
                                menuDisabled:true,
                                text: 'Product Family',
                                dataIndex: 'productFamily',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Brand',
                                dataIndex: 'barnd',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Model',
                                dataIndex: 'modleDesc',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Period',
                                dataIndex: 'period',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'No of Intervals',
                                dataIndex: 'intervals',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Accepted No of Intervals',
                                dataIndex: 'accept_intervals',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Salse',
                                dataIndex: 'salse',
                                flex:1
                                
                            },
                            {menuDisabled:true,
                                text: 'Points',
                                dataIndex: 'points',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Point Schema-Fixed',
                                dataIndex: 'points_fixed',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Float-Increment',
                                dataIndex: 'float_Inc',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Float-Points',
                                dataIndex: 'float_points',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Min Requirement',
                                dataIndex: 'min_req',
                                flex:1
                            },
                            {
                                menuDisabled:true,
                                text: 'Total Achievement',
                                dataIndex: 'tot_achieve',
                                flex:1
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'label',
                forId: 'myFieldId',
                text: 'Non-Sales Rules',
                margin: '25 0 10 20',
                style: 'display:block;border-bottom: 1px dotted #ccc;padding-bottom: 5px; font-weight: bold;'
            },
            {
                xtype: 'grid',
                store:'eventNonSalesRuleList',
                id:'EvntEnqNonSaleGrid',
                //store: Ext.data.StoreManager.lookup('simpsonsStore'),
                columns: [
                    {
                        menuDisabled:true,
                        text: 'Description',
                        dataIndex: 'description',
                        flex:1
                    },
                    {
                        menuDisabled:true,
                        text: 'Target',
                        dataIndex: 'taget',
                        flex:1
                        //                        flex: 1
                    },
                    {
                        menuDisabled: true,
                        text: 'Total Achievement',
                        dataIndex: 'tot_achieve',
                        flex:1
                    }

                ]
            }
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Done',
                action: 'cancel'
            }
        ];
    }

});