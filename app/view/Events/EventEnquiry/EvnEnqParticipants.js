
Ext.define('singer.view.Events.EventEnquiry.EvnEnqParticipants', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.evnenqparticipants',
//    title: 'Assing Users',
    height: 300,
    width: 700,
    layout: 'fit',
    store: 'EventInquirePart',
//    forceFit: true,
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'pagingtoolbar',
                store: 'RepairLevels',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildColumns: function () {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {"name": 1, "value": "Active"},
                {"name": 2, "value": "Inactive"}
            ]
        });
        return [
            {
                xtype: 'searchablecolumn',
                text: 'Participant',
                dataIndex: 'paticipantName',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Location',
                dataIndex: 'location',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Participant Type',
                dataIndex: 'paticipantType',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Current Achivement(points)',
                dataIndex: 'achivePoint',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Current Place',
                dataIndex: 'posotion',
                flex:2
            }, 
            {
                xtype: 'actioncolumn',
                text: 'View',
                menuDisabled: true,
                flex:1,
                align: 'center',
                items: [{
                    icon: 'resources/images/view-icon.png',
                    tooltip: 'View',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.up('grid').fireEvent('eventView', grid, rec);
                    }
                    }]
            }

        ];
    }


});




