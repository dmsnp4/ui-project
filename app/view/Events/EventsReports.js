

Ext.define('singer.view.Events.EventsReports', {
    extend: 'Ext.container.Container',
    alias: 'widget.eventMReports',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    overflowY: 'auto',
    initComponent: function() {
        var inquiryCmp = this;
        inquiryCmp.items = [
                     this.buildMasterReportsContainer(),
        ];
        inquiryCmp.callParent();
    },
    buildMasterReportsContainer: function() {
        
        return Ext.create('Ext.panel.Panel',
        {
            title: "Event Reports",
            layout: {type: 'vbox', align: 'stretch', pack: 'start'},
            
            items: [{
                    xtype: 'form',
                    flex: 1,
                    padding: '0',
                    overflowY: 'auto',
                    border: false,
                    items: [

                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            defaults: {
                                margin: '30 20 30 200',
                                labelWidth: 150,
                                width: 380
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    text:'Ongoing Events Report',                                    
                                },
                                {
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'Ongoing'
                                }]
                        },
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            defaults: {
                                margin: '30 20 600 200',
                                labelWidth: 150,
                                width: 380
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    text:'Completed Event Report',
                                },
                                {
                                    flex: 1,
                                    maxWidth: 100,
                                    xtype: 'button',
                                    text: 'Generate',
                                    action: 'cmpleted'
                                }]
                        },

                    ]
                }
                 ]
        });
    }


});
