Ext.define('singer.view.Events.AssignMarks.AssignMarksView', {
    extend: 'Ext.window.Window',
    alias: 'widget.assignMarksView',
    layout: 'fit',
    autoScroll: true,
    modal: true,
    constrain: true,
    loadMask: true,
    width: "80%",
    height: "80%",
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.grid.*'
    ],
    initComponent: function() {
        var assignMarksView = this;
        var gridPanel = Ext.create('Ext.grid.Panel', {
            forceFit: true,
            minHeight: 200,
            selType: 'rowmodel',
            store: 'EventMarks',
            columns: assignMarksView.buildColumns(),
            dockedItems : assignMarksView.buildDockedItems()
        });
        assignMarksView.items = [gridPanel];
        assignMarksView.callParent();
    },
    buildColumns: function() {
        return [
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Points Assigned',
                dataIndex: 'nonSalePoint'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Assigned To',
                dataIndex: 'assignTo'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Assignee',
                dataIndex: 'assignBy'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Date',
                dataIndex: 'pointAddDate'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Status',
                dataIndex: 'statusMsg'
            },
            {
                flex: 1,
                xtype: 'actioncolumn',
                header: 'View',
                width: 100,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'View',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('window').fireEvent('open_view_marks_win', grid, record);
                        }
                    }]
            },
            {
                flex: 1,
                xtype: 'actioncolumn',
                header: 'Edit',
                width: 100,
                items: [{
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('window').fireEvent('open_edit_marks_win', grid, record);
                        }
                    }]
            }

        ];
    },
    buildDockedItems: function() {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                items : [
                    {
                        xtype : "hiddenfield",
                        name : 'eventId'
                    }
                ],
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    },
                    '->'
                    ,
                    {
                        text: 'Assign Points',
                        action: 'open_assign_points_form'
                    }

                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'EventMarks',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});







