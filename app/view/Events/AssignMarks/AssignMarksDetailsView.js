Ext.define('singer.view.Events.AssignMarks.AssignMarksDetailsView', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.form.*'
    ],
    alias: 'widget.assignMarksDetailsView',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: "50%",
    height: "70%",
    initComponent: function() {
        var assignMarksWin = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            height: 550,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: assignMarksWin.buildItems(),
            buttons: assignMarksWin.buildButtons()
        });
        assignMarksWin.items = [formPanel];
        assignMarksWin.callParent();

    },
    buildItems: function() {
        return [
            {
                fieldLabel: 'Assignee',
                name: 'assignBy'
            },
            {
                fieldLabel: 'Assign to',
                name: 'assignTo'
            },
            {
                fieldLabel: 'Points Assigned',
                name: 'nonSalePoint'
            },
            {
                fieldLabel: 'Date',
                name: 'pointAddDate'
            },
            {
                fieldLabel: 'Status',
                name: 'statusMsg'
            },
            {
                fieldLabel: 'Remarks',
                name: 'remarks'
            }
        ];
    },
    buildButtons: function() {
        return [ '->', {
                text: 'Close',
                action: 'close'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});
