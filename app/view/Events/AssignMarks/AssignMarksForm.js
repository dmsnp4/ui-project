Ext.define('singer.view.Events.AssignMarks.AssignMarksForm', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.form.*'
    ],
    alias: 'widget.assignMarksForm',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: "70%",
    height: "80%",
    initComponent: function() {
        var assignMarksWin = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            height: 550,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: assignMarksWin.buildItems(),
            buttons: assignMarksWin.buildButtons()
        });
        assignMarksWin.items = [formPanel];
        assignMarksWin.callParent();

    },
    buildItems: function() {
        return [
            {
                xtype: 'displayfield',
                fieldLabel: 'Assignee',
                name: 'assignBy',
                afterLabelTextTpl: ''
            },
            {
                fieldLabel: 'Assign to',
                name: 'assignTo',
                xtype: 'combobox',
                store: 'AssignMarksParticipants',
                queryMode: 'local',
                displayField: 'userId',
                valueField: 'userId',
                listeners: {
                    change: function(combo, newValue) {
                        if (Ext.getCmp("assignMarksForm-save").isVisible() === false) {
                            this.up('window').fireEvent('participant_select_change', combo, newValue);
                        }
                    }
                }
            },
            {
                xtype: 'label',
                text: 'Nonsale Rules',
                style: "float: left; margin: 0px 0px 10px 0px;"
            },
            {
                margin: '0 0 30 0',
                xtype: 'grid',
                store: 'NonSaleEventRules',
                overflowY: 'auto',
                frame: false,
                border: true,
                height: 150,
                columns: [
                    {
                        text: 'Description',
                        dataIndex: 'description',
                        flex: 4
                    },
                    {
                        text: 'Target',
                        dataIndex: 'taget',
                        flex: 4
                    },
                    {
                        text: 'Achievement',
                        dataIndex: 'point',
                        flex: 2
                    },
                    {
                        text: 'Assign',
                        dataIndex: 'status',
                        xtype: 'checkcolumn',
                        width: 80,
                        listeners: {
                            checkchange: function(column, rowIndex, checked, eOpts) {
                                var record = (this.up('grid').getStore().getAt(rowIndex));
                                this.up('window').fireEvent('check_change_rules', checked, record, rowIndex);
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'displayfield',
                name: 'totalPoints',
                fieldLabel: 'Total Assigned Points',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue()) === true){
                            comp.setValue("<strong style='font-size: 20px;'>0</strong>");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>"+comp.getValue()+"</strong>");
                        }
                    }
                }
            },
            {
                xtype: 'datefield',
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                name: 'pointAddDate',
                fieldLabel: 'Date'
            },
            {
                fieldLabel: 'Status',
                name: 'status',
                xtype: 'combobox',
                store: Ext.create('Ext.data.Store', {
                    fields: ['statusId', 'statusMsg'],
                    data: [
                        {"statusId": 1, "statusMsg": "Active"},
                        {"statusId": 9, "statusMsg": "Inactive"}
                    ]
                }),
                queryMode: 'local',
                displayField: 'statusMsg',
                valueField: 'statusId'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Remarks',
                name: 'remarks'
            },
            {
                xtype: 'hiddenfield',
                name: 'txnId'
            },
            {
                xtype: 'hiddenfield',
                name: 'eventId'
            },
            {
                xtype: 'hiddenfield',
                name: 'userId'
            },
            {
                xtype: 'hiddenfield',
                name: 'ruleId'
            },
            {
                xtype: 'hiddenfield',
                name: 'salePoint'
            },
            {
                xtype: 'hiddenfield',
                name: 'nonSaleId'
            },
            {
                xtype: 'hiddenfield',
                name: 'nonSalePoint'
            },
            {
                xtype: 'hiddenfield',
                name: 'assignId'
            },
            {
                xtype: 'hiddenfield',
                name: 'statusMsg'
            },
            {
                xtype: 'hiddenfield',
                name: 'salesType'
            }
        ];
    },
    buildButtons: function() {
        return [{
                text: 'Cancel',
                action: 'cancel'
            }, '->', {
                text: 'Create',
                action: 'create',
                id: 'assignMarksForm-create',
                formBind: true
            }, {
                text: 'Save',
                action: 'save',
                id: 'assignMarksForm-save',
                formBind: true
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});
