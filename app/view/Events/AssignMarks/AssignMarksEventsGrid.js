Ext.define('singer.view.Events.AssignMarks.AssignMarksEventsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.assignMarksEventsGrid',
    requires: [
        'Ext.grid.*'
    ],
    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
    store: 'AssignMarksEvents',
    initComponent: function() {
        var assignMarksEventsGrid = this;
        assignMarksEventsGrid.columns = assignMarksEventsGrid.buildColumns();
        assignMarksEventsGrid.dockedItems = assignMarksEventsGrid.buildDockedItems();
        assignMarksEventsGrid.callParent();
    },
    buildColumns: function() {
        return [
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Event ID',
                dataIndex: 'eventId'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Event Name',
                dataIndex: 'eventName'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Start Date',
                dataIndex: 'startDate'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'End Date',
                dataIndex: 'endDate'
            },
//            {
//                flex: 1,
//                xtype: 'searchablecolumn',
//                header: 'Rules',
//                dataIndex: 'eventSalesRuleList',
//                renderer: function(value) {
//                    if (value === "" || value === null) {
//                        return "-";
//                    }else{
//                        return value;
//                    }
//                }
//            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Threshold',
                dataIndex: 'threshold'
            },
            {
                flex: 1,
                xtype: 'searchablecolumn',
                header: 'Status',
                dataIndex: 'statusMsg',
                renderer: function(value) {
                    if (value === "" || value === null) {
                        return "-";
                    }else{
                        return value;
                    }
                }
            },
            {
                flex: 1,
                xtype: 'actioncolumn',
                header: 'Assign Marks',
                width: 100,
                items: [{
                        icon: 'resources/images/approved.png', // Use a URL in the icon config
                        tooltip: 'Assign Marks',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('assign_marks_view', grid, record);
                        }
                    }]
            }

        ];
    },
    buildDockedItems: function() {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearSearch'
                    }

                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'AssignMarksEvents',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});