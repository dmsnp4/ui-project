
Ext.define('singer.view.DeviceIssueModule.ReplacementReturn.ReplacementReturnGrid', {
   extend: 'Ext.grid.Panel',
    alias: 'widget.replacmentReturnGrid',
    store: 'ReplacementReturnStore',
    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
    
    initComponent: function() {
        var rrGrid = this;
        rrGrid.columns = rrGrid.buildColumns();
        rrGrid.dockedItems = rrGrid.buildDockedItems();
        rrGrid.callParent();
    },
    
    buildColumns: function() {
        return [
            {
                xtype: 'searchablecolumn',
                header: 'Replacement Return No',
                dataIndex: 'replRetutnNo',
                flex: 1
            },
            {
                xtype: 'searchablecolumn',
                header: 'Exchange Refference No ',
                dataIndex: 'exchangeReferenceNo',
                flex: 1
            }
            ,
            {
                xtype: 'searchablecolumn',
                header: 'Old IMEI No',
                dataIndex:'imeiNo',
                flex: 1
            },
//            {
//                xtype: 'searchablecolumn',
//                header: 'New IMEI No',
//                dataIndex:'',
//                flex: 1
//            },
            {
                xtype: 'searchablecolumn',
                header: 'Model',
                dataIndex:'modleDesc',
                flex: 1
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Date',
                dataIndex: 'returnDate',
                flex: 1
            }
            
            ,{
                xtype: 'actioncolumn',
                header: 'Delete',
                align:'center',
                width: 100,
                items: [{
                        icon: 'resources/images/delete-icon.png', // Use a URL in the icon config
                        tooltip: 'Delete',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('del', grid, record);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                        if (record.get('status') === 3 ) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
//            {
//                xtype: 'actioncolumn',
//                header: 'Edit',
//                align:'center',
//                width: 100,
//                items: [{
//                        icon: 'resources/images/edit.png', // Use a URL in the icon config
//                        tooltip: 'Edit',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('editRR', grid, record);
////                            //console.log(this.up('grid'));
//                        }
//                    }]
//            }
            {
                xtype: 'actioncolumn',
                header: 'Authorize',
                items: [{
                        icon: 'resources/images/approved.png', // Use a URL in the icon config
                        tooltip: 'Approve',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('aprove', grid, record);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                        if (record.get('status') === 3 ) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }]
            },
        ];
    },
    
    buildDockedItems: function() {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },

                buttons: [
                    {
                        text: 'Clear Search',
                        action: 'clearsearch'
                    }
                    ,'->',
                    
//                    {
//                        iconCls: 'icon-search',
//                        text: 'Add New',
//                        action: 'add_new',
//                       
//                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'ReplacementReturnStore',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});


