
Ext.define('singer.view.DeviceIssueModule.ReplacementReturn.EditForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.editform',
    layout: 'fit',
    constrain: true,
    modal: true,
    width: 500,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {

        return [
            {
              name:'distributerId',
              xtype:'hiddenfield'
            },
            {
                fieldLabel: 'Replacement & Return No',
                name: 'replRetutnNo',
                allowBlank: false
            }, {
                xtype:'combobox',
                fieldLabel: 'Exchange Reffrence No',
//                emptyText: 'Select Exchange Reffrence No',
                action:'editRefChange',
                store: 'exchangeRfnStore',
                queryMode: 'local',
                displayField: 'exchangeReferenceNo',
                editable: true,
                name: 'exchangeReferenceNo',
//                valueField: 'exchangeReferenceNo',
                valueField: 'exchangeReferenceNo',
                
                allowBlank: false,
                readOnly:true
            }, {
                readOnly:'true',
                fieldLabel: 'IMEI No',
                id:'editIME',
                name: 'imeiNo',
                allowBlank: false
            }, {
                fieldLabel: 'Date',
                name: 'returnDate',
                xtype: 'datefield',
                allowBlank: false,
                maxValue: new Date(),
                format: 'Y/m/d',
                altFormats: 'm/d/Y',
                editable: false
            }
           
        ];
    },
    
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel_edit'
            }, 
            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'editform-save',
                formBind: true
            },
            
            {
                xtype: 'button',
                text: 'Submit',
                action: 'create',
                id: 'editform-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});

