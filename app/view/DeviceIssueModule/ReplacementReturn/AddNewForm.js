

Ext.define('singer.view.DeviceIssueModule.ReplacementReturn.AddNewForm', {
    extend: 'Ext.form.Panel',
//    extend: 'Ext.window.Window',
    alias: 'widget.addNWform',
    modal: true,
    constrain: true,
    loadMask: true,
    layout: 'fit',
    width: 430,
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()

        });
        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
    },
    buildItems: function () {

        return [
            {
                name: 'distributerId',
                xtype: 'hiddenfield'
            },
            {
                
                fieldLabel: 'Exchange Refference No',
                xtype: 'combobox',
                action: 'refChange',
                allowBlank: false,
                name: 'exchangeReferenceNo',
                store: 'exchangeRfnStore',
                queryMode: 'local',
                displayField: 'exchangeReferenceNo',
                editable: true,
                valueField: 'exchangeReferenceNo',
                id:'exchangeRRnum',
            },
            {
                fieldLabel: 'Old IMEI No',
                readOnly: 'true',
                id: 'ime',
                name: 'imeiNo',
                allowBlank: false,
            },
            {
                fieldLabel: 'Work Order No',
                readOnly: 'true',
                id: 'ReplacemetReturn_ADD_New_wonum',
                name: 'workOrderNo',
                allowBlank: false,
            },
            {
                fieldLabel:'New IMEI No',
                allowBlank: false,
            },
            {
                width:100,
                margin:'0, 0, 30, 310',
                xtype: 'button',
                text: 'Scan',
                action: 'scan_New_imei',
            }
//           
        ];
    },
    buildButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Create',
                action: 'create',
                id: 'addnewform-create',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            },
            {
                xtype: 'button',
                maxWidth: 100,
                text: 'Print',
                action: 'printRR',
                formBind: true
            }

        ];
    },
    buildDockedItems: function () {

    },
    buildPlugins: function () {
        return [];
    }
});

