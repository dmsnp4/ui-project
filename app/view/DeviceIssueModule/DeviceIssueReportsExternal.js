
Ext.define('singer.view.DeviceIssueModule.DeviceIssueReportsExternal', {
    extend: 'Ext.container.Container',
    alias: 'widget.DeviceIssueReportsExternal',
    requires: ['Ext.layout.container.Column'],
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    overflowY: 'auto',
    initComponent: function () {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.buildMasterReportsContainer(),
        ];
        inquiryCmp.callParent();
    },
    buildMasterReportsContainer: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": '1', "value": "IN STOCK"},
                {"status": '2', "value": "OUT STOCK"}
            ]
        });

        return Ext.create('Ext.panel.Panel',
                {
                    title: "Device Issue Reports",
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    items: [{
                            xtype: 'form',
                            flex: 1,
                            padding: '0',
                            overflowY: 'auto',
                            border: false,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            margin: '20 10 20 200',
                                            xtype: 'datefield',
                                            fieldLabel: 'Start Date',
                                            id: 'startD',
                                            name: '',
                                            allowBlank: false,
                                            value: new Date(),
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            altFormats: 'm/d/Y',
                                            editable: false,
                                            listeners: {
                                                change: function (me, newValue, oldValue, eOpts) {
                                                    var s = newValue; //new Date();
                                                    var m = s.setMonth(s.getMonth() + 3);
                                                    console.log('AAAAAAAA: ', s);
                                                    if (new Date() > s) {
                                                        Ext.getCmp('endD').setValue(s);
                                                        Ext.getCmp('endD').setMaxValue(s);
                                                    } else {
                                                        Ext.getCmp('endD').setValue(new Date());
                                                        Ext.getCmp('endD').setMaxValue(new Date());
                                                    }


                                                }
                                            }
                                        },
                                        {
                                            margin: '20 10 20 200',
                                            xtype: 'datefield',
                                            fieldLabel: 'End Date',
                                            name: '',
                                            id: 'endD',
                                            allowBlank: false,
                                            value: new Date(),
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            altFormats: 'm/d/Y',
                                            editable: false
                                        },
                                        {
                                            margin: '20 10 20 200',
                                            width: 600,
                                            xtype: 'treecombo',
                                            fieldLabel: 'Select Location',
                                            store: 'ServiceUserType',
                                            treeStore: 'UserTypeListTree', //tree store is ok
                                            valueField: 'bisId',
                                            displayField: 'bisName',
                                            name: 'location_struct',
                                            id: 'location_struct_id'//sh, catch this @controller
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        postion : 'absolute',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            allowBlank: false,
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            action: 'showSalesReport'
                                        }
                                    ]}
                            ]
                        }
                    ]
                });
    }


});




function calcThreeMonthsRange() {
    var s = new Date();

    var m = s.setMonth(s.getMonth() + 3);

    return new Date(m);

}