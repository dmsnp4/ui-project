Ext.define('singer.view.DeviceIssueModule.DebitNote.Form', {
    extend: 'Ext.window.Window',
    alias: 'widget.debitnoteform',
//    title: 'Repair Levels - Category - 1',
    layout: 'fit',
    constrain: true,
    modal: true,
    width: 500,
//    height: 600,
    initComponent: function() {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function() {

        return [{
                fieldLabel: 'Serial No(IMEI)',
                name: 'levelCatId',
                visible: false,
            }, {
                fieldLabel: 'Customer Code',
                name: '',
                allowBlank: false
            },
            {
                fieldLabel: 'Shop Code',
                name: 'levelDesc',
////                maxLength: 19,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/
            }, 
            {
                fieldLabel: 'Model',
                xtype: 'combobox',
                name: '',
//                store: stat,
////                queryMode: 'local',
////                displayField: 'value',
////                valueField: 'status',
//                listeners: {
//                    scope: this,
//                    afterRender: function(me) {
//                        me.setValue(1);
//                    }
//                }
            },{
                fieldLabel: 'Site',
                name: '',
            },{
                fieldLabel: 'Order No',
                name: '',
            },{
                fieldLabel: 'Sales Price(Rs.)',
                name: '',
            },
        ];
    },
    buildButtons: function() {
        return [
//            , {
//                text: 'Cancel',
//                action: 'cancel'
//
//            }, {
//                text: 'Save',
//                action: 'save'
//
//            }

            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'debitnoteform-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Submit',
//                        ui: 'success',
                action: 'create',
                id: 'debitnoteform-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});

