


Ext.define('singer.view.DeviceIssueModule.ImportDebitNotes.DebitView', {
    extend: 'Ext.window.Window',
    alias: 'widget.debitVieww',
    iconCls: 'icon-user',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    
    initComponent: function() {
        var debitView = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: debitView.buildItems(),
            buttons: debitView.buildButtons()
        });
        debitView.items = [formPanel];
        debitView.callParent();
    },
    buildItems: function() {
        return [{
                fieldLabel: 'Debit Note No:',
                name: 'debitNoteNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Order No:',
                name: 'orderNo'
            },
            {
                fieldLabel: 'Invoice No:',
                name: 'invoiceNo'
            },{
                fieldLabel: 'Contact No:',
                name: 'contactNo'
            },
            {
                fieldLabel: 'Part No:',
                name: 'partNo'
            },
            {
                fieldLabel: 'Order Qty:',
                name: 'orderQty'
            },
            {
                fieldLabel: 'Delivered Qty:',
                name: 'deleveryQty'
            },
            {
                fieldLabel: 'Delivered Date:',
                name: 'deleveryDate'
            },
            {
                fieldLabel: 'Head Status:',
                name: 'headStatus'
            },
            {
                fieldLabel: 'Customer No:',
                name: 'customerNo'
            }
        ];
    },
    
    buildButtons: function() {
        return [ '->', {
                text: 'Done',
                action: 'cancel'
            }];
    },

    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});


