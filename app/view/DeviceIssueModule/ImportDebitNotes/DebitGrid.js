
Ext.define('singer.view.DeviceIssueModule.ImportDebitNotes.DebitGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.importDebitgrid',
    store: 'ImportDebitNotesStore',
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function () {
        var debitNoteGrid = this;
        debitNoteGrid.columns = debitNoteGrid.buildColumns();
        debitNoteGrid.dockedItems = debitNoteGrid.buildDockedItems();
        debitNoteGrid.callParent();
    },
    buildColumns: function () {
        return [
            {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Debit Note No',
                id: 'dbitNteNo',
                dataIndex: 'debitNoteNo'
            },
            {
                xtype: 'searchablecolumn',
                header: 'Order No',
                dataIndex: 'orderNo'
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Invoice No',
                dataIndex: 'invoiceNo'
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Contract No',
                dataIndex: 'contactNo'
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Part No',
                dataIndex: 'partNo'
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Order Qty',
                dataIndex: 'orderQty'
            }
            , {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Delivered Qty',
                dataIndex: 'deleveryQty'
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Deliver Date',
                dataIndex: 'deleveryDate'
            }
            , {
                xtype: 'searchablecolumn',
                header: 'Head Status',
                dataIndex: 'headStatus'
            }
            , {
                width: 150,
                xtype: 'searchablecolumn',
                header: 'Customer No',
                dataIndex: 'customerNo'
            }
            , {
                header: 'Details',
                xtype: 'actioncolumn',
                width: 100,
                align:'center',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/details.png', // Use a URL in the icon config
                        tooltip: 'Details',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('debitDetailsgrid', grid, record);
//                            //console.log(this.up('grid'));

                        }
                    }]
            }, {
                header: 'view',
                xtype: 'actioncolumn',
                width: 100,
                align:'center',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config
                        tooltip: 'view',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('debitView', grid, record);
//                            //console.log(this.up('grid'));
                        }
                    }]
            }

        ];
    },
    buildDockedItems: function () {
        return [{xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch'
                                //  id: 'suppliersgrid_search'
                    },'->',]
            }, {
                xtype: 'pagingtoolbar',
                store: 'ImportDebitNotesStore',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildPlugins: function () {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});