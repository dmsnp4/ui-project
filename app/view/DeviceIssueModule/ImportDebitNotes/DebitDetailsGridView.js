Ext.define('singer.view.DeviceIssueModule.ImportDebitNotes.DebitDetailsGridView', {
   extend: 'Ext.grid.Panel',
    alias: 'widget.debitDetailGridView',
    iconCls: 'icon-user',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 600,
    
    initComponent: function() {
        var debitView = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: debitView.buildItems(),
            buttons: debitView.buildButtons()
        });
        debitView.items = [formPanel];
        debitView.callParent();
    },
    buildColumns: function() {
         return [
            {
                header: 'Debit Note No',
                dataIndex: ''
            },
            {
                header: 'IMEI',
                dataIndex: ''
            }
            , {
                header: 'Customer Code',
                dataIndex:''
            }
            , {
                header: 'Shop Code',
                dataIndex: ''
            }
            ,{
                header: 'Model',
                dataIndex: ''
            }
            ,{
                header: 'Site',
                dataIndex: ''
            }
            ,{
                header: 'Order No',
                dataIndex: ''
            }
            ,{
                header: 'Sales Price(Rs.)',
                dataIndex: ''
            }
//            ,{
//                xtype: 'actioncolumn',
//                width: 50,
//                items: [{
//                        icon: 'resources/images/view-icon.png', 
//                        tooltip: 'Details',
//                        handler: function(grid, rowIndex, colIndex) {
//                            var record = (grid.getStore().getAt(rowIndex));
//                            this.up('grid').fireEvent('tranDeleteView', grid, record);
////                            //console.log(this.up('grid'));
//
//                        }
//                    }]
//            }
        ];
    },
    
    buildButtons: function() {
        return [ '->', {
                text: 'Done',
                action: 'cancel'
            }];
    },

    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});





