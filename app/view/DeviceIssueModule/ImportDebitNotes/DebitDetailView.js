



Ext.define('singer.view.DeviceIssueModule.ImportDebitNotes.DebitDetailView', {
    extend: 'Ext.window.Window',
    alias: 'widget.debitDetailView',
    iconCls: 'icon-user',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: '80%',
    
    initComponent: function() {
        var debitView = this;
        var formPanel = Ext.create('Ext.form.Panel', {
//            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: debitView.buildItems(),
            buttons: debitView.buildButtons()
        });
        debitView.items = [formPanel];
        debitView.callParent();
    },
    buildItems: function() {
        return [{
                fieldLabel: 'IMEI :',
                name: 'imeiNo'
            },
            {
                fieldLabel: 'Customer Code:',
                name: 'customerCode'
            },
            {
                fieldLabel: 'Shop Code:',
                name: 'shopCode'
            },
            {
                fieldLabel: 'Model:',
                name: 'modleNo'
            },
            {
                fieldLabel: 'Model Description:',
                name: 'modleDesc'
            },
            {
                fieldLabel: 'Site:',
                name: 'site'
            },
            {
                fieldLabel: 'Order No:',
                name: 'orderNo'
            },
            {
                fieldLabel: 'Sales Price:',
                name: 'salerPrice'
            }
        ];
    },
    
    buildButtons: function() {
        return [ '->', {
                text: 'Done',
                action: 'done'
            }];
    },

    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});


