
Ext.define('singer.view.DeviceIssueModule.ImportDebitNotes.DetailsDebitGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.importdebitdetailsGrid',
    height: 300,
    width: 1000,
    layout: 'fit',
    store: 'ImportDebitDetails',
    forceFit: true,
    initComponent: function () {
        var me = this;
        me.columns = me.buildColumns();
        me.dockedItems = me.buildDockItems();

        me.callParent();
    },
    buildDockItems: function (grid) {
        return [{
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    },'->',

                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'ImportDebitDetails',
                dock: 'bottom',
                displayInfo: true
            }];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'IMEI',
                dataIndex: 'imeiNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Customer Code',
                dataIndex: 'customerCode'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Shop Code',
                dataIndex: 'shopCode'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model',
                dataIndex: 'modleNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Model Description',
                dataIndex: 'modleDesc'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Site',
                dataIndex: 'site'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Order No',
                dataIndex: 'orderNo'
            },
            {
                xtype: 'searchablecolumn',
                text: 'Sales Price(Rs.)',
                align:'center',
                dataIndex: 'salerPrice',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'actioncolumn',
                header: 'View',
                width: 50,
                text: 'View',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config  
                        tooltip: 'View',
                        width: 50,
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('detailVieww', grid, rec);
                        }
                    }]
            }
        ];
    }


});