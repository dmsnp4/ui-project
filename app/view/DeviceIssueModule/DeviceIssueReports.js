
Ext.define('singer.view.DeviceIssueModule.DeviceIssueReports', {
    extend: 'Ext.container.Container',
    alias: 'widget.deviceIssueReports',
    requires: ['Ext.layout.container.Column'],
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    overflowY: 'auto',
    initComponent: function () {
        var inquiryCmp = this;
        inquiryCmp.items = [
            this.buildMasterReportsContainer(),
        ];
        inquiryCmp.callParent();
    },
    buildMasterReportsContainer: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": '1', "value": "IN STOCK"},
                {"status": '2', "value": "OUT STOCK"}
            ]
        });

        return Ext.create('Ext.panel.Panel',
                {
                    title: "Device Issue Reports",
                    layout: {type: 'vbox', align: 'stretch', pack: 'start'},
                    items: [{
                            xtype: 'form',
                            flex: 1,
                            padding: '0',
                            overflowY: 'auto',
                            border: false,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            margin: '20 10 20 200',
                                            xtype: 'datefield',
                                            fieldLabel: 'Start Date',
                                            id: 'startD',
                                            name: '',
                                            allowBlank: false,
                                            value: new Date(),
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            altFormats: 'm/d/Y',
                                            editable: false,
                                            listeners: {
                                                change: function (me, newValue, oldValue, eOpts) {
                                                    var s = newValue; //new Date();
                                                    var m = s.setMonth(s.getMonth() + 3);
                                                    console.log('AAAAAAAA: ', s);
                                                    if (new Date() > s) {
                                                        Ext.getCmp('endD').setValue(s);
                                                        Ext.getCmp('endD').setMaxValue(s);
                                                    } else {
                                                        Ext.getCmp('endD').setValue(new Date());
                                                        Ext.getCmp('endD').setMaxValue(new Date());
                                                    }


                                                }
                                            }
                                        },
                                        {
                                            margin: '20 10 20 200',
                                            xtype: 'datefield',
                                            fieldLabel: 'End Date',
                                            name: '',
                                            id: 'endD',
                                            allowBlank: false,
                                            value: new Date(),
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            altFormats: 'm/d/Y',
                                            editable: false
                                        },
                                        {
                                            margin: '20 10 20 200',
                                            width: 600,
                                            xtype: 'treecombo',
                                            fieldLabel: 'Select Location',
                                            store: 'ServiceUserType',
                                            treeStore: 'UserTypeListTree', //tree store is ok
                                            valueField: 'bisId',
                                            displayField: 'bisName',
                                            name: 'location_struct',
                                            id: 'location_struct_id'//sh, catch this @controller
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Balance Stock Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'showBalanceStockReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Balance Stock Report - Detail',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'showBalanceStockReportDtl'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Distributor Stock Report',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'showDistrubutorStockReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Distributor Stock Report - Detail',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'showDistrubutorStockReportDtl'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Dealer Stock Report',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'showDealerStockReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Dealer Stock Report - Detail',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'showDealerStockReportDtl'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report  - Area, Distributor, DSR wise',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'salesAreaDisReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report - Area, Distributor, DSR wise -Detail',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'salesAreaDisReportDtl'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report  - Area, Distributor, DSR wise and dealer wise',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'salesAreaDisDealerReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report - Area,Distributor,DSR wise and dealer wise - Detail',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'salesAreaDisDealerReportDtl'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Adjustment Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'show_adjustment_Report'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Credit Note Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'show_credit_note_Report'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Device Return Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'show_Device_return_Report'
                                        }
                                    ]
                                },
//                                {
//                                    xtype: 'fieldcontainer',
//                                    layout: 'hbox',
//                                    defaults: {
//                                        margin: '30 20 30 200',
//                                        labelWidth: 150,
//                                        width: 380
//                                    },
//                                    items: [
//                                        {
//                                            xtype: 'label',
//                                            text: 'Purchase Report',
//                                            name: '',
//                                        },
//                                        {
//                                            flex: 1,
//                                            maxWidth: 100,
//                                            xtype: 'button',
//                                            text: 'Generate',
//                                            tooltip: 'Before click on the GENERATE button, select specific date range',
//                                            action: 'showPurchaseReport'
//                                        }
//                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            allowBlank: false,
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            action: 'showSalesReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Distributor Stock Issues Report To DSR',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            allowBlank: false,
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            action: 'showDistIssueToDsr'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Sales Report - Summary',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            allowBlank: false,
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            action: 'showSalesReportSumry'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            margin: '30 0 30 200',
                                            xtype: 'label',
                                            text: 'Inventory At Transits Report',
                                            name: '',
                                        },
                                        {
                                            margin: '30 10 30 0',
                                            xtype: 'combo',
                                            id: 'typ_rep',
                                            width: 200,
                                            store: stat,
                                            queryMode: 'local',
                                            displayField: 'value',
                                            valueField: 'status'
                                        },
                                        {
                                            margin: '30 0 30 10',
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'ShowInventoryReport'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            margin: '30 432 30 200',
                                            xtype: 'label',
                                            text: 'Non Moving Inventory Report',
                                            name: '',
                                        },
//                                        {
//                                            margin: '30 151 30 0',
//                                            xtype: 'combo',
//                                            id: 'duration',
//                                            store: stat,
//                                            queryMode: 'local',
//                                            displayField: 'value',
//                                            editable: false,
//                                            valueField: 'status',
//                                        },
                                        {
                                            margin: '30 0 30 0',
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'showNonMovingReports'
                                        }
                                    ]},
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Outlet Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            text: 'Generate',
                                            action: 'showOutletReport'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Warrenty Registration Detail Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'show_warr_reg_Report'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'Inserted IMEI / Model NO Report',
                                            name: '',
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Before click on the GENERATE button, select specific date range',
                                            text: 'Generate',
                                            action: 'show_inserted_reg_Report'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaults: {
                                        margin: '30 20 30 200',
                                        labelWidth: 150,
                                        width: 380
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'IMEI Movement Report',
                                            name: ''
                                        },
                                        {
                                            flex: 1,
                                            maxWidth: 100,
                                            xtype: 'button',
                                            tooltip: 'Click here',
                                            text: 'Generate',
                                            action: 'show_imei_movement'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                });
    }


});










//Ext.define('singer.view.DeviceIssueModule.DeviceIssueReports', {
//    extend: 'Ext.container.Container',
//    alias: 'widget.deviceIssueReports',
//    layout: {
//        type: 'hbox',
//        style: {
//            padding: 20
//        }
//    },
//    items: [
//        {
//            xtype: 'container',
//            layout: 'vbox',
//            flex: 1,
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Balance Stock Report',
//                    height: 100,
//                    width: 100
//
//                }
//            ]
//        },
//        {
//            xtype: 'container',
//            layout: 'vbox',
//            flex: 1,
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Purchase Report',
//                    height: 100,
//                    width: 100
//
//                }
//            ]
//        },
//        {
//            xtype: 'container',
//            layout: 'vbox',
//            flex: 1,
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Sales Report',
//                    height: 100,
//                    width: 100
//
//                }
//            ]
//        },
//        {
//            xtype: 'container',
//            layout: 'vbox',
//            flex: 1,
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Inventory At Transits Report',
//                    height: 100,
//                    width: 100
//
//                }
//            ]
//        },
//        {
//            xtype: 'container',
//            layout: 'vbox',
//            flex: 1,
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Non Moving Inventory Report',
//                    height: 100,
//                    width: 100
//
//                }
//            ]
//        },
//        {
//            xtype: 'container',
//            layout: 'vbox',
//            flex: 1,
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Outlet Report',
//                    height: 100,
//                    width: 100
//
//                }
//            ]
//        },
//    ]
//
//
//
//});



function calcThreeMonthsRange() {
    var s = new Date();

    var m = s.setMonth(s.getMonth() + 3);

    return new Date(m);

}