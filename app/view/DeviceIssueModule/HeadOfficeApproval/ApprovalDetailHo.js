Ext.define('singer.view.DeviceIssueModule.HeadOfficeApproval.ApprovalDetailHo', {
    extend: 'Ext.form.Panel',
    alias: 'widget.approvalDetailHo',
    modal: true,
    layout: 'fit',
    constrain: true,
    width: 1200,
    resizable: false,
    loadMask: true,
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 1000,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems()
        });
        var groupItems = Ext.create('Ext.container.Container', {
            width: 1000,
            bodyPadding: 5,
            overflowY: 'auto',
            //            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formBottom = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.bottomButtons(),
        });

        var formTop = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.topButtons(),
        });

        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {},
                items: [
                    formTop,
                    {

                    },
                    groupItems,
                    formPanel,
                    formBottom
                ]
            }
        ];
        newUserForm.callParent();
    },
    buildGroupItems: function () {

        return [
            {
                xtype: 'panel',
//                flex: 3,
                items: [
                    Ext.create('Ext.grid.Panel', {
                        height: 200,
                         store: 'HeadOffliceReturnApproval',
                        id: 'debitNoteImeiHo',
                        // disabled: true,
                        emptyText: 'No Records',
                        requires: [
                            'Ext.grid.*',
                            'Ext.form.field.Text',
                            'Ext.ux.grid.FiltersFeature',
                            'Ext.toolbar.Paging'
                        ],
                        columns: [
                            {
                                xtype: 'searchablecolumn',
                                searchable: true,
                                sortable: false,
                                text: 'Model',
                                flex: 1,
                                dataIndex: 'modleDesc'
                            },
                            {
                                xtype: 'searchablecolumn',
                                searchable: true,
                                sortable: false,
                                text: 'IMEI No',
                                flex: 1,
                                dataIndex: 'imeiNo'
                            },
                            {
                                xtype: 'searchablecolumn',
                                searchable: true,
                                sortable: false,
                                text: 'Price',
                                flex: 1,
                                dataIndex: 'salesPrice'

                            },
                            {
                                xtype: 'checkcolumn',
                                text: 'Select',
                                flex: 1,
                                dataIndex: 'chk',
                                action: 'approveReturnChk'
                            }
                        ]
                    }
                    )
                ]
            }
        ];

    },
    //end accept grid

    buildItems: function () {
        return [
           
            {
                xtype: 'panel',
//                flex: 3,
                items: [
                    Ext.create('Ext.grid.Panel', {
                        height: 200,
                        store : 'HoApproveDtlTemp',
//                                        id: 'allUserGroups',
                        id: 'rejectedImeiHo',
                        // disabled: true,
                        emptyText: 'No Records',
                        requires: [
                            'Ext.grid.*',
                            'Ext.form.field.Text',
                            'Ext.ux.grid.FiltersFeature',
                            'Ext.toolbar.Paging'
                        ],
                        columns: [
                            {
//                                xtype: 'searchablecolumn',
//                                searchable: true,
//                                sortable: false,
                                text: 'Model',
                                flex: 1,
                                dataIndex: 'modleDesc'
                            },
                            {
//                                xtype: 'searchablecolumn',
//                                searchable: true,
//                                sortable: false,
                                text: 'IMEI No',
                                flex: 1,
                                dataIndex: 'imeiNo'
                            },
                            {
//                                xtype: 'searchablecolumn',
//                                searchable: true,
//                                sortable: false,
                                text: 'Price',
                                flex: 1,
                                dataIndex: 'salesPrice'

                            },
                            {
                                xtype: 'checkcolumn',
                                text: 'Select',
                                flex: 1,
                                dataIndex: 'chk',
                                action: 'addToApprove'
                            }
                        ]
                    }
                    )
                ]
            }

        ];
    },
    bottomButtons: function () {
        return [

            '->',
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel',
                formBind: true
            },
            {
                xtype: 'button',
                id: 'doneBtn',
                text: 'Done',
                action: 'done_ho',
                formBind: true
            }
        ];
    },
    topButtons: function () {
    },
    buildPlugins: function () {
        return [];
    }

});
