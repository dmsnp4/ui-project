/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.DeviceIssueModule.HeadOfficeReturn.HeadofficeNewDeviceReturn', {
    extend: 'Ext.window.Window',
    alias: 'widget.headofficenewdevicereturn',
    loadMask: true,
    modal: true,
    //    iconCls: 'icon-user',
    title: 'Add New Device Return To Head Office',
    //    store: 'Status',
    //    modal: true,       
    layout: 'fit',
    constrain: true,
    width: 500,
    //    height: 200,
    initComponent: function() {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            //            height: 300,
            //            loadMask: true,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                //                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                //                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });

        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function() {

    },
    buildItems: function() {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Pending"
                }
            ]
        });
        return [
            /*{
             fieldLabel: 'Return NO',
             name: 'returnNo',
             xtype: 'hiddenfield',
             maxLength: 10,
             
             //                visible: true
             //                vtype: 'ValidUserID',
             //                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
             //                allowBlank: false,
             //                regex: /^[a-zA-Z0-9_]+$/
             },*/
//            {
//                fieldLabel: 'Send To',
//                xtype: 'combobox',
//                name: 'distributorId',
//                emptyText: 'Select to Return',
//                store: 'device_return_Store',
//                id: 'stdsr',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                //                store: stat,
//                displayField: 'bisName',
//                valueField: 'bisId',
//                visible: true,
//                editable: false,
//                queryMode: 'remote',
//                allowBlank: false
//            },

            {
                xtype: 'displayfield',
                fieldLabel: 'Send To',
                //name: 'home_score',
                value: 'Singer Head Office',
                store: 'device_return_Store_Ho'
            },
            {
                        id: 'select_bis_id',
                        xtype: 'hiddenfield'
                    },
            {
                xtype: 'datefield',
                fieldLabel: 'Date',
                name: 'returnDate',
                //                maxLength: 20,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                maxValue: new Date(),
                value: new Date(),
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false
                        //                vtype: 'ValidUserID',
            },
            {
                fieldLabel: 'Status',
                xtype: 'combobox',
                name: 'status',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                //                 maxLength: 2,
                editable: false,
                valueField: 'status',
                listeners: {
                    scope: this,
                    afterRender: function(me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }
                }
            },
            {
                xtype: 'moneyfield',
                readOnly: true,
                name: 'price',
                fieldLabel: 'Total Price(Rs)',
                id: 'totalPrice',
                //cls:'DevRetTotPrice',
                listeners: {
                    afterrender: function(comp) {
//                        if (Ext.isEmpty(comp.getValue()) === true){
//                            comp.setValue("<strong style='font-size: 16px;'>0.00 Rs</strong>");
                        comp.setValue("0.00");
//                        }
                    }
                }
            },
            {
                xtype: 'textfield',
                name: 'reasonreturn',
                id: 'reason',
                fieldLabel: 'Reason',
                allowBlank: true  // requires a non-empty value
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaultType: 'button',
                items: [{
                        //                    itemId: 'showEmployees',
                        margin: '0 0 10 390',
                        text: 'Scan IMEIs',
                        scope: this,
                        //                    handler: this.onShowEmployeesClick,
                        action: 'scanimei_ho'
                    }]


                        //                iconAlign: 'right'
            },
            {
                xtype: 'grid',
                frame: false,
                id: 'holmgrid',
                store: 'DeviceIssueIme',
                border: false,
                columns: [
                    {xtype: 'actioncolumn',
                        text: '',
                        align: 'center',
                        flex: 1,
                        items: [{
                                icon: 'resources/images/delete-icon.png',
                                //action: 'remDevIsuItem',
                                tooltip: 'Remove',
                                handler: function(grid, rowIndex, colIndex) {
                                    var rec = grid.getStore().getAt(rowIndex);
                                    var me = this;
                                    Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function(button) {
                                        if (button == 'yes') {
                                            grid.getStore().removeAt(rowIndex);
                                            me.up('window').fireEvent('remDevIsuItem');

                                            var totalPrice = grid.getStore().sum('salesPrice');
                                            totalPrice = Ext.util.Format.number(totalPrice, '0000000.00');
                                            Ext.getCmp('totalPrice').setValue(totalPrice);
                                        } else {
                                            return false;
                                        }
                                    });

                                }
                            }]

                    },
                    {
                        text: 'Model',
                        dataIndex: 'modleDesc',
                        flex: 3
                    },
                    {
                        text: 'Serial No (IMEI)',
                        dataIndex: 'imeiNo',
                        flex: 3
                    },
                    {
                        text: 'Net Price',
                        dataIndex: 'salesPrice',
                        align: 'center',
                        flex: 2,
                        renderer: function(value) {
                            return Ext.util.Format.number(value, '0,000.00');
                        }
                    }
                    
                ]
                //                viewConfig: {
                ////                    emptyText: 'Click a button to show a dataset',
                //                    deferEmptyText: false,
                //                    stripeRows: false,
                //                    loadMask: false
                //                }
            }
            //            
            //           
        ];
    },
    buildButtons: function() {
        return [
            {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancelnewho'
                        //                id: 'userform-reset-insert'
            }, {
                //                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                hidden : true,
                id: 'headofficenewdevicereturn-save',
                formBind: true
                        //                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
                //                        ui: 'success',
                action: 'create_ho',
                id: 'headofficenewdevicereturn-create',
                formBind: true
            }
        ];
    },
    buildDockedItems: function() {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
                        //                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Submit',
                        action: 'save',
                        id: 'headofficenewdevicereturn-save',
                        formBind: true
                                //                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
                        //                        ui: 'success',
                        action: 'create',
                        id: 'headofficenewdevicereturn-create',
                        formBind: true
                    },
                    {
                        //                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
                                //                id: 'userform-reset-insert'
                    }
                ]
            }
        ];
    },
    buildPlugins: function() {
        return [];
    }

});