Ext.define('singer.view.DeviceIssueModule.Imei.EditImei', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.editImei',
    //    title: 'Add New',
    layout: 'fit',
    modal: true,
    constrain: true,
    loadMask: true,
    width: 500,
    initComponent: function () {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: 'bisId',
                id: 'businessId'
            },
            {
                xtype: 'hiddenfield',
                name: 'debitNoteNo',
                id: 'debitNo'
            },
            {
                fieldLabel: 'Serial No(IMEI):',
//                readOnly: 'true',
                name: 'imeiNo',
                maxLength: 100,
                allowBlank: false
            },
            {
                xtype: 'panel',
                border: false,
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'Verify IMEI',
                        action: 'onVerify'
                    }
                ]
            },
            {
                fieldLabel: 'Customer Code:',
                name: 'customerCode',
                maxLength: 100,
                allowBlank: false,
                readOnly: 'true'
            },
            {
                fieldLabel: 'Shop Code:',
                name: 'shopCode',
                maxLength: 100,
                readOnly: 'true',
//                allowBlank: false
            },
            {
                xtype: 'combobox',
                fieldLabel: 'Model:',
                store: 'modStore',
                name: 'modleNo',
                //                maxLength: 100,
//                allowBlank: false,
                displayField: 'model_Description',
                valueField: 'model_No',
                visible: true,
                readOnly: 'true',
                queryMode: 'local'
            },
            {
                fieldLabel: 'Site:',
                name: 'site',
                maxLength: 100,
                readOnly: 'true',
                allowBlank: false
            },
            {
                fieldLabel: 'Order No:',
                name: 'orderNo',
                maxLength: 100,
                readOnly: 'true',
                allowBlank: false
            },
            {
                xtype: 'moneyfield',
                fieldLabel: 'Sales Price(Rs.):',
                name: 'salerPrice',
                maxLength: 100,
                align: 'right',
                readOnly: 'true',
                allowBlank: false
            }
        ];
    },
    buildButtons: function () {
        return [
//            {
//                xtype: 'button',
//                text: 'Cancel',
//                action: 'cancel_edit'
//            }, 
//            
            {
                xtype: 'button',
                text: 'Submit',
                action: 'submit_edit',
//                formBind: true,
                disabled: true,
                id: 'editImei_submit_edit'
            }
        ];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});