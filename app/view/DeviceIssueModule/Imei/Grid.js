Ext.define('singer.view.DeviceIssueModule.Imei.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.imeigrid',
    loadMask: true,
    store: 'AcceptDebitNote',
//    requires: [
//        'Ext.grid.*',
//        'Ext.form.field.Text',
//        'Ext.ux.grid.FiltersFeature',
//        'Ext.toolbar.Paging'
//    ],
    minHeight: 200,
    selType: 'rowmodel',
//    forceFit:false,
    //store: 'Users',
    initComponent: function () {
        var grid = this;
        // var tempStore = Ext.create('Ext.data.Store', {
        //     storeId: 'simpsonsStore',
        //     fields: ['name', 'email', 'phone'],
        //     data: {'items': [
        //             {'name': 'Lisa', "email": "lisa@simpsons.com", "phone": "555-111-1224"},
        //             {'name': 'Bart', "email": "bart@simpsons.com", "phone": "555-222-1234"},
        //             {'name': 'Homer', "email": "home@simpsons.com", "phone": "555-222-1244"},
        //             {'name': 'Marge', "email": "marge@simpsons.com", "phone": "555-222-1254"}
        //         ]},
        //     proxy: {
        //         type: 'memory',
        //         reader: {
        //             type: 'json',
        //             root: 'items'
        //         }
        //     }
        // });
//        grid.store = 'ImeiAdjs';
        grid.columns = grid.buildColumns();

        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    }, '->',
//                    {
//                        text: 'Add New',
//                        action: 'open_window'
//
//                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: 'AcceptDebitNote',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'Debit Note No',
                dataIndex: 'debitNoteNo',
//                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Order No',
                dataIndex: 'orderNo',
//                flex: 2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Invoice No',
                dataIndex: 'invoiceNo',
//                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Contact No',
//                flex: 2,
                dataIndex: 'contactNo',
                renderer: function (value) {
                    if (value === "" || value === null) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Part No',
//                flex: 2,
                dataIndex: 'partNo',
                renderer: function (value) {
                    if (value === "" || value === null) {
                        return "-";
                    } else {
                        return value;
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Order Quantity',
                dataIndex: 'orderQty',
//                flex: 2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Delivered Quantity',
                dataIndex: 'deleveryQty',
//                flex: 2
//                renderer: function (value) {
//                    return Ext.util.Format.number(value, '0,000.00');
//                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Delivery Date',
                dataIndex: 'deleveryDate',
//                flex: 2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Head Status',
                dataIndex: 'headStatus',
//                flex: 2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Customer No',
                dataIndex: 'customerNo',
//                flex: 2
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'View ',
                menuDisabled: true,
                align: 'center',
//                flex: 1,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View Debit Note',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('onView', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Details',
                menuDisabled: true,
                align: 'center',
//                flex: 1,
                items: [{
                        icon: 'resources/images/details.png',
                        tooltip: 'Details',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('onDtail', grid, rec);
                        }
                    }]
            }
//            {
//                xtype: 'actioncolumn',
//                width: 50,
//                text: 'Details',
//                items: [{
//                        icon: 'resources/images/view-details.png',
//                        tooltip: 'Details',
//                        handler: function (grid, rowIndex, colIndex) {
//                            var rec = grid.getStore().getAt(rowIndex);
////                            alert("Terminate " + rec.get('first_name'));
//                            this.up('grid').fireEvent('onDetails', grid, rec);
//                        }
//                    }]
//            }

        ];
    }
});


