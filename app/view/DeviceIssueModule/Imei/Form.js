Ext.define('singer.view.DeviceIssueModule.Imei.Form', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.imeiform',
    layout: 'fit',
    constrain: true,
    modal: true,
    width: 400,
    height:600,
    initComponent: function() {

        var newForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newForm.buildItems(),
            buttons: newForm.buildButtons()
        });

        newForm.items = [formPanel];

        newForm.callParent();
    },
    buildItems: function() {

        return [
//            {
//                xtype: 'button',
//                text: 'Add new',
//                action: 'open_window',
//                style: {
//                    marginBottom: '6px'
//                }
//            },
//            {
//                xtype: 'label',
//                id: 'DebitNtNo',
//                text: '',
//                style: {
//                    float: 'right',
//                    margin:'5px',
//                    'font-weight':'bold',
//                    color:'#333'
//                }
//            },
            {
                xtype: 'grid',
                store: 'DebitNoteDetails',
                id: 'debitNoteImeii',
                columns: [
                    {text: 'Debit Note IMEIs', dataIndex: 'imeiNo', flex: 1, menuDisabled: true}
                ]
            }

        ];
    },
    buildButtons: function() {
        return [
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel_detail'
            },
//            {
//                xtype: 'button',
//                text: 'Save',
//                action: 'save',
//                id: 'imeiform-save',
//                formBind: true
//            },
//            
//            {
//                xtype: 'button',
//                text: 'Submit',
//                action: 'create',
//                id: 'imeiform-create',
//                formBind: true
//            }
        ];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});

