Ext.define('singer.view.DeviceIssueModule.Imei.DetailGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.imeidetailgrid',
    loadMask: true,
    store: 'ImeiDetail',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    forceFit: true,
    minHeight: 200,
    selType: 'rowmodel',
//    store: 'Users',
    initComponent: function () {
        var grid = this;
//        var tempStore = Ext.create('Ext.data.Store', {
//            storeId: 'simpsonsStore',
//            fields: ['name', 'email', 'phone'],
//            data: {'items': [
//                    {'name': 'Lisa', "email": "lisa@simpsons.com", "phone": "555-111-1224"},
//                    {'name': 'Bart', "email": "bart@simpsons.com", "phone": "555-222-1234"},
//                    {'name': 'Homer', "email": "home@simpsons.com", "phone": "555-222-1244"},
//                    {'name': 'Marge', "email": "marge@simpsons.com", "phone": "555-222-1254"}
//                ]},
//            proxy: {
//                type: 'memory',
//                reader: {
//                    type: 'json',
//                    root: 'items'
//                }
//            }
//        });
//        grid.store = 'ImeiDetail';
        grid.columns = grid.buildColumns();

        grid.dockedItems = grid.buildDockItems(this);
        grid.callParent();
    },
    buildDockItems: function (grid) {
        return [
            {
                xtype: 'form',
                dock: 'top',
                buttons: [{
                        text: 'Clear Search',
                        action: 'clearSearch'

                    },'->']
            },
            {
                xtype: 'pagingtoolbar',
//                store: 'RepairCategories',
                dock: 'bottom',
                displayInfo: true
            }

        ];
    },
    buildColumns: function () {

        return [
            {
                xtype: 'searchablecolumn',
                text: 'Serial No',
                dataIndex: 'name'
            }, {
                xtype: 'searchablecolumn',
                text: 'Customer Code',
                dataIndex: ''
            }, {
                xtype: 'searchablecolumn',
                text: 'Shop Code'
            }, 
            {
                // xtype:'checkboxfield',
                defaultType: 'checkboxfield',
                text:'Accept'
            }
        ];
    }
});


