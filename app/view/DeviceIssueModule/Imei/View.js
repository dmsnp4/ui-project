
Ext.define('singer.view.DeviceIssueModule.Imei.View', {
    extend: 'Ext.window.Window',
    alias: 'widget.imeiview',
//    xtype:'userview',
    iconCls: 'icon-user',
    title: 'Debit Note',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
//    height: 240,
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield',
                cls:'ViewTable'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {

        return [
            {
                fieldLabel: 'Debit Note No:',
                name: 'debitNoteNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Order No:',
                name: 'orderNo'
            },
            {
                fieldLabel: 'Invoice No:',
                name: 'invoiceNo'
            },
            {
                fieldLabel: 'Contract No:',
                name: 'contactNo'
            },
            {
                fieldLabel: 'Part No:',
                name: 'partNo'
            },
            {
                fieldLabel: 'Order Qty:',
                name: 'orderQty'
            },
            {
                fieldLabel: 'Deliver Qty:',
                name: 'deleveryQty'
            },
            {
                fieldLabel: 'Deliver Date:',
                name: 'deleveryDate'
            },
            {
                fieldLabel: 'Head Status:',
                name: 'headStatus'
            },
            {
                fieldLabel: 'Customer No:',
                name: 'customerNo'
            }
        ];
    },
    buildButtons: function () {
        return ['->', {
                text: 'Close',
                action: 'cancel'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});



