Ext.define('singer.view.DeviceIssueModule.credit_note.CreditNoteView', {
    extend: 'Ext.window.Window',
    alias: 'widget.creditnoteview',
    loadMask: true,
    iconCls: 'icon-user',
    title: 'Credit Note Details',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            
            defaults: {
                xtype: 'displayfield'
            },
            
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {
        return [
            {
                fieldLabel: 'Date',
                name: 'issueDate',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            
            {
                fieldLabel: 'Status',
                name: 'status',
                renderer: function(value) {
                    if (value === '1') {
                        return "Pending";
                    }
                    if (value === '2') {
                        return "Complete";
                    }
                }
            },
            {
                
                xtype: 'grid',
                frame:false,
                id: 'cvgrid',
                store: 'CreditNoteIssuIME',
                border: false,
                columns: [
                                {text: 'Model', dataIndex: 'modleDesc',flex: 1},
                                {text: 'Serial No (IMEI)', dataIndex: 'imeiNo',flex: 1},
                                {text: 'Net Price', dataIndex: 'salesPrice',flex: 1,align:'right',}
                            ],

            }
        ];
    },
    buildButtons: function() {
        return [ '->', {
                text: 'Done',
                action: 'cancel'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});
