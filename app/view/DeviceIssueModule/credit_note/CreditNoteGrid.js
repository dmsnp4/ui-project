/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.DeviceIssueModule.credit_note.CreditNoteGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.creditnotegrid',
    loadMask: true,
    store: 'CreditNote',
    id:'CreditNoteGrid_GRID',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging',
    ],
    forceFit: true,
    minHeight: 200,
//    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var usersGrid = this;
        usersGrid.columns = usersGrid.buildColumns();

//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        usersGrid.dockedItems = usersGrid.buildDockItems(this);
        usersGrid.features = usersGrid.buildFeatures();
        usersGrid.callParent();
    },
    buildDockItems: function (userGrid) {
        return [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
                        xtype: 'pagingtoolbar',
                        store: 'CreditNote',
                        dock: 'bottom',
                        displayInfo: true
                    }]
            }, {xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action:'clearSearch',
                        minWidth: 100,
                        minHeight: 10
                    },
//                   
                    '->',
//                    
                    {
                        iconCls: 'icon-search',
                        text: 'Add New',
                        action: 'open_window',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }
        ];
    },
    buildColumns: function () {
        return [
            {
                xtype: 'searchablecolumn',
                text: 'Credit Note No',
                dataIndex: 'creditNoteIssueNo',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Date',
                dataIndex: 'issueDate',
                flex: 2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'status',
                flex: 2,
                renderer: function (value) {
                    if (value === 1) {
                        return "Pending";
                    }
                    if (value === 2) {
                        return "Complete";
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                text: 'Print',
                menuDisabled: true,
                flex:1,
                align:'center',
                items: [{
                        icon: 'resources/images/printerr.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('printCreditNote', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                text: 'View',
                menuDisabled: true,
                flex:1,
                align:'center',
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('creditView', grid, rec);
                        }
                    }]
            },{
                xtype: 'actioncolumn',
                text: 'Edit',
                menuDisabled: true,
                flex:1,
                align:'center',
                items: [{
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('creditEdit', grid, record);
//                            ////console.log(this.up('grid'));
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                                // Returns true if 'editable' is false (, null, or undefined)
                                if (record.get('status') === 1) {
                                        return false;
                                } else {
                                        return true;
                                }
                        }
                    }]
            },
            

        ];
    },
    buildFeatures: function () {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'creditNoteIssueNo',
                    type: 'int'
//                    width: 50
                }, {
                    type: 'string',
                    dataIndex: 'issueDate'
                }, 
            ]
        };
        return [filtersCfg];
    }

});

