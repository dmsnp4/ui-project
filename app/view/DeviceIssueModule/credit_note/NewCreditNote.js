Ext.define('singer.view.DeviceIssueModule.credit_note.NewCreditNote', {
    extend: 'Ext.window.Window',
    alias: 'widget.newcreditnote',
    loadMask: true,
    modal: true,
    title: 'Add New Credit Note Issue',
    layout: 'fit',
    constrain: true,
    width: 530,
    height: 400,
    initComponent: function () {

        var newUserForm = this;

        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            //height: 100,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItemsForm(),
            //buttons: newUserForm.buildButtons()

        });

        //        var pan = Ext.create('Ext.form.Panel', {
        //            width: 500,
        //            height: 400,
        //            bodyPadding: 5,
        //            overflowY: 'auto',
        ////            defaults: {
        ////                xtype: 'textfield'
        ////            },
        //            fieldDefaults: {
        //                anchor: '100%',
        //                labelAlign: 'left',
        //                msgTarget: 'side',
        //                labelWidth: 180,
        //                margin: '10 0 10 0',
        //                border:'false'
        //            },
        //            //items: newUserForm.buildItemsPanel(),
        //        });

        var main = Ext.create('Ext.Panel', {
            width: 500,
            //height: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            //            defaults: {
            //                xtype: 'textfield'
            //            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: [formPanel] //newUserForm.buildItemsPanel(),
        });
        //        pan2.dockedItems = newUserForm.buildButtons();
        //        pan2.items=[formPanel, pan];
        newUserForm.items = [main];
        newUserForm.dockedItems = newUserForm.buildDockedItems();;
        newUserForm.callParent();


    },

    buildItemsForm: function () {
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {
                    "status": 1,
                    "value": "Pending"
                },
                {
                    "status": 2,
                    "value": "Complete"
                }
            ]
        });
        return [
            {
                xtype: 'hiddenfield',
                //                fieldLabel: 'distri',
                name: 'distributerId'
            },
            {
                fieldLabel: 'Credit Note No',
                name: 'creditNoteIssueNo',
                xtype: 'hiddenfield',
                maxLength: 10,
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Date',
                name: 'issueDate',
                allowBlank: false,
                maxValue: new Date(),
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false,
                value: new Date()
            },
            {
                fieldLabel: 'Status',
                xtype: 'combobox',
                name: 'status',
                store: stat,
                queryMode: 'local',
                displayField: 'value',
                editable: false,
                valueField: 'status',
                listeners: {
                    scope: this,
                    afterRender: function (me) {
                        if (me.getValue() === null) {
                            me.setValue(1);
                        }
                    }
                }
            },
            {
                xtype: 'button',
                margin: '5 0 10 405',
                text: 'Scan IMEIs',
                action: 'scanimei'
            },

            {
                xtype: 'grid',
                frame: false,
                id: 'cmgrid',
                //store: 'WinGrid',
                store: 'CreditNoteIssuIME',
                border: false,
                columns: [
                  //  {text: 'Model', dataIndex: 'modleNo', flex: 1},
                    {
                        text: 'Model',
                        dataIndex: 'modleDesc',
                        flex: 1
                    },
                    {
                        text: 'Serial No (IMEI)',
                        dataIndex: 'imeiNo',
                        flex: 2
                    },
                    {
                        text: 'Net Price',
                        dataIndex: 'salesPrice',
                        align: 'right',
                        flex: 1,
                        renderer: function (value) {
                            return Ext.util.Format.number(value, '0,000.00');
                        }
                    },
                    {
                        xtype: 'actioncolumn',
                        text: 'Cancel',
                        flex: 1,
                        items: [{
                            icon: 'resources/images/delete-icon.png',
                            action: 'removeItem',
                            align: 'center',
                            tooltip: 'Remove',
                            handler: function (grid, rowIndex, colIndex) {
                                //                                    //console.log("Click .............", grid.getStore().getAt(rowIndex));
                                var rec = grid.getStore().getAt(rowIndex);

                                Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function (button) {
                                    if (button == 'yes') {
                                        grid.getStore().removeAt(rowIndex);
                                    } else {
                                        return false;
                                    }
                                });

                                //rec.destroy();
                                //grid.getStore().removeAt(rowIndex);
                                ;
                            }
                        }]
                    }]
            },
        ];
    },
    buildButtons: function () {
        return [


            {
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newcreditnote-save',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Submit',
                action: 'create',
                id: 'newcreditnote-create',
                formBind: true
            },
            {
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
            }
        ];
    },
    buildDockedItems: function () {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
//                    {
//                        xtype: 'button',
//                        text: 'Print',
//                        action: 'PrintCreditNote',
//                        id: 'creditNote_print',
//                        formBind: true,
//                        //hidden:true
//                    },

                    {
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newcreditnote-save',
                        formBind: true
                    },
                    {
                        xtype: 'button',
                        text: 'Submit',
                        action: 'create',
                        id: 'newcreditnote-create',
                        formBind: true
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
                    },
                ]
            }
        ];
    },
    buildPlugins: function () {
        return [];
    }

});





//==========================================================================

//Ext.define('singer.view.DeviceIssueModule.credit_note.NewCreditNote', {
//    extend: 'Ext.window.Window',
//    alias: 'widget.newcreditnote',
//    loadMask: true,
//    modal: true,
//    title: 'Add New Credit Note Issue',
//    layout: 'fit',
//    constrain: true,
//    width: 500,
////    extend: 'Ext.window.Window',
////    alias: 'widget.newuser',
//////    iconCls: 'icon-user',
////    title: 'Add New User',
//////    store: 'Status',
////    modal: true,
////    layout: 'fit',
////    constrain: true,
////    width: 500,
////    resizable: false,
////    loadMask: true,
//////    maskOnDisable: false,
//////    height: 700,
//    initComponent: function () {
//
//        var newUserForm = this;
//        
//        var formPanel = Ext.create('Ext.form.Panel', {
//            width: 500,
////            height: 600,
//            bodyPadding: 5,
//            overflowY: 'auto',
//            defaults: {
//                xtype: 'textfield'
//            },
//            fieldDefaults: {
//                anchor: '100%',
//                labelAlign: 'left',
//                msgTarget: 'side',
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//                labelWidth: 180,
//                margin: '10 0 10 0'
//            },
//            items: newUserForm.buildItems(),
////            buttons: newUserForm.buildButtons(),
////            items: groupItems
//
//        });
//        
//        
//        var groupItems = Ext.create('Ext.container.Container', {
//            width: 500,
////            height: 300,
//            bodyPadding: 5,
//            overflowY: 'auto',
////            defaults: {
////                xtype: 'textfield'
////            },
//            fieldDefaults: {
//                anchor: '100%',
//                labelAlign: 'left',
//                msgTarget: 'side',
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//                labelWidth: 180,
//                margin: '10 0 10 0'
//            },
//            items: newUserForm.buildGroupItems()
//        });
//        
//        var formButtons = Ext.create('Ext.form.Panel', {
////            width: 500,
////            height: 600,
//            bodyPadding: 5,
//            overflowY: 'auto',
//            fieldDefaults: {
//                anchor: '100%',
//                labelAlign: 'left',
//                msgTarget: 'side',
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//                labelWidth: 180,
//                margin: '10 0 10 0'
//            },
//            buttons: newUserForm.buildButtons(),
////            buttons: newUserForm.buildButtons(),
////            items: groupItems
//
//        });
////        newUserForm.items = [formPanel, groupItems];
//        newUserForm.items = [
//            {
//                xtype: 'container',
//                layout: {
//                    type: 'vbox',
//                    align: 'stretch'
//                },
//                defaults: {
////                    flex: 5
//                },
//                items: [
//                    formPanel,
////                    this.buildGroupItems()
//                    groupItems,
//                    formButtons
//                ]
//            }
//        ];
//
//
////        newUserForm.dockedItems = newUserForm.buildDockedItems()
////        supplierForm.items = [vbox];
//        newUserForm.callParent();
//    },
//    
//    
//    buildGroupItems: function () {
//        return Ext.create('Ext.Panel', {
//            width: 500,
//            title: "User Groups",
//            layout: {
//                type: 'hbox',
//                align: 'stretch'
//            },
//            items: [
//                 {
//                xtype: 'grid',
//                frame:false,
//                id: 'cmgrid',
//                store: 'WinGrid',
//                border: false,
//                columns: [
//                                {text: 'Model', dataIndex: 'modleNo',flex: 1},
//                                {text: 'Serial No (IMEI)', dataIndex: 'iMEINo',flex: 1},
//                                {xtype:'actioncolumn',text:'Cancel', flex:1,
//                                items: [{
//                                icon: 'resources/images/delete-icon.png',
//                                tooltip: 'Remove',
//                                handler: function (grid, rowIndex, colIndex) {
//                                    var rec = grid.getStore().getAt(rowIndex);
//                                    rec.destroy();
//                                }
//                            }]
//                            }
//                        ]
//            }  
//                
////                {
////                    xtype: 'panel',
////                    flex: 2,
////                    items: [
////                        Ext.create('Ext.grid.Panel', {
////                            id: 'allUserGroups', //do not duplicate the name
////                            columns: [
////                                {text: 'Available Groups', dataIndex: 'groupName', flex: '1'}
////                            ]
//////                                height: 200,
//////                                width: 400,
//////                                renderTo: Ext.getBody()
////                        })
////                    ]
////                }, 
////                {
////                    xtype: 'panel',
//////                        title: 'Inner Panel One',
////                    flex: 2,
//////                    layout:'fit',
//////                    height:'100',
////                    layout: {type: 'vbox', align: 'center', pack: 'center'},
//////                    buttonAlign: 'center',
////                    items: [
////                        {
////                            xtype: 'button',
////                            text: '>',
////                            action: 'nextOne',
//////                            height: '25%'
////                            width: '50%'
////                        },
////                        {
////                            xtype: 'button',
////                            text: '>>',
////                            action: 'nextAll',
////                            width: '75%'
////                        },
////                        {
////                            xtype: 'button',
////                            text: '<<',
////                            action: 'prevAll',
////                            width: '75%'
////                        }, {
////                            xtype: 'button',
////                            text: '<',
////                            action: 'prevOne',
////                            width: '50%'
////                        },
//////                        
////                    ]
////                }, 
////                    {
////                    xtype: 'panel',
//////                        title: 'Inner Panel Three',
////                    flex: 2,
////                    items: [
////                        Ext.create('Ext.grid.Panel', {
////                            id: 'UserGroupsGrid', //Do not duplicate the name
////                            columns: [
////                                {text: 'Assigned Groups', dataIndex: 'groupName', flex: '1'}
////                            ]
////                        })
////                    ]
////                }
//            ]
//        });
//    },
//    
//    buildItems: function () {
//
//         var stat = Ext.create('Ext.data.Store', {
//            fields: ['status', 'value'],
//            data: [
//                {"status": 1, "value": "Pending"},
//                {"status": 2, "value": "Complete"}
//            ]
//        });
//        return [
//            {
//                fieldLabel: 'Credit Note No',
//                name: 'creditNoteIssueNo',
//                xtype: 'hiddenfield',
//                maxLength: 10,
//            },
//            {
//                xtype: 'datefield',
//                fieldLabel: 'Date',
//                name: 'issueDate',
////                maxLength: 20,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                maxValue: new Date(),
//                format: 'Y-m-d',
//                altFormats: 'm/d/Y',
//                editable: false
////                vtype: 'ValidUserID',
//            },
//            {
//                fieldLabel: 'Status',
//                xtype: 'combobox',
//                name: 'status',
//                store: stat,
//                queryMode: 'local',
//                displayField: 'value',
////                 maxLength: 2,
//                editable: false,
//                valueField: 'status',
//                 listeners: {
//                    scope: this,
//                    afterRender: function(me) {
//                        if (me.getValue() === null) {
//                            me.setValue(1);
//                        }
//                    }
//                }
//            }, 
//            
//            {
//                xtype: 'container',
//                layout: 'hbox',
//                id:'sibtn',
//                defaultType: 'button',
//                items: [{
//                    margin: '0 0 0 400',
//                    text: 'Scan IMEIs',
//                    scope: this,
////                    handler: this.onShowEmployeesClick,
//                    action: 'scanimei'
//                }]
//            },
//            {
//                fieldLabel: 'IMEI',
//                name: 'iMEINo',
//            },
//            {
//                fieldLabel: 'Model',
//                xtype: 'combobox',
//                name: 'modleNo',
//                emptyText: 'Select Model',
//                store: 'modStore',
//                queryMode: 'local',
//                displayField: 'model_No',
//                valueField: 'model_No',
//                visible: true,
//                editable: false
//            },
//            {
//                xtype: 'container',
//                layout: 'hbox',
//                id:'agbtn',
//                defaultType: 'button',
////                formBind: true,
//                items: [{
////                    itemId: 'showEmployees',
//                    margin: '0 0 0 400',
//                    text: 'Add to Grid',
//                    scope: this,
////                    handler: this.onShowEmployeesClick,
//                    action: 'gridadd'
//                }]
//                
//                
////                iconAlign: 'right'
//            }
//     
//        ];          
////        var stat = Ext.create('Ext.data.Store', {
////            fields: ['status', 'value'],
////            data: [
////                {"status": '1', "value": "Active"},
////                {"status": '2', "value": "Inactive"}
////            ]
////        });
////        return [
////            {
////                fieldLabel: 'User ID',
////                name: 'userId',
////                maxLength: 19,
//////                vtype: 'ValidUserID',
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
////                regex: /^[a-zA-Z0-9_]+$/
////            }, {
////                fieldLabel: 'NIC',
////                name: 'nic',
////                maxLength: 10,
////                minLength: 10,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//////                vtype: 'ValidUserID',
////                regex: /^[V0-9_]+$/
////            }, {
////                fieldLabel: 'First Name',
////                name: 'firstName',
////                maxLength: 10,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//////                vtype: 'ValidUserID',
////                regex: /^[a-zA-Z_]+$/
////            }, {
////                fieldLabel: 'Last Name',
////                name: 'lastName',
////                maxLength: 10,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//////                vtype: 'ValidUserID',
////                regex: /^[a-zA-Z_]+$/
////            }, {
////                fieldLabel: 'Common Name',
////                name: 'commonName',
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
////                maxLength: 10,
//////                vtype: 'ValidUserID',
////                regex: /^[a-zA-Z_]+$/
////            }, {
////                fieldLabel: 'Designation',
////                name: 'designation',
////                maxLength: 10,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////                allowBlank: false,
//////                vtype: 'ValidUserID',
////                regex: /^[a-zA-Z_]+$/
////            }, {
////                fieldLabel: 'Telephone No',
////                name: 'telephoneNumber',
////                maxLength: 10,
////                minLength: 10,
//////                vtype: 'int',
////                allowBlank: true,
////                regex: /^[0-9_]+$/,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
////            }, {
//////                xtype: 'textfield',
////                fieldLabel: 'Email',
////                name: 'email',
//////                maxLength: 10,
////                vtype: 'email',
////                allowBlank: true,
////                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//////                regex: /^[V0-9_]+$/
////            },
////            {
////                fieldLabel: 'Active Status',
////                xtype: 'combobox',
////                name: 'status',
////                editable: false,
////                store: stat,
////                queryMode: 'local',
////                displayField: 'value',
////                valueField: 'status',
////                listeners: {
////                    scope: this,
////                    afterRender: function (me) {
//////                        ////console.log(me.getValue());
////                        if (me.getValue() === null) {
////                            me.setValue('1');
////                        }
////
////
////                    }
////                }
////            },
////            {
//////                iconCls: 'icon-repeat',
////                xtype: 'button',
////                text: 'Reset Password',
////                action: 'resetPass'
//////                id: 'userform-reset-insert'
////            }
////            // {
////            //     fieldLabel: 'groups',
////            //     name:'groups'
////            // },
//////            {
//////                    fieldLabel: 'userID',
//////                name:'createdBy'
//////            }
//////           
////        ];
//    },
//    buildButtons: function () {
//        return [
//            {
////                iconCls: 'icon-repeat',
//                xtype: 'button',
//                text: 'Cancel',
//                action: 'cancel'
////                id: 'userform-reset-insert'
//            }, {
////                iconCls: 'icon-repeat',
//                xtype: 'button',
//                text: 'Save',
//                action: 'save',
//                id: 'newuser-save',
//                formBind: true
////                id: 'userform-reset-update'
//            },
//            {
//                xtype: 'button',
//                text: 'Create',
////                        ui: 'success',
//                action: 'create',
//                id: 'newuser-create',
//                formBind: true,
//            }
////           
//        ];
//    },
//    buildDockedItems: function () {
//        return [
//            {
//                xtype: 'toolbar',
//                dock: 'bottom',
//                items: ['->',
//                    {
////                iconCls: 'icon-repeat',
//                        xtype: 'button',
//                        text: 'Cancel',
//                        action: 'cancel'
////                id: 'userform-reset-insert'
//                    }, {
////                iconCls: 'icon-repeat',
//                        xtype: 'button',
//                        text: 'Save',
//                        action: 'save',
//                        id: 'newuser-save',
//                        formBind: true
////                id: 'userform-reset-update'
//                    },
//                    {
//                        xtype: 'button',
//                        text: 'Create',
////                        ui: 'success',
//                        action: 'create',
//                        id: 'newuser-create',
//                        formBind: true
//                    }
//                ]
//            }
//        ];
//    },
//    buildPlugins: function () {
//        return [];
//    }
//
//});