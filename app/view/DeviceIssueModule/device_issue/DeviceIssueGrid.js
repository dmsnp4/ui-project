/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.DeviceIssueModule.device_issue.DeviceIssueGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.deviceissuegrid',
    loadMask: true,
    store: 'DeviceIssue',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging'
    ],
    forceFit: true,
    minHeight: 200,
    //    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function() {
        var usersGrid = this;
        usersGrid.columns = usersGrid.buildColumns();

        //        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        usersGrid.dockedItems = usersGrid.buildDockItems(this);
        usersGrid.features = usersGrid.buildFeatures();
        usersGrid.callParent();
    },
    buildDockItems: function(userGrid) {
        return [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
                        xtype: 'pagingtoolbar',
                        store: 'DeviceIssue',
                        dock: 'bottom',
                        displayInfo: true
                    }]
            }, {
                xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch',
                        //                        handler: function() {
                        //                            userGrid.filters.clearFilters();},
                        minWidth: 100,
                        minHeight: 10
                    },
                    //                    {
                    //                        iconCls: 'icon-search',
                    //                        text: 'Add Sub Department',
                    //                        action: 'open_Add_Sub_Department_window',
                    //                        minWidth: 100,
                    //                        minHeight: 10
                    //                    },
                    '->',
                    //                    {
                    //                        iconCls: 'icon-search',
                    //                        text: 'Edit',
                    //                        action: 'open_Edit_window',
                    //                        minWidth: 100,
                    //                        minHeight: 10
                    //                    },
                    {
                        iconCls: 'icon-search',
                        text: 'Add New',
                        action: 'open_window',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }
            //            {
            //                xtype: 'toolbar',
            ////                xtype: 'form',
            //                dock: 'top',
            //                items: [{
            //                        xtype: 'button',
            //                        text: 'Clear Search',
            //                        scale: 'large',
            ////                        icon: 'resources/res/img.jpg',
            //                        minWidth: 100,
            //                        minHeight: 50,
            //                        handler: function() {
            //                            userGrid.filters.clearFilters();
            //                        }
            //
            //                    }, '->', {
            //                        xtype: 'button',
            //                        text: 'Add New Category',
            //                        scale: 'large',
            ////                        icon: 'resources/res/img.jpg',
            //                        action: 'open_window',
            //                        minWidth: 100,
            //                        minHeight: 50
            //
            //                    }]
            //            }
        ];
    },
    buildColumns: function() {
        var status = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                {
                    "name": "2",
                    "value": "Complete"
                },
                {
                    "name": "1",
                    "value": "Pending"
                },
                {
                    "name": "3",
                    "value": "Accepted"
                }
            ]
        });
        return [
            //            {text: 'User ID', dataIndex: 'USER_ID'},
            //            {text: 'First Name', dataIndex: 'FIRST_NAME', flex: 1},
            //            {text: 'Last Name', dataIndex: 'LAST_NAME'},
            //            {text: 'Common Name', dataIndex: 'COMMON_NAME'},
            //            {text: 'Designation', dataIndex: 'DESIGNATION'},
            //            {text: 'Department', dataIndex: 'BIS_ID'},
            //            {text: 'Tel.No', dataIndex: 'TELEPHONE_NO'},
            //            {text: 'Email', dataIndex: 'EMAIL'},
            //            {text: 'Reg.Date', dataIndex: 'REGISTERED_DATE'},
            //            {text: 'Inactive Date', dataIndex: 'INACTIVE_DATE'},
            //            {text: 'Status', dataIndex: 'STATUS'},
            {
                xtype: 'searchablecolumn',
                text: 'Issue Number',
                dataIndex: 'issueNo',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Distributor',
//				dataIndex: 'distributerID',
                dataIndex: 'fromName',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'DSR/Sub Dealer',
//				dataIndex: 'dSRId',
                dataIndex: 'toName',
                flex: 2
            },
            {
//                xtype: 'searchablecolumn',
                text: 'Count',
                dataIndex: 'dtlCnt',
                flex: 2
            },
            //            {
            //                hidden: true,
            //                text: 'Date',
            //                dataIndex: 'issueDate',
            //                flex: 2
            //            },
            //            {  hidden: true,
            //                text: 'Price(Rs.)',
            //                dataIndex: 'price',
            //                flex: 2
            //            },
            //            { hidden: true,
            //                text: 'Distributor Margin',
            //                dataIndex: 'distributorMargin',
            //                flex: 2
            //            },
            //            { hidden: true,
            //                text: 'Dealer Margin',
            //                dataIndex: 'delerMargin',
            //                flex: 2
            //            },
            //            { hidden: true,
            //                text: 'Discount',
            //                dataIndex: 'discount',
            //                flex: 2
            //            },
            {
                xtype: 'searchablecolumn',
                //searchable: false,
                text: 'Status',
                dataIndex: 'status',
                flex: 2,
                renderer: function(value) {
                    if (value === 2) {
                        return "Complete";
                    }
                    if (value === 1) {
                        return "Pending";
                    }
                    if (value === 3) {
                        return "Accepted";
                    }

                }
            },
            {
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'View',
                width: 70,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            //                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('issueView', grid, rec);
                        }
                    }]
            },{
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'Edit',
                width: 70,
                items: [{
                        icon: 'resources/images/edit.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('issueEdit', grid, record);
                            //                            //console.log(this.up('grid'));
                        },
                        isDisabled: function(view, rowIndex, colIndex, item, record) {
                            // Returns true if 'editable' is false (, null, or undefined)
                            if ((record.get('status') === 2)||record.get('status') === 3) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'Report',
                width: 70,
                items: [{
                        icon: 'resources/images/details.png', // Use a URL in the icon config
                        tooltip: 'Edit',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('dsrReport', grid, record);
                            //                            //console.log(this.up('grid'));
                        },
                        isDisabled: function(view, rowIndex, colIndex, item, record) {
                            // Returns true if 'editable' is false (, null, or undefined)
                            if ((record.get('status') === 2)||record.get('status') === 3) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }]
            }


        ];
    },
    buildFeatures: function() {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'issueNo',
                    type: 'int'
                            //                    width: 50
                }, {
                    type: 'int',
                    dataIndex: 'distributerID'
                }, {
                    type: 'int',
                    dataIndex: 'dSRId'
                }, {
                    dataIndex: 'status',
                    type: 'string'
                }
            ]
        };
        return [filtersCfg];
    }

});
