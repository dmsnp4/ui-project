Ext.define('singer.view.DeviceIssueModule.device_issue.DeviceIssueView', {
    extend: 'Ext.window.Window',
    alias: 'widget.deviceissueview',
    loadMask: true,
//    xtype:'userview',
    iconCls: 'icon-user',
    title: 'System User Details',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
//    height: 240,
    initComponent: function() {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
//            height: 340,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function() {
        return [
//            {
//                fieldLabel: 'User Type',
//                name: 'user_type',
//                listeners: {
//                    afterrender: function(comp) {
//                        if (parseInt(comp.getValue()) === 1)
//                            comp.setValue('Admin');
//                        if (parseInt(comp.getValue()) === 2)
//                            comp.setValue('Default User');
//                        if (parseInt(comp.getValue()) === 3)
//                            comp.setValue('Inquiry User');
//                    }
//                }
//
//            },
            {
                fieldLabel: 'Date',
                name: 'issueDate',
                id: 'deviseIssueDate',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Price(Rs.)',
                name: 'price',
                id: 'deviseIssuenetPrice',
                renderer: function(value) {
                    return Ext.util.Format.number(value, '0,000.00');  }
            },
//            {
//                fieldLabel: 'Distributor Margin',
//                name: 'distributorMargin'
//            },
//            {
//                fieldLabel: 'Dealer Margin',
//                name: 'delerMargin'
//            },
//             {
//                fieldLabel: 'Discount',
//                name: 'discount',
//                renderer: function(value) {
//                    return Ext.util.Format.number(value, '0,000.00');  }
//            },
            {
                fieldLabel: 'Status',
                name: 'status',
                id: 'deviseIssueStatue',
                renderer: function(value) {
                    if (value === '1') {
                        return "Pending";
                    }
                    if (value === '2') {
                        return "Complete";
                    }
                    if (value === '3') {
                        return "Accepted";
                    }
                }
            },
            {
                xtype: 'grid',
                frame:false,
                id: 'ivgrid',
                store: 'DeviceIssueIme',
                border: false,
                columns: [
                                {text: 'Model', dataIndex: 'modleDesc',flex: 3},
                                {text: 'Margin(%)', dataIndex: 'margin',flex: 2},
                                {text: 'Serial No (IMEI)', dataIndex: 'imeiNo',flex: 3},
                                {text: 'Net Price(Rs)', dataIndex: 'salesPrice', flex: 2,align:'right'}
                            ],
//                 viewConfig: {
////                    emptyText: 'Click a button to show a dataset',
//                    deferEmptyText: false,
//                    stripeRows: false,
//                    loadMask: false
//                }
            }
        ];
    },
    buildButtons: function() {
        return [ '->', {
                text: 'Done',
                action: 'cancel'
            },
            {
                text: 'Report',
                action: 'report'
            }];
    },
    buildDockedItems: function() {
        return [];
    },
    buildPlugins: function() {
        return [];
    }

});
