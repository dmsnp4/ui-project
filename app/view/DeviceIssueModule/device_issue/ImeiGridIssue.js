
Ext.define('singer.view.DeviceIssueModule.device_issue.ImeiGridIssue', {
    extend: 'Ext.container.Container',
   alias: 'widget.imeigridissue',
//    iconCls: 'icon-user',
   Title: 'IMEIs',
//    store: 'Status',
    modal: true,
    layout: 'fit',
    constrain: true,
//    width: 500,
    resizable: false,
    loadMask: true,
    //forcefit: true,
//    maskOnDisable: false,
    height: 400,
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 600,
            bodyPadding: 5,
            overflowY: 'auto',
            layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
            fieldDefaults: {
//                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems()
//            buttons: newUserForm.buildButtons(),
//            items: groupItems

        });
        var groupItems = Ext.create('Ext.container.Container', {
            width: 500,
//            height: 300,
            bodyPadding: 0,
            overflowY: 'auto',
//            defaults: {
//                xtype: 'textfield'
//            },
            fieldDefaults: {
//                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formButtons = Ext.create('Ext.form.Panel', {
//            width: 500,
//            height: 600,
//            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
//                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.buildButtons()
//            buttons: newUserForm.buildButtons(),
//            items: groupItems

        });
//        newUserForm.items = [formPanel, groupItems];
        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
//                    flex: 5
                },
                items: [
                    formPanel,
//                    this.buildGroupItems()
                    groupItems,
                    formButtons
                ]
            }
        ];


//        newUserForm.dockedItems = newUserForm.buildDockedItems()
//        supplierForm.items = [vbox];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
        return Ext.create('Ext.Panel', {
           height:165,
                            width: 500,
//            title: "User Groups",
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
//                renderTo: document.body,
            items: [  {
                    xtype: 'panel',
//                        title: 'Inner Panel Three',
//                    flex: 2,
                    items: [
                        Ext.create('Ext.grid.Panel', {
//                            forcefit: true,
                            height:165,
                            id:'imgrid',
                            store: 'WinGrid',
                            width: 500,
//                            id: 'UserGroupsGrid', //Do not duplicate the name
                            columns: [
                                {text: 'Model', dataIndex: 'modleDesc',flex: 1},
                                {text: 'Serial No (IMEI)', dataIndex: 'imeiNo',flex: 1}
                            ]
                        })
                    ]
                }]
        });
    },
    buildItems: function () {
//
//
        return [

           {
                            xtype: 'textfield',
                            name: 'enterimei',
                            emptyText: 'Enter Serial No(IMEI)',
                            id:'imtextf',
                            minHeight: 10,
                            regex: /^[a-zA-Z0-9]*$/ ,
                            stripCharsRe: /(^\s+|\s+$)/g,
                            allowBlank: false
                        },
                        {
                            xtype: 'button',
                            text: 'Add',
                            name: 'adbtn',
                            action: 'add',
                            minWidth: 45,
                            style: {
                                margin: '10px'
                            },
            //                            minHeight: 10,
                            height: 20,
                            formBind: true,
            //                            listeners: {
            //                                scope: this,
            //                                click: function(me){
            ////                                    //console.log('onclick');
            //                                    Ext.getCmp('imtextf').setValue("");
            //                                }
            //                            }
                        },
                         {
                            xtype:'label',
                            text: 'No of Imeis:',
                            id:'noimeiz',
                            style:{
                                margin: '20px'
                            }
                        },
                        {
                            xtype:'label',
                            text: '0',
                            hideLabel: true,
                            id:'noimei',
                            style:{
                                margin: '20px'
                            }
                        }
//
        ];
    },
    buildButtons: function () {
        return [

//            {
////                iconCls: 'icon-repeat',
//                xtype: 'button',
//                text: 'Cancel',
//                action: 'cancel'
////                id: 'userform-reset-insert'
//            },
            {
//                        ui: 'success',

                                        id: 'imeigridissue-create',
                                        xtype: 'button',
                                        iconCls: 'icon-search',
                                        text: 'OK',
                                        action: 'okay',
//                        margin: '0 0 400',
//                        handler: function() {
//                            majordefectGrid.filters.clearFilters();},
                                        minWidth: 100,
                                        minHeight: 10,
//                                        formBind: true
                                    },
                                    '->',
                                    {
                                        xtype: 'button',
//                        iconCls: 'icon-search',
                                        text: 'Cancel',
                                        action: 'cancel',
//                        margin: '0 400 0 0',
                                        minWidth: 100,
                                        minHeight: 10,
                                        id: 'imeigridissue-cancel',
//                                        formBind: true,
                                    }

//
        ];
    },
    buildDockedItems: function () {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
//                id: 'userform-reset-insert'
                    }, {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newuser-save',
                        formBind: true
//                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
//                        ui: 'success',
                        action: 'create',
                        id: 'newuser-create',
                        formBind: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function () {
        return [];
    }

});
