
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('singer.view.DeviceIssueModule.device_issue.NewDeviceIssue', {
    extend: 'Ext.window.Window',
    alias: 'widget.newdeviceissue',
    loadMask: true,
    modal: true,
//    iconCls: 'icon-user',
    title: 'Add New Issue',
//    store: 'Status',
//    modal: true,
    layout: 'fit',
    constrain: true,
    width: 600,
    height: 480,
    initComponent: function() {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
//            height: 300,
//            loadMask: true,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
            buttons: newUserForm.buildButtons()
        });
//
        newUserForm.items = [formPanel];
        newUserForm.callParent();
    },
    buildGroupItems: function() {
//
    },
    buildItems: function() {
//
//
        var stat = Ext.create('Ext.data.Store', {
            fields: ['status', 'value'],
            data: [
                {"status": 2, "value": "Complete"},
                {"status": 1, "value": "Pending"},
//                {"status": 3, "value": "Unconfirmed"}
            ]
        });
        return [
            {
                fieldLabel: 'Issue No',
                name: 'issueNo',
                xtype: 'hiddenfield',
                maxLength: 10,
//                visible: true
//                vtype: 'ValidUserID',
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false,
//                regex: /^[a-zA-Z0-9_]+$/
            },
            {
                xtype: 'hiddenfield',
//                fieldLabel: 'distri',
                name: 'distributerID'
            },
            {
                xtype: "radiogroup",
                id: 'rad',
                fieldLabel: 'Issuee Type',
                action: 'clickRdio',
                columns: 3,
                items: [
                    {
                        boxLabel: 'DSR',
                        checked: true,
                        id: 'dsrrad',
                        name: 'type',
                        inputValue: '1',
//                        action:'clickRdio1',
                    }
//                    {
//                        boxLabel: 'Sub Dealer',
//                        name: 'type',
//                        id: 'subrad',
//                        inputValue: '2'
////                        action:'clickRdio1',
//                    }
                ],
                listeners: {
                    change: function(me)
                    {
                        var val = me.getValue();
                        var cmp1 = Ext.getCmp('dsr');
                        var cmp2 = Ext.getCmp('subdealer');
                        if (val.type === '1')
                        {
                            //////console.log("value 1");
                            cmp2.setVisible(false);
                            cmp1.setVisible(true);
                        }
                        else
                        {
                            //////console.log("value 2");
                            cmp1.setVisible(false);
                            cmp2.setVisible(true);
                        }
                    },
                    beforeRender: function(me) {
                        var check = Ext.getCmp('rad').getValue();
                        var cmp1 = Ext.getCmp('dsr');
                        var cmp2 = Ext.getCmp('subdealer');
                        if (check.type === '1')
                        {
                            cmp2.setVisible(false);
                            cmp1.setVisible(true);
                        }
                        else
                        {
                            cmp1.setVisible(false);
                            cmp2.setVisible(true);
                        }
                    },
                }
            },
            {
                fieldLabel: 'Issuee',
                xtype: 'combobox',
                emptyText: 'Select DSR',
                name: 'dSRId',
                store: 'radioStr',
                id: 'dsr',
                queryMode: 'local',
                displayField: 'bisName',
                valueField: 'bisId',
//                visible: true,
                editable: false,
//                allowBlank: false
            },
            {
                fieldLabel: 'Issuee',
                xtype: 'combobox',
                emptyText: 'Select Sub Dealer',
                name: 'dSRId',
                store: 'radioStr',
                id: 'subdealer',
                queryMode: 'local',
                displayField: 'bisName',
                valueField: 'bisId',
//                visible: true,
                editable: false,
//                allowBlank: false
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Date',
                name: 'issueDate',
//                maxLength: 20,
                //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                maxValue: new Date(),
                format: 'Y-m-d',
                altFormats: 'm/d/Y',
                editable: false,
                value: new Date()
//                vtype: 'ValidUserID', 2012-09-10 00:00:00.0
//                regex: /^[a-zA-Z_]+$/
            }
            ,
            {
                xtype: 'moneyfield',
                fieldLabel: 'Total Price(Rs.)',
                name: 'price',
                align:'right',
                maxLength: 12,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
                readOnly: true,
                maxValue: 999999.99,
                id: 'priceBox'
//                vtype: 'ValidUserID',
//                regex: /^[a-zA-Z_]+$/
            },
//
//            {
//                fieldLabel: 'Distributor Margin',
//                name: 'distributorMargin',
////                maxLength: 30,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false
////                vtype: 'ValidUserID',
////                regex: /^[a-zA-Z_]+$/
//            },
//            {
//                fieldLabel: 'Dealer Margin',
//                name: 'delerMargin',
//                maxLength: 12,
//                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
//                allowBlank: false
////                vtype: 'ValidUserID',
////                regex: /^[a-zA-Z_]+$/
//            },
            {
                fieldLabel: 'Margin/Discount(%)',
                name: 'discount',
                maxLength: 12,
                id: 'marginBox',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
//                vtype: 'ValidUserID',
                value: 0,
                maxValue: 100.00,
                minValue: 1
            },
//
            {
                fieldLabel: 'Status',
                xtype: 'combobox',
                name: 'status',
                store: stat,
//                 maxLength: 2,
//                id: 'cateStatus',
                queryMode: 'local',
                displayField: 'value',
                valueField: 'status',
                editable: false,
                listeners: {
                    scope: this,
                    beforeRender: function(me) {
                        var cmp = Ext.getCmp('remarks');
                        cmp.setValue("Pending_Status");

                        var val = me.getValue();
//                        var cmp = Ext.getCmp('newdeviceissue-remark');
                        if (val === null) {
                            me.setValue(1);
//                            cmp.setValue('empty');
                        }
//                        var cmp = Ext.getCmp('newcategory-remark');
//                        cmp.setVisible(false);
                    },
//                    afterRender: function(me) {
////                        //////console.log(this.next());
//                        var val = me.getValue();
//                        var cmp = Ext.getCmp('newcategory-remark');
//                        if (val === null) {
//                            me.setValue(1);
//                            cmp.setValue('empty');
//                        } else if (val === 1) {
////                            cmp.setVisible(false);
//                            cmp.setValue('empty');
//                        } else {
//                            cmp.setVisible(true);
////                            cmp.setValue('');
//                        }
//                    },
                    change: function(me) {
                        var cmp = Ext.getCmp('remarks');
                        cmp.setVisible(true);
                        cmp.setValue("Pending_Status");
                        cmp.setVisible(true);
                        var val = me.getValue();
                        if (val === 1) {
                            cmp.setVisible(false);
//                            if (cmp.getValue() === '')
                            cmp.setValue('Pending_Status');

                        } else {
                            cmp.setVisible(true);
//                            if (cmp.getValue() === 'Pending_Status')
                            cmp.setValue('');
                        }
                    }
                }
            },
            {
                fieldLabel: 'Remarks',
                name: 'remarks',
                id: 'remarks',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                allowBlank: false,
            },
//
            {
                xtype: 'container',
                layout: 'hbox',
                defaultType: 'button',
                items: [{
//                    itemId: 'showEmployees',
                        margin: '0 0 10 490',
                        text: 'Scan IMEIs',
                        scope: this,
//                    handler: this.onShowEmployeesClick,
                        action: 'scanimei'
                    }]


//                iconAlign: 'right'
            },
//
            {
                xtype: 'grid',
                frame: false,
                id: 'igrid',
                store: 'DeviceIssueIme',
                border: false,
                columns: [
                    {xtype: 'actioncolumn',
                        text: '',
                        align: 'center',
                        flex: 1,
                        items: [{
                                icon: 'resources/images/delete-icon.png',
                                //action: 'remDevIsuItem',
                                tooltip: 'Remove',
                                handler: function(grid, rowIndex, colIndex) {
                                    var rec = grid.getStore().getAt(rowIndex);
                                    var me=this;
                                    Ext.Msg.confirm("Delete", "Are you sure you want to delete?", function(button) {
                                        if (button == 'yes') {
                                            grid.getStore().removeAt(rowIndex);
                                            me.up('window').fireEvent('remDevIsuItem');
                                        } else {
                                            return false;
                                        }
                                    });

                                }
                            }]

                    },
                    {text: 'Model', dataIndex: 'modleDesc', flex: 3},
                    {text: 'Serial No (IMEI)', dataIndex: 'imeiNo', flex: 3},
                    {text: 'Margin(%)', dataIndex: 'margin', flex: 2},
                    {text: 'Net Price(Rs)', dataIndex: 'salesPrice', flex: 2,align:'right'}
                ],
//                 viewConfig: {
////                    emptyText: 'Click a button to show a dataset',
//                    deferEmptyText: false,
//                    stripeRows: false,
//                    loadMask: false
//                }
            }
        ];
    },
    buildButtons: function() {
        return [
            {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Cancel',
                action: 'cancel'
//                id: 'userform-reset-insert'
            }, {
//                iconCls: 'icon-repeat',
                xtype: 'button',
                text: 'Save',
                action: 'save',
                id: 'newdeviceissue-save',
                formBind: true
//                id: 'userform-reset-update'
            },
            {
                xtype: 'button',
                text: 'Create',
//                        ui: 'success',
                action: 'create',
                id: 'newdeviceissue-create',
                formBind: true
            },
//            {
//                xtype: 'button',
//                text: 'test',
//                handler: function(me) {
//                    ////console.log(this);
//                }
//            }
        ];
    },
    buildDockedItems: function() {
        return [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->',
                    {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Cancel',
                        action: 'cancel'
//                id: 'userform-reset-insert'
                    }, {
//                iconCls: 'icon-repeat',
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'newdeviceissue-save',
                        formBind: true
//                id: 'userform-reset-update'
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
//                        ui: 'success',
                        action: 'create',
                        id: 'newdeviceissue-create',
                        formBind: true
                    }
                ]
            }
        ];
    },
    buildPlugins: function() {
        return [];
    }

});
