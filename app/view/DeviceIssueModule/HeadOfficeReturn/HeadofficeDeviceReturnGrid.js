/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('singer.view.DeviceIssueModule.HeadOfficeReturn.HeadofficeDeviceReturnGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.headofficedevicereturngrid',
    loadMask: true,
    store: 'DeviceReturnHeadoffice',
    requires: [
        'Ext.grid.*',
        'Ext.form.field.Text',
        'Ext.ux.grid.FiltersFeature',
        'Ext.toolbar.Paging',
    ],
    forceFit: true,
    minHeight: 200,
//    selType: 'rowmodel',
    //store: 'Users',
    initComponent: function () {
        var usersGrid = this;
        usersGrid.columns = usersGrid.buildColumns();

//        locationMaintenancegrid.dockedItems = locationMaintenancegrid.buildDockedItems();
        usersGrid.dockedItems = usersGrid.buildDockItems(this);
        usersGrid.features = usersGrid.buildFeatures();
        usersGrid.callParent();
    },
    buildDockItems: function (userGrid) {
        return [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
                        xtype: 'pagingtoolbar',
                        store: 'DeviceReturnHeadoffice',
                        dock: 'bottom',
                        displayInfo: true
                    }]
            }, {xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch',
                        handler: function () {

                        },
                        minWidth: 100,
                        minHeight: 10
                    },

                    {
                        iconCls: 'icon-search',
                        text: 'New Head Office Return',
                        action: 'open_window_ho',
                        minWidth: 100,
                        minHeight: 10
                    }]
            }

        ];
    },
    buildColumns: function () {

        return [

            {
                xtype: 'searchablecolumn',
                text: 'Return No',
                dataIndex: 'returnNo',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Return From',
                dataIndex: 'dsrName',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Return To',
                dataIndex: 'distributorName',
                flex: 2
            }, {
                xtype: 'searchablecolumn',
                text: 'Date',
                dataIndex: 'returnDate',
                flex: 2
            },
            {
                xtype: 'searchablecolumn',
                text: 'Return Price',
                dataIndex: 'price',
                align: 'right',
                flex: 2,
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Status',
                dataIndex: 'status',
                align: 'right',
                flex: 2,
                renderer: function (value) {
                    if (value === 1) {
                        return 'Pending';
                    } else if (value === 2) {
                        return 'Approved';
                    }else if(value == 3){
                         return 'Rejected';
                    } else {
                        return '-';
                    }
                }
            },
            {
                xtype: 'searchablecolumn',
                text: 'Reason',
                dataIndex: 'reason',
                flex: 2
            },
            {
                xtype: 'actioncolumn',
                header: 'Accept',
                width: 100,
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/approved.png', // Use a URL in the icon config
                        tooltip: 'Accept',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('accept', grid, record);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {

                            var sto = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');

//                            if (sto === record.getData().bisId) {

                                if (record.get('status') === 1) {
                                    return false;
                                } else {
                                    return true;
                                }
//                            } else {
//                                return true;
//                            }
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                menuDisabled: true,
                text: 'View',
                width: 70,
                items: [{
                        icon: 'resources/images/view-icon.png',
                        tooltip: 'View',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('returnViewho', grid, rec);
                        }
                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Print ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/printerr.png', // Use a URL in the icon config
                        tooltip: 'Print',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('printHoReturn', grid, record);

                        }


                    }]
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'Details ',
                menuDisabled: true,
                items: [{
                        icon: 'resources/images/printerr.png', // Use a URL in the icon config
                        tooltip: 'Print',
                        handler: function (grid, rowIndex, colIndex) {
                            var record = (grid.getStore().getAt(rowIndex));
                            this.up('grid').fireEvent('printHoReturnDtl', grid, record);

                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {

                            var sto = Ext.getStore('LoggedInUserData').data.getAt(0).get('extraParams');

//                            if (sto === record.getData().bisId) {

//                                if (record.get('status') === 2) {
//                                    return true;
//                                } else {
//                                    return false;
//                                }
//                            } else {
//                                return true;
//                            }
                        }

                    }]
            }

        ];
    },
    buildFeatures: function () {
        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [
                {
                    dataIndex: 'returnNo',
                    type: 'int'
//                    width: 50
                }, {
                    type: 'int',
                    dataIndex: 'distributorId'
                }, {
                    type: 'int',
                    dataIndex: 'dsrId'
                }, {
                    dataIndex: 'returnDate',
                    type: 'string'
                }
            ]
        };
        return [filtersCfg];
    }

});
