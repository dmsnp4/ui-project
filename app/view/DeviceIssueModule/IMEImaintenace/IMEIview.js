

Ext.define('singer.view.DeviceIssueModule.IMEImaintenace.IMEIview', {
    extend: 'Ext.window.Window',
    alias: 'widget.imeiGridview',
    iconCls: 'icon-user',
    title: 'Debit Note',
    modal: true,
    constrain: true,
    layout: 'fit',
    width: 430,
    
    initComponent: function () {
        var categoryForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 300,
            overflowY: 'auto',
            bodyPadding: '5 15 5 15',
            defaults: {
                xtype: 'displayfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                beforeLabelTextTpl: '<b>',
                afterLabelTextTpl: '</b>',
                labelWidth: 120,
                margin: '10 0 10 0'
            },
            items: categoryForm.buildItems(),
            buttons: categoryForm.buildButtons()
        });
        categoryForm.items = [formPanel];
        categoryForm.callParent();
    },
    buildItems: function () {

        return [
            {
                fieldLabel: 'Serial No(IMEI):',
                name: 'imeiNo',
                listeners: {
                    afterrender: function(comp) {
                        if (Ext.isEmpty(comp.getValue())) {
                            comp.setValue("-");
                        }else{
                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                        }
                    }
                }
            },
            {
                fieldLabel: 'Model:',
                name: 'modleNo'
            },
            {
                fieldLabel: 'Product:',
                name: 'product'
            },
            {
                fieldLabel: 'Brand:',
                name: 'brand'
            },
            {
                fieldLabel: 'Location:',
                name: 'bisName'
            },
            {
                fieldLabel: 'Sales Price(Rs.):',
                name: 'salesPrice',
                align:'center',
            },
            {
                fieldLabel: 'Name of the Deparatment:',
                name: 'bisName'
            }
            ////class ImeiMaster {
//            String imeiNo;
//            int modleNo;
//            int bisId;
//            String debitnoteNo;
//            String purchaseDate;
//            String brand;
//            String product;
//            String location;
//            double salesPrice;
//            int status;
//            String user;
//}
            
        ];
    },
    buildButtons: function () {
        return ['->', {
                text: 'Done',
                action: 'done'
            }];
    },
    buildDockedItems: function () {
        return [];
    },
    buildPlugins: function () {
        return [];
    }

});



