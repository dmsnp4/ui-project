

Ext.define('singer.view.DeviceIssueModule.IMEImaintenace.IMEIgrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.imeiMaintenaceGrid',
    store: 'IMEImaintenace',
    minHeight: 200,
    selType: 'rowmodel',
    initComponent: function() {
        var debitNoteGrid = this;
        debitNoteGrid.columns = debitNoteGrid.buildColumns();
        debitNoteGrid.dockedItems = debitNoteGrid.buildDockedItems();
        debitNoteGrid.callParent();
    },
    buildColumns: function() {
        return [
            
            {
                xtype: 'searchablecolumn',
                header: 'IMEI',
                dataIndex: 'imeiNo',
                flex:2
            },
            {
                
                xtype: 'searchablecolumn',
                header: 'Bis Locations',
                dataIndex: 'bisName',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                header: 'Model',
                dataIndex: 'modleDesc',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                header: 'ERP Code',
                dataIndex: 'erpCode',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                header: 'Product',
                dataIndex: 'product',
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                header: 'Brand',
                dataIndex: 'brand',
                flex:2
            },
            ////class ImeiMaster {
//            String imeiNo;
//            int modleNo;
//            int bisId;
//            String debitnoteNo;
//            String purchaseDate;
//            String brand;
//            String product;
//            String location;
//            double salesPrice;
//            int status;
//            String user;
//}
//            {
//                xtype: 'searchablecolumn',
//                header: 'Location',
//                dataIndex: 'bisName',
//                flex:2
//            },
            {
               
                xtype: 'searchablecolumn',
                header: 'Sales Price(Rs.)',
//                 width:'150',
                dataIndex: 'salesPrice',
                align:'center',
                renderer: function (value) {
                    return Ext.util.Format.number(value, '0,000.00');
                },
                flex:2
            },
            {
                xtype: 'searchablecolumn',
                header: 'Status',
                flex:2,
                dataIndex: 'status',
                renderer: function (value) {
                    if (value === 1) {
                        return "Active";
                    }
                    if (value === 9) {
                        return "Inactive";
                    }

                }
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: 'View',
                menuDisabled: true,
                flex:1,
                align:'center',
                items: [{
                        icon: 'resources/images/view-icon.png', // Use a URL in the icon config  
                        tooltip: 'View',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
//                            alert("Terminate " + rec.get('first_name'));
                            this.up('grid').fireEvent('IMEIView', grid, rec);
                        }
                    }]
            },
            {
                text: 'Deactivate',
                xtype: 'actioncolumn',
                width: 100,
                menuDisabled: true,
                flex:1,
                align:'center',
                items: [{
                        icon: 'resources/images/deactivate.png', // Use a URL in the icon config
                        tooltip: 'Deacivate',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('deactivate1', grid, record);
//                            //console.log(this.up('grid'));

                        }
                    }]
            },
            {
                text: 'Activate',
                xtype: 'actioncolumn',
                width: 100,
                menuDisabled: true,
                flex:1,
                align:'center',
                items: [{
                        icon: 'resources/images/deactivate.png', // Use a URL in the icon config
                        tooltip: 'Deacivate',
                        handler: function(grid, rowIndex, colIndex) {
                            var record = grid.getStore().getAt(rowIndex);
                            this.up('grid').fireEvent('activate1', grid, record);
                        }
                    }]
            },

        ];
    },
    buildDockedItems: function() {
        return [
            {   xtype: 'form',
                bodyPadding: 2,
                frame: false,
                border: false,
                fieldDefaults: {
                    labelWidth: 100,
                    labelAlign: 'top'
                },
                buttons: [{
                        iconCls: 'icon-search',
                        text: 'Clear Search',
                        action: 'clearSearch'
                                //  id: 'suppliersgrid_search'
                    },'->']
            }, {
                xtype: 'pagingtoolbar',
                store: 'IMEImaintenace',
                dock: 'bottom',
                displayInfo: true
            }
        ];
    },
    buildPlugins: function() {
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });
        return [rowEditing];
    }
});