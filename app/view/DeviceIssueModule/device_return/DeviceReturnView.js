Ext.define('singer.view.DeviceIssueModule.device_return.DeviceReturnView', {
	extend: 'Ext.window.Window',
	alias: 'widget.devicereturnview',
	loadMask: true,
	//    xtype:'userview',
	iconCls: 'icon-user',
	title: 'System User Details',
	modal: true,
	constrain: true,
	layout: 'fit',
	width: 430,
	//    height: 240,
	initComponent: function () {
		var categoryForm = this;
		var formPanel = Ext.create('Ext.form.Panel', {
			width: 300,
			//            height: 340,
			overflowY: 'auto',
			bodyPadding: '5 15 5 15',
			defaults: {
				xtype: 'displayfield'
			},
			fieldDefaults: {
				anchor: '100%',
				labelAlign: 'left',
				beforeLabelTextTpl: '<b>',
				afterLabelTextTpl: '</b>',
				labelWidth: 120,
				margin: '10 0 10 0'
			},
			items: categoryForm.buildItems(),
			buttons: categoryForm.buildButtons()
		});
		categoryForm.items = [formPanel];
		categoryForm.callParent();
	},
	buildItems: function () {
		return [
 //            {
 //                fieldLabel: 'User Type',
 //                name: 'user_type',
 //                listeners: {
 //                    afterrender: function(comp) {
 //                        if (parseInt(comp.getValue()) === 1)
 //                            comp.setValue('Admin');
 //                        if (parseInt(comp.getValue()) === 2)
 //                            comp.setValue('Default User');
 //                        if (parseInt(comp.getValue()) === 3)
 //                            comp.setValue('Inquiry User');
 //                    }
 //                }
 //
 //            }, 
			{
				fieldLabel: 'Sent To',
				name: 'distributorName',
                                listeners: {
                                    afterrender: function(comp) {
                                        if (Ext.isEmpty(comp.getValue())) {
                                            comp.setValue("-");
                                        }else{
                                            comp.setValue("<strong style='font-size: 20px;'>" + comp.getValue() + "</strong>");
                                        }
                                    }
                }
            },
			{
				fieldLabel: 'Date',
				name: 'returnDate'
            },

			{
				fieldLabel: 'Status',
				name: 'status',
				renderer: function (value) {
					if (value === '1') {
						return "Active";
					}
					if (value === '2') {
						return "Inactive";
					}

				}
            },
			{
				xtype: 'grid',
				//				frame: false,
				id: 'lvgrid',
				store: 'DevReturnTemp',
				//				border: false,
				columns: [
					{
						text: 'Model',
						dataIndex: 'modleDesc',
						flex: 1
					},
					{
						text: 'Serial No (IMEI)',
						dataIndex: 'imeiNo',
						flex: 1
					},
                                        {
                                            text: 'Net Price', 
                                            dataIndex: 'salesPrice',
                                            align:'center',
                                            flex: 1
                                        }
                            ],
				//                 viewConfig: {
				////                    emptyText: 'Click a button to show a dataset',
				//                    deferEmptyText: false,
				//                    stripeRows: false,
				//                    loadMask: false
				//                }
            }
        ];
	},
	buildButtons: function () {
		return ['->', {
			text: 'Done',
			action: 'cancel'
            }];
	},
	buildDockedItems: function () {
		return [];
	},
	buildPlugins: function () {
		return [];
	}

});