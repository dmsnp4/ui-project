Ext.define('singer.view.DeviceIssueModule.device_return.Detail', {
    extend: 'Ext.form.Panel',
    alias: 'widget.returndetail',
    modal: true,
    layout: 'fit',
    constrain: true,
    width: 500,
    resizable: false,
    loadMask: true,
    initComponent: function () {

        var newUserForm = this;
        var formPanel = Ext.create('Ext.form.Panel', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            defaults: {
                xtype: 'textfield'
            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildItems(),
        });
        var groupItems = Ext.create('Ext.container.Container', {
            width: 500,
            bodyPadding: 5,
            overflowY: 'auto',
            //            },
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            items: newUserForm.buildGroupItems()
        });
        var formBottom = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.bottomButtons(),
        });

        var formTop = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            overflowY: 'auto',
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'left',
                msgTarget: 'side',
                labelWidth: 180,
                margin: '10 0 10 0'
            },
            buttons: newUserForm.topButtons(),
        });

        newUserForm.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {},
                items: [
                    formTop,
                    {
//                        xtype: 'label',
//                        id: 'DebitNtNo',
//                        text: '',
//                        style: {
//                            //float: 'right',
//                            'text-align': 'right',
//                            margin: '5px',
//                            'font-weight': 'bold',
//                            color: '#333'
//                        }
                    },
                    groupItems,
                    formPanel,
                    formBottom
                ]
            }
        ];
        newUserForm.callParent();
    },
    buildGroupItems: function () {
        return Ext.create('Ext.Panel', {
            width: 500,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    flex: 3,
                    items: [
                        {
                            xtype: 'grid',
                            height: 400,
                            id: 'debitNoteImei',
                            columns: [
                                {
                                    text: 'Returned IMEI',
                                    dataIndex: 'imeiNo',
                                    flex: '1',
                                    menuDisabled: true
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    flex: 1,
                    items: [
                        {
                            xtype: 'button',
                            text: '<',
                            action: 'prevOne',
                            tooltip: 'Shift One IMEI',
                            margin: '150 0 0 18',
                        },
                        {
                            xtype: 'button',
                            text: '<<',
                            action: 'prevAll',
                            tooltip: 'Shift All IMEI',
                            margin: '20 0 0 18',
                        }
                    ]
                }, {
                    xtype: 'panel',
                    flex: 3,
                    items: [
                        Ext.create('Ext.grid.Panel',
                                {
                                    id: 'acceptedImei', //Do not duplicate the name
                                    height: 400,
                                    store: 'DeviceReturnDetail',
                                    columns: [
                                        {
                                            text: 'Accepted IMEI',
                                            dataIndex: 'imeiNo',
                                            flex: '1',
                                            menuDisabled: true,
                                        }
                                    ]
                                })
                    ]
                }]
        });
    },
    buildItems: function () {
        return [
            {
                fieldLabel: 'IMEI',
                id: 'imeiID',
                name: ''
            }
        ];
    },
    bottomButtons: function () {
        return [
            {
                xtype: 'button',
                text: 'Scan',
                action: 'scan'
            },
//            {
//                xtype: 'button',
//                text: 'Remove',
//                formBind: true
//            },
            '->',
            {
                xtype: 'button',
                text: 'Accept',
                action: 'accept',
                formBind: true
            },
            {
                xtype: 'button',
                id: 'doneBtn',
                text: 'Done',
                action: 'done',
                formBind: true
            },
        ];
    },
    topButtons: function () {
    },
    buildPlugins: function () {
        return [];
    }

});
