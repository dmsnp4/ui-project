Ext.define('singer.view.ReportsModule.ReportsMenu',
        {
            extend: 'Ext.grid.Panel',
            alias: 'widget.reportsmenu',
            loadMask: true,
            store: 'ReadExcel',
            requires: [
                'Ext.grid.*',
                'Ext.form.field.Text',
                'Ext.ux.grid.FiltersFeature',
                'Ext.toolbar.Paging'
            ],
            minHeight: 200,
            initComponent: function ()
            {
                var reportsGrid = this;
                reportsGrid.columns = reportsGrid.buildColumns();
                reportsGrid.dockedItems = reportsGrid.buildDockItems(this);
                reportsGrid.features = reportsGrid.buildFeatures();
                reportsGrid.callParent();
            },
            buildDockItems: function (userGrid)
            {
                return [
                    {
                        xtype: 'toolbar',
                        dock: 'bottom',
                        items: [
                            {
                                xtype: 'pagingtoolbar',
                                store: 'ReadExcel',
                                dock: 'bottom',
                                displayInfo: true
                            }
                        ]
                    },
                    {
                        xtype: 'form',
                        frame: false,
                        border: false,
                        dock: 'top',
                        buttons: [
                            {
                                text: 'Add New Report',
                                action: 'addNewReportBookWindow'
                            }
                        ]
                    }
                ];
            },
            buildColumns: function ()
            {
                var status = Ext.create('Ext.data.Store',
                        {
                            fields: ['name', 'value'],
                            data: [
                                {
                                    "status": '1',
                                    "value": "Active"
                                },
                                {
                                    "status": '9',
                                    "value": "Inactive"
                                }
                            ]
                        }
                );
                return [
                    {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Report Name',
                        dataIndex: 'reportName',
                        flex: 2
                    },
                    {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Condition 1',
                        dataIndex: 'cond1a',
                        flex: 2
                    },
                     {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Condition 2',
                        dataIndex: 'cond2a',
                        flex: 2
                    },
                     {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Condition 3',
                        dataIndex: 'cond3a',
                        flex: 2
                    },
                     {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Condition 4',
                        dataIndex: 'cond4a',
                        flex: 2
                    },
                     {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Condition 5',
                        dataIndex: 'cond5a',
                        flex: 2
                    },
                     {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Condition 6',
                        dataIndex: 'cond6a',
                        flex: 2
                    },
                    {
                        sortable: false,
                        menuDisabled: true,
                        text: 'Status',
                        dataIndex: 'status',
                        flex: 2,
                        renderer: function (value)
                        {
                            if (value === 1) {
                                return "Active";
                            }
                            if (value === 9) {
                                return "Inactive";
                            }

                        }
                    },
                    {
                        xtype: 'actioncolumn',
                        width: 50,
                        text: 'Generate Report',
                        flex: 2,
                        items: [
                            {
                                xtype: 'button',
                                tooltip: 'Report',
                                scale: 'large',
                                icon: 'resources/images/details.png',
                                handler: function (grid, rowIndex, colIndex)
                                {
                                    var record = (grid.getStore().getAt(rowIndex));
                                    this.up('grid').fireEvent('genExcel', grid, record);
                                },
                                isDisabled: function (view, rowIndex, colIndex, item, record) 
                                {
                                    if (record.get('status') === 9) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'actioncolumn',
                        width: 50,
                        text: 'Edit Report',
                        flex: 1,
                        items: [
                            {
                                xtype: 'button',
                                tooltip: 'Edit',
                                scale: 'large',
                                icon: 'resources/images/details.png',
                                handler: function (grid, rowIndex, colIndex)
                                {
                                    var record = (grid.getStore().getAt(rowIndex));
                                    this.up('grid').fireEvent('EditRep', grid, record);
                                }
                            }
                        ]
                    }
                ];
            },
            buildFeatures: function ()
            {

            }

        }
);