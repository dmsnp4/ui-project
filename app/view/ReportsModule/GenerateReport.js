Ext.define('singer.view.ReportsModule.GenerateReport',
        {
            extend: 'Ext.window.Window',
            alias: 'widget.GenerateReport',
            id: 'generateReport',
            title: 'Generate Report',
            modal: true,
            layout: 'fit',
            constrain: true,
            width: 500,
            height: 400,
            resizable: true,
            loadMask: true,
            initComponent: function ()
            {
                var window = this;
                var formPanel = Ext.create('Ext.form.Panel',
                        {
                            width: 500,
                            height: 200,
                            bodyPadding: 5,
                            overflowY: 'auto',
                            defaults:
                                    {
                                        xtype: 'textfield'
                                    },
                            fieldDefaults:
                                    {
                                        anchor: '100%',
                                        labelAlign: 'left',
                                        msgTarget: 'side',
                                        labelWidth: 180,
                                        margin: '10 0 10 0'
                                    },
                            items: window.buildItems(),
                            buttons: window.buildButtons()

                        }
                );

                window.items = [formPanel];
                window.callParent();

            },
            buildItems: function ()
            {
                return [
                    {
                        fieldLabel: 'Report Name',
                        name: 'reportName',
                        id: 'repName',
                        maxLength: 20,
                        emptyText: 'Reprot Name',
                        value: localStorage.getItem("reptName"),
                        allowBlank: true,
                        readOnly: true
                    },
                     {
                        fieldLabel: localStorage.getItem("cond1a"),
                        name: 'cond1',
                        id: 'sql-cond1',
                        maxLength: 90,
                        emptyText: 'VALUE',
                        allowBlank: false
                    },
                    {
                        fieldLabel: localStorage.getItem("cond2a"),
                        name: 'cond2',
                        id: 'sql-cond2',
                        maxLength: 90,
                        emptyText: 'VALUE',
                        allowBlank: false
                    },
                    {
                        fieldLabel: localStorage.getItem("cond3a"),
                        name: 'cond3',
                        id: 'sql-cond3',
                        maxLength: 90,
                        emptyText: 'VALUE',
                        allowBlank: false
                    },
                    {
                        fieldLabel: localStorage.getItem("cond4a"),
                        name: 'cond4',
                        id: 'sql-cond4',
                        maxLength: 90,
                        emptyText: 'VALUE',
                        allowBlank: true
                    },
                    {
                        fieldLabel: localStorage.getItem("cond5a"),
                        name: 'cond5',
                        id: 'sql-cond5',
                        maxLength: 90,
                        emptyText: 'VALUE',
                        allowBlank: true
                    },
                    {
                        fieldLabel: localStorage.getItem("cond6a"),
                        name: 'cond6',
                        id: 'sql-cond6',
                        maxLength: 90,
                        emptyText: 'VALUE',
                        allowBlank: true
                    }
                ];
            },
            buildButtons: function ()
            {
                return [
                    {
                        xtype: 'button',
                        text: 'Generate Report',
                        action: 'generateReport',
                        id: 'generate-report',
                        formBind: true
                    },
                    {
                        xtype: 'button',
                        text: 'Generate Report1',
                        id: 'GenerateReport-save',
                        hidden : true
                    },
                    {
                        xtype: 'button',
                        text: 'Generate Report2',
                        id: 'GenerateReport-create',
                        hidden : true
                    }
                ];
            },
            buildPlugins: function ()
            {
                return [];
            }

        }
);