Ext.define('singer.view.ReportsModule.addReportBook',
        {
            extend: 'Ext.window.Window',
            alias: 'widget.addReportBook',
            id: 'addreportbook',
            title: 'Add new report Query',
            modal: true,
            layout: 'fit',
            constrain: true,
            width: 500,
            resizable: true,
            loadMask: true,
            initComponent: function ()
            {
                var window = this;
                var formPanel = Ext.create('Ext.form.Panel',
                        {
                            width: 500,
                            height: 200,
                            bodyPadding: 5,
                            overflowY: 'auto',
                            defaults:
                                    {
                                        xtype: 'textfield'
                                    },
                            fieldDefaults:
                                    {
                                        anchor: '100%',
                                        labelAlign: 'left',
                                        msgTarget: 'side',
                                        labelWidth: 180,
                                        margin: '10 0 10 0'
                                    },
                            items: window.buildItems(),
                            buttons: window.buildButtons()

                        }
                );

                window.items = [formPanel];
                window.callParent();

            },
            buildItems: function ()
            {

                var stat = Ext.create('Ext.data.Store',
                        {
                            fields: ['status', 'value'],
                            data: [
                                {
                                    "status": 1,
                                    "value": "Active"
                                },
                                {
                                    "status": 9,
                                    "value": "Inactive"
                                }
                            ]
                        }
                );

                return [
                    {
                        fieldLabel: 'Report Name',
                        name: 'reportName',
                        id: 'repName',
                        maxLength: 20,
                        emptyText: 'Reprot Name',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',
                        allowBlank: false
                    },
                    {
                        fieldLabel: 'Query',
                        name: 'reportQuery',
                        emptyText: 'SELECT example\n\FROM example',
                        xtype: 'textareafield',
                        id: 'repQuery',
                        allowBlank: false
                    },
                     {
                        fieldLabel: 'Condition 1',
                        name: 'cond1',
                        id:'sql-cond1',
                        maxLength: 90,
                        emptyText: 'COLUMN',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 1 Alias',
                        name: 'cond1a',
                        id:'sql-cond1a',
                        maxLength: 90,
                        emptyText: 'ALIAS',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 2',
                        name: 'cond2',
                        id:'sql-cond2',
                        maxLength: 90,
                        emptyText: 'COLUMN',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 2 Alias',
                        name: 'cond2a',
                        id:'sql-cond2a',
                        maxLength: 90,
                        emptyText: 'ALIAS',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 3',
                        name: 'cond3',
                        id:'sql-cond3',
                        maxLength: 90,
                        emptyText: 'COLUMN',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 3 Alias',
                        name: 'cond3a',
                        id:'sql-cond3a',
                        maxLength: 90,
                        emptyText: 'ALIAS',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 4',
                        name: 'cond4',
                        id:'sql-cond4',
                        maxLength: 90,
                        emptyText: 'COLUMN',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 4 Alias',
                        name: 'cond4a',
                        id:'sql-cond4a',
                        maxLength: 90,
                        emptyText: 'ALIAS',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 5',
                        name: 'cond5',
                        id:'sql-cond5',
                        maxLength: 90,
                        emptyText: 'COLUMN',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 5 Alias',
                        name: 'cond5a',
                        id:'sql-cond5a',
                        maxLength: 90,
                        emptyText: 'ALIAS',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 6',
                        name: 'cond6',
                        id:'sql-cond6',
                        maxLength: 90,
                        emptyText: 'COLUMN',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Condition 6 Alias',
                        name: 'cond6a',
                        id:'sql-cond6a',
                        maxLength: 90,
                        emptyText: 'ALIAS',
                        allowBlank: true
                    },
                    {
                        fieldLabel: 'Active Status',
                        xtype: 'combobox',
                        name: 'status',
                        id: 'status',
                        editable: false,
                        allowBlank: false,
                        emptyText: 'Status',
                        store: stat,
                        queryMode: 'local',
                        displayField: 'value',
                        valueField: 'status'
                    }
                ];
            },
            buildButtons: function ()
            {
                return [
                    {
                        xtype: 'button',
                        text: 'Save',
                        action: 'save',
                        id: 'addReportBook-save',
                        formBind: true
                    },
                    {
                        xtype: 'button',
                        text: 'Create',
                        action: 'create',
                        id: 'addReportBook-create',
                        formBind: true
                    }
                ];
            },
            buildPlugins: function ()
            {
                return [];
            }

        }
);