Ext.define("singer.AppParams", {
    singleton: true,
    SYSTEM_CODE: 'SingerISS',
    //Changed for Dev by Hashan
    //BASE_PATH: "http://"+(window.location.href).split("/")[2]+"/App",
    BASE_PATH: "http://" + (window.location.href).split("/")[2] + "/singerMIX_P4",
    //Changed for Dev by Hashan
//    JBOSS_PATH: "http://" + (window.location.href).split("/")[2] + "/SingerPortalWebService-1.0/Services/",
    JBOSS_PATH: "http://203.189.68.156:8181/SingerPortalWebService-1.0/Services/",
//    JBOSS_PATH: "http://dmsn.lk:8080/SingerPortalWebService-1.0/Services/",
    // JBOSS_PATH: "http://192.0.0.222:8080/SingerPortalWebService-1.0/Services/",
    //JBOSS_PATH: "http://192.0.0.60:8080/SingerPortalWebService-1.0/Services/",
    //Changed for Dev by Hashan
    REPO: "http://" + (window.location.href).split("/")[2] + "/DMS_FA_BIRT/",
    REPO2: "http://" + (window.location.href).split("/")[2] + "/DMS_FA_BIRT/",
    //REPO:"http://192.0.0.60:8080/FA_BIRT/",
    REPOTYPE: 'pdf',
    ATTEMPTS: 5,
    TIMEOUT: 6000,
    IDLE_TIMEOUT: 600,
    PAGE_SIZE: 25,
    INFINITE_PAGE: 999999,
    CURRENCY_CODE: 'LKR',
    TEMP: '',
//    dashbuilder:"http://"+(window.location.href).split("/")[2]+"/dashbuilder",
//    NBT:4/100,
//    TAX:12.5/100,
    NBT: 2 / 100,
    TAX: 15 / 100,
    ACCEPT_DEBIT_IMEI_LIMIT: 100
});



