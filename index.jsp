<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>singer</title>
        <!-- <x-compile> -->
        <!-- <x-bootstrap> -->
        <link rel="stylesheet" href="bootstrap.css">
        <link rel="stylesheet" href="resources/css/GridFilters.css">
        <link rel="stylesheet" href="resources/css/RangeMenu.css">
        <script src="ext/ext-dev.js"></script>
        <script src="bootstrap.js"></script>
        <script src="resources/js/jquery.min.js"></script>
        <!-- </x-bootstrap> -->
        <script src="app.js"></script>
        <script>
            window.onbeforeunload = function (e) {
            var e = e || window.event;
            var msg = "Do you really want to leave this page?"

            // For IE and Firefox
            if (e) {
                e.returnValue = msg;
            }

            // For Safari / chrome
            return msg;
         };

        </script>

        <script>
            window.onbeforeunload = function (e) {
            var e = e || window.event;
            var msg = "Integrated Solution for Singer Digital Media-SINGER (Sri Lanka) PLC";
            if (e) {
                e.returnValue = msg;
            }
            return msg;
         };
        </script>
        <!-- </x-compile> -->
    </head>
    <body></body>
</html>
