/* 
 * @author Hashan Madushanka Jayakody
 * 
 * Copyright (C) Data Management Systems Software Division- All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * Proprietary and confidential
 * 
 * Written by <Hashan Madushanka Jayakody> <hashan41@gmail.com>, September 2014
 */
Ext.define('Ext.ux.grid.column.SearchableColumn', {
    extend: 'Ext.grid.column.Column',
    alternateClassName: 'Ext.ux.SearchableColumn',
    alias: 'widget.searchablecolumn',
    searchableField: null,
    sortable: true,
    searchable: true,
    initComponent: function () {
        var me = this,
                renderer,
                listeners;

        if (me.header != null) {
            me.text = me.header;
            me.header = null;
        }

        if (!me.triStateSort) {
            me.possibleSortStates.length = 2;
        }
        //console.log(me.dataIndex);
        me.renderTpl = new Ext.XTemplate(
                '<div id="{id}-titleEl" {tipMarkup}class="' + Ext.baseCSSPrefix + 'column-header-inner">' +
                '<span id="{id}-textEl" class="' + Ext.baseCSSPrefix + 'column-header-text' +
                '{childElCls}">' +
                '{text}' +
                '</span>' +
                '<div style="width: 100%;">' +
                '   <div style="float: left; width: 20px;" class="grid-button" ></div>' +
                '   <div class="grid-text-field" ></div>' +
                '</div>' +
                '</div>' +
                '{%this.renderContainer(out,values)%}'
                );
        var searchField = Ext.create('Ext.form.field.Text', {
            name: me.dataIndex,
            margin: '4 0 4 0',
//            width: '100%',
            enableKeyEvents: true,
            listeners: {
//                keyup: me.onSearchFieldKeyReleased,
                specialkey: me.onSearchFieldKeyReleased,
                scope: this,
//                specialkey: function (f, e) {
//                    if (e.getKey() === e.ENTER) {
//                        me.onSearchFieldKeyReleased;
//                    }
//                }
            }
        });

        var btn_asc = Ext.create('Ext.button.Button', {
            name: me.accending,
//            margin: '4 0 4 0',
//            width: '100%',
//            enableToggle: true,
            height: 12,
            //icon: 'resources/images/sort_asc.png',
            //text: '<b>^</b>',
            cls: 'toparrow',
            listeners: {
                scope: this,
                click: me.sortAsc
            }
        });
        var btn_desc = Ext.create('Ext.button.Button', {
            name: me.accending,
//            margin: '4 0 4 0',
//            width: '100%',
//            enableToggle: true,
            height: 12,
            cls: 'bottomarrow',
            //icon: 'resources/images/sort_desc.png',
            listeners: {
                scope: this,
                click: me.sortDesc
            }
        });
        var btnContainer = Ext.create('Ext.container.Container', {
            layout: {
                type: 'vbox',
                align: 'stretchmax',
                pack: 'start'
            },
            items: [
                btn_asc,
                btn_desc
            ]
        });
        me.on('render', function () {
            // render text field to the <li class=".custom-text-field"></li>

            if (me.sortable === true) {
                //btn_asc.render(me.getEl().down('.grid-button'));
                //btn_desc.render(me.getEl().down('.grid-button'));
                btnContainer.render(me.getEl().down('.grid-button'));
            }
            if (me.searchable === true) {
                searchField.render(me.getEl().down('.grid-text-field'));
                me.searchableField = searchField;
            }
        });
        // A group header; It contains items which are themselves Headers
        if (me.columns != null) {
            me.isGroupHeader = true;

            //<debug>
            if (me.dataIndex) {
                Ext.Error.raise('Ext.grid.column.Column: Group header may not accept a dataIndex');
            }
            if ((me.width && me.width !== Ext.grid.header.Container.prototype.defaultWidth) || me.flex) {
                Ext.Error.raise('Ext.grid.column.Column: Group header does not support setting explicit widths or flexs. The group header width is calculated by the sum of its children.');
            }

            //</debug>

            // The headers become child items
            me.items = me.columns;
            me.columns = me.flex = me.width = null;
            me.cls = (me.cls || '') + ' ' + me.groupHeaderCls;

            // A group cannot be sorted, or resized - it shrinkwraps its children
            me.sortable = me.resizable = false;
            me.align = 'center';
        } else {
            // Flexed Headers need to have a minWidth defined so that they can never be squeezed out of existence by the
            // HeaderContainer's specialized Box layout, the ColumnLayout. The ColumnLayout's overridden calculateChildboxes
            // method extends the available layout space to accommodate the "desiredWidth" of all the columns.
            if (me.flex) {
                me.minWidth = me.minWidth || Ext.grid.plugin.HeaderResizer.prototype.minColWidth;
            }
        }
        me.addCls(Ext.baseCSSPrefix + 'column-header-align-' + me.align);

        renderer = me.renderer;
        if (renderer) {
            // When specifying a renderer as a string, it always resolves
            // to Ext.util.Format
            if (typeof renderer == 'string') {
                me.renderer = Ext.util.Format[renderer];
            }
            me.hasCustomRenderer = true;
        } else if (me.defaultRenderer) {
            me.scope = me;
            me.renderer = me.defaultRenderer;
        }

        // Initialize as a HeaderContainer
        me.callParent(arguments);
        listeners = {
            click: me.onTitleElClick,
            scope: me
        };
        me.on(listeners);

    },
    onTitleElDblClick: function (e, t) {

    },
    onTitleElClick: function (e, t) {
        this.searchableField.focus(true, false);
    },
    onSearchFieldKeyReleased: function (searchField, e) {
        if (e.getKey() === e.ENTER) {
            this.searchStoreByQuery();
            this.toggleBindListeners(false);
        }

    },
    toggleBindListeners: function (isBind) {
        var me = this;
        var tablePanel = this.up('gridpanel'), allColumns = tablePanel.columns;
        for (var i = 0; i < allColumns.length; i++) {
            if (!Ext.isEmpty(allColumns[i].searchableField)) {
                var field = allColumns[i].searchableField;
                if (isBind) {
                    //console.log("ssssssssssssssssssssssssssssss");
                    field.setReadOnly(false);
//                    field.mon({
//                        keyup : me.onSearchFieldKeyReleased,
//                        scope : this
//                    });
                } else {
                    field.setReadOnly(true);
                    //field.clearManagedListeners();
                }
            }
        }
    },
    getStoreProxy: function (store) {
        return !Ext.isEmpty(store) ? (!Ext.isEmpty(store.getProxy()) ? store.getProxy().type : false) : false;
    },
    searchStoreByQuery: function () {
        var query = "";
        var tablePanel = this.up('gridpanel'), store = tablePanel.store, allColumns = tablePanel.columns;
        for (var i = 0; i < allColumns.length; i++) {
            if (!Ext.isEmpty(allColumns[i].searchableField)) {
                query += this.getDefaultQueryStringURL(allColumns[i].dataIndex, allColumns[i].searchableField.getValue());
            }
        }
        query = !Ext.isEmpty(query) ? query.substring(1, query.length) : query;
        this.beforeLoadStore(store, query);
    },
    getDefaultQueryStringURL: function (key, value) {
        return !Ext.isEmpty(value) ? "&" + key + "=" + value : "";
    },
    getProxyReadURI: function (store) {
        switch (store.getProxy().type) {
            case "rest" :
                return store.getProxy().api.read;
            case "ajax" :
                return store.getProxy().url;
            case "jsonp" :
                return store.getProxy().url;
            default :
                return Ext.Error.raise("Searchable column only support with rest, ajax or jsonp proxies.");
        }
    },
    setProxyReadURI: function (store, query) {
        switch (store.getProxy().type) {
            case "rest" :
                store.getProxy().api.read = this.resetProxy(this.getProxyReadURI(store)) + "?" + query;
                return store;
            case "ajax" :
                store.getProxy().url = this.resetProxy(this.getProxyReadURI(store)) + "?" + query;
                return store;
            case "jsonp" :
                store.getProxy().url = this.resetProxy(this.getProxyReadURI(store)) + "?" + query;
                return store;
            default :
                Ext.Error.raise("Searchable column only support with rest, ajax or jsonp proxies.");
                break;
        }
    },
    resetProxy: function (proxyURI) {
        return proxyURI.indexOf("?") !== -1 ? proxyURI.split("?")[0] : proxyURI;
    },
    beforeLoadStore: function (store, query) {
        store = this.setProxyReadURI(store, query);
        this.loadStore(store);
    },
    loadStore: function (store) {
        var me = this;
        store.loadPage(1, {
            callback: function (records, operation, success) {
                me.toggleBindListeners(true);
                if (Ext.getBody().isMasked()) {
                    Ext.getBody().unmask();
                }
            }
        });
    },
    sortAsc: function (me) {
        this.sortColumn(this.dataIndex, "ASC");
    },
    sortDesc: function (me) {
        this.sortColumn(this.dataIndex, "DESC");
    },
    sortColumn: function (key, option) {
        var tablePanel = this.up('gridpanel'), store = tablePanel.store;
//        var temp = store.getProxy().api.read;
        var read = this.getProxyReadURI(store);
        var read = this.resetProxy(read);
        if (read.indexOf("?") === -1) {
//                read += "?" + '';
            read = read + '?order=' + key + '&type=' + option + '';
        } else {
            read = read + '&order=' + 'key' + '&type=' + key + '';
        }
        store.getProxy().api.read = read;
        this.loadStore(store);
        console.log('Sort ' + key + "column in " + option + " Store : " + read);
    }
});
