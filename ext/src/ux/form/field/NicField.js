/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('Ext.ux.form.field.NicField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.nicfield',
    maxLength: '10',
    minLength: '10',
    initComponent: function() {
        var me = this;

        if (me.allowOnlyWhitespace === false) {
            me.allowBlank = false;
        }

        me.callParent();

        me.addEvents(
                /**
                 * @event autosize
                 * Fires when the **{@link #autoSize}** function is triggered and the field is resized according to the
                 * {@link #grow}/{@link #growMin}/{@link #growMax} configs as a result. This event provides a hook for the
                 * developer to apply additional logic at runtime to resize the field if needed.
                 * @param {Ext.form.field.Text} this This text field
                 * @param {Number} width The new field width
                 */
                'autosize',
                /**
                 * @event keydown
                 * Keydown input field event. This event only fires if **{@link #enableKeyEvents}** is set to true.
                 * @param {Ext.form.field.Text} this This text field
                 * @param {Ext.EventObject} e
                 */
                'keydown',
                /**
                 * @event keyup
                 * Keyup input field event. This event only fires if **{@link #enableKeyEvents}** is set to true.
                 * @param {Ext.form.field.Text} this This text field
                 * @param {Ext.EventObject} e
                 */
                'keyup',
                /**
                 * @event keypress
                 * Keypress input field event. This event only fires if **{@link #enableKeyEvents}** is set to true.
                 * @param {Ext.form.field.Text} this This text field
                 * @param {Ext.EventObject} e
                 */
                'keypress'
                );
        me.addStateEvents('change');
        me.setGrowSizePolicy();
        Ext.apply(Ext.form.field.VTypes, {
            ValidNic: function(val, field) {
                var invalidNicNo = false;
                //check has V or X OR has V or X more than one
                if ((field.getValue().indexOf("V") === -1 && field.getValue().indexOf("X") === -1) || (field.getValue().indexOf("V") !== -1 && field.getValue().indexOf("X") !== -1))
                {
                    invalidNicNo = true;
                } else {
                    if ((field.getValue().indexOf("V") !== 9) && (field.getValue().indexOf("X") !== 9)) {
                        invalidNicNo = true;
                    }
                }
                return !invalidNicNo;
            },
            ValidNicText: 'Enter Correct NIC Number. Eg: 123456789V or X'
        });
        this.vtype = 'ValidNic';
    },
    onChange: function(newVal, oldVal) {
        this.callParent(arguments);
        this.autoSize();

        //Replace Symbols
        var validatedVal = newVal;

        var validatedVal = newVal.toUpperCase();

        var symbolReplace = /[!@^()_+=`;',.$/[\]\\\~#%&*{}/:<>?|\"-]/;
        validatedVal = validatedVal.replace(symbolReplace, '');

        //Replace spaces
        var spaceReplace = /^\s\s*/;
        validatedVal = validatedVal.replace(spaceReplace, '');

        validatedVal = validatedVal.replace(/[^VX1234567890]/, '');

        if ((validatedVal.match(/V/g) || []).length > 1) {
            validatedVal = validatedVal.replace("V", "");
        }

        if ((validatedVal.match(/X/g) || []).length > 1) {
            validatedVal = validatedVal.replace("X", "");
        }

        if (validatedVal.length > 10) {
            validatedVal = validatedVal.substring(0, 10);
        }

        this.setValue(validatedVal);
    },
    postBlur: function() {
        this.callParent(arguments);
        this.applyEmptyText();
    }
});