/**
 * @author Hashan Madushanka Jayakody
 * @class Ext.ux.form.field.FormSeperator
 * 
 * Provides an easy to group fields in form 
 */
Ext.define('Ext.ux.form.field.FormHeader', {
    extend: 'Ext.Component',
    alias: 'widget.formheader',
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            height: 33,
            style: {
                width: '100%',
                borderBottom: '1px solid #e5e5e5',
                color: '#333333',
                marginBottom: '10px',
                fontSize: '18px',
                lineHeight: '33px',
                fontWeight : 600
            }
        });
        me.callParent(arguments);
    }
});

