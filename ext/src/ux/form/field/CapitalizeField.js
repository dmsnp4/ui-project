/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('Ext.ux.form.field.CapitalizeField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.capfield',
    noSymbols: true,
    noSpaces: true,
    onChange: function(newVal, oldVal) {
        this.callParent(arguments);
        this.autoSize();
        var validatedVal = newVal.toUpperCase();
        if (this.noSymbols) {
            var symbolReplace = /[!@^()_+=`;',.$/[\]\\\~#%&*{}/:<>?|\"-]/;
            validatedVal = validatedVal.replace(symbolReplace, '');
        }
        if (this.noSpaces) {
            var spaceReplace = /^\s\s*/;
            validatedVal = validatedVal.replace(spaceReplace, '');
        }
        this.setValue(validatedVal);
    }
});