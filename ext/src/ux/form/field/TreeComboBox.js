Ext.define('Ext.ux.form.field.TreeComboBox', {
    extend: 'Ext.form.FieldContainer',
    layout: {
        type: 'column'
    },
    filterTextField: null,
    triggerButton: null,
    hiddenValueHolder: null,
    labelAlign: 'right',
    alias: 'widget.treecombo',
    requires: [
        'Ext.form.field.Text',
        'Ext.form.field.Hidden',
        'Ext.button.Button',
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.tree.*'
    ],
    store: '',
    treeStore: '',
    displayField: '',
    valueField: '',
    name : '',
    validator : '',
    initComponent: function() {
        treeWindow = undefined;
        var me = this;
        var formTextField = Ext.apply(
                {
                    xtype: 'textfield',
                    msgTarget: 'under',
                    columnWidth: 1,
                    allowBlank: true,
                    enableKeyEvents: true,
                    readOnly : true,
                    listeners: {
                        keyup: me.onKeyReleasedFilter,
                        blur: me.onBlurComp,
                        focus: me.onFocusComp
                    }
                }, me.filterTextField || {});
        var hiddenField = Ext.apply(
                {
                    xtype: 'hiddenfield',
                    name : me.name,
                    validator : me.validator
                }, me.hiddenValueHolder || {});
        var triggerBtn = Ext.apply(
                {
                    xtype: 'button',
                    cls: 'x-trigger-index-0 x-form-trigger  x-form-trigger-first',
                    margin: '0 0 0 -7',
                    style: "border-radius: 0px; border-left: 1px rgb(201, 201, 201) solid;",
                    overCls: 'x-formButtonOver',
                    iconCls: 'icon-lov',
                    listeners: {
                        click: me.onClickedTrigger
                    }
                }, me.triggerButton || {});
        Ext.applyIf(me, {
            items: [
                formTextField,
                hiddenField,
                triggerBtn
            ],
            listeners: {
                destroy: function() {
                    console.log("destroyed,,,,,,");
                    me.onParentDestroyed();
                }
            }
        });
        Ext.EventManager.onWindowResize(function(w, h) {
            me.onResizeComp();
        });
        me.callParent(arguments);
    },
    onParentDestroyed: function() {
        if (Ext.isDefined(treeWindow)) {
            treeWindow.close();
            treeWindow = undefined;
        }
    },
    onResizeComp: function() {
        if (Ext.isDefined(treeWindow)) {
            treeWindow.close();
            treeWindow = undefined;
        }
    },
    onFocusComp: function() {
        if (Ext.isDefined(treeWindow)) {
            treeWindow.close();
            treeWindow = undefined;
        }
    },
    onBlurComp: function() {

    },
    onClickedTrigger: function(btn) {
        var me = btn.up('fieldcontainer');
        if (!Ext.isDefined(treeWindow)) {
            treeWindow = Ext.create("Ext.window.Window", {
                width: ((btn.up('fieldcontainer').getComponent(0).getWidth() + btn.getWidth()) - 6.5),
                layout: 'fit',
                frame: false,
                border: false,
                bodyBorder: false,
                shadow: false,
                draggable: false,
                closable: false,
                resizable: false,
                maxHeight: 200,
                listeners: {
                    deactivate: function() {
                        console.log("deactivated................");
                        this.close();
                        treeWindow = undefined;
                    }
                }
            });
            treeWindow.insert(0, me.getTreeGrid());
            if (Ext.getStore(me.store).loadingSeq <= 0) {
                Ext.getStore(me.store).load();
            }
            (treeWindow).showBy(btn, "tr-br");
        } else {
            console.log("close win.....");
            treeWindow.close();
            treeWindow = undefined;
        }

    },
    onKeyReleasedFilter: function() {
        console.log("keypressed..............");
    },
    getTreeGrid: function() {
        var me = this;
        var treePanel = Ext.create('Ext.tree.Panel', {
            rootVisible: false,
            forceFit: true,
            store: Ext.getStore(me.treeStore),
            border: false,
            hideHeaders: true,
            columns: [
                {
                    xtype: 'treecolumn',
                    dataIndex: me.displayField,
                    header: false,
                    menuDisabled: true
                }
            ]
        });
        treePanel.on('itemclick', this.onTreeItemClick, this);
        return treePanel;
    },
    onTreeItemClick: function(ownerCmp, record, item, index, e, eOpts) {
        this.setValue(record.raw.id);
        console.log(record.raw.id, this.getValue());
        if (Ext.isDefined(treeWindow)) {
            treeWindow.close();
            treeWindow = undefined;
        }
    },
    setValue: function(id) {
        var me = this;
        this.getComponent(1).setValue(id);
        if (Ext.getStore(me.store).loadingSeq <= 0) {
            Ext.getStore(me.store).load({
                callback: function(store) {
                    var rec = store.findRecord(me.valueField, id, 0, false, true, true);
                    me.getComponent(0).setValue(rec.get(me.displayField));
                }
            });
        } else {
            var rec = Ext.getStore(this.store).findRecord(me.valueField, id, 0, false, true, true);
            this.getComponent(0).setValue(rec.get(me.displayField));
        }
    },
    getValue: function() {
        return this.getComponent(1).getValue();
    }
});

