Ext.define('Ext.ux.form.field.TreePicker', {
    extend: 'Ext.form.field.Picker',
    alias: 'widget.treepicker',
    requires: [
        'Ext.form.field.Picker',
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.tree.*'
    ],
    maxLevel: 0,
    childrenOnly: true,
    compStore: '',
    parentRef: '',
    idRef: '',
    codeRef: '',
    descRef: '',
    levelRef: '',
    emptyText: 'Click to Select',
    enableKeyEvents: true,
    editable: false,
    onRender: function() {
        this.callParent(arguments);
        this.nextSibling().on("change",this._updatePickerValue,this);
        this.setValue = Ext.Function.createSequence(this.setValue, this._onUpdateHidden, this);
    },
    _updatePickerValue: function(hiddenElement, newValue, oldValue) {
        console.log(newValue,'onchange');
        this.setValue(newValue);
        this.setRawValue(newValue);
    },
    initEvents: function() {
        var input = this.inputEl;
        input.on("click", this._onClick, this);
        input.on("focus", this._onFocus, this);
        this.callParent();
    },
    _onClick: function() {
        if (this.isExpanded) {
            this.collapse();
        } else {
            this.expand();
        }
    },
    createPicker: function() {
        var me = this;
        return Ext.create('Ext.window.Window', {
            width: 200,
            minHeight: 100,
            layout: 'fit',
            closable: false,
            draggable: false,
            resizable: false,
            frameHeader: false,
            height: 200,
            overflowY: 'auto',
            items: [me._createTree()]
        });
    },
    _createTree: function() {
        var me = this;
        var treePanel = Ext.create('Ext.tree.Panel', {
            rootVisible: false,
            forceFit: true,
            selType: 'rowmodel',
            columns: [
                {
                    xtype: 'treecolumn',
                    header: me.fieldLabel,
                    dataIndex: 'text'
                }
            ]
        });
        treePanel.on('afterrender', this._onTreeRendered, this);
        treePanel.on('itemclick', this._onTreeItemClick, this);
        return treePanel;
    },
    _onTreeRendered: function(treePanel, eOpts) {
        this._createTreeNodes(this._checkStoreLoadedBefore(), treePanel);
    },
    _onTreeItemClick: function(ownerCmp, record, item, index, e, eOpts) {
        var me = this;
        if (me.childrenOnly) {
            if (record.raw[me.levelRef] === me.maxLevel) {
                me.setValue(record.raw[me.idRef]);
                me.setRawValue(record.raw.text);
                me.collapse();
            }
        } else {
            me.setValue(record.raw[me.idRef]);
            me.setRawValue(record.raw.text);
            me.collapse();
        }
    },
    _checkStoreLoadedBefore: function() {
        return Ext.getStore(this.compStore).loadingSeq === 0 ? false : true;
    },
    _createTreeNodes: function(storeLoaded, treePanel) {
        var me = this;
        if (storeLoaded) {
            this._createRootJSON(Ext.getStore(me.compStore).data.items, treePanel);
        } else {
            Ext.getStore(me.compStore).load({
                callback: function(records) {
                    me._createRootJSON(records, treePanel);
                }
            });
        }
    },
    _createRootJSON: function(records, treePanel) {
        var me = this;
        var maxLocLevel = this.maxLevel;
        var map = {};
        for (var i = 0; i < records.length; i++) {
            var obj = records[i].data;
            obj.id = records[i].get(this.idRef);
            obj.text = records[i].get(this.codeRef) + " - " + records[i].get(this.descRef);
            obj.children = [];
            map[records[i].get(this.idRef)] = obj;
            var parent = records[i].get(this.parentRef) || '-';
            if (obj[me.levelRef] === maxLocLevel) {
                obj.leaf = true;
            }
            if (!map[parent]) {
                map[parent] = {
                    children: []
                };
                map[parent].children.push(obj);
            } else {
                map[parent].children.push(obj);
            }
        }
        treePanel.setRootNode(map['-']);
    },
    _onFocus: function() {
    },
    _onBlur: function() {
        if (this.isExpanded) {
            this.collapse();
        }
    },
    _onUpdateHidden: function() {
        this.nextSibling('[name=' + this.refName + ']').setValue(this.getValue());
    }
});