/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('Ext.ux.form.field.TelField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.phonefield',
    noSymbols: true,
    noSpaces: true,
    minLength : 10,
    maxLength : 10,
    
    onChange: function(newVal, oldVal) {
        this.callParent(arguments);
        this.autoSize();
//        var validatedVal = newVal;
//        var spaceReplace = /^\s\s*/;
//        validatedVal = validatedVal.replace(spaceReplace, '');
//        var symbolReplace = /[A-Z!@^()_+=`;',.$/[\]\\\~#%&*{}/:<>?|\"-]/;
//        validatedVal = validatedVal.replace(symbolReplace, '');
//        this.setValue(validatedVal);
    }
});