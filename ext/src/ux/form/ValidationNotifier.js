/**
 * @author Hashan Madushanka Jayakody
 * @class Ext.ux.form.field.ValidationNotifier
 * 
 * Provides an easy to notify validation message side of the field
 */
Ext.define('Ext.ux.form.ValidationNotifier', {
    extend: 'Ext.Component',
    alias: 'widget.vmessage',
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            height: 27,
            maxWidth : 200,
            style: {
                width: 'auto',
                backgroundColor : '#f2dede',
                border : '1px solid #eed3d7',
                color: '#b94a48',
                textShadow : '0 1px 0 rgba(255, 255, 255, 0.5)',
                fontSize: '14px',
                padding : '5px',
                borderRadius : '3px'
            },
            renderTo: Ext.getBody()
        });
        
        me.callParent(arguments);
    }
});
