# fa-flat-theme/sass/etc

This folder contains miscellaneous SASS files. Unlike `"fa-flat-theme/sass/etc"`, these files
need to be used explicitly.
