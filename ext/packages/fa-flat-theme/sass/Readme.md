# fa-flat-theme/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    fa-flat-theme/sass/etc
    fa-flat-theme/sass/src
    fa-flat-theme/sass/var
